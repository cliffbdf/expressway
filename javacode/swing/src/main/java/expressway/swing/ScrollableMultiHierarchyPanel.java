/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.swing;


import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import expressway.swing.MultiHierarchyPanel.*;


public class ScrollableMultiHierarchyPanel extends JScrollPane
{
	private MultiHierarchyPanel hPanel;
	
	
	public void addTreeTableComponent(int position, TreeTableComponent tt)
	{
		hPanel.addTreeTableComponent(position, tt);
		repaint();
	}
	
	
	public void addCrossMapPanel(int position, CrossMapPanel p)
	{
		hPanel.addCrossMapPanel(position, p);
		repaint();
	}
	

	public MultiHierarchyPanel getMultiHierarchyPanel() { return hPanel; }
	public TreeTableComponent getTreePanel(int pos) { return hPanel.getTreeTableComponent(pos); }
	public CrossMapPanel getCrossMapPanel(int pos) { return hPanel.getCrossMapPanel(pos); }


	public ScrollableMultiHierarchyPanel(String name)
	{
		super();
		this.setName(name);
		this.hPanel = new MultiHierarchyPanel(name);
		setViewportView(hPanel);
		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
	}


	public void postConstructionSetup()
	throws
		Exception
	{
		hPanel.postConstructionSetup();
	}
}

