/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.graph;


import java.awt.Graphics;
import java.awt.Color;
import java.util.List;
import expressway.geometry.Point;
import expressway.generalpurpose.Truncate;
import expressway.statistics.HistogramDomainParameters;
import expressway.statistics.TimeSeriesParameters;


/**
 * Container for deterination of the optimal dimensions of a Graph for displaying
 * a set of data, based on the domain and range of the data and on the dimensions
 * of the display.
 */
 
public class GraphDimensions implements java.io.Serializable
{
	public double pixelXSize;
	public double pixelYSize;
	
	public double xStartLogical;
	public double yStartLogical;
	
	public double xEndLogical;
	public double yEndLogical;
	
	public double xDeltaLogical;
	public double yDeltaLogical;


	/**
	 * Compute optimal Graph dimensions, and return the results in this object.
		bucketSize;
		bucketStart;
		noOfBuckets;
	 */
	 
	public void getOptimalDimensions(HistogramDomainParameters histoParams,
		GraphInput graphInput, int displayWidth, int displayHeight)
	{		
		xStartLogical = histoParams.bucketStart;
		yStartLogical = 0.0;  // could instead use graphInput.minYValue
		
		xEndLogical = histoParams.bucketStart + histoParams.bucketSize * 
			histoParams.noOfBuckets;
		yEndLogical = graphInput.maxYValue;
		
		xDeltaLogical = histoParams.bucketSize;
		yDeltaLogical = (((int)(graphInput.maxYValue * 10.0)) + 100) / 100;
		

		// Compute pixel sizes.
		
		pixelXSize = (1.2) * (xEndLogical - xStartLogical) / (displayWidth - 100);
		pixelYSize = (1.2) * (yEndLogical - yStartLogical) / (displayHeight - 200);
	}
	
	
	/**
	 * Compute optimal Graph dimensions, and return the results in this object.
		periodDuration;
		startTime;
		noOfPeriods;
	 */
	 
	public void getOptimalDimensions(TimeSeriesParameters params,
		GraphInput graphInput, int displayWidth, int displayHeight)
	{
		xStartLogical = params.startTime;
		yStartLogical = graphInput.minYValue;
		//yStartLogical = 0.0;
		
		xEndLogical = params.startTime + params.periodDuration * params.noOfPeriods;
		yEndLogical = graphInput.maxYValue;
		
		xDeltaLogical = params.periodDuration;
		yDeltaLogical = 
			(((int)((graphInput.maxYValue  - graphInput.minYValue) * 10.0)) + 100) / 100;
		
		
		// Compute pixel sizes.
		
		pixelXSize = (1.2) * (xEndLogical - xStartLogical) / (displayWidth - 100);
		pixelYSize = (1.2) * (yEndLogical - yStartLogical) / (displayHeight - 100);
		
		if (pixelXSize == 0.0) pixelXSize = displayWidth/500;
		if (pixelYSize == 0.0) pixelYSize = displayHeight/500;
	}
	

	public void getOptimalDimensions(GraphInput graphInput, int displayWidth, 
		int displayHeight)
	throws
		Graph.InvalidGraph  // if the display dimensions are too small
	{
		if (displayWidth < 300) throw new Graph.InvalidGraph("Display too narrow");
		if (displayHeight < 300) throw new Graph.InvalidGraph("Display too short");
		
		xStartLogical = graphInput.xValues[0];
		yStartLogical = graphInput.minYValue;
		
		xEndLogical = graphInput.xValues[graphInput.xValues.length-1];
		yEndLogical = graphInput.maxYValue;
		
		double domainWidth = xEndLogical - xStartLogical;
		if (domainWidth < 0.0) throw new Graph.InvalidGraph("Domain width is negative");
		if (domainWidth == 0.0) throw new Graph.InvalidGraph("Domain width is zero");
		//if (domainWidth == 0.0) domainWidth = xEndLogical/2.0;
		xDeltaLogical = Truncate.roundToNearestDigit(domainWidth / 10.0);
		if (xDeltaLogical <= 0.0) throw new RuntimeException(
			"xDeltaLogical=" + xDeltaLogical + "; domainWidth=" + domainWidth);
		//System.out.println("xDeltaLogical=" + xDeltaLogical);
		
		yDeltaLogical = Truncate.roundToNearestDigit(
			(graphInput.maxYValue - graphInput.minYValue)/ 10.0);
		if (yDeltaLogical <= 0.0) throw new RuntimeException(
			"yDeltaLogical=" + yDeltaLogical + "; graphInput.maxYValue="
			+ graphInput.maxYValue + ", graphInput.minYValue=" + graphInput.minYValue);
		//System.out.println("yDeltaLogical=" + yDeltaLogical);
		
		
		// Compute pixel sizes.
		
		pixelXSize = (1.2) * domainWidth / (displayWidth - 100);
		if (pixelXSize == 0)
		{
			System.out.println("graphInput.xValues.length=" + graphInput.xValues.length);
			System.out.println("xEndLogical=" + xEndLogical);
			System.out.println("xStartLogical=" + xStartLogical);
			System.out.println("displayWidth=" + displayWidth);
			
			throw new RuntimeException("pixelXSize is zero");
		}
		
		double rangeWidth = graphInput.maxYValue - graphInput.minYValue;
		//double rangeWidth = yEndLogical - yStartLogical;
		if (rangeWidth == 0.0) rangeWidth = yEndLogical/2.0;
		pixelYSize = 1.2 * rangeWidth / (displayHeight - 100);
	}

	
	public static class ZeroDomainWidth extends Graph.InvalidGraph
	{
		static final String stdMsg = "All X values are the same";
		
		public ZeroDomainWidth() { super(stdMsg); }
		public ZeroDomainWidth(String msg) { super(stdMsg + ": " + msg); }
		public ZeroDomainWidth(Throwable cause) { super(stdMsg, cause); }
		public ZeroDomainWidth(String msg, Throwable cause) { super(stdMsg + ": " + 
			msg, cause); }
	}
	
	
	public void dump()
	{
		System.out.println("GraphDimensions:");
		System.out.println("\tpixelXSize=" + pixelXSize);
		System.out.println("\tpixelYSize=" + pixelYSize);
		System.out.println("\txStartLogical=" + xStartLogical);
		System.out.println("\tyStartLogical=" + yStartLogical);
		System.out.println("\txEndLogical=" + xEndLogical);
		System.out.println("\tyEndLogical=" + yEndLogical);
		System.out.println("\txDeltaLogical=" + xDeltaLogical);
		System.out.println("\tyDeltaLogical=" + yDeltaLogical);
	}
}

