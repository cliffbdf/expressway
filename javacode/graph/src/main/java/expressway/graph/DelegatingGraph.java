/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.graph;


import java.awt.Graphics;
import java.awt.Color;
import java.util.List;
import expressway.geometry.Point;



/**
 * For creating a graph that delegates to an embedded Graph instance.
 *
 * The data to be graphed can be provided either as a List of Points, or as arrays
 * of X and Y values.
 */

public abstract class DelegatingGraph extends Graph
{
	protected Graph delegate;
	
	
	public DelegatingGraph(Graph g)
	{
		this.delegate = g;
	}


	public Graph copyConfiguration() { return delegate.copyConfiguration(); }
	
	
	public void draw(Graphics g)
	throws
		InvalidGraph
	{
		delegate.draw(g);
	}
	
	
	public String getXAxisLabel() { return delegate.getXAxisLabel(); }
	public void setXAxisLabel(String s) { delegate.setXAxisLabel(s); }
	
	public String getYAxisLabel() { return delegate.getYAxisLabel(); }
	public void setYAxisLabel(String s) { delegate.setYAxisLabel(s); }
	
	public Color getAxisColor() { return delegate.getAxisColor(); }
	public void setAxisColor(Color c) { delegate.setAxisColor(c); }
	
	public Color getPointColor() { return delegate.getPointColor(); }
	public void setPointColor(Color c) { delegate.setPointColor(c); }
	
	public List<Point> getPoints() { return delegate.getPoints(); }
	public void setPoints(List<Point> points) { delegate.setPoints(points); }
	
	public double[] getXValues() { return delegate.getXValues(); }
	public void setXValues(double[] xValues) { delegate.setXValues(xValues); }
	
	public double[] getYValues() { return delegate.getYValues(); }
	public void setYValues(double[] yValues) { delegate.setYValues(yValues); }
	
	public double getXStartLogical() { return delegate.getXStartLogical(); }
	public void setXStartLogical(double x) { delegate.setXStartLogical(x); }
	
	public double getXEndLogical() { return delegate.getXEndLogical(); }
	public void setXEndLogical(double x) { delegate.setXEndLogical(x); }
	
	public double getXDeltaLogical() { return delegate.getXDeltaLogical(); }
	public void setXDeltaLogical(double d) { delegate.setXDeltaLogical(d); }
	
	public double getYStartLogical() { return delegate.getYStartLogical(); }
	public void setYStartLogical(double y) { delegate.setYStartLogical(y); }
	
	public double getYEndLogical() { return delegate.getYEndLogical(); }
	public void setYEndLogical(double y) { delegate.setYEndLogical(y); }
	
	public double getYDeltaLogical() { return delegate.getYDeltaLogical(); }
	public void setYDeltaLogical(double d) { delegate.setYDeltaLogical(d); }
	
	public double getPixelXSize() { return delegate.getPixelXSize(); }
	public void setPixelXSize(double psize) { delegate.setPixelXSize(psize); }
	
	public double getPixelYSize() { return delegate.getPixelYSize(); }
	public void setPixelYSize(double psize) { delegate.setPixelYSize(psize); }
	
	public int getMaxXAxisLength() { return delegate.getMaxXAxisLength(); }
	public void setMaxXAxisLength(int max) { delegate.setMaxXAxisLength(max); }

	public int getMaxYAxisLength() { return delegate.getMaxYAxisLength(); }
	public void setMaxYAxisLength(int max) { delegate.setMaxYAxisLength(max); }
	
	public int getXStartDisplay() { return delegate.getXStartDisplay(); }
	public void setXStartDisplay(int xStart) { delegate.setXStartDisplay(xStart); }
	
	public int getYStartDisplay() { return delegate.getYStartDisplay(); }
	public void setYStartDisplay(int yStart) { delegate.setYStartDisplay(yStart); }

	public int getXAxisNotchLength() { return delegate.getXAxisNotchLength(); }
	public void setXAxisNotchLength(int notchLength) { delegate.setXAxisNotchLength(notchLength); }
	
	public int getYAxisNotchLength() { return delegate.getYAxisNotchLength(); }
	public void setYAxisNotchLength(int notchLength) { delegate.setYAxisNotchLength(notchLength); }
	
	public int getPointRectXLength() { return delegate.getPointRectXLength(); }
	public void setPointRectXLength(int rectSize) { delegate.setPointRectXLength(rectSize); }

	public int getPointRectYLength() { return delegate.getPointRectYLength(); }
	public void setPointRectYLength(int rectSize) { delegate.setPointRectYLength(rectSize); }
}

