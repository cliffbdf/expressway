/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.geometry;


/** ****************************************************************************
 * Represents a geometric face of a rectangular object.
 */
 
public class Face
{
	private Point corner1;
	private Point corner2;
	private Point midpoint;
	private Ray outwardRay;
	
	
	/**
	 * Construct a Face with the specified entpoints, such that the Face's outward
	 * direction is a positive right angle from the path from the first Point to
	 * the second Point.
	 */
	 
	public Face(Point corner1, Point corner2)
	throws
		Exception
	{
		this.corner1 = corner1;
		this.corner2 = corner2;
		
		Ray ray = new Ray(corner1, corner2);
		double angleFromCorner1toCorner2 = ray.getRadians();
		double angle = angleFromCorner1toCorner2 + Math.PI/2.0;
		
		double dx = corner2.getX() - corner1.getX();
		double dy = corner2.getY() - corner1.getY();
		this.midpoint = new PointImpl(corner1.getX() + dx/2.0, corner1.getY() + dy/2.0);
		
		this.outwardRay = new Ray(this.midpoint, angle, 1.0);
	}
	
	
	/**
	 * Construct a Face with the specified endpoints, such that the Face's outward
	 * direction is the specified angle in radians.
	 */
	 
	public Face(Point corner1, Point corner2, double outwardRadians)
	throws
		Exception
	{
		this.corner1 = corner1;
		this.corner2 = corner2;
		
		double dx = corner2.getX() - corner1.getX();
		double dy = corner2.getY() - corner1.getY();
		this.midpoint = new PointImpl(corner1.getX() + dx/2.0, corner1.getY() + dy/2.0);
		
		this.outwardRay = new Ray(this.midpoint, outwardRadians, 1.0);
	}
	
	
	/**
	 * Construct a Face of a rectangle, with the specified endpoints and the
	 * specified center Point of the rectangle.
	 */
	 
	public Face(Point corner1, Point corner2, Point center)
	throws
		Exception
	{
		this.corner1 = corner1;
		this.corner2 = corner2;
		
		this.midpoint = PointImpl.findMidpoint(corner1, corner2);
		
		this.outwardRay = new Ray(center, midpoint);
		this.outwardRay.setMagnitude(PointImpl.findDistance(center, midpoint) + 1.0);
		this.outwardRay.setOrigin(midpoint);
		
		// Check that the center Point is midway between the two endpoints.		
		
		if (Math.abs(Ray.dot(this.outwardRay, new Ray(corner1, corner2))) > .01)
			throw new Exception("Center point of Face is not actually in the center.");
	}
	
	
	public Point getCorner1() { return corner1; }
	public Point getCorner2() { return corner2; }
	public Point getMidpoint() { return midpoint; }
	public Ray getOutwardRay() { return outwardRay; }
	
	
	/**
	 * Create a unit-magnitude Ray projecting outward from this Face and
	 * perpendicular to it.
	 */
	 
	public Ray createOutwardRay()
	throws
		Exception
	{
		return new Ray(outwardRay.getOrigin(), outwardRay.getRadians());
	}
	
	
	/**
	 * Determine if this Face intersects the specified Rectangular area.
	 */
	 
	public boolean intersects(Rectangle rectangle)
	{
		LineSegment faceSegment = new LineSegment(corner1, corner2);
		
		//System.out.println("Checking if segment " + faceSegment + " intersects " + rectangle);
		
		for (int i = 0; i < 4; i++)
		{
			Point p1 = rectangle.corners[i];
			Point p2 = rectangle.corners[(i+1)%4];
			try
			{
				if (faceSegment.intersects(new LineSegment(p1, p2)))
				{
					//System.out.println("It does:");
					//System.out.println("\t" + faceSegment + "\n\tintersects"
					//	+ "\n\t" + new LineSegment(p1, p2));
					
					return true;
				}
			}
			catch (Exception ex) // Segments are congruent.
			{
				//System.out.println("Congruence: it does: ex:");
				//ex.printStackTrace();
				return true;
			}
		}
	
		return false;
	}
	
	
	public LineSegment getAsLineSegment()
	{
		LineSegment segment = new LineSegment(corner1, corner2);
		
		return segment;
	}
	
	
	public String toString()
	{
		return "{ Face: " + corner1.toString() + ", " + corner2.toString() +
			", " + midpoint.toString() + ", " + outwardRay.toString() + " }";
	}
}

