/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.geometry;


/** ************************************************************************
 * A Ray is a line that projects from a given point, in a given direction, with
 * a given length (magnitude). A Ray can be thought of as a mathematical vector
 * that begins at a given point in space. The magnitude must be positive or zero.
 * The magnitude is not a "length": a Ray is infinite in length.
 */

public class Ray
{
	private Point origin;
	private double radians;
	private double magnitude;
	
	
	/**
	 * Construct a canonicalized Ray based on the input parameters. The resulting
	 * Ray will have an angle between 0 and 2pi radians. A negative magnitude is
	 * not allowed.
	 */
	 
	public Ray(Point origin, double radians, double magnitude)
	throws
		Exception
	{
		init(origin, radians, magnitude);
	}
	
	
	public Ray(Point origin, double radians)
	throws
		Exception
	{
		this(origin, radians, 1.0);
	}
	
	
	/** ************************************************************************
	 * Construct a Ray that projects from one Point to another. The magnitude is
	 * considered to be the distance between the two Points.
	 */
	 
	public Ray(Point origin, Point destination)
	throws
		Exception
	{
		double dx = destination.getX() - origin.getX();
		double dy = destination.getY() - origin.getY();
		double mag = Math.hypot(dx, dy);
		
		double tan = dy / dx;
		double rad = Math.atan(tan);
		if (dx < 0) rad = rad + Math.PI;
		
		init(origin, rad, mag);
	}
	
	
	private void init(Point origin, double radians, double magnitude)
	throws
		Exception
	{
		if (magnitude < 0) throw new Exception(
			"A Ray's magnitude must be greater than zero.");
		
		this.origin = origin;
		this.radians = radians;
		this.magnitude = magnitude;
		
		// Canonicalize to an angle between 0 and 2 pi radians.
		double n = Math.floor( radians / (Math.PI * 2.0) );
		if (Math.abs(n) > 1.0)
		{
			if (n > 0.0)
				radians = radians - n * 2.0 * Math.PI;
			else
				radians = radians + n * 2.0 * Math.PI;
		}
	}
	
	
	public Point getOrigin() { return origin; }
	
	public double getRadians() { return radians; }
	
	public double getMagnitude() { return magnitude; }
	
	public void setOrigin(Point origin) { this.origin = origin; }
	
	public void setRadians(double radians) { this.radians = radians; }
	
	public void setMagnitude(double magnitude)
	throws
		Exception
	{
		if (magnitude < 0) throw new Exception(
			"A Ray's magnitude must be greater than zero.");
		
		this.magnitude = magnitude;
	}
	
	
	/**
	 * Add the specified radians to the Ray's angle. However, normalize the resulting
	 * angle so that it is within the range 0 to 2PI.
	 */
	 
	public void addModulo(double radiansToAdd)
	{
		this.radians += radiansToAdd;
		this.radians = this.radians % (2.0 * Math.PI);
	}
	
	
	/**
	 * Reverse the direction of this Ray, so that it points PI radians from its
	 * prior direction.
	 */
	 
	public void reverse()
	{
		this.addModulo(Math.PI);
	}
	
	
	/** ************************************************************************
	 * Create a new Ray, parallel to the specified Ray, with origin at the
	 * specified Point.
	 */
	 
	public static Ray createRay(Ray parallelRay, Point origin)
	throws
		Exception
	{
		return new Ray(origin, parallelRay.radians);
	}
	
	
	/** ************************************************************************
	 * Find the Point of intersection of the two specified Rays. If they do not
	 * intersect (i.e., they diverge), then return null.
	 * See the design slide "Finding the Intersection of Two Rays".
	 * If they overlap, thrown an OverlapException.
	 */
	 
	public static Point findIntersection(Ray ray1, Ray ray2)
	throws
		Exception
	{
		//System.out.println("Ray.findIntersection for \n\t" + ray1 + " and \n\t" + ray2);
		
		double theta1 = ray1.radians;
		double theta2 = ray2.radians;
		
		
		// Determine if either (or both) of the Rays are vertical. (If so, the
		// slope is infinite and the algorithm used must be different.)
		
		boolean theta1IsVertical = false;
		if (PointImpl.areAlmostEqual(Math.cos(theta1), 0.0))
		{
			theta1IsVertical = true;  // infinite slope.
		}
		
		boolean theta2IsVertical = false;
		if (PointImpl.areAlmostEqual(Math.cos(theta2), 0.0))
		{
			theta2IsVertical = true;  // infinite slope.
		}
		

		// Define convenience variables.
		
		double o1x = ray1.origin.getX();
		double o1y = ray1.origin.getY();
		double o2x = ray2.origin.getX();
		double o2y = ray2.origin.getY();
		
		
		// Find the intersection of the lines going through each Ray.
		
		double tan1;
		double tan2;
		
		double xInt;
		double yInt;

		if (theta1IsVertical && theta2IsVertical)  // both Rays are vertical
		{
			if (o1x != o2x) return null;
			if (ray1.contains(ray2.getOrigin())) throw new Exception(constructOverlapErrorMessage(ray1, ray2));
			if (ray2.contains(ray1.getOrigin())) throw new Exception(constructOverlapErrorMessage(ray1, ray2));
			return null;
		}
		else if (theta1IsVertical)  // ray1 is vertical but ray2 is not
		{
			tan2 = Math.tan(theta2);
			xInt = o1x;
			yInt = tan2 * (o1x - o2x) + o2y;
		}
		else if (theta2IsVertical)  // ray2 is vertical but ray1 is not
		{
			tan1 = Math.tan(theta1);
			xInt = o2x;
			yInt = tan1 * (o2x - o1x) + o1y;
		}
		else  // neither Ray is vertical
		{
			tan1 = Math.tan(theta1);
			tan2 = Math.tan(theta2);
			
			if (PointImpl.areAlmostEqual(tan1, tan2))  // possible overlap of each Ray
			{
				if (ray1.contains(ray2.getOrigin())) throw new Exception(constructOverlapErrorMessage(ray1, ray2));
				if (ray2.contains(ray1.getOrigin())) throw new Exception(constructOverlapErrorMessage(ray1, ray2));
				return null;  // Rays are parallel but do not intersect.			
			}
			else
			{
				tan1 = Math.tan(theta1);
				tan2 = Math.tan(theta2);
	
				xInt = ( -tan2 * o2x + o2y + tan1 * o1x - o1y ) / ( tan1 - tan2 );
				yInt = tan1 * ( xInt - o1x ) + o1y;
			}
		}
		
		
		// Check that the intersection point of the two lines falls on each Ray.
		
		Point intersectionPoint = new PointImpl(xInt, yInt);

		if (! ray1.contains(intersectionPoint)) return null;
		if (! ray2.contains(intersectionPoint))
		{
			//System.out.println(ray2 + " does not contain " + intersectionPoint);
			return null;
		}
		
		return intersectionPoint;
	}
	
	
	protected static String constructOverlapErrorMessage(Ray ray1, Ray ray2)
	{
		return "Rays overlap: " + ray1.toString() + ", " + ray2.toString();
	}
	
	
	/** ************************************************************************
	 * Return the dot product of the mathematical vectors represented by the
	 * two Rays.
	 */
	 
	public static double dot(Ray ray1, Ray ray2)
	{
		return ray1.magnitude * ray2.magnitude * Math.cos(ray2.radians - ray1.radians);
	}
	
	
	/**
	 * Return true if this Ray contains the specified Point. The Ray contains the
	 * Point if the Point is along the line containing the Ray, but at or past the
	 * origin Point.
	 */
	 
	public boolean contains(Point point)
	throws
		Exception
	{
		if (point == null) throw new Exception("Point is null");
		
		if (PointImpl.areAlmostEqual(Math.cos(radians), 0.0))  // vertical
		{
			return PointImpl.areAlmostEqual(origin.getX(), point.getX());
		}
		
		
		double m = Math.tan(radians);
		
		double b = origin.getY() - m * origin.getX();		
		double mx = m * point.getX();
		
		// If |mx| and |b| are of the order of 10^15 and opposite in sign, then
		// the calculation mx+b will not be reliable, and we should treat this Ray
		// as vertical and merely check if its origin's x coordinate is equal to
		// the point's x coordinate.
		
		if ((Math.abs(mx) > 1.0E15) && (Math.abs(b) > 1.0E15)
			&& (Math.signum(mx) != Math.signum(b)))
		{
			return PointImpl.areAlmostEqual(origin.getX(), point.getX());
		}
			
		
		// No numerical range difficulties: proceed for normal case.
		
		double yOfRayAtXOfPoint = mx + b;
		
		if (! PointImpl.areAlmostEqual(yOfRayAtXOfPoint, point.getY())) return false;
		
		
		// Create a Ray from the origin to the point, and see if it points toward
		// or away from this Ray. If the former, then they are aligned, and the
		// point is contained in this Ray.
		
		Ray ray = new Ray(origin, point);
		
		//System.out.println(this + " dot " + ray + "=" + Ray.dot(this, ray));
		
		if (Ray.dot(this, ray) >= 0.0) return true;
		//System.out.println("00000");
			
		return false;
	}
	
	
	/** ************************************************************************
	 * Determine the Point at which this Ray passes closest to the specified Point.
	 * If this Ray is directed away from the specified Point, then this Ray's origin
	 * is the closest Point.
	 */
	 
	public Point findClosestPoint(Point point)
	throws
		Exception
	{
		Point p = findIntersection(this, new Ray(point, this.radians + Math.PI/2.0));
		//if (p != null) System.out.println("\t\tintersection=" + p);
		if (p != null) return p;
		
		p = findIntersection(this, new Ray(point, this.radians - Math.PI/2.0));
		//if (p != null) System.out.println("\t\tIntersection =" + p);
		if (p != null) return p;
		
		//System.out.println("\t\tclosest point is origin: " + origin);
		return this.origin;
	}
	
	
	/** ************************************************************************
	 * Multiply this Ray's magnitude by the specified factor.
	 */
	 
	public Ray scalarMult(double factor)
	throws
		Exception
	{
		this.setMagnitude(factor * this.magnitude);
		return this;
	}
	
	
	/** ************************************************************************
	 * Return the Point at which this Ray terminates, based on its origin and
	 * its magnitude, which is interpreted as a length.
	 */
	 
	public Point getEndpoint()
	{
		double dx = magnitude * Math.cos(radians);
		double dy = magnitude * Math.sin(radians);
		return new PointImpl(origin.getX() + dx, origin.getY() + dy);
	}
	
	
	public String toString()
	{
		return "{ Ray: " + origin.toString() + ", radians=" + radians +
			", magnitude=" + magnitude + " }";
	}
}

