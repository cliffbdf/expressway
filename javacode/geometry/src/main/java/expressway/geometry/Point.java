/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.geometry;


/**
 * A two-dimensional coordinate locaton.
 */

public interface Point extends Cloneable
{
	double getX();
	double getY();
	
	void setX(double newX);
	void setY(double newY);
	
	
	//public Object clone() throws CloneNotSupportedException;


	Object clone() throws CloneNotSupportedException;
	
	//Object deepCopy();
}
