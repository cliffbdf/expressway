/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.statistics;

/**
 * Container for parameters that characterize the domain of a histogram.
 */
 
public class HistogramDomainParameters implements java.io.Serializable
{
	public double bucketSize;
	public double bucketStart;
	public int noOfBuckets;
	
	public String toString()
	{
		return "[ HistogramDomainParameters: bucketSize=" + bucketSize +
			", bucketStart=" + bucketStart + ", noOfBuckets=" + noOfBuckets + "]";
	}
}
	
	

