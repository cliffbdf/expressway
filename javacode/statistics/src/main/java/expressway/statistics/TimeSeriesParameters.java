/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.statistics;

/**
 * Container for parameters that characterize a time series of values.
 */
 
public class TimeSeriesParameters implements java.io.Serializable
{
	public long periodDuration;
	public long startTime;
	public int noOfPeriods;
	
	public String toString()
	{
		return "[ TimeSeriesParameters: periodDuration=" + periodDuration +
			", startTime=" + startTime + ", noOfPeriods=" + noOfPeriods + "]";
	}
}

