/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.statistics;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;


public class NormalizedGammaDistribution implements ContinuousDistribution
{
	public double shape;
	public double scale;
	public DistributionFactory distFactory = null;
	public ContinuousDistribution distribution = null;
	
	
	public NormalizedGammaDistribution(double distShape, double distScale)
	{
		this.shape = distShape;
		this.scale = distScale;
		
		distFactory = DistributionFactory.newInstance();
		distribution = distFactory.createGammaDistribution(distShape, 1.0);
	}
	
	
	public double cumulativeProbability(double x)
	throws MathException
	{
		return distribution.cumulativeProbability(x / scale);
	}
	
	
	public double cumulativeProbability(double x0, double x1)
	throws MathException
	{
		return distribution.cumulativeProbability(x0 / scale, x1 / scale);
	}
	
	
	public double inverseCumulativeProbability(double p)
	throws MathException
	{
		return distribution.inverseCumulativeProbability(p) * scale;
	}
}

