/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.debug;

import java.awt.*;


public class DumpComponents
{
	public static void dump(Component comp, int indentation)
	{
		String tabStr = "";

		int multipleOf16 = indentation / 16;
		for (int noOf16s = 0; noOf16s < multipleOf16; noOf16s++) tabStr += tabs[16];

		int remainder = indentation % 16;
		tabStr += tabs[remainder];

		System.out.println(tabStr + comp.getClass().getName() +
			" (" + comp.hashCode() + ")," +
			",bounds=" + comp.getBounds() + ",visible=" + comp.isVisible() +
			",showing=" + comp.isShowing());

		if (comp instanceof Container)
		{
			Component[] cs = ((Container)comp).getComponents();
			for (Component c : cs) dump(c, indentation+1);
		}
	}


	private static String[] tabs =
	{
		"",
		"\t",
		"\t\t",
		"\t\t\t",
		"\t\t\t\t",
		"\t\t\t\t\t",
		"\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	};
}
