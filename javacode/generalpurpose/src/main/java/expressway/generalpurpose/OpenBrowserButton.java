/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.generalpurpose;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.awt.Desktop;
import java.awt.event.*;
import java.net.URL;
import java.net.MalformedURLException;


/**
 * A Button that opens a browser to display the content at a URL.
 */
 
public class OpenBrowserButton extends JButton
{
	public OpenBrowserButton(final String title, String urlString)
	throws
		MalformedURLException
	{
		this(title, new URL(urlString));
	}
	
	
	public OpenBrowserButton(final String title, final URL url)
	{
		super(title);
		
		addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (Desktop.isDesktopSupported())
				{
					Desktop desktop = Desktop.getDesktop();
					if (desktop.isSupported(Desktop.Action.BROWSE))
					{
						// Open a browser window.
						try { desktop.browse(url.toURI()); }
						catch (Exception ex)
						{
							JOptionPane.showMessageDialog(OpenBrowserButton.this,
								ThrowableUtil.getAllMessages(ex), "Error",
								JOptionPane.ERROR_MESSAGE);
						}
						
						return;
					}
				}
				
				// Open a HTMLWindow instead.
				
				HTMLWindow htmlWindow = new HTMLWindow(title, true, true);
				
				try { htmlWindow.setURL(url); }
				catch (Exception ex)
				{
					JOptionPane.showMessageDialog(OpenBrowserButton.this,
						ThrowableUtil.getAllMessages(ex), "Error",
						JOptionPane.ERROR_MESSAGE); return;
				}
				
				htmlWindow.show();
			}
		});
	}
}

