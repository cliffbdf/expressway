/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.generalpurpose;

import java.net.*;
import java.io.*;
import javax.swing.JOptionPane;


public class HTTPPoster
{
	/**
	 * Perform an HTTP POST to a URL, providing the supplied HTTP name/value pairs.
	 * The pairs are provided in 'args' as name1, value1, name2, value2, etc.
	 */
	 
	public static void post(URL postURL, String... args)
	throws
		IOException,  // If a timeout occurs
		Exception  // If any other error occurs
	{
		if (args.length <= 1) throw new Exception("No arguments");
		
		if ((args.length /2) * 2 != args.length) throw new Exception(
			"The arguments must be in pairs: found " + args.length + " args");
		
		HttpURLConnection httpUrlConnection = null;
		
		try
		{
			URLConnection urlConnection = postURL.openConnection();
			
			httpUrlConnection = (HttpURLConnection)urlConnection;
			
			httpUrlConnection.setDoOutput(true);
			
			httpUrlConnection.setRequestMethod("POST");
			
	
			// Construct data
			
			String data = "";
			int i = 0;
			boolean firstTime = true;
			for (;;)
			{
				if (i >= args.length) break;
				
				String fieldName = args[i++];
				String fieldValue = args[i++];
				
				if (firstTime) firstTime = false;
				else data += "&";
				
				data += URLEncoder.encode(fieldName, "UTF-8") + "=" +
					URLEncoder.encode(fieldValue, "UTF-8");
			}
				
			
			// POST data to the server.
			
			httpUrlConnection.connect();
			
			OutputStreamWriter osw = 
				new OutputStreamWriter(httpUrlConnection.getOutputStream());
			
			osw.write(data);
			
			osw.flush();
			
			httpUrlConnection.getContent();
			
			int responseCode = httpUrlConnection.getResponseCode();
			
			switch (responseCode)
			{
				case 200: break;  // ok
				
				case 408:
					throw new IOException(
						"Timeout at remote bug reporting server: please click Submit again.");
				
				default:
					throw new Exception(
						"Response code: " + responseCode + ";\n" +
							"see http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html");
			}
		}
		finally
		{
			if (httpUrlConnection != null) httpUrlConnection.disconnect();
		}
	}


	/**
	 * Perform the POST in a separate Thread so that the call does not block.
	 * Automatically retry twice if there is a timeout error. If ultimately fails,
	 * display a popup to notify the user.
	 */
	 
	public static void postAsync(final URL postURL, final String... args)
	{
		(new Thread()
		{
			public void run()
			{
				for (int retries = 0; retries < 3; retries++)
				{
					try { post(postURL, args); }
					catch (IOException ioe) { continue; }
					catch (Exception ex)
					{
						JOptionPane.showMessageDialog(null, 
							ThrowableUtil.getAllMessages(ex), "Error", 
							JOptionPane.ERROR_MESSAGE); return;
					}
				}
			}
		}).start();
	}
}

