package expressway.generalpurpose;


import com.apple.eawt.ApplicationEvent;
import com.apple.eawt.ApplicationAdapter;
import expressway.awt.AWTTools;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;


public class AppleApplicationListener extends ApplicationAdapter
{
	private MainCallback mainCallback;
	private String aboutTitle;
	private String aboutText;
	private ImageIcon aboutIcon;


	public AppleApplicationListener()
	{
		super();
	}


	public void handleQuit(ApplicationEvent event)
	{
		mainCallback.closeMainWindowAndApplication();
		event.setHandled(false);
	}


	public void handleAbout(ApplicationEvent e)
	{
		e.setHandled(true);
		JOptionPane.showMessageDialog(null, aboutText, aboutTitle,
			JOptionPane.INFORMATION_MESSAGE, aboutIcon);
	}


	public void setMainFrame(MainCallback mainCallback) { this.mainCallback = mainCallback; }
	public void setAboutTitle(String title) { this.aboutTitle = title; }
	public void setAboutText(String text) { this.aboutText = text; }
	public void setAboutImageIcon(ImageIcon icon) { this.aboutIcon = icon; }
}
