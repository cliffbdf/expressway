/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */




package expressway.generalpurpose;


/**
 * Utilities for diagnosing thread issues, such as deadlock, native crash, 
 * infinite loop, etc.
 */
 
public class ThreadDiagnosticUtils
{
	public static Thread[] getThreads()
	{
		Thread[] th = new Thread[Thread.activeCount()+10];
		int noOfThreads = Thread.enumerate(th);
		
		Thread[] threads = new Thread[noOfThreads];
		for (int t = 0; t < noOfThreads; t++) threads[t] = th[t];
		
		return threads;
	}
	
	
	public static void dumpThreads(Thread[] threads)
	{
		System.out.println("Threads:");
		System.out.println("\tName\tState\tFile Name\tClass\tMethod\tLine No\tIs Native");
		for (Thread thread : threads)
		{
			System.out.print("\t"+ thread.getName() + "\t<" + thread.getState() + ">");
			
			StackTraceElement[] stes = thread.getStackTrace();
			if (stes.length == 0)
			{
				System.out.println();
				continue;
			}

			StackTraceElement ste0 = stes[0];
			System.out.println("\t"
				+ "'" + ste0.getFileName() + "'\t" 
				+ "'" + ste0.getClassName() + "'\t"
				+ "'" + ste0.getMethodName() + "'\t" 
				+ ste0.getLineNumber() + "\t"
				+ ste0.isNativeMethod());
		}
	}
	
	
	public static void dumpThreads()
	{
		dumpThreads(getThreads());
	}
	
	
	public static void timedSnapshot(final long timeToWait)
	{
		(new Thread()
		{
			public void run()
			{
				try { Thread.currentThread().sleep(timeToWait); }
				catch (InterruptedException ex)
				{
					return;
				}
				
				dumpThreads(getThreads());
			}
		}).start();
	}
}

