/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.generalpurpose;

import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.Document;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Insets;
import java.awt.Desktop;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.util.List;
import java.util.Vector;
import java.net.URL;


/**
 * A Panel for viewing rendered HTML.
 */
 
public class HTMLWindow extends JFrame
{
	private JPanel controlPanel;
	private JTextPane htmlPanel;
	private JButton saveAsButton;
	private JButton backButton;
	private JButton forwardButton;
	
	private List history = new Vector();
	private int historyIndex = -1;
	
	
	public void addHyperlinkListener(HyperlinkListener listener)
	{
		htmlPanel.addHyperlinkListener(listener);
	}
	
	
	public JButton createControlButton(String title, ActionListener listener)
	{
		JButton button;
		controlPanel.add(button = new JButton(title));
		button.addActionListener(listener);
		return button;
	}
	
	
	public HTMLWindow(String name, boolean includeSaveButton, 
		boolean includeBackForwardButtons)
	{
		super(name);
		setLayout(new BorderLayout());
		add(controlPanel = new JPanel(), BorderLayout.NORTH);
		htmlPanel = new JTextPane();
		htmlPanel.setMargin(new Insets(4, 4, 4, 4));
		htmlPanel.setContentType("text/html");
		
		//Put the HTML pane in a scroll pane.
		JScrollPane editorScrollPane = new JScrollPane(htmlPanel);
		editorScrollPane.setVerticalScrollBarPolicy(
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		editorScrollPane.setPreferredSize(new Dimension(800, 400));
		editorScrollPane.setMinimumSize(new Dimension(10, 10));
		add(editorScrollPane, BorderLayout.CENTER);
		
		if (includeBackForwardButtons)
		{
			controlPanel.add(backButton = new JButton("\u2190"));
			
			backButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					goBackOne();
				}
			});
		}
		
		if (includeSaveButton)
		{
			controlPanel.add(saveAsButton = new JButton("Save As"));
			saveAsButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					JFileChooser chooser = new JFileChooser();
					int returnVal = chooser.showSaveDialog(HTMLWindow.this);
					FileWriter fwriter = null;
					if(returnVal == JFileChooser.APPROVE_OPTION) try
					{
						File file = chooser.getSelectedFile();
						
						if (file.exists())
						{
							if (JOptionPane.showConfirmDialog(HTMLWindow.this,
								"File exists; overwrite?", "Please Confirm",
								JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
							{
								return;
							}
						}
						
						fwriter = new FileWriter(file);
						String text = htmlPanel.getText();
						fwriter.write(text, 0, text.length());
					}
					catch (IOException ex)
					{
						JOptionPane.showMessageDialog(HTMLWindow.this,
							ThrowableUtil.getAllMessages(ex), "Error",
							JOptionPane.ERROR_MESSAGE);
					}
					finally
					{
						try { if (fwriter != null) fwriter.close(); }
						catch (Throwable t) { t.printStackTrace(); }
					}
					
					//HTMLWindow.this.dispose();
				}
			});
		}
		
		if (includeBackForwardButtons)
		{
			controlPanel.add(forwardButton = new JButton("\u2192"));
			
			forwardButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					goForwardOne();
				}
			});
		}

		htmlPanel.addHyperlinkListener(new HyperlinkListener()
		{
			public void hyperlinkUpdate(HyperlinkEvent e)
			{
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
				{
					JTextPane pane = (JTextPane)(e.getSource());
					
					URL url = e.getURL();
					
					try
					{
						if (url.getHost().endsWith("expresswaysolutions.com") &&
								url.getPath().startsWith("/help/"))
						{
							addToHistory(url);
						}
						else
							if (Desktop.isDesktopSupported())
							{
								Desktop desktop = Desktop.getDesktop();
								if (desktop.isSupported(Desktop.Action.BROWSE))
								{
									// Open a browser window.
									desktop.browse(url.toURI());
								}
								else
								{
									addToHistory(url);
								}
							}
							else
							{
								addToHistory(url);
							}
					}
					catch (Throwable t) { t.printStackTrace(); }
				}
			}
		});
		
		validate();
	}
	
	
	public void setText(String text)
	{
		addToHistory(text);
	}
	
	
	public String getHTMLText() { return htmlPanel.getText(); }
	
	
	public void setURL(URL url)
	throws
		IOException
	{
		addToHistory(url);
	}
	
	
	public void setEditable(boolean editable)
	{
		htmlPanel.setEditable(editable);
	}


	protected void goBackOne()
	{
		if (historyIndex < 1) return;
		
		historyIndex--;
		
		showHistory();
	}
	
	
	protected void goForwardOne()
	{
		if (historyIndex == (history.size()-1)) return;
		
		historyIndex++;
		
		showHistory();
	}


	protected void addToHistory(Object obj)
	{
		// Pare all history following the current history index.
		for (int i = historyIndex+1; i < history.size();)
			history.remove(i);
		
		// Append the Object to the history.
		history.add(obj);
		
		historyIndex++;
		
		showHistory();
	}
	
	
	protected void showHistory()
	{
		if (historyIndex < 0) return;
		
		Object content = history.get(historyIndex);
		
		if (content instanceof String)
			htmlPanel.setText((String)content);
		else if (content instanceof URL)
			try
			{
				Document doc = htmlPanel.getDocument();
				doc.putProperty(Document.StreamDescriptionProperty, null);

				htmlPanel.setPage((URL)content);
			}
			catch (Exception ex) { ex.printStackTrace(); }
		else throw new RuntimeException("Unexpected content type");
	}
}

