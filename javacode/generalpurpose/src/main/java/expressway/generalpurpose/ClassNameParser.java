/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.generalpurpose;



/** ****************************************************************************
 * For translating class names between the dot-separated and dollar-separated
 * representations. The dot-separated notation is used in a Java program, but
 * the dollar-separated format is required by the Class.forName() method.
 */
 
public class ClassNameParser
{
	/** ************************************************************************
	 * If there is a package abc and a visible nested class abc, the nested
	 * class will be selected by a reference to abc. (This was determined
	 * empirically: see the test trythis.TestThis4.) Thus, first translate all
	 * periods to $, and progressively translate them back (from left to right)
	 * until either there are no more $s to translate back or the class is found.
	 */
	 
	public static Class recognizeQualifiedClassName(String dotSeparatedName)
	throws
		ClassNotFoundException
	{
		String dollarSeparatedName = dotSeparatedName.replace('.', '$');
		
		Class c = null;
		String str = dollarSeparatedName;
		for (;;)
		{
			//System.out.println("str=" + str);
			try
			{
				c = Class.forName(str);
				return c;
			}
			catch (ClassNotFoundException ex) {}
			
			int pos = str.indexOf('$');
			if (pos < 0) throw new ClassNotFoundException(dotSeparatedName);
			
			str = str.substring(0, pos) + "." + str.substring(pos+1);
		}
	}
	
	
	
	/** ************************************************************************
	 * Translate a dot-separated class name to its actual internal name, which is
	 * dollar-separated (except for the dot after the package name).
	 *
	 
	public static String translateToInternal(String dotSeparatedName)
	throws
		InvalidClassName
	{
		if (dotSeparatedName == null) throw new InvalidClassName(
			"Class name is null");
		
		if (dotSeparatedName.length() == 0) throw new InvalidClassName(
			"Class name is an empty string");
		
		String[] parts = dotSeparatedName.split("\\.");
		
		String dollarSeparatedName = parts[0];
		if (parts.length > 1) dollarSeparatedName += ("." + parts[1]);
		
		for (int i = 2; i < parts.length; i++)
		{
			dollarSeparatedName += ("$" + parts[i]);
		}
		
		return dollarSeparatedName;
	}*/
	
	
	/** ************************************************************************
	 * Translate a dollar-separated class name into dot-separated representation.
	 *
	 
	public static String translateFromInternal(String dollarSeparatedName)
	throws
		InvalidClassName
	{
		if (dollarSeparatedName == null) throw new InvalidClassName(
			"Class name is null");
		
		if (dollarSeparatedName.length() == 0) throw new InvalidClassName(
			"Class name is an empty string");
		
		String[] parts = dollarSeparatedName.split("\\$");
		
		String dotSeparatedName = parts[0];
		
		for (int i = 1; i < parts.length; i++)
		{
			dotSeparatedName += ("." + parts[i]);
		}
		
		return dotSeparatedName;
	}
	
	
	public static class InvalidClassName extends Exception
	{
		final static String stdMsg = "Invalid class name.";
		
		public InvalidClassName() { super(stdMsg); }
		public InvalidClassName(Throwable t) { super(stdMsg, t); }
		public InvalidClassName(String msg, Throwable t) { super(stdMsg + "; " + msg, t); }
		public InvalidClassName(String msg) { super(stdMsg + "; " + msg); }
	}*/
}

