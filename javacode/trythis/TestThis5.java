package trythis;


import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Insets;
import java.awt.Color;


public class TestThis5
{
	public static void main(String[] args)
	{
		JFrame frame = new JFrame();
		frame.setSize(200, 200);
		frame.setLayout(null);
		frame.setLocation(100, 100);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.blue);
		
		panel.setSize(100, 100);
		
		frame.add(panel);
		panel.setLocation(20, 20);
		
		Insets pinsets = panel.getInsets();
		System.out.println("Panel insets top,bottom,right,left=" +
			pinsets.top + "," + pinsets.bottom + "," + pinsets.right + "," + pinsets.left);
		
		frame.show();
	}
}

