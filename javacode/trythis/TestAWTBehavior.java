package trythis;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.Border;
import expressway.awt.AWTTools;


/**
 * Test the ModelEngine Pojo implementation.
 *
 * Instantiate a ModelEngine, and create a set of models.
 * Then exercise those models.
 
	Q: Does a transparent Component receive events? 
	A: Yes.
	
	Q: How can one determine the Component that is immediately underneath a 
		given Point within a Component? (so that one can delegate AWT event 
		response to the Component underneath)
	A: Write a complex algorithm.
		
	Q: Is the location (0, 0) within a Component at the top left of the Component's 
		border, or inside the border?
	A: At the top left.
		
	Q: If I call translate() on the Graphics of the Component, does it affect 
		the Border, or only the draw operations that I do?
	A: translate() seems to have no affect.
 
 */

public class TestAWTBehavior
{
	public static void main(String[] args)
	{
		// Create Window Frame.
		
		(new MainFrame()).setSize(200, 200);
	}
}


class MainFrame extends JFrame
{
	public MainFrame()
	{
		setLocation(0, 0);
		setSize(200, 200);
		getContentPane().setLayout(null);
		
		// Create Green.
		JPanel panelG = new PanelG();
		panelG.setName("Green");
		panelG.setSize(100, 100);
		panelG.setLocation(0, 0);
		panelG.setBackground(new Color(0, 255, 0));  // green, opaque.
		panelG.setLayout(null);
		getContentPane().add(panelG);
		
		// Create Blue (translucent).
		JPanel panelB = new JPanel();
		panelB.setName("Blue");
		panelB.setSize(100, 100);
		panelB.setLocation(50, 50);
		panelB.setBackground(new Color(0, 0, 255, 50));  // blue, transparent.
		//panelB.setOpaque(true);
		panelB.setLayout(null);
		getContentPane().add(panelB);
		
		// Move Blue to the front.
		getContentPane().setComponentZOrder(panelB, 0);  // bring panel B to the front.
		
		// Create Green-Sub.
		JPanel  panelGreenSub = new JPanel();
		panelGreenSub.setName("GreenSub");
		panelGreenSub.setSize(500, 500);
		panelGreenSub.setLocation(80, 80);
		panelGreenSub.setBackground(new Color(0, 128, 0));  // dark green.
		panelGreenSub.setLayout(null);
		panelG.add(panelGreenSub);

		VisualDragListener mouseListener = new VisualDragListener();
		//this.mouseInputListener = mouseListener;
		//this.mouseMotionListener = mouseListener;
		this.addMouseListener(mouseListener);
		this.addMouseMotionListener(mouseListener);

		panelG.addMouseListener(mouseListener);
		panelG.addMouseMotionListener(mouseListener);

		panelB.addMouseListener(mouseListener);
		panelB.addMouseMotionListener(mouseListener);
		
		panelGreenSub.addMouseListener(mouseListener);
		panelGreenSub.addMouseMotionListener(mouseListener);
		
		pack();
		validate();
		setVisible(true);

		
		//panelA.validate();
		//panelA.show();
		//panelA.repaint();
		
	}
	
	class PanelG extends JPanel
	{
		public PanelG()
		{
			Border outerNormalBorder = BorderFactory.createLineBorder(
				new Color(100, 200, 100),
				2);
				
			Border normalBorder = 
				BorderFactory.createTitledBorder(outerNormalBorder, "Opaque");
				
			setBorder(normalBorder);
		}
		
		
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
		
			getGraphics().setColor(new Color(0, 0, 0));
			getGraphics().setPaintMode();
			//getGraphics().translate(10, 10);
			getGraphics().drawLine(0, 0, 200, 50);
			//getGraphics().fillRect(0, -100, 200, 200);
		}
	}
	

	class VisualDragListener implements MouseListener, MouseMotionListener
	{
		public void mousePressed(MouseEvent e)
		{
			if (e.getButton() != MouseEvent.BUTTON1) return;
			
			Component component = e.getComponent();
			
			System.out.println("Mouse button 1 press event on " + 
				component.getName() + " at " + e.getPoint());
			
			if (component.getName().equals("Blue"))
				if (AWTTools.forwardEventToDeepestContainingComponent(e))
					System.out.println("Event forwarded");
				else
					System.out.println("Event not forwarded - and not handled.");
		}
		
		
		public void mouseDragged(MouseEvent e)
		{
			if (e.getButton() != MouseEvent.BUTTON1) return;
			
			Component source = e.getComponent();

			//System.out.println("Mouse button 1 drag event " + source.getName());
			
		}
		
		
		public void mouseReleased(MouseEvent e)
		{
			if (e.getButton() != MouseEvent.BUTTON1) return;
			
			Component source = e.getComponent();
			
			//System.out.println("Mouse button 1 release event " + source.getName());
		}
		
		
		public void mouseEntered(MouseEvent e) {}

		
		public void mouseExited(MouseEvent e) {}

		
		public void mouseClicked(MouseEvent e) {}

		
		public void mouseMoved(MouseEvent e) {}
	}
}

