package trythis;

import expressway.gui.ViewPanelBase;
import java.util.Set;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.awt.*;
import expressway.gui.modeldomain.*;


public class TestThis3
{
	public static void main(String[] args)
	{
		MyPanel panel = new MyPanel();
		System.out.println(panel.getName());
	}
}


class MyPanel extends ModelScenarioViewPanel
{
	MyPanel()
	{
		super(JTabbedPane container, 
		ModelDomainSer modelDomain, ViewFactory viewFactory, ModelEngineRemote modelEngine);
	}
	
	
	public ModelEngineRemote getModelEngine() { return null; }
	
	
	public Class getVisualClass(NodeSer node) { return null; }
	
	
	public VisualComponent makeVisual(NodeSer node, Container container,
		boolean asIcon)
	throws
		ParameterError,
		Exception { return null; }
	
	
	public Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError { return null; }
	
	
	public Set<VisualComponent> identifyVisualComponents()
	throws
		ParameterError
	{
		return new HashSet<VisualComponent>();
	}
	
	
	public Set<VisualComponent> addNode(String nodeId)
	throws
		Exception { return null; }
		
		
	public void removeVisual(VisualComponent visual)
	throws
		Exception {}

		
	public Set<VisualComponent>  refreshVisuals(String nodeId)
	throws
		Exception { return null; }
	

	public void select(VisualComponent visual, boolean yes) {}
	

	public Set<VisualComponent> getSelected() { return null; }
	
	
	public void selectAll(boolean yes) {}


	public void domainCreated(String domainId) {}
}

