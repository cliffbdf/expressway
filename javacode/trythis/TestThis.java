package trythis;

public class TestThis
{
	public static void main(String[] args)
	{
		TypeA typeA = new TypeB()
		{
			public void notifyme()
			{
				System.out.println("typeA, of actual type TypeB");
			}
		};
		
	
		Type1 type1 = new Type1()
		{
			public void notifyme(TypeA a)
			{
				System.out.println("type1, of actual type Type1");
				a.notifyme();
			}
		};
		
		
		Type2 type2 = new Type2()
		{
			public void notifyme(TypeB b)
			{
				System.out.println("type2, of actual type Type2");
				b.notifyme();
			}
			
			public void notifyme(TypeA a) { notifyme((TypeB)a); }
		};
		
		
		type1.notifyme(typeA);
	}
	
	

	public interface TypeA
	{
		public void notifyme();
	}
	
	
	public interface TypeB extends TypeA
	{
		public void notifyme();
	}
	
	
	public interface Type1
	{
		public void notifyme(TypeA a);
	}
	
	
	public interface Type2 extends Type1
	{
		public void notifyme(TypeB b);
	}

}

