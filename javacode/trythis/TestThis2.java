package trythis;

import java.util.Collection;
import java.util.Vector;


public class TestThis2
{
	public static void main(String[] args)
	{
		Collection<TypeB> c = new Vector<TypeB>();
		
		TestThis2 t = new TestThis2();
		
		t.doStuff(c);
		//t.doStuff1(c);
		t.doStuff2(c);
		//t.doStuff3(c);
	}
	
	
	void doStuff(Collection c)
	{
		System.out.println("doStuff");
	}
	

	void doStuff1(Collection<TypeA> c)
	{
		System.out.println("doStuff");
	}
	
	

	void doStuff2(Collection<TypeB> c)
	{
		System.out.println("doStuff");
	}
	
	
	void doStuff3(Collection<TypeC> c)
	{
		System.out.println("doStuff");
	}
	

	public interface TypeA
	{
		public void notifyme();
	}
	
	
	public interface TypeB extends TypeA
	{
		public void notifyme();
	}
	
	
	public interface TypeC extends TypeB
	{
		public void notifyme();
	}
}

