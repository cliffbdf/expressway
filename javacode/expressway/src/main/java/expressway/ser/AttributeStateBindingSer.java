package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;


public class AttributeStateBindingSer extends ModelElementSer
{
	public String attributeNodeId;
	public String stateNodeId;
	public String stateDomainId;
	public String stateFullName;

	public String getNodeKind() { return NodeKindNames.AttributeStateBinding; }
}

