package expressway.ser;

import expressway.common.*;



public class VariableSer extends ParameterSer
{
	public String[] varAttrBindingNodeIds;
	public String[] decisionNodeIds;
	
	public String getNodeKind() { return NodeKindNames.Variable; }
	
	public Object deepCopy()
	{
		VariableSer copy = (VariableSer)(super.deepCopy());
		
		copy.varAttrBindingNodeIds = new String[this.varAttrBindingNodeIds.length];
		for (int i = 0; i < copy.varAttrBindingNodeIds.length; i++)
			copy.varAttrBindingNodeIds[i] = this.varAttrBindingNodeIds[i];
		
		copy.decisionNodeIds = new String[this.decisionNodeIds.length];
		for (int i = 0; i < copy.decisionNodeIds.length; i++)
			copy.decisionNodeIds[i] = this.decisionNodeIds[i];
		
		return copy;
	}
}

