package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.util.List;
import java.util.Vector;
import java.util.Date;


public class SimulationRunSer extends ModelElementSer
{
	public long[][] iterEventIds;
	public List<Epoch> epochs;
	public List<Exception> simErrors;
	public TableEntry[] finalStateValuesByNodeId;
	public List<Integer> iterations;
	public String modelScenarioNodeId;
	//public String modelDomainNodeId;
	public Date startingDate;  // starting time specified. May be null.
	public Date finalDate;
	public int iterationLimit;
	public long duration;
	public Date actualStartTime;  // actual initial simulated time. Never null.
	public long actualElapsedTime;
	public long actualEndTime;
	public long randomSeed;
	public boolean completedNorm;
	public int noOfGeneratedEvents;
	//public String[] allEventNodeIds;
	public String simRunSetNodeId;
	
	public String getNodeKind() { return NodeKindNames.SimulationRun; }
	
	public Object deepCopy()
	{
		SimulationRunSer copy = (SimulationRunSer)(super.deepCopy());
		
		copy.iterEventIds = new long[this.iterEventIds.length][];
		for (int i = 0; i < copy.iterEventIds.length; i++)
		{
			long[] ids = new long[iterEventIds[i].length];
			copy.iterEventIds[i] = ids;
			for (int j = 0; j < ids.length; j++)
				ids[j] = this.iterEventIds[i][j];
		}
		
		copy.epochs = new Vector<Epoch>();
		for (Epoch e : this.epochs)
			copy.epochs.add((Epoch)e.deepCopy());
		
		copy.simErrors = new Vector<Exception>();
		for (Exception e : this.simErrors)
			copy.simErrors.add(e);  // treating Exception as if it were immutable.
		
		TableEntry[] finalStateValuesByNodeId;
		copy.finalStateValuesByNodeId = new TableEntry[this.finalStateValuesByNodeId.length];
		for (int i = 0; i < this.finalStateValuesByNodeId.length; i++)
			copy.finalStateValuesByNodeId[i] = 
				(TableEntry)this.finalStateValuesByNodeId[i].deepCopy();
		
		copy.iterations = new Vector<Integer>();
		for (Integer iter : this.iterations)
			copy.iterations.add(iter);
		
		//copy.genEventNodeIds = new String[this.genEventNodeIds.length];
		//for (int i = 0; i < copy.genEventNodeIds.length; i++)
		//	copy.genEventNodeIds[i] = this.genEventNodeIds[i];
		
		//copy.allEventNodeIds = new String[this.allEventNodeIds.length];
		//for (int i = 0; i < copy.allEventNodeIds.length; i++)
		//	copy.allEventNodeIds[i] = this.allEventNodeIds[i];
		
		return copy;
	}
	
	
	public String toString()  // displayed in the DomainListPanel
	{
		return this.getName() + " " +
			((epochs.size() == 0) ? "Not run" :
			("(" + epochs.size() + " epochs), " + (completedNorm ? "OK" : "Error")));
	}
}

