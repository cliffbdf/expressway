/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.ser;

import expressway.common.*;


/**
 * See the specification in CrossRefableSer.
 */
 
public abstract class CrossRefableSerBase extends NodeSerBase implements CrossRefableSer
{
	public String[][] crossRefs;
	
	public String[][] getCrossRefs() { return crossRefs; }
}

