package expressway.ser;

import expressway.common.*;



public abstract class AbstractSwitchSer extends ActivitySer
{
	public String inputPortNodeId;
	public String outputPortNodeId;
	public String controlPortNodeId;
	public String stateNodeId;
	public String startConductingAttrNodeId;
	
	public boolean defaultStartConducting;
}

