/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.ser;

import expressway.common.*;


/**
 * See the specification in CrossRefableSer.
 */
 
public abstract class TemplatizableNodeSer extends CrossRefableSerBase
	implements TemplateInstanceSer, MenuOwnerSer
{
	public String templateNodeId;
	public String templateFullName;
	public String[] menuItemTextAr;
	public String[] menuItemDescAr;
	
	
	/**
	 * Contains one entry for each menuItemTextAr element. If one of those elements
	 * is a MenuTree, then the corresponding entry in javaMethodNames is null.
	 */
	 
	public String[] javaMethodNames;
	
	
	/**
	 * Contains one entry for each menuItemTextAr element. If one of those elements
	 * is an Action, then the corresponding entry in javaMethodNames is null.
	 */
	 
	public String[] menuTreeNodeIds;
	
	public String[] menuItemNodeIds;

	
	
	
	/*
	 * From TemplateInstanceSer.
	 */
	 
	public String getTemplateNodeId()  { return templateNodeId; }
	public String getTemplateFullName() { return templateFullName; }
	
	public void setTemplateNodeId(String id) { this.templateNodeId = id; }
	public void setTemplateFullName(String s) { this.templateFullName = s; }


	/*
	 * From MenuOwnerSer.
	 */
	 
	public void setMenuItemNodeIds(String[] ids) { this.menuItemNodeIds = ids; }
	public void setMenuItemTextAr(String[] textAr) { this.menuItemTextAr = textAr; }
	public void setMenuItemDescAr(String[] descAr) { this.menuItemDescAr = descAr; }
	public void setJavaMethodNames(String[] names) { this.javaMethodNames = names; }
	public void setMenuTreeNodeIds(String[] ids) { this.menuTreeNodeIds = ids; }
	
	public String[] getMenuItemNodeIds() { return menuItemNodeIds; }
	public String[] getMenuItemTextAr() { return menuItemTextAr; }
	public String[] getMenuItemDescAr() { return menuItemDescAr; }
	public String[] getJavaMethodNames() { return javaMethodNames; }
	public String[] getMenuTreeNodeIds() { return menuTreeNodeIds; }
}

