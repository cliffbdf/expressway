package expressway.ser;


import expressway.common.*;


public class HierarchyDomainMotifDefSer extends HierarchyDomainImplSer
	implements MotifDefSer, MenuOwnerSer
{
	public String[] motifClassNames;
	public String getNodeKind() { return NodeKindNames.HierarchyDomainMotifDef; }

	/* From MotifDefSer. */
	
	public String[] getMotifClassNames() { return motifClassNames; }
}

