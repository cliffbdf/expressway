package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;



public class PortSer extends ModelComponentSer
{
	public double xInIcon;
	public double yInIcon;
	public boolean isBlack;
	public PortDirectionType direction;
	public Position currentSide;
	public String[] sourceConnectionNodeIds;  // Conduit Node Ids.
	public String[] destinationConnectionNodeIds;  // Conduit Node Ids.
	
	public String getNodeKind() { return NodeKindNames.Port; }
	
	public Object deepCopy()
	{
		PortSer copy = (PortSer)(super.deepCopy());
		
		copy.sourceConnectionNodeIds = new String[this.sourceConnectionNodeIds.length];
		for (int i = 0; i < copy.sourceConnectionNodeIds.length; i++)
			copy.sourceConnectionNodeIds[i] = this.sourceConnectionNodeIds[i];
		
		copy.destinationConnectionNodeIds = new String[this.destinationConnectionNodeIds.length];
		for (int i = 0; i < copy.destinationConnectionNodeIds.length; i++)
			copy.destinationConnectionNodeIds[i] = this.destinationConnectionNodeIds[i];
		
		return copy;
	}	
}

