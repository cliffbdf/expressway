package expressway.ser;

import expressway.common.*;



public class DoubleExpressionSer extends ActivitySer
{
	public String[] inputPortNodeIds;
	public String outputPortNodeId;
	public String stateNodeId;
	public String exprAttrNodeId;
	public String defaultExpr;
	
	public String getNodeKind() { return NodeKindNames.DoubleExpression; }
	
	public Object deepCopy()
	{
		DoubleExpressionSer copy = (DoubleExpressionSer)(super.deepCopy());
		
		copy.inputPortNodeIds = new String[this.inputPortNodeIds.length];
		for (int i = 0; i < copy.inputPortNodeIds.length; i++)
			copy.inputPortNodeIds[i] = this.inputPortNodeIds[i];
		
		return copy;
	}
}

