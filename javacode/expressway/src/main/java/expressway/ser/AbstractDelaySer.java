package expressway.ser;

import expressway.common.*;



public abstract class AbstractDelaySer extends ActivitySer
{
	public String inputPortNodeId;
	public String outputPortNodeId;
	public String stateNodeId;
	public long defaultDelay;
	public String delayAttrNodeId;
}

