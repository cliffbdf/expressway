package expressway.ser;

import expressway.common.*;



public class DecisionPointSer extends DecisionElementSer
{
	public String[] parameterNodeIds = null;
	public String[] precursorDecisionPointNodeIds = null;
	public String[] subordinateDecisionPointNodeIds = null;
	public String[] decisionNodeIds = null;
	
	public String getNodeKind() { return NodeKindNames.DecisionPoint; }


	public Object deepCopy()
	{
		DecisionPointSer copy = (DecisionPointSer)(super.deepCopy());
		
		copy.parameterNodeIds = new String[this.parameterNodeIds.length];
		for (int i = 0; i < copy.parameterNodeIds.length; i++)
			copy.parameterNodeIds[i] = this.parameterNodeIds[i];
		
		copy.precursorDecisionPointNodeIds = new String[this.precursorDecisionPointNodeIds.length];
		for (int i = 0; i < copy.precursorDecisionPointNodeIds.length; i++)
			copy.precursorDecisionPointNodeIds[i] = this.precursorDecisionPointNodeIds[i];
		
		copy.subordinateDecisionPointNodeIds = new String[this.subordinateDecisionPointNodeIds.length];
		for (int i = 0; i < copy.subordinateDecisionPointNodeIds.length; i++)
			copy.subordinateDecisionPointNodeIds[i] = this.subordinateDecisionPointNodeIds[i];
		
		copy.decisionNodeIds = new String[this.decisionNodeIds.length];
		for (int i = 0; i < copy.decisionNodeIds.length; i++)
			copy.decisionNodeIds[i] = this.decisionNodeIds[i];
		
		return copy;
	}
}

