package expressway.ser;


import expressway.common.*;
import java.net.URL;


public class HierarchyDomainImplSer extends HierarchyImplSer
	implements HierarchyDomainSer, NodeDomainSer
{
	public String sourceName;
	public URL sourceURL;
	public String declaredName;
	public String declaredVersion;
	public String[] namedReferenceNames;
	public String currentScenarioNodeId;
	public String[] scenarioSetNodeIds;
	public String[] scenarioIds;
	public String[] motifDefNodeIds;

	public String[] menuItemNodeIds;
	public String[] menuItemTextAr;
	public String[] menuItemDescAr;
	public String[] javaMethodNames;
	public String[] menuTreeNodeIds;

	public String getNodeKind() { return NodeKindNames.HierarchyDomain; }
	
	public String getSourceName() { return sourceName; }
	public URL getSourceURL() { return sourceURL; }
	public String getDeclaredName() { return declaredName; }
	public String getDeclaredVersion() { return declaredVersion; }
	
	public void setSourceName(String sourceName) { this.sourceName = sourceName; }
	public void setSourceURL(URL sourceURL) { this.sourceURL = sourceURL; }
	public void setDeclaredName(String declaredName) { this.declaredName = declaredName; }
	public void setDeclaredVersion(String declaredVersion) { this.declaredVersion = declaredVersion; }

	
	public void setNamedReferenceNames(String[] names) { this.namedReferenceNames = names; }
	public String[] getNamedReferenceNames() { return namedReferenceNames; }
	

	public void setCurrentScenarioNodeId(String id) { this.currentScenarioNodeId = id; }
	public String getCurrentScenarioNodeId() { return currentScenarioNodeId; }
	

	public void setScenarioSetNodeIds(String[] ids) { this.scenarioSetNodeIds = ids; }
	public String[] getScenarioSetNodeIds() { return scenarioSetNodeIds; }
	

	public void setScenarioIds(String[] ids) { this.scenarioIds = ids; }
	public String[] getScenarioIds() { return scenarioIds; }
	

	public void setMotifDefNodeIds(String[] ids) { this.motifDefNodeIds = ids; }
	public String[] getMotifDefNodeIds() { return motifDefNodeIds; }
	
	
	public void setMenuItemNodeIds(String[] ids) { menuItemNodeIds = ids; }
	public String[] getMenuItemNodeIds() { return menuItemNodeIds; }
	
	
	public void setMenuItemTextAr(String[] textAr) { menuItemTextAr = textAr; }
	public String[] getMenuItemTextAr() { return menuItemTextAr; }

	
	public void setMenuItemDescAr(String[] descAr) { menuItemDescAr = descAr; }
	public String[] getMenuItemDescAr() { return menuItemDescAr; }
	
	
	public void setJavaMethodNames(String[] names) { javaMethodNames = names; }
	public String[] getJavaMethodNames() { return javaMethodNames; }
	
	
	public void setMenuTreeNodeIds(String[] ids) { menuTreeNodeIds = ids; }
	public String[] getMenuTreeNodeIds() { return menuTreeNodeIds; }
}

