package expressway.ser;

import expressway.common.*;
import java.io.Serializable;
import java.util.Date;


public class SimRunSetSer extends ModelElementSer
{
	public String[] simRunIds;
	public String scenarioId;
	public Date startingDate;  // starting time specified. May be null.
	public Date finalDate;
	public int iterationLimit;
	public long duration;
	public Double computedNetValueMean;
	public Double computedNetValueVariance;
	public Double computedNetValueAssurance;
	
	public String getNodeKind() { return NodeKindNames.SimRunSet; }
	
	public Object deepCopy()
	{
		SimRunSetSer copy = (SimRunSetSer)(super.deepCopy());
		
		copy.simRunIds = new String[this.simRunIds.length];
		for (int i = 0; i < copy.simRunIds.length; i++)
			copy.simRunIds[i] = this.simRunIds[i];
		
		return copy;
	}
}

