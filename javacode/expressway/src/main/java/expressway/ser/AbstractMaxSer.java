package expressway.ser;

import expressway.common.*;



public abstract class AbstractMaxSer extends ActivitySer
{
	public String[] inputPortNodeIds;
	public String outputPortNodeId;
	public String stateNodeId;
	public String initValueAttrNodeId;
	
	
	public Object deepCopy()
	{
		AbstractMaxSer copy = (AbstractMaxSer)(super.deepCopy());
		
		copy.inputPortNodeIds = new String[this.inputPortNodeIds.length];
		for (int i = 0; i < copy.inputPortNodeIds.length; i++)
			copy.inputPortNodeIds[i] = this.inputPortNodeIds[i];
		
		return copy;
	}
}

