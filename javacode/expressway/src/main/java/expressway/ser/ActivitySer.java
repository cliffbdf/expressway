package expressway.ser;

import expressway.common.*;
import java.util.Arrays;


public class ActivitySer extends PortedContainerSer implements MenuOwnerSer,
	TemplateInstanceSer
{
	public String getNodeKind() { return NodeKindNames.Activity; }
}

