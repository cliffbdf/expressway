/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.ser;

import expressway.common.Display;
import java.util.Date;
import java.util.Set;


public interface NodeSer extends Comparable<NodeSer>
{
	String getNodeKind();
	String getName();
	String getHtmlDescription();
	String getFullName();
	void setName(String name);
	void setFullName(String fullName);
	String getDomainName();
	void setDomainName(String n);
	String getDomainId();
	void setViewClassName(String name);
	String getViewClassName();
	String[] getAvailableHTTPFormats();
	double getWidth();
	double getHeight();
	double getIconWidth();
	double getIconHeight();
	void setIconWidth(double w);
	void setIconHeight(double h);
	boolean useBorderWhenIconified();
	String getIconImageHandle();
	void setWidth(double w);
	void setHeight(double h);
	boolean isVisible();
	boolean isPredefined();
	boolean isMovable();
	boolean isResizable();
	String getNodeId();
	String getParentNodeId(); // may be null.
	Display getDisplay();
	double getX();
	double getY();
	void setX(double x);
	void setY(double y);
	Date getDeletionDate();
	boolean getWatchEnable();
	String getPriorVersionNodeId();
	long getLastUpdated();
	void setLastUpdated(long lastUpdated);
	int getVersionNumber();
	void setVersionNumber(int vn);
	
	/** If the Node is a Hierarchy Node, the children should be listed in order
		of ascending position. */
	String[] getChildNodeIds();
	
	NodeSer shallowCopy();
	String[] getAttributeNodeIds();
	boolean ownedByMotifDef();
	boolean ownedByActualTemplateInstance();
}

