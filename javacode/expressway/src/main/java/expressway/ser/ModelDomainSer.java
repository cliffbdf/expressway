package expressway.ser;

import expressway.common.*;


import java.util.List;
import java.util.Vector;
import java.net.URL;


public class ModelDomainSer extends ModelContainerSer implements NodeDomainSer, MenuOwnerSer
{
	public String sourceName;
	public URL sourceURL;
	public String declaredName;
	public String declaredVersion;
	public String[] namedReferenceNames;

	public String[] modelScenarioSetNodeIds;
	public String currentModelScenarioNodeId;
	public String[] scenarioIds;
	public String[] attributeStateBindingNodeIds;
	public String[] varAttrBindingNodeIds;
	public String[] compsWithNativeImplNodeIds;
	public String[] motifDefNodeIds;
	public Boolean externalStateBindingType;
	public ModelAPITypes.TimePeriodType externalStateGranularity;

	public String getNodeKind() { return NodeKindNames.ModelDomain; }
	public String getSourceName() { return sourceName; }
	public URL getSourceURL() { return sourceURL; }
	public String getDeclaredName() { return declaredName; }
	public String getDeclaredVersion() { return declaredVersion; }

	
	public void setSourceName(String sourceName) { this.sourceName = sourceName; }
	public void setSourceURL(URL sourceURL) { this.sourceURL = sourceURL; }
	public void setDeclaredName(String declaredName) { this.declaredName = declaredName; }
	public void setDeclaredVersion(String declaredVersion) { this.declaredVersion = declaredVersion; }
	
	
	public void setNamedReferenceNames(String[] names) { this.namedReferenceNames = names; }
	public String[] getNamedReferenceNames() { return namedReferenceNames; }
	
	public void setScenarioSetNodeIds(String[] ids) { modelScenarioSetNodeIds = ids; }
	public String[] getScenarioSetNodeIds() { return modelScenarioSetNodeIds; }
	
	public void setCurrentScenarioNodeId(String id) { this.currentModelScenarioNodeId = id; }
	public String getCurrentScenarioNodeId() { return currentModelScenarioNodeId; }
	
	public void setScenarioIds(String[] ids) { this.scenarioIds = ids; }
	public String[] getScenarioIds() { return scenarioIds; }
	
	public void setMotifDefNodeIds(String[] ids) { this.motifDefNodeIds = ids; }
	public String[] getMotifDefNodeIds() { return motifDefNodeIds; }

	
	public Object deepCopy()
	{
		ModelDomainSer copy = (ModelDomainSer)(super.deepCopy());
		
		copy.modelScenarioSetNodeIds = new String[this.modelScenarioSetNodeIds.length];
		for (int i = 0; i < copy.modelScenarioSetNodeIds.length; i++)
			copy.modelScenarioSetNodeIds[i] = this.modelScenarioSetNodeIds[i];
		
		copy.scenarioIds = new String[this.scenarioIds.length];
		for (int i = 0; i < copy.scenarioIds.length; i++)
			copy.scenarioIds[i] = this.scenarioIds[i];
		
		copy.attributeStateBindingNodeIds = new String[this.attributeStateBindingNodeIds.length];
		for (int i = 0; i < copy.attributeStateBindingNodeIds.length; i++)
			copy.attributeStateBindingNodeIds[i] = this.attributeStateBindingNodeIds[i];
		
		copy.varAttrBindingNodeIds = new String[this.varAttrBindingNodeIds.length];
		for (int i = 0; i < copy.varAttrBindingNodeIds.length; i++)
			copy.varAttrBindingNodeIds[i] = this.varAttrBindingNodeIds[i];
		
		copy.compsWithNativeImplNodeIds = new String[this.compsWithNativeImplNodeIds.length];
		for (int i = 0; i < copy.compsWithNativeImplNodeIds.length; i++)
			copy.compsWithNativeImplNodeIds[i] = this.compsWithNativeImplNodeIds[i];
		
		copy.motifDefNodeIds = new String[this.motifDefNodeIds.length];
		for (int i = 0; i < copy.motifDefNodeIds.length; i++)
			copy.motifDefNodeIds[i] = this.motifDefNodeIds[i];
		
		return copy;
	}



	/* See MenuOwnerBaseSer */
	
	public String[] menuItemNodeIds;
	public String[] menuItemTextAr;
	public String[] menuItemDescAr;
	
	
	/**
	 * Contains one entry for each menuItemTextAr element. If one of those elements
	 * is a MenuTree, then the corresponding entry in javaMethodNames is null.
	 */
	 
	public String[] javaMethodNames;
	
	
	/**
	 * Contains one entry for each menuItemTextAr element. If one of those elements
	 * is an Action, then the corresponding entry in javaMethodNames is null.
	 */
	 
	public String[] menuTreeNodeIds;


	public void setMenuItemNodeIds(String[] ids) { this.menuItemNodeIds = ids; }
	public void setMenuItemTextAr(String[] textAr) { this.menuItemTextAr = textAr; }
	public void setMenuItemDescAr(String[] descAr) { this.menuItemDescAr = descAr; }
	public void setJavaMethodNames(String[] names) { this.javaMethodNames = names; }
	public void setMenuTreeNodeIds(String[] ids) { this.menuTreeNodeIds = ids; }
	
	public String[] getMenuItemNodeIds() { return menuItemNodeIds; }
	public String[] getMenuItemTextAr() { return menuItemTextAr; }
	public String[] getMenuItemDescAr() { return menuItemDescAr; }
	public String[] getJavaMethodNames() { return javaMethodNames; }
	public String[] getMenuTreeNodeIds() { return menuTreeNodeIds; }
	

	/**
	 * Add the specified Sceanrio Node Id to this Model Domain Ser's array of
	 * Model Scenario Node Ids.  If it is already in the list, do not add it
	 * redundantly.
	 */
	 
	public void addScenario(String scenarioNodeId)
	{
		String[] nodeIds = this.scenarioIds;
		
		List<String> scenarioList = new Vector<String>(nodeIds.length+1);
		boolean found = false;
		for (String s : nodeIds)
		{
			if (s.equals(scenarioNodeId)) found = true;
			scenarioList.add(s);
		}
		
		if (! found) scenarioList.add(scenarioNodeId);
		
		String[] newIds = scenarioList.toArray(new String[scenarioList.size()]);
		
		this.scenarioIds = newIds;
	}
	
	
	/**
	 * Remove the specified Sceanrio Node Id from this Model Domain Ser's array
	 * of Model Scenario Node Ids. If it is not in the list, do not throw
	 * an Exception.
	 */
	 
	public void removeScenario(String scenarioNodeId)
	{
		String[] nodeIds = this.scenarioIds;

		List<String> scenarioList = new Vector<String>(nodeIds.length-1);
		for (String s : nodeIds)
		{
			if (s.equals(scenarioNodeId)) continue;
			scenarioList.add(s);
		}

		String[] newIds = scenarioList.toArray(new String[scenarioList.size()]);
		
		this.scenarioIds = newIds;
	}
	
	
	public String toString()  // displayed in the DomainListPanel
	{
		return this.getName();
	}
}

