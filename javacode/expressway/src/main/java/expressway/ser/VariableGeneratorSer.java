package expressway.ser;

import expressway.common.*;



public class VariableGeneratorSer extends GeneratorSer
{
	public String theta1PortNodeId;
	public String theta2PortNodeId;
	
	public String getNodeKind() { return NodeKindNames.VariableGenerator; }
}

