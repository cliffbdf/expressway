package expressway.ser;

import expressway.common.*;



public class ConstraintSer extends ModelComponentSer
{    
	public String getNodeKind() { return NodeKindNames.Constraint; }
	
	
	public Object deepCopy()
	{
		ConstraintSer copy = (ConstraintSer)(super.deepCopy());
		
		return copy;
	}	
}

