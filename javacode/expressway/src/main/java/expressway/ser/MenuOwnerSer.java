package expressway.ser;


import expressway.ser.NodeSer;


public interface MenuOwnerSer extends NodeSer
{
	void setMenuItemNodeIds(String[] ids);
	void setMenuItemTextAr(String[] textAr);
	void setMenuItemDescAr(String[] descAr);
	void setJavaMethodNames(String[] names);
	void setMenuTreeNodeIds(String[] ids);
	
	String[] getMenuItemNodeIds();
	String[] getMenuItemTextAr();
	String[] getMenuItemDescAr();
	String[] getJavaMethodNames();
	String[] getMenuTreeNodeIds();
}

