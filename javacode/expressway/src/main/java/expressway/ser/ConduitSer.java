package expressway.ser;

import expressway.common.*;
import expressway.geometry.Point;
import java.util.Arrays;

public class ConduitSer extends ModelComponentSer implements MenuOwnerSer
{
	public String p1NodeId;
	public String p2NodeId;
	
	/** In reference frame of the Conduit. If the Port's owner is the same as this
		Conduit's owner, the Port's owner will be full size (not iconified)
		and p1Location will be set accordingly; but if the Port's owner is internal
		to this Conduit's owner, then the Port's owner will be iconified and p1Location
		will be set accordingly. */
	public Point p1Location;

	/** Same comments as for p1Location but for p2Location. */
	public Point p2Location;
	
	public InflectionPoint[] inflectionPoints; // in ref frame of Conduit.

	
	public String getNodeKind() { return NodeKindNames.Conduit; }


	public String toString()
	{
		String s = super.toString();
		s += "; inflection points:";
		for (InflectionPoint ip : inflectionPoints) s += ip.toString();
		return s;
	}
}

