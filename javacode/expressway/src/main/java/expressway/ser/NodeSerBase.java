package expressway.ser;

import expressway.common.*;


import java.util.Date;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.net.URL;
import java.awt.Image;
import expressway.awt.AWTTools;


public abstract class NodeSerBase implements NodeSer, java.io.Serializable, Cloneable
{
	public String name;
	public String htmlDescription;  // may be null.
	public String fullName;
	public String domainName;
	public String domainId;
	public String viewClassName;
	public String[] availableHTTPFormats;
	public double width;
	public double height;
	public boolean iconBorder;
	public double iconWidth;
	public double iconHeight;
	public String iconImageHandle;
	public boolean visible;
	public boolean predefined;
	public boolean movable;
	public boolean resizable;
	private String nodeId;
	public String parentNodeId;
	public Display display;
	public Date deletionDate;
	public boolean watchEnable;
	public String priorVersionNodeId;
	public long lastUpdated;
	public int versionNumber;
	public String[] childNodeIds;
	public boolean isOwnedByMotifDef;
	public boolean isOwnedByActualTemplateInstance;
	public String[] attributeNodeIds;


	/**
	 * For making a "blank" NodeSer. This is intended to be used by clients that
	 * want to define a new Node and then send a NodeSer to the server to have
	 * the server create the Node defined by the NodeSer.
	 *
	 * Derived classes should nullify or create value copies of any values or
	 * references that represent ownership.
	 */
	 
	public NodeSer createAnonymousCopy()
	{
		NodeSerBase copy = (NodeSerBase)(this.shallowCopy());
		copy.name = null;
		copy.fullName = null;
		copy.domainName = null;
		copy.nodeId = null;
		copy.parentNodeId = null;
		copy.lastUpdated = 0L;
		copy.deletionDate = null;
		copy.priorVersionNodeId = null;
		
		return copy;
	}
	
	
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	public String getViewClassName() { return viewClassName; }
	public void setViewClassName(String name) { this.viewClassName = name; }
	public String getHtmlDescription() { return this.htmlDescription; }
	public void setHtmlDescription(String desc) { this.htmlDescription = desc; }
	public String getFullName() { return fullName; }
	public void setFullName(String fullName) { this.fullName = fullName; }
	public String getDomainName() { return domainName; }
	public void setDomainName(String n) { this.domainName = n; }
	public String getDomainId() { return domainId; }
	public String[] getAvailableHTTPFormats() { return availableHTTPFormats; }
	public double getWidth() { return width; }
	public double getHeight() { return height; }
	public void setWidth(double w) { this.width = w; }
	public void setHeight(double h) { this.height = h; }
	public boolean useBorderWhenIconified() { return iconBorder; }
	public String getIconImageHandle() { return this.iconImageHandle; }
	public double getIconWidth() { return iconWidth; }
	public double getIconHeight() { return iconHeight; }
	public void setIconWidth(double w) { this.iconWidth = w; }
	public void setIconHeight(double h) { this.iconHeight = h; }
	public boolean isVisible() { return visible; }
	public boolean isPredefined() { return predefined; }
	public boolean isMovable() { return movable; }
	public boolean isResizable() { return resizable; }
	public String getNodeId() { return nodeId; }
	public void setNodeId(String id)
	{
		if (id == null) throw new RuntimeException("Setting Node Id to null");
		this.nodeId = id;
	}
	
	public String getParentNodeId() { return parentNodeId; }
	public Display getDisplay() { return display; }
	public double getX() { return display.getX(); }
	public double getY() { return display.getY(); }
	public void setX(double x) { this.display.setX(x); }
	public void setY(double y) { this.display.setY(y); }
	public Date getDeletionDate() { return deletionDate; }
	public boolean getWatchEnable() { return watchEnable; }
	public String getPriorVersionNodeId() { return priorVersionNodeId; }
	public long getLastUpdated() { return lastUpdated; }
	public void setLastUpdated(long lastUpdated) { this.lastUpdated = lastUpdated; }
	public int getVersionNumber() { return versionNumber; }
	public void setVersionNumber(int vn) { this.versionNumber = vn; }
	public String[] getChildNodeIds() { return childNodeIds; }
	public boolean ownedByMotifDef() { return isOwnedByMotifDef; }
	public boolean ownedByActualTemplateInstance() { return isOwnedByActualTemplateInstance; }
	public String[] getAttributeNodeIds() { return attributeNodeIds; }
	
	public void anonymize()
	{
		this.name = null;
		this.nodeId = null;
		this.deletionDate = null;
		this.priorVersionNodeId = null;
		this.lastUpdated = 0L;
	}
	
	public int compareTo(NodeSer node)
	{
		/*
		 Important: See the slide "Ensuring Randomness and Repeatability".
		 */
		 
		String a = nodeId;
		String b = node.getNodeId();
		return a.compareTo(b);
	}
	
	
	/**
	 * Convenient for making a copy in which certain fields can be changed and 
	 * then the copy can be given to the updateNode(NodeSer) remote method.
	 */
	 
	public NodeSer shallowCopy()
	{
		try { return (NodeSer)(this.clone()); }
		catch (CloneNotSupportedException ex) { throw new RuntimeException(ex); }
	}
	
	
	public Object deepCopy()
	{
		NodeSerBase copy;
		
		try { copy = (NodeSerBase)(this.clone()); }
		catch (CloneNotSupportedException ex) { throw new RuntimeException(ex); }
		
		//copy.display = (Display)display.deepCopy();
		
		return copy;
	}
	
	
	public String toString()
	{
		return this.fullName;
	}
}

