package expressway.ser;

import expressway.common.*;
import java.io.Serializable;
import java.util.Date;


public class HierarchyScenarioImplSer extends HierarchyElementImplSer
	implements HierarchyScenarioSer
{
	public Date date;  // real time when Scenario was created.
	public String scenarioThatWasCopiedNodeId;
	public String scenarioSetNodeId;
	public String[] attributeNodeIds;
	public Serializable[] attributeValues;
	
	public String[] subHierarchyNodeIds;
	public String[] fieldNames;
	public int seqNo;
	
	
	public String getNodeKind() { return NodeKindNames.HierarchyScenario; }
	
	
	/* From HierarchySer */
	
	public void setSubHierarchyNodeIds(String[] ids) { this.subHierarchyNodeIds = ids; }
	
	public String[] getSubHierarchyNodeIds() { return subHierarchyNodeIds; }
	
	public void setFieldNames(String[] fieldNames) { this.fieldNames = fieldNames; }
	
	public String[] getFieldNames() { return fieldNames; }
	
	public void setSeqNo(int n) { this.seqNo = n; }
	
	public int getSeqNo() { return seqNo; }
	
	
	/* From ScenarioSer */
	
	public void setDate(Date d) { this.date = d; }
	public Date getDate() { return date; }
	
	public void setScenarioThatWasCopiedNodeId(String id) { this.scenarioThatWasCopiedNodeId = id; }
	public String getScenarioThatWasCopiedNodeId() { return scenarioThatWasCopiedNodeId; }
	
	public void setScenarioSetNodeId(String id) { this.scenarioSetNodeId = id; }
	public String getScenarioSetNodeId() { return scenarioSetNodeId; }
	
	public void setAttributeNodeIds(String[] ids) { this.attributeNodeIds = ids; }
	public String[] getAttributeNodeIds() { return attributeNodeIds; }
	
	public void setAttributeValues(Serializable[] values) { this.attributeValues = values; }
	public Serializable[] getAttributeValues() { return attributeValues; }
}

