package expressway.ser;

import expressway.common.*;



public class CompensatorSer extends ActivitySer
{
	public String inputPortNodeId;
	public String outputPortNodeId;
	public String stateNodeId;
	public long defaultValue;

	public String getNodeKind() { return NodeKindNames.Compensator; }
}

