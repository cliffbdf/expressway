package expressway.ser;


import expressway.common.*;


public class MenuItemSer extends MotifElementSer
{
	public String definedByNodeId;
	public String description;
	public String getNodeKind() { return NodeKindNames.MenuItem; }
}

