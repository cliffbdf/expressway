package expressway.ser;

import expressway.common.*;



public class DecisionFunctionSer extends ParameterSer
{    
	public String[] variableRefNodeIds;

	public String getNodeKind() { return NodeKindNames.DecisionFunction; }
	
	public Object deepCopy()
	{
		DecisionFunctionSer copy = (DecisionFunctionSer)(super.deepCopy());
		
		copy.variableRefNodeIds = new String[this.variableRefNodeIds.length];
		for (int i = 0; i < copy.variableRefNodeIds.length; i++)
			copy.variableRefNodeIds[i] = this.variableRefNodeIds[i];
		
		return copy;
	}
}

