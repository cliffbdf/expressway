package expressway.ser;

import expressway.common.*;
import java.io.Serializable;
import java.util.Date;


public class ModelScenarioSetSer extends ModelElementSer implements ScenarioSetSer, HierarchySer
{
	public String attributeId;
	public String[] scenarioIds;
	public String scenarioThatWasCopiedNodeId;
	public Date startingDate;  // starting time specified. May be null.
	public Date finalDate;
	public int iterationLimit;
	public long duration;
	
	public String[] subHierarchyNodeIds;
	public String[] fieldNames;
	public int seqNo;

	public String getNodeKind() { return NodeKindNames.ModelScenarioSet; }

	public void setScenarioIds(String[] ids) { scenarioIds = ids; }
	public String[] getScenarioIds() { return scenarioIds; }

	public void setScenarioThatWasCopiedNodeId(String id) { scenarioThatWasCopiedNodeId = id; }
	public String getScenarioThatWasCopiedNodeId() { return scenarioThatWasCopiedNodeId; }
	
	public void setVariableAttributeId(String id) { this.attributeId = id; }
	public String getVariableAttributeId() { return attributeId; }
	
	
	/* From HierarchySer */
	
	public void setSubHierarchyNodeIds(String[] ids) { this.subHierarchyNodeIds = ids; }
	
	public String[] getSubHierarchyNodeIds() { return subHierarchyNodeIds; }
	
	public void setFieldNames(String[] fieldNames) { this.fieldNames = fieldNames; }
	
	public String[] getFieldNames() { return fieldNames; }
	
	public void setSeqNo(int n) { this.seqNo = n; }
	
	public int getSeqNo() { return seqNo; }
	
	
	public Object deepCopy()
	{
		ModelScenarioSetSer copy = (ModelScenarioSetSer)(super.deepCopy());
		
		copy.scenarioIds = new String[this.scenarioIds.length];
		for (int i = 0; i < copy.scenarioIds.length; i++)
			copy.scenarioIds[i] = this.scenarioIds[i];
		
		return copy;
	}
}

