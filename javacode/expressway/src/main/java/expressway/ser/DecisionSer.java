package expressway.ser;

import expressway.common.*;



public class DecisionSer extends DecisionElementSer
{
	public String decisionPointNodeId;
	public String variableNodeId;
	public Object value;

	public String getNodeKind() { return NodeKindNames.Decision; }
	
	public Object deepCopy()
	{
		DecisionSer copy = (DecisionSer)(super.deepCopy());
		
		copy.value = null;
		
		return copy;
	}
}

