package expressway.ser;

import expressway.common.*;


import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Arrays;

public abstract class ModelContainerSer extends ModelComponentSer implements HierarchySer
{
	public String[] functionNodeIds;
	public String[] activityNodeIds;
	public String[] conduitNodeIds;
	public String[] constraintNodeIds;
	
	
	public String[] fieldNames;
	public String[] subHierarchyNodeIds;
	public int seqNo;


  /* From HierarchySer */

	public void setSubHierarchyNodeIds(String[] ids) { subHierarchyNodeIds = ids; }
	public String[] getSubHierarchyNodeIds() { return subHierarchyNodeIds; }
	public void setFieldNames(String[] fieldNames) { this.fieldNames = fieldNames; }
	public String[] getFieldNames() { return fieldNames; }
	public void setSeqNo(int n) { this.seqNo = n; }
	public int getSeqNo() { return seqNo; }
	
	
  /* Obsolete */
  
	public Object deepCopy()
	{
		ModelContainerSer copy = (ModelContainerSer)(super.deepCopy());
		
		copy.functionNodeIds = new String[this.functionNodeIds.length];
		for (int i = 0; i < copy.functionNodeIds.length; i++)
			copy.functionNodeIds[i] = this.functionNodeIds[i];
		
		copy.activityNodeIds = new String[this.activityNodeIds.length];
		for (int i = 0; i < copy.activityNodeIds.length; i++)
			copy.activityNodeIds[i] = this.activityNodeIds[i];
		
		copy.conduitNodeIds = new String[this.conduitNodeIds.length];
		for (int i = 0; i < copy.conduitNodeIds.length; i++)
			copy.conduitNodeIds[i] = this.conduitNodeIds[i];
		
		copy.constraintNodeIds = new String[this.constraintNodeIds.length];
		for (int i = 0; i < copy.constraintNodeIds.length; i++)
			copy.constraintNodeIds[i] = this.constraintNodeIds[i];
		
		return copy;
	}	
}

