package expressway.ser;

import expressway.common.*;


import java.util.Date;
import java.io.Serializable;


public class ModelScenarioSer extends ModelElementSer implements HierarchyScenarioSer
{
	public Date date;  // real time when ModelScenario was created.
	public String scenarioThatWasCopiedNodeId;
	public String modelScenarioSetNodeId;
	public String[] attributeNodeIds;
	public Serializable[] attributeValues;

	public Date startingDate;  // starting time specified. May be null.
	public Date finalDate;
	public long duration;
	public int iterationLimit;

	public String[] subHierarchyNodeIds;
	public String[] fieldNames;
	public int seqNo;

	public String[] predefinedEventNodeIds;
	public String[] simRunNodeIds;
	public String[] simRunSetNodeIds;
	
	public Boolean externalStateBindingType;
	public ModelAPITypes.TimePeriodType externalStateGranularity;
	
	public Double computedNetValueMean;
	public Double computedNetValueVariance;
	public Double computedNetValueAssurance;

	public String netValueStatePath;
	public String netValueAttrId;

	public String getNodeKind() { return NodeKindNames.ModelScenario; }
	
	
	public void setStartingDate(Date d) { this.startingDate = d; }
	public Date getStartingDate() { return startingDate; }
	
	public void setDuration(long dur) { this.duration = dur; }
	public long getDuration() { return duration; }
	
	public void setIterationLimit(int limit) { this.iterationLimit = limit; }
	public int getIterationLimit() { return iterationLimit; }
	
	public void setFinalDate(Date d) { this.finalDate = d; }
	public Date getFinalDate() { return finalDate; }
	


	public void setDate(Date d) { this.date = d; }
	public Date getDate() { return date; }
	
	public void setScenarioThatWasCopiedNodeId(String id) { this.scenarioThatWasCopiedNodeId = id; }
	public String getScenarioThatWasCopiedNodeId() { return scenarioThatWasCopiedNodeId; }
	
	public void setScenarioSetNodeId(String id) { this.modelScenarioSetNodeId = id; }
	public String getScenarioSetNodeId() { return modelScenarioSetNodeId; }
	
	public void setAttributeNodeIds(String[] ids) { this.attributeNodeIds = ids; }
	public String[] getAttributeNodeIds() { return attributeNodeIds; }
	
	public void setAttributeValues(Serializable[] values) { this.attributeValues = values; }
	public Serializable[] getAttributeValues() { return attributeValues; }

	
	/* From HierarchySer */
	
	public void setSubHierarchyNodeIds(String[] ids) { this.subHierarchyNodeIds = ids; }
	
	public String[] getSubHierarchyNodeIds() { return subHierarchyNodeIds; }
	
	public void setFieldNames(String[] fieldNames) { this.fieldNames = fieldNames; }
	
	public String[] getFieldNames() { return fieldNames; }
	
	public void setSeqNo(int n) { this.seqNo = n; }
	
	public int getSeqNo() { return seqNo; }



	public Object deepCopy()
	{
		ModelScenarioSer copy = (ModelScenarioSer)(super.deepCopy());
		
		copy.predefinedEventNodeIds = new String[this.predefinedEventNodeIds.length];
		for (int i = 0; i < copy.predefinedEventNodeIds.length; i++)
			copy.predefinedEventNodeIds[i] = this.predefinedEventNodeIds[i];
		
		copy.simRunNodeIds = new String[this.simRunNodeIds.length];
		for (int i = 0; i < copy.simRunNodeIds.length; i++)
			copy.simRunNodeIds[i] = this.simRunNodeIds[i];
		
		copy.attributeNodeIds = new String[this.attributeNodeIds.length];
		for (int i = 0; i < copy.attributeNodeIds.length; i++)
			copy.attributeNodeIds[i] = this.attributeNodeIds[i];
		
		copy.attributeValues = new Serializable[this.attributeValues.length];
		for (int i = 0; i < copy.attributeValues.length; i++)
			copy.attributeValues[i] = null;
		
		return copy;
	}
	
	
	public String toString()  // displayed in the DomainListPanel
	{
		return this.getName();
	}
}

