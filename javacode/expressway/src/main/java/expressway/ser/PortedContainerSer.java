package expressway.ser;

import expressway.common.*;
import java.util.Set;
import java.util.Arrays;


public abstract class PortedContainerSer extends ModelContainerSer
{
	public String[] portNodeIds;
	public String[] stateNodeIds;
	
	
	public Object deepCopy()
	{
		PortedContainerSer copy = (PortedContainerSer)(super.deepCopy());
		
		copy.portNodeIds = new String[this.portNodeIds.length];
		for (int i = 0; i < copy.portNodeIds.length; i++)
			copy.portNodeIds[i] = this.portNodeIds[i];
		
		copy.stateNodeIds = new String[this.stateNodeIds.length];
		for (int i = 0; i < copy.stateNodeIds.length; i++)
			copy.stateNodeIds[i] = this.stateNodeIds[i];
		
		return copy;
	}
}

