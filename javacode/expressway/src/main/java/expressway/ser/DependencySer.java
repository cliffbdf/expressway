package expressway.ser;

import expressway.common.*;



public class DependencySer extends DecisionElementSer
{
	public String aNodeId;
	public String bNodeId;
	
	public String getNodeKind() { return NodeKindNames.Dependency; }
}

