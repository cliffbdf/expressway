package expressway.ser;

import expressway.common.*;


import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.net.URL;


public class DecisionDomainSer extends DecisionElementSer implements NodeDomainSer
{    
	public String sourceName;
	public URL sourceURL;
	public String declaredName;
	public String declaredVersion;
	public String[] namedReferenceNames;
	
	public String[] decisionPointNodeIds;
	public String[] decisionScenarioNodeIds;
	public String[] decisionScenarioSetNodeIds;
	public String[] dependencyNodeIds;
	public String[] precursorRelationshipNodeIds;
	public String currentDecisionScenarioNodeId;
	public String[] motifDefNodeIds;
	
	
	public String[] menuItemNodeIds;
	public String[] menuItemTextAr;
	public String[] menuItemDescAr;
	public String[] javaMethodNames;
	public String[] menuTreeNodeIds;
	
	
	public String getNodeKind() { return NodeKindNames.DecisionDomain; }


	public void setSourceName(String sourceName) { this.sourceName = sourceName; }
	public String getSourceName() { return sourceName; }
	
	public void setSourceURL(URL sourceURL) { this.sourceURL = sourceURL; }
	public URL getSourceURL() { return sourceURL; }
	
	public void setDeclaredName(String declaredName) { this.declaredName = declaredName; }
	public String getDeclaredName() { return declaredName; }
	
	public void setDeclaredVersion(String declaredVersion) { this.declaredVersion = declaredVersion; }
	public String getDeclaredVersion() { return declaredVersion; }
	
	public void setNamedReferenceNames(String[] names) { this.namedReferenceNames = names; }
	public String[] getNamedReferenceNames() { return namedReferenceNames; }
	
	public void setScenarioSetNodeIds(String[] ids) { this.decisionScenarioSetNodeIds = ids; }
	public String[] getScenarioSetNodeIds() { return decisionScenarioSetNodeIds; }
	
	public void setCurrentScenarioNodeId(String id) { this.currentDecisionScenarioNodeId = id; }
	public String getCurrentScenarioNodeId() { return currentDecisionScenarioNodeId; }
	
	public void setScenarioIds(String[] ids) { this.decisionScenarioNodeIds = ids; }
	public String[] getScenarioIds() { return decisionScenarioNodeIds; }
	
	public void setMotifDefNodeIds(String[] ids) { this.motifDefNodeIds = ids; }
	public String[] getMotifDefNodeIds() { return motifDefNodeIds; }
	
	
	public void setMenuItemNodeIds(String[] ids) { menuItemNodeIds = ids; }
	public String[] getMenuItemNodeIds() { return menuItemNodeIds; }
	
	
	public void setMenuItemTextAr(String[] textAr) { menuItemTextAr = textAr; }
	public String[] getMenuItemTextAr() { return menuItemTextAr; }

	
	public void setMenuItemDescAr(String[] descAr) { menuItemDescAr = descAr; }
	public String[] getMenuItemDescAr() { return menuItemDescAr; }
	
	
	public void setJavaMethodNames(String[] names) { javaMethodNames = names; }
	public String[] getJavaMethodNames() { return javaMethodNames; }
	
	
	public void setMenuTreeNodeIds(String[] ids) { menuTreeNodeIds = ids; }
	public String[] getMenuTreeNodeIds() { return menuTreeNodeIds; }

	
	public Object deepCopy()
	{
		DecisionDomainSer copy = (DecisionDomainSer)(super.deepCopy());
		
		copy.decisionPointNodeIds = new String[this.decisionPointNodeIds.length];
		for (int i = 0; i < copy.decisionPointNodeIds.length; i++)
			copy.decisionPointNodeIds[i] = this.decisionPointNodeIds[i];
		
		copy.decisionScenarioNodeIds = new String[this.decisionScenarioNodeIds.length];
		for (int i = 0; i < copy.decisionScenarioNodeIds.length; i++)
			copy.decisionScenarioNodeIds[i] = this.decisionScenarioNodeIds[i];
		
		copy.decisionScenarioSetNodeIds = new String[this.decisionScenarioSetNodeIds.length];
		for (int i = 0; i < copy.decisionScenarioSetNodeIds.length; i++)
			copy.decisionScenarioSetNodeIds[i] = this.decisionScenarioSetNodeIds[i];
		
		copy.dependencyNodeIds = new String[this.dependencyNodeIds.length];
		for (int i = 0; i < copy.dependencyNodeIds.length; i++)
			copy.dependencyNodeIds[i] = this.dependencyNodeIds[i];
		
		copy.precursorRelationshipNodeIds = new String[this.precursorRelationshipNodeIds.length];
		for (int i = 0; i < copy.precursorRelationshipNodeIds.length; i++)
			copy.precursorRelationshipNodeIds[i] = this.precursorRelationshipNodeIds[i];
		
		return copy;
	}
}

