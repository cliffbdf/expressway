package expressway.ser;

import expressway.common.*;



public class NamedReferenceSer extends NodeSerBase
{
	public String description;
	public String fromTypeName;
	public String toTypeName;
	public String[] crossReferenceIds;
	public String getNodeKind() { return NodeKindNames.NamedReference; }
}

