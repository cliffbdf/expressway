package expressway.ser;

import expressway.common.*;
import java.util.Arrays;


public class FunctionSer extends PortedContainerSer implements MenuOwnerSer,
	TemplateInstanceSer
{
	public int maxEvals;
	
	
	public String getNodeKind() { return NodeKindNames.Function; }
}

