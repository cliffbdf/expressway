package expressway.ser;

import expressway.common.*;
import java.io.Serializable;


public interface HierarchySer extends HierarchyElementSer, TabularSer
{
	void setSubHierarchyNodeIds(String[] ids);
	String[] getSubHierarchyNodeIds();
	
	
	//void setValueAt(Serializable value, int column);
	//java.io.Serializable getValueAt(int column);
	
	
	/**
	 * The ordinal position, beginning with 0, of the Hierarchy, relative to
	 * other non-Attribute sibling Nodes (having the same parent). If this
	 * Hierarchy has no parent, its sequence number is 0.
	 */
	 
	void setSeqNo(int n);
	int getSeqNo();
}

