package expressway.ser;

import expressway.common.*;
import java.io.Serializable;
import java.util.Date;


public interface ScenarioSetSer extends NodeSer
{
	void setScenarioIds(String[] ids);
	String[] getScenarioIds();
	
	void setVariableAttributeId(String id);
	String getVariableAttributeId();
	
	void setScenarioThatWasCopiedNodeId(String id);
	String getScenarioThatWasCopiedNodeId();
}

