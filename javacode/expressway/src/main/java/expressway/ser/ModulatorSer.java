package expressway.ser;

import expressway.common.*;



public class ModulatorSer extends ActivitySer
{
	public String inputPortNodeId;
	public String outputPortNodeId;
	public String controlPortNodeId;
	public String stateNodeId;
	public Double defaultFactor;
	public String factorAttrNodeId;
	
	public String getNodeKind() { return NodeKindNames.Modulator; }
}

