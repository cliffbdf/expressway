package expressway.ser;

import expressway.common.*;



public abstract class AbstractSustainSer extends ActivitySer
{
	public String inputPortNodeId;
	public String outputPortNodeId;
	public String periodPortNodeId;
	public String stateNodeId;
	public String timeDistShapeAttrNodeId;
	public String timeDistScaleAttrNodeId;

	public double defaultTimeDistShape;
	public double defaultTimeDistScale;
}

