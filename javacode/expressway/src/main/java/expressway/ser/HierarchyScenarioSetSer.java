package expressway.ser;

import expressway.common.*;
import java.io.Serializable;
import java.util.Date;


public class HierarchyScenarioSetSer extends HierarchyElementImplSer
	implements HierarchySer
{
	public String attributeId;
	public String[] scenarioIds;
	public String scenarioThatWasCopiedNodeId;

	public String[] subHierarchyNodeIds;
	public String[] fieldNames;
	public int seqNo;

	
	public String getNodeKind() { return NodeKindNames.HierarchyScenarioSet; }

	public String derivedFromScenarioId() { return scenarioThatWasCopiedNodeId; }
	
	public void setScenarioIds(String[] ids) { scenarioIds = ids; }
	public String[] getScenarioIds() { return scenarioIds; }
	
	public void setScenarioThatWasCopiedNodeId(String id) { scenarioThatWasCopiedNodeId = id; }
	public String getScenarioThatWasCopiedNodeId() { return scenarioThatWasCopiedNodeId; }

	public void setVariableAttributeId(String id) { this.attributeId = id; }
	public String getVariableAttributeId() { return attributeId; }
	
	
	/* From HierarchySer */
	
	public void setSubHierarchyNodeIds(String[] ids) { this.subHierarchyNodeIds = ids; }
	
	public String[] getSubHierarchyNodeIds() { return subHierarchyNodeIds; }
	
	public void setFieldNames(String[] fieldNames) { this.fieldNames = fieldNames; }
	
	public String[] getFieldNames() { return fieldNames; }
	
	public void setSeqNo(int n) { this.seqNo = n; }
	
	public int getSeqNo() { return seqNo; }
}

