package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;


import java.util.Date;
import java.io.Serializable;

public class PredefinedEventSer extends ModelElementSer
{
	public boolean pulse;
	public String timeAttrNodeId;
	public String valueAttrNodeId;
	public String timeExprString;
	public String valueExprString;
	public String getNodeKind() { return NodeKindNames.PredefinedEvent; }
}

