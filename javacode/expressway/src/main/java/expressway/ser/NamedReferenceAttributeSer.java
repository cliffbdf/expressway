package expressway.ser;


import expressway.common.*;
import java.io.Serializable;


public class NamedReferenceAttributeSer extends NodeSerBase implements AttributeSer
{
	public Serializable defaultValue;
	public Serializable value;
	
	
	public String getNodeKind() { return NodeKindNames.NamedReferenceAttribute; }
	
	
	public Serializable getDefaultValue() { return defaultValue; }
	
	
	public void setDefaultValue(Serializable dv) { this.defaultValue = dv; }
}

