/**
 * Implementation for Expressway server prototype.
 *
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 *
 * @author Cliff Berg
 * @version 0.3.
 * @see <a href="{@docRoot}/Tool%20Architecture.odg">Tool Architecture (Open Office Draw format)</a>.
 */

package expressway.server;

