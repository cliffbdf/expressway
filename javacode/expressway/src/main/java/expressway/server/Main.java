/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.generalpurpose.ThrowableUtil;
import expressway.generalpurpose.MainCallback;
import expressway.generalpurpose.JarUtils;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.awt.AWTTools;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.Image;
import java.util.Timer;
import java.util.TimerTask;
import java.util.List;
import java.util.Vector;
import java.util.Enumeration;
import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.InetAddress;
import java.rmi.Remote;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.*;
import java.rmi.RemoteException;
import java.rmi.RMISecurityManager;
import org.xml.sax.SAXParseException;
import com.apple.eawt.*;



/** ****************************************************************************
 * Main class for Expressway Server control panel, for starting, stopping, and
 * configuring server.
 */
 
public class Main
{
	/** ************************************************************************
	 * Main method for Expressway server administrative UI.
	 */
	 
	public static void main(String[] args)
	{
		final long MinimumMegabytesRequired = 512;
		final long BytesInAMegabyte = 1000000;
		final int DefaultHttpPort = 8080;
		
		String httpNetworkInterfaceName = null;
		
		
		/*
		 * Determine if program should be restarted with more memory.
		 */
		
		if ((args.length == 0 ) || (! args[0].equals("-norestart")))
		{
			long memory = Runtime.getRuntime().totalMemory();
			
			if (memory < (MinimumMegabytesRequired * BytesInAMegabyte))
			{
				URLClassLoader urlClassLoader = (URLClassLoader)(Main.class.getClassLoader());
				URL[] urls = urlClassLoader.getURLs();
				String classpath = "";
				String pathSeparator = System.getProperty("path.separator");
				String fileSeparator = System.getProperty("file.separator");
				boolean first = true;
				String path = null;
				for (URL url : urls)
				{
					if (first) first = false;
					else classpath += pathSeparator;
					
					classpath += url.toString();
					
					path = url.getPath();
				}
				
				path = URLDecoder.decode(path);
				
				List<String> newArgs = new Vector<String>();
				
				String sMemoryOption = "-Xms" + MinimumMegabytesRequired + "m";
				String xMemoryOption = "-Xmx" + MinimumMegabytesRequired + "m";
				
				// Replace file separators.
				
				path = path.replace("/", fileSeparator);
				
				if (fileSeparator.equals("\\"))
					if (path.startsWith(fileSeparator))
						if ((path.charAt(2) == ':') || (path.charAt(2) == '|'))
							path = path.substring(1);
				
				GlobalConsole.print("java " + 
					sMemoryOption + " " + xMemoryOption + " -jar \"" +
					path + "\" " + 
					"-norestart ");
				
				for (String arg : args) GlobalConsole.print(arg + " ");
				GlobalConsole.println();
				
				newArgs.add("java");
				newArgs.add(sMemoryOption);
				newArgs.add(xMemoryOption);
				newArgs.add("-jar");
				newArgs.add(path);
				newArgs.add("-norestart");  // must be first after the class name
				
				for (String arg : args) newArgs.add(arg);
			
				JFrame restartNoticeFrame = null;
				
				ProcessBuilder pb = new ProcessBuilder(newArgs);
				try
				{
					restartNoticeFrame = new JFrame("Restarting with increased memory");
					restartNoticeFrame.setLayout(new FlowLayout());
					restartNoticeFrame.setSize(500, 100);
					Dimension screenDimension = restartNoticeFrame.getToolkit().getScreenSize();

					restartNoticeFrame.setLocation(
						(screenDimension.width - restartNoticeFrame.getWidth())/2,
						(screenDimension.height - restartNoticeFrame.getHeight())/2);
					restartNoticeFrame.add(new JLabel("Expressway server restarting with increased memory"));
					restartNoticeFrame.show();
					
					final Process p = pb.start();
					restartNoticeFrame.dispose();
					restartNoticeFrame = null;
										
					(
					new Thread()
					{
						public void run()
						{
							InputStream is = p.getInputStream();
							InputStreamReader isr = new InputStreamReader(is);
							BufferedReader br = new BufferedReader(isr);
							
							while (true) try
							{
								String line = br.readLine();
								if (line == null) break;
								GlobalConsole.println(line);
							}
							catch (IOException ex)
							{
								GlobalConsole.printStackTrace(ex);
								break;
							}
						}
					}
					).start();
					
					(
					new Thread()
					{
						public void run()
						{
							InputStream is = p.getErrorStream();
							InputStreamReader isr = new InputStreamReader(is);
							BufferedReader br = new BufferedReader(isr);
							
							while (true) try
							{
								String line = br.readLine();
								if (line == null) break;
								GlobalConsole.println(line);
							}
							catch (IOException ex)
							{
								GlobalConsole.printStackTrace(ex);
								break;
							}
						}
					}
					).start();
					
					try
					{
						p.waitFor();
						GlobalConsole.println(
							"Restart process exited with status=" + p.exitValue());
					}
					catch (InterruptedException iex)
					{
						p.destroy();
					}
					
					System.exit(0);
				}
				catch (IOException ex)
				{
					if (restartNoticeFrame != null) restartNoticeFrame.dispose();
					ErrorDialog.showReportableDialog(null,
						"Unable to restart with increased memory: " +
						ThrowableUtil.getAllMessages(ex), "Error", ex);
					
					System.exit(1);
				}
			}
		}
		
		
        /*if (System.getSecurityManager() == null) 
        {
            System.setSecurityManager(new RMISecurityManager());
            GlobalConsole.println("Security manager installed.");
        }
        else
            GlobalConsole.println("Security manager already exists.");
		*/
 
		
		/*
		 * Retrieve and validate input options.
		 */
		
		int noOfOptions = 0;
		//long randomSeed = 0;
		//boolean randomSeedProvided = false;
		List<String> jarFilePaths = new Vector<String>();
		String dbdirString = null;
		List<String> libraryPathStrs = new Vector<String>();
		NetworkInterface httpNetworkInterface = null;
		InetAddress httpInetAddress = null;
		int httpPort = DefaultHttpPort;
		
		for (int i = 0; i < args.length; i++)
		{
			if (args[i].substring(0,1).equals("-"))
			{
				noOfOptions++;
				String optionString = args[i].substring(1);
				String[] optionParts = optionString.split("=");
				
				/*
				if (optionParts[0].equals("random_seed"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option random_seed must specify a seed value");
						return;
					}
					
					try { randomSeed = Long.parseLong(optionParts[1]); }
					catch (NumberFormatException nfe)
					{
						GlobalConsole.println(
							"Option random_seed must specify a long value");
						return;
					}
					
					randomSeedProvided = true;
				}
				else */
				
				if (optionParts[0].equals("norestart"))
				{
					GlobalConsole.println("Started with -norestart...");
				}
				else if (optionParts[0].equals("dbdir"))
				{
					dbdirString = optionParts[1];
				}
				else if (optionParts[0].equals("motif"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option motif must specify a JAR file path");
						return;
					}
					
					jarFilePaths.add(optionParts[1]);
					
				}
				else if (optionParts[0].equals("library"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option library must specify the file path of a Java JAR file.");
						return;
					}
					
					libraryPathStrs.add(optionParts[1]);
				}
				else if (optionParts[0].equals("httpadapter"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option httpadapter must specify the name of a network adapter.");
						return;
					}
					
					httpNetworkInterfaceName = optionParts[1];
				}
				else if (optionParts[0].equals("httpport"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option httpport must specify a TCP port number");
						return;
					}
					
					try { httpPort = Integer.parseInt(optionParts[1]); }
					catch (NumberFormatException ex)
					{
						GlobalConsole.println("httpport value must be an integer");
						return;
					}
				}
			}
			else break;  // past the options.
		}
		
		
		/*
		 * Identify the network interface to use for incoming HTTP requests.
		 */
		
		if (httpNetworkInterfaceName == null)  // Choose a network interface.
		{
			Enumeration<NetworkInterface> ens;
			try { ens = NetworkInterface.getNetworkInterfaces(); }
			catch (SocketException ex)
			{
				GlobalConsole.println("When getting list of network interfaces");
				GlobalConsole.printStackTrace(ex);
				return;
			}

			for (; ens.hasMoreElements();)
			{
				NetworkInterface ni = ens.nextElement();
				try
				{
					if (ni.isUp())
					{
						if (OperationMode.PrintNetworkInformation)
							GlobalConsole.println("Network Interface '" + ni.getName() + "' is up.");
						
						httpNetworkInterface = ni;
						break;
					}
				}
				catch (SocketException ex)
				{
					GlobalConsole.println("When checking status of " + ni.getName());
					GlobalConsole.printStackTrace(ex);
					continue;
				}
			}
			
			if (httpNetworkInterface == null)
			{
				GlobalConsole.println("Could not find an operational network interface");
				return;
			}
		}
		else
		{
			NetworkInterface ni;
			try { ni = NetworkInterface.getByName(httpNetworkInterfaceName); }
			catch (SocketException ex)
			{
				GlobalConsole.println("When getting network interface " + httpNetworkInterfaceName);
				GlobalConsole.printStackTrace(ex);
				return;
			}
			
			if (ni == null)
			{
				GlobalConsole.println("Network adapter with name '" + httpNetworkInterfaceName +
					"' not found");
				return;
			}
			
			httpNetworkInterface = ni;
			if (OperationMode.PrintNetworkInformation)
				GlobalConsole.println("Network Interface set to '" + ni.getName() + "'.");
		}
		
			
		/*
		 * Identify the Internet Address to use for incoming HTTP requests.
		 */
		
		if (OperationMode.PrintNetworkInformation)
			GlobalConsole.println("Checking internet addresses for interface '" +
				httpNetworkInterface.getName() + "'.");
		int i = 0;
		Enumeration<InetAddress> ens = httpNetworkInterface.getInetAddresses();
		List<InetAddress> iaddrs = new Vector<InetAddress>();
		for (; ens.hasMoreElements();)
		{
			i++;
			InetAddress iaddr = ens.nextElement();
			if (OperationMode.PrintNetworkInformation)
				GlobalConsole.println("Checking internet address '" + iaddr.toString() + "'.");
			if (iaddr.isSiteLocalAddress())
			{
				if (OperationMode.PrintNetworkInformation)
					GlobalConsole.println("Internet address '" + iaddr.toString() + 
						"' is a site-local address.");
				//continue;
			}
			
			httpInetAddress = iaddr;
			break;
		}
		if (OperationMode.PrintNetworkInformation)
			GlobalConsole.println(i + " addresses checked.");

		
		if (httpInetAddress == null)
		{
			GlobalConsole.println("Could not find an Internet address for adapter " +
				httpNetworkInterface.getName());
			return;
		}
		
		GlobalConsole.println("For HTTP requests, using network address " + httpInetAddress);
		
					
		/*
		 * Creatre RMI Registry if there is not one already.
		 */
		
        try
        {
            LocateRegistry.createRegistry(1099); 
            GlobalConsole.println("java RMI registry created.");
        }
        catch (RemoteException e)
        {
            //do nothing, error means registry already exists
            GlobalConsole.println("java RMI registry already exists.");
        }


        /*
         * Create ClassLoader for loading motif resources.
         */

		ServiceContext.setMotifClassLoader(new MotifClassLoader(
			Thread.currentThread().getContextClassLoader()));


		/*
         * Create UI for managing server.
         */
         
		new MainFrame(dbdirString, jarFilePaths, libraryPathStrs, 
			httpNetworkInterface, httpInetAddress, httpPort);
	}
}


class MainFrame extends JFrame implements MainCallback
{
	public static final String StoppedImagePath = "/images/RedLight.png";
	public static final String RunningImagePath = "/images/GreenLight.png";
	public static final String SmallIconImagePath = "/images/SmallIconImage.png";
	public static final String MediumIconImagePath = "/images/MediumIconImage.png";
	public static final String LargeIconImagePath = "/images/LargeIconImage.png";
	
	private JPanel controlPanel;

	//private JLabel seedLabel;
	//private JTextField seedTextField;
	private JButton dbdirButton;
	private JButton importButton;
	private JButton saveButton;
	private JButton startButton;
	private JButton stopButton;
	private JLabel statusLabel;
	private Icon stoppedStatusIcon;
	private Icon runningStatusIcon;
	private JButton netButton;
	private JButton httpPortButton;
	private JTextField httpPortField;
	
	private JTextArea consoleTextArea;
	private JScrollPane consoleScrollPane;
	
	private ModelEngineLocalPojoImpl localServer;
	private ModelEngineRMIPojoImpl remoteServer;
	private HTTPManager httpManager;
	private NetworkInterface httpNetworkInterface;
	private InetAddress httpInetAddress;
	private int httpPort;
	private Thread pollingThread;
	private Thread consoleThread;
	
	private Image smallIconImage;
	private Image mediumIconImage;
	private Image largeIconImage;

	
	
	public MainFrame(String dbDirString, List<String> jarFilePaths, List<String> libraryPathStrs,
		NetworkInterface httpNetworkInterface, InetAddress httpInetAddress, int httpPort)
	{
		super("Expressway\u2122 Server Version " + ReleaseInfo.VersionNumber);
		this.httpNetworkInterface = httpNetworkInterface;
		this.httpInetAddress = httpInetAddress;
		this.httpPort = httpPort;
		
		
		// *********************************************************************
		// Define behavior for when user closes the main window.
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowListener()
		{
			public void windowActivated(WindowEvent e) {}
			public void windowClosed(WindowEvent e) {}
			
			public void windowClosing(WindowEvent e)
			{
				closeMainWindowAndApplication();
			}
			
			public void windowDeactivated(WindowEvent e) {}
			public void windowDeiconified(WindowEvent e) {}
			public void windowIconified(WindowEvent e) {}
			public void windowOpened(WindowEvent e) {}
		});
		
		
		
		// *********************************************************************
		// Provide icon images for the OS to use to depict this application.
		
		List<Image> iconImages = new Vector<Image>();
		iconImages.add(smallIconImage = getImage(SmallIconImagePath));
		iconImages.add(mediumIconImage = getImage(MediumIconImagePath));
		iconImages.add(largeIconImage = getImage(LargeIconImagePath));
		setIconImages(iconImages);
		
		try  // Use reflection so that it will not try to load these classes on a PC.
		{
			//import com.apple.eawt.Application;  // for Mac only
			//import com.apple.eawt.ApplicationListener;  // for Mac only
			//import com.apple.eawt.ApplicationEvent;  // for Mac only
			//import com.apple.eawt.ApplicationAdapter;  // for Mac only
			
			//Application app = Application.getApplication();

			Class appClass = Class.forName("com.apple.eawt.Application");
			java.lang.reflect.Method methodGetApp = appClass.getMethod("getApplication");
			Object app = methodGetApp.invoke(null);
			

			//app.setDockIconImage(mediumIconImage);
			
			java.lang.reflect.Method methodSetDocIconImage = appClass.getMethod(
				"setDockIconImage", Image.class);
				
			methodSetDocIconImage.invoke(app, mediumIconImage);


			/*app.addApplicationListener(new ApplicationAdapter()
			{
				public void handleQuit(ApplicationEvent event)
				{
					closeMainWindowAndApplication();
					event.setHandled(false);
				}
			});*/
			
			Class appListenerClass = Class.forName("generalpurpose.AppleApplicationListener");
			Object appListener = appListenerClass.newInstance();
			java.lang.reflect.Method methodSetMainFrame = appListenerClass.getMethod(
				"setMainFrame", MainCallback.class);
			
			methodSetMainFrame.invoke(appListener, this);
			
			Class appleApplAdapterClass = Class.forName("com.apple.eawt.ApplicationListener");
			java.lang.reflect.Method methodAddAppListener = appClass.getMethod(
				"addApplicationListener", appleApplAdapterClass);
	
			java.lang.reflect.Method methodSetAboutTitle = appListenerClass.getMethod(
				"setAboutTitle", String.class);
			methodSetAboutTitle.invoke(appListener, "About Expressway\u2122");
			
			java.lang.reflect.Method methodSetAboutText = appListenerClass.getMethod(
				"setAboutText", String.class);
			methodSetAboutText.invoke(appListener, "Expressway\u2122 v. " + 
				ReleaseInfo.VersionNumber + "\n" + ReleaseInfo.CopyrightStatement);
			
			java.lang.reflect.Method methodSetAboutImageIcon = appListenerClass.getMethod(
				"setAboutImageIcon", javax.swing.ImageIcon.class);
			methodSetAboutImageIcon.invoke(appListener, new ImageIcon(
				AWTTools.getImage(MainFrame.class, MainFrame.MediumIconImagePath)));
			
			methodAddAppListener.invoke(app, appListener);
		}
		catch (Throwable ex)  // Give up: probably not a Mac.
		{
			GlobalConsole.println("Warning: " + ThrowableUtil.getAllMessages(ex));
			GlobalConsole.println("\t(Apple Mac-specific features not loaded)");
		}
		
		
		// *********************************************************************
		// Provide images needed by this UI.
		
		stoppedStatusIcon = new ImageIcon(getImage(StoppedImagePath));
		runningStatusIcon = new ImageIcon(getImage(RunningImagePath));
		
		
		// *********************************************************************
		// Set initial position and size.
		
		setLocation(50, 50);
		setSize(600, 400);
		
		
		// *********************************************************************
		// Add copyright notice.
		
		JPanel bottomPanel;
		getContentPane().add(bottomPanel = new JPanel(), BorderLayout.SOUTH);
		JLabel copyrightLabel = new JLabel(ReleaseInfo.CopyrightStatement);
		bottomPanel.add(copyrightLabel);
		copyrightLabel.setFont(copyrightLabel.getFont().deriveFont((float)10.0));
		
		
		// *********************************************************************
		// Add controls for random_seed, dbdir, import, export, saving (syncing),
		// starting, and stopping.
		//
		// random_seed		TextField
		// dbdir			FileChooser and JLabel
		// import			FileChooser
		// //export
		// saving			Button
		// starting			Button
		// stopping			Button
		
		add(controlPanel = new JPanel(), BorderLayout.NORTH);
		controlPanel.setLayout(new GridLayout(5, 2));
		
		// Row 1.
		controlPanel.add(startButton = new JButton("Start"));
		controlPanel.add(stopButton = new JButton("Stop"));
		
		// Row 2.
		controlPanel.add(statusLabel = new JLabel("Server Status"));
		controlPanel.add(statusLabel = new JLabel(stoppedStatusIcon));
		
		//controlPanel.add(seedLabel = new JLabel("Random Seed"));
		//controlPanel.add(seedTextField = new JTextField());
		
		// Row 3.
		controlPanel.add(dbdirButton = new JButton("Choose Database Directory"));
		controlPanel.add(netButton = new JButton("Choose a network interface"));
		
		// Row 4.
		controlPanel.add(httpPortField = new JTextField(String.valueOf(httpPort)));
		controlPanel.add(httpPortButton = new JButton("Set HTTP Port"));
		
		// Row 5.
		controlPanel.add(importButton = new JButton("Import XML File"));
		controlPanel.add(saveButton = new JButton("Sync Database"));
		
		
		// *********************************************************************
		// Add action handlers.
		
		startButton.addActionListener(new StartActionListener());
		
		stopButton.addActionListener(new StopActionListener());
		
		//seedTextField.addActionListener(new SetSeedActionListener());
		
		dbdirButton.addActionListener(new SetDBDirActionListener());
		
		netButton.addActionListener(new ChooseNetActionListener());
		
		httpPortButton.addActionListener(new SetHttpPortActionListener());
		
		importButton.addActionListener(new ImportActionListener());
		
		saveButton.addActionListener(new SaveActionListener());
		
		
		// *********************************************************************
		// Add console.
		
		if (System.console() == null)  // Assigning stdout and stderr to a Panel.
		{
			consoleTextArea = new JTextArea(10, 50);
			consoleTextArea.setEditable(false);
			consoleScrollPane = new JScrollPane(consoleTextArea);
			GlobalConsole.setTextArea(consoleTextArea);
			

			// Construct and start a Thread to copy data from stdout and stderr
			// to a Piped Stream that writes to a console TextArea.
			
			consoleThread = new Thread()
			{
				public void run()
				{
					int errors = 0;
					while (errors < 3) try
					{
						PipedInputStream pin = new PipedInputStream();
						PipedOutputStream pout = new PipedOutputStream(pin);
						BufferedReader iis = new BufferedReader(new InputStreamReader(pin));
						PrintStream ps = new PrintStream(pout, true);
						
						System.setOut(ps);
						System.setErr(ps);
						
						//if (errors > 0) GlobalConsole.println(errors + " console IO errors");

						String line;
						while ((line = iis.readLine()) != null)
						{
							consoleTextArea.append(line);
							consoleTextArea.append("\n");
						}
					}
					catch (IOException ex)
					{
						errors++;
					}
				}
			};
			
			consoleThread.start();
			
			add(consoleScrollPane, BorderLayout.CENTER);
		}
		
		
		// *********************************************************************
		// Make them visible.
		
		setVisible(true);


		// *********************************************************************
		// Instantiate a server, using the user's home directory, or the
		// directory specified.
		
		if (dbDirString == null) dbDirString = System.getProperty("user.home");
		File homeDir = new File(dbDirString);
		//File curDir = new File(System.getProperty("user.dir"));
		File expresswayDir = new File(homeDir, "expressway");
		if (! expresswayDir.exists())
		{
			if (! expresswayDir.mkdir())
			{
				ErrorDialog.showErrorDialog(this, 
					"Unable to create Expressway directory, " + expresswayDir);
				return;
			}
		}
		
		GlobalConsole.println("Using database directory: " + expresswayDir.getAbsolutePath());
		
		try
		{
			File dbfile = null;
			try
			{
				dbfile = ModelEngineLocalPojoImpl.findDatabaseFile(expresswayDir);
			}
			catch (ParameterError pe1) {}  // do nothing
			
			if (dbfile == null)
			{
				localServer = new ModelEngineLocalPojoImpl(expresswayDir);
				ServiceContext.create(localServer, ModelAPITypes.LocalServerClientId);
			}
			else
			{
				File simRunDir = new File(expresswayDir, "simrundir");
				GlobalConsole.println("Loading sim runs from " + simRunDir.toString());
				
				JFrame loadMessageFrame = new JFrame("Loading database");
				loadMessageFrame.setSize(300, 100);
				loadMessageFrame.add(new JLabel("Loading database..."), BorderLayout.CENTER);
				loadMessageFrame.setLocation(100, 100);
				loadMessageFrame.show();
				
				localServer = ModelEngineLocalPojoImpl.loadDatabase(dbfile, simRunDir);
				
				loadMessageFrame.dispose();
			}
			
			try { instantiateRemoteServer(localServer); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(MainFrame.this,
					"Unable to instantiate server by reading database file: " +
					ThrowableUtil.getAllMessages(ex), "Error", ex);
			}
		}
		catch (IOException ioe)
		{
			ErrorDialog.showReportableDialog(MainFrame.this,
				"Error reading server database: " +
				ThrowableUtil.getAllMessages(ioe), "Error", ioe);
		}
		catch (ParameterError pe)
		{
			try { instantiateServer(expresswayDir); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(MainFrame.this, ex);
			}
		}


		try
		{
			// *********************************************************************
			// Register a PeerListener to receive Notices from asynchronous processing
			// threads.
		
			PeerListener peerListener = new PeerListener()
			{
				public String getId() throws IOException
				{
					return "LocalServer Listener";
				}
				
				public void notify(PeerNotice notice)
				throws
					NotInterested,
					InconsistencyError,
					IOException
				{
					GlobalConsole.println(
						"Received " + notice.getClass().getName());
				}
			};

			ListenerRegistrar registrar = localServer.registerPeerListener(
				ModelAPITypes.LocalServerClientId, peerListener);
			
		
			// *********************************************************************
			// Process libraries, if any.
			//
			
			for (String libraryPathStr : libraryPathStrs)
			{
				try
				{
					File jarFile = JarUtils.getJarFile(libraryPathStr);
					byte[] byteAr = JarUtils.getBytes(jarFile);
					
					ModelEngineLocal.JarProcessor jarProcessor = localServer.getJarProcessor(
						false /* replace */,
						null, // no PeerListener because this call is synchronous.
						ModelAPITypes.StandaloneClientId,
						byteAr);
					
					jarProcessor.processJar(false /* not XML */);
				}
				catch (Exception ex)
				{
					GlobalConsole.println("While processing library option, " +
						ThrowableUtil.getAllMessages(ex));
					return;
				}
			}


			// *********************************************************************
			// Process the JAR files that were specified at startup, if any.
			
			for (String jarFilePath : jarFilePaths)
			{
				File jarFile = JarUtils.getJarFile(jarFilePath);
				byte[] jarByteAr = JarUtils.getBytes(jarFile);
				
				ModelEngineLocal.JarProcessor jarProcessor = 
					localServer.getJarProcessor(true /* replace */,
						null, //PeerListener
						ServiceContext.getCurrentClientId(),
						jarByteAr);
				
				jarProcessor.processJar(true);
			}
		
		
			// *********************************************************************
			// Start server.
		
			startServer();
			//startRemoteServer();
		}
		catch (Exception ex)
		{
			GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
			return;
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	ModelEngineRMIPojoImpl getRemoteServer() { return remoteServer; }
	
	
	void setHTTPNetworkInterface(NetworkInterface ni) { this.httpNetworkInterface = ni; }
	
	
	NetworkInterface getHTTPNetworkInterface() { return this.httpNetworkInterface; }
	
	
	void setHTTPInetAddress(InetAddress iaddr) { this.httpInetAddress = iaddr; }
	
	
	InetAddress getHTTPInetAddress() { return httpInetAddress; }
	
	
	int getHttpPort() { return httpPort; }
	
	
	void setHttpPort(int p) { this.httpPort = p; }
	
	
	/** Call to indicate in the UI that the server is active. */
	
	void serverIsActive()
	{
		statusLabel.setIcon(runningStatusIcon);
	}
	
	
	/** Call to indicate in the UI that the server is not active. */
	
	void serverIsNotActive()
	{
		statusLabel.setIcon(stoppedStatusIcon);
	}
	
	boolean isServerActive()
	{
		return statusLabel.getIcon() == runningStatusIcon;
	}
	
	
	/**
	 * From MainCallback.
	 * To be called by this class or any other to close the application gracefully.
	 */
	 
	public synchronized void closeMainWindowAndApplication()
	{
		if (localServer == null)
		{
			this.dispose();
			System.exit(0);
		}
		
		if (isServerActive())
			if (JOptionPane.showConfirmDialog(MainFrame.this, 
				"Warning: the server will be shut down. Continue?",
				"Warning", JOptionPane.OK_CANCEL_OPTION) != JOptionPane.OK_OPTION)
				return;
		
		
		// Stop server.
		// warn user if simulations are running.
		// stop all simulations
		
		localServer.allowNewSimulations(false);
		
		if (localServer.simulationsAreRunning())
		{
			if (JOptionPane.showConfirmDialog(MainFrame.this, 
				"There are simulations running. Exit?", "Please Confirm",
				JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
			{
				try { stopServer(); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(MainFrame.this,
						"Unable to gracefully stop server; it will be aborted", 
						"Error", ex);
				}
			}
			else
			{
				localServer.allowNewSimulations(true);
				return;
			}
		}
		else
			try
			{
				stopServer(); 
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(MainFrame.this,
					"Unable to gracefully stop server; it will be aborted", 
					"Error", ex);
			}
		
		
		// Exit application.
		// warn if there are unsaved changes
		// unregister and terminate app
		
		if (localServer.hasUnflushedChanges()) // there are unsaved changes,
		{
			// Ask user if changes should be saved
			if (JOptionPane.showConfirmDialog(MainFrame.this, 
				"There are unsynced changes in the DB; sync before quitting?",
				"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
			{
				// Write database to disk.
				
				try { localServer.flush(ModelAPITypes.LocalServerClientId); }
				catch (Exception ex)
				{
					GlobalConsole.printStackTrace(ex);
					
					if (JOptionPane.showConfirmDialog(MainFrame.this, 
						"Error while attempting to sync. Do you still want to quit?",
						"Error", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
					
						return;  // don't exit: data is unsaved. Allow the user the
								// chance to save it as XML.
				}
			}
		}
		
		this.dispose();
		System.exit(0);
	}
	
	
	/**
	 * Remove and shut down the server if it is currently running, and create a
	 * new instance of the server that is ready to run. Use the specified directory
	 * as the server's directory.
	 */
	 
	protected synchronized void instantiateServer(File serverDir)
	throws
		Exception
	{
		
		try
		{
			File simRunDir = new File(serverDir, "simrundir");
			GlobalConsole.println("Loading sim runs from " + simRunDir.toString());
			
			File dbfile = ModelEngineLocalPojoImpl.findDatabaseFile(serverDir);
			this.localServer = ModelEngineLocalPojoImpl.loadDatabase(dbfile, simRunDir);
		}
		catch (ParameterError pe)
		{
			if (this.localServer != null) stopServer();
			this.localServer = new ModelEngineLocalPojoImpl(serverDir);
		}
		
		instantiateRemoteServer(this.localServer);
	}
	
	
	/**
	 * Same as instantiateServer(File) but accepts a local server instance.
	 */
	 
	protected synchronized void instantiateRemoteServer(ModelEngineLocalPojoImpl localServer)
	throws
		Exception
	{
		this.localServer = localServer;
		remoteServer = new ModelEngineRMIPojoImpl(localServer);
		
		resetControlPanel();
	}
	
	
	protected synchronized void uninstantiateServer()
	{
		localServer = null;
		remoteServer = null;
	}
	
	
	protected synchronized void startRemoteServer() throws Exception
	{
		remoteServer.register();
			
			
		// Create thread for monitoring the health of the server.
		
		pollingThread = new ServerPollingThread();
		pollingThread.start();
	}
	
	
	protected synchronized void stopRemoteServer() throws Exception
	{
		GlobalConsole.println("Stopping service...");
		
		if (pollingThread != null) try
		{
			pollingThread.stop();
			GlobalConsole.println("\t...polling thread stopped;");
		}
		catch (Exception ex) {}

		if (remoteServer != null) try
		{
			remoteServer.unregister();
			GlobalConsole.println("\t...service unregistered;");
		}
		catch (Exception ex) {}
		
		serverIsNotActive();
	}
	
	
	protected void startServer()
	throws
		Exception  // if cannot start server
	{
		netButton.setEnabled(false);
		httpPortButton.setEnabled(false);
		httpPortField.setEditable(false);
		
		if (localServer != null)
		{
			localServer.reinitialize();
			startRemoteServer();
		}
		else throw new Exception(
			"There is no local server instance: cannot start remote service.");
		
		try
		{
			if (httpManager == null) httpManager = new HTTPManager(localServer);
			httpManager.startHTTPServer(getHTTPInetAddress(), getHttpPort());
			localServer.setHttpManager(httpManager);
		}
		catch (Exception ex)
		{
			GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
			return;
		}
		finally
		{
		}
	}
	
	
	protected void stopServer() throws Exception
	{
		stopRemoteServer();
		
		if (this.localServer != null)
		{
			localServer.stopAllServiceThreads();
			GlobalConsole.println("\t...service threads stopped.");
		}

		if (httpManager != null) httpManager.stopHTTPServer();

		netButton.setEnabled(true);
		httpPortButton.setEnabled(true);
		httpPortField.setEditable(true);
	}
	
	
	protected void resetControlPanel()
	{
		//seedTextField.setText("");
		serverIsNotActive();
	}
	
	
	class StartActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				startServer();
			}
			catch (ParameterError pe)  // not a problem.
			{
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(MainFrame.this, ex);  return;
			}
		}
	}
	
	
	class StopActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				stopServer();
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(MainFrame.this, ex);  return;
			}
		}
	}
	
	
	/*class SetSeedActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			String seedText = seedTextField.getText();
			long seed = 0;
			try { seed = Long.parseLong(seedText); }
			catch (NumberFormatException nfe)
			{
				GlobalConsole.printStackTrace(nfe);
				JOptionPane.showMessageDialog(MainFrame.this,
					"The seed " + seedText + " is not a valid integral number",
					"Error", JOptionPane.ERROR_MESSAGE);  return;
			}
			
			try { localServer.setRandomSeed(seed); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
				JOptionPane.showMessageDialog(MainFrame.this,
					ThrowableUtil.getAllMessages(ex), "Error",
					JOptionPane.ERROR_MESSAGE);  return;
			}
		}
	}*/
	
	
	class SetDBDirActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			// Display warning that models in memory will be deleted if a database
			// is found in the new directory.
			
			if (JOptionPane.showConfirmDialog(MainFrame.this, 
				"Warning: If a database exists in the directory specified, " +
				"models in memory will be deleted and replaced with the database's models; " +
				"and motifs in use will be unloaded and motifs from the new location " +
				"will be loaded",
				"Warning", JOptionPane.OK_CANCEL_OPTION) != JOptionPane.OK_OPTION)
				return;			
			
			
			// Identify the directory to use as the database directory.
			
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogTitle("Choose a directory for the Expressway database");
			
			File currentSetting = localServer.getDatabaseDirectory();
			if (currentSetting == null) currentSetting = new File(System.getProperty("user.home"));
			if (! currentSetting.exists()) currentSetting = new File(System.getProperty("user.dir"));
			
			chooser.setCurrentDirectory(currentSetting);
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			int returnVal = chooser.showOpenDialog(MainFrame.this);
			
			if(returnVal != JFileChooser.APPROVE_OPTION) return;
			
			File dbDir = chooser.getSelectedFile();
			
			File dbFile = new File(dbDir, "expresswaydb.ser");
			
			File simRunDir = new File(dbDir, "simrundir");
			
			try  // Load a database if one exists in the directory.
			{
				instantiateRemoteServer(ModelEngineLocalPojoImpl.loadDatabase(dbFile, 
					simRunDir));
			}
			catch (ParameterError pe)  // database file not found
			{
				localServer.setDatabaseDirectory(dbDir);
				localServer.setSimulationRunDirectory(simRunDir);
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(MainFrame.this, ex);  return;
			}
		}
	}
	
	
	class ChooseNetActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Component component = (Component)(e.getSource());
			JPopupMenu popup = new JPopupMenu();
			
			ActionListener listener = new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					Object source = e.getSource();
					if (! (source instanceof JMenuItem)) throw new RuntimeException(
						"Menu item source is not a JMenuItem");
					
					JMenuItem mi = (JMenuItem)source;
					try
					{
						NetworkInterface ni = NetworkInterface.getByName(mi.getText());
						if (ni == null)
						{
							ErrorDialog.showErrorDialog(MainFrame.this,
								"Network interface '" + mi.getText() + "' not found");
							return;
						}
						
						Enumeration<InetAddress> ens = ni.getInetAddresses();
						List<InetAddress> iaddrs = new Vector<InetAddress>();
						for (; ens.hasMoreElements();) iaddrs.add(ens.nextElement());
						
						InetAddress iaddr;
						if (iaddrs.size() == 0)
						{
							ErrorDialog.showErrorDialog(MainFrame.this,
								"The selected network interface has no Inet Address");
							return;
						}

						if (iaddrs.size() > 1)  // Ask users which one to use.
						{
							InetAddress[] choices = iaddrs.toArray(new InetAddress[iaddrs.size()]);
							int choiceNo = JOptionPane.showOptionDialog(MainFrame.this,
								"There is more than one Inet Address on network interface '" +
									ni.getName() + "': Please select one:",
								"Please select network address for HTTP server",
								JOptionPane.OK_CANCEL_OPTION,
								JOptionPane.QUESTION_MESSAGE,
								null,  // icon
								choices,
								choices[0]);
							
							if (choiceNo < 0) return;
							iaddr = choices[choiceNo];
						}
						else
							iaddr = iaddrs.get(0);
						
						setHTTPNetworkInterface(ni);
						setHTTPInetAddress(iaddr);
					}
					catch (SocketException ex)
					{
						ErrorDialog.showReportableDialog(MainFrame.this, ex);
						return;
					}
				}
			};
			
			try
			{
				Enumeration<NetworkInterface> nis = NetworkInterface.getNetworkInterfaces();
				
				for (; nis.hasMoreElements();)
				{
					NetworkInterface ni = nis.nextElement();
					JMenuItem mi = new JMenuItem(ni.getName());
					if (ni.isUp())
						mi.addActionListener(listener);
					else
						mi.setEnabled(false);
					popup.add(mi);
				}
			}
			catch (SocketException ex)
			{
				ErrorDialog.showReportableDialog(MainFrame.this, ex);
				return;
			}
			
			popup.show(component, component.getWidth(), 0);
		}
	}
	
	
	class SetHttpPortActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			int portNo;
			try { portNo = Integer.parseInt(httpPortField.getText()); }
			catch (NumberFormatException ex)
			{
				ErrorDialog.showReportableDialog(MainFrame.this, 
					"Invalid TCP port number", ex);  return;
			}
			
			setHttpPort(portNo);
		}
	}
	
	
	class ImportActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogTitle("Select and XML Expressway model file");
			chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
			
			// Filter all but directories and files ending with .xml
			FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"XML model files", "xml", "XML");
			chooser.setFileFilter(filter);

			
			int returnVal = chooser.showOpenDialog(MainFrame.this);
			
			if(returnVal != JFileChooser.APPROVE_OPTION) return;
			
			File file = chooser.getSelectedFile();
			
			XMLParser xmlParser = new XMLParser();
			try
			{
				ServiceContext.create(localServer, ModelAPITypes.LocalServerClientId);
				xmlParser.parse(file);
				xmlParser.setModelEngineLocal(localServer);
				xmlParser.setClientId(ModelAPITypes.LocalServerClientId);
				xmlParser.genDocument();
				System.err.println("Done parsing input file");
			}
			catch (SAXParseException spe)
			{
				GlobalConsole.println();
				GlobalConsole.printStackTrace(spe);
				ErrorDialog.showThrowableDialog(MainFrame.this,
					"At line " + spe.getLineNumber() +
					", column " + spe.getColumnNumber() + ",<br>" +
					ThrowableUtil.getAllMessagesAsHTML(spe), 
					"Error", spe);  return;
			}
			catch (Exception ex)
			{
				ErrorDialog.showThrowableDialog(MainFrame.this, ex);  return;
			}
			finally
			{
				ServiceContext.clear();
				List<String> warnings = xmlParser.getWarnings();
				if (warnings.size() > 0)
				{
					String warningString = "";
					boolean first = true;
					for (String warning : warnings)
					{
						if (first) first = false;
						else warningString += "\n";
						warningString += warning;
					}
					
					JOptionPane.showMessageDialog(MainFrame.this,
						warningString, "Warning",
						JOptionPane.WARNING_MESSAGE);
				}
			}
		}
	}
	
	
	class SaveActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			try { localServer.flush(ModelAPITypes.LocalServerClientId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(MainFrame.this, ex); return;
			}
		}
	}


	/** ************************************************************************
	 * Utility method for retrieving an image specified by a resource name.
	 */
	 
	static Image getImage(String imageResourceName)
	{
		return AWTTools.getImage(Main.class, imageResourceName);
	}
	
	
	/**
	 * Thread for continually polling the remote server to determine the server's
	 * actual availability.
	 */
	 
	class ServerPollingThread extends Thread
	{
		/** Indiates if abort has been called to abort polling. */
		private boolean mustAbort = false;
		
		
		/** Other threads should call this to abort polling. */
		public void abort()
		{
			this.mustAbort = true;
			this.interrupt();
		}
		
		
		public void run()
		{
			Timer timer = new Timer();  // For timing server calls, in case they hang.
			
			ModelEngineRMI me;
			try  // to obtain a remote reference to the server.
			{
				Registry registry = LocateRegistry.getRegistry();
				Remote remote = registry.lookup(Constants.RMIServiceName);
				me = (ModelEngineRMI)remote;
			}
			catch (Exception ex)
			{
				ErrorDialog.showThrowableDialog(MainFrame.this,
					"Cannot connect to server to monitor it: " +
					ThrowableUtil.getAllMessages(ex), "Error",
					ex);  return;
			}
			
			for (;;)
			{
				if (mustAbort) return;
				
				Thread threadToInterrupt = Thread.currentThread();
					
				TimerTask interruptTask = new InterruptTask(threadToInterrupt);
				
				try
				{
					timer.schedule(interruptTask, 300);  // Schedule an interrupt
						// for 300ms from now
					
					me.ping();  // Call the server.
					
					serverIsActive();  // Indicate that the call succeeded.
				}
				catch (Exception ex)  // timeout, abort, or some other error
				{
					GlobalConsole.printStackTrace(ex);
					
					serverIsNotActive();
					if (mustAbort) return;
					
					try  // to obtain the remote reference again.
					{
						Registry registry = LocateRegistry.createRegistry(1099);
						Remote remote = registry.lookup(Constants.RMIServiceName);
						me = (ModelEngineRMI)remote;
					}
					catch (Exception ex2) {}
				}
				finally
				{
					interruptTask.cancel();
					timer.purge();
					Thread.interrupted();  // clear interrupt status.
				}
				
				
				if (mustAbort) return;
				
				try { sleep(1000); }
				catch (InterruptedException ex)
				{
				}
				
				
				if (mustAbort) return;
			}
		}
	}


	static class InterruptTask extends TimerTask
	{
		private Thread threadToInterrupt;
		
		
		public InterruptTask(Thread threadToInterrupt)
		{
			this.threadToInterrupt = threadToInterrupt;
		}
		
		
		public void run()
		{
			threadToInterrupt.interrupt();
		}
	}
}

