/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.NamedReference.*;
import expressway.ser.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.List;
import java.util.Vector;


public abstract class CrossReferenceableNodeImpl
	extends PersistentNodeImpl
	implements CrossReferenceable
{
	/** Members are reference only. */
	private Set<CrossReference> crossReferences = new TreeSetNullDisallowed<CrossReference>();
	
	/** Ownership. */
	private List<CrossReferenceCreationListener> crCreationListeners =
		new Vector<CrossReferenceCreationListener>();
	
	/** Ownership. */
	private List<CrossReferenceDeletionListener> crDeletionListeners =
		new Vector<CrossReferenceDeletionListener>();
	
	
	public CrossReferenceableNodeImpl() { super(); }
	
	
	public CrossReferenceableNodeImpl(PersistentNode parent) { super(parent); }
	
	
	public Set<CrossReference> getCrossReferences() { return crossReferences; }
		
		
	public Set<NamedReference> getNamedReferences()
	{
		Set<NamedReference> nrs = new TreeSetNullDisallowed<NamedReference>();
		Set<CrossReference> xrefs = getCrossReferences();
		for (CrossReference xref : xrefs)
		{
			nrs.add(xref.getNamedReference());
		}
		
		return nrs;
	}
	
	
	public NamedReference getNamedReference(String refName)
	{
		Set<NamedReference> nrs = new TreeSetNullDisallowed<NamedReference>();
		Set<CrossReference> xrefs = getCrossReferences();
		for (CrossReference xref : xrefs)
		{
			NamedReference nr = xref.getNamedReference();
			if (nr.getName().equals(refName)) return nr;
		}
		
		return null;
	}
	
	
	public Set<CrossReferenceable> getToNodes(NamedReference nr)
	{
		Set<CrossReferenceable> toNodes = new TreeSetNullDisallowed<CrossReferenceable>();
		Set<CrossReference> xrefs = getCrossReferences();
		for (CrossReference xref : xrefs)
		{
			if (xref.getFromNode() == this) toNodes.add(xref.getToNode());
		}
		
		return toNodes;
	}
	
	
	public Set<CrossReferenceable> getFromNodes(NamedReference nr)
	{
		Set<CrossReferenceable> fromNodes = new TreeSetNullDisallowed<CrossReferenceable>();
		Set<CrossReference> xrefs = getCrossReferences();
		for (CrossReference xref : xrefs)
		{
			if (xref.getToNode() == this) fromNodes.add(xref.getFromNode());
		}
		
		return fromNodes;
	}
	
	
	public Set<CrossReferenceable> getToNodes(String refName)
	throws
		ParameterError
	{
		NamedReference nr = getNamedReference(refName);
		if (nr == null) throw new ParameterError("Cannot find Ref named '" + refName + "'");
		return getToNodes(nr);
	}
	
	
	public Set<CrossReferenceable> getFromNodes(String refName)
	throws
		ParameterError
	{
		NamedReference nr = getNamedReference(refName);
		if (nr == null) throw new ParameterError("Cannot find Ref named '" + refName + "'");
		return getFromNodes(nr);
	}
	

	public void addRef(CrossReference cr)
	throws
		ParameterError
	{
		for (CrossReference cr2 : crossReferences)
		{
			NamedReference nr = cr.getNamedReference();
			NamedReference nr2 = cr2.getNamedReference();
			if ((nr != nr2) && (nr.getName().equals(nr2.getName())))
				throw new ParameterError(
					"The NamedReference of the specified CrossReference has the " +
					"same name as another NamedReference to which this Node is linked");
		}
				
		this.crossReferences.add(cr);
	}
	

	public void removeRef(CrossReference cr)
	throws
		ParameterError
	{
		if (! this.crossReferences.remove(cr)) throw new ParameterError(
			"Cross Reference not found");
	}


	public void addCrossReferenceCreationListener(CrossReferenceCreationListener listener)
	{
		crCreationListeners.add(listener);
	}
	
	
	public void addCrossReferenceDeletionListener(CrossReferenceDeletionListener listener)
	{
		crDeletionListeners.add(listener);
	}
	
	
	public List<CrossReferenceCreationListener> getCrossRefCreationListeners()
	{
		return crCreationListeners;
	}
	
	
	public List<CrossReferenceDeletionListener> getCrossRefDeletionListeners()
	{
		return crDeletionListeners;
	}
	
	
	public void removeCrossReferenceListener(CrossReferenceListener listener)
	{
		if (listener instanceof CrossReferenceCreationListener)
			crCreationListeners.remove((CrossReferenceCreationListener)listener);
		else
			crDeletionListeners.remove((CrossReferenceDeletionListener)listener);
	}
	
	
	public void signalCrossReferenceCreated(final CrossReference cr)
	throws
		Exception
	{
		for (CrossReferenceCreationListener listener : crCreationListeners)
		{
			listener.invokeHandler(new CrossReferenceCreationContext()
			{
				public CrossReferenceable getCrossReferenceable()
				{
					return CrossReferenceableNodeImpl.this;
				}
				
				public NamedReference getNamedReference()
				{
					return cr.getNamedReference();
				}
				
				public CrossReferenceable getFromNode()
				{
					return cr.getFromNode();
				}
				
				public CrossReferenceable getToNode()
				{
					return cr.getToNode();
				}
				
				public CrossReference getCrossReference()
				{
					return cr;
				}
			});
		}
	}
	
	
	public void signalCrossReferenceDeleted(final NamedReference nr,
		final CrossReferenceable fromNode, final CrossReferenceable toNode)
	throws
		Exception
	{
		for (CrossReferenceDeletionListener listener : crDeletionListeners)
		{
			listener.invokeHandler(new CrossReferenceDeletionContext()
			{
				public CrossReferenceable getCrossReferenceable()
				{
					return CrossReferenceableNodeImpl.this;
				}
				
				public NamedReference getNamedReference()
				{
					return nr;
				}
				
				public CrossReferenceable getFromNode()
				{
					return fromNode;
				}
				
				public CrossReferenceable getToNode()
				{
					return toNode;
				}
			});
		}
	}
	
	
	void removeAllRefs(NamedReference nr)
	{
		for (CrossReference cr : crossReferences) try
		{
			if (cr.getNamedReference() == nr) removeRef(cr);
		}
		catch (ParameterError ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
  /* Standard methods to extend */
	
	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		for (CrossReference cr : crossReferences) try
		{
			cr.getNamedReference().unlink(cr);
		}
		catch (ParameterError ex) { GlobalConsole.printStackTrace(ex); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		crossReferences = null;
		crCreationListeners = null;
		crDeletionListeners = null;
	}
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		CrossRefableSerBase ser = (CrossRefableSerBase)nodeSer;
		
		String[][] crossRefs = new String[crossReferences.size()][];
		int i = 0;
		for (CrossReference cr : crossReferences)
		{
			String[] crossRef = new String[3];
			
			crossRef[0] = cr.getNamedReference().getName();
			crossRef[1] = cr.getFromNode().getNodeId();
			crossRef[2] = cr.getToNode().getNodeId();
			
			crossRefs[i++] = crossRef;
		}
		
		ser.crossRefs = crossRefs;
		
		return super.externalize(nodeSer);
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		CrossReferenceableNodeImpl newInstance = 
			(CrossReferenceableNodeImpl)(super.clone(cloneMap, cloneParent));
			
		newInstance.crossReferences = cloneNodeSet(crossReferences, cloneMap, newInstance);
		
		newInstance.crCreationListeners = new Vector<CrossReferenceCreationListener>(crCreationListeners);
		newInstance.crDeletionListeners = new Vector<CrossReferenceDeletionListener>(crDeletionListeners);
		
		/*
		// Build equivalent CrossReferences, linking newInstance to each cross
		// Node via the NamedReference.
		
		newInstance.crossReferences = new TreeSetNullDisallowed<CrossReference>();
		
		for (CrossReference cr : crossReferences) try
		{
			CrossReferenceable fromNode = cr.getFromNode();
			CrossReferenceable toNode = cr.getToNode();
			NamedReference nr = cr.getNamedReference();
			
			if (this == fromNode) nr.link(newInstance, toNode);
			if (this == toNode) nr.link(fromNode, newInstance);
		}
		catch (ParameterError ex) { throw new RuntimeException(ex); }
		*/
		
		return newInstance;
	}
}

