/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.ser.*;
import expressway.server.PersistentNode.*;
import expressway.server.Event.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import expressway.server.NamedReference.CrossReferenceable;
import expressway.geometry.*;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.io.*;
import java.util.Date;
import java.util.Set;
import java.util.SortedSet;
import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;


/**
 * ModelElements represent a structural model that describes (1) the components
 * (elements) of a model and (2) the relationships between those components.
 * This is a very abstract model, and is intended to be sufficient for
 * representing models in many different domains, including business
 * process modeling, software architecture, and economic modeling of
 * business concepts.
 *
 * None of these methods manage transactions. It is assumed that the caller
 * performs transaction management.
 */

public interface ModelElement extends PersistentGraphicNode, CrossReferenceable
{
  /* ***************************************************************************
	 * *************************************************************************
	 * Behavioral methods.
	 */
	
	/** Set flag indicating that this Element is being accessed by an active
		simulation. Set flag for all sub-Elements as well. If _this_ ModelElement
		is already locked, then throw a CannotObtainLock (this aspect is not
		recursive). */
	void lockForSimulation(boolean lock)
	throws
		CannotObtainLock;  // if this ModelElement is already locked.
	
		
	/** Return the state of the flag set by lockForSimulation(boolean). */
	boolean isLockedForSimulation();
	
	
	/** Setter: just set the field. */
	void setLockedForSimulation(boolean locked);
	

	/**
	 * Retrieve the ModelElement named by 'qualifiedName'.
	 */

	ModelElement findModelElement(String qualifiedName)
	throws
		ParameterError;  // if the qualifiedName is mal-formed.


	/**
	 * Same as findModelElement(String), but the name is split into its period-
	 * separated parts. The number of parts may be zero.
	 */
	 
	ModelElement findModelElement(String[] pathParts)
	throws
		ParameterError;  // if the qualifiedName is mal-formed.
	
	
	/** Shift the location of each child Node by the specified amount. */
	void shiftAllChildren(double dx, double dy);
	
		
	ModelDomain getModelDomain();
	
	
	ModelAttribute getModelAttribute(String name);
	
	
	SortedSet<ModelAttribute> getModelAttributes();
	
	
	ModelAttribute createModelAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	
	
	ModelAttribute createModelAttribute(PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	
	
	ModelAttribute createModelAttribute(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;


	/** ************************************************************************
	 * Find the first occurrence of a Model Element with the specified name,
	 * beginning with this Model Element and traversing downward through all
	 * nested Elements. Nested Elements include non-"child" Elements such as
	 * Attributes.
	 */
	 
	ModelElement findFirstNestedElement(String name)
	throws
		ElementNotFound,
		ParameterError;
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Diagnostic methods.
	 */

	void dump();


	void dump(int indentation);
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Template interfaces.
	 */

		
	interface ModelTemplate extends Template
	{
	}


	
  /* ***************************************************************************
	 * *************************************************************************
	 * Interfaces for simulation-time objects.
	 */

		
	/**
	 * A Model Element that contains or references Simulation Runs and therefore
	 * has a distribution with respect to each of the States of those Runs.
	 */

	public interface DistributionOwner extends ModelElement
	{
		/**
		 * The SimulationRuns on which this Element's statistics are based.
		 * These comprise a 'statistical context'.
		 */
		 
		SortedSet<SimulationRun> getSimulationRuns();
		
		
		/**
		 * The Model Scenario that was simulated by this DistributionOwnwer's
		 * SimulationRuns.
		 */
		 
		ModelScenario getModelScenario();
		
		
		/**
		 * Retrieve the final value of the specified State within this
		 * DistributionOwner, based on all SimulationRuns for the DistributionOwner.
		 * The values are returned in sequence, with the most recent last.
		 */

		Serializable[] getStateFinalValues(State state)
		throws
			ParameterError;  // if the state cannot be found, or its final
				// value has not yet been determined.
		
		
		/**
		 * Return the Distribution for the final value of the specified State.
		 * This is computed based on the SimulationRuns that exist
		 * for the Element.
		 */
		 
		ContinuousDistribution getDistributionOfFinalValueFor(State state)
		throws
			ModelContainsError,  // If a Distribution cannot be computed because
				// the data is incompatible with a well formed Distribution.
			ElementNotFound;  //  If the Distribution cannot be computed because
				// there are no Simulation Runs.
		
		
		/**
		 * Return the mean of the final value of the specified State, using all
		 * Simulation Runs for this Element.
		 */
		 
		double getMeanFor(State state)
		throws
			ModelContainsError,  // if the State's value is non-Numeric.
			ElementNotFound;  // if there are no SimulationRuns and so the mean
				// cannot be computed.
		

		/**
		 * Return the variance of the final value of the specified State, using all
		 * Simulation Runs for this Element.
		 */
		 
		double getVarianceFor(State state)
		throws
			ModelContainsError,  // if the State's value is non-Numeric.
			ElementNotFound;  // if there are no completed SimulationRuns and so
				// the mean cannot be computed.
		
		
		
		/**
		 * Compute the 'assurance' by computing the 'assuranceExpression' as defined
		 * by the Attribute of type Types.State.assuranceExpression.
		 * Use all Simulation Runs that exist for this Element.
		 */
		 
		double getAssuranceFor(State state)
		throws
			ModelContainsError;  // if there is more than one Attribute of type
				// Types.State in the Model Domain, or if the 'assuranceExpression'
				// computation resulted in an error.
		
		
		double getNetValueMean()
		throws
			ModelContainsError,
			ElementNotFound;
		
		
		double getNetValueVariance()
		throws
			ModelContainsError,
			ElementNotFound;
		
		
		double getNetValueAssurance()
		throws
			ModelContainsError,
			ElementNotFound;
			
			
		/**
		 * Refresh any cached statistical calculations that depend on the distribution,
		 * and notify clients of the change.
		 */
			
		void updateStatistics();
	}


	/**
	 * A container for a set of PredefinedEvents and Attribute values. Represents
	 * a conceptual set of initial conditions or expected external events for 
	 * simulation.
	 */

	public interface ModelScenario extends ModelElement, HierarchyScenario,
		Hierarchy,  // so it can own SimulationRuns
		DistributionOwner,
		ModelScenarioChangeListener,
		ModelScenarioSetChangeListener
		//, SimulationRunChangeListener, StateChangeListener
	{
		void setBoundScenarios(Set<ModelScenario> scens);
		
	
		/** Accessor. */
		Set<ModelScenario> getBoundScenarios();
		
		
		void setBindingScenarios(Set<ModelScenario> scens);
		
		
		/** Accessor. */
		Set<ModelScenario> getBindingScenarios();
		
		
		/** Determine which Scenario is bound to this Scenario for the specified
			Domain. Return null if none. */
		ModelScenario getBoundScenarioForDomain(ModelDomain domain)
		throws
			ParameterError;  // if the Domain is not the Domain of any bound Scenario.
			
		
		/** Simply add a Scenario. Do not verify whether 's' is actually a Scenario
			of a Domain of a bound State. */
		void addBoundScenario(ModelScenario s);
		
		
		/** Simply remove the Scenario if it is present. */
		void removeBoundScenario(ModelScenario s);
		
		
		/**
		 * Create a new Model Scenario Set, specifying the Attribute that should
		 * be varied over the Scenarios in the Scenario Set. Create the constituent
		 * Model Scenarios as well, based on this Scenario: one for each value in 'values'.
		 * The name provided is use as the name of the new ScenarioSet, as long as
		 * the name is unique within the immediate scope of the owning Model Domain;
		 * otherwise, a unique name is created. If name is null, then a unique
		 * name is created.
		 */
		 
		ModelScenarioSet createModelScenarioSet(String name, ModelAttribute attr, 
			Serializable[] values)
		throws
			ParameterError,
			CloneNotSupportedException;
		

		/**
		 * Return the ModelScenarioSet that this ModelScenario is a member of.
		 * May be null.
		 */
		 
		ModelScenarioSet getModelScenarioSet();
		
		
		/** Accessor. */
		void setModelScenarioSet(ModelScenarioSet scenSet);
		
		
		/**
		 * Return the SimulationRun of the specified name, or return null
		 * if it does not exist within this ModelScenario.
		 */
		 
		SimulationRun getSimulationRun(String name);


		/**
		 * Return a List of the SimulationRuns owned by this ModelScenario.
		 */
		 
		SortedSet<SimulationRun> getSimulationRuns();
		
		
		boolean hasSimulationRuns();
		
		
		/** Identify all of the SimulationRuns that are 'dependent' on this
			ModelScenario. A SimulationRun is dependent on a Scenario if a change
			to an Attribute value in the Sceanrio would invalidate the SimulationRun.
			Thus, when discussing Scenarios, dependency is with respect to Attribute
			value changes. */
			
		Set<SimulationRun> getDependentSimulationRuns();
		

		boolean dependentSimulationRunsExist();
		
		
		/** Delete all of the SimulationRuns that are 'dependent' on this
			ModelScenario. Return the dependent Scenarios (including this one). */
			
		Set<ModelScenario> deleteDependentSimulationRuns();
		
		
		Set<PredefinedEvent> getPredefinedEvents();


		void addPredefinedEvent(PredefinedEvent event);


		void deletePredefinedEvent(PredefinedEvent event);
		
		
		/**
		 * Create and return a ValueHistoryFactory for this Scenario.
			A simulation can use one of two approaches:
			(1) Select SimulationRuns of the bound Scenarios, in sequence, or
			(2) Use the average of each State, averages across all simulations
				of the bound Scenario.
		 *
		 * If 'averages'
		 * is true, then the values in the State values in the Value Histories
		 * returned will be averages over all SimulationRuns for the bound Scenario
		 * of the State. Otherwise, the SimulationRuns for a bound Scenario will
		 * be chosen in sequence when producing a ValueHistory.
		 */
		 
		ValueHistoryFactory getValueHistoryFactory(boolean averages,
			long startTime, long deltaTime, long endTime)
		throws
			ParameterError,  // if the history cannot be determined for any reason.
			UndefinedValue,
			ModelContainsError;
			

		/** Return the type of external State binding to use when simulating this
			Scenario. A value of true indicates that 'averages' should be used. */
			
		boolean getExternalStateBindingType();
		
		void setExternalStateBindingType(Boolean averages);

		
		/** Return the time interval (in ms) to use for obtaining the value of
			external bound States, when simulating. */
			
		TimePeriodType getExternalStateGranularity();
		
		
		void setExternalStateGranularity(TimePeriodType periodType);
		
		
		/**
		 * Create a new SimulationRun for simulation under this ModelScenario.
		 */

		SimulationRun createSimulationRun(
			String simRunName,
			ValueHistory externalStateValues,
			ModelScenario modelScenario,
			ModelDomain modelDomain,
			SimCallback callback, Date startingEpoch, Date finalEpoch,
			int iterationLimit, long duration, long randomSeed
			);


		/**
		 * Remove all of the SimulationRuns (if any) belonging to this Model Scenario.
		 * Do this in a consistent way, removing all objects owned by each Run, and
		 * nullifying aliases to all deleted objects. Delete applicable SimRunSets
		 * as well.
		 */
		 
		void deleteSimulationRuns();
		
		
		/**
		 * 
		 */
		 
		void deleteSimulationRun(SimulationRun simRun) throws ParameterError;
		
		
		SimRunSet getSimRunSet(String name);
		
		
		SortedSet<SimRunSet> getSimRunSets();
		
		
		SimRunSet createSimRunSet(Date startDate, Date endDate, long duration, int iterLimit);
		
		
		void addSimRunSet(SimRunSet simRunSet);
		
		
		void deleteSimRunSet(SimRunSet simRunSet);
		//void removeSimRunSet(SimRunSet simRunSet);
		
		
		/**
		 * Identify the States tagged with an Attribute of the specified type. 
		 * If an Attribute's value is of type Class, then the value itself
		 * (that class) is compared with 'type'. If 'onlyOne' is true, there may
		 * be only one State within the Model Scenario that is tagged with an
		 * Attribute of that type.
		 */
			
		SortedSet<State> identifyTaggedStates(Class type, boolean onlyOne)
		throws
			ModelContainsError;  // if more than one State is tagged
		
		
		/**
		 * Clone this ModelScenario for the purpose of creating a new, initialized
		 * ModelScenario. The criteria for cloning is that a deep copy is made for
		 * any elements that are owned by the scenario, and references are merely
		 * copied for other elements. However, an element is not copied if the element's
		 * value should be unique for the scenario. (See the CloneForSimulation pattern
		 * in the Expressway Product Architecture.)
		 */

		ModelScenario cloneForSimulation()
		throws
			CloneNotSupportedException;  // if unable to clone the scenario, e.g.,
				// if an attribute value cannot be copied.
				
				
		/** The simulated time at which a simulation of these Scenarios should start.
			If null, simulation starts at the current real time. A value for a
			SimulationRun overrides this default. */
		Date getStartingDate();
		
		void setStartingDate(Date newDate);
		
		/** The simulated time past which simulation is not permitted to progress. If
			null, the simulation may progress until some other condition causes
			it to stop. A value for a SimulationRun overrides this default. */
		Date getFinalDate();
		
		void setFinalDate(Date newDate);
		
		/** The maximum simulation elapsed time. If zero, this is ignored.
			A value for a SimulationRun overrides this default. A value of 0
			is considered to be an unspecified value. */
		long getDuration();
		
		void setDuration(long dur);
		
		/** The maximum number of epochs (iterations) allowed for each simulation
			A value for a SimulationRun overrides this default. A value of 0
			is considered to be an unspecified value. */
		int getIterationLimit();


		/**
		 * Set a limit on the maximum number of times that a simulation of
		 * this scenario's ModelDomain may loop. A value <= implies no limit.
		 */

		void setIterationLimit(int limit);
		

		/** If this Scenario has a starting Date, return that. Otherwise, if this
			Scenario belongs to a ScenarioSet, return its starting Date. Otherwise
			return null. */
		Date getApplicableStartingDate();
		
		
		/** If this Scenario has an ending Date, return that. Otherwise, if this
			Scenario belongs to a ScenarioSet, return its ending Date. Otherwise
			return null. */
		Date getApplicableFinalDate();
		
		
		/** If this Scenario has a non-zero duration set, return that. Otherwise, if this
			Scenario belongs to a ScenarioSet, return its duration. Otherwise
			return 0. */
		long getApplicableDuration();
		
		
		/** If this Scenario has a non-zero iteration limit, return that. Otherwise, if this
			Scenario belongs to a ScenarioSet, return its iteration limit. Otherwise
			return 0. */
		int getApplicableIterationLimit();


		/** Determine which Scenario of the specified State's Domain is bound to
			this Scenario. If none, return null. */
		Scenario getBoundScenarioForState(State state)
		throws
			ParameterError;  // if the State is not a bound State.
	}
	
	
	/**
	 * An aggregator for Scenarios. The Scenarios in a Scenario Set differ
	 * only in their value for a specified Attribute.
	 */
	 
	interface ModelScenarioSet extends ModelElement, HierarchyScenarioSet,
		Hierarchy,
		ModelScenarioChangeListener
		//, SimulationRunChangeListener, StateChangeListener
	{
		List<ModelScenario> getModelScenarios();
		
		
		/** Return the type of external State binding to use when simulating this
			Scenario. A value of true indicates that 'averages' should be used. */
			
		boolean getExternalStateBindingType();

		
		/** Return the time interval to use for obtaining the value of
			external bound States, when simulating. */
			
		TimePeriodType getExternalStateGranularity();
		
		
		/**
		 * Compute the assurance of each Model Scenario in the Set by calling
		 * each Scenario's computeAssurance() method.
		 */
		 
		double[] computeAssurances()
		throws
			ModelContainsError;
		
		
		/**
		 * Return the Distribution for the mean of the specified State.
		 * This is computed based on the set of means that are computed
		 * for the Scenarios in the Scenario Set. The implementation is allowed
		 * to cache Distributions, as long as they are refreshed when they become
		 * obsolete (e.g., if more Simulation Runs are created).
		 */
		 
		ContinuousDistribution getDistributionOfMeanFor(State state)
		throws
			ModelContainsError,  // If a Distribution cannot be computed because
				// the data is incompatible with a well formed Distribution.
			ElementNotFound;  //  If the Distribution cannot be computed because
				// there are no Simulation Runs.
		

		/**
		 * Clear any values that this ScenarioSet has cached based on SimulationRuns
		 * that it references. This is commonly called if one of those SimulationRuns
		 * is deleted.
		 */
		 
		void resetCachedStatistics();


		/** The simulated time at which a simulation of these Scenarios should start.
			If null, simulation starts at the current real time. A value for a
			Scenario overrides this default. */
		Date getStartingDate();
		
		void setStartingDate(Date d);
		
		
		/** The simulated time past which simulation is not permitted to progress. If
			null, the simulation may progress until some other condition causes
			it to stop. A value for a Scenario overrides this default. */
		Date getFinalDate();
		
		void setFinalDate(Date d);
		
		
		/** The maximum simulation elapsed time. If zero, this is ignored.
			A value for a Scenario overrides this default. A value of 0
			is considered to be an unspecified value. */
		long getDuration();
		
		void setDuration(long dur);
		
		
		/** The maximum number of epochs (iterations) allowed for each simulation
			A value for a Scenario overrides this default. A value of 0
			is considered to be an unspecified value. */
		int getIterationLimit();
		
		void setIterationLimit(int iterLim);
		

		/** If this Scenario Set has a starting Date, return that. Otherwise
			return null. */
		Date getApplicableStartingDate();
		
		
		/** If this Scenario has an ending Date, return that. Otherwise
			return null. */
		Date getApplicableFinalDate();
		
		
		/** If this Scenario has a non-zero duration set, return that. Otherwise
			return 0. */
		long getApplicableDuration();
		
		
		/** If this Scenario has a non-zero iteration limit, return that. Otherwise
			return 0. */
		int getApplicableIterationLimit();
	}
	
	
	public interface ValueHistoryFactory
	{
		ValueHistory getValueHistory()
		throws
			ParameterError,
			UndefinedValue,
			ModelContainsError;
	}
	
	
	/**
	 * 
	 */
	 
	public interface ValueHistory
	{
		long getStartTime();
		long getDeltaTime();
		long getEndTime();
		
		/** Return the starting time for the specified interval. Interval numbering
			starts at 0. */
		long getStartTimeForInterval(int interval);
		
		
		Set<String> getStateIds();
		
		EventSer[] getEventSersForStateId(String stateId);
		
		
		/** The first Event in the time period is the most
			recent Event at the start of the time period - even if that Event occurred
			before the time period. */
		
		Serializable getMostRecentRisingValueAtTime(State state, long simulatedTime)
		throws
			ParameterError;  // if the specified State is not found.
			
		ValueHistory makeCopy() throws CloneNotSupportedException;
	}
	
	
	public interface ValueHistoryArchiveWriter
	{
		void writeToArchive(ValueHistory vh)
		throws
			IOException;
	}


	public interface ValueHistoryArchiveReader
	{
		ValueHistory readFromArchive()
		throws
			IOException;
	}


	/**
	 * A container for the results of a simulation of a ModelDomain. The results
	 * consist of a set of Events, each associated with an Activity State. Also,
	 * defines the entry point for simulation: the run method. That is, the
	 * simulation engine delegates simulation to an implementation of this
	 * interface. It is assumed that the simulation engine will use a ServiceThread
	 * for running a SimulationRun, thereby allowing access to the ServiceContext.
	 */

	public interface SimulationRun extends ModelElement, SimRunSetChangeListener,
		Runnable
	//public interface SimulationRun extends PersistentNode, Runnable
	{
		String getName();
		
		
		/** Create an Id for a new Event. The Id must be unique within the set
			of all Events for this SimulationRun. */
			
		long createNewEventId();
		
		
		/**
		 * Return the native implemetation that is associated with the specified
		 * ModelComponent in this SimulationRun. If there is none, return null.
		 */
		 
		NativeComponentImplementation getNativeImplementation(ModelComponent comp);
		

		/**
		 * Retrieve the ModelScenario that owns this SimulationRun.
		 */

		ModelScenario getModelScenario();
		
		
		/**
		 * Retrieve the value of the specified State (of another Domain), as it
		 * existed at the specifed simulated time. The value is the value that
		 * this SimulationRun should use during simulation, and might represent
		 * an average or a sampled value. For pulse Events, the most recent
		 * rising value is used.
		 */
		 
		Serializable getExternalStateValueAtTime(State externalState, long simTime)
		throws
			ParameterError;  // if the specified State is not found to be bound
				// to an Attribute in this SimulationRun's Domain.
		
		
		/**
		 * Return the SimRunSet that this SimulationRun is a member of. May be null.
		 */
		 
		SimRunSet getSimRunSet();
		
		
		/** Accessor. */
		void setSimRunSet(SimRunSet simRunSet);
		
		
		/**
		 * Return the RandomGenerator to be used for this Simulation Run.
		 * The Random Generator should be set by the Simulation Run's constructor.
		 */
		 
		RandomGenerator getRandomGenerator();

		
		/**
		 * Return a List of each iteration. Each element in the List is a SortedSet
		 * of the SimulationTimeEvents for that iteration.
		 */
		 
		List<SortedEventSet<SimulationTimeEvent>> getEventsByIteration();

		/**
		 * Retrieve a List of the GeneratedEvents for this SimulationRun.
		 * This list should be assumed to be a copy and should not be modified.
		 */

		List<GeneratedEvent> getGeneratedEvents();


		/**
		 * Retrieve a List of the GeneratedEvents for this SimulationRun for
		 * for the specified State.
		 * This list should be assumed to be a copy and should not be modified.
		 */

		List<GeneratedEvent> getGeneratedEvents(State state);


		/**
		 * Retrieve a List of the Events for this SimulationRun.
		 * This list should not be assumed to be a copy and should not be modified.
		 */

		List<SimulationTimeEvent> getEvents();


		/**
		 * Retrieve a List of the Events for this SimulationRun for the
		 * specified State.
		 * This list should not be assumed to be a copy and should not be modified.
		 */

		List<SimulationTimeEvent> getEvents(State state);
		
		
		/**
		 * Retrieve a List of the Events that have occurred for this SimulationRun
		 * for the specified State, but only those Events that fall within the inclusive
		 * interval specified. Include the most recent Event that occurs at OR
		 * BEFORE the start time.
		 * The Events in this list should not be assumed to be copies and should
		 * not be modified.
		 */

		List<SimulationTimeEvent> getEvents(State state, long startTime, long endTime);


		/**
		 * Get the first Event for the specified State, or return null if there
		 * is not one.
		 */
		 
		GeneratedEvent getFirstEvent(State state);
		
		
		/**
		 * Insert the specified Event into this SimulationRun's Event Sets.
		 * Used when reconstructing a SimulationRun from a file. Not used during
		 * simulation.
		 */
		 
		void reAddEvent(SimulationTimeEvent event);
		
		
		/**
		 * Retrieve a List of the simulation time epochs that were simulated. The
		 * list is in ascending order of time, and in the cause-effect sequence
		 * in which each epoch occurred, even if two epochs occurred at the
		 * same simulation time value. (If two epochs occured at the same simulation
		 * time, because one preceded the other by infinitesimal time, then their
		 * time would be identical, but they would still be independent epochs, and
		 * so each would have its own entry in this list.)
		 */

		List<Epoch> getEpochs();


		/**
		 * Return a List of all of the iterations that occurred during the
		 * Simulation Run. Note that an iteration is merely an epoch, but
		 * measured by its ordinal sequence instead of by its date.
		 */
		 
		List<Integer> getIterations();
		
		
		/**
		 * Return the number of milliseconds that have elapsed since start of
		 * simulation, in simulated time, by the specified iteration.
		 */
		 
		long getElapsedMs(int iteration)
		throws
			ParameterError;  // if the specified iteration has not yet occurred.
		
		
		/**
		 * If an Epoch for the specified iteration exists, return it. Otherwise,
		 * create it, but only if the specified iteration will be the next in 
		 * sequence in the List of Epochs. This method is intended to be used 
		 * when reconstructing a SimulationRun from an archive.
		 */
		 
		Epoch getOrAppendEpochForIteration(int iteration, long ms)
		throws
			IndexOutOfBoundsException;
		
		
		Epoch getEpochForIteration(int iteration)
		throws
			ParameterError;
		
		
		/**
		 * Return a List of the Events that occurred during the specified
		 * simulation iteration. Iteration 1 is the first iteration and it is
		 * the iteration in which Startup Events occur. There is no "iteration 0".
		 * Note that all Simulation Time Events are included (including Startup
		 * Events).
		 */
		 
		SortedEventSet<SimulationTimeEvent> getEventsAtIteration(int iteration)
		throws
			ParameterError;  // if there are no events at that iteration.
		
		
		/**
		 * Same as getEventsAtIteration, but does not include Startup Events.
		 */
		 
		SortedEventSet<GeneratedEvent> getGeneratedEventsAtIteration(int iteration)
		throws
			ParameterError;


		/**
		 * Retrieve a List of all of the errors that resulted during this
		 * SimulationRun, in ascending order of time. (The sequence within a
		 * time epoch is undefined.)
		 */

		List<Exception> getErrors();


		/**
		 * Return true if this SimulationRun completed without fatal errors.
		 */

		boolean completedNormally();


		/**
		 * Return the value of the specified State as it existed at the specified
		 * simulation-time epoch.
		 */

		Serializable getStateValueAtEpoch(State state, Epoch epoch);
		
		
		/**
		 * Return the sequence of values that the specified State had at the
		 * specified time, which might span multiple epochs. The values are
		 * returned in the order in which they occurred.
		 */
		 
		Serializable[] getStateValuesAtDate(State state, Date date);


		/**
		 * Return a Map of the final values of all Activity States. Only driven
		 * States (States that received values) are included.
		 */

		Map<State, Serializable> getFinalStateValues();


		/**
		 * Set the current value of the specified State. This should be used
		 * to initiatlize States at simulation startup.
		 */
		 
		void setInitialStateValue(State state, Serializable initValue)
		throws
			ParameterError;
			
			
		/**
		 * Retrieve the final simulation value of the specified State.
		 */

		Serializable getStateFinalValue(State state)
		throws
			ParameterError;  // if the value is indeterminate, e.g., because
				// the simulation has not completed.
		
		
		/**
		 * Return the average (sum((value)*(deltaT))/T) of the specified State's
		 * value for the specified
		 * time period T. If the State generates pulse Events, ignore the State's
		 * compensation Events. Return in isPulseRef[0] an indication of whether
		 * the Events are pulse Events. Return in averageDelayRef[0] the average
		 * time (ms) between the Events in the period.
		 * If the State has a null value at any time during the period, then
		 * its average value is treated as being null (undefined), and the
		 * averageDelay returned will be 0. If the State has a value that is
		 * not a Number, then an average cannot be computed.
		 */
		 
		Serializable getAverageRisingValueForPeriod(State state, long startTime,
			long endTime, boolean[] isPulseRef, long[] averageDelayRef)
		throws
			UndefinedValue,  // if there is a null valued Event during the period.
			ModelContainsError;  // if an average cannot be computed because
				// if pulse Events are mixed with flow Events or if there is
				// an Event with a non-Number value.

		
		/**
		 * Compute the average value of the specified State, where the average is
		 * weighted by the time that the State exists at each value that it has
		 * during the specified time interval (i.e., compute a time-based
		 * average). The time values are the long value of the Java Date.
		 */
		
		double computeTimeAveragedValue(State state, long startTime, long endTime)
		throws
			UndefinedValue,  // if the startTime and endTime are outside the
				// simulated time, 
			ParameterError;  // if the specified State is not in the Model,
				// if the State is not a Number type, or if startTime
				// equals endTime.
		
		
		/**
		 * Return the last Event that occurs for the specified State at or before
		 * the specified time. If no such Event is found, return null.
		 */
				
		GeneratedEvent getDrivingEventAt(State state, long time);

		/**
		 * Create a new GeneratedEvent for the specified State and simulation time,
		 * with the specified future value.
		 *
		 * May only be called during simulation.
		 */

		GeneratedEvent generateEvent(State state, Date time, Serializable newValue,
			SortedEventSet<GeneratedEvent> triggeringEvents)
		throws
			ParameterError; // if the specified time is earlier than
				// the current time.


		/**
		 * Retrieve the State that is currently (in the current epoch) driving
		 * the specified Port. Returns null if the Port is not driven.
		 */

		State getCurrentDriver(Port port);
		
		
		/**
		 * Return the current value for the specified State.
		 */
		 
		Serializable getStateValue(State state);


		/**
		 * Remove future events that are driven by each State that is
		 * defined within this Activity, or by any embedded Activities.
		 * Return the number of Events that are purged.
		 */
		 
		int purgeFutureEvents(Activity activity);

	
		/**
		 * Initialize Attribute values within a ModelScenario, to prepare for simulation
		 * of the scenario:
		 *	1. All Attribute default values are copied to the ModelScenario.
		 *	2. All Attributes that are bound to a State are identified, and their value
		 *		is obtained from the current ModelScenario that applies to that State,
		 *		and used to update the corresponding bound value in this
		 *		SimulationRun's ModelScenario.
		 *	3. All Decisions that impact the model (i.e., that have Variables bound to
		 *		an Attribute in the model, and for which the Variable has a Decision
		 *		that is part of a current DecisionScenario) are identified, and the
		 *		corresponding Decision value is propagated to the Attribute value
		 *		in the ModelScenario. Some of these value updates may overwrite
		 *		(take precedence over) values obtained from State bindings.
		 */

		void initializeAttributes()
		throws
			ParameterError;  // if a DecisionScenario cannot be identified.


		/**
		 * Generate a StartupEvent for each EventProducer, to occur in the first
		 * epoch. This ensures that each State is driven. Recursive, beginning
		 * with the specified ModelContainer. Return true if at least one
		 * Startup Event was created.
		 */
		
		boolean scheduleStartupEvents(ModelContainer container);


		/**
		 * Update all model-calculated Attributes. This is done by reading the final value of
		 * each State that has an AttributeStateBinding, and copying the value to the
		 * associated Attribute.
		 *

		void updateAttributes()
		throws
			ParameterError;  // if unable to copy attribute values. */
			
			
		//Set<Port> getConnectedPorts(State state)
		//throws
		//	ModelContainsError;
		
		
		// For serialization.
		long[][] getIterEventIds();
		TableEntry[] getFinalStateValuesByNodeId();
		String getModelScenarioNodeId();
		
		/** The values of external States that are referenced by the Attributes
			of the Domain being simulated. */
		ValueHistory getExternalStateValues();
		
		/** The random generator seed value used for this Simulation Run. */
		public long getRandomSeed();
		
		/** The time at which the simulation should start. If null, simulation
			starts at the current real time. */
		Date getStartingDate();
		
		void setStartingDate(Date d);
		
		/** The time past which the simulation is not permitted to progress. If
			null, the simulation may progress until some other condition causes
			it to stop. */
		Date getFinalDate();
		
		void setFinalDate(Date d);
		
		
		/** The maximum simulation elapsed time. If zero, this is ignored. 
			A value of 0 is considered to be an unspecified value. */
		long getDuration();
		
		void setDuration(long dur);
		
		
		/** The maximum number of epochs (iterations) allowed for the simulation. 
			A value of 0 is considered to be an unspecified value. */
		int getIterationLimit();
		
		void setIterationLimit(int iterLim);
		
		
		/** The status of the simulation, as to whether it completed normally or
			an error occurred. */
		boolean getCompletedNorm();
		
		/** The number of simulation epochs (iterations) that actually occurred. */
		int getActualNoOfEpochs();
		
		/** The elapsed time in the simulation virtual world. */
		long getActualElapsedTime();
		
		/** The time before which the simulation is not defined. */
		long getActualStartTime();
		
		/** The time past which the simulation is no longer defined. */
		long getActualEndTime();
	}
	
	
	interface SimRunSet extends ModelElement, 
		DistributionOwner,
		SimulationRunChangeListener
	{
		SortedSet<SimulationRun> getSimulationRuns();
		
		void addSimRun(SimulationRun simRun);
		
		void removeSimRun(SimulationRun simRun);


		/** The simulated time at which a simulation of these Scenarios should start.
			If null, simulation starts at the current real time. A value for a
			SimulationRun overrides this default. */
		Date getStartingDate();
		
		void setStartingDate(Date newDate);
		
		/** The simulated time past which simulation is not permitted to progress. If
			null, the simulation may progress until some other condition causes
			it to stop. A value for a SimulationRun overrides this default. */
		Date getFinalDate();
		
		void setFinalDate(Date newDate);
		
		/** The maximum simulation elapsed time. If zero, this is ignored.
			A value for a SimulationRun overrides this default. A value of 0
			is considered to be an unspecified value. */
		long getDuration();
		
		void setDuration(long dur);
		
		/** The maximum number of epochs (iterations) allowed for each simulation
			A value for a SimulationRun overrides this default. A value of 0
			is considered to be an unspecified value. */
		int getIterationLimit();


		/**
		 * Set a limit on the maximum number of times that a simulation of
		 * this scenario's ModelDomain may loop. A value <= implies no limit.
		 */

		void setIterationLimit(int limit);
	}
	
	
	/**
	 * An interface provided by a simulation thread to allow a client to manage
	 * the simulation (asynchronously with respect to the simulation). This
	 * interface is normally (but not necessarily) implemented
	 * by the simulation thread class.
	 */
	 
	public interface SimControl extends ProgressControl
	{
		//String getName();
	}

	/**
	 * Callback interface for signaling the client on the progress of the
	 * simulation (asynchronously with respect to the client).
	 */

	public interface SimCallback extends ProgressCallback
	{
		interface AttributeValuePair
		{
			Attribute getAttribute();
			
			Serializable getValue();
		}
		
		
		interface StateValuePair
		{
			State getState();
			
			Serializable getValue();
		}
		
		
		void setSimRunNodeId(String simRunNodeId);
		

		/**
		 * To be called by the simulator to wait for verification that
		 * simulation should be started.
		 */

		boolean getConfirmationToProceed();
		
		
		void showProgress(int totalEpochsCompleted, long totalTimeElapsed);
	
	
		//void completed(SimulationRun simRun);
		
		///AttributeValuePair reportAttributeInitialization();
		
		///Date reportNewEpoch();
		
		///StateValuePair reportStateChange();
		
		///AttributeValuePair reportAttributeUpdate();
		
		
		////ActivityPeer getPeer(Activity activity);
		
		////FunctionPeer getPeer(Function function);
		
		////ConduitPeer getPeer(Conduit conduit);
		
		////PortPeer getPeer(Port port);
		
		////AttributePeer getPeer(Attribute attribute);
	}


	/* ------------------------------------------------------------------------
	 * Interfaces that extend ModelElement and that represent static model
	 * components.
	 */

	/**
	 * A self-contained model. Simulation of a ModelDomain does not require
	 * simultaneous simulation of any other ModelDomains. However, domains may
	 * affect each other through their interconnection by Decision Variables.
	 * Decision Variables are read after a ModelDomain model has been simulated.
	 */

	public interface ModelDomain extends ModelContainer, HierarchyDomain, AttributeChangeListener,
		AttributeStateBindingChangeListener, ModelComponentChangeListener,
		DecisionElement.VariableAttributeBindingChangeListener
	{
		final TimePeriodType DefaultTimePeriodForExternalStates = TimePeriodType.monthly;
		
		
		/** Create a new ModelDomain that is a complete copy of this one, except
			for SimulationRuns. A unique name is created for the new Domain. */
			
		ModelDomain createIndependentCopy()
		throws
			CloneNotSupportedException;
		
		
		/** Return the external States (in other Domains) that are bound to this
			ModelDomain via an AttributeStateBinding. */
			
		Set<State> getBoundStates();
		
		
		/** Return the Domains that own Attributes that are bound to States of 
			this Domain. */
		
		Set<ModelDomain> getBindingDomains();
		
		
		/** Return the Domains that own States to which an Attribute of this
			Domain is bound. */
			
		Set<ModelDomain> getBoundDomains();
		
		
		boolean getExternalStateBindingType();
		
		
		void setExternalStateBindingType(Boolean averages);
		
		
		TimePeriodType getExternalStateGranularity();
		
		
		void setExternalStateGranularity(TimePeriodType periodType);


		/** Return true if this ModelDomain owns ModelScenarios that own
			SimulationRuns. */
			
		boolean hasSimulationRuns();
		
		
		/** Identify all of the SimulationRuns that are 'dependent' on this
			ModelDomain. A SimulationRun is dependent on a ModelDomain if a change
			to the Domain would invalidate the Run. Thus, when discussing ModelDomains,
			dependency is with respect to model (Domain) changes. */
			
		Set<SimulationRun> getDependentSimulationRuns();


		/** Return true if (1) there are SimulationRuns for this ModelDomain, or
			(2) there are SimulationRuns for any higher level models that
			bind to this Domain. */
			
		boolean dependentSimulationRunsExist();
		
		
		/** Delete all of the SimulationRuns that are 'dependent' on this
			ModelDomain. Return the dependent Domains (including this one). */
			
		Set<ModelDomain> deleteDependentSimulationRuns();
		
		
		List<ModelScenario> getModelScenarios();
		
		
		ModelScenario getModelScenario(String name)
		throws
			ElementNotFound, // if the ModelScenario cannot be found.
			ParameterError;
		
		
		void addModelScenario(ModelScenario scenario);
		
		
		ModelScenarioSet getModelScenarioSet(String name)
		throws
			ElementNotFound,
			ParameterError;
			
			
		/**
		 * Return this Domain's ModelScenarioSets.
		 */
		 
		List<ModelScenarioSet> getModelScenarioSets();
		
		
		void addModelScenarioSet(ModelScenarioSet s);
		
		
		/**
		 * Retrieve the ModelScenario that the user considers to be the "current"
		 * one at this moment. I.e., the user desires that the current scenario be
		 * used when simuation is performed on the ModelDomain, or when the
		 * simulator needs to obtain the value of a State from another ModelDomain
		 * and it needs to determine which ModelScenario to choose for that
		 * other ModelDomain.
		 * May return null.
		 */

		ModelScenario getCurrentModelScenario();


		void setCurrentModelScenario(ModelScenario scenario)
		throws
			ElementNotFound,
			ParameterError;


		/**
		 * Return a Set of VariableAttributeBindings that identifies all
		 * Variables within DecisionDomains that are bound to Attributes in
		 * this ModelDomain. This is not an owning set.
		 */

		Set<DecisionElement.VariableAttributeBinding> getVariableAttributeBindings();


		/**
		 * Add the specified binding to this ModelDomain's set of known
		 * VariableAttributeBindings. Note that this Set is not an owning set.
		 */

		void addVariableAttributeBinding(DecisionElement.VariableAttributeBinding binding);


		Set<AttributeStateBinding> getAttributeStateBindings();


		/**
		 * Retrieve a Set of the native implementation classes of this ModelDomain's
		 * ModelComponents.
		 */

		//Set<Class> getNativeComponentImplementationClasses();
		
		
		/**
		 * Return the Set of ModelComponents in this Domain that have native
		 * implementations.
		 */
		 
		Set<ModelComponent> getComponentsWithNativeImpls();
		
		
		/**
		 * 
		 */
	
		Set<Port> determineReachablePorts(Set<Conduit> conduits);
		

		/**
		 * 
		 */
	
		Set<Port> determineReachablePorts(State state)
		//Set<Port> determineConnectedPorts(State state)
		throws
			ModelContainsError; // when a model conflict of any kind is detected.
			
			
		/**
		 * Identify all of the Ports that are sensitive to 'state'. A Port is
		 * sensitive to a State if an Event on the State (1) may reach the Port,
		 * (2) may pass into (or be reflected back into) the Port's Component, and
		 * (3) if the Component has native behavior.. 
		 * The result includes Ports that are bound to the State.
		 */
	
		Set<Port> determineSensitivePorts(State state)
		//Set<Port> determineConnectedPorts(State state)
		throws
			ModelContainsError; // when a model conflict of any kind is detected.
			
			
		/**
		 * Identify all of the Conduits that can conduct Events for the specified
		 * State, based on their connectivity and Port direactions. This is
		 * equivalent to determining the Conduits that are reachable.
		 */
	
		Set<Conduit> determineSensitiveConduits(State state)
		throws
			ModelContainsError; // when a model conflict of any kind is detected.
			
			
		/**
		 * 
		 */
		 
		Set<Conduit> determineSensitiveConduits(Port port);
		
		
		/**
		 * 
		 */
		 
		Set<Conduit> determineSensitiveConduits(Set<Port> ports);
		
		
		/**
		 * Identify all of the Functions that are lowest-level (behavioral) and
		 * that are sensitive to the specified State. If there are no such
		 * Functions, then return an empty Set. Note that a Function is not
		 * considered to be sensitive to a State that it owns.
		 * 
		 * A behavioral component C is sensitive to a change in State value if 
		 * (and only if) there is a path (via Conduits) from an output-capable 
		 * (output or bi) bound Port of the State owner to an input-capable
		 * (input or bi) Port of C. In the case in which C is the State owner, 
		 * the two Ports must not be the same.
		 */
	
		Set<Function> determineSensitiveFunctions(State state)
		throws
			ModelContainsError; // when a model conflict of any kind is detected.
			
			
		/**
		 * Identify all of the Activities that are lowest-level (behavioral) and
		 * that are sensitive to the specified State. If there are no such
		 * Activities, then return an empty Set. Note that an Activity is not
		 * considered to be sensitive to a State that it owns.
		 * 
		 * A behavioral component C is sensitive to a change in State value if 
		 * (and only if) there is a path (via Conduits) from an output-capable 
		 * (output or bi) bound Port of the State owner to an input-capable
		 * (input or bi) Port of C. In the case in which C is the State owner, 
		 * the two Ports must not be the same.
		 */
	
		Set<Activity> determineSensitiveActivities(State state)
		throws
			ModelContainsError; // when a model conflict of any kind is detected.
		
		
		/**
		 * Perform a discrete event simulation of this ModelDomain, until the
		 * specified epoch has passed or until there are no more future
		 * events to processes - whichever comes first. If finalEpoch is
		 * null, only the latter condition applies.
		 *
		 * This method does not block. When the SimulationRun is complete
		 * it calls the callback handler. The callback object is used by the
		 * simulation thread to send results to the caller, and the caller uses
		 * the callback object to send commands (such as quit) to the simulation
		 * thread.
		 *
		 * This method should copy the state of all model Attributes. The
		 * copied values should be used for simulation. This ensures isolation
		 * with respect to other simulations that are underway. However,
		 * while Attribute values may be changed, structural changes to the
		 * Elements of a ModelDomain are not allowed while a simulation is underway.
		 *
		 * The argument 'simRunSet' may be null.
		 */

		SimServiceThread simulate(
			ValueHistoryFactory valueHistoryFactory,
			SimRunSet simRunSet, // may be null
			ModelScenario modelScenario,
			SimCallback callback, Date startingEpoch,
			Date finalEpoch, int iterationLimit, long duration, long randomSeed)
		throws
			ParameterError,
			ModelContainsError;


		/**
		 * Perform a discrete event simulation of this ModelDomain, until there
		 * are no more future events to processes.
		 */

		SimServiceThread simulate(
			ValueHistoryFactory valueHistoryFactory,
			SimRunSet simRunSet, // may be null
			ModelScenario modelScenario,
			SimCallback callback, int iterationLimit, long randomSeed)
		throws
			ParameterError,
			ModelContainsError;
		
		
		/** Convenience method: retrieve all of the SimulationRuns that belong
			to all of the ModelScenarios that this Domain owns. */
			
		SortedSet<SimulationRun> getSimulationRuns();
		
		
		/**
		 * Remove the SimulationRuns (if any) owned by each Scenario (if any) of
		 * this Model Domain. Delete applicable SimRunSets as well.
		 * Do this in a consistent way, removing all objects owned by each Run, and
		 * nullifying aliases to all deleted objects.
		 */
		 
		void deleteSimulationRuns();
	}
	
	
	public interface ModelDomainMotifDef extends ModelDomain, MotifDef
	{
	}


	/**
	 * A ModelDomain contains ModelComponents and Constraints.
	 */

	public interface ModelComponent extends ModelElement
	{
		ModelContainer getModelContainer();  // the enclosing Activity, Function,
			// or ModelDomain, if any.
				
		
		/** ************************************************************************
		 * Translate the specified Point, as expressed in the coordinate space of
		 * this Node, into the coordinate space of the parent Node. If there is no
		 * parent Node, then throw a ParameterError.
		 */
		 
		Point translateToContainer(Point p)
		throws
			ParameterError;  // if there is no parent NodeContainer.
		
		
		/** ************************************************************************
		 * Translate the specified Point, as expressed in the coordinate space of
		 * this Node's parent Container, into the coordinate space of this Node.
		 * If there is no parent Node, then throw a ParameterError.
		 */
		
		Point translateFromContainer(Point p)
		throws
			ParameterError;  // if there is no parent NodeContainer.
	}


	/**
	 * Contains ModelComponents and Constraints.
	 */

	public interface ModelContainer
	extends 
		ModelComponent, Hierarchy
	{
		Set<Function> getFunctions();


		Set<Activity> getActivities();


		Set<Conduit> getConduits();


		Set<Constraint> getConstraints();
		
		
		/**
		 * Return all of the owned Nodes that implement ModelComponent.
		 */
		 
		Set<ModelComponent> getSubcomponents();
		
		
		/**
		 * Return a List of all of the States defined within this ModelContainer
		 * and its sub-Components.
		 */
		 
		List<State> getStatesRecursive();
		
		
		Rectangle getRectangle(boolean asIcon);
		
		
		boolean contains(ModelElement element);
		
		
		/**
		 * Return true if the segment beginning with segBegin and ending with
		 * segEnd overlaps any segment of any Conduit within this ModelContainer.
		 * Two segments are considered to overlap if they are less than 'tolerance'
		 * from each other at any point and their angles (in radians) are less than
		 * 'minAngle' apart.
		 */
		 
		boolean chechForConduitOverlaps(Conduit thisConduit, Point segBegin, 
			Point segEnd, double minDistance, double minRadians)
		throws
			ParameterError; // if segBegin equals segEnd.
		
		
		void rerouteAllConduits()
		throws
			ParameterError;

		
		/**
		 * Arrange this ModelContainer's Components horizontally so as to make
		 * them approxomately equidistant from each other, and centered vertically.
		 * If necessary, this ModelContainer is expanded in size to accommodate
		 * the Components. If this ModelContainer contains a Component that is
		 * not listed in the arguments, do not reposition that component and
		 * do not consider it in the calculations.
		 */
		 
		void flowComponentsHorizontally(ModelComponent... components);
		
		
		/**
		 * Arrange this ModelContainer's Components vertically so as to make
		 * them approxomately equidistant from each other, and centered horizontally.
		 * If necessary, this ModelContainer is expanded in size to accommodate
		 * the Components. If this ModelContainer contains a Component that is
		 * not listed in the arguments, do not reposition that component and
		 * do not consider it in the calculations.
		 */
		 
		void flowComponentsVertically(ModelComponent... components);
		
		
		/** Create a ModelElement of the specified kind, as a child Element of this
			ModelContainer. The created instance will be initialized with
			default field values. The kind must be a kind returned by the 
			getInstantiableNodeKinds() method. */
			
		ModelElement createSubcomponent(String kind, PersistentNode layoutBound,
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;  // if the kind cannot be instantiated in
				// this ModelContainer.
		
		
		/**
		 * Retrieve the immediate child element that has the specified name. 
		 * No two child elements may have the same name, even if they are different
		 * kinds. A child must be owned by its parent, according to the Ownership
		 * Pattern.
		 */

		ModelElement getSubcomponent(String name)
		throws
			ElementNotFound;


		/**
		 * nativeImplClass should implement Function.NativeFunctionImplementation.
		 */
		 
		Function createFunction(String name, Class nativeImplClass,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;


		Function createFunction(String name, Template template,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;


		/**
		 * Create a basic Function that can then be configured.
		 */
		 
		Function createFunction(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;


		void initFunction(Function function, Class nativeImplClass,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		

		void deleteFunction(Function function, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

		
		Terminal createTerminal(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
			

		/**
		 * @param timeDistShape If zero (0), the time distribution is treated
		 *			as determinstic.
		 * @param timeDistScale
		 * @param valueDistShape If zero (0), the value distribution is treated
		 *			as deterministic.
		 * @param valueDistScale
		 */

		Generator createGenerator(String name, boolean variable,
			double timeDistShape, double timeDistScale, double valueDistShape,
			double valueDistScale, boolean ignoreStartup, boolean pulse,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

		
		/**
		 * For creating a basic Generator that can then be configured.
		 */
		
		Generator createGenerator(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		Generator createPulseGenerator(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		/**
		 * Create a Conduit connecting the input Port and the count Port of the
		 * Generator, thereby making the Generator repeating. If there is already
		 * a Conduit connecting these, and it is named according to the naming
		 * pattern for the Conduit to be created, then do not create another and
		 * return null.
		 * If a Conduit is created, return it.
		 */
		 
		Conduit makeGeneratorRepeating(Generator gen, boolean repeating)
		throws
			ParameterError,
			ModelContainsError;


		/**
		 * Return true if there is a Conduit connecting the input Port and the
		 * count Port, and it is named according to the naming pattern for a
		 * repeat Conduit (see prior method).
		 */
		 
		boolean isGeneratorRepeating(Generator gen)
		throws
			ElementNotFound, // if the Generator is not found in this Model Container.
			ModelContainsError;
		
		
		Delay createDelay(String name, long delay, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

		
		Delay createDelay(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

		
		Sustain createSustain(String name, double timeDistShape, double timeDistScale,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

		
		Sustain createSustain(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

		
		OneWay createOneWay(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

		
		Tally createTally(String name, Number initValue, String[] inputPortNames,
			boolean doubleCount, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		Tally createTally(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		Max createMax(String name, Number initValue, String[] inputPortNames,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		Max createMax(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
	

		Switch createSwitch(String name, boolean conducting, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		Switch createSwitch(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		Modulator createModulator(String name, Double Factor, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
			
			
		Modulator createModulator(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
			
			
		DoubleExpression createDoubleExpression(String name, String expr, 
			String[] inputPortNames, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
			
			
		DoubleExpression createDoubleExpression(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
			
			
		NotExpression createNotExpression(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		Summation createSummation(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
			
			
		Discriminator createDiscriminator(String name, Double threshold, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		Discriminator createDiscriminator(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		/**
		 * nativeImplClass should implement Activity.NativeActivityImplementation.
		 */
		 
		Activity createActivity(String name, Class nativeImplClass,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;


		Activity createActivity(String name, Template template,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;


		Activity createActivity(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		void initActivity(Activity activity, Class nativeImplClass,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
			
			
		void deleteActivity(Activity activity, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

		
		Conduit createConduit(String name, Port portA, Port portB, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;


		Conduit createConduit(String name, Template template,
			Port portA, Port portB,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;


		void deleteConduit(Conduit conduit, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

		
		Satisfies createSatisfiesRelation(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;


		Derives createDerivesRelation(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		void deleteConstraint(Constraint constraint, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

		
		/** Delete the specified Model Element from its ModelContainer. */
		
		void deleteSubcomponent(ModelElement child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;  // If the element does not belong to a ModelContainer.
		
		
		// For serialization.
		
		String[] getFunctionNodeIds();
		String[] getActivityNodeIds();
		String[] getConduitNodeIds();
		String[] getConstraintNodeIds();
	}


	/** ************************************************************************
	 * A ModelContainer that may contain Ports: ModelContainers that do not
	 * implement this interface may not contain Ports.
	 */

	public interface PortedContainer extends ModelContainer, EventProducer
	{
		void setIconWidth(double w);
		
		void setIconHeight(double h);
		
		
		List<Port> getPorts();
		
		
		Port getPort(String name)
		throws
			ElementNotFound;


		Port createPort(String name, Position side, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
			
			
		Port createPort(String name, PortDirectionType direction, Position side,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
			
			
		Port createPort(String name, boolean black, PortDirectionType direction,
			Position side, PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
			
			
		Port createPort(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;


		/** ********************************************************************
		 * Remove and destroy the specified Port in this PortedContainer.
		 * Any PersistentNodes (Port change listeners) that reference this Port
		 * must be notified.
		 */
		 
		void deletePort(Port port, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;  // if the Port is not a Port for this PortedContainer.
			
		
		void setPortDirection(Port port, PortDirectionType dir)
		throws
			ParameterError;  // if the specified direction is not allowed by this Container.

		
		String[] getPortNodeIds();
		String[] getStateNodeIds();
		
		
		/**
		 * Find and return the specified State. If not found, return null.
		 */
		 
		State getState(String name);
		
		
		State createState(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		void deleteState(State state, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

		
		/**
		 * Return a List of the Ports that are on the specified side of this 
		 * Ported Container.
		 */
	 
		List<Port> getPorts(Position pos);
		
		
		/**
		 * Return the Ports of this PortedContainer that can receive input.
		 */
		 
		List<Port> getInputPorts();
		
		
		/**
		 * Return a Face of this PortedContainer that contains the specified Port.
		 * To "contain" a Port, the Port must be approximately along the edge of
		 * the Face and not on a corner. The degree of approximation is
		 * implementation-specific. If no Face of this container contains the Port,
		 * return null. If the Port is not found in this container, throw an exception.
		 * This PortedContainer is assumed to be expaned (not iconified).
		 * If 'asIcon' is true, assume that this PortedContainer is depicted as
		 * an icon.
		 */
		 
		Face getFace(Port port, boolean asIcon)
		throws
			ParameterError;  // if the Port is not found.
	}
	
	
	/**
	 * An Attribute has a value in a particular Scenario.
	 */

	public interface ModelAttribute extends ModelElement, Attribute, Cloneable,
		DecisionElement.VariableAttributeBindingChangeListener, StateChangeListener
	{
		//Object clone() throws CloneNotSupportedException;


		ModelElement getModelElement();
		
		
		/**
		 * Create and return a binding between this Attribute and the specified
		 * model Activity State. The State may not belong to the same model as
		 * this Attribute: it must represent a cross-model binding. Also,
		 * binding cycles must be detected: that is, a ParameterError must be
		 * thrown if there is a path along AttributeStateBindings back to
		 * a State in the same Domain as the Attribute.
		 */

		AttributeStateBinding bindToState(State state, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;	// if the Domain of the attribute and state are the same.
			
			
		/**
		 * Remove the specified binding. Notify listeners.
		 */
		 
		void deleteStateBinding(AttributeStateBinding binding,
			PersistentNode[] outermostAffectedRef);


		DecisionElement.VariableAttributeBinding getParameterBinding();


		void setParameterBinding(DecisionElement.VariableAttributeBinding binding);


		/**
		 * Retrieve the bindging between this Attributes and a State in
		 * another model. The Attribute will derive its value from that State.
		 */

		AttributeStateBinding getStateBinding();
	}
	
	
	public interface ModelContainerAttribute extends ModelAttribute, HierarchyAttribute
	{
	}


	public interface AttributeStateBinding extends ModelElement
	{
		ModelAttribute getAttribute()
		throws
			ValueNotSet;
		
		State getForeignState()  // a State defined in another ModelDomain.
		throws
			ValueNotSet;
	}


	/**
	 *
	 */

	public interface Constraint extends ModelComponent
	{
		ModelContainer getModelContainer();
	}


	/**
	 *
	 */

	public interface DirectedRelation extends Constraint, ModelElementChangeListener
	{
		Set<ModelElement> getSubjects();

		Set<ModelElement> getDirectObjects();
		
		String[] getSubjectNodeIds();
		String[] getDirectObjectNodeIds();
	}


	/**
	 * A ModelElement that may generate Events.
	 */

	public interface EventProducer extends ModelComponent
	{
		List<State> getStates();
		
		
		/** Return the ordinal number of the State, within this Event Producer.
			The first element is 1. */
		int getStateNo(State state);


		/**
		 * Create a new State associated with this Activity.
		 */

		State createState(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;

			
		/*
		void deleteState(State state)
		throws
			ParameterError;  // if this EventProducer does not own the State.
		*/
			

		/**
		 * Retrieve the State with the specified name. If it is not found,
		 * then return null.
		 */

		State getState(String name);
	}


	/**
	 * A function has no internal state: it mathematically transforms a
	 * set of inputs to a set of outputs. This transformation occurs
	 * in zero simulation time, i.e., instantaneously. Thus, a function
	 * generates output events that the simulator must schedule for the
	 * same time epoch that generates the events.
	 * A Function may not have a Port that is bi-directional, and must have
	 * exactly one output Port.
	 */

	public interface Function extends PortedContainer, EventProducer
	{
		/**
		 * Identify a native implementation class to use for this Function. If a
		 * native implementation class is identified, then it will be used instead
		 * of the Function's internal components (if any).
		 */

		void setNativeImplementationClass(Class implClass)
		throws
			ParameterError;  // if the implClass is not a NativeFunctionImplementation.
		
		
		/**
		 * Return the output Port for this Function.
		 */
		 
		Port getOutputPort();
		
		
		/**
		 * Return the Set of Functions that "feed" this Function. A Function f "feeds"
		 * another Function g if any input of g is reachable from the output of f.
		 * See the slide "Detecting Function Feedback".
		 */
		
		Set<Function> getFeeders();
		
		
		/**
		 * See the slide "Detecting Function Feedback".
		 */
		 
		void checkForFeedback(Set<Function> functionsChecked)
		throws
			ModelContainsError;  // if feedback is detected.


		/**
		 * Set the maximum number of times to evaluate the Function during any
		 * epoch of a simulation. This is to prevent race conditions. The value
		 * should be greater than zero.
		 */
		 
		void setMaxEvals(int maxEvals);
		
		
		/**
		 * Retrieve the value set by setMaxEvals.
		 */
		 
		int getMaxEvals();


		/**
		 * Return the NativeFunctionImplementation class identified for this Function.
		 */

		Class getNativeImplementationClass();


		/**
		 * Determine the events that result from a set of input events, for a given
		 * attribute configuration (SimulationRun). These events will be scheduled
		 * to occur in the same time epoch.
		 */

		SortedEventSet<GeneratedEvent> respond(SimulationRun simRun, 
			SortedEventSet<GeneratedEvent> events)
		throws
			ModelContainsError;  // if an error occurs within the function model.


		/**
		 * Native Function implementations must implement this interface. A native
		 * Function implementation provides a hand-coded implementation of a
		 * Function, in Java or some other language.
		 */

		interface NativeFunctionImplementation extends NativeComponentImplementation
		{
			/**
			 * Completely initialize the internal state of this implementation.
			 */
	
			void start(FunctionContext context) throws Exception;
		}


		/**
		 * Java native Function implementations must implement this interface.
		 */

		interface JavaFunctionImplementation extends NativeFunctionImplementation
		{
		}
	}


	/**
	 * A Terminal is a special Function that, upon activation from an external
	 * event, merely reads all of its Ports, selects the one that is currently
	 * driven, and then broadcasts that value to all of its Ports. More precisely,
	 * upon activation, a Terminal does the following things:
	 *	1. Selects the Port that is currently driven (it is an error if multiple Ports are driven with different values during the same epoch).
	 *	2. Updates its internal state with the value of that Port.
	 *	3. Since all of the Terminal's Ports are bound to the internal state, all then receive events (in the current epoch) containing the new state value.
	 */

	public interface Terminal extends Function
	{
		Port getPort();

		void setAttention();

		void setUrgent();
		
		int getColor();

		// Serialization methods.
		String getPortNodeId();
		String getStateNodeId();
	}


	/**
	 * A Generator is a pre-defined Activity that generates Events according
	 * to a parameterized gamma distribution. A Generator maintains an
	 * internal State to which its Events apply. There are Generators pre-defined
	 * for various types of distribution, and for both discrete and non-discrete
	 * State values. For any Generator, there are actually two distributions
	 * involved: (1) the distribution of time between changes in value,
	 * and (2) the distribution of values.
	 */

	public interface Generator extends Activity
	{
		public static final String RepeatingConduitName = "_count_to_input_";
	
		Port getInputPort();
		Port getOutputPort();
		Port getCountPort();
		
		//DistributionFactory getTimeDistFactory();
		//DistributionFactory getValueDistFactory();

		double getDefaultTimeDistShape();
		double getDefaultTimeDistScale();
		double getDefaultValueDistShape();
		double getDefaultValueDistScale();
		
		boolean getIgnoreStartup();
		void setIgnoreStartup(boolean ignore);
		
		ModelAttribute getTimeShapeAttr();

		// Serialization methods.
		String getInputPortNodeId();
		String getOutputPortNodeId();
		String getCountPortNodeId();
		String getStateNodeId();
		String getCountStateNodeId();
		String getTimeShapeAttrNodeId();
		String getTimeScaleAttrNodeId();
		String getValueShapeAttrNodeId();
		String getValueScaleAttrNodeId();
	}
	
	
	/**
	 * A Generator that allows the Scale parameter for both the time and value
	 * distributions to change dynamically during simulation.
	 */
	 
	public interface VariableGenerator extends Generator
	{
		Port getTheta1Port();
		Port getTheta2Port();

		String getTheta1PortNodeId();
		String getTheta2PortNodeId();
	}
	
	
	/**
	 * A Generator that outputs pulses instead of continuous output.
	 */
	 
	public interface PulseGenerator extends Generator
	{
		//String getEmbeddedGeneratorNodeId();
		//String getCompensatorNodeId();
		String getRiseStateNodeId();
		String getRiseInNodeId();
		String getRiseOutNodeId();
		
		Port getRiseIn();
		Port getRiseOut();
	}
	
	
	public interface VariablePulseGenerator extends PulseGenerator, VariableGenerator
	{
	}
	
	
	/**
	 * A Delay is a pre-defined type of Activity.
	 * A Delay repeats on its output Port any input event that it receives on
	 * its input Port, identical in value, but delayed by a pre-specified
	 * time delay (in simulation time).
	 */
	 
	public interface Delay extends Activity
	{
		long getDefaultDelay();

		// Serialization methods.
		String getInputPortNodeId();
		String getOutputPortNodeId();
		String getStateNodeId();
		String getDelayAttrNodeId();
	}
	
	
	/**
	 * A Sustain is a pre-defined type of Activity.
	 * A Sustain responds to a rising change to its input, and generates that
	 * as an output for a period of time, which is a random variable according
	 * to a specified distribution. After the time has passed, the Sustain's
	 * output drops to zero. The input to a Sustain must be numeric.
	 */
	 
	public interface Sustain extends Activity
	{
		Port getInputPort();
		Port getOutputPort();
		Port getPeriodPort();
		double getDefaultTimeDistShape();
		double getDefaultTimeDistScale();
		ModelAttribute getTimeDistShapeAttr();
		ModelAttribute getTimeDistScaleAttr();
	
		// Serialization methods.
		String getInputPortNodeId();
		String getOutputPortNodeId();
		String getPeriodPortNodeId();
		String getStateNodeId();
		String getTimeDistShapeAttrNodeId();
		String getTimeDistScaleAttrNodeId();
	}
	
	
	/**
	 * A OneWay is a pre-defined type of Activity.
	 * A OneWay propagates Events that enter through its input Port, but not
	 * through its output Port.
	 */
	 
	public interface OneWay extends Activity
	{
		String getInputPortNodeId();
		String getOutputPortNodeId();
		String getStateNodeId();
	}
	
	
	/**
	 * A Tally is a pre-defined type of Activity.
	 * A Tally accumulates the values that are presented to it. The values
	 * must be numeric.
	 */
	 
	public interface Tally extends Activity
	{
		State getTallyState();
		Port getOutputPort();
		Number getDefaultInitValue();
		ModelAttribute getInitValueAttr();
		boolean getDefaultDoubleCount();
		ModelAttribute getDoubleCountAttr();

		// Serialization methods.
		String[] getInputPortNodeIds();
		String getOutputPortNodeId();
		String getStateNodeId();
		String getInitValueAttrNodeId();
		String getDoubleCountAttrNodeId();
	}
	
	
	/**
	 * A Max is a pre-defined type of Activity.
	 * A Max selects the max of the values that are presented to it. The values
	 * must be numeric.
	 */
	 
	public interface Max extends Activity
	{
		ModelAttribute getInitValueAttr();
		Port getOutputPort();

		// Serialization methods.
		String[] getInputPortNodeIds();
		String getOutputPortNodeId();
		String getStateNodeId();
		String getInitValueAttrNodeId();
	}
	
	
	/**
	 * A Switch is a pre-defined type of Activity.
	 * A Switch blocks a driver when it is open, and propagates a driver when
	 * it is closed.
	 */
	 
	public interface Switch extends Activity
	{
		Port getInputPort();
		Port getOutputPort();
		Port getControlPort();
		State getState();
		boolean getDefaultStartConducting();
		ModelAttribute getStartConductingAttr();
		
		// Serialization methods.
		String getInputPortNodeId();
		String getOutputPortNodeId();
		String getControlPortNodeId();
		String getStateNodeId();
		String getStartConductingAttrNodeId();
		//boolean getConducting();
	}
	
	
	/**
	 * When a Modulator receives an Event on its input_port, it produces an
	 * Event on its output_port that is the product of the input_port value and
	 * the value on the control_port. These values must be numeric.
	 * The Modulator's internal state, which drives its output, is 
	 * called "state".
	 */
	 
	public interface Modulator extends Activity
	{
		String getInputPortNodeId();
		String getOutputPortNodeId();
		String getControlPortNodeId();
		String getStateNodeId();
		String getFactorAttrNodeId();  // Attr value is Scenario-specific.
		Double getDefaultFactor();  // Default: applies if Scenario does not override.
	}
	
	
	/**
	 * An Activity that evaluates a specified expression. The expression may
	 * contain references to any input ports that are defined. The result of
	 * the expression is placed on the special port �output_port�.
	 * Currently Exrepssion is an Activity, but eventually it will be made into
	 * a Function.
	 */
	 
	public interface Expression extends Activity
	{
	}
	
	
	/**
	 * Whenever the value driving a Discriminator's input_port rises to a value
	 * that is equal to or greater than the value on its control_port, 
	 * the Discriminator generates an Event of value true.
	 * Whenever the value driving the input_port falls below the value on the
	 * control_port, the Discriminator generates an Event of value false.
	 * The Discriminator's internal state, which drives its output, is 
	 * called "state".
	 */
	 
	public interface Discriminator extends Activity
	{
		Double getDefaultThreshold();
		String getThresholdAttrNodeId();
	}
	
	
	public interface Compensator extends Activity
	{
		long getDefaultValue();
	}
	
	
	public interface DoubleExpression extends Expression
	{
		String getDefaultExpressionString();
		ModelAttribute getExpressionAttr();
		
		void validateExpression(String expr)
		throws
			ParameterError;  // if the expression String is not syntactically valid.
			
	}
	
	
	public interface NotExpression extends Expression
	{
	}
	
	
	public interface Summation extends Function
	{
	}
	
	
	/**
	 * Represents a process that acts over time, generating events and
	 * maintaining internal state.
	 *
	 * An Activity receives Events via its respond(...) method. In response,
	 * it generates new Events. These new events
	 * are not scheduled or managed by the Activity: they are merely returned;
	 * the caller (the simulator) is responsible for scheduling and managing
	 * the new events. The Activity should treat these new events as tentative
	 * and should not respond to them: it should only respond to its input
	 * events. An Activity should assume that time does not advance while its
	 * respond(...) method is executing.
	 */

	public interface Activity extends PortedContainer, EventProducer
	{
		/**
		 * Identify a native implementation class to use for this activity. If a
		 * native implementation class is identified, then it will be used instead
		 * of the Activity's internal components (if any).
		 */

		void setNativeImplementationClass(Class implClass)
		throws
			ParameterError;  // if the implClass is not a NativeActivityImplementation.


		/**
		 * Return the NativeActivityImplementation class identified for this Activity.
		 */

		Class getNativeImplementationClass();


		/**
		 * Determine the events that result from a set of input events, for a given
		 * attribute configuration (SimulationRun).
		 */

		SortedEventSet<GeneratedEvent> respond(SimulationRun simRun, 
			SortedEventSet<GeneratedEvent> events)
		throws
			ModelContainsError;	// if an error occurs within the Activity model.


		/**
		 * Native Activity implementations must implement this interface. A native
		 * Activity implementation provides a hand-coded implementation of an
		 * Activity, in Java or some other language.
		 */

		interface NativeActivityImplementation extends NativeComponentImplementation
		{
			/**
			 * Completely initialize the internal state of this implementation.
			 */
	
			void start(ActivityContext context) throws Exception;
		}


		/**
		 * Java native Activity implementations must implement this interface.
		 */

		interface JavaActivityImplementation extends NativeActivityImplementation
		{
			Activity getActivity();
		}
	}


	/**
	 * The simulator passes an instance of this to a NativeImplementation when
	 * it starts. This provides a gateway for the implementation to access
	 * simulation functions such as scheduling events.
	 *
	 * There is normally no need to persist a ModelContext: it is merely a
	 * run-time gateway to link native components to the simulation.
	 */

	interface ModelContext
	{
		/**
		 * Retrieve the ModelComponent that owns this ModelContext.
		 */

		ModelComponent getComponent();


		/**
		 * Retrieve the SimulationRun associated with this ModelContext.
		 */

		SimulationRun getSimulationRun();
		
		
		/**
		 * Return the simulation time, in ms, at which the simulation is defined
		 * to start.
		 */
		 
		long getSimulationStartTimeMs();


		/**
		 * Schedule a state change event, to occur after a delay of zero.
		 * May only be called during simulation.
		 * May only be called via the Activity that owns the State.
		 */

		GeneratedEvent setState(State state, Serializable value, 
			SortedEventSet<GeneratedEvent> events)
		throws
			ModelContainsError;  // if the state is not owned by the component.


		/**
		 * Schedule a state change event, to occur at the specified future time.
		 * May only be called during simulation.
		 * May only be called via the Activity that owns the State.
		 */

		GeneratedEvent scheduleFutureStateChange(State state,
			Date time, Serializable value, SortedEventSet<GeneratedEvent> events)
		throws
			ModelContainsError,  // if the state is not owned by the component.
			ParameterError; // if the specified time is earlier than
				// the current time.

		/**
		 * For use only by pulse Components (Pulse Generator, Variable Pulse Generator)
		 * and Components that propagate compensation Events.
		 */
		 
		GeneratedEvent scheduleCompensationEvent(State state, Serializable value,
			SortedEventSet<GeneratedEvent> triggeringEvents)
		throws
			ModelContainsError;
			
			
		/**
		 * Schedule a state change event, to occur after the specified delay in
		 * milliseconds.
		 * May only be called during simulation.
		 * May only be called via the Activity that owns the State.
		 */

		GeneratedEvent scheduleDelayedStateChange(State state,
			long msDelay, Serializable value, SortedEventSet<GeneratedEvent> events)
		throws
			ModelContainsError,  // if the state is not owned by the component.
			ParameterError; // if the specified time delay is negative or too large.


		/**
		 * Retrieve the Component within this context's container, or within an
		 * enclosing container, that has the
		 * specified name. If it is not found then return null.
		 */

		ModelComponent getComponent(String name);
		
		
		/**
		 * Retrieve the Port within this context's container that has the
		 * specified name. If it is not found then return null.
		 */
		 
		Port getPort(String name);


		/**
		 * Retrieve the State within this context's container, or within an
		 * enclosing container, that has the
		 * specified name. If it is not found then return null.
		 */

		State getState(String name);

		/**
		 * Retrieve the value of the specified Attribute, in the context of the
		 * specified SimulationRun.
		 */

		Serializable getAttributeValue(String qualifiedName)
		throws
			ModelContainsError,  // if the parameter is not found.
			ParameterError;


		/**
		 * Return the current simulation-time epoch. (See explanation at
		 * {@link ModelElement.SimulationRun#getEpochs()}.
		 */

		Epoch getCurrentSimulationEpoch();

		
		long getElapsedSimulationMs();
		

		/**
		 * (Do I really need this method?)
		 */

		SortedEventSet<GeneratedEvent> getAndPurgeNewEvents();


		/**
		 * Identify all future events scheduled by this context's component, and
		 * delete them from the queue. Return the number of events that are
		 * purged.
		 */

		int purgeFutureEvents();


		/**
		 * Retrieve a Set of the Ports that are sensitive to the specified State.
		 * A Port is sensitive to a State if (1) the Port can receive Events on 
		 * the State, (2) the Port can pass the Event into its Component, and 
		 * (3) the Component has native behavior.
		 */
		 
		Set<Port> getSensitivePorts(State state)
		throws
			ModelContainsError;
			
			
		/**
		 * Return the Port(s) on which the specified Event has arrived, if any.
		 */
		
		Set<Port> getEventPorts(Event event)
		throws
			ModelContainsError;
			
		
		/**
		 * Read the current value of the specified State.
		 */
		 
		Serializable getStateValue(State state);
		
		
		/**
		 * Read the current value of the State that is driving the specified
		 * Port. The Port may be anywhere within a model: it need not be directly
		 * bound to the State that drives it.
		 * This method may return null if the State's value is undefined.
		 */

		Serializable getStateValue(Port port);


		/**
		 * Identify the State (if any) that is currently driving the specified
		 * Port. Return null if the Port is currently un-driven.
		 */

		State getCurrentDriver(Port port)
		throws
			ModelContainsError,  // if the parameter is not found.
			ParameterError;
		
		
		/**
		 * Return the elapsed number of milliseconds that the specified Event
		 * occurred after it was scheduled. The Event must be active in the current
		 * epoch.
		 */
		 
		long getElapsedMs(GeneratedEvent e)
		throws
			ModelContainsError,  // any other error pertaining to the model.
			ParameterError;  // if the Event is not active in the current epoch.
	}


	interface FunctionContext extends ModelContext
	{
	}
	
	
	interface ActivityContext extends ModelContext
	{
		/**
		 * Read the current value of the specified State. Note that State value
		 * changes that are scheduled in the current epoch do not take affect
		 * until the next epoch at the soonest. This method may return null
		 * if the State's value is undefined.
		 */

		Serializable getStateValue(State state);
	}
	
	
	/**
	 * Native implementations must implement this interface. A native
	 * implementation provides a hand-coded implementation of a ModelElement,
	 * in Java or some other language. Native implementations need not
	 * be re-entrant: the simulator must protect against concurrent access.
	 * However, a native implementation should take care to initialize its
	 * state each time it is started: otherwise, behavior might change between
	 * simulation runs.
	 */

	interface NativeComponentImplementation
	{
		ModelContext getModelContext();
		
		
		/**
		 * Set the ModelComponent that is implemented by this native
		 * implementation.
		 */

		void setModelComponent(ModelComponent modelComponent);

		/**
		 * Retrieve the ModelComponent that is implemented by this native
		 * implementation.
		 */

		ModelComponent getModelComponent();


		void respond(SortedEventSet<GeneratedEvent> events)
		throws
			ModelContainsError;
			
			
		/**
		 * Release any native resources held for the purpose of simulation.
		 */

		void stop();


		/**
		 * Return the Set of events that are generated by the component
		 * implementation for the most recent call to respond(...); then purge
		 * the component's ModelContext of these events.
		 */

		SortedEventSet<GeneratedEvent> getAndPurgeNewEvents();
	}


	/**
	 * Connection point for Conduits. Multiple Conduits may
	 * connect to a Port. An Activity or Function may ONLY communicate outside
	 * of itself via its Ports. Thus, there are no global variables that may
	 * change at simulation time.
	 * A Port has zero width and zero height. However, a client may display it
	 * with postitive width and height.
	 */

	public interface Port extends ModelComponent
	{
		PortedContainer getContainer();
		
		/** May return null. */
		State getBoundState();
		
		PortDirectionType getDirection();
		
		void setDirection(PortDirectionType dir);
		
		/** A Port is "black" if it does not propagate ("reflect") Events that
			arrive on an externally attached Conduit to other externally attached
			Conduits. See the slide "Propagation Algorithm". */
		void setBlack(boolean black);
		
		boolean getBlack();

		
		/** Arrange the location of this Port so that it is positioned along
		 the specified side of its Container, extending inward into its Container
		 (i.e., not projecting out of its Container). Re-route any Conduits
		 connected to the Port. */
		void setSide(Position pos);
		
		/** Return the side that this Port lies along. If it does not lie along
		 any side, return null. */
		Position getSide();
		
		Point getLocationInIcon();
		
		/** Return the X location of this Port within its owner, when the owner is
			an icon (not expanded). */
		double getXInIcon();
		
		/** Return the Y location of this Port within its owner, when the owner is
			an icon (not expanded). */
		double getYInIcon();
		
		void setXInIcon(double x);
		
		void setYInIcon(double y);

		boolean allowsInputEvents();

		boolean allowsOutputEvents();
		
		void addSourceConnection(Conduit c);  // perhaps this should not be in the interface.
		
		void addDestinationConnection(Conduit c);  // perhaps this should not be in the interface.
		
		void removeSourceConnection(Conduit c);

		void removeDestinationConnection(Conduit c);
		
		Set<Conduit> getSourceConnections();

		Set<Conduit> getDestinationConnections();
		
		Set<Conduit> getExternalSourceConnections();
		
		Set<Conduit> getExternalDestinationConnections();
		
		Set<Conduit> getExternalConnections();
		
		/** Return a Rectangle that geometrically bounds this Port. If 'inIcon'
			is true, assume that the PortedContainer is an icon. */
		Rectangle getRectangle(boolean inIcon);
		
		/** Return a Point at the geometric center of this Port. */
		Point getCenterPoint();
		
		/** Choose a ModelContainer to own a Conduit for connecting this Port
			to 'target'. */
		ModelContainer chooseContainerForConduit(Port target)
		throws
			ParameterError;  // if this Port cannot be connected to 'target'.
		
		// For serialization.
		String[] getSourceConnectionNodeIds();
		String[] getDestinationConnectionNodeIds();
	}


	/**
	 * Bi-directional component that conducts events from one Port to another.
	 */

	public interface Conduit extends ModelComponent,
		PortChangeListener  // for being notified when a Port to which this
			// Conduit is connected has been deleted.
	{
		void setPort1(Port p1);
		
		void setPort2(Port p2);
		
		ModelContainer getContainer();
		
		
		/**
		 * Disconnect Port p from this Conduit. If this Conduit is not connected
		 * to Port p, then do nothing.
		 */
		 
		void disconnectFromPort(Port p);  // perhaps this should not be in the interface.

		Port getPort1() throws ValueNotSet;

		Port getPort2() throws ValueNotSet;

		void setInflectionPoints(InflectionPoint[] ipArray);

		InflectionPoint[] getInflectionPoints(); // may return null.
		
		
		/**
		 * Create a new InflectionPoint with the specified coordinates, and insert
		 * it as the position'th InflectionPoint in this Conduit's list of
		 * InflectionPoints. To be consistent with Java, the first InflectionPoint
		 * is considered to be the zero'th position. If there are fewer than
		 * position InflectionPoints, then the insertion is not possible, and
		 * a ParameterError is thrown. The two Points specified are in the 
		 * coordinate system of the Conduit's parent Container.
		 */
		 
		InflectionPoint insertInflectionPointAt(double x, double y, int position)
		throws
			ParameterError;  // if the inflection point is not inside this Conduit's Container.
			
			
		void deleteInflectionPoint(InflectionPoint point)
		throws
			ValueNotSet,  // if Port 1 or Port 2 is not set.
			ParameterError;  // if this Conduit does not own the InflectionPoint.
			
		/**
		 * Create Inflection Points to route the Conduit from one Port to the
		 * other. If the Ports are not set, do nothing.
		 */
		 
		void reroute()
		throws
			ParameterError;
	}


	/**
	 * The value of a State is defined as the value of the most recent Event
	 * for the State, up through the current epoch. If there is no prior Event
	 * for a state, then its value (returned by getValueAt()) is undefined (null).
	 */

	public interface State extends ModelComponent, AttributeStateBindingChangeListener,
		PortChangeListener
	{
		EventProducer getEventProducer();

		
		//Event getEventPreceding(Date t);

		//Serializable getValueAt(Date t);

		Set<Port> getPortBindings();  // These must be internal to the
			// Activity that defines the State.

		void bindPort(Port p)
		throws
			ParameterError;  // if the Port does not belong to this State's Container.

		void unbindPort(Port p)
		throws
			ParameterError;  // if this State is not bound to the Port.

		void addAttributeBinding(AttributeStateBinding binding);  // perhaps this should not be in the interface.
		
		Set<AttributeStateBinding> getAttributeBindings();


		/**
		 * May only be called prior to simulation.
		 * Create a PredefinedEvent prior to simulation. This Event will be
		 * associated with the model, rather than with a ModelScenario. Thus, it will
		 * be included in all ModelScenario simulations. The time value is in days
		 * from the start of simulation. A time value of zero specifies
		 * that the Event should occur during the first epoch following the startup
		 * The value of the Event is explicitly specified.
		 */

		PredefinedEvent predefineEvent(String name, String timeExprString,
			String valueExprString, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;  // if the object value is invalid (e.g., not cloneable).
		
		
		/**
		 * Same as predefineEvent(long, Serializable) except that
		 * the time and value of the Event are taken from the specified Attributes
		 * during simulation.
		 */
		
		PredefinedEvent predefineEvent(String name, ModelAttribute timeAttr, 
			ModelAttribute valueAttr, PersistentNode layoutBound,
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;  // if the Attribute value is invalid for any reason.

		
		/**
		 * Intended for use by ModelEngine when instantiating a new Predefined Event
		 * in response to a client request.
		 */
		 
		PredefinedEvent predefineEvent(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;  // if the name is already in use.
			

		void deletePredefinedEvent(PredefinedEvent event, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		Set<PredefinedEvent> getPredefinedEvents();


		String[] getPreEventNodeIds();
		String[] getAttributeBindingNodeIds();
		String[] getPortBindingNodeIds();
	}


	/**
	 * An event that is defined statically as part of the model. During simulation
	 * a Generated Event is created for each Predefined Event. Predefined Events
	 * themselves are never seen by any Components during simulation: only their
	 * Generated Event equivalents.
	 */

	public interface PredefinedEvent extends ModelElement, Cloneable
	{
		State getState();
		
		
		/** Return true if this Event has zero duration, representing a 'pulse'. */
		boolean isPulse();
		
		
		/** Return true if this Event will be followed by a compensation Event. */
		void setPulse(boolean p);


		ModelAttribute getTimeAttribute();
		ModelAttribute getValueAttribute();
		
		String getDefaultTimeExpression();
		void setDefaultTimeExpression(String expr);
		String getDefaultValueExpression();
		void setDefaultValueExpression(String expr);
	}
	
	
	/**
	 *
	 */

	public interface Satisfies extends DirectedRelation
	{

	}


	/**
	 *
	 */

	public interface Derives extends DirectedRelation
	{

	}


	/**
	 *
	 */

	public interface ObjectiveFunction extends Function
	{

	}
	
	
	
	/* *************************************************************************
	 * The following interfaces define methods to be called to notify non-owners
	 * of objects that have references to those objects that need to be nullified.
	 * Notification should be synchronous. The <>Deleted(...) method should be
	 * called immediately before the Node is actually deleted. The <>NameChanged(...)
	 * method should be called immediately after the Node's name has been changed.
	 * See also the Listeners defined in MotifElement.
	 */
		 
	interface AttributeStateBindingChangeListener
	{
		void attributeStateBindingDeleted(AttributeStateBinding binding);
		void attributeStateBindingNameChanged(AttributeStateBinding binding, String oldName);
	}
	
	
	interface ModelComponentChangeListener
	{
		void modelComponentDeleted(ModelComponent component);
		void modelComponentNameChanged(ModelComponent component, String oldName);
	}
	
	
	interface ModelElementChangeListener
	{
		void modelElementDeleted(ModelElement element);
		void modelElementNameChanged(ModelElement element, String oldName);
	}
	
	
	interface ModelScenarioChangeListener
	{
		void modelScenarioDeleted(ModelScenario s);
	}
	
	
	interface ModelScenarioSetChangeListener
	{
		void modelScenarioSetDeleted(ModelScenarioSet s);
	}
	
		
	interface SimulationRunChangeListener
	{
		void simulationRunDeleted(SimulationRun s);
	}
	
	
	interface SimRunSetChangeListener
	{
		void simRunSetDeleted(SimRunSet s);
	}
	
	
	interface ConstraintChangeListener
	{
		void constraintDeleted(Constraint constraint);
		void constraintNameChanged(Constraint constraint, String oldName);
	}
	
	
	interface StateChangeListener
	{
		void stateDeleted(State state);
		void stateNameChanged(State state, String oldName);
	}
	
	
	interface PredefinedEventChangeListener
	{
		void predefinedEventDeleted(PredefinedEvent event);
		void predefinedEventNameChanged(PredefinedEvent event, String oldName);
	}
	
	
	interface InflectionPointChangeListener
	{
		void inflectionPointDeleted(InflectionPoint point);
		void inflectionPointNameChanged(InflectionPoint point, String oldName);
	}
	
	
	interface PortChangeListener
	{
		void portDeleted(Port port);
		void portNameChanged(Port port, String oldName);
	}
}
