/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import java.util.HashMap;
import java.util.Set;


/**
 * Maps Nodes to their cloned counterparts. See the slide "Clone Map Pattern".
 */

public class CloneMap extends HashMap<PersistentNode, PersistentNode>
{
	/** The top of the hierarchy enclosed by this Map. This is the root of the
		hierarchy that is being cloned. */
	PersistentNode root;
	
	
	public CloneMap(PersistentNode root)
	{
		this.root = root;
	}
	
	
	public PersistentNode getRoot() { return this.root; }
	
	
	public void addClonedNode(PersistentNode original, PersistentNode clonedNode)
	{
		if (original == null) throw new RuntimeException(
			"Attempt to map a null original in a CloneMap");

		if (clonedNode == null) throw new RuntimeException("Clone is null");
			
		PersistentNode entry = this.get(original);
		
		if (entry != null) return;  // must have been set by getOriginalNode.
	
		this.put(original, clonedNode);
	}
	
	
	public <T extends PersistentNode> T getClonedNode(T original)
	throws
		CloneNotSupportedException
	{
		if (original == null) return null;
		
		T entry = (T)(this.get(original));
		
		if (entry == null)
			if (original.isDescendantOf(root))  // in hierarchy
			{
				PersistentNode parent = original.getParent();
				PersistentNode cloneParent = this.getClonedNode(parent);  // recursive
				
				this.put(original, entry = (T)(original.clone(this, cloneParent)));
			}
			else
				return original;
		
		return entry;
	}
	
	
	public <T extends PersistentNode> T getOriginalNode(T clonedNode)
	{
		if (clonedNode == null) throw new RuntimeException("Null cloned Node");
		
		Set<PersistentNode> keys = this.keySet();
		for (PersistentNode key : keys)
		{
			PersistentNode n = this.get(key);
			if (n == clonedNode) return (T)key;
		}
		
		return null;
	}
	
	
	/*static class NullEntry extends PersistentNodeImpl
	{
		public double getPreferredWidthInPixels() { return 0.0; }
		public double getPreferredHeightInPixels() { return 0.0; }
		public void setSizeToStandard() {}
		public Domain getDomain() { return null; }
	}*/
}

