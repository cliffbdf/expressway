/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import java.util.Set;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.IOException;


public class CompensatorImpl extends ActivityBase implements Compensator
{
	private static final long DefaultDefaultValue = 0;
	
	/** reference only. */
	public Port inputPort = null;

	/** reference only. */
	public Port outputPort = null;
	
	/** reference only. */
	public State state = null;

	private long defaultValue = DefaultDefaultValue;
	

	public Class getSerClass() { return CompensatorSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((CompensatorSer)nodeSer).inputPortNodeId = this.getNodeIdOrNull(inputPort);
		((CompensatorSer)nodeSer).outputPortNodeId = this.getNodeIdOrNull(outputPort);
		((CompensatorSer)nodeSer).stateNodeId = this.getNodeIdOrNull(state);
		((CompensatorSer)nodeSer).defaultValue = this.getDefaultValue();
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		CompensatorImpl newInstance = (CompensatorImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.inputPort = cloneMap.getClonedNode(inputPort);
		newInstance.outputPort = cloneMap.getClonedNode(outputPort);
		newInstance.state = cloneMap.getClonedNode(state);
		
		return newInstance;
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(inputPort, null);
		deletePort(outputPort, null);
		deleteState(state, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		inputPort = null;
		outputPort = null;
		state = null;
	}
	
	
	public CompensatorImpl(String name, ModelContainer parent, ModelDomain domain,
		long defaultValue)
	{
		super(name, parent, domain);
		init(defaultValue);
	}
	
	
	public CompensatorImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init(DefaultDefaultValue);
	}
	
	
	public String getTagName() { return "compensator"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.CompensatorIconImageName);
	}
	
	
	private void init(long defaultValue)
	{
		setResizable(false);
		this.defaultValue = defaultValue;
		
		setLayoutManager(DefaultLayoutManager);
		
		try
		{
			state = createState("State", this, null);
			inputPort = createPort("input_port", PortDirectionType.input, Position.left, this, null);
			outputPort = createPort("output_port", PortDirectionType.output, Position.right, this, null);
		}
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		try { state.bindPort(outputPort); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		inputPort.setMovable(false);
		outputPort.setMovable(false);
		state.setMovable(false);
		
		inputPort.setPredefined(true);
		outputPort.setPredefined(true);
		state.setPredefined(true);
	}
	
	
	public int getNoOfHeaderRows() { return 1; }
	
	
	private static final double PreferredWidthInPixels = 30.0;
	private static final double PreferredHeightInPixels = 30.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change Port direction for a standard component");
	}


	public long getDefaultValue() { return defaultValue; }
	
	public State getState() { return state; }
	
	public Port getInputPort() { return inputPort; }
	
	public Port getOutputPort() { return outputPort; }


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init(DefaultDefaultValue);
	}
}


class CompensatorNativeImpl extends ActivityNativeImplBase
{
	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		if (portHasEvent(events, getInputPort()))
		{
			Serializable eventValue = getActivityContext().getStateValue(getInputPort());
			
			// Verify that there is at least one event contributing to the port value
			// that did not originate from this Compensator.
			
			SortedEventSet<GeneratedEvent> evsOnPort = getEventsOnPort(getInputPort(), events);
			boolean atLeastOne = false;
			for (GeneratedEvent e : evsOnPort) if (! (e.getState() == getCompensator().getState()))
			{
				atLeastOne = true;
				break;
			}
			
			if (! atLeastOne) return;  // return to prevent race condition.
			
			
			// Generate an output Event to negate the input event.
				
			try
			{				
				long defaultValue = getCompensator().getDefaultValue();
				if (eventValue != null)
				{
					if (! (eventValue instanceof Number)) throw new ModelContainsError(
						"Input Event on a Compensator is not a Number");
					
					Number number = (Number)eventValue;
					long value;
					try { value = number.longValue(); }
					catch (Exception ex) { throw new ModelContainsError(
						"Unable to convert Compensator input value to a long"); }
				}
				
				long newValue = defaultValue;
				//System.out.println("...scheduling state change, to " + newValue);
				getActivityContext().setState(getCompensator().getState(), 
					new Double(newValue), events);
			}
			catch (Throwable t)
			{
				System.err.println("Caught Throwable: ");
				GlobalConsole.printStackTrace(t);
			}
			//finally { System.out.println("...leaving Compensator native impl"); }
		}
	}
	
	
	protected CompensatorImpl getCompensator()
	{
		return (CompensatorImpl)(getActivity());
	}
	
	
	public Port getInputPort() { return getCompensator().getInputPort(); }
	
	public Port getOutputPort() { return getCompensator().getOutputPort(); }
}

