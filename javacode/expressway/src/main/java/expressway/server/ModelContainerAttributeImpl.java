/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.HierarchyElement.*;
import java.io.Serializable;
import java.io.PrintWriter;


public class ModelContainerAttributeImpl extends ModelAttributeImpl
	implements ModelContainerAttribute
{
	private HierarchyAttribute definedBy = null;


	public ModelContainerAttributeImpl(String name, ModelElement parent,
		Serializable defaultValue)
	{
		super(name, parent, defaultValue);
	}


	public ModelContainerAttributeImpl(ModelElement parent, String baseName)
	{
		super(parent, baseName);
	}
	
	
  /* From HierarchyAttribute */
	
	
	public void setDefining(boolean newValue)
	throws
		DisallowedOperation
	{
		if (newValue == false) HierarchyAttributeHelper.removeFromHierarchy(this);
		this.defining = true;
	}
	
	
	boolean isDefining() { return this.defining; }
	
		
	public HierarchyAttribute getDefinedBy() { return definedBy; }
	
	
	public void setDefinedBy(HierarchyAttribute definedBy)
	throws
		ParameterError
	{
		if (definedBy == null)
			this.definedBy = null;
		else
		{
			if (definedBy == this) throw new ParameterError(
				"Attempt to set definedBy Attribute to itself");
			if (! (getParent().isDescendantOf(definedBy.getParent())))
				throw new ParameterError("Attempt to set a definedby Attribute " +
					"that does not belong to a parent of this Attribute");
			if (getParent().getAttribute(definedBy.getName()) != null)
				throw new ParameterError("'" + getParent().getFullName() + 
					" already has an Attribute with the name '" + definedBy.getName() + "'");
			this.definedBy = definedBy;
		}
	}
	
		
	public void applyToHierarchy()
	throws
		ParameterError, DisallowedOperation
	{
		a = HierarchyAttributeHelper.findNestedAttrWithName(getParent(), this.getName());
		if ((a != null) && a.getDefinedBy() == this) return;
		if (a != null) throw new DisallowedOperation(
			"An attribute with that name is already owned by " + a.getParent().getFullName());
		
		try { HierarchyAttributeHelper.applyToHierarchy(this); }
		catch (Exception ex) { throw new RuntimeException(ex); }
	}
	
	
	public void removeFromHierarchy()
	throws
		ParameterError
	{
		if (! isDefining()) throw new ParameterError(
			"Attempt to remove a non-defining Attribute as if it were defining");
		
		HierarchyAttributeHelper.removeFromHierarchy(this);
	}


	public void attributeDeleted(Attribute attribute)
	{
		// Does the deleted Attribute define this Attribute?
		if (this.getDefinedBy() == attribute)
		{
			// Change this Attribute so that it is self-defined:
			if (this.getDefaultValue() == null)
				this.setDefaultValue(attribute.getDefaultValue());
			try { this.setDefinedBy(null); }
			catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		}
		else
			super.attributeDeleted(attribute);
	}
	
	
  /* From HierarchyElement */
	
	
	public HierarchyAttribute createHierarchyAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef));
	}
	
	
	public HierarchyAttribute createHierarchyAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(layoutBound, outermostAffectedRef));
	}


	public HierarchyAttribute createHierarchyAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(name, layoutBound, outermostAffectedRef));
	}
	
	
	public HierarchyAttribute createHierarchyAttribute(HierarchyAttribute attr,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return HierarchyElementHelper.createHierarchyAttribute(this, attr, layoutBound,
			outermostAffectedRef);
	}
	
	
	public HierarchyDomain getHierarchyDomain()
	{
		return (HierarchyDomain)(getDomain());
	}

	
  /* From PersistentNode */
	

	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		ModelContainerAttributeImpl newInstance =
			(ModelContainerAttributeImpl)(super.clone(cloneMap, cloneParent));
			
		if (definedBy != null)
		{
			newInstance.definedBy = cloneMap.getClonedNode(definedBy);
			if (! newInstance.getParent().isDescendantOf(newInstance.definedBy.getParent()))
				try { newInstance.setDefinedBy(null); }
				catch (ParameterError pe) { throw new RuntimeException(pe); }
		}

		return newInstance;
	}

	
	public void setName(String name)
	throws
		ParameterError
	{
		if (definedBy == null) super.setName(name);
		else definedBy.setName(name);
	}
	

	public String getName()
	{
		if (definedBy == null) return super.getName();
		else return definedBy.getName();
	}
	
	
	public void writeSpecializedXMLAttributes(PrintWriter writer)
	{
		writer.println(
			(definedBy == null) ? "" : "defined_by=\"" + definedBy.getFullName() + "\" ");
	}
}

