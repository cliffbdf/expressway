/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.PersistentNode.AttributeValidator;


public abstract class AttributeValidatorBase implements AttributeValidator, Cloneable
{
	public AttributeValidator makeCopy()
	throws
		CloneNotSupportedException
	{
		return (AttributeValidator)(clone());
	}
	
	protected Object clone() throws CloneNotSupportedException { return super.clone(); }
}

