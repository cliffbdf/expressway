/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.PrintWriter;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.SortedSet;


public abstract class DependencyImpl extends DecisionElementImpl implements Dependency
{
	private DecisionPoint dpa = null;
	private DecisionPoint dpb = null;
	
	
	public DependencyImpl(DecisionDomain decisionDomain, DecisionPoint a, DecisionPoint b)
	{
		super(decisionDomain);
		setResizable(false);
		this.dpa = a;
		this.dpb = b;
	}
	
	
	protected abstract String getKindName();


  /* From PersistentNode */


	public String getTagName() { return "dependency"; }
		
		
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public Class getSerClass() { return DependencySer.class; }
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DependencyImpl newInstance = (DependencyImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.dpa = cloneMap.getClonedNode(dpa);
		newInstance.dpb = cloneMap.getClonedNode(dpb);
		
		return newInstance;
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		
		writer.println(indentString + "<" + getTagName() + " name=\"" + this.getName() + "\" "
			+ "kind=\"" + getKindName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" ");
		
		writeSpecializedXMLAttributes(writer);
		writer.println(">");
		
		writeSpecializedXMLElements(writer, indentation);
		writeXMLAttributesForPredefElements(writer, indentation+1);

		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);
		
		writer.println(indentString + "</" + getTagName() + ">");
	}
	
	
	public void writeSpecializedXMLAttributes(PrintWriter writer)
	{
		String pa = dpa.getFullName();
		String pb = dpb.getFullName();
		
		try { pa = PathUtilities.getRelativePath(pa, this.getFullName()); }
		catch (ParameterError ex) { GlobalConsole.printStackTrace(ex); }
		
		try { pb = PathUtilities.getRelativePath(pb, this.getFullName()); }
		catch (ParameterError ex) { GlobalConsole.printStackTrace(ex); }
			
		if (dpa != null) writer.println("source=\"" + pa + "\" ");
		if (dpb != null) writer.println("source=\"" + pb + "\" ");
	}
	
	
  /* From PersistentNodeImpl */


	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((DependencySer)nodeSer).aNodeId = getNodeIdOrNull(dpa);
		((DependencySer)nodeSer).bNodeId = getNodeIdOrNull(dpb);
		
		return super.externalize(nodeSer);
	}
	
	
  /* From Dependency */
	
		
	public DecisionPoint getA() throws ValueNotSet
	{
		if (dpa == null) throw new ValueNotSet("Field 'dpa' for '" + getFullName() + "'");
		return dpa;
	}


	public DecisionPoint getB() throws ValueNotSet
	{
		if (dpb == null) throw new ValueNotSet("Field 'dpb' for '" + getFullName() + "'");
		return dpb;
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		setResizable(false);
	}
	
	
  /* Specialized Dependency classes */


	public static class PrecludesImpl extends DependencyImpl implements Precludes
	{
		PrecludesImpl(DecisionDomain decisionDomain, DecisionPoint a, DecisionPoint b) { super(decisionDomain, a, b); }
		
		protected String getKindName() { return "precludes"; }

		public void setIconImageToDefault() throws IOException { setIconImage(NodeIconImageNames.PrecludesIconImageName); }
	}


	public static class RequiresImpl extends DependencyImpl implements Requires
	{
		RequiresImpl(DecisionDomain decisionDomain, DecisionPoint a, DecisionPoint b) { super(decisionDomain, a, b); }
		
		protected String getKindName() { return "requires"; }

		public void setIconImageToDefault() throws IOException { setIconImage(NodeIconImageNames.RequiresIconImageName); }
	}


	public static class ModifiesImpl extends DependencyImpl implements Modifies
	{
		ModifiesImpl(DecisionDomain decisionDomain, DecisionPoint a, DecisionPoint b) { super(decisionDomain, a, b); }
		
		protected String getKindName() { return "modifies"; }

		public void setIconImageToDefault() throws IOException { setIconImage(NodeIconImageNames.ModifiesIconImageName); }
	}


	public static class CoDependencyImpl extends DependencyImpl implements CoDependency
	{
		CoDependencyImpl(DecisionDomain decisionDomain, DecisionPoint a, DecisionPoint b) { super(decisionDomain, a, b); }
		
		protected String getKindName() { return "co-depends-on"; }

		public void setIconImageToDefault() throws IOException { setIconImage(NodeIconImageNames.CoDependencyIconImageName); }
	}
}
