/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.server.ModelElement.*;
import java.io.IOException;
import java.net.ConnectException;
import java.util.*;


public class CallbackManager
{
	private ModelEngineLocal modelEngine;
	
	private NotificationThread notificationThread;
	
	
	public CallbackManager(ModelEngineLocal modelEngine)
	{
		this.modelEngine = modelEngine;
		this.notificationThread = new NotificationThread();
		notificationThread.start();
	}
	
	
	public void notifyClient(String clientId, PeerNotice notice) throws IOException
	{
		// Get the Listener Registrar corresponding to the specified client Id.
		
		Set<ListenerRegistrarImpl> listenerRegistrars = modelEngine.getListenerRegistrars();
		Set<ListenerRegistrarImpl> registrarsToNotify = new HashSet<ListenerRegistrarImpl>();
		
		for (ListenerRegistrarImpl registrar : listenerRegistrars)
		{
			if (registrar == null) GlobalConsole.println("registrar is null");
			if (registrar.getClientId() == null) GlobalConsole.println(
				"registrar client id is null");
			
			if (registrar.getClientId().equals(clientId))
			{
				registrarsToNotify.add(registrar);
			}
		}
		
		if (registrarsToNotify.size() == 0)
		{
			GlobalConsole.printStackTrace(
				new Exception("No client identified for client Id " + clientId));
			return;
		}
		
		notificationThread.enqueue(notice, registrarsToNotify);
	}
	
	
	public void notifyAllListeners(PeerNotice notice) throws Exception
	{
		// Get Model Engine Set of Listener Registrars
		
		Set<ListenerRegistrarImpl> listenerRegistrars = 
			new HashSet<ListenerRegistrarImpl>(modelEngine.getListenerRegistrars());
		
		notificationThread.enqueue(notice, listenerRegistrars);
	}
	
	
	/**
	 * Create and start a new Thread for sending the Peer Notice to all subscribed
	 * Peer Listeners. Uses a producer/consumer pattern.
	 */
	 
	class NotificationThread extends Thread
	{
		private LinkedList<PeerNotice> notices = new LinkedList<PeerNotice>();
		
		private Queue<Set<ListenerRegistrarImpl>> registrarSets = 
			new LinkedList<Set<ListenerRegistrarImpl>>();
		
		
		public NotificationThread()
		{
		}
		
		
		public void enqueue(PeerNotice notice,
			Set<ListenerRegistrarImpl> listenerRegistrars)
		{
			synchronized (notices)
			{
				if (OperationMode.PrintQueuedNotices)
				{
					GlobalConsole.println("Queueing " + notice.getClass().getName());
				}
				
				notices.add(notice);
				registrarSets.add(listenerRegistrars);
				
				notices.notify();
			}
		}
		
		
		public void run()
		{
			try
			{
				for (;;)
				{
					if (Thread.currentThread() != this) throw new RuntimeException();
					
					// Get the next Notice in the queue.
					
					PeerNotice notice;
					Set<ListenerRegistrarImpl> listenerRegistrars;
					
					try
					{
						synchronized (notices)
						{
							removeObsoleteNotices();
							
							notice = notices.remove();
							listenerRegistrars = registrarSets.remove();
							
							if (OperationMode.PrintDequeuedNotices)
							{
								GlobalConsole.println("Dequeued " + notice.getClass().getName() +
									" for these Registrars:");
								for (ListenerRegistrarImpl listenerRegistrar : listenerRegistrars)
								{
									GlobalConsole.println("\t" + listenerRegistrar.toString());
								}
							}
						}
					}
					catch (NoSuchElementException ex)  // queue was empty
					{
						try
						{
							synchronized (notices)
							{
								notices.wait(400);
							}
							
							sleep(100);
						}
						catch (InterruptedException ex2)
						{
							return;
						}
						
						continue;
					}
				
					// Send the Notice to all clients that are registered as listeners
					// for the Node identified in the Notice.
					
					Timer timer = new Timer();  // For timing remote calls, in case they hang.
					
					Set<ListenerRegistrarImpl> registrarsCopy = 
						new HashSet<ListenerRegistrarImpl>(listenerRegistrars);
					
					for (ListenerRegistrarImpl listenerRegistrar : registrarsCopy)
					{
						// Get the server callback object (the "PeerListener").
						PeerListener peerListener = 
							((ListenerRegistrarImpl)listenerRegistrar).getPeerListener();
						if (peerListener == null) throw new RuntimeException(
							"PeerListener is null");
						
						String nodeId = notice.getPeerId();

						if (OperationMode.PrintPeerListeners)
						{
							GlobalConsole.println("Identified PeerListener " +
								peerListener.getId());
						}

						if (nodeId == null)
						{
							if (notice.isAlwaysForAVisual()) throw new RuntimeException(
								"Null Node Id for a " + notice.getClass().getName());
						}
						else
						{
							if (! listenerRegistrar.isPeerListenerInterrestedIn(notice.getPeerId()))
							{
								if (OperationMode.PrintPeerListeners)
								{
									PersistentNode node = null;
									if (nodeId != null) node = PersistentNodeImpl.getNode(nodeId);
									
									GlobalConsole.println("PeerListener " + peerListener.getId() +
										" is not interested in " +
										(node == null ? "<null>" : node.getFullName()));
								}
								
								// Make exception for case when Node has been deleted
								// and the Notice is a NodeDeletedNotice.
								if (!((notice instanceof PeerNoticeBase.NodeDeletedNotice) &&
									(PersistentNodeImpl.getNode(nodeId) == null)))
									continue;
							}
						}
						
						Thread threadToInterrupt = Thread.currentThread();
						TimerTask interruptTask = new InterruptTask(threadToInterrupt);
								
						try
						{
							// Set timer to interrupt the call if the call hangs.
							timer.schedule(interruptTask, MsAllowedForClientToRespond);
								// interrupt after MsAllowedForClientToRespond ms.
							
							if (OperationMode.PrintOutgoingPeerNotices)
							{
								GlobalConsole.println("Sending " + notice.getClass().getName() +
									" to clients");
							}
							
							// Notify the client by calling its callback object.
							peerListener.notify(notice);
						}
						catch (InconsistencyError ie)
						{
							interruptTask.cancel();
							GlobalConsole.println(
								"Inconsistency error while notifying client; Node Id =" + 
									notice.getPeerId());
							GlobalConsole.printStackTrace(ie);
						}
						catch (NotInterested ni)
						{
							interruptTask.cancel();
							
							// Remove the client's subscription to the Node.
							try { listenerRegistrar.subscribe(notice.getPeerId(), false); }
							catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
						}
						catch (ConnectException ce)
						{
							// The client must have disconnected, or the callback
							// connection lost for another reason. Remove the callback.
							
							interruptTask.cancel();
							GlobalConsole.println("Connection error while notifying client while sending " +
								notice.getClass().getName() + "; Node Id =" + 
								notice.getPeerId());
							listenerRegistrars.remove(listenerRegistrar);
							modelEngine.unregisterPeerListener(peerListener);
						}
						catch (IOException ioex)
						{
							interruptTask.cancel();
							GlobalConsole.println("IO error while notifying client while sending " +
								notice.getClass().getName() + "; Node Id =" + 
								notice.getPeerId());
							GlobalConsole.printStackTrace(ioex);
							
							// Remove the Registrar from the list: most likely the
							// View associated with the Registrar has been closed.
							
							listenerRegistrars.remove(listenerRegistrar);
							modelEngine.unregisterPeerListener(peerListener);
						}
						catch (Throwable t)
						{
							interruptTask.cancel();
							GlobalConsole.println("Error while notifying client; Node Id =" + 
								notice.getPeerId());
							GlobalConsole.printStackTrace(t);
							
							// Remove the Registrar from the list: most likely the
							// View associated with the Registrar has been closed.
							
							listenerRegistrars.remove(listenerRegistrar);
							modelEngine.unregisterPeerListener(peerListener);
						}
						finally
						{
							timer.purge();
							interruptTask.cancel();
							Thread.interrupted();  // clear interrupt status.
						}
					}
					
					timer.cancel();  // close the Timer permanently.
				}
			}
			catch (Throwable t)
			{
				GlobalConsole.printStackTrace(t);
			}
		}
		
		
		/**
		 * Remove all Notices from the queue that are not needed because there are
		 * more recent notices in the queue that have equivalent but more recent
		 * information. A notice can be removed (is 'obsolete') if it is a 
		 * ScenarioSimProgressNotice and it has the same requestId, the same peerId,
		 * and the same simNodeId as a later ScenarioSimProgressNotice.
		 * Note: This method need not be synchronized because the only call
		 * to it synchronizes on this Thread.
		 */
		 
		void removeObsoleteNotices()
		{
			synchronized (notices)
			{
				PeerNotice[] noticesCopy = notices.toArray(new PeerNotice[notices.size()]);
				int position = 0;
				for (PeerNotice notice : noticesCopy)
					// each Notice notice in queue, from the most recent,
				{
					if (notice instanceof PeerNoticeBase.ScenarioSimProgressNotice)
					{
						PeerNoticeBase.ScenarioSimProgressNotice scenSimProgNotice =
							(PeerNoticeBase.ScenarioSimProgressNotice)notice;
							
						for (int i = position+1; ; i++)
						{
							if (i >= noticesCopy.length) break;
							
							if (! (noticesCopy[i] instanceof 
								PeerNoticeBase.ScenarioSimProgressNotice)) continue;
							
							PeerNoticeBase.ScenarioSimProgressNotice sspn = 
								(PeerNoticeBase.ScenarioSimProgressNotice)(noticesCopy[i]);
								
							if (! sspn.requestId.equals(scenSimProgNotice.requestId)) continue;
							if (! sspn.peerId.equals(scenSimProgNotice.peerId)) continue;
							if (! sspn.simNodeId.equals(scenSimProgNotice.simNodeId)) continue;
							
							// Remove notice from the queue
							notices.remove(notice);
							
							if (OperationMode.PrintDequeuedNotices)
							{
								GlobalConsole.println("Deleting obsolete " + notice.getClass().getName());
							}
							
							break;
						}
					}
					
					position++;
				}
			}
		}
	}
	
	
	public static final long MsAllowedForClientToRespond = 200;
	
	
	static class InterruptTask extends TimerTask
	{
		private Thread threadToInterrupt;
		
		
		public InterruptTask(Thread threadToInterrupt)
		{
			this.threadToInterrupt = threadToInterrupt;
		}
		
		
		public void run()
		{
			threadToInterrupt.interrupt();
		}
	}
}

