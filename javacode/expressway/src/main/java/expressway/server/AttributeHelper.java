/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.util.Set;
import java.io.Serializable;
import java.io.IOException;
import java.io.PrintWriter;


public class AttributeHelper
{
	public static void writeAsXML(Attribute attr, PrintWriter writer, int indentation,
		PersistentNode appliesTo)
	throws
		IOException
	{
		if (attr.isPredefined()) return;

		String indentString = PersistentNodeImpl.getIndentString(indentation);
		String indentString2 = PersistentNodeImpl.getIndentString(indentation+1);
		
		if (attr.getName() == null) throw new IOException(new Exception(
			"Attribute name is null; parent is " + attr.getParent().getFullName()));
		
		Serializable defaultValue = attr.getDefaultValue();
		
		writer.println(indentString + "<attribute name=\"" + attr.getName() + "\" "
			+ (appliesTo == null? "" : "for=\"" + appliesTo.getName() + "\" ")
			+ (defaultValue == null? "" : "default=\"" + defaultValue + "\" ")
			+ (defaultValue == null? "" : 
				(isParsableJavaType(defaultValue) ? "" : "default_type=\"" 
					+ defaultValue.getClass().getName() + "\" "))
			+ (attr.isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + attr.getX() + "\" "
			+ "y=\"" + attr.getY() + "\" >");
				
		
		// Write each Attribute of this Attribute.
		
		Set<Attribute> attrs = attr.getAttributes();
		for (Attribute attr2 : attrs) attr2.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</attribute>");
	}
	

	/**
	 * Return true if the specified Object is of a type that is allowed in an
	 * Attribute expression.
	 */
	 
	protected static boolean isParsableJavaType(Object obj)
	{
		return (obj instanceof String) || (obj instanceof Number) || (obj instanceof Character);
	}
}

