/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.MathException;
import expressway.ser.SwitchSer;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.io.PrintWriter;
import java.io.IOException;
import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;


public class SwitchImpl extends AbstractSwitch
{
	/**
	 * Constructor.
	 */

	public SwitchImpl(String name, ModelContainer parent, ModelDomain domain,
		boolean conducting)
	{
		super(name, parent, domain, conducting);
	}
	
	
	public SwitchImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.SwitchIconImageName);
	}
	
	
	public Class getSerClass() { return SwitchSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		// Set Ser fields.
		
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		SwitchImpl newInstance = (SwitchImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		String indentStrin2g = getIndentString(indentation+1);
		
		writer.println(indentString + "<switch name=\"" + this.getName() + "\" "
			+ "start_conducting=\"" + this.getDefaultStartConducting() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\">");
		
		
		writeXMLAttributesForPredefElements(writer, indentation+1);


		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);
		
		writer.println(indentString + "</switch>");
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
	}
}


class SwitchNativeImpl extends AbstractSwitchNativeImpl
{
}
