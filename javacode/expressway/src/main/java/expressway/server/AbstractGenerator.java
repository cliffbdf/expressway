/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.ser.*;
import expressway.generalpurpose.DateAndTimeUtils;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.MathException;

import expressway.statistics.DeterministicDistribution;
import expressway.statistics.NormalizedGammaDistribution;
import java.util.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;
import expressway.generalpurpose.TreeSetNullDisallowed;



public abstract class AbstractGenerator extends ActivityBase implements Generator
{
	public static final double DefaultTimeDistShape = 2.0;
	public static final double DefaultTimeDistScale = 1.0;
	public static final double DefaultValueDistShape = 2.0;
	public static final double DefaultValueDistScale = 1.0;
	public static final boolean DefaultIgnoreStartup = false;
	
	
	public static final AttributeValidator GenShapeParamValidator = new AttributeValidatorBase()
	{
		public synchronized void validate(Serializable newValue)
		throws
			ParameterError
		{
			if (! (newValue instanceof Number)) throw new ParameterError(
				"Generator parameter value must be a Number");
			
			Double value = ((Number)newValue).doubleValue();
			
			if (value < 0) throw new ParameterError(
				"Generator shape parameter value must be non-negative");
		}
	};


	public static final AttributeValidator GenScaleParamValidator = new AttributeValidatorBase()
	{
		public synchronized void validate(Serializable newValue)
		throws
			ParameterError
		{
			if (! (newValue instanceof Number)) throw new ParameterError(
				"Generator parameter value must be a Number");
			
			Double value = ((Number)newValue).doubleValue();
			
			if (value < 0) throw new ParameterError(
				"Generator scale parameter value must be non-negative");
		}
	};


	/** reference only. */
	public Port inputPort = null;

	/** reference only. */
	public Port outputPort = null;
	
	/** reference only. */
	public Port countPort = null;

	/** reference only. */
	public State state = null;
	
	/** reference only. */
	public State countState = null;
	
	/** reference only. */
	public ModelAttribute timeShapeAttr = null;
	
	/** reference only. */
	public ModelAttribute timeScaleAttr = null;
	
	/** reference only. */
	public ModelAttribute valueShapeAttr = null;
	
	/** reference only. */
	public ModelAttribute valueScaleAttr = null;
	
	public boolean ignoreStartup = DefaultIgnoreStartup;

	
	private double defaultTimeDistShape = DefaultTimeDistShape;
	private double defaultTimeDistScale = DefaultTimeDistScale;
	private double defaultValueDistShape = DefaultValueDistShape;
	private double defaultValueDistScale = DefaultValueDistScale;
	
	
	// The following are set by externalize(), prior to serialization.
	
	public transient String inputPortNodeId = null;
	public transient String outputPortNodeId = null;
	public transient String countPortNodeId = null;
	public transient String stateNodeId = null;
	public transient String countStateNodeId = null;
	
	public transient String timeShapeAttrNodeId = null;
	public transient String timeScaleAttrNodeId = null;
	public transient String valueShapeAttrNodeId = null;
	public transient String valueScaleAttrNodeId = null;
	
	
	public String getInputPortNodeId() { return inputPortNodeId; }
	public String getOutputPortNodeId() { return outputPortNodeId; }
	public String getCountPortNodeId() { return countPortNodeId; }
	public String getStateNodeId() { return stateNodeId; }
	public String getCountStateNodeId() { return countStateNodeId; }
	public String getTimeShapeAttrNodeId() { return timeShapeAttrNodeId; }
	public String getTimeScaleAttrNodeId() { return timeScaleAttrNodeId; }
	public String getValueShapeAttrNodeId() { return valueShapeAttrNodeId; }
	public String getValueScaleAttrNodeId() { return valueScaleAttrNodeId; }


	public String setNameAndNotify(String newName)
	throws
		ParameterError
	{
		String oldRepCondName = ModelContainerImpl.getRepeatingConduitName(this);
		
		String oldName = super.setNameAndNotify(newName);
		
		
		// Rename the Repeating Conduit (if any).
		
		ModelElement child;
		ModelContainer genContainer = this.getModelContainer();
		
		try { child = genContainer.getSubcomponent(oldRepCondName); }
		catch (ElementNotFound ex) { return oldName; }
		
		if (! (child instanceof Conduit)) throw new RuntimeException(
			"A Model Element exists with the repeating pattern name but is not a Conduit.");
		
		Conduit conduit = (Conduit)child;
		String newRepCondName = ModelContainerImpl.getRepeatingConduitName(this);
		conduit.setNameAndNotify(newRepCondName);
		
		return oldName;
	}


	public Class getSerClass() { return AbstractGeneratorSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		inputPortNodeId = getNodeIdOrNull(inputPort);
		outputPortNodeId = getNodeIdOrNull(outputPort);
		countPortNodeId = getNodeIdOrNull(countPort);
		stateNodeId = getNodeIdOrNull(state);
		countStateNodeId = getNodeIdOrNull(countState);
		
		timeShapeAttrNodeId = getNodeIdOrNull(timeShapeAttr);
		timeScaleAttrNodeId = getNodeIdOrNull(timeScaleAttr);
		valueShapeAttrNodeId = getNodeIdOrNull(valueShapeAttr);
		valueScaleAttrNodeId = getNodeIdOrNull(valueScaleAttr);
		
		
		// Set Ser fields.
		
		((AbstractGeneratorSer)nodeSer).inputPortNodeId = this.getInputPortNodeId();
		((AbstractGeneratorSer)nodeSer).outputPortNodeId = this.getOutputPortNodeId();
		((AbstractGeneratorSer)nodeSer).countPortNodeId = this.getCountPortNodeId();
		((AbstractGeneratorSer)nodeSer).stateNodeId = this.getStateNodeId();
		((AbstractGeneratorSer)nodeSer).countStateNodeId = this.getCountStateNodeId();
		
		((AbstractGeneratorSer)nodeSer).timeShapeAttrNodeId = this.getTimeShapeAttrNodeId();
		((AbstractGeneratorSer)nodeSer).timeScaleAttrNodeId = this.getTimeScaleAttrNodeId();
		((AbstractGeneratorSer)nodeSer).valueShapeAttrNodeId = this.getValueShapeAttrNodeId();
		((AbstractGeneratorSer)nodeSer).valueScaleAttrNodeId = this.getValueScaleAttrNodeId();
		
		((AbstractGeneratorSer)nodeSer).defaultTimeDistShape = this.getDefaultTimeDistShape();
		((AbstractGeneratorSer)nodeSer).defaultTimeDistScale = this.getDefaultTimeDistScale();
		((AbstractGeneratorSer)nodeSer).defaultValueDistShape = this.getDefaultValueDistShape();
		((AbstractGeneratorSer)nodeSer).defaultValueDistScale = this.getDefaultValueDistScale();
		
		((AbstractGeneratorSer)nodeSer).ignoreStartup = this.getIgnoreStartup();
		
		
		//System.out.println("Checking if gen " + d.getNodeId() + " is repeating"
		//	+ ", parent ModelContainer=" + d.getModelContainer().getNodeId());
			
		try { ((AbstractGeneratorSer)nodeSer).repeating = this.getModelContainer().isGeneratorRepeating(this); }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		AbstractGenerator newGen = (AbstractGenerator)(super.clone(cloneMap, cloneParent));
		
		newGen.inputPort = cloneMap.getClonedNode(inputPort);
		newGen.outputPort = cloneMap.getClonedNode(outputPort);
		newGen.countPort = cloneMap.getClonedNode(countPort);
		newGen.state = cloneMap.getClonedNode(state);
		newGen.countState = cloneMap.getClonedNode(countState);
		newGen.timeShapeAttr = cloneMap.getClonedNode(timeShapeAttr);
		newGen.timeScaleAttr = cloneMap.getClonedNode(timeScaleAttr);
		newGen.valueShapeAttr = cloneMap.getClonedNode(valueShapeAttr);
		newGen.valueScaleAttr = cloneMap.getClonedNode(valueScaleAttr);
		
		return newGen;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		boolean repeating = true;
		try { getModelContainer().getSubcomponent(ModelContainerImpl.getRepeatingConduitName(this)); }
		catch (ElementNotFound ex) { repeating = false; }
		
		writer.println(indentString + "<generator name=\"" + this.getName() + "\" "
			+ "x=\"" + this.getX() + "\" y=\"" + this.getY() + "\"");
		writer.println(indentString2 + "ignore_startup=\"" + this.getIgnoreStartup() + "\"");
		writer.println(indentString2 + "pulse=\"" + (this instanceof PulseGenerator) + "\"");
		writer.println(indentString2 + "variable=\"" + (this instanceof VariableGenerator) + "\"");
		writer.println(indentString2 + "repeating=\"" + repeating + "\"");
		writer.println(indentString2 + (this.getDefaultTimeDistShape() == 0.0 ?
				("time=\"" + this.getDefaultTimeDistScale() + "\" ")
				:
				("time_shape=\"" + this.getDefaultTimeDistShape() + "\" "
					+ "time_scale=\"" + this.getDefaultTimeDistScale() + "\" ")
				));
		writer.println(indentString2 + (this.getDefaultValueDistShape() == 0.0 ?
				("value=\"" + this.getDefaultValueDistScale() + "\" ")
				:
				("value_shape=\"" + this.getDefaultValueDistShape() + "\" "
					+ "value_scale=\"" + this.getDefaultValueDistScale() + "\" ")
				)
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ ">");
		
		
		writeXMLAttributesForPredefElements(writer, indentation+1);

		
		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);
		
		
		writer.println(indentString + "</generator>");
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(inputPort, null);
		deletePort(outputPort, null);
		deletePort(countPort, null);
		deleteState(state, null);
		deleteState(countState, null);
		deleteAttribute(timeShapeAttr, null);
		deleteAttribute(timeScaleAttr, null);
		deleteAttribute(valueShapeAttr, null);
		deleteAttribute(valueScaleAttr, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		inputPort = null;
		outputPort = null;
		countPort = null;
		state = null;
		countState = null;
		timeShapeAttr = null;
		timeScaleAttr = null;
		valueShapeAttr = null;
		valueScaleAttr = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public AbstractGenerator(String name, ModelContainer parent, ModelDomain domain,
		double timeDistShape, double timeDistScale, double valueDistShape, 
		double valueDistScale, boolean ignoreStartup)
	{
		super(name, parent, domain);
		
		init(name, parent, domain, timeDistShape, timeDistScale, valueDistShape,
			valueDistScale, ignoreStartup);
	}


	public AbstractGenerator(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init(this.getName(), parent, this.getModelDomain(),
			DefaultTimeDistShape, DefaultTimeDistScale,
			DefaultValueDistShape, DefaultValueDistScale, DefaultIgnoreStartup);
	}
	
	
	public String getTagName() { return "generator"; }
	
	
	private void init(String name, ModelContainer parent, ModelDomain domain,
		double timeDistShape, double timeDistScale, double valueDistShape, 
		double valueDistScale, boolean ignoreStartup)
	{
		setResizable(false);
		this.setLayoutManager(DefaultLayoutManager);
		//setUseBorderWhenIconified(false);
		
		this.defaultTimeDistShape = timeDistShape;
		this.defaultTimeDistScale = timeDistScale;
		this.defaultValueDistShape = valueDistShape;
		this.defaultValueDistScale = valueDistScale;
		
		this.ignoreStartup = ignoreStartup;
		
		try
		{
			this.timeShapeAttr = createModelAttribute("time_shape", new Double(timeDistShape), this, null);
			this.timeScaleAttr = createModelAttribute("time_scale", new Double(timeDistScale), this, null);
			this.valueShapeAttr = createModelAttribute("value_shape", new Double(valueDistShape), this, null);
			this.valueScaleAttr = createModelAttribute("value_scale", new Double(valueDistScale), this, null);
		
			inputPort = super.createPort("input_port", PortDirectionType.input, Position.left, this, null);
			outputPort = super.createPort("output_port", PortDirectionType.output, Position.right, this, null);
			countPort = super.createPort("count_port", PortDirectionType.output, Position.bottom, this, null);
			
			countPort.setVisible(false);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}

		
		this.timeShapeAttr.setValidator(GenShapeParamValidator);
		this.timeScaleAttr.setValidator(GenScaleParamValidator);
		this.valueShapeAttr.setValidator(GenShapeParamValidator);
		this.valueScaleAttr.setValidator(GenScaleParamValidator);
		
		
		// Automatically bind the output Port to the internal State.

		try
		{
			state = super.createState("GeneratorState", this, null);
			countState = super.createState("CountState", this, null);
			
			countState.setVisible(false);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}


		try
		{
			state.bindPort(outputPort);
			countState.bindPort(countPort);
		}
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		inputPort.setPredefined(true);
		outputPort.setPredefined(true);
		countPort.setPredefined(true);
		state.setPredefined(true);
		countState.setPredefined(true);
		timeShapeAttr.setPredefined(true);
		timeScaleAttr.setPredefined(true);
		valueShapeAttr.setPredefined(true);
		valueScaleAttr.setPredefined(true);
		
		inputPort.setMovable(false);
		outputPort.setMovable(false);
		countPort.setMovable(false);
		state.setMovable(false);
		countState.setMovable(false);
		timeShapeAttr.setMovable(false);
		timeScaleAttr.setMovable(false);
		valueShapeAttr.setMovable(false);
		valueScaleAttr.setMovable(false);
		
		countPort.setVisible(false);
	}
	
	
	public double getMinimumWidth() { return 180; }
	
	
	public int getNoOfHeaderRows() { return 5; }
	
	
	private static final double PreferredWidthInPixels = 180.0;  // 50
	private static final double PreferredHeightInPixels = 100.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		SortedSet<PersistentNode> nodes = super.getPredefinedNodes();
		nodes.remove(state);
		return nodes;
	}
	
	
	public void layout()
	{
		super.layout();
		
		double x = 2.0 * this.getWidth()/3.0 - state.getWidth()/2.0;
		state.setX(x);
		state.setY(2.0 * this.getHeight()/3.0 - state.getHeight()/2.0);
		countState.setX(x);
		countState.setY(this.getHeight()/3.0 - countState.getHeight()/2.0);
	}
	

	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change Port direction for a standard component");
	}


	public Port getInputPort() { return inputPort; }
	public Port getOutputPort() { return outputPort; }
	public Port getCountPort() { return countPort; }
	
	public double getDefaultTimeDistShape() { return defaultTimeDistShape; }
	public double getDefaultTimeDistScale() { return defaultTimeDistScale; }
	public double getDefaultValueDistShape() { return defaultValueDistShape; }
	public double getDefaultValueDistScale() { return defaultValueDistScale; }
	
	public boolean getIgnoreStartup() { return ignoreStartup; }
	public void setIgnoreStartup(boolean ignore) { this.ignoreStartup = ignore; }
	
	
	public ModelAttribute getTimeShapeAttr() { return timeShapeAttr; }
	public ModelAttribute getTimeScaleAttr() { return timeScaleAttr; }
	public ModelAttribute getValueShapeAttr() { return valueShapeAttr; }
	public ModelAttribute getValueScaleAttr() { return valueScaleAttr; }
}


class AbstractGeneratorNativeImpl extends ActivityNativeImplBase
{
	private State state = null;
	private String stateName = "GeneratorState";

	private State countState = null;
	private String countStateName = "CountState";
	
	private double timeDistScale;
	private double valueDistScale;

	private ContinuousDistribution timeDistribution = null;
	private ContinuousDistribution valueDistribution = null;
	
	boolean hasReceivedNonNullEvent = false;
	boolean hasBeenActivatedOnce = false;
	Double nextCount = null;
	
	boolean stopped = false;
	
	private Serializable priorInputValue = null;  // the most recently received input
		// event value.


	public synchronized void start(ActivityContext context) throws Exception
	{
		if (stopped) throw new RuntimeException("Already stopped");
		super.start(context);

		state = context.getState(stateName);
		countState = context.getState(countStateName);

		//System.out.println("Initializing distributions for " + getGenerator().getName());
		
		this.timeDistScale = getAttributeDoubleValue(getGenerator().getTimeScaleAttr());
		this.valueDistScale = getAttributeDoubleValue(getGenerator().getValueScaleAttr());
		
		initializeTimeDistribution();
		initializeValueDistribution();
	}


	public synchronized void stop()
	{
		stopped = true;
		timeDistribution = null;
		valueDistribution = null;

		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		respond(events, false);
	}
	
	
	protected synchronized void respond(SortedEventSet<GeneratedEvent> events, 
		boolean eventsWereDeleted)
	throws
		ModelContainsError
	{
		super.respond(events);
		
		if ((events.size() == 0) && getGenerator().getIgnoreStartup()) return;
		
		if (events.size() == 0) hasBeenActivatedOnce = true;  // startup.
		

		// Check if the input port has an event that we care about.
		
		Serializable inVal = null;
		Number num = null;
		boolean foundRisingValue = false;
		
		if (portHasNonCompensatingEvent(events, getInputPort()))
		{
			// Get the input value (if any).
			
			inVal = getActivityContext().getStateValue(getInputPort());
	
			
			// Check if the input value is null: if so, verify that we have not already
			// received a non-null event, since once we receive a non-null event we
			// should not receive any more.
			
			if (inVal == null)
			{
				if (hasReceivedNonNullEvent) throw new ModelContainsError(
					"Generator has received a null-valued event after having " +
						"previously receiving a non-null valued event");
						
				return; // do not respond to null-valued events.
			}
			else if (! (inVal instanceof Number)) throw new ModelContainsError(
				"Input to Generator must be numeric");
			else
			{
				hasReceivedNonNullEvent = true;
				hasBeenActivatedOnce = true;
			}
		
			// Get the numeric value.
			
			try { num = (Number)inVal; }
			catch (ClassCastException cce) { throw new ModelContainsError(
				"Generator input event must be numeric"); }
				
			// Check for rising value.
			
			if (priorInputValue == null)
				
				foundRisingValue = true;
			
			else if (num.doubleValue() > ((Number)priorInputValue).doubleValue())
				foundRisingValue = true;


			// Save the input value so that we can check it on next time around.
			
			priorInputValue = inVal;  // Note that if Numbers were mutable we
										// would have to copy the value.
		}
		
		
		/*
		 * Continue if:
			Startup: no Events, no Events were deleted, and no rising value; or
		 	Events were deleted; or
			Found a rising value on the input Port.
		 */
		
		if (eventsWereDeleted ||
			foundRisingValue ||
			((events.size() == 0) && (! eventsWereDeleted) && (! foundRisingValue)))
		{
			  // proceed.
		}
		else
		{
			return;
		}
		
		// Active for this epoch.
		
		
		// Compute the time and value of the event to generate.
		
		double timeDelayDays = 0;
		double newDoubleValue = 0;

		try
		{
			if (timeDistribution == null)
			{
				throw new RuntimeException("Time dist is null for " + getGenerator().getName());
			}
			
			double r = getModelContext().getSimulationRun().getRandomGenerator().nextDouble();
			timeDelayDays = getUpdatedTimeDistribution(events).inverseCumulativeProbability(
				r);
				
			newDoubleValue = getUpdatedValueDistribution(events).inverseCumulativeProbability(
				getModelContext().getSimulationRun().getRandomGenerator().nextDouble());
		}
		catch (MathException ex)
		{
			throw new ModelContainsError(
				"When computing inverse cumulative probability;", ex);
		}
		catch (Throwable t) { GlobalConsole.printStackTrace(t); return; }
		

		long timeDelayMs = (long)(timeDelayDays * DateAndTimeUtils.MsInADay);
		//System.out.println("*H; timeDelayMs=" + timeDelayMs);
		//System.out.println("timeDelayDays=" + timeDelayDays);

		
		// Schedule future events, for the output and the count.
		
		GeneratedEvent event = null;
		GeneratedEvent countEvent = null;
		try
		{
			//System.out.println("*I");
			Double newCount = null;
			
			if (num == null)
			{
				if (nextCount == null)
				{
					if (! ((events.size() == 0) && (! eventsWereDeleted) && (! foundRisingValue)))
						throw new RuntimeException("No next count");
						
					newCount = new Double(1);
				}
				else
				{
					newCount = nextCount;
				}
			}
			else
			{
				newCount = new Double(num.doubleValue() + 1);
			}
			
			
			getActivityContext().purgeFutureEvents();
			
			event = getActivityContext().scheduleDelayedStateChange(
					this.state, timeDelayMs, new Double(newDoubleValue), events);
					
			notifyOfScheduleEventOnGeneratorState(event);
					
			countEvent = getActivityContext().scheduleDelayedStateChange(
					this.countState, timeDelayMs, newCount, events);
					
			nextCount = newCount;
		}
		catch (ParameterError ex)
		{
			throw new ModelContainsError(
				"When scheduling delayed state change", ex);
		}
	}
	
	
	/**
	 * Allows subclasses to be notified of newly scheduled Events by this object.
	 */
	 
	protected synchronized void notifyOfScheduleEventOnGeneratorState(Event event)
	{
	}


	protected State getState() { return state; }
	
	protected String getStateName() { return stateName; }
	
	protected ContinuousDistribution getTimeDistribution() { return timeDistribution; }

	protected ContinuousDistribution getValueDistribution() { return valueDistribution; }

	protected void setTimeDistribution(ContinuousDistribution timeDistribution)
	{
		this.timeDistribution = timeDistribution;
	}

	protected void setValueDistribution(ContinuousDistribution valueDistribution)
	{
		this.valueDistribution = valueDistribution;
	}
	
	protected ContinuousDistribution getUpdatedTimeDistribution(
		SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{ return timeDistribution; }

	protected ContinuousDistribution getUpdatedValueDistribution(
		SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{ return valueDistribution; }

	
	/* From User Manual:
	
		"The shape and scale values entered in a <generator> statement are default 
		values: they apply only if they are not overridden by a Model Scenario 
		<attribute_value> statement."
		
			Implementation:
			1. The initialize<>Distribution() methods below should obtain the
			initial value of a distribution parameter value from the Scenario value
			of the Attribute.
			2. The default value of a Generator distribution attribute must be 
			obtained from the Generator's constructor.
		
		"If a Generator is variable, then the value of a time_scale or value_scale
		Attribute only applies until the time_scale or value_scale Port is driven,
		respectively."
		
			Implementation:
			1. At startup, copy scale Attribute values to internal scale variables,
			and use those variables for distribution initialization (the methods
			below.)
			2. When an Event is received through a scale Port, update the associated
			scale parameter.
	 */
	

	protected void initializeTimeDistribution()
	{
		double timeDistShape = getAttributeDoubleValue(getGenerator().getTimeShapeAttr());
		//double timeDistScale = getAttributeValue(getGenerator().getTimeScaleAttr());
		// Note: The value of the above two Attributes must be set to the default
		// values for the distributions, unless they are overridden by a ModelScenario.
		
		createTimeDistribution(timeDistShape, this.timeDistScale);
	}

	protected void initializeValueDistribution()
	{
		double valueDistShape = getAttributeDoubleValue(getGenerator().getValueShapeAttr());
		//double valueDistScale = getAttributeValue(getGenerator().getValueScaleAttr());
		// Note: The value of the above two Attributes must be set to the default
		// values for the distributions, unless they are overridden by a ModelScenario.
		
		createValueDistribution(valueDistShape, valueDistScale);
	}
	
	
	protected void createTimeDistribution(double timeDistShape, double timeDistScale)
	{
		if (timeDistShape == 0)
			timeDistribution = new DeterministicDistribution(timeDistScale);
		else
			timeDistribution = 
				new NormalizedGammaDistribution(timeDistShape, timeDistScale);
				//getGenerator().getTimeDistFactory().createGammaDistribution(
				//	timeDistShape, timeDistScale);
				
		if (timeDistribution == null) throw new RuntimeException(
			"Time distribution initialized to null");
	}
	
	
	protected void createValueDistribution(double valueDistShape, double valueDistScale)
	{
		if (valueDistShape == 0)
			valueDistribution = new DeterministicDistribution(valueDistScale);
		else
			valueDistribution = 
				new NormalizedGammaDistribution(valueDistShape, valueDistScale);
				//getGenerator().getValueDistFactory().createGammaDistribution(
				//	valueDistShape, valueDistScale);
	}
	
	
	protected AbstractGenerator getGenerator() { return (AbstractGenerator)(getActivity()); }
	protected Port getInputPort() { return getGenerator().getInputPort(); }
	protected Port getOutputPort() { return getGenerator().getOutputPort(); }
	protected Port getCountPort() { return getGenerator().getCountPort(); }
	
	protected void setTimeDistScale(double newScale) { this.timeDistScale = newScale; }
	protected void setValueDistScale(double newScale) { this.valueDistScale = newScale; }
	
	protected double getTimeDistScale() {  return this.timeDistScale; }
	protected double getValueDistScale() {  return this.valueDistScale; }
	
	protected double getTimeDistShape() { return getAttributeDoubleValue(getGenerator().getTimeShapeAttr()); }
	protected double getValueDistShape() { return getAttributeDoubleValue(getGenerator().getValueShapeAttr()); }
}
