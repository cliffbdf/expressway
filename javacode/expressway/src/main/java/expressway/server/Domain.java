/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.Serializable;
import java.util.Date;
import java.net.URL;


/**
 * The base type for any kind of model domain or decision domain.
 */

public interface Domain
extends
	PersistentListNode,  // of core elements
	MenuOwner
{
	void setDomain(Domain d);
	void setScenarios(List<Scenario> ss);
	void setMenuItems(List<MenuItem> mis);
	void setMotifDefs(Set<MotifDef> mds);
	void setScenarioSets(List<ScenarioSet> sss);
	void setNamedReferences(Set<NamedReference> nrs);
	void setVisualGuidance(VisualGuidance vg);


	String getSourceName();
	
	void setSourceName(String name);
	
	URL getSourceURL();
	
	void setSourceURL(URL url);
	
	String getDeclaredName();
	
	void setDeclaredName(String name);
	
	String getDeclaredVersion();
	
	void setDeclaredVersion(String version);
	
	String getDefaultDomainViewTypeName();
	
	
	/** Return the MotifDefs that are available to this Domain. */
	
	Set<MotifDef> getMotifDefs();
	
	boolean usesMotif(MotifDef md);
		
	void addMotifDef(MotifDef md);
		
	void removeMotifDef(MotifDef md);
	
	
	/** Ownership */
	Set<NamedReference> getNamedReferences();
	
	NamedReference getNamedReference(String nrName);
	
	NamedReference createNamedReference(String name, String desc, Class fromType, Class toType)
	throws
		ParameterError;
	
	void deleteNamedReference(NamedReference nr);
		
		
	VisualGuidance getVisualGuidance();
	
		
	/**
	 * Return this Domains's Scenarios. If it has none, return an empty Set.
	 * Must return the actual backed List.
	 */
	 
	List<Scenario> getScenarios();
	
	
	Scenario getScenario(String name)
	throws
		ElementNotFound, // if the Scenario cannot be found.
		ParameterError;

	
	Scenario getCurrentScenario();
	
	
	void setCurrentScenario(Scenario scenario)
	throws
		ElementNotFound,
		ParameterError;
	

	/**
	 * Create a new Scenario with the specified name. If the name is null,
	 * create a unique name for the new Scenario.
	 */

	Scenario createScenario(String name)
	throws
		ParameterError;  // if the Context already contsins a Scenario for this Domain.
		

	/**
	 * Create a new Scenario with the specified name.
	 * Create a unique name for the new Scenario.
	 */

	Scenario createScenario()
	throws
		ParameterError;  // if the Context already contsins a Scenario for this Domain.
		

	/**
	 * Create a new Scenario. Attributes from the specified scenario
	 * are copied to the new one. If the specified scenario is null, create
	 * a scenario using default or null values. If the specified Scenario
	 * name is null, create a unique name. 'scenario' may be null.
	 */

	Scenario createScenario(String name, Scenario scenario)
	throws
		ParameterError,  // if the Context already contsins a Scenario for this Domain.
		CloneNotSupportedException;  // if unable to copy scenario values.


	void deleteScenario(Scenario scenario)
	throws
		ParameterError;  // if this Domain does not own the Scenario.
	
	
	/**
	 * Used by Scenario.createIndependentCopy.
	 */
	 
	void addScenario(Scenario scenario);
	
	
	/**
	 * Create a ScenarioSet under this Domain. One Scenario is
	 * also created for each of the elements in 'values', based on the base Scenario.
	 * 'values' may be null, in which case no Scenarios are created.
	 */
	 
	ScenarioSet createScenarioSet(String name, Scenario baseScenario,
		Attribute attrToVary, Serializable[] values)
	throws
		ParameterError,
		CloneNotSupportedException;
		
	
	void deleteScenarioSet(ScenarioSet scenarioSet)
	throws
		ParameterError;  // if this Domain does not own the ScenarioSet.
		
		
	void addScenarioSet(ScenarioSet scenarioSet);
	
	
	/** Must return the actual backed List. */
	List<ScenarioSet> getScenarioSets();
	
	
	ScenarioSet getScenarioSet(String name)
	throws
		ElementNotFound;
}
