/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.MathException;
import expressway.server.Event.*;
import expressway.ser.TallySer;
import expressway.server.ModelElement.*;
import java.util.*;
import java.io.PrintWriter;
import java.io.IOException;
import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;


public class TallyImpl extends AbstractTally
{
	/**
	 * Constructor.
	 */

	public TallyImpl(String name, ModelContainer parent, ModelDomain domain,
		Number initValue, String[] inputPortNames, boolean doubleCount)
	{
		super(name, parent, domain, initValue, inputPortNames, doubleCount);
		
		//try { state = createState("Total"); }
		//catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public TallyImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.TallyIconImageName);
	}
	
	
	public Class getSerClass() { return TallySer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		// Set Ser fields.
		
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		TallyImpl newInstance = (TallyImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<tally name=\"" + this.getName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
			
		
		List<Port> inputPorts = this.getInputPorts();
		for (Port port : inputPorts) writer.println(indentString2 +
			"<input_port name=\"" + port.getName() + "\"/>");
		
		
		writeXMLAttributesForPredefElements(writer, indentation+1);


		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);
		
		
		writer.println(indentString + "</tally>");
	}		
}


class TallyNativeImpl extends AbstractTallyNativeImpl
{
}
