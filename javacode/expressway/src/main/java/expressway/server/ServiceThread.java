/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Vector;
import java.util.List;


/**
 * A thread, for performing client requests, that can be managed by the client.
 * The thread automatically
 * inherits the ServiceContext of the thread that creates it. 
 * The ServiceContext carries information about the nature of the request.
 * This context allows activities performed by the thread to access information
 * about the request without having to pass all of that information through
 * method arguments.
 */
	 
public interface ServiceThread
extends
	ProgressControl,  // to allow the client to manage the service thread.
	ProgressCallback  // to allow the service thread's objects to log progress.
{
	/**
	 * Obtain the progress messages that have been set by the thread via
	 * the ProgressCallback interface.
	 */
	 
	List<String> getProgress();
	

	/**
	 * Obtain the messages that have been set by the thread via
	 * the ProgressCallback interface.
	 */
	 
	List<String> getMessages();
	

	/**
	 * Obtain the completion status that has been set by the thread via
	 * the completed() method.
	 */
	 
	boolean isComplete();
	

	/**
	 * Obtain the abort status that has been set by the thread via
	 * the abort(...) method.
	 */
	 
	boolean isAborted();
	
		
	/**
	 * Signal the thread to abort gracefully. The thread is expected to call
	 * the checkAbort() method at various points, to see if an abort has been
	 * requested.
	 */
	 
	void abort();

	
	/**
	 * Block until this thread signals that it has completed in any manner.
	 */
	 
	void waitForFinish();
	
	
	void go();
	
	
	Integer getRequestHandle();
	
	
	ServiceContext getServiceContext();
}
