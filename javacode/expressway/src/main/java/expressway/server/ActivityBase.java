/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.List;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


/**
 * The base type for specialized Activities. See interface specification, {@link Activity}.
 */

public abstract class ActivityBase extends PortedContainerImpl implements Activity
{
	public Class nativeImplClass = null;
	
	
	public Class getSerClass() { return ActivitySer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ActivityBase newInstance = (ActivityBase)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	public ActivityBase(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
	}


	public ActivityBase(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
	}
	
	
	public void setNativeImplementationClass(Class implClass)
	throws
		ParameterError
	{
		//impl.setModelComponent(this);
		this.nativeImplClass = implClass;
		if (! NativeActivityImplementation.class.isAssignableFrom(implClass))
			throw new ParameterError(implClass.getName() +
				" is not a NativeActivityImplementation");

		// Add to the ModelDomain's list of native implementations.

		synchronized (getModelDomain())
		{
			getModelDomain().getComponentsWithNativeImpls().add(this);
		}
	}


	public Class getNativeImplementationClass() {
		return nativeImplClass; }


	/**
	 * See interface specification.
	 * This implementation is synchronized to protect the native state between the
	 * calls to respond(...) and getAndPurgeNewEvents(). Also, native implementations
	 * are not assumed to be re-entrant.
	 */

	public synchronized SortedEventSet<GeneratedEvent> respond(SimulationRun simRun, 
		SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		NativeComponentImplementation impl = simRun.getNativeImplementation(this);
		
		SortedEventSet<GeneratedEvent> treeSet = new SortedEventSetImpl<GeneratedEvent>();
		
		if (getActivities().isEmpty() && getFunctions().isEmpty())
		{
			// No sub-components:
		
			if (impl == null)
			{
				// Check if the Activity has pre-defined events.
				
				List<State> states = getStates();
				for (State state : states)
				{
					Set<PredefinedEvent> pes = state.getPredefinedEvents();
					if (! pes.isEmpty()) return treeSet;  // it does. Ok.
				}
				
				// Does not have pre-defined events.
				
				throw new ModelContainsError(getFullName() + 
					": No native impl., no sub-components, and no pre-defined events");
			}
			else try
			{
				impl.respond(events);

				return impl.getAndPurgeNewEvents();
			}
			catch (ModelContainsError mce)
			{
				throw mce;
			}
			catch (Throwable t)
			{
				throw new ModelContainsError(
					"Within native implementation of Activity " + getName(), t);
			}
		}
		else
		{
			// Has sub-components:
		
			if (impl == null) return treeSet;  // THIS Activity does not generate
										// any Events, but its sub-components might.
			else
				throw new ModelContainsError("cannot have sub-components AND a native impl.");
		}
	}


	public void addComponent(ModelComponent component)
	throws
		ParameterError
	{
		if (nativeImplClass != null) throw new ParameterError(
			"Cannot add a Component to an Activity that has a native implementation.");
		
		super.addComponent(component);
	}
	
	
	/*
	public void addConstraint(Constraint constraint)
	throws
		ParameterError
	{
		if (nativeImplClass != null) throw new ParameterError(
			"Cannot add a Constraint to an Activity that has a native implementation.");
		
		super.addConstraint(constraint);
	}
	*/
	
	
	public Function createFunction(String name, Class funcNativeImplClass,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (this.nativeImplClass != null) throw new ParameterError(
			"Cannot add a Function to an Activity that has a native implementation.");
		
		return super.createFunction(name, funcNativeImplClass, layoutBound,
			outermostAffectedRef);
	}
	
	
	public Terminal createTerminal(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (nativeImplClass != null) throw new ParameterError(
			"Cannot add a Terminal to an Activity that has a native implementation.");
		
		return super.createTerminal(name, layoutBound, outermostAffectedRef);
	}
	
	
	public Generator createGenerator(String name, boolean variable,
		double timeDistShape, double timeDistScale, double valueDistShape,
		double valueDistScale, boolean ignoreStartup, boolean pulse,
		PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (nativeImplClass != null) throw new ParameterError(
			"Cannot add a Generator to an Activity that has a native implementation.");
		
		return super.createGenerator(name, variable, timeDistShape, timeDistScale,
			valueDistShape, valueDistScale, ignoreStartup, pulse, layoutBound,
			outermostAffectedRef);
	}
	
	
	public Activity createActivity(String name, Class actNativeImplClass,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (this.nativeImplClass != null) throw new ParameterError(
			"Cannot add an Activity to an Activity that has a native implementation.");
		
		return super.createActivity(name, actNativeImplClass, layoutBound,
			outermostAffectedRef);
	}
	
	
	public Conduit createConduit(String name, Port portA, Port portB,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (nativeImplClass != null) throw new ParameterError(
			"Cannot add a Conduit to an Activity that has a native implementation.");
		
		return super.createConduit(name, portA, portB, layoutBound, outermostAffectedRef);
	}
	
	
	public Satisfies createSatisfiesRelation(String name, 
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (nativeImplClass != null) throw new ParameterError(
			"Cannot add a Relation to an Activity that has a native implementation.");
		
		return super.createSatisfiesRelation(name, layoutBound, outermostAffectedRef);
	}
	
	
	public Derives createDerivesRelation(String name, 
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (nativeImplClass != null) throw new ParameterError(
			"Cannot add a Relation to an Activity that has a native implementation.");
		
		return super.createDerivesRelation(name, layoutBound, outermostAffectedRef);
	}

	
	public String toString()
	{
		return super.toString();
	}


	static void dump(Set<Activity> activities, int indentation, String title)
	{
		for (int i = 1; i <= indentation; i++) GlobalConsole.print("\t");
		GlobalConsole.println(title);
		for (Activity activity : activities)
		{
			activity.dump(indentation+1);
		}
	}
}
