/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.OperationMode.*;
import expressway.common.ExpressionInterpreter.*;
import expressway.ser.*;
import expressway.server.Event.*;
import expressway.server.ModelElement.*;
import expressway.server.DecisionElement.*;
import expressway.tools.parser.*;
import expressway.tools.lexer.*;
import expressway.tools.node.*;
import expressway.tools.analysis.*;
import expressway.generalpurpose.DateAndTimeUtils;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.SortedSet;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Date;
import java.util.NoSuchElementException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;


public class SimulationRunImpl extends ModelElementImpl
	implements SimulationRun  // is Runnable.
	// is a SimRunSetChangeListener
{
	/* -------------------------------------------------------------------------
		Instance variables that represent the initial state of the simulation. */

	
	/** The SimRunSet that this SimulationRun is a member of, if any. May be null. */
	
	private SimRunSet simRunSet = null;
	
	
	/** Ownership.
		The values (versus time) used by this SimulationRun for the States of any
		bound Attributes that the Domain has. */
		
	private ValueHistory externalStateValues;
	
	public ValueHistory getExternalStateValues() { return this.externalStateValues; }
	
	void setExternalStateValues(ValueHistory vh) { this.externalStateValues = vh; }
		

	/** Ownership.
		User-specified value for the initial simulation time. May be null. */
	
	public Date startingDate = null;
	
	public Date getStartingDate() { return startingDate; }
	
	public void setStartingDate(Date d) { this.startingDate = d; }
	
	
	/** Ownership.
		The simulated time end as specified by the caller.. */
	
	public Date finalDate = null;
	
	public Date getFinalDate() { return finalDate; }
	
	public void setFinalDate(Date d) { this.finalDate = d; }
	
	
	/** Ownership.
		If the simulation has not completed after this number of
		iterations, it will abort. */
	
	public int iterationLimit = 0;
	
	public int getIterationLimit() { return iterationLimit; }
	
	public void setIterationLimit(int lim) { this.iterationLimit = lim; }
	
	
	/** Ownership.
		The elapsed time (ms) past which the simulation should not progress. */
	
	public long duration;
	
	public long getDuration() { return duration; }
	
	public void setDuration(long dur) { this.duration = dur; }

	
	/** The Random Generator seed used for this simulation. This is a contructor
		parameter. */
	
	public long randomSeed;
	
	public long getRandomSeed() { return randomSeed; }
	
	
	public int getActualNoOfEpochs()
	{
		return epochs.size();
	}
	
	
	public long getActualElapsedTime()
	{
		if (epochs.size() == 0) return 0;
		return getActualEndTime() - getActualStartTime();
	}
	
	
	/* -------------------------------------------------------------------------
		Objects that change during simulation and that are part of the simulation's
		final state. */
	
	
	/** For dispensing unique Ids for this SimulationRun's Events. */
	
	private long largestEventId = 0;
	
	
	/** Ownership.
		A List, ordered by iteration, of all of the Events that occurred in that
		iteration. The first (0th) element in the List is iteration 1.
		Declared transient so that it is not serialized when persisting. Instead,
		Events are saved to archive files. See EventArchiver. */
	
	private transient List<SortedEventSet<SimulationTimeEvent>> iterEventList;
	

	/** Ownership.
		List of all of the Events that were simulated. Includes Startup Events.
		Declared transient so that it is not serialized when persisting. Instead,
		Events are saved to archive files. See EventArchiver. */
	
	private transient List<SimulationTimeEvent> allEvents;
	
	
	/** Ownership.
		All epochs, in order of causality. An epoch is an iteration of
		the simulation main loop, and represents a slice of time. Any
		event generated within an epoch can only affect other components
		during the next epoch at the soonest. */
	
	public List<Epoch> epochs;

	
	/** Ownership.
		List of errors that occur during this SimulationRun, in order
		of occurrence. */
	
	public List<Exception> simTimeErrors = new Vector<Exception>();

	
	/** Ownership.
		The value of each model State at the end of this SimulationRun. */
		
	private Map<State, Serializable> finalStateValues;
	
	
	/** Ownership.
		A sequential List of all of the iterations that occurred during
		this simulation run. */
	
	public List<Integer> iterations = new Vector<Integer>();
		
	
	/** Ownership.
	 */
	
	public boolean completedNorm = false;
	
	public boolean getCompletedNorm() { return completedNorm; }


	/** Ownership.
		The simulation time at which simulation begins. */
	 
	public Date actualInitSimTimeDate = null;
	

	public long getActualStartTime()
	{
		return actualInitSimTimeDate.getTime();
		//if (epochs.size() == 0) return 0;
		//return epochs.get(0).getTime();
	}
	
	
	public long getActualEndTime()
	{
		if (epochs.size() == 0) return 0;
		
		long finalEpochTime = epochs.get(epochs.size()-1).getTime();
		
		//if (! completedNorm) 
			return finalEpochTime;
		
		//return finalDate.getTime();
	}
	
	
	
	/* -------------------------------------------------------------------------
		"Working" objects that change during simulation and that are not intended
		to be part of the simulation's final state. */
	
	
	private transient Date effectiveFinalDate = null;
	private transient long effectiveDuration = 0;
	private transient int effectiveIterationLimit = 0;
	
	private transient int currentIteration = 0;
	
	protected int getCurrentIteration() { return currentIteration; }
	
	
	/** The random number generator to use for this Simulation Run. This is
		constructed from a seed passed as a constructor parameter. */
		
	private transient RandomGenerator randomGenerator;
	
	
	/** Functions that have been scheduled in this epoch for evaluation as part
		of startup. Nullify when sim complete. */
		
	private transient Set<Function> startupFunctions = new TreeSetNullDisallowed<Function>();
			
			
	/** The predefined events at the beginning of this SimulationRun. Nullify 
		when sim complete. */
	
	private transient SortedEventSet<SimulationTimeEvent> futureEvents = 
		new SortedEventSetImpl<SimulationTimeEvent>();

	
	/** Cache of Set of Ports that are sensitive to each State. Nullify when sim 
		complete. */
	
	private transient Map<State, Set<Port>> sensitivePorts = new HashMap<State, Set<Port>>();

	
	/** Cache of Set of Ports that are reachable by each State. Nullify when sim 
		complete. */
	
	private transient Map<State, Set<Port>> reachablePorts = new HashMap<State, Set<Port>>();

	
	/** Cache of Set of sensitive Functions, keyed by State. Nullify when sim complete. */
	
	private transient Map<State, Set<Function>> sensitiveFunctions = 
		new HashMap<State, Set<Function>>();

	
	/** Cache of Set of sensitive Activities, keyed by State. Nullify when sim complete. */
	
	private transient Map<State, Set<Activity>> sensitiveActivities = 
		new HashMap<State, Set<Activity>>();

	
	/** Map of the State that drives each Port in the current epoch. Thus,
		this Map changes during the course of simulation. Nullify when sim complete. */
	
	private transient Map<Port, State> currentDriver = new HashMap<Port, State>();

	
	/** Native components that have been started. This list is kept so that they
		can be cleanly stopped when the simulation terminates. Nullify when sim complete. */
	
	private transient Set<NativeComponentImplementation> startedNativeImpls =
		new HashSet<NativeComponentImplementation>();
		

	/** Table of which States receive new values in the current epoch, and the
		new value received. Nullify when sim complete. */
	private transient Map<State, Serializable> currentStateChanges = null;
	

	/**
	 */
	
	private transient SimCallback callback = null;

	
	/**
	 */
	
	private transient Date currentTimeDate = null;  // maintained by getCurrentEvents().

	
	/**
	 */
	
	private transient Thread thread = null;
	
	
	/** Map of the native implementation used by each model component. Nullify 
		when sim complete. */
	
	private transient Map<ModelComponent, NativeComponentImplementation> nativeImpls =
		new HashMap<ModelComponent, NativeComponentImplementation>();
	

	/* -------------------------------------------------------------------------
		For serialization. */
	
	public transient long[][] iterEventIds = null;
	public transient TableEntry[] finalStateValuesByNodeId = null;
	public transient String modelScenarioNodeId = null;
	
	public long[][] getIterEventIds() { return iterEventIds; }
	public TableEntry[] getFinalStateValuesByNodeId() { return finalStateValuesByNodeId; }
	public String getModelScenarioNodeId() { return modelScenarioNodeId; }
	
	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		/* Owned: 
		finalStateValues (not allowed)
		simTimeErrors (not allowed)
		iterEventList (not allowed)
		*/
		
		super.renameChild(child, newName);
	}
	
	
	public Class getSerClass() { return SimulationRunSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		synchronized (this)
		{
			// Set all transient externalizable Node fields.
			
			if (iterEventList == null) throw new RuntimeException(
				"Null Event list for Simulation Run " + this.getFullName() +
				"; most likely the simrundir was removed before the database was loaded.");
				
			int noOfIterations = iterEventList.size();
			iterEventIds = new long[noOfIterations][];
			int iterNo = 0;
			
			List<SortedEventSet<SimulationTimeEvent>> iterEventListCopy = 
				new Vector<SortedEventSet<SimulationTimeEvent>>(iterEventList);
				
			for (SortedEventSet<SimulationTimeEvent> eventSet : iterEventListCopy)
				// each eventSet in the List,
			{
				long[] eventArray = SimulationTimeEventImpl.createIdArray(eventSet);
				iterEventIds[iterNo++] = eventArray;
			}
			
			finalStateValuesByNodeId = convertMapToTableEntryArray(finalStateValues);
			modelScenarioNodeId = getNodeIdOrNull(getModelScenario());
			int noOfGeneratedEvents = getGeneratedEvents().size();
			//allEventNodeIds = createNodeIdArray(allEvents);
			
			
			// Set Ser fields.
			
			((SimulationRunSer)nodeSer).iterEventIds = this.getIterEventIds();
			((SimulationRunSer)nodeSer).epochs = this.getEpochs();
			((SimulationRunSer)nodeSer).simErrors = this.getErrors();
			((SimulationRunSer)nodeSer).finalStateValuesByNodeId = this.getFinalStateValuesByNodeId();
			((SimulationRunSer)nodeSer).iterations = this.getIterations();
			((SimulationRunSer)nodeSer).modelScenarioNodeId = this.getModelScenarioNodeId();
			//((SimulationRunSer)nodeSer).domainId = this.getModelDomainNodeId();
			((SimulationRunSer)nodeSer).startingDate = this.getStartingDate();
			((SimulationRunSer)nodeSer).finalDate = this.getFinalDate();
			((SimulationRunSer)nodeSer).iterationLimit = this.getIterationLimit();
			((SimulationRunSer)nodeSer).duration = this.getDuration();
			
			((SimulationRunSer)nodeSer).actualElapsedTime = this.getActualElapsedTime();
			((SimulationRunSer)nodeSer).actualStartTime = this.actualInitSimTimeDate;
			((SimulationRunSer)nodeSer).actualEndTime = this.getActualEndTime();

			((SimulationRunSer)nodeSer).randomSeed = this.getRandomSeed();
			((SimulationRunSer)nodeSer).completedNorm = this.getCompletedNorm();
			((SimulationRunSer)nodeSer).noOfGeneratedEvents = noOfGeneratedEvents;
			((SimulationRunSer)nodeSer).simRunSetNodeId = getNodeIdOrNull(this.simRunSet);
			
			return super.externalize(nodeSer);
		}
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		SimulationRunImpl newInstance = (SimulationRunImpl)(super.clone(cloneMap, cloneParent));
		
		// Don't actually use this method.
		
		newInstance.simRunSet = cloneMap.getClonedNode(simRunSet);
		newInstance.externalStateValues = externalStateValues.makeCopy();
		
		return newInstance;
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		List<SimulationTimeEvent> allEventsCopy = new Vector<SimulationTimeEvent>(allEvents);
		for (SimulationTimeEvent e : allEventsCopy)
		{
			e.prepareForDeletion();
			allEvents.remove(e);
		}
		
		List<Set> iterEventSets = new Vector<Set>(iterEventList);
		for (Set s : iterEventSets) iterEventList.remove(s);
		
		List<Epoch> epochsCopy = new Vector<Epoch>(epochs);
		for (Epoch e : epochsCopy) epochs.remove(e);
		
		List<Exception> simErrorsCopy = new Vector<Exception>(simTimeErrors);
		for (Exception e : simErrorsCopy) simTimeErrors.remove(e);
		
		Set<State> valueStates = new HashSet<State>(finalStateValues.keySet());
		for (State s : valueStates) finalStateValues.remove(s);

		List<Integer> iterCopy = new Vector<Integer>(iterations);
		for (Integer i : iterCopy) iterations.remove(i);
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		if (this.simRunSet != null) this.simRunSet.simulationRunDeleted(this);
		
		// Nullify all references.
		startingDate = null;
		finalDate = null;
	}
	
	
	public void simRunSetDeleted(SimRunSet simRunSet)
	{
		if (simRunSet == this.simRunSet)
			this.simRunSet = null;
	}
	
	
	/**
	 * Constructor for creating a new SimulationRun.
	 */

	SimulationRunImpl
	(
		String name,
		ValueHistory externalStateValues,
		ModelScenario modelScenario,
		ModelDomain modelDomain,
		SimCallback callback,
		Date startingDate,  // may be null
		Date finalDate,  // may be null
		int iterationLimit,
		long duration,
		long randomSeed
	)
	{
		super(name, modelScenario, modelDomain);
		setResizable(false);

		this.iterEventList = new Vector<SortedEventSet<SimulationTimeEvent>>();
		this.allEvents = new Vector<SimulationTimeEvent>();
		this.epochs = new Vector<Epoch>();
		this.finalStateValues = new HashMap<State, Serializable>();
		
		this.externalStateValues = externalStateValues;
		
		this.callback = callback;
		this.randomSeed = randomSeed;

		
		// If any of the following is set, it takes precedence over the corresponding
		// setting in the Scenario or ScenarioSet.
		
		this.startingDate = startingDate;
		this.finalDate = finalDate;
		this.iterationLimit = iterationLimit;
		this.duration = duration;
	}
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public String getTagName() { return "simulation_run"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.SimulationRunIconImageName);
	}
	
	
	/**
	 * Used for reconstructing a SimulationRun from an archive.
	 */
	
	protected void addSimulationTimeErrors(Collection<Exception> simErrors)
	{
		this.simTimeErrors.addAll(simErrors);
	}
	
	
	protected void addSimulationTimeError(Exception simError)
	{
		this.simTimeErrors.add(simError);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	/*
	 * Methods from Runnable:
	 */


	/**
	 * The top-level entry point for a simulation run. This method contains
	 * the 'simulation loop'. Simulate the model until there are no future
	 * events pending, or until the effectiveFinalDate has passed.
	 */

	public void run()
	{
		this.thread = Thread.currentThread();
		
		try
		{
		
		/* *********************************************************************
		 * Establish random number generator for this run.
		 */
		
		this.randomGenerator = new RandomGenerator(randomSeed);
		

		/* *********************************************************************
		 * Initialize Attribute values.
		 *
		 * Propagate Decision Parameters to associated Attribute values in
		 * the ModelScenario, or apply default Attribute values, as applicable.
		 */

		try { initializeAttributes(); }
		catch (ParameterError pe)
		{
			callback.showMessage(pe.getMessage());
			return;
		}


		/* *********************************************************************
		 * Establish the simulation time domain: i.e., the start and end dates.
		 * If a start time is provided use it as the actual simulated start time;
		 * otherwise use the current time. If an end time is provided use the
		 * earlier of (the actual simulated start time plus the duration) and
		 * (end time); otherwise, use the actual simulated start time plus the
		 * duration.
		 */

		Date currentRealTimeDate = new Date();


		// Determine the initial simulation time.
		
		if (startingDate == null)
		{
			actualInitSimTimeDate = getModelScenario().getApplicableStartingDate();
			if (actualInitSimTimeDate == null)
				actualInitSimTimeDate = currentRealTimeDate;
		}
		else
			actualInitSimTimeDate = startingDate;
		
		
		// Determine the duration to use.
		
		if (duration == 0)
			effectiveDuration = getModelScenario().getApplicableDuration();
		else
			effectiveDuration = duration;
		
		
		// Calculate the Date of the final epoch. It is the earlier of the final
		// epoch specified by the caller, or the epoch that is 'duration' ms from
		// the starting epoch.
		
		if (finalDate == null)
		{
			effectiveFinalDate = getModelScenario().getApplicableFinalDate();
			
			if (effectiveFinalDate == null)
				effectiveFinalDate = new Date(actualInitSimTimeDate.getTime() + effectiveDuration);
			else
				effectiveFinalDate = new Date(Math.min(effectiveFinalDate.getTime(),
					actualInitSimTimeDate.getTime() + effectiveDuration));
		}
		else
			effectiveFinalDate = new Date(actualInitSimTimeDate.getTime() + Math.min(
				effectiveDuration,
				finalDate.getTime() - actualInitSimTimeDate.getTime()));
		
		
		// Determine iteration limit to use.
		
		if (iterationLimit == 0)
			effectiveIterationLimit = getModelScenario().getApplicableIterationLimit();
		else
			effectiveIterationLimit = iterationLimit;

		
		callback.showMessage(
			"Simulating from " + actualInitSimTimeDate + " to " + effectiveFinalDate);
		
		
		// Set the current simulation time to the initial time.
		
		currentTimeDate = actualInitSimTimeDate;
		

		/* *********************************************************************
		 * Schedule PredefinedEvents.
		 */

		Set<PredefinedEvent> predefinedEvents = new HashSet<PredefinedEvent>();
		
		
		// Identify all Predefined Events defined by the Model Domain.
		
		getPredefinedEvents(getModelDomain(), predefinedEvents);
		
		
		// For each Predefined Event, create a corresponding Generated Event.
		
		SortedEventSet<GeneratedEvent> genPredEvents = null;
		try { genPredEvents = createSimulationPredefinedEvents(predefinedEvents); }
		catch (ParameterError pe)
		{
			callback.aborted(pe.getMessage());
			return;
		}
		
		
		// Convert the time for each Predefined Event into a simulation time, based
		// on the simulated start time.
		
		//shiftEventsByTime(genPredEvents, actualInitSimTimeDate.getTime());
		
		
		// Schedule the newly created Generated Events.
		
		futureEvents.addAll(genPredEvents);
	
		
		/* *********************************************************************
		 * Queue a Startup Event for each Event Producer, signaling each Producer
		 * to drive its States. Note that a Startup Event has a null Date.
		 */

		boolean createdAStartupEvent = scheduleStartupEvents(getModelDomain());
		

		/* *********************************************************************
		 * Start each native ModelComponent implementation, and give each component
		 * a ModelContext.
		 */

		startAllNativeComponents();
		
		
		/* *********************************************************************
		 * Deliver the startup Events.
		 */

		if (createdAStartupEvent)
			performSimulationIteration(true /* startup */);
		

		/* *********************************************************************
		 * Simulation loop: iterate until no more events to process, or until
		 * the designated time epoch has passed.
		 *
		 * Note: The numbers on each comment reference the numbers in the
		 * slide "Processing a Simulation Iteration".
		 */

		simloop: for (;;)
			// each epoch (time) identified by the complete set of future events.
			// (Note that this set may grow during simulation as the result of
			// the generation of events.)
		{
			synchronized (this)
			{
				// Obtain Events for the next epoch and deliver them. I.e., perform
				// a simulation iteration.
				 
				boolean iterationPerformed = performSimulationIteration(false);

				
				// Report progress.
				
				long elapsedTime = currentTimeDate.getTime() - actualInitSimTimeDate.getTime();
				
				if (this.currentIteration % 64 == 0)  // every 64th iteration
					callback.showProgress(this.currentIteration, elapsedTime);
				
				
				//System.gc();
				
				
				if (! iterationPerformed)  // termination condition was met.
					break;
			}
		}

		
		/* *********************************************************************
		 * Update Attribute values.
		 *
		 
		try { updateAttributes(); }
		catch (ParameterError pe)
		{
			callback.showMessage(pe.getMessage());
			GlobalConsole.printStackTrace(pe);
			return;
		}*/
		

		/* *********************************************************************
		 * Report results.
		 */
		 
		completedNorm = true;
		//callback.completed(this);
		
		}
		catch (SimulationError se)
		{
			addSimulationTimeError(se);
			if (callback.checkAbort()) { callback.aborted(); return; }
			
			GlobalConsole.printStackTrace(se);
			callback.aborted(se);
			return;
		}
		catch (ModelContainsError mce)
		{
			addSimulationTimeError(mce);
			GlobalConsole.printStackTrace(mce);
			callback.aborted(mce);
			return;
		}
		catch (RuntimeException ex)
		{
			addSimulationTimeError(ex);
			callback.showMessage("Internal simulator error: " + ex.getMessage());
			GlobalConsole.printStackTrace(ex);
			return;
		}
		catch (Error er)
		{
			callback.showMessage("Internal simulator error: " + er.getMessage());
			GlobalConsole.printStackTrace(er);
			return;
		}
		finally  // stop all native components.
		{
			for (NativeComponentImplementation impl : startedNativeImpls)
			{
				try
				{
					impl.stop();
					//callback.showMessage(impl.getModelComponent().getName() +
					//	" stopped.");
				}
				catch (Exception ex) { callback.showMessage(
					"Could not stop native implementation for " +
						impl.getModelComponent().getName()); }
			}
			
			callback.completed();

			
			// Nullify transient references, so that those objects can be
			// reclaimed.
			
			startupFunctions = null;
			futureEvents = null;
			sensitivePorts = null;
			reachablePorts = null;
			sensitiveFunctions = null;
			sensitiveActivities = null;
			currentDriver = null;
			startedNativeImpls = null;
			currentStateChanges = null;
			callback = null;
			currentTimeDate = null;
			thread = null;
			nativeImpls = null;

			System.gc();
		}
	}

	
	/*
	 * Methods from SimulationRun:
	 */
	
	public long createNewEventId()
	{
		return largestEventId++;
	}
	
	
	public NativeComponentImplementation getNativeImplementation(
		ModelComponent comp)
	{
		return nativeImpls.get(comp);
	}
	
		
	public ModelScenario getModelScenario()
	{
		return (ModelScenario)(getParent());
	}


	public Serializable getExternalStateValueAtTime(State externalState, long simTime)
	throws
		ParameterError
	{
		if (externalStateValues == null) throw new ParameterError(
			"No external State values");
		
		return externalStateValues.getMostRecentRisingValueAtTime(externalState, simTime);
	}
	
	
	public SimRunSet getSimRunSet() { return simRunSet; }
	
	
	public void setSimRunSet(SimRunSet s) { this.simRunSet = s; }


	public RandomGenerator getRandomGenerator() { return this.randomGenerator; }
	
	
	public List<SortedEventSet<SimulationTimeEvent>> getEventsByIteration()
	{
		return iterEventList;
	}


	public List<GeneratedEvent> getGeneratedEvents()
	{
		return new Vector<GeneratedEvent>(removeStartupEvents(new SortedEventSetImpl(allEvents)));
	}


	public List<GeneratedEvent> getGeneratedEvents(State state)
	{
		List<GeneratedEvent> stateEvents = new Vector<GeneratedEvent>();
		List<GeneratedEvent> genEvents = this.getGeneratedEvents();
		for (GeneratedEvent event : genEvents)
		{
			if (event.getState() == state) stateEvents.add(event);
		}

		return stateEvents;
	}


	public List<SimulationTimeEvent> getEvents()
	{
		return allEvents;
	}


	public List<SimulationTimeEvent> getEvents(State state)
	{
		List<SimulationTimeEvent> stateEvents = new Vector<SimulationTimeEvent>();
		for (SimulationTimeEvent event : allEvents)
		{
			if (event.getState() == null) continue;
			if (event.getState() == state) stateEvents.add(event);
		}

		return stateEvents;
	}


	public List<SimulationTimeEvent> getEvents(State state, long startTime, long endTime)
	{
		List<SimulationTimeEvent> stateEvents = new Vector<SimulationTimeEvent>();
		SimulationTimeEvent priorEvent = null;
		for (SimulationTimeEvent event : allEvents)
		{
			if (event.getState() == null) continue;
			if (event.getState() != state) continue;
			
			if (event instanceof GeneratedEvent)  // includes those that
				// represent Predefined Events
			{
				GeneratedEvent genEvent = (GeneratedEvent)event;
				
				long eventTime = genEvent.getEpoch().getTime();
				
				if (eventTime >= startTime)
				{
					if (eventTime > startTime)
						if (priorEvent != null)
							stateEvents.add(priorEvent);
					
					if (genEvent.getEpoch().getTime() > endTime) return stateEvents;
					
					stateEvents.add(event);
				}
				
				priorEvent = event;
			}
		}
		
		// Handle the case where there are no Events after the start time.
		if (stateEvents.size() == 0)
			if (priorEvent != null)
				stateEvents.add(priorEvent);

		return stateEvents;
	}


	public List<SimulationTimeEvent> getEvents(State state, Event firstEvent, long endTime)
	{
		List<SimulationTimeEvent> stateEvents = new Vector<SimulationTimeEvent>();
		boolean firstEventFound = false;
		for (SimulationTimeEvent event : allEvents)
		{
			if (event == firstEvent) firstEventFound = true;
			
			if (firstEventFound)
			{
				if (event.getTimeToOccur().getTime() > endTime) return stateEvents;
				
				if (event.getState() == null) continue;
				if (event.getState() == state) stateEvents.add(event);
			}
		}

		return stateEvents;
	}
	
	
	public GeneratedEvent getFirstEvent(State state)
	{
		for (SimulationTimeEvent event : allEvents)
		{
			if (! (event instanceof GeneratedEvent)) continue;
			
			if (event.getState() == state) return (GeneratedEvent)event;
		}
		
		return null;
	}
	
	
	public void reAddEvent(SimulationTimeEvent event)
	{
		if (event == null) throw new RuntimeException("event is null");
		
		int iteration = event.getIteration();
		
		if (iteration <= 0) throw new RuntimeException("iteration is 0");
		
		
		Epoch epoch = event.getEpoch();
		SortedEventSet<SimulationTimeEvent> eventSet = null;
		
		if (iterEventList == null) iterEventList = new Vector<SortedEventSet<SimulationTimeEvent>>();
		
		//if (epochs == null) epochs = new Vector<Epoch>();
		
		try
		{
			eventSet = iterEventList.get(iteration-1);
			if (eventSet == null) throw new RuntimeException("eventSet is null!");
		}
		catch (IndexOutOfBoundsException ex)  // iteration not in the List.
		{
			// Add iterations to the List of all iterations.
			int curHighestIterNo = iterEventList.size();

			for (int i = curHighestIterNo+1; i <= iteration; i++)
			{
				eventSet = new SortedEventSetImpl<SimulationTimeEvent>();
				iterEventList.add(eventSet);
			}
		}
		
		eventSet.add(event);
			// private List<SortedEventSet<SimulationTimeEvent>> iterEventList

		int position = 0; // find out where the epoch begins
		if (allEvents == null) allEvents = new Vector<SimulationTimeEvent>();
		for (SimulationTimeEvent e : allEvents)
		{
			if (e.getIteration() > event.getIteration()) break;
			position++;
		}
			
		allEvents.add(position, event);
			// private transient List<SimulationTimeEvent> allEvents
	}


	public List<Epoch> getEpochs() { return epochs; }


	public List<Integer> getIterations() { return iterations; }
	

	public long getElapsedMs(int iteration)
	throws
		ParameterError
	{
		if (iteration >= epochs.size()) throw new ParameterError(
			"Iteration " + iteration + " has not yet occurred.");
		
		Epoch epoch = epochs.get(iteration);
		
		long time = epoch.getTime();
		
		long simStartTime = actualInitSimTimeDate.getTime();
		
		return time - simStartTime;
	}


	public Epoch getOrAppendEpochForIteration(int iteration, long ms)
	throws
		IndexOutOfBoundsException
	{
		Epoch epoch;
		try { epoch = epochs.get(iteration-1); }
		catch (IndexOutOfBoundsException ex)
		{
			if (iteration <= 0) throw ex;
			if (iteration != epochs.size()) throw ex;
			
			epoch = new EpochImpl(ms, iteration);
			epochs.add(epoch);
		}
		
		return epoch;
	}


	public Epoch getEpochForIteration(int iteration)
	throws
		ParameterError
	{
		try { return epochs.get(iteration-1); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
	}


	public SortedEventSet<SimulationTimeEvent> getEventsAtIteration(int iteration)
	throws
		ParameterError
	{
		if (iteration <= 0) throw new ParameterError("No iteration " + iteration);
		if (iterEventList.size() < iteration) throw new ParameterError(
			"No iteration " + iteration + ": there are " + iterEventList.size() + " iterations");
		
		SortedEventSet<SimulationTimeEvent> events = null;
		try { events = iterEventList.get(iteration-1); }
		catch (Exception ex) { throw new RuntimeException(
			"Getting events for iteration " + iteration + " but SimRun " +
				this.getFullName() + " only has " + this.iterEventList.size() + " iterations", ex); }
				
		if (events == null) return new SortedEventSetImpl();
		
		return events;
	}
	

	public SortedEventSet<GeneratedEvent> getGeneratedEventsAtIteration(int iteration)
	throws
		ParameterError
	{
		SortedEventSet<SimulationTimeEvent> events = getEventsAtIteration(iteration);
		SortedEventSet<GeneratedEvent> genEvents = new SortedEventSetImpl<GeneratedEvent>();
		for (SimulationTimeEvent event : events)
			if (event instanceof GeneratedEvent)
				genEvents.add((GeneratedEvent)event);
		
		return genEvents;
	}


	public List<Exception> getErrors() { return simTimeErrors; }


	public boolean completedNormally() { return completedNorm; }


	public Serializable getStateValueAtEpoch(State state, Epoch epoch)
	{
		// Traverse through all events until the specified Epoch
		// is passed, and return the most recent event value.

		Serializable mostRecentValue = null;

		for (SimulationTimeEvent event : this.allEvents)
		{
			if (event == null) throw new RuntimeException("null Event in allEvents");
			if (event.getEpoch() == null) throw new RuntimeException(
				"Event epoch is null, in iteration " + getCurrentIteration() +
				": event: " + event.toString());
			if (epoch == null) throw new RuntimeException("epoch argument is null");
			
			if (event.getEpoch().getIteration() > epoch.getIteration())
				return mostRecentValue;

			if (event.getState() == state)
				mostRecentValue = event.getNewValue();
		}

		return mostRecentValue;
	}
	
	
	public Serializable[] getStateValuesAtDate(State state, Date date)
	{
		// Traverse through all events until the specified Date value
		// is reached, and return the most recent event value.
		
		List<Serializable> mostRecentValues = new Vector<Serializable>();
		for (Event event : this.allEvents)
		{
			if (event instanceof StartupEvent) continue;
			if (! (event instanceof GeneratedEvent)) continue;
			
			GeneratedEvent genEvent = (GeneratedEvent)event;
			
			if (genEvent.getEpoch().getTime() < date.getTime()) continue;
			
			if (genEvent.getEpoch().getTime() > date.getTime())
				return mostRecentValues.toArray(new Serializable[mostRecentValues.size()]);
			
			if (genEvent.getState() == state)
				mostRecentValues.add(genEvent.getNewValue());
		}

		return mostRecentValues.toArray(new Serializable[mostRecentValues.size()]);
	}


	public Map<State, Serializable> getFinalStateValues()
	{
		return finalStateValues;
	}
	
	
	public void setInitialStateValue(State state, Serializable initValue)
	throws
		ParameterError
	{
		finalStateValues.put(state, initValue);
	}


	public Serializable getStateFinalValue(State state)
	throws
		ParameterError
	{
		if (! completedNorm) throw new ParameterError(
			"Simulation did not complete normally, so final value for State\n" 
				+ state.getName() + " is indeterminate.");

		Serializable value = finalStateValues.get(state);
		return value;
	}
	
	
	public Serializable getAverageRisingValueForPeriod(State state, long startTime,
		long endTime, boolean[] isPulseRef, long[] averageDelayRef)
	throws
		UndefinedValue,
		ModelContainsError
	{
		long timespan = endTime - startTime;
		if (timespan == 0) throw new UndefinedValue("time span is zero");
		
		List<SimulationTimeEvent> eventsForPeriod = getEvents(state, startTime, endTime);
		
		isPulseRef[0] = false;
		averageDelayRef[0] = 0;
		
		double sum = 0.0;
		long priorEventTime = startTime;
		Serializable priorValue = null;
		
		boolean first = true;
		int noOfEvents = 0;
		for (SimulationTimeEvent event : eventsForPeriod)
		{
			if ((event instanceof GeneratedEvent) && ((GeneratedEvent)event).isCompensation()) continue;
			
			long eventTime = event.getTimeToOccur().getTime();

			if (first)
			{
				first = false;
				isPulseRef[0] = event.isPulse();
				if (eventTime > startTime) throw new UndefinedValue();
				
				priorValue = event.getNewValue();
			}
			else if (event.isPulse() != isPulseRef[0]) throw new ModelContainsError(
				"Pulse Events are mixed with flow Events");
			
			long effectiveEventTime = Math.max(eventTime, startTime);
			long dt = effectiveEventTime - priorEventTime;

			if (priorValue == null) throw new UndefinedValue();
			if (! (priorValue instanceof Number)) throw new ModelContainsError(
				"Value of State at " + (new Date(effectiveEventTime)) + " is not a Number.");
			double doubleValue = ((Number)priorValue).doubleValue();
			
			sum += (doubleValue * ((double)dt));
			
			priorEventTime = effectiveEventTime;
			priorValue = event.getNewValue();
			noOfEvents++;
		}
		
		
		// Account for period <last event, endTime>.
		
		if (priorEventTime < endTime)
		{
			long dt = endTime - priorEventTime;

			if (priorValue == null) throw new UndefinedValue();
			if (! (priorValue instanceof Number)) throw new ModelContainsError(
				"Value of State at " + (new Date(priorEventTime)) + " is not a Number.");
			double doubleValue = ((Number)priorValue).doubleValue();
			
			sum += (doubleValue * ((double)dt));
		}
		
		
		double average = sum / timespan;
		
		if (noOfEvents == 0) throw new UndefinedValue(
			"No events for State '" + state.getFullName() + "' in simulation run '" +
			this.getFullName() + "' for period " +
			(new Date(startTime)) + " to " + (new Date(endTime)));
		
		averageDelayRef[0] = timespan / (long)noOfEvents;
		
		return new Double(average);
	}


	public double computeTimeAveragedValue(State state, long startTime,
		long endTime)
	throws
		UndefinedValue,
		ParameterError
	{
		if (endTime - startTime == 0) throw new ParameterError(
			"Cannot time average: elapsed time is zero.");
		
		GeneratedEvent firstEvent = getDrivingEventAt(state, startTime);
		
		if (firstEvent == null) firstEvent = getFirstEvent(state);
		
		if (firstEvent.getEpoch().getTime() > endTime) throw new UndefinedValue(
			"End time is " + new Date(endTime) + " and so " +
			state.getFullName() + " is undefined at " + firstEvent.getEpoch().getTime());
		
		if (endTime > effectiveFinalDate.getTime()) throw new ParameterError(
			"Simulation Run " + this.getName() + 
			" does not extend past the specified end time " + (new Date(endTime)));
		
		Serializable firstValueObject = firstEvent.getNewValue();
		if (! (firstValueObject instanceof Number)) throw new ParameterError(
			"For '" + firstEvent.getState().getFullName() + "', " +
			" Event value '" + (firstValueObject == null? "null" : firstValueObject.toString()) + 
			"' at time " + firstEvent.getEpoch().getTime() + " is not a Number.");
		
		List<SimulationTimeEvent> events = getEvents(state, firstEvent, endTime);
		
		// Check if the elapsed time is zero.
		if (endTime == firstEvent.getEpoch().getTime()) throw new ParameterError(
			"Cannot time average: elapsed time between first Event and end time is 0.");
		
		double priorTime = startTime;
		double priorValue = ((Number)firstValueObject).doubleValue();
		double average = 0.0;
		events.remove(firstEvent);
		for (SimulationTimeEvent event : events)
		{
			if (! (event instanceof GeneratedEvent)) continue;
			
			GeneratedEvent genEvent = (GeneratedEvent)event;
			
			Serializable valueObject = genEvent.getNewValue();
			if (! (valueObject instanceof Number)) throw new ParameterError(
				"Event value at time " + genEvent.getEpoch().getTime() + " is not a Number.");
			
			long time = genEvent.getEpoch().getTime();
			double value = ((Number)valueObject).doubleValue();
			
			average += (priorValue * (time - priorTime));
			
			priorValue = value;
			priorTime = time;
		}
		
		if (priorTime < endTime) average += (priorValue * (endTime - priorTime));
		
		average /= (endTime - firstEvent.getEpoch().getTime());
		
		return average;
	}
	
	
	public GeneratedEvent getDrivingEventAt(State state, long time)
	{
		GeneratedEvent drivingEvent = null;
		
		for (Event event : allEvents)
		{
			if (! (event instanceof GeneratedEvent)) continue;
			
			GeneratedEvent genEvent = (GeneratedEvent)event;
			
			if (genEvent.getState() != state) continue;
			
			long eventTime = genEvent.getEpoch().getTime();
			
			if (eventTime <= time)
			{
				drivingEvent = genEvent;
				//drivingValue = event.getNewValue();
			}
			else
				return drivingEvent;
		}
		
		return null;
	}
	
	
	
  /* The following methods support the simulation lifecycle. They cannot be
  	called after simulation has completed.
	*/
	
	public GeneratedEvent generateEvent(State state,
		Date time, Serializable newValue, SortedEventSet<GeneratedEvent> triggeringEvents)
	throws
		ParameterError // if the specified time is earlier than the current time.
	{
		GeneratedEvent genEvent = new GeneratedEventImpl(state, time, 
			getCurrentEpoch(), newValue, this);
		
		//this.generatedEvents.add(genEvent);
			
		genEvent.setTriggeringEvents(triggeringEvents);

		return genEvent;
	}
	
	
	void addGeneratedEvent(GeneratedEvent e)
	{
		this.allEvents.add(e);
	}
	
	
	public State getCurrentDriver(Port port)
	{
		return currentDriver.get(port);
	}


	public Serializable getStateValue(State state)
	{
		Serializable value = finalStateValues.get(state);
		return value;
	}
	
	
	public int purgeFutureEvents(Activity activity)
	{
		/*
		 * Remove future events that are driven by each State that is
		 * defined within the Activity, or by any embedded Activities.
		 */
		 
		int noOfEventsDeleted = 0;
		
		SortedEventSet<SimulationTimeEvent> eventsToRemove = new SortedEventSetImpl();
		
		for (State state : activity.getStates())
		{
			for (SimulationTimeEvent e : futureEvents)
			{
				if (e.getState() != state) continue;
				
				//if (e.getEpoch().getTime() == getCurrentTime()) continue;
					// not a future event.
				
				// Remove the event from the queue.
				
				eventsToRemove.add(e);
			}
		}
		
		for (SimulationTimeEvent event : eventsToRemove)
		{
			futureEvents.remove(event);
			noOfEventsDeleted++;
		}
		
		for (Activity a : activity.getActivities())  // recursive.
		{
			noOfEventsDeleted += purgeFutureEvents(a);
		}
		
		return noOfEventsDeleted;
	}


	public void initializeAttributes()
	throws
		ParameterError
	{
		/*
		 * Note: During simulation, if an Attribute is found to not have
		 * a value within the ModelScenario, then the Attribute's default
		 * value is retrieved and written to the ModelScenario. I.e., a
		 * lazy retrieval is used for default values.
		 */

		/*
		 * Identify all of the Attributes within the ModelDomain that are
		 * bound to Variables in a DecisionDomain, and for each of those,
		 * identify the current DecisionScenario and the Decision that
		 * applies to the Variable, if any.
		 */

		for (VariableAttributeBinding varAttrBinding :
				getModelDomain().getVariableAttributeBindings())

			// each Attribute in this ModelDomain that is bound to a
			// Variable in some DecisionDomain...
		{
			Variable variable;
			try { variable = varAttrBinding.getVariable(); }
			catch (ValueNotSet w) { throw new RuntimeException(w); }
			
			ModelAttribute attribute;
			try { attribute = varAttrBinding.getAttribute(); }
			catch (ValueNotSet w) { throw new RuntimeException(w); }


			// Identify the current DecisionScenario for the DecisionDomain
			// that owns the bound Variable.

			DecisionDomain varDecisionDomain =
				variable.getDecisionPoint().getDecisionDomain();

			DecisionScenario varDecisionScenario =
				varDecisionDomain.getCurrentDecisionScenario();

			if (varDecisionScenario == null)
				throw new ParameterError(
					"Cannot identify the DecisionScenario from which to " +
					"read the value of Variable " + variable.getName());


			// Copy the value from the Decision that is associated with
			// the bound Variable.

			Decision decision = varDecisionScenario.getDecision(variable);

			if (decision != null)
			{
				Serializable value = decision.getValue();
				getModelScenario().setAttributeValue(attribute, value);
			}
		}


		/*
		 * Identify all of the Attributes within the ModelDomain that are
		 * bound to State (in another ModelDomain). For each of those,
		 * identify the current ModelScenario that applies to the ModelDomain
		 * that owns that State, read the State's final value, and use it
		 * to update the Attribute value in this SimulationRun's ModelScenario.
		 *

		for (AttributeStateBinding attrStateBinding :
			getModelDomain().getAttributeStateBindings())
		{
			Attribute attribute = attrStateBinding.getAttribute();
			State state = attrStateBinding.getForeignState();
			ModelDomain stateModelDomain = state.getModelDomain();

			ModelScenario stateModelScenario =
				stateModelDomain.getCurrentModelScenario();

			if (stateModelScenario == null)
				throw new ParameterError(
					"Could not identify the ModelScenario from which to " +
					"obtain the value of State " + state.getName());

			Serializable[] values = stateModelScenario.getStateFinalValues(state);
			
			Serializable value = attrStateBinding.deriveAttributeValue(values);

			stateModelScenario.setAttributeValue(attribute, value);
		}*/
	}


	public boolean scheduleStartupEvents(ModelContainer container)
	{
		boolean createdAStartupEvent = false;
		
		Set<ModelComponent> eventProducers = new TreeSetNullDisallowed<ModelComponent>();

		for (ModelComponent component : container.getFunctions())
			eventProducers.add(component);
		
		for (ModelComponent component : container.getActivities())
			eventProducers.add(component);
		
		for (ModelComponent component : eventProducers)
		{
			// Generate a StartupEvent for the EventProducer at t=null.
			StartupEvent e = null;
			try { e = new StartupEventImpl(component, this, null); }
				// Note: the 'epoch' field is filled in during the startup Epoch.
			catch (ParameterError ex) { throw new RuntimeException(ex); }
			
			futureEvents.add(e);
			
			createdAStartupEvent = true;
			
			// Generate events for sub-components, if any.
			
			if (component instanceof ModelContainer)
			{
				createdAStartupEvent = createdAStartupEvent ||
					scheduleStartupEvents((ModelContainer)component);
			}
		}
		
		return createdAStartupEvent;
	}


	/* -------------------------------------------------------------------------
	 * Implementation-defined (protected) methods.
	 */


	/**
	 * Retrieve the current simulation epoch.
	 */

	Epoch getCurrentSimulationEpoch()
	{
		return epochs.get(epochs.size()-1);
	}


	protected void startAllNativeComponents()
	{
		for (ModelComponent comp : getModelDomain().getComponentsWithNativeImpls())
		{
			NativeComponentImplementation impl = null;
			
			Class implClass = null;
				
			try
			{
				if (comp instanceof Function)
				{
					implClass = ((Function)comp).getNativeImplementationClass();
					Function.NativeFunctionImplementation funcImpl = null;
	
					funcImpl =
						(Function.NativeFunctionImplementation)
										(implClass.newInstance());
										
					funcImpl.setModelComponent(comp);
					
					funcImpl.start(new FunctionContextImpl(this, comp));
					impl = funcImpl;
				}
				else if (comp instanceof Activity)
				{
					implClass = ((Activity)comp).getNativeImplementationClass();
					Activity.NativeActivityImplementation actImpl = null;
					
					actImpl = (Activity.NativeActivityImplementation)
										(implClass.newInstance());
										
					actImpl.setModelComponent(comp);
					
					actImpl.start(new ActivityContextImpl(this, comp));
					impl = actImpl;
				}
				else throw new RuntimeException(
					"Unexpected: a " + comp.getClass().getName() + " has a native impl");
			}
			catch (InstantiationException ie)
			{
				GlobalConsole.printStackTrace(ie);
				callback.showMessage(
					"Unable to instantiate '" + implClass.getName() + "' for " +
						comp.getName() + "; " +
						ie.getMessage() + "; stack trace printed");
				return;
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
				callback.showMessage(
					"Unable to start native implementation for " +
						comp.getName() + "; " +
						ex.getMessage() + "; stack trace printed");
				return;
			}
			
			synchronized (this)
			{
				nativeImpls.put(comp, impl);
			}
	
			startedNativeImpls.add(impl);
		}
	}


	/**
	 * See the slide "Processing a Simulation Iteration".
	 * If an iteration was performed (i.e., there were Events and the termination
	 * condition was not met), return true.
	 * If 'startup' is true, then assume that the iteration is a startup iteration:
	 * it is then an error if any Events for the iteration are not Startup Events.
	 */
	 
	protected boolean performSimulationIteration(boolean startup)
	throws
		SimulationError,  // if aborted
		ModelContainsError
	{
		/* 1
		 * Check resource usage.
		 */
		 
		Runtime runtime = Runtime.getRuntime();
		long freeMemory = runtime.freeMemory();
		if (freeMemory < 30000000)  // abort
		{
			GlobalConsole.println("Free memory = " + freeMemory);
			GlobalConsole.println("Total Java memory = " + runtime.totalMemory());
			throw new SimulationError("Server memory getting too low.");
		}
		
		if (callback.checkAbort()) throw new SimulationError("Aborted by client");


		/* 2
		 * Identify Scheduled Events for this iteration.
		 */
		
		SortedEventSet<SimulationTimeEvent> curEvents = getCurrentEvents();
			// This removes Events from 'futureEvents'.
		
		synchronized (this)
		{
			iterEventList.add(curEvents);  // Log the currentEvents for the epoch.
			allEvents.addAll(curEvents);
		}
		
		currentStateChanges = new HashMap<State, Serializable>();
		
			
		// Check if simloop's exit condition has been met.
		 
		if (curEvents.isEmpty())
		{
			return false;  // no more Events at iteration 'currentIteration'
		}

		
		if ((effectiveIterationLimit > 0) && (this.currentIteration >= effectiveIterationLimit))
			// Note: At this point, currentIteration has not been incremented yet, and
			// its value remains from the prior iteration.
		{
			callback.aborted("Iteration limit (" + effectiveIterationLimit + ") exceeded");
			return false;
		}
		

		for (SimulationTimeEvent event : curEvents)
		{
			if (startup)
			{
				if (! (event instanceof StartupEvent)) throw new RuntimeException(
					"Expected a Startup Event");
			}
		}
		

		/* 3
		 * Define epoch to aggregate the current events
		 */
		 
		Epoch epoch = null;

		synchronized (this)
		{
			++(this.currentIteration);  // Note: First iteration is iteration 1.
			iterations.add(new Integer(this.currentIteration));
			
			epochs.add(epoch = new EpochImpl(currentTimeDate, this.currentIteration));
		}
		
		
		/* 3a
		 * Determine which Events propagate. This is needed in step 5.
		 * This computation must be done before step 4, because step 4 (applyEvents)
		 * obliterates the information needed to perform this computation.
		 */
		
		SortedEventSet<SimulationTimeEvent> noPropagateScheduledEvents = 
			findNoPropagateEvents(curEvents);
		
		SortedEventSet<SimulationTimeEvent> propagatingEvents = new SortedEventSetImpl(curEvents);
		propagatingEvents.removeAll(noPropagateScheduledEvents);
		
		
		/* 4
		 * Update value of each State that has a scheduled Event for this iteration.
		 *
		 * While doing this, update the Map of current drivers (States) for
		 * each Port.
		 *
		 * Also set the Epoch and iteration for each Event.
		 */

		applyEvents(curEvents);
		
		
		/* 4a.
		 * Schedule a compensation Event for each pulse Event.
		 */
		
		for (Event event : curEvents)
		{
			if (event.isPulse())
			{
				GeneratedEvent compEvent = createCompensationEvent((GeneratedEvent)event);
				futureEvents.add(compEvent);
			}
		}
		
		
		/* 5
		 * Filter Events that do not affect other Components: those that either
		 * do not represent a value change or that do not reach a Conduit.
		 */
		
		if (propagatingEvents.size() == 0)
		{
			return true; // skip this iteration.
		}
		
		
		/* 6
		 * Identify all behavioral Functions that are sensitive to
		 * the Set of Events.
		 */

		Set<Function> functions = getSensitiveFunctions(propagatingEvents);


		/* 7
		 * Evaluate each Function receiving an Event.
		 */

		SortedEventSet<GeneratedEvent> functionGeneratedEvents = 
			evaluateFunctions(functions, propagatingEvents);
		
		
		// Add the generated events of all Functions to the set of
		// events for the current epoch.

		propagatingEvents.addAll(functionGeneratedEvents);

		synchronized (this)
		{
			curEvents.addAll(functionGeneratedEvents);
			allEvents.addAll(functionGeneratedEvents);
		}


		/* 8
		 * Filter Function-generated Events that do not change the value
		 * driving any Port. Such Events will not be deleted but they will
		 * not be propagated.
		 */

		SortedEventSet<SimulationTimeEvent> noPropagateFunctionEvents = 
			findNoPropagateEvents(functionGeneratedEvents);
		
		propagatingEvents.removeAll(noPropagateFunctionEvents);
		

		if (propagatingEvents.size() == 0)
		{
			return true; // skip this iteration.
		}

		
		/* 9
		 * Identify all behavioral Activities that are receving an Event.
		 */

		Set<Activity> activities = getSensitiveActivities(propagatingEvents);

		
		/* 10
		 * Activate each Activity that is receiving an Event.
		 *
		 * This may result in the generation of future events.
		 * (Events that are generated to occur after time delay 0 are scheduled
		 * for the next cycle of the simulation loop.)
		 */

		SortedEventSet<GeneratedEvent> activityGenEvents = 
			activateActivities(activities, propagatingEvents);

		
		// Add this epoch's generated events for all Activities to the set of
		// future events.
		
		synchronized (this)
		{
			futureEvents.addAll(activityGenEvents);
		}

		return true;
	}
	
	
	
	// For debug only
	void searchForEvent(String name, long id, Collection collection)
	{
		for (Object o : collection)
		{
			Event e = (Event)o;
			
			if (e instanceof SimulationTimeEvent)
			{
				SimulationTimeEvent ste = (SimulationTimeEvent)e;
				if (ste.getId() == id)
				{
					GlobalConsole.println(name + ": contains Event " + id +
						" and epoch is " + (ste.getEpoch() == null? "" : "NOT ") + "null");
					return;
				}
			}
		}
		
		GlobalConsole.println(name + " does NOT contain Event " + id);
	}
	// end debug
	
	
	
	

	/**
	 * Identify all Functions that should receive at least one of the specified
	 * Events. This is statically determinable.
	 */
	 
	protected Set<Function> getSensitiveFunctions(SortedEventSet<SimulationTimeEvent> events)
	throws
		ModelContainsError
	{
		Set<Function> functions = new TreeSetNullDisallowed<Function>();

		for (SimulationTimeEvent event : events)
		{
			// If signaling a Function, add the Function to the list of
			// Functions to be activated.
			
			if (event instanceof StartupEvent)
			{
				ModelComponent component = ((StartupEvent)event).getComponent();
				if (component instanceof Function)
				{
					functions.add((Function)component);
					startupFunctions.add((Function)component);
				}
				
				continue; // don't need to do the rest.
			}


			/*
				Identify all Functions that are sensitive to the event,
				so that these Functions can be evaluated.
			 */

			Set<Function> eventFunctions = getSensitiveFunctions(event.getState());

			functions.addAll(eventFunctions);
			// Note: the fact that we add to this list non-redundantly will
			// prevent a race condition from occurring.
		}
		
		return functions;
	}

	
	/**
	 * Identify all behavioral Activities that should receive at least one of the
	 * specified Events. This is statically determinable.
	 */
	 
	protected Set<Activity> getSensitiveActivities(SortedEventSet<SimulationTimeEvent> events)
	throws
		ModelContainsError
	{
		Set<Activity> activities = new TreeSetNullDisallowed<Activity>();

		for (SimulationTimeEvent event : events)
		{
			// If signaling an Activity, add the Activity to the list of
			// Activities to be activated.
				
			if (event instanceof StartupEvent)
			{
				ModelComponent component = ((StartupEvent)event).getComponent();
				if (component instanceof Activity) activities.add((Activity)component);
				continue; // don't need to do the rest.
			}


			/*
				Identify all behavioral Activities that are sensitive to
				the Event, so that these Activities can be activated.
			 */

			Set<Activity> eventActivities = getSensitiveActivities(event.getState());
			
			
			// Check for obvious race condition: that a Component is responding
			// to itself after zero elapsed time. This is only a partial check,
			// since it does not detect races that traverse multiple components.
			
			EventProducer ep = event.getState().getEventProducer();
			if (eventActivities.contains(ep)
				//eventActivities includes the events''s source
				&& (event instanceof GeneratedEvent) && 
					((((GeneratedEvent)event).getTimeToOccur() != null) 
						&& (((GeneratedEvent)event).getWhenScheduled().equals(getCurrentTime()))))
			{
				callback.showMessage("Warning: possible race condition: " +
					"in iteration " + getCurrentEpoch().getIteration() + ", " +
					((ModelElement)ep).getFullName() + 
						" is responding to its own Event: " + event.toString());
			}
			
			
			activities.addAll(eventActivities);
		}
		
		return activities;
	}
	
	
	/**
	 * Schedule a compensation Event for the specified pulse Event. That is,
	 * schedule an Event for the next iteration that will return the Event's State
	 * to its baseline value.
	 */
	 
	protected GeneratedEvent createCompensationEvent(GeneratedEvent pulseEvent)
	throws
		ModelContainsError
	{
		Serializable compensatingValue = new Double(0.0);
		State state = pulseEvent.getState();
		Date time = getCurrentTime();

		SortedEventSet<GeneratedEvent> triggeringEvents = new SortedEventSetImpl<GeneratedEvent>();
		triggeringEvents.add(pulseEvent);
		
		try { return this.generateEvent(state, time, compensatingValue, triggeringEvents); }
		catch (ParameterError pe) { throw new ModelContainsError(pe); }
		//newEvents.add(genEvent);
	}
	
	
	/**
	 * Call the respond(SortedEventSet) method on each of the specified Functions.
	 * Return a Set of the Events generated by each Function.
	 */
	 
	protected SortedEventSet<GeneratedEvent> evaluateFunctions(Set<Function> functions,
		SortedEventSet<SimulationTimeEvent> visibleEvents)
	throws
		ModelContainsError,
		SimulationError
	{
		Set<Function> alreadyEvaluated = new TreeSetNullDisallowed<Function>();
		
		SortedEventSet<GeneratedEvent> allEvents = new SortedEventSetImpl();
		
		for (Function function : functions)
		{
			if (alreadyEvaluated.contains(function)) continue;
			
			allEvents.addAll(evaluateFunction(function, visibleEvents));
			
			alreadyEvaluated.add(function);
		}
		
		return allEvents;
	}
	
	
	protected SortedEventSet<GeneratedEvent> evaluateFunction(Function function, 
		SortedEventSet<SimulationTimeEvent> visibleEvents)
	throws
		ModelContainsError,
		SimulationError
	{
		if (callback.checkAbort()) throw new SimulationError("Aborted");

		SortedEventSet<GeneratedEvent> eventsGenByFunction = 
			new SortedEventSetImpl<GeneratedEvent>();
		
		Set<Function> feeders = function.getFeeders();
		
		for (Function g : feeders)
		{
			eventsGenByFunction.addAll(evaluateFunction(g, visibleEvents));
		}
		
		SortedEventSet<GeneratedEvent> responseEvents = null;
		try { responseEvents = function.respond(this,  removeStartupEvents(visibleEvents)); }
		catch (ModelContainsError mce)
		{
			try { throw new ComponentBehaviorError(function.externalize(),
					getCurrentEpoch(), mce); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
		}
		
		eventsGenByFunction.addAll(responseEvents);
		
		
		// Update State changes for the response Events.
		
		applyEvents(new SortedEventSetImpl(responseEvents));
		
		
		return eventsGenByFunction;
	}
	
	
	/**
	 * Call the respond(SortedEventSet) method on each of the specified Activities.
	 * Return a Set of the Events generated by those Activities.
	 */
	 
	protected SortedEventSet<GeneratedEvent> activateActivities(Set<Activity> activities,
		SortedEventSet<SimulationTimeEvent> events)
	throws
		ModelContainsError,
		SimulationError  // if an abort is detected
	{
		SortedEventSet<GeneratedEvent> epochGenEvents = new SortedEventSetImpl();

		for	(Activity activity : activities)
		{
			if (callback.checkAbort()) throw new SimulationError("Aborted");

			SortedEventSet<GeneratedEvent> actGenEvents = null;

			SortedEventSet<SimulationTimeEvent> visibleEvents = 
				getVisibleSubset(events, activity);

			if (visibleEvents.size() > 0)
			{
				actGenEvents = null;
				try
				{
					SortedEventSet<GeneratedEvent> es = removeStartupEvents(visibleEvents);
					actGenEvents = activity.respond(this, es);
				}
				catch (ModelContainsError mce)
				{
					try { throw new ComponentBehaviorError(activity.externalize(),
						getCurrentEpoch(), mce); }
					catch (ParameterError pe) { throw new RuntimeException(pe); }
				}

				epochGenEvents.addAll(actGenEvents);
			}
		}
		
		return epochGenEvents;
	}
	
	
	/**
	 * Update the value of the State corresponding to each specified Event.
	 *
	 * It is an error if any Events that occur within an epoch are in conflict with
	 * each other.
	 *
	 * Port Drivers Map 'currentDriver' is updated as well, to reflect which State
	 * is driving each Port: the State that has just changed that can reach the
	 * Port.
	 */
	 
	protected void applyEvents(SortedEventSet<SimulationTimeEvent> events)
	throws
		ModelContainsError  // if any Events conflict, or an Event value is illegal
	{
		/** Events that start to drive a Port during this epoch. */
		Map<Port, SimulationTimeEvent> epochDriverEvents = new HashMap<Port, SimulationTimeEvent>();
			
		
		for (SimulationTimeEvent event : events)
		{
			// Set Epoch and iteration for each Event.
			
			event.setEpoch(getCurrentEpoch());
			

			if (event instanceof StartupEvent) continue;
			
			
			State state = event.getState();
			if (state == null) throw new RuntimeException("State is null");
			
			
			Serializable newValue = event.getNewValue();
			//if (newValue == null) throw new ModelContainsError(
			//	"Null value for Event, for State " + state.getFullName());
			
			
			// Check if the Event is in conflict with another Event in this epoch.
			
			Serializable changedValueInThisEpoch = currentStateChanges.get(state);
			
			if (currentStateChanges.containsKey(state))
				if (! ObjectValueComparator.compare(changedValueInThisEpoch, newValue))
				{
					// Identify the other Event that this Event conflicts with.
					
					for (SimulationTimeEvent e : events)
					{
						if (e.getState() == state)
						{
							if (e == event) continue;
							if (! ObjectValueComparator.compare(e.getNewValue(), newValue))
							{
								throw new ModelContainsError(
									"Event conflict: for State " + state.getFullName() +
									" in iteration " + this.currentIteration +
									"; conflicting Events are: " + e.toString() +
									" and " + event.toString());
									
									//"; conflicting values are " + e.getNewValue() + 
									//" (scheduled in iteration " + e.getWhenScheduled().getIteration() +
									//") and " +
									//newValue + " (scheduled in iteration " +
									//event.getWhenScheduled().getIteration() + ")");
							}
						}
					}
					
					throw new RuntimeException(
						"Event conflict detected but original Event unidentified");
				}
			
			
			// Remove the prior value from the final State value table.
			
			synchronized (this)
			{
				finalStateValues.remove(state);
			}
			
			
			// Update 'currentDriver' for all Ports that are reachable from
			// the State that is the source of the Event.
			
			Set<Port> boundPorts = event.getState().getPortBindings();
			for (Port p : boundPorts)
			{
				currentDriver.put(p, state);
			}

			Set<Port> reachablePorts = getReachablePorts(state);
				
			for (Port p : reachablePorts)
			{
				// Check for conflict: that no two different-valued Events
				// are attempting to drive the same Port.
			
				SimulationTimeEvent otherEvent = epochDriverEvents.get(p);
				if (otherEvent == null)
				{
					epochDriverEvents.put(p, event);
				}
				else  // another State is driving the Port in this epoch.
				{
					// Check that value is the same.
					Serializable otherValue = otherEvent.getNewValue();
				
					if (! ObjectValueComparator.compare(otherValue, newValue))
					{
						if (! p.getBlack()) throw new ModelContainsError(
							"In epoch " + getCurrentEpoch().getDate() +
							", two Events are attempting to drive Port " + 
							p.getFullName() + " with different values, " +
							otherValue + " and " + newValue + ", from states " +
							otherEvent.getState().getFullName() + " and " +
							event.getState().getFullName() + ", respectively.");
					}
				}							
				
				currentDriver.put(p, state);
			}
			
			
			// Update the value of the State.

			Serializable valueCopy = null;
			try { valueCopy = PersistentNodeImpl.copyObject(newValue); }
			catch (CloneNotSupportedException ex)
			{
				throw new ModelContainsError(
					"Cannot copy value '" + newValue.toString() + "' for State " +
						state.getFullName() + ": the value is not Cloneable");
			}
				
			synchronized (this)
			{
				finalStateValues.put(state, valueCopy);
			}
			
			currentStateChanges.put(state, valueCopy);
		}
	}


	/**
	 * Find the Set of Events among the specified Events that do not actually cause
	 * the value on a Conduit to change.
	 *
	 * Ignore StartupEvents because StartupEvents are not propagated anyway.
	 */
	 
	protected SortedEventSet<SimulationTimeEvent> findNoPropagateEvents(
		SortedEventSet events)
	{
		SortedEventSet<SimulationTimeEvent> noPropagateEvents = 
			new SortedEventSetImpl<SimulationTimeEvent>();

		for (Object o : events)
		{
			if (! (o instanceof SimulationTimeEvent)) throw new RuntimeException();
			SimulationTimeEvent event = (SimulationTimeEvent)o;
			
			if (event instanceof StartupEvent) continue;
			
			
			// Find any bound port for the State.
			
			State state = event.getState();
			Set<Port> boundPorts = state.getPortBindings();
			if (boundPorts.size() == 0) continue;
			
			
			boolean changesValue = false;
			for (Port boundPort : boundPorts)
			{
				/* Determine if a Conduit is attached to the bound Port and the
					value driving the Conduit equals the Event value.
					
					If so, the Event is not a true logical Event and should not
					be propagated. */
				
				
				// Determine which State is had been driving the bound Port.
				
				State drivingState = getCurrentDriver(boundPort);

				
				// Obtain the value that was driving the bound Port.
				
				Serializable currentPortValue = null;
				if (drivingState != null) currentPortValue = 
						finalStateValues.get(drivingState);
				
				Serializable newValue = event.getNewValue();

				// Determine if the Event actually changes the value driving
				// the Port. If not, it is not visible to any other Activities
				// or Functions.
				
				if (! ObjectValueComparator.compare(currentPortValue, newValue))
				{
					changesValue = true;
					break;
				}
			}
			
			if (! changesValue)  // Does not change value: do not propagate.
			{
				noPropagateEvents.add(event);
			}
		}
		
		return noPropagateEvents;
	}


	/**
	 * Create and return a Set of Events that is a subset of the input Set,
	 * but not containing any events to which the specified PortedContainer
	 * is not sensitive.
	 */

	protected SortedEventSet<SimulationTimeEvent> getVisibleSubset(
		SortedEventSet<SimulationTimeEvent> events, PortedContainer container)
	throws
		ModelContainsError
	{
		SortedEventSet<SimulationTimeEvent> visibleEvents = 
			new SortedEventSetImpl<SimulationTimeEvent>();

		for (SimulationTimeEvent event : events)
		{
			// Automatically include Startup Events, to ensure that the
			// associated component receives the Event.
			
			if (event instanceof StartupEvent)
			{
				visibleEvents.add(event);
				continue;
			}
			
			
			// For non-Startup Events:
			
			State state = event.getState();

			if (container instanceof Activity)
			{
				Set<Activity> sas = null;

				if ((sas = getSensitiveActivities(state)).contains((Activity)container))
				{
					visibleEvents.add(event);
				}
			}
			else if (container instanceof Function)
			{
				if (getSensitiveFunctions(state).contains((Function)container))
				{
					visibleEvents.add(event);
				}
			}
			else throw new RuntimeException(
				"Unexpected type of PortedContainer");
		}

		return visibleEvents;
	}
	
	
	/**
	 * Purge Startup Events from the specified Set of Events.
	 */
	 
	protected SortedEventSet<GeneratedEvent> removeStartupEvents(
		SortedEventSet<SimulationTimeEvent> events)
	{
		SortedEventSet<GeneratedEvent> genEvents = new SortedEventSetImpl<GeneratedEvent>();
		
		//SimulationTimeEvent[] eventArray = events.toArray(new SimulationTimeEvent[0]);
		
		for (SimulationTimeEvent event : events)
		//for (SimulationTimeEvent event : eventArray)
		{
			//if (event instanceof StartupEvent) events.remove(event);
			
			if (event instanceof GeneratedEvent) genEvents.add((GeneratedEvent)event);
		}
		
		return genEvents;
	}
	
	
	/**
	 * Return a Set of the Events that are statically defined by the model.
	 * Recursive. The caller provides the Set as an argument.
	 */

	protected void getPredefinedEvents(ModelElement element,
		Set<PredefinedEvent> events)
	{
		if (element instanceof EventProducer)
		{
			for (State state : ((EventProducer)element).getStates())
			{
				Set<PredefinedEvent> stateEvents = state.getPredefinedEvents();
				for (PredefinedEvent stateEvent : stateEvents)
				{
					PredefinedEvent event = stateEvent;
					
					events.add(event);
				}
			}
		}

		// Recursively get Predefined Events of children.
		
		if (element instanceof ModelContainer)
		{
			ModelContainer container = (ModelContainer)element;

			for (ModelElement child : container.getFunctions())
			{
				getPredefinedEvents(child, events);
			}

			for (ModelElement child : container.getActivities())
			{
				getPredefinedEvents(child, events);
			}
		}
	}
	
	
	/**
	 * Return the value of the specified Attribute for this SimlationRuns's
	 * Scenario, at the current simulated time. If the Attribute is bound to
	 * a foreign State (i.e., a State in another Domain), the State's value must
	 * be obtained from the table of foreign State values.
	 * This method can only be called after the externalStateValues have been
	 * populated.
	 */
	 
	protected Serializable getAttributeValue(ModelAttribute attr)
	throws
		ParameterError
	{
		AttributeStateBinding stateBinding = attr.getStateBinding();
		if (stateBinding == null)
			return this.getModelScenario().getAttributeValue(attr);
		
		return getAttributeValue(attr, getCurrentTime().getTime());
	}
	
	
	/**
	 * Same as getAttributeValue(Attribute), but permitting the specification
	 * of the simulated time for which the Attribute's value should be determined.
	 * This method can only be called after the externalStateValues have been
	 * populated.
	 */
	 
	protected Serializable getAttributeValue(ModelAttribute attr, long time)
	throws
		ParameterError
	{
		AttributeStateBinding stateBinding = attr.getStateBinding();
		if (stateBinding == null)
			return this.getModelScenario().getAttributeValue(attr);

		if (externalStateValues == null) throw new ParameterError(
			"No external State values");
		
		try { return externalStateValues.getMostRecentRisingValueAtTime(
			stateBinding.getForeignState(), time); }
		catch (ValueNotSet w) { throw new RuntimeException("Should not occur", w); }
	}
	
	
	/**
	 * For each Predefined Event, create a corresponding Generated Event.
	 * Must be called after external State values have been imported.
	 */
	 
	protected SortedEventSet<GeneratedEvent> createSimulationPredefinedEvents(
		Set<PredefinedEvent> predEvents)
	throws
		ParameterError
	{
		SortedEventSet<GeneratedEvent> genEvents = new SortedEventSetImpl<GeneratedEvent>();
		
		for (PredefinedEvent event : predEvents)
		{
			Date time = null;
			long ms = 0;
			Serializable value = null;
			
			ModelAttribute timeAttr = event.getTimeAttribute();
			ModelAttribute valueAttr = event.getValueAttribute();
			
			Serializable serT = this.getAttributeValue(timeAttr);  // in days
			
			if (serT instanceof Number)
			{
				double daysDouble = ((Number)serT).doubleValue();
				if (daysDouble * DateAndTimeUtils.MsInADay > Long.MAX_VALUE)
					throw new ParameterError("Value of Attribute " + timeAttr.getFullName() +
						" is beyond the allowed range.");
						
				if (daysDouble < 0) throw new ParameterError(
					"Value of time for Attribute " + timeAttr.getFullName() +
					" cannot be negative.");
				
				long delayMs = (long)(daysDouble * DateAndTimeUtils.MsInADay);
				ms = getActualStartTime() + delayMs;
				time = new Date(ms);
			}

			if (time == null)
			{
				String timeExpr;
				
				if (serT == null) // Use the time expression String.
					timeExpr = event.getDefaultTimeExpression();
				else
				{
					if (! (serT instanceof String)) throw new ParameterError(
						"Value of Attribute " + timeAttr.getFullName() + 
						" is not a String: it is a " + serT.getClass().getName());
					
					timeExpr = (String)serT;
				}
				
				Start ast;
				AttributeExpressionInterpreter interpreter;
				
				// Evaluate the time expression.
				
				try
				{
					if (timeExpr == null) throw new ParameterError(
						"Null time expression for Predefiend Event " + event.getName());
					
					// Create expression lexer.
					Lexer lexer = new Lexer (new PushbackReader(new StringReader(timeExpr)));
			
					// Parse expression string.
					Parser parser = new Parser(lexer);
					ast = parser.parse();
					
					InterpreterScope.ClassScope classScope = new InterpreterScope.ClassScope()
					{
						public Package findJavaPackage(String name)
						{
							return ServiceContext.getMotifClassLoader().getPackage(name);
						}
					};

					interpreter = new AttributeExpressionInterpreter(classScope, event);
				}
				catch (Exception ex) { throw new ParameterError (ex); }
				
				try
				{
					ast.apply(interpreter);
					double result = interpreter.getFinalDoubleResult();
					
					//System.out.println("Expression result: " + result);
					
					if (result < 0) throw new ParameterError(
						"Invalid negative time expression for Predefined Event " + event.getName());
					
					if ((result * DateAndTimeUtils.MsInADay) > Long.MAX_VALUE)
						throw new ParameterError("Time value beyond the maximum possible value " +
							"for Predefined Event " + event.getName());
					
					long delayMs = (long)(result * DateAndTimeUtils.MsInADay);
					ms = getActualStartTime() + delayMs;
					time = new Date(ms);
				}
				catch (InterpretationError ie) { throw new ParameterError(ie); }
				catch (NullError ne) { throw new ParameterError(
					"No value for time expression, for Predefined Event " + event.getFullName()); }
			}
			
			Serializable serV = this.getAttributeValue(valueAttr, ms);

			if (serV instanceof Number)
			{
				value = serV;
			}
			
			if (value == null)
			{
				String valueExpr;

				if (serV == null) // Use the value expression String.
					valueExpr = event.getDefaultValueExpression();
				else
				{
					if (! (serV instanceof String)) throw new ParameterError(
						"Value of Attribute " + valueAttr.getFullName() +
						" is not a String: it is a " + serV.getClass().getName());
					
					valueExpr = (String)serV;
				}
				
				Start ast;
				AttributeExpressionInterpreter interpreter;
				
				// Evaluate the value expression.
				
				try
				{
					if (valueExpr == null) throw new ParameterError(
						"Null value expression for Predefiend Event " + event.getName());
					
					// Create expression lexer.
					Lexer lexer = new Lexer (new PushbackReader(new StringReader(valueExpr)));
			
					// Parse expression string.
					Parser parser = new Parser(lexer);
					ast = parser.parse();
					
					InterpreterScope.ClassScope classScope = new InterpreterScope.ClassScope()
					{
						public Package findJavaPackage(String name)
						{
							return ServiceContext.getMotifClassLoader().getPackage(name);
						}
					};

					interpreter = new AttributeExpressionInterpreter(classScope,
						event, time);
				}
				catch (Exception ex) { throw new ParameterError (ex); }
					
				try
				{
					ast.apply(interpreter);
					double result = interpreter.getFinalDoubleResult();
					
					value = new Double(result);
				}
				catch (InterpretationError ie) { throw new ParameterError(ie); }
				catch (NullError ne) { throw new ParameterError(
					"No value for value expression, for Predefined Event " + event.getFullName()); }
			}
			
			
			// Instantiate a Generated Event for the Predefined Event.
			
			GeneratedEvent genEvent = new GeneratedEventImpl(
				event.getState(), time, null /* when scheduled */,
					value, this);
			
			if (event.isPulse()) genEvent.setPulse(true);
			
			genEvents.add(genEvent);
		}
		
		return genEvents;
	}
	
	
	/**
	 * For evaluating expressions in a Predefined Event, at start of simulation.
	 */
	 
	class AttributeExpressionInterpreter extends ExpressionInterpreter
	{
		PredefinedEvent preEvent;
		Date time;  // may be null
		
		
		AttributeExpressionInterpreter(InterpreterScope.ClassScope classScope,
			PredefinedEvent preEvent)
		{
			this(classScope, preEvent, null);
		}
		
		
		AttributeExpressionInterpreter(InterpreterScope.ClassScope classScope,
			PredefinedEvent preEvent, Date time)
		{
			super(classScope);
			this.preEvent = preEvent;
			this.time = time;
		}
		
		
		protected double getPredefinedPathValue(List<String> pathParts)
		throws
			ModelContainsError, ElementNotFound
		{
			Serializable value = null;
			
			try
			{
				return super.getPredefinedPathValue(pathParts);
			}
			catch (ElementNotFound enf) {}
			
			
			// Identify the Attribute with the specified path.
			
			ModelElement element = null;
			try
			{
				element = preEvent.getState().findModelElement(pathParts.toArray(
					new String[pathParts.size()]));
			}
			catch (ParameterError pe) { throw new ModelContainsError(pe); }

			if (element == null) throw new ElementNotFound(PathUtilities.assemblePath(pathParts));
			
			if (! (element instanceof ModelAttribute)) throw new ModelContainsError(
				"Can only reference Attributes in a Predefined Event expression.");
			
			ModelAttribute attr = (ModelAttribute)element;
			
			
			// Determine the Attribute's value.
			
			try
			{
				if (time == null)
				{
					if (attr.getStateBinding() != null) // attr is bound to an external State) 
						throw new ModelContainsError(
						"Attribute " + attr.getFullName() + " may not be bound to a State " +
						"when used in a Predefined Event time expression.");
					
					value = getAttributeValue(attr);
					
					if (value == null) throw new ModelContainsError(
						"Value of Attribute '" + attr.getFullName() + "' is null");
				}
				else
				{
					value = getAttributeValue(attr, time.getTime());
					
					if (value == null) throw new ModelContainsError(
						"Value of Attribute '" + attr.getFullName() + "' is null at time " +
						this.time.toString());
				}
			}
			catch (ParameterError pe) { throw new ModelContainsError(pe); }
			
			if (! (value instanceof Number)) throw new ModelContainsError(
				"Value of Attribute " + attr.getFullName() + " is not a Number: " +
				value.toString());
				
			return ((Number)value).doubleValue();
		}


		protected double callBuiltinFunction(String name, List argObjects)
		throws
			ModelContainsError
		{
			throw new InterpretationError("Function " + name + " not recognized");
		}
	}
	
	
	/**
	 * Shift the scheduled time of each Event in the Set by the specified number
	 * of milliseconds. If an Event's scheduled time is null, leave it null.
	 *
	 
	protected void shiftEventsByTime(SortedEventSet<GeneratedEvent> events, long deltaMs)
	{
		SortedEventSet<GeneratedEvent> eventsCopy = 
			new SortedEventSetImpl<GeneratedEvent>(events);
		
		for (GeneratedEvent event : eventsCopy)
		{
			// Remove each Event, shift its time, and then put it back in the Set
			// to ensure that the Events are properly sorted.
			
			events.remove(event);
			
			if (event.getTimeToOccur() != null)
			{
				long shiftedTime = event.getTimeToOccur().getTime() + deltaMs;
				((GeneratedEventImpl)event).setTimeToOccur(new Date(shiftedTime));
			}
			
			events.add(event);
		}
	}*/
	
	
	/**
	 * Return a Set of Events that are all of the same time and the earliest
	 * time among all future events, but not after the specified effectiveFinalDate.
	 * Remove these events from the set of all future events. Null is never
	 * returned. Side effect: updates the value of 'currentTimeDate'.
	 * A Startup Event is considered to occur before any non-Startup Event.
	 */

	protected SortedEventSet<SimulationTimeEvent> getCurrentEvents()
	{
		Date priorDate = currentTimeDate;
		
		SortedEventSet<SimulationTimeEvent> currentEvents = 
			new SortedEventSetImpl<SimulationTimeEvent>();
		
		
		// Note: Ensure that Startup Events are returned as a group, without any
		// non-Startup Events.
		
		boolean foundAStartupEvent = false;
		
		
		for (SimulationTimeEvent event : futureEvents)
		{
			if (event instanceof StartupEvent)
			{
				foundAStartupEvent = true;
				currentEvents.add(event);
				continue;
			}
			
			if (foundAStartupEvent) continue;  // don't return any non-Startup Events.
			
			
			Date eventTime = event.getTimeToOccur();  // may be null.

			if (eventTime == null)  // Current epoch is 0 time after prior epoch.
				eventTime = priorDate;
			else if (eventTime.getTime() < 0) throw new RuntimeException(
				"Event has negative time.");
			
			if ((this.effectiveFinalDate != null) && (eventTime != null) && eventTime.after(this.effectiveFinalDate))
				break; // have passed the final epoch.

			if (currentEvents.size() == 0)  // first time through in this new epoch.
				if (eventTime != null) currentTimeDate = eventTime;
			
			if ((eventTime == null) || currentTimeDate.equals(eventTime))
				currentEvents.add(event);
			else
				break;  // should be later: we are done.
		}


		futureEvents.removeAll(currentEvents);
		
		return currentEvents;
	}


	/**
	 * See determineSensitivePorts.
	 *
	 * This implementation of this method caches its result in the SimulationRun.
	 */

	protected Set<Port> getSensitivePorts(State state)
	throws
		ModelContainsError
	{
		Set<Port> ports = this.sensitivePorts.get(state);

		if (ports == null)
		{
			ports = getModelDomain().determineSensitivePorts(state);
			this.sensitivePorts.put(state, ports);
		}

		return ports;
	}
	
	
	/**
	 * See determineReachablePorts.
	 *
	 * This implementation of this method caches its result in the SimulationRun.
	 */
	 
	protected Set<Port> getReachablePorts(State state)
	throws
		ModelContainsError
	{
		Set<Port> ports = reachablePorts.get(state);
		
		if (ports == null)
		{
			ports = getModelDomain().determineReachablePorts(state);
			this.reachablePorts.put(state, ports);
		}
		
		return ports;
	}


	/**
	 * This implementation of this method caches its result in the SimulationRun.
	 */

	protected Set<Function> getSensitiveFunctions(State state)
	throws
		ModelContainsError // when a model conflict of any kind is detected.
	{
		Set<Function> functions = sensitiveFunctions.get(state);

		if (functions == null)
		{
			functions = getModelDomain().determineSensitiveFunctions(state);
			sensitiveFunctions.put(state, functions);
		}

		return functions;
	}


	/**
	 * This implementation of this method caches its result in the SimulationRun.
	 */

	protected Set<Activity> getSensitiveActivities(State state)
	throws
		ModelContainsError // when a model conflict of any kind is detected.
	{
		Set<Activity> activities = sensitiveActivities.get(state);
		if (activities == null)
		{
			activities = getModelDomain().determineSensitiveActivities(state);
			sensitiveActivities.put(state, activities);
		}

		return activities;
	}
	
	
	/**
	 * Return the current simulation epoch.
	 */
	 
	protected Epoch getCurrentEpoch()
	{
		if (epochs.size() == 0) return null;
		return epochs.get(epochs.size()-1);
	}


	/**
	 * Return the Date corresponding to the current simulation epoch. This
	 * method is intended to be called during simulation, since outside of
	 * simulation there is no "current epoch".
	 */

	protected Date getCurrentTime()
	{
		Epoch epoch = getCurrentEpoch();
		if (epoch == null) return null;
		return epoch.getDate();
	}
	
	
	/**
	 * The calling Thread will wait for this SimulationRun's Thread to finish.
	 */
	 
	void waitForFinish()
	throws
		ParameterError
	{
		synchronized (this)
		{
			if (thread == null) throw new ParameterError(
				"Thread is null; perhaps simulation has not started yet.");
		}
		
		try { this.thread.join(); }
		catch (InterruptedException ie) {}
	}
	
	
	public void dumpEvents()
	{
		GlobalConsole.println("Events by iteration:");
		int iter = 0;
		for (SortedEventSet<SimulationTimeEvent> eventsForIter : iterEventList)
		{
			GlobalConsole.println("\tIteration " + (++iter));
			if (eventsForIter == null)
			{
				GlobalConsole.println("\t\t (null Event list)");
				continue;
			}
			
			for (SimulationTimeEvent event : eventsForIter)
			{
				GlobalConsole.println("\t\t" + event.toString());
			}
		}
	}
}

