/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;
import expressway.common.*;
import expressway.ser.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.server.DecisionElement.*;
import java.util.Date;
import java.util.List;


/**
 * For writing portions of a Persistent Node database to a file.
 */
 
public class XMLWriter
{
	private ModelEngineLocal me;
	private String clientId;
	
	
	public XMLWriter(ModelEngineLocal me, String clientId)
	{
		this.me = me;
		this.clientId = clientId;
	}
	
	
	/**
	 * Write the specified Node to the specified File.
	 */
	 
	public void writeNode(File file, String nodeId)
	throws
		ElementNotFound,
		IOException
	{
		FileWriter writer = new FileWriter(file);
		PrintWriter printWriter = new PrintWriter(writer);
		
		PersistentNode node = PersistentNodeImpl.getNode(nodeId);
		if (node == null) throw new ElementNotFound(nodeId);
		
		writeNode(printWriter, node);
	}
	
	
	/**
	 * Write all the Domains in the database.
	 */
	 
	public void writeLibrary(File directory)
	throws
		IOException
	{
		// Obtain each ModelDomain in the database.
		
		List<NodeDomainSer> domains;
		try { domains = me.getDomains(this.clientId); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		for (NodeDomainSer ser : domains)
		{
			String fileName = ser.getName() + ".xml";
			
			File file = new File(directory, fileName);
			FileWriter writer = new FileWriter(file);
			PrintWriter printWriter = new PrintWriter(writer);
		
			PersistentNode node = PersistentNodeImpl.getNode(ser.getNodeId());
			//System.out.println("Writing Node " + ser.getName());
			writeNode(printWriter, node);
		}
	}
	
	
	public void writeNode(PrintWriter writer, PersistentNode node)
	throws
		IOException
	{
		writer.println("<expressway>");
		writer.println("\t<!--Written " + (new Date()) + "-->");
		writer.println();
		
		node.writeAsXML(writer, 1);  // indent 1 tab.
		
		writer.println();
		writer.println("</expressway>");
		
		writer.close();
	}
}

