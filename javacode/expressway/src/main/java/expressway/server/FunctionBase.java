/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.List;
import java.io.PrintWriter;
import java.io.IOException;


public abstract class FunctionBase extends PortedContainerImpl implements Function
{
	protected Class nativeImplClass = null;
	private int maxEvals = 0;

	/** Reference. */
	public Port outputPort = null;
	
	/** Reference. */
	public State state = null;
	
	
	public Class getSerClass() { return FunctionSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		// Set Ser fields.
		
		((FunctionSer)nodeSer).maxEvals = this.getMaxEvals();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		FunctionImpl newInstance = (FunctionImpl)(super.clone(cloneMap, cloneParent));
		newInstance.outputPort = cloneMap.getClonedNode(outputPort);
		newInstance.state = cloneMap.getClonedNode(state);
		
		return newInstance;
	}

	
	/**
	 * Constructor.
	 */

	public FunctionBase(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		init();
	}


	public FunctionBase(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init();
	}
	
	
	private void init()
	{
		try
		{
			outputPort = super.createPort("output_port", PortDirectionType.output, 
				Position.right, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		try
		{
			state = super.createState("FunctionState", this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}

		try { state.bindPort(outputPort); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }

		outputPort.setPredefined(true);
		state.setPredefined(true);
		
		outputPort.setMovable(false);
		state.setMovable(false);
	}
	
	
	public void deleteChild(PersistentNodeImpl child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError  // if the Port is not a Port for this PortedContainer.
	{
		if (child == outputPort) throw new ParameterError(
			"Cannot delete a Function's output Port");
		
		super.deleteChild(child, outermostAffectedRef);
	}
	
	
	public void deletePort(Port port, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError  // if the Port is not a Port for this PortedContainer.
	{
		deleteChild(port, outermostAffectedRef);
	}


	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		if (port == outputPort)
		{
			if (dir != PortDirectionType.output) throw new ParameterError(
				"Cannot change Port direction for a standard component");
		}
		else
			if (dir != PortDirectionType.input) throw new ParameterError(
				"A Function may only have input Ports and one output Port.");
		
		super.setPortDirection(port, dir);
	}


	/*
	 * Override methods that add children to enforce Function-specific rules.
	 */

	public synchronized void addComponent(ModelComponent component)
	{
		throw new RuntimeException("Not supported");
	}


	//public synchronized void addConstraint(Constraint constraint)
	//{
	//	throw new RuntimeException("Not supported");
	//}


	public synchronized Function createFunction(String name, Class nativeImplClass,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		throw new RuntimeException("Not supported");
	}


	public synchronized Activity createActivity(String name, Class nativeImplClass,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		throw new RuntimeException("Not supported");
	}


	public synchronized Conduit createConduit(String name, Port portA, Port portB,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		throw new RuntimeException("Not supported");
	}


	/**
	 * This implementation is synchronized to protect the native state between the
	 * calls to respond(...) and getAndPurgeNewEvents(). Also, native implementations
	 * are not assumed to be re-entrant.
	 */

	public synchronized SortedEventSet<GeneratedEvent> respond(SimulationRun simRun, 
		SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		NativeComponentImplementation impl = simRun.getNativeImplementation(this);

		SortedEventSet<GeneratedEvent> treeSet = new SortedEventSetImpl<GeneratedEvent>();
		
		if (getFunctions().isEmpty())
		{
			// No sub-components:
		
			if (impl == null)
			{
				// Check if the Function has pre-defined events.
				
				List<State> states = getStates();
				for (State state : states)
				{
					Set<PredefinedEvent> pes = state.getPredefinedEvents();
					if (! pes.isEmpty()) return treeSet;  // it does. Ok.
				}
				
				// Does not have pre-defined events.
				
				throw new ModelContainsError(getFullName() + 
					": No native impl., no sub-components, and no pre-defined events");
			}
			else try
			{
				impl.respond(events);

				return impl.getAndPurgeNewEvents();
			}
			catch (ModelContainsError mce)
			{
				throw mce;
			}
			catch (Throwable t)
			{
				throw new ModelContainsError(
					"Within native implementation of Function " + getName(), t);
			}
		}
		else
		{
			// Has sub-components
			
			if (impl == null) return treeSet;  // THIS Activity does not generate
										// any Events, but its sub-components might.
			else
				throw new ModelContainsError("cannot have sub-components AND a native impl.");
		}
	}


	public synchronized void setMaxEvals(int maxEvals) { this.maxEvals = maxEvals; }
	
	
	public synchronized int getMaxEvals() { return this.maxEvals; }
	
	
	public synchronized void setNativeImplementationClass(Class implClass)
		throws
			ParameterError
	{
		//impl.setModelComponent(this);
		this.nativeImplClass = implClass;
		if (! NativeFunctionImplementation.class.isAssignableFrom(implClass))
			throw new ParameterError(implClass.getName() +
				" is not a NativeFunctionImplementation");
		
		
		// Add to the ModelDomain's list of native implementations.

		synchronized (getModelDomain())
		{
			getModelDomain().getComponentsWithNativeImpls().add(this);
		}
	}
	
	
	public synchronized Port getOutputPort()
	{
		return outputPort;
	}


	public synchronized Set<Function> getFeeders()
	{
		Set<Function> feeders = new TreeSetNullDisallowed<Function>();
		
		
		// Determine the Ports that can reach any of this Function's input Ports.
		
		Set<Conduit> sensitiveConduits = 
			this.getModelDomain().determineSensitiveConduits(
				new TreeSetNullDisallowed<Port>(this.getInputPorts()));
			
		Set<Port> potentialFeederPorts = 
			this.getModelDomain().determineReachablePorts(sensitiveConduits);
		
		
		// Determine if any of the potential feeder Ports is actually a feeder.
		// That is, if it is owned by a Function and is output capable.
		
		for (Port port : potentialFeederPorts)
		{
			PortedContainer container = port.getContainer();
			if (container instanceof Function)
			{
				Function function = (Function)container;
				
				if (function.getNativeImplementationClass() == null)  // not behavioral
					continue;
				
				PortDirectionType dir = port.getDirection();
				if ((dir == PortDirectionType.output) || (dir == PortDirectionType.bi))
				{
					feeders.add(function);
				}
			}
		}
				
				
		return feeders;
	}
	
	
	public synchronized void checkForFeedback(Set<Function> functionsChecked)
	throws
		ModelContainsError  // if feedback is detected.
	{
		if (functionsChecked.contains(this))
		{
			String functionPath = "";
			boolean first = true;
			for (Function f : functionsChecked)
			{
				if (first)
					first = false;
				else
					functionPath += " -> ";
				
				functionPath += f.getName();
			}
			
			throw new ModelContainsError(
				"Feedback detected along the following path: " + functionPath);
		}
				
				
		functionsChecked.add(this);
		
		
		// Identify all Functions that feed this Function.
		
		Set<Function> feedingFunctions = this.getFeeders();
		for (Function g : feedingFunctions)
		{
			g.checkForFeedback(functionsChecked);
		}
	}


	public Class getNativeImplementationClass()
	{
		return nativeImplClass;
	}
}
