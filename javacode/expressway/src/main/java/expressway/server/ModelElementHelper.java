/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.DecisionElement.VariableAttributeBinding;
import expressway.server.Event.*;
import expressway.ser.*;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.IOException;
import java.lang.reflect.Method;



public class ModelElementHelper
{
	static void lockForSimulation(ModelElement elt, boolean lockValue)
	throws
		CannotObtainLock
	{
		if (lockValue && elt.isLockedForSimulation())
			throw new CannotObtainLock(elt.getFullName());
		
		lockForSimulationRecursive(elt, lockValue);
	}
	
	
	static ModelElement findModelElement(ModelElement elt, String qualifiedName)
	throws
		ParameterError
	{
		PersistentNode node = elt.findNode(qualifiedName);
		if (! (node instanceof ModelElement)) throw new ParameterError(
			"The specified path does not reference a ModelElement");
		
		return (ModelElement)node;
	}
	
	
	static ModelElement findModelElement(ModelElement elt, String[] pathParts)
	throws
		ParameterError
	{
		PersistentNode node = elt.findNode(pathParts);
		if (! (node instanceof ModelElement)) throw new ParameterError(
			"The specified path does not reference a ModelElement");
		
		return (ModelElement)node;
	}
	
	
	static void shiftAllChildren(ModelElement elt, double dx, double dy)
	{
		if ((dx == 0.0) && (dy == 0.0)) return;
		
		Set<PersistentNode> children = elt.getChildren();
		
		if (dx != 0.0)
			for (PersistentNode child : children)
				if (elt.allowShift(child))
					child.setX(child.getX() + dx);
		
		if (dy != 0.0)
			for (PersistentNode child : children)
				if (elt.allowShift(child))
					child.setY(child.getY() + dy);
	}

	
	/**
	 * See the slide "Component Layout" for the pattern for instantiating a Node
	 * within another Node.
	 */
	 
	static ModelAttribute createModelAttribute(ModelElement elt, 
		String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (ModelAttribute)(elt.createAttribute(name, defaultValue, layoutBound,
			outermostAffectedRef));
	}
	
	
	static ModelAttribute createModelAttribute(ModelElement elt, 
		PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (ModelAttribute)(elt.createAttribute(layoutBound, outermostAffectedRef));
	}
	
	
	static ModelAttribute createModelAttribute(ModelElement elt, 
		String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (ModelAttribute)(elt.createAttribute(name, layoutBound, outermostAffectedRef));
	}


	static final ModelDomain getModelDomain(ModelElement elt)
	{
		return (ModelDomain)(elt.getDomain());
	}


	static ModelAttribute getModelAttribute(ModelElement elt, String name)
	{
		Attribute attr = elt.getAttribute(name);
		if (attr == null) return null;
		if (attr instanceof ModelAttribute) return (ModelAttribute)attr;
		return null;
	}
	

	static SortedSet<ModelAttribute> getModelAttributes(ModelElement elt)
	{
		SortedSet<ModelAttribute> set = new TreeSetNullDisallowed<ModelAttribute>();
		Set<Attribute> attrs = elt.getAttributes();
		for (Attribute attr : attrs)
		{
			if (! (attr instanceof ModelAttribute)) throw new RuntimeException(
				"ModelElement Attribute is not a ModelAttribute");
			set.add((ModelAttribute)attr);
		}
		
		return set;
	}


	static ModelElement findFirstNestedElement(ModelElement elt, String name)
	throws
		ElementNotFound,
		ParameterError
	{
		PersistentNode node = elt.findFirstNestedNode(name);
		if (! (node instanceof ModelElement)) throw new ParameterError(
			"Node '" + name + "' is not a Model Element");
		return (ModelElement)node;
	}
	

	static void lockForSimulationRecursive(ModelElement elt, boolean lockValue)
	{
		elt.setLockedForSimulation(lockValue);
		
		Set<PersistentNode> elts = elt.getChildren();
		for (PersistentNode elt2 : elts)
			if (elt2 instanceof ModelElementImpl)
				lockForSimulationRecursive((ModelElementImpl)elt2, lockValue);
	}
}

