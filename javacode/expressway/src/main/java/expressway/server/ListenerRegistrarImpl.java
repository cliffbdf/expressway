/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import java.io.Serializable;
import java.rmi.Remote;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.rmi.server.UnicastRemoteObject;
import expressway.generalpurpose.TreeSetNullDisallowed;


/** ****************************************************************************
 * A server-side object for receiving subscription requests from a View.
 * See the slide "PeerListener Pattern" in the design.
 */

public class ListenerRegistrarImpl extends UnicastRemoteObject implements ListenerRegistrar
{
	private final PeerListener listener;
	private final ModelEngineLocal modelEngine;
	private final String clientId;
	
	private Set<String> nodeIds = new TreeSetNullDisallowed<String>();
	
	
	public ListenerRegistrarImpl(PeerListener listener, String clientId,
		ModelEngineLocal me)
	throws
		IOException
	{
		if (clientId == null)
		{
			RuntimeException ex = new RuntimeException("clientId is null");
			GlobalConsole.printStackTrace(ex);
			//throw ex;
		}
		
		this.listener = listener;
		this.clientId = clientId;
		this.modelEngine = me;
	}
	
	
	/** ************************************************************************
	 * Obtain an ID that uniquely identifies the client. This is used to determine
	 * if two conflicting requests come from different clients: if they are from
	 * the same client then merely accept the most recent request.
	 */
	 
	String getClientId()
	//throws
	//	IOException
	{
		return this.clientId;
		//try { return ((java.rmi.server.RemoteServer)this).getClientHost(); }
		//catch (java.rmi.server.ServerNotActiveException ex) { throw new IOException(ex); }
	}

		
  /* ***************************************************************************
   * From ListenerRegistrar.
   */
	
	
	public synchronized void subscribe(String nodeId, boolean subscr)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		if (nodeId == null) throw new ParameterError("Node ID is null");
		
		if (subscr) nodeIds.add(nodeId);
		else nodeIds.remove(nodeId);
		
		if (OperationMode.PrintSubcribingPeerListeners)
		{
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			GlobalConsole.println("PeerListener '" + listener.getId() +
				"' has " + (subscr ? "" : "un") + "subscribed to Node " +
				(node == null ? "<null>" : ("'" + node.getFullName() + "'") + "."));
		}
	}
	
	
	public synchronized void subscribe(String nodeId, String scenarioName,
		boolean subscr)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		GlobalConsole.println(
			"Not implemented yet: subscribing to a Scenario, in ListenerRegistrarImpl.");
	}
	
	
	public synchronized void unsubscribeAll()
	throws
		IOException
	{
		nodeIds.clear();

		if (OperationMode.PrintSubcribingPeerListeners)
		{
			GlobalConsole.println("PeerListener '" + listener.getId() +
				" has unsubscribed from all Nodes.");
		}
	}
	
	
	
  /* ***************************************************************************
   * Methods that should be called locally on the server. These are not part of
   * the remote interface.
   */
	
	
	public synchronized PeerListener getPeerListener() { return listener; }
	
	
	public synchronized Set<PersistentNode> getNodes()
	{
		Set<PersistentNode> nodes = new TreeSetNullDisallowed<PersistentNode>();
		Set<String> nodeIdsCopy = new TreeSetNullDisallowed<String>(nodeIds);
		
		for (String nodeId : nodeIdsCopy)
		{
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node != null) nodes.add(node);
		}
		
		return nodes;
	}
	
	
	public synchronized Set<NodeSer> getNodeSers()
	{
		Set<NodeSer> nodes = new TreeSetNullDisallowed<NodeSer>();
		
		for (String nodeId : nodeIds)
		{
			NodeSer node = null;
			try { node = modelEngine.getNode(getClientId(), nodeId); }
			catch (Exception ex) { continue; }
			
			nodes.add(node);
		}
		
		return nodes;
	}
	
	
	/** ************************************************************************
	 * Return true if the Peer Listener has subscribed to the specified Node, or
	 * if it has subscribed to a parent of the Node.
	 */
	 
	public synchronized boolean isPeerListenerInterrestedIn(String nodeId)
	{
		if (nodeIds.contains(nodeId))
		{
			return true;
		}
		
		PersistentNode node = PersistentNodeImpl.getNode(nodeId);
		
		if (node == null)  // Node was probably removed.
		{
			return false;
		}
		
		for (;;)
		{
			if (nodeIds.contains(node.getNodeId()))
			{
				return true;
			}
			
			PersistentNode parent = node.getParent();
			
			if (parent == null) break;
			
			node = parent;
		}
		
		return false;
	}
	
	
	public String toString()
	{
		String nodeIdStr = "";
		boolean firstTime = true;
		for (String nodeId : nodeIds)
		{
			if (firstTime) firstTime = false;
			else nodeIdStr += ",";
			nodeIdStr += nodeId;
		}
		
		String id;
		try { id = listener.getId(); }
		catch (Exception ex) { id = "<Error '" + ex.getMessage() + "' getting Id>"; }
		return "ListenerRegistrarImpl: PeerListener=" + id +
			" (@=" + listener.hashCode() + "), cliendId=" + clientId + ", Node Ids={" + nodeIdStr + "}";
	}
}



