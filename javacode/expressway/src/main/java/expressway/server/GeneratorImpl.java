/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.MathException;
import expressway.server.Event.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.io.PrintWriter;
import java.io.IOException;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.ser.*;


public class GeneratorImpl extends AbstractGenerator
{
	/**
	 * Constructor.
	 */

	public GeneratorImpl(String name, ModelContainer parent, ModelDomain domain,
		double timeDistShape, double timeDistScale, double valueDistShape, 
		double valueDistScale, boolean ignoreStartup)
	{
		super(name, parent, domain, timeDistShape, timeDistScale, valueDistShape,
			valueDistScale, ignoreStartup);
			
		init();
	}
	
	
	public GeneratorImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init();
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.GeneratorIconImageName);
	}
	
	
	private void init()
	{
		try { this.setNativeImplementationClass(GeneratorNativeImpl.class); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public Class getSerClass() { return GeneratorSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		// Set Ser fields.
		
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		GeneratorImpl newInstance = (GeneratorImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init();
	}
}


class GeneratorNativeImpl extends AbstractGeneratorNativeImpl
{
}
