/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;
import expressway.common.ClientModel.*;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.Event.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


public class SummationImpl extends FunctionBase implements Summation
{
	/** reference only. */
	public Port inputPort = null;
	
	public String getInputPortNodeId() { return getNodeIdOrNull(inputPort); }
	

	public Class getSerClass() { return SummationSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((SummationSer)nodeSer).inputPortNodeId = this.getInputPortNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		SummationImpl newInstance = (SummationImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.inputPort = cloneMap.getClonedNode(inputPort);
		
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<summation name=\"" + this.getName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
		
		writeXMLAttributesForPredefElements(writer, indentation+1);


		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</summation>");
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(inputPort, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		inputPort = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public SummationImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		init();
	}
	
	
	public SummationImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init();
	}
	
	
	public String getTagName() { return "summation"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.SummationIconImageName);
	}
	
	
	private void init()
	{
		setResizable(false);
		setLayoutManager(DefaultLayoutManager);
		
		try
		{
			inputPort = super.createPort("input_port", true, PortDirectionType.input, 
				Position.left, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}

		inputPort.setMovable(false);
		inputPort.setPredefined(true);
	}
	
	
	public int getNoOfHeaderRows() { return 1; }
	
	
	private static final double PreferredWidthInPixels = 60.0;
	private static final double PreferredHeightInPixels = 20.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change Port direction for a standard component");
	}


	public Port getInputPort() { return inputPort; }
	public Port getOutputPort() { return outputPort; }
	
	State getState() { return state; }


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init();
	}
}


class SummationNativeImpl extends FunctionNativeImplBase
{
	boolean stopped = false;
	

	public synchronized void start(FunctionContext context) throws Exception
	{
		if (stopped) throw new RuntimeException("Already stopped");
		super.start(context);
	}


	public synchronized void stop()
	{
		stopped = true;
		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		super.respond(events);
		
		// Check that at least one input port has an event. Include startup
		// events.
		
		boolean hasInputEvents = false;
		for (Port p : getSummation().getPorts())
		{
			if (p == getOutputPort()) continue;  // don't respond to Events on the output.
			if (portHasEvent(events, p, true))
			{
				hasInputEvents = true;
				break;
			}
		}
		
		// Check if there are either no events (the initialization condition) or
		// there is at least one Event on an input Port.
		
		if (! ((events.size() == 0) || hasInputEvents)) return;


		/*
		 * Evaluate the sum.
		 */
		
		Port inputPort = null;
		try { inputPort = getPort("input_port"); }
		catch (ElementNotFound enf) { throw new ModelContainsError(
			"Unable to find port 'input_port' in Summation"); }
			
		//Serializable inVal;
		double sum = 0.0;
		Serializable newValue = null;
		boolean nullInput = false;
		SortedEventSet<GeneratedEvent> eventsOnPort = getEventsOnPort(inputPort, events);
		
		Set<Serializable> values;
		try { values = readAllConduits(inputPort); }
		catch (ParameterError pe) { throw new ModelContainsError(pe); }
		
		for (Serializable inVal : values)
		//for (GeneratedEvent event : eventsOnPort)
		{
			//inVal = event.getNewValue();
			if (inVal == null)
			{
				nullInput = true;
				break;
			}
			
			if (! (inVal instanceof Number))
			{
				String sourceListString = "";
				for (GeneratedEvent e : eventsOnPort)
				{
					sourceListString = 
						sourceListString + e.getState().getEventProducer().getFullName() +
						", ";
				}
				
				throw new ModelContainsError(
					"Value of input to a Summation must be numeric; found " +
					inVal + "(" + inVal.getClass().getName() + "), originating from " +
					sourceListString);
			}
			
			sum += ((Number)inVal).doubleValue();
		}
		
		if (! nullInput) newValue = new Double(sum);
		
		// Update output value.
		updateOutput(newValue, events);		
	}
	
	
	/**
	 * Generate an output event to match the current driven state of the input
	 * port.
	 */
	
	protected void updateOutput(Serializable newValue, SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		// Drive the output port.
		getFunctionContext().setState(getSummation().getState(), newValue, events);
	}
	
	
	protected Port getPort(String name)
	throws ElementNotFound
	{
		ModelElement me = getSummation().getSubcomponent(name);
		if (! (me instanceof Port)) throw new ElementNotFound();
		
		return (Port)me;
	}
	
	
	protected SummationImpl getSummation() { return (SummationImpl)(getFunction()); }
	protected Port getInputPort() { return getSummation().getInputPort(); }
	protected Port getOutputPort() { return getSummation().getOutputPort(); }
}

