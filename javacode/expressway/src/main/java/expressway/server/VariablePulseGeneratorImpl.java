/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import java.util.Set;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class VariablePulseGeneratorImpl extends PulseGeneratorImpl implements VariablePulseGenerator
{
	/** reference only. */
	public Port theta1Port = null;
	
	/** reference only. */
	public Port theta2Port = null;

	
	public transient String theta1PortNodeId = null;
	public transient String theta2PortNodeId = null;
	
	public String getTheta1PortNodeId() { return theta1PortNodeId; }
	public String getTheta2PortNodeId() { return theta2PortNodeId; }
	
	
	public Class getSerClass() { return VariablePulseGeneratorSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		theta1PortNodeId = getNodeIdOrNull(theta1Port);
		theta2PortNodeId = getNodeIdOrNull(theta2Port);
		
		
		// Set Ser fields.
		
		((VariablePulseGeneratorSer)nodeSer).theta1PortNodeId = this.getTheta1PortNodeId();
		((VariablePulseGeneratorSer)nodeSer).theta2PortNodeId = this.getTheta2PortNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		VariablePulseGeneratorImpl newInstance = (VariablePulseGeneratorImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.theta1Port = cloneMap.getClonedNode(theta1Port);
		newInstance.theta2Port = cloneMap.getClonedNode(theta2Port);
		
		return newInstance;
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
			deletePort(theta1Port, null);
			deletePort(theta2Port, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		theta1Port = null;
		theta2Port = null;
	}
	
	
	/**
	 * Constructor.
	 */
	public VariablePulseGeneratorImpl(String name, ModelContainer parent, ModelDomain domain,
		double timeDistShape, double timeDistScale, double valueDistShape,
		double valueDistScale, boolean ignoreStartup)
	{
		super(name, parent, domain, timeDistShape, timeDistScale, valueDistShape,
			valueDistScale, ignoreStartup);
			
		init();
	}
	

	public VariablePulseGeneratorImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init();
	}
	
	
	private void init()
	{
		try { this.setNativeImplementationClass(VariablePulseGeneratorNativeImpl.class); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		try
		{
			theta1Port = super.createPort("time_scale_port", PortDirectionType.input, 
				Position.top, this, null);
			theta2Port = super.createPort("value_scale_port", PortDirectionType.input, 
				Position.top, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		theta1Port.setMovable(false);
		theta2Port.setMovable(false);
		
		theta1Port.setPredefined(true);
		theta2Port.setPredefined(true);
	}
	
	
	public Port getTheta1Port() { return theta1Port; }

	public Port getTheta2Port() { return theta2Port; }
	
	public void dump(int indentation)
	{
		super.dump(indentation);
		
		inputPort.dump(indentation);
		outputPort.dump(indentation);
		countPort.dump(indentation);
		theta1Port.dump(indentation);
		theta2Port.dump(indentation);
	}
}


class VariablePulseGeneratorNativeImpl extends PulseGeneratorNativeImpl
{
	public synchronized void start(ActivityContext context) throws Exception
	{
		super.start(context);
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		int noOfEventsDeleted = 0;
		
		/*
		 * Check if any parameters changed, If so, clear all future events
		 * and re-initialize the distribution functions.
		 */

		if (portHasNonCompensatingEvent(events, getTheta1Port()))
		{
			// Clear all future events that originated from this Generator.

			noOfEventsDeleted = getActivityContext().purgeFutureEvents();
			
			
			// Update the generator's distribution scale parameters.
			
			propagateTimeDistributionChange();
		}
		
		if (portHasNonCompensatingEvent(events, getTheta2Port()))
		{
			// Clear all future events that originated from this Generator.

			noOfEventsDeleted = getActivityContext().purgeFutureEvents();
			
			
			// Update the generator's distribution scale parameters.
			
			propagateValueDistributionChange();
		}


		/*
		 * Schedule a future event on the output port.
		 */

		super.respond(events, noOfEventsDeleted > 0);
	}
	

	protected void propagateTimeDistributionChange()
	throws
		ModelContainsError
	{
		Serializable obj = getActivityContext().getStateValue(getTheta1Port());
		if (obj != null)
		{
			Number num = null;
			try { num = (Number)obj; }
			catch (ClassCastException cce) { throw new ModelContainsError(
				"Non-numeric value on time_scale_port"); }
				
			double d = num.doubleValue();
			this.setTimeDistScale(d);
			
			//System.out.println("Changing time distribution");
			createTimeDistribution(this.getTimeDistShape(), d);
		}
	}


	protected void propagateValueDistributionChange()
	throws
		ModelContainsError
	{
		Serializable obj = getActivityContext().getStateValue(getTheta2Port());
		if (obj != null)
		{
			Number num = null;
			try { num = (Number)obj; }
			catch (ClassCastException cce) { throw new ModelContainsError(
				"Non-numeric value on value_scale_port"); }
				
			double d = num.doubleValue();
			this.setValueDistScale(d);

			//System.out.println("Changing value distribution");
			createValueDistribution(this.getValueDistShape(), d);
		}
	}


	protected ContinuousDistribution getUpdatedTimeDistribution(
		SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		if (portHasEvent(events, getTheta1Port()))
		{
			propagateTimeDistributionChange();
		}
		
		return getTimeDistribution();
	}
	

	protected ContinuousDistribution getUpdatedValueDistribution(
		SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		if (portHasEvent(events, getTheta2Port()))
		{
			propagateValueDistributionChange();
		}
		
		return getValueDistribution();
	}

	
	protected VariablePulseGeneratorImpl getVariablePulseGenerator()
	{
		return (VariablePulseGeneratorImpl)(getGenerator());
	}


	public Port getTheta1Port() { return getVariablePulseGenerator().getTheta1Port(); }

	public Port getTheta2Port() { return getVariablePulseGenerator().getTheta2Port(); }
}
