/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import java.io.IOException;


public class SatisfiesImpl extends DirectedRelationImpl implements Satisfies
{
	public SatisfiesImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
	}


	public SatisfiesImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.SatisfiesIconImageName);
	}
}
