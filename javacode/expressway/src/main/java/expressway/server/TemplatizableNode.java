/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.ser.*;
import expressway.server.MotifElement.*;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;


public abstract class TemplatizableNode
	extends CrossReferenceableNodeImpl
	implements TemplateInstance, Template
{
	/** Primary Node Reference. */
	private List<MenuItem> menuItems = new Vector<MenuItem>();
	
	private Template template;
	
	private Set<TemplateInstance> templateInstances = new TreeSetNullDisallowed<TemplateInstance>();
	
	
	public TemplatizableNode() { super(); }
	
	
	public TemplatizableNode(PersistentNode parent) { super(parent); }



  /* From PersistentNode or PersistentNodeImpl */
	
	
	TemplateInstance createTemplateInstance(String kind,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ElementNotFound
	{
		Domain d = getDomain();
		if (! (d instanceof MotifDef)) throw new ElementNotFound(
			"Domain is not a MotifDef: cannot create a TemplateInstance");
		
		// Determine if the kind is a Template.
		
		Template template = null;
		try { template = ((MotifDef)d).getTemplate(kind); }
		catch (ElementNotFound ex) {}
		
		if (template != null)  // the 'kind' is a Template in a MotifDef.
		{
			String baseName = kind;
			String name = this.createUniqueChildName(baseName);
			TemplateInstance instance;
			try { instance = template.createInstance(name, this); }
			catch (ParameterError pe) { throw new RuntimeException("Should not happen"); }
			
			try { instance.initialize(null, layoutBound, outermostAffectedRef); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
				// Calls back to this Node to perform the initialization.
			
			return instance;
		}
		
		return super.createTemplateInstance(kind, layoutBound, outermostAffectedRef);
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		if (this.template != null) return true;
		return super.isOwnedByActualTemplateInstance();
	}
	
	
	@Override
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		try { return super.getChild(name); }
		catch (ElementNotFound ex)
		{
			for (MenuItem mi : menuItems)
				if (mi.getName().equals(name)) return mi;
			throw ex;
		}
	}
	

	@Override
	public SortedSet<PersistentNode> getChildren()
	{
		SortedSet<PersistentNode> children = super.getChildren();
		children.addAll(menuItems);
		return children;
	}
	
	
	@Override
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		// Delete (per the Delete Pattern) or disconnect the Node from any other
		// child Nodes known to this class that depend on the existence of the Node
		// that is being deleted. These are normally siblings of the Node that
		// is being deleted.
		
		// Call super.deleteChild.
		try { super.deleteChild(child, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			// Nullify references to the Node that this class knows about.
			if (! menuItems.remove(child)) throw pe;
			
			// Set outermostAffectedRef argument, if any.
			setRefArgIfNotNull(outermostAffectedRef, this);
		}
	}
	
	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		super.renameChild(child, newName);
	}
	
	
	public abstract Class getSerClass();
	
	
	@Override
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		TemplatizableNodeSer ser = (TemplatizableNodeSer)nodeSer;

		ser.templateNodeId = getNodeIdOrNull(template);
		ser.templateFullName = 
			(template == null? null : template.getFullName());
		ser.menuItemNodeIds = createNodeIdArray(menuItems);
		
		MenuOwnerHelper.copySerValues(this, ser);

		return super.externalize(ser);
	}
	
	
	@Override
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		TemplatizableNode newInstance =
			(TemplatizableNode)(super.clone(cloneMap, cloneParent));
		
		newInstance.menuItems = cloneNodeList(menuItems, cloneMap, newInstance);
		newInstance.template = cloneMap.getClonedNode(template);
		newInstance.templateInstances = cloneNodeSet(templateInstances, cloneMap, newInstance);
		
		return newInstance;
	}
	
	
	@Override
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		for (MenuItem menuItem : menuItems) deleteMenuItem(menuItem);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		if (template != null) template.templateInstanceDeleted(this);
		
		// Nullify all references.
		menuItems = null;
		template = null;
		templateInstances = null;
	}
	
	
  /* From MenuOwner (via Template) */


	public final Action createAction(int position, String text, String desc, String javaMethodName)
	throws
		ParameterError
	{
		Action action = new ActionImpl(text, this, this);
		action.setHTMLDescription(desc);
		action.setJavaMethodName(javaMethodName);
		menuItems.add(position, action);
		return action;
	}
	
	
	public final Action createAction(String text, String desc, String javaMethodName)
	throws
		ParameterError
	{
		return createAction(0, text, desc, javaMethodName);
	}
	
	
	public final MenuTree createMenuTree(int position, String text, String desc)
	throws
		ParameterError
	{
		MenuTree menuTree = new MenuTreeImpl(text, this, this);
		menuTree.setHTMLDescription(desc);
		menuItems.add(position, menuTree);
		return menuTree;
	}
	
	
	public final MenuTree createMenuTree(String text, String desc)
	throws
		ParameterError
	{
		return createMenuTree(0, text, desc);
	}
	
	
	public final void deleteMenuItem(MenuItem menuItem)
	throws
		ParameterError
	{
		deleteChild(menuItem);
	}
	
	
	public final void deleteMenuItem(int menuItemPos)
	throws
		ParameterError
	{
		deleteMenuItem(menuItems.get(menuItemPos));
	}
	
	
	public final List<MenuItem> getMenuItems()
	{
		return menuItems;
	}
	
	
	public final int getIndexOf(MenuItem mi)
	{
		return menuItems.indexOf(mi);
	}

	

  /* From Template */
	
	
	public TemplateInstance createInstance(String name, PersistentNode parent)
	throws
		ParameterError
	{
		if (! (parent instanceof TemplatizableNode)) throw new RuntimeException(
			"Expected parent to be a TemplatizableNode");
		
		TemplatizableNode container = (TemplatizableNode)parent;
		CloneMap cloneMap = new CloneMap(this);
		TemplatizableNode newInstance;
		try { newInstance = (TemplatizableNode)(this.clone(cloneMap, this)); }
		catch (CloneNotSupportedException ex) { throw new RuntimeException(ex); }
		
		newInstance.template = this;
		List<MenuItem> mis = newInstance.getMenuItems();
		for (MenuItem mi : mis) mi.setDefinedBy(this);
		
		templateInstances.add(newInstance);
		
		return newInstance;
	}
	
	
	public Set<TemplateInstance> getInstances()
	{
		return templateInstances;
	}


  /* From TemplateInstance */
	
	
	public Template getTemplate() { return this.template; }
	

	public void setTemplate(Template t)
	throws
		ParameterError
	{
		this.template = t;
	}
	
	
	public void verifyAgainstTemplate()
	throws
		PatternViolation
	{
		PatternHelper.verifyStructuralConformity(this, this.template);
	}


  /* From TemplateInstanceChangeListener */
	
	
	public void templateInstanceDeleted(TemplateInstance inst)
	{
		templateInstances.remove(inst);
	}
}

