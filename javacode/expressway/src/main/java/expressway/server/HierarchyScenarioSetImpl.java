/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.ser.*;
import expressway.server.HierarchyElement.*;
import expressway.server.NamedReference.*;
import expressway.geometry.*;
import java.util.SortedSet;
import java.util.List;
import java.util.Vector;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.Date;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.Serializable;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;


public class HierarchyScenarioSetImpl extends HierarchyElementImpl implements HierarchyScenarioSet
	// is a HierarchyScenarioChangeListener
{
	private HierarchyScenarioSetHelper helper;
	
	
	public HierarchyScenarioSetImpl(String name, HierarchyAttribute attr)
	{
		super(name, attr.getHierarchyDomain());
		helper = new HierarchyScenarioSetHelper(this, new SuperScenarioSet());
		
		this.setVariableAttribute(attr);
	}
	
	
	public HierarchyScenarioSetImpl(HierarchyScenario scenarioCopied, String name,
		HierarchyAttribute attr, SortedSet<HierarchyScenario> scenarios)
	{
		this(name, attr);
		setDerivedFrom(scenarioCopied);
		
		for (HierarchyScenario scenario : scenarios) this.addScenario(scenario);
	}


	HierarchyScenarioSetImpl(HierarchyScenario scenarioCopied, String name,
		HierarchyAttribute attr)
	{
		this(name, attr);
		setDerivedFrom(scenarioCopied);
	}
	
	
	public String getTagName() { return "hierarchy_scenario_set"; }
	
	
	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{
		super.writeSpecializedXMLAttributes(writer);
		if (getVariableAttribute() != null)
			writer.print("variable_attribute=\"" + getVariableAttribute().getFullName() + "\" ");
	}
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{
		super.writeSpecializedXMLElements(writer, indentation);
	}
	

	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.HierarchyScenarioSetIconImageName);
	}
	
	
	public Class getSerClass() { return HierarchyScenarioSetSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		return helper.externalize(nodeSer);
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		return helper.clone(cloneMap, cloneParent);
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		helper.writeAsXML(writer, indentation);
	}
	

	public void prepareForDeletion()
	{
		helper.prepareForDeletion();
		helper = null;
	}
	
	
  /* From PersistentListNode */
	
	
	public PersistentNode getOrderedChild(String name) throws ElementNotFound
	{
		return helper.getOrderedChild(name);
	}
	
	public PersistentNode getOrderedChildAt(int index) throws ParameterError
	{
		return helper.getOrderedChildAt(index);
	}
	
	public int getIndexOfOrderedChild(PersistentNode child)
	{
		return helper.getIndexOfOrderedChild(child);
	}
	
	public void insertOrderedChildAt(PersistentNode child, int index) throws ParameterError
	{
		helper.insertOrderedChildAt(child, index);
	}
	
	public boolean appendOrderedChild(PersistentNode child) throws ParameterError
	{
		return helper.appendOrderedChild(child);
	}
	
	public boolean removeOrderedChild(PersistentNode child) throws ParameterError
	{
		return helper.removeOrderedChild(child);
	}
	
	public List<PersistentNode> getOrderedNodesOfKind(Class c)
	{
		return helper.getOrderedNodesOfKind(c);
	}
	
	public void removeOrderedNodesOfKind(Class c)
	{
		helper.removeOrderedNodesOfKind(c);
	}
	
	public boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError
	{
		return helper.addAllAsOrderedNodes(nodes);
	}

	public List<PersistentNode> getOrderedChildren()
	{
		return helper.getOrderedChildren();
	}
	
	public int getNoOfOrderedChildren() { return helper.getNoOfOrderedChildren(); }


	public PersistentNode getFirstOrderedChild()
	{
		return ListNodeHelper.getFirstOrderedChild(this);
	}
	
	
	public PersistentNode getLastOrderedChild()
	{
		return ListNodeHelper.getLastOrderedChild(this);
	}
	
	
	public int getOrderedChildPosition(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildPosition(this, child);
	}


	public PersistentNode getOrderedChildBefore(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildBefore(this, child);
	}
	
	
	public PersistentNode getOrderedChildAfter(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildAfter(this, child);
	}
	
	
	public PersistentNode createListChild(PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return ListNodeHelper.createListChild(this, nodeBefore, nodeAfter, scenario,
			copiedNode);
	}


	public void addOrderedChildBefore(PersistentNode child, PersistentNode nextChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildBefore(this, child, nextChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}


	public void addOrderedChildAfter(PersistentNode child, PersistentNode priorChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildAfter(this, child, priorChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}
	
	
  /* From Hierarchy */
	
	
	public int getNoOfSubHierarchies() { return helper.getNoOfSubHierarchies(); }
	
	public Hierarchy getChildHierarchyAt(int index) throws ParameterError
	{ return helper.getChildHierarchyAt(index); }
	
	public int getIndexOfChildHierarchy(Hierarchy child) { return helper.getIndexOfChildHierarchy(child); }
	
	public void insertChildHierarchyAt(Hierarchy child, int index) throws ParameterError
	{ helper.insertChildHierarchyAt(child, index); }
	
	public boolean appendChildHierarchy(Hierarchy child) throws ParameterError
	{ return helper.appendChildHierarchy(child); }
		
	public boolean removeChildHierarchy(Hierarchy child) throws ParameterError
	{ return helper.removeChildHierarchy(child); }
		
		
	public Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		return new HierarchyScenarioImpl(name, (HierarchyDomain)(getDomain()));
	}
		
		
	public void setReasonableLocation()
	{
		HierarchyHelper.setReasonableLocation(this);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			layoutBound, outermostAffectedRef);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position);
	}
		
		
	public Hierarchy createSubHierarchy(String name)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, copiedNode);
	}
	
	
	public Hierarchy createSubHierarchy(int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, position, scenario, copiedNode);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position, 
		Scenario scenario, HierarchyTemplate template)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, template);
	}
	
	
	public void deleteHierarchy(Hierarchy hier)
	throws
		ParameterError
	{
		HierarchyHelper.deleteHierarchy(this, hier);
	}
	
	
	public void changePosition(int pos)
	throws
		ParameterError
	{
		HierarchyHelper.changePosition(this, pos);
	}
	
	
	public int getPosition()
	throws
		ParameterError
	{
		return HierarchyHelper.getPosition(this);
	}
	
	
	public Hierarchy getPriorHierarchy()
	{
		return HierarchyHelper.getPriorHierarchy(this);
	}
	
	
	public Hierarchy getNextHierarchy()
	{
		return HierarchyHelper.getNextHierarchy(this);
	}
	
	
	public int getNoOfHierarchies()
	{
		return HierarchyHelper.getNoOfHierarchies(this);
	}
	
	
	public Hierarchy getParentHierarchy()
	{
		return HierarchyHelper.getParentHierarchy(this);
	}
	
	
	public void setParentHierarchy(Hierarchy parent)
	throws
		ParameterError
	{
		HierarchyHelper.setParentHierarchy(this, parent);
	}
	
	
	public final List<Hierarchy> getSubHierarchies()
	{
		List<Scenario> ss = getScenarios();
		List<Hierarchy> hs = new Vector<Hierarchy>();
		for (Scenario s : ss) hs.add((Hierarchy)s);
		return hs;
	}
	
	
	public List<Hierarchy> getSubHierarchies(Class kind)
	{
		return HierarchyHelper.getSubHierarchies(this, kind);
	}
	
	
	public Hierarchy getSubHierarchy(String name)
	throws
		ElementNotFound
	{
		return HierarchyHelper.getSubHierarchy(this, name);
	}
	
		
  /* From HierarchyScenarioSet */


	public List<HierarchyScenario> getHierarchyScenarios()
	{
		return helper.getHierarchyScenarios();
	}
	
	
  /* From ScenarioSet */


	public Scenario derivedFrom() { return helper.derivedFrom(); }
	
	
	public void setDerivedFrom(Scenario scen) { helper.setDerivedFrom(scen); }
	
	
	public void setVariableAttribute(Attribute attr)
	{
		helper.setVariableAttribute(attr);
	}
	
	
	public Attribute getVariableAttribute() { return helper.getVariableAttribute(); }
	
	
	public void setScenarios(List<Scenario> scens)
	{
		helper.setScenarios(scens);
	}


	public List<Scenario> getScenarios()
	{
		return helper.getScenarios();
	}
	
	
	public void addScenario(Scenario scen)
	{
		helper.addScenario(scen);
	}
	
	
	public void removeScenario(Scenario scen)
	{
		helper.removeScenario(scen);
	}

	
	public void setScenarioThatWasCopied(Scenario scenario) { helper.setScenarioThatWasCopied(scenario); }
	
	
	public Scenario getScenarioThatWasCopied() { return helper.getScenarioThatWasCopied(); }

	
  /* From HierarchyScenarioChangeListener */

	
	public void hierarchyScenarioDeleted(HierarchyScenario scenario)
	{
		helper.hierarchyScenarioDeleted(scenario);
	}

	
  /* From PersistentNodeImpl */


	public int getNoOfHeaderRows() { return helper.getNoOfHeaderRows(); }
	
	
	public double getPreferredWidthInPixels() { return helper.getPreferredWidthInPixels(); }
	

	public double getPreferredHeightInPixels() { return helper.getPreferredHeightInPixels(); }


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		helper = new HierarchyScenarioSetHelper(this, new SuperScenarioSet());
	}
	
	
  /* Proxy classes needed by HierarchyScenarioSetHelper */

	
	private class SuperScenarioSet extends SuperPersistentNode
	{
	}
	
	
	private class SuperPersistentNode implements PersistentNode
	{
		public int compareTo(PersistentNode node)
		{
			throw new RuntimeException("Should not be called");
		}
		
		
		public Object clone() throws CloneNotSupportedException
		{
			throw new RuntimeException("Should not be called");
		}

		
	  /* From PersistentNode */
		
		
		public ModelEngineLocal getModelEngine()
		{ return HierarchyScenarioSetImpl.super.getModelEngine(); }
	
	
		public List<String> getInstantiableNodeKinds()
		{ return HierarchyScenarioSetImpl.super.getInstantiableNodeKinds(); }
		
		
		public List<String> getStaticCreateMethodNames()
		{ return HierarchyScenarioSetImpl.super.getStaticCreateMethodNames(); }
		
		
		public List<String> getInstantiableNodeDescriptions()
		{ return HierarchyScenarioSetImpl.super.getInstantiableNodeDescriptions(); }
		
		
		public PersistentNode createChild(String kind, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.createChild(kind, layoutBound, outermostAffectedRef); }
		
		
		public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ HierarchyScenarioSetImpl.super.deleteChild(child, outermostAffectedRef); }
			
		
		public void deleteChild(PersistentNode child)
		throws
			ParameterError
		{ HierarchyScenarioSetImpl.super.deleteChild(child); }
			
			
		public Template findTemplate(String templatePath)
		throws
			ElementNotFound,
			ParameterError
		{ return HierarchyScenarioSetImpl.super.findTemplate(templatePath); }
		
		
		public Class getNodeKind(String className)
		throws
			ClassNotFoundException,
			ParameterError
		{ return HierarchyScenarioSetImpl.super.getNodeKind(className); }
			
		
		public Class getSerClass()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Set<Attribute> getAttributes()
		{ return HierarchyScenarioSetImpl.super.getAttributes(); }
		
		
		public String getTagName()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Attribute getAttribute(String attrName)
		{ return HierarchyScenarioSetImpl.super.getAttribute(attrName); }
		
		
		public int getAttributeSeqNo(Attribute attr)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.getAttributeSeqNo(attr); }
		

		public void setAttributeSeqNo(Attribute attr, int seqNo)
		throws
			ParameterError
		{ HierarchyScenarioSetImpl.super.setAttributeSeqNo(attr, seqNo); }
				
				
		public void setAttributeAsFirstInSeq(Attribute attr)
		throws
			ParameterError
		{ HierarchyScenarioSetImpl.super.setAttributeAsFirstInSeq(attr); }
		
		
		public void setAttributeAsLastInSeq(Attribute attr)
		throws
			ParameterError
		{ HierarchyScenarioSetImpl.super.setAttributeAsLastInSeq(attr); }
		
		
		public void moveAttributeBefore(Attribute attr, Attribute nextAttr)
		throws
			ParameterError
		{ HierarchyScenarioSetImpl.super.moveAttributeBefore(attr, nextAttr); }
		
		
		public void moveAttributeAfter(Attribute attr, Attribute priorAttr)
		throws
			ParameterError
		{ HierarchyScenarioSetImpl.super.moveAttributeAfter(attr, priorAttr); }
	
		
		public int getFirstAttributeSeqNo()
		{ return HierarchyScenarioSetImpl.super.getFirstAttributeSeqNo(); }
		
		
		public int getLastAttributeSeqNo()
		{ return HierarchyScenarioSetImpl.super.getLastAttributeSeqNo(); }
		
		
		public int getNumberOfAttributes()
		{ return HierarchyScenarioSetImpl.super.getNumberOfAttributes(); }


		public List<Attribute> getAttributesInSequence()
		{ return HierarchyScenarioSetImpl.super.getAttributesInSequence(); }
		
		
		public Attribute getFirstAttribute()
		{ return HierarchyScenarioSetImpl.super.getFirstAttribute(); }
		
		
		public Attribute getLastAttribute()
		{ return HierarchyScenarioSetImpl.super.getLastAttribute(); }

		
		public Attribute getAttributeAtSeqNo(int seqNo)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.getAttributeAtSeqNo(seqNo); }


		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.constructAttribute(name, defaultValue); }
		
	
		public Attribute constructAttribute(String name)
		{ return HierarchyScenarioSetImpl.super.constructAttribute(name); }
		
	
		public Attribute createAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		
		public Attribute createAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.createAttribute(layoutBound, outermostAffectedRef); }
	
	
		public Attribute createAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.createAttribute(name, layoutBound, outermostAffectedRef); }
		
		
		public void deleteAttribute(Attribute attribute)
		throws
			ParameterError
		{ HierarchyScenarioSetImpl.super.deleteAttribute(attribute); }
			
			
		public void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ HierarchyScenarioSetImpl.super.deleteAttribute(attribute, outermostAffectedRef); }
			
			
		public boolean isOwnedByMotifDef()
		{ return HierarchyScenarioSetImpl.super.isOwnedByMotifDef(); }
		
		
		public boolean isOwnedByActualTemplateInstance()
		{ return HierarchyScenarioSetImpl.super.isOwnedByActualTemplateInstance(); }
		
		
		public boolean actuallyUsesMotif(MotifDef md)
		{ return HierarchyScenarioSetImpl.super.actuallyUsesMotif(md); }
		
		
		public Image getLocalStoredImage(String handle)
		{ return HierarchyScenarioSetImpl.super.getLocalStoredImage(handle); }
		
		
		public void deleteFromAllNodesTable()
		{ HierarchyScenarioSetImpl.super.deleteFromAllNodesTable(); }
		
		
		public String getName()
		{ return HierarchyScenarioSetImpl.super.getName(); }
		
		
		public void setHTMLDescription(String html)
		{ HierarchyScenarioSetImpl.super.setHTMLDescription(html); }
		
		
		public String getHTMLDescription()
		{ return HierarchyScenarioSetImpl.super.getHTMLDescription(); }
		
		
		public String getFullName()
		{ return HierarchyScenarioSetImpl.super.getFullName(); }
		
		
		public String getNameRelativeTo(PersistentNode parent)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.getNameRelativeTo(parent); }
	
	
		public void setName(String name)
		throws
			ParameterError
		{ HierarchyScenarioSetImpl.super.setName(name); }
		
		
		public String setNameAndNotify(String newName)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.setNameAndNotify(newName); }
		
		
		public void renameChild(PersistentNode child, String newName)
		throws
			ParameterError
		{ HierarchyScenarioSetImpl.super.renameChild(child, newName); }
		
		
		public Domain getDomain()
		{ return HierarchyScenarioSetImpl.super.getDomain(); }
		
		
		public String createUniqueChildName()
		{ return HierarchyScenarioSetImpl.super.createUniqueChildName(); }
	
		
		public String createUniqueChildName(String baseName)
		{ return HierarchyScenarioSetImpl.super.createUniqueChildName(baseName); }
		
		
		public SortedSet<PersistentNode> getChildren()
		{ return HierarchyScenarioSetImpl.super.getChildren(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive()
		{ return HierarchyScenarioSetImpl.super.getChildrenRecursive(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive(Class kind)
		{ return HierarchyScenarioSetImpl.super.getChildrenRecursive(kind); }
		
		
		public PersistentNode getChild(String name)
		throws
			ElementNotFound
		{ return HierarchyScenarioSetImpl.super.getChild(name); }
	
	
		public PersistentNode getParent()
		{ return HierarchyScenarioSetImpl.super.getParent(); }
		
		
		public void setParent(PersistentNode parent)
		{ HierarchyScenarioSetImpl.super.setParent(parent); }
		
		
		public boolean isDescendantOf(PersistentNode ancestor)
		{ return HierarchyScenarioSetImpl.super.isDescendantOf(ancestor); }
		
		
		public PersistentNode findNode(String qualifiedName)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.findNode(qualifiedName); }
	
	
		public PersistentNode findNode(String[] pathParts)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.findNode(pathParts); }
			
		
		public PersistentNode findFirstNestedNode(String name)
		throws
			ElementNotFound
		{ return HierarchyScenarioSetImpl.super.findFirstNestedNode(name); }
		
		
		public PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.getChildByRelativeQualifiedName(pathParts); }
		
		
		public void prepareForDeletion()
		{ HierarchyScenarioSetImpl.super.prepareForDeletion(); }
		
		
		public Display getDisplay()
		{ return HierarchyScenarioSetImpl.super.getDisplay(); }
		
		
		public void setVisible(boolean vis)
		{ HierarchyScenarioSetImpl.super.setVisible(vis); }
		
		
		public boolean isVisible()
		{ return HierarchyScenarioSetImpl.super.isVisible(); }
		
		
		public void layout()
		{ HierarchyScenarioSetImpl.super.layout(); }
		
		
		public Set<PersistentNode> getNodesWithSpecialLayout()
		{ return HierarchyScenarioSetImpl.super.getNodesWithSpecialLayout(); }
		
		
		public void conditionalLayout()
		{ HierarchyScenarioSetImpl.super.conditionalLayout(); }
		
		
		public PersistentNode layoutUpward(PersistentNode layoutBound)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.layoutUpward(layoutBound); }
	
		
		public void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		{ HierarchyScenarioSetImpl.super.layout(layoutBound, outermostAffectedRef); }
	
	
		public void setPredefined(boolean pre)
		{ HierarchyScenarioSetImpl.super.setPredefined(pre); }
		
		
		public boolean isPredefined()
		{ return HierarchyScenarioSetImpl.super.isPredefined(); }
		
		
		public void setDeletable(boolean del)
		{ HierarchyScenarioSetImpl.super.setDeletable(del); }
		
		
		public boolean isDeletable()
		{ return HierarchyScenarioSetImpl.super.isDeletable(); }
		
		
		public void setMovable(boolean m)
		{ HierarchyScenarioSetImpl.super.setMovable(m); }
		
		
		public boolean isMovable()
		{ return HierarchyScenarioSetImpl.super.isMovable(); }
		
		
		public void setResizable(boolean yes)
		{ HierarchyScenarioSetImpl.super.setResizable(yes); }
		
		
		public boolean isResizable()
		{ return HierarchyScenarioSetImpl.super.isResizable(); }
		
		
		public double getWidth()
		{return HierarchyScenarioSetImpl.super.getWidth();  }
	
	
		public double getHeight()
		{ return HierarchyScenarioSetImpl.super.getHeight(); }
		
		
		public void setWidth(double width)
		{ HierarchyScenarioSetImpl.super.setWidth(width); }
	
	
		public void setHeight(double height)
		{ HierarchyScenarioSetImpl.super.setHeight(height); }
		
		
		public boolean useBorderWhenIconified()
		{ return HierarchyScenarioSetImpl.super.useBorderWhenIconified(); }
		
		
		public void setUseBorderWhenIconified(boolean yesOrNo)
		{ HierarchyScenarioSetImpl.super.setUseBorderWhenIconified(yesOrNo); }
		
		
		public Image getIconImage()
		{ return HierarchyScenarioSetImpl.super.getIconImage(); }
		
		
		public void setIconImage(String path)
		throws
			IOException
		{ HierarchyScenarioSetImpl.super.setIconImage(path); }
		
		
		public ImageIcon getImageIcon()
		{ return HierarchyScenarioSetImpl.super.getImageIcon(); }
		
		
		public String getIconImageHandle()
		{ return HierarchyScenarioSetImpl.super.getIconImageHandle(); }
		
		
		public double getIconWidth()
		{ return HierarchyScenarioSetImpl.super.getIconWidth(); }
		
		
		public double getIconHeight()
		{ return HierarchyScenarioSetImpl.super.getIconHeight(); }
		
		
		public void setLayoutManager(LayoutManager lm)
		{ HierarchyScenarioSetImpl.super.setLayoutManager(lm); }
		
		
		public LayoutManager getLayoutManager()
		{ return HierarchyScenarioSetImpl.super.getLayoutManager(); }
		
		
		public String getDefaultViewTypeName()
		{ return HierarchyScenarioSetImpl.super.getDefaultViewTypeName(); }
		
		public void setViewClassName(String name)
		{ HierarchyScenarioSetImpl.super.setViewClassName(name); }
		
		public String getViewClassName()
		{ return HierarchyScenarioSetImpl.super.getViewClassName(); }

		
		public boolean contains(Point p)
		{ return HierarchyScenarioSetImpl.super.contains(p); }
		
		
		public boolean contains(double x, double y)
		{ return HierarchyScenarioSetImpl.super.contains(x, y); }
		
		
		public Face[] getFaces(double x, double y)
		{ return HierarchyScenarioSetImpl.super.getFaces(x, y); }
		
		
		public Face[] getIconFaces(double x, double y)
		{ return HierarchyScenarioSetImpl.super.getIconFaces(x, y); }
		
		
		public Position findSideClosestToInternalPoint(double x, double y)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.findSideClosestToInternalPoint(x, y); }
		
		
		public Position findSideClosestToExternalPoint(Point location)
		{ return HierarchyScenarioSetImpl.super.findSideClosestToExternalPoint(location); }
		
		
		public void setSizeToStandard()
		{ HierarchyScenarioSetImpl.super.setSizeToStandard(); }
		
		
		public double getHeaderLabelHeight()
		{ return HierarchyScenarioSetImpl.super.getHeaderLabelHeight(); }
		
		
		public int getNoOfHeaderRows()
		{ return HierarchyScenarioSetImpl.super.getNoOfHeaderRows(); }
	
	
		public SortedSet<PersistentNode> getPredefinedNodes()
		{ return HierarchyScenarioSetImpl.super.getPredefinedNodes(); }
		
		
		public double getMinimumWidth()
		{ return HierarchyScenarioSetImpl.super.getMinimumWidth(); }
		
		
		public double getMinimumHeight()
		{ return HierarchyScenarioSetImpl.super.getMinimumHeight(); }
		
		
		public double getPreferredWidthInPixels()
		{ return HierarchyScenarioSetImpl.super.getPreferredWidthInPixels(); }
	
		public double getPreferredHeightInPixels()
		{ return HierarchyScenarioSetImpl.super.getPreferredHeightInPixels(); }
	
		
		public int getPreferredHeaderRowHeightInPixels()
		{ return HierarchyScenarioSetImpl.super.getPreferredHeaderRowHeightInPixels(); }
		
		
		public double getX() { return HierarchyScenarioSetImpl.super.getX(); }
		public double getY() { return HierarchyScenarioSetImpl.super.getY(); }
		public double getScale() { return HierarchyScenarioSetImpl.super.getScale(); }
		public double getOrientation() { return HierarchyScenarioSetImpl.super.getOrientation(); }
		public void setX(double x) { HierarchyScenarioSetImpl.super.setX(x); }
		public void setY(double y) { HierarchyScenarioSetImpl.super.setY(y); }
		public void setLocation(double x, double y) { HierarchyScenarioSetImpl.super.setLocation(x, y); }
		public void setScale(double scale) { HierarchyScenarioSetImpl.super.setScale(scale); }
		public void setOrientation(double angle) { HierarchyScenarioSetImpl.super.setOrientation(angle); }
		
		
		public boolean allowShift(PersistentNode childNode)
		{ return HierarchyScenarioSetImpl.super.allowShift(childNode); }
		
		
		public long getLastUpdated()
		{ return HierarchyScenarioSetImpl.super.getLastUpdated(); }
		
		
		public int getVersionNumber()
		{ return HierarchyScenarioSetImpl.super.getVersionNumber(); }
		
		
		public String getClientThatLastModified()
		{ return HierarchyScenarioSetImpl.super.getClientThatLastModified(); }
		
		
		public void update()
		{ HierarchyScenarioSetImpl.super.update(); }
		
	
		public Date getDeletionDate()
		{ return HierarchyScenarioSetImpl.super.getDeletionDate(); }
		
	
		public PersistentNode getPriorVersion()
		{ return HierarchyScenarioSetImpl.super.getPriorVersion(); }
		
		
		public String getPriorVersionNodeId()
		{ return HierarchyScenarioSetImpl.super.getPriorVersionNodeId(); }
		
		
		public String getNodeId()
		{ return HierarchyScenarioSetImpl.super.getNodeId(); }
		
		
		public String[] getNodeIdPath()
		{ return HierarchyScenarioSetImpl.super.getNodeIdPath(); }
		
		
		public String getNodeIdPathString()
		{ return HierarchyScenarioSetImpl.super.getNodeIdPathString(); }
		
		
		public void setWatch(boolean enable)
		{ HierarchyScenarioSetImpl.super.setWatch(enable); }
		
		
		public boolean getWatch()
		{ return HierarchyScenarioSetImpl.super.getWatch(); }
	
	
		public NodeSer externalize()
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.externalize(); }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.externalize(nodeSer); }
		
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{ HierarchyScenarioSetImpl.super.writeAsXML(writer, indentation); }
		
		
		public void writeSpecializedXMLAttributes(PrintWriter writer)
		throws
			IOException
		{ HierarchyScenarioSetImpl.super.writeSpecializedXMLAttributes(writer); }
		
		
		public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
		throws
			IOException
		{ HierarchyScenarioSetImpl.super.writeSpecializedXMLElements(writer, indentation); }
		
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		{ return HierarchyScenarioSetImpl.super.getAvailableHTTPFormats(descriptions); }
		
		
		public String getWebURLString(String format)
		throws
			ParameterError
		{ return HierarchyScenarioSetImpl.super.getWebURLString(format); }
		
		
		public byte[] renderAsSVG(double wCmMax, double hCmMax)
		throws
			IOException
		{ return HierarchyScenarioSetImpl.super.renderAsSVG(wCmMax, hCmMax); }
	
		
		public java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
			double pixPerModel)
		{ return HierarchyScenarioSetImpl.super.renderHierarchyAsLeaves(g, x, y, pixPerModel); }
		
		
		public java.awt.Dimension render(Graphics2D g, double pixPerModel)
		{ return HierarchyScenarioSetImpl.super.render(g, pixPerModel); }
	
	
		public void renderAsChild(Graphics2D g, double pixPerModel)
		{ HierarchyScenarioSetImpl.super.renderAsChild(g, pixPerModel); }
		
		
		public byte[] renderAsHTML()
		throws
			ParameterError,
			IOException
		{ return HierarchyScenarioSetImpl.super.renderAsHTML(); }
		
	
		public void notifyAllListeners()
		throws
			Exception
		{ HierarchyScenarioSetImpl.super.notifyAllListeners(); }
		
		
		public void notifyNodeDeleted()
		throws
			Exception
		{ HierarchyScenarioSetImpl.super.notifyNodeDeleted(); }
		
		
		public void notifyAllListeners(PeerNotice notice)
		throws
			Exception
		{ HierarchyScenarioSetImpl.super.notifyAllListeners(notice); }
		
		
		public String[] createNodeIdArray(Collection col)
		{ return HierarchyScenarioSetImpl.super.createNodeIdArray(col); }
		
		
		public String[] createNodeIdArray(Map map)
		{ return HierarchyScenarioSetImpl.super.createNodeIdArray(map); }
	
	
		public String getNodeIdOrNull(PersistentNode node)
		{ return HierarchyScenarioSetImpl.super.getNodeIdOrNull(node); }
	
	
		public void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
		{ HierarchyScenarioSetImpl.super.setRefArgIfNotNull(ar, value); }
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws
			CloneNotSupportedException
		{ return HierarchyScenarioSetImpl.super.clone(cloneMap, cloneParent); }
		
		
		public void dump(int indentation)
		{ HierarchyScenarioSetImpl.super.dump(indentation); }
		
		
		public String identify()
		{ return HierarchyScenarioSetImpl.super.identify(); }
		
		
		public void printChildren(int indentationLevel)
		{ HierarchyScenarioSetImpl.super.printChildren(indentationLevel); }
		
		
		public void printChildren(int indentationLevel, boolean recursive)
		{ HierarchyScenarioSetImpl.super.printChildren(indentationLevel, recursive); }
	
	
		public void printChildrenRecursive(int indentationLevel)
		{ HierarchyScenarioSetImpl.super.printChildrenRecursive(indentationLevel); }
	}
}

