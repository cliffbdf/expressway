/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.ser.NodeSer;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.common.*;
import expressway.server.HierarchyElement.*;
import expressway.server.NamedReference.*;
import expressway.ser.HierarchyDomainSer;
import expressway.ser.HierarchyDomainImplSer;
import expressway.geometry.*;
import java.net.URL;
import java.util.List;
import java.util.SortedSet;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import java.util.Collection;
import java.util.Map;
import java.util.Date;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;


public class HierarchyDomainImpl
	extends HierarchyImpl
	implements HierarchyDomain
{
	private HierarchyDomainHelper helper;
	
	
  /* Constructors */
  
  
	public HierarchyDomainImpl(String baseName)
	{
		super(baseName, null);
		this.setViewClassName("expressway.gui.hier.HierarchyScenarioViewPanel");
		this.helper = new HierarchyDomainHelper(this, new SuperDomain(), getHierarchyHelper());
	}
	
	
	public Class getSerClass() { return HierarchyDomainImplSer.class; }
	
	
  /* Local methods */
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.HierarchyDomainIconImageName);
	}
	
	
  /* From Hierarchy */
	
	
	public Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		return new HierarchyImpl(name, this);
	}
	
	
  /* From Domain */
  
	
	public void setScenarios(List<Scenario> ss) { helper.setScenarios(ss); }
	
	
	public void setMenuItems(List<MenuItem> mis) { helper.setMenuItems(mis); }
	
	
	public void setMotifDefs(Set<MotifDef> mds) { helper.setMotifDefs(mds); }
	
	
	public void setScenarioSets(List<ScenarioSet> sss) { helper.setScenarioSets(sss); }
	
	
	public void setNamedReferences(Set<NamedReference> nrs) { helper.setNamedReferences(nrs); }
	
	
	public void setVisualGuidance(VisualGuidance vg) { helper.setVisualGuidance(vg); }


	public String getSourceName() { return helper.getSourceName(); }
	
	
	public void setSourceName(String name) { helper.setSourceName(name); }
	
	
	public URL getSourceURL() { return helper.getSourceURL(); }
	
	
	public void setSourceURL(URL url) { helper.setSourceURL(url); }
	
	
	public String getDeclaredName() { return helper.getDeclaredName(); }
	
	
	public void setDeclaredName(String name) { helper.setDeclaredName(name); }
	
	
	public String getDeclaredVersion() { return helper.getDeclaredVersion(); }
	
	
	public void setDeclaredVersion(String version) { helper.setDeclaredVersion(version); }
	
		
	public Set<MotifDef> getMotifDefs()
	{
		return helper.getMotifDefs();
	}
	
	
	public void addMotifDef(MotifDef md)
	{
		helper.addMotifDef(md);
	}
	
	
	public void removeMotifDef(MotifDef md)
	{
		helper.removeMotifDef(md);
	}
	
	
	public Set<NamedReference> getNamedReferences() { return helper.getNamedReferences(); }
	
	
	public NamedReference getNamedReference(String nrName)
	{
		return helper.getNamedReference(nrName);
	}

	
	public NamedReference createNamedReference(String name, String desc,
		Class fromType, Class toType)
	throws
		ParameterError
	{
		return helper.createNamedReference(name, desc, fromType, toType);
	}

	
	public void deleteNamedReference(NamedReference nr)
	{
		helper.deleteNamedReference(nr);
	}
	
	
	public boolean usesMotif(MotifDef md) { return helper.usesMotif(md); }
	

	public List<Scenario> getScenarios()
	{
		return helper.getScenarios();
	}


	public Scenario getScenario(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		return helper.getScenario(name);
	}	


	public Scenario getCurrentScenario()
	{
		return helper.getCurrentScenario();
	}
	
	
	public void setCurrentScenario(Scenario scenario)
	throws
		ElementNotFound,
		ParameterError
	{
		helper.setCurrentScenario(scenario);
	}


	public Scenario createScenario(String name)
	throws
		ParameterError
	{
		return helper.createScenario(name);
	}


	public Scenario createScenario()
	throws
		ParameterError
	{
		return helper.createScenario();
	}


	public Scenario createScenario(String name, Scenario scenario)
	throws
		ParameterError,
		CloneNotSupportedException
	{
		if (scenario != null)
			if (! (scenario instanceof HierarchyScenario)) throw new RuntimeException(
				"sceanrio is not a HierarchyScenario");
		
		if (name == null) name = createUniqueChildName("Scenario");
		else if (name.equals("")) throw new ParameterError(
			"Cannot use an empty string for a Scenario name");
		else
		{
			List<Scenario> scenarios = helper.getScenarios();
			for (Scenario s : scenarios)
				if (s.getName().equals(name)) throw new ParameterError(
					"Domain " + this.getName() + 
					" already contains a Scenario named " + name);
		}
		
		HierarchyScenario newScenario = null;

		if (scenario == null)
			newScenario = new HierarchyScenarioImpl(name, this);
		else
		{
			newScenario = (HierarchyScenario)(scenario.createIndependentCopy());
			renameChild(newScenario, name);
		}

		helper.addScenario(newScenario);

		ServiceContext context = ServiceContext.getServiceContext();
		context.addScenario(newScenario);

		try { helper.setCurrentScenario(newScenario); }
		catch (ElementNotFound ex) { throw new RuntimeException("Should not happen"); }

		return newScenario;
	}


	public void deleteScenario(Scenario scenario)
	throws
		ParameterError
	{
		helper.deleteScenario(scenario);
	}
	
	
	public void addScenario(Scenario scenario)
	{
		helper.addScenario(scenario);
	}
	
	
	public ScenarioSet createScenarioSet(String name, 
		Scenario baseScenario, Attribute attr, Serializable[] values)
	throws
		ParameterError,
		CloneNotSupportedException
	{
		if (! (baseScenario instanceof HierarchyScenario)) throw new ParameterError(
			"Scenario '" + baseScenario + "' is not a HierarchyScenario");
		if (! (attr instanceof HierarchyAttribute)) throw new ParameterError(
			"Attribute '" + attr.getFullName() + "' is not a HierarchyAttribute");
		
		HierarchyScenario hBaseScenario = (HierarchyScenario)baseScenario;
		String setName = name;
		if (setName == null) setName = baseScenario.getName() + "_" + attr.getName();
		HierarchyAttribute hattr = (HierarchyAttribute)attr;
		

		// For each value of the Attribute's Iterator (in the context of a selected Scenario)

		SortedSet<HierarchyScenario> newScenarios = new TreeSetNullDisallowed<HierarchyScenario>();
		
		if (values != null) for (Serializable value : values)
		{
			// Create a Scenario for that value and link it to the ScenarioSet.
			
			HierarchyScenario newScenario = null;
			try { newScenario = (HierarchyScenario)(baseScenario.createIndependentCopy()); }
			catch (CloneNotSupportedException ex) { throw new ParameterError(ex); }
			
			newScenario.setAttributeValue(attr, value);
			newScenarios.add(newScenario);
		}
		
		HierarchyScenarioSetImpl scenarioSet = new HierarchyScenarioSetImpl(hBaseScenario,
			createUniqueChildName(setName), hattr, newScenarios);
		
		helper.addScenarioSet(scenarioSet);
		
		return scenarioSet;
	}
	
	
	public void deleteScenarioSet(ScenarioSet scenarioSet)
	throws
		ParameterError
	{
		helper.deleteScenarioSet(scenarioSet);
	}
	
	
	public void addScenarioSet(ScenarioSet scenarioSet)
	{
		helper.addScenarioSet(scenarioSet);
	}
	
	
	public List<ScenarioSet> getScenarioSets()
	{
		return helper.getScenarioSets();
	}
	
	
	public ScenarioSet getScenarioSet(String name)
	throws
		ElementNotFound
	{
		return helper.getScenarioSet(name);
	}
	
	
	public VisualGuidance getVisualGuidance()
	{
		return helper.getVisualGuidance();
	}
	
	
  /* From HierarchyDomain */
	

	public HierarchyScenarioSet createHierarchyScenarioSet(String name,
		HierarchyScenario baseScenario, HierarchyAttribute attr, Serializable[] values)
	throws
		ParameterError,
		CloneNotSupportedException
	{
		return (HierarchyScenarioSet)(createScenarioSet(name, baseScenario, attr, values));
	}
	
	
	public List<HierarchyScenario> getHierarchyScenarios()
	{
		return helper.getHierarchyScenarios();
	}
	
	
	public HierarchyScenario getHierarchyScenario(String name)
	throws
		ElementNotFound, // if the HierarchyScenario cannot be found.
		ParameterError
	{
		return helper.getHierarchyScenario(name);
	}
	
	
	public void addHierarchyScenario(HierarchyScenario scenario)
	{
		helper.addHierarchyScenario(scenario);
	}
	
	
	public HierarchyScenarioSet getHierarchyScenarioSet(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		return helper.getHierarchyScenarioSet(name);
	}
		
		
	public List<HierarchyScenarioSet> getHierarchyScenarioSets()
	{
		return helper.getHierarchyScenarioSets();
	}
	
	
	public void addHierarchyScenarioSet(HierarchyScenarioSet s)
	{
		helper.addHierarchyScenarioSet(s);
	}
	
	
	public void deleteHierarchyScenarioSet(HierarchyScenarioSet s)
	throws
		ParameterError
	{
		helper.deleteHierarchyScenarioSet(s);
	}


	public HierarchyScenario getCurrentHierarchyScenario()
	{
		return helper.getCurrentHierarchyScenario();
	}
	
	
	public void setCurrentHierarchyScenario(HierarchyScenario scenario)
	throws
		ElementNotFound,
		ParameterError
	{
		helper.setCurrentHierarchyScenario(scenario);
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		this.setViewClassName("expressway.gui.hier.HierarchyScenarioViewPanel");
		this.helper = new HierarchyDomainHelper(this, new SuperDomain(), getHierarchyHelper());
	}
	
	
  /* From PersistentNode */
	
	
	public String getTagName() { return "hierarchy_domain"; }
	
	
	public String getDefaultDomainViewTypeName() { return "Hierarchy"; }
	
	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		super.renameChild(child, newName);
	}
	
	
	@Override
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		HierarchyDomain newDomain = (HierarchyDomain)(helper.clone(cloneMap, cloneParent));
		return newDomain;
	}

	
	@Override
	public final void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		
		writer.print(indentString 
			+ "<hierarchy_" + (this instanceof MotifDef? "motif" : "domain")
			+ " name=\"" 
			+ (getDeclaredName() == null? this.getName() : getDeclaredName())
			+ "\" "
			+ (getSourceName() == null? "" : "source_name=\"" + getSourceName() + "\" ")
			+ (getSourceURL() == null? "" : "source_url=\"" + getSourceURL() + "\" ")
			+ (getDeclaredVersion() == null? "" : "version=\"" + getDeclaredVersion() + "\" ")
			
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			);

		writeSpecializedXMLAttributes(writer);
		writer.println(">");
			
		
		// Write description, if any.
		
		String desc = getHTMLDescription();
		if (desc != null)
		{
			writer.println(indentString + "\t<description>");
			writer.println(indentString + "\t\t" + desc);
			writer.println(indentString + "\t</description>");
		}
		
		
		writeSpecializedXMLElements(writer, indentation);


		// Write MenuItems, if any.
		
		if (this instanceof MenuOwner)
		{
			List<MenuItem> mis = ((MenuOwner)this).getMenuItems();
			if (mis != null)
			{
				for (MenuItem mi : mis) mi.writeAsXML(writer, indentation+1);
			}
		}
		
		
		// Write each child.
		
		Set<PersistentNode> children = this.getChildren();
		for (PersistentNode child : children)
		{
			writer.println();
			child.writeAsXML(writer, indentation+1);
		}
		
		writer.println();
		writer.println(indentString
			+ "</hierarchy_" + (this instanceof MotifDef? "motif" : "domain") + ">"
			);
	}
	
	
	@Override
	public void prepareForDeletion()
	{
		helper.prepareForDeletion();
	}
	
	
	@Override
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		return helper.getChild(name);
	}
	

	@Override
	public SortedSet<PersistentNode> getChildren()
	{
		return helper.getChildren();
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		helper.deleteChild(child, outermostAffectedRef);
	}

	
	public int getNoOfHeaderRows() { return 2; }
	
	
	public double getPreferredWidthInPixels() { return helper.getPreferredWidthInPixels(); }
	public double getPreferredHeightInPixels() { return helper.getPreferredHeightInPixels(); }
	
	
  /* From PersistentNodeImpl */
	
	
	@Override
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		HierarchyDomainSer ser = (HierarchyDomainSer)(helper.externalize(nodeSer));
		return ser;
	}
	
	
  /* Proxy classes needed by HierarchyDomainHelper */

	
	private class SuperDomain extends SuperMenuOwner
	{
		/* No need to list Domain methods because they do not exist in the superclass
			and hence their super method is never called. */
	}
	
	
	private class SuperMenuOwner extends SuperCrossReferenceable implements MenuOwner
	{
		public Action createAction(int position, String text, String desc, String javaMethodName)
			throws ParameterError
		{ return HierarchyDomainImpl.super.createAction(position, text, desc, javaMethodName); }
		
		public Action createAction(String text, String desc, String javaMethodName) throws ParameterError
		{ return HierarchyDomainImpl.super.createAction(text, desc, javaMethodName); }
		
		public MenuTree createMenuTree(int position, String text, String desc) throws ParameterError
		{ return HierarchyDomainImpl.super.createMenuTree(position, text, desc); }
		
		public MenuTree createMenuTree(String text, String desc) throws ParameterError
		{ return HierarchyDomainImpl.super.createMenuTree(text, desc); }
		
		public void deleteMenuItem(int menuItemPos) throws ParameterError
		{ HierarchyDomainImpl.super.deleteMenuItem(menuItemPos); }
		
		public void deleteMenuItem(MenuItem menuItem) throws ParameterError
		{ HierarchyDomainImpl.super.deleteMenuItem(menuItem); }
		
		public List<MenuItem> getMenuItems()
		{ return HierarchyDomainImpl.super.getMenuItems(); }
		
		public int getIndexOf(MenuItem mi)
		{ return HierarchyDomainImpl.super.getIndexOf(mi); }
	}
	
	
	private class SuperCrossReferenceable extends SuperPersistentNode implements CrossReferenceable
	{
		public Set<CrossReference> getCrossReferences()
		{ return HierarchyDomainImpl.super.getCrossReferences(); }
		
		
		public void addRef(CrossReference cr)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.addRef(cr); }
		
		
		public Set<NamedReference> getNamedReferences()
		{ return HierarchyDomainImpl.super.getNamedReferences(); }
		
		
		public NamedReference getNamedReference(String refName)
		{ return HierarchyDomainImpl.super.getNamedReference(refName); }
		
		
		public Set<CrossReferenceable> getToNodes(NamedReference nr)
		{ return HierarchyDomainImpl.super.getToNodes(nr); }
		
		
		public Set<CrossReferenceable> getFromNodes(NamedReference nr)
		{ return HierarchyDomainImpl.super.getFromNodes(nr); }
		
		
		public Set<CrossReferenceable> getToNodes(String refName)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.getToNodes(refName); }
		
		
		public Set<CrossReferenceable> getFromNodes(String refName)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.getFromNodes(refName); }
		
		
		public void removeRef(CrossReference cr)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.removeRef(cr); }
			
			
		public void addCrossReferenceCreationListener(CrossReferenceCreationListener listener)
		{ HierarchyDomainImpl.super.addCrossReferenceCreationListener(listener); }
		
		public void addCrossReferenceDeletionListener(CrossReferenceDeletionListener listener)
		{ HierarchyDomainImpl.super.addCrossReferenceDeletionListener(listener); }
		
		public List<CrossReferenceCreationListener> getCrossRefCreationListeners()
		{ return HierarchyDomainImpl.super.getCrossRefCreationListeners(); }
		
		public List<CrossReferenceDeletionListener> getCrossRefDeletionListeners()
		{ return HierarchyDomainImpl.super.getCrossRefDeletionListeners(); }
		
		public void removeCrossReferenceListener(CrossReferenceListener listener)
		{ HierarchyDomainImpl.super.removeCrossReferenceListener(listener); }
		
		public void signalCrossReferenceCreated(CrossReference cr)
		throws
			Exception
		{ HierarchyDomainImpl.super.signalCrossReferenceCreated(cr); }
		
		public void signalCrossReferenceDeleted(NamedReference nr,
			CrossReferenceable fromNode, CrossReferenceable toNode)
		throws
			Exception
		{ HierarchyDomainImpl.super.signalCrossReferenceDeleted(nr, fromNode, toNode); }
	}
	
	
	private class SuperPersistentNode implements PersistentNode
	{
		public int compareTo(PersistentNode node)
		{
			throw new RuntimeException("Should not be called");
		}
		
		
		public Object clone() throws CloneNotSupportedException
		{
			throw new RuntimeException("Should not be called");
		}

		
	  /* From PersistentNode */
		
		
		public ModelEngineLocal getModelEngine()
		{ return HierarchyDomainImpl.super.getModelEngine(); }
	
	
		public List<String> getInstantiableNodeKinds()
		{ return HierarchyDomainImpl.super.getInstantiableNodeKinds(); }
		
		
		public List<String> getStaticCreateMethodNames()
		{ return HierarchyDomainImpl.super.getStaticCreateMethodNames(); }
		
		
		public List<String> getInstantiableNodeDescriptions()
		{ return HierarchyDomainImpl.super.getInstantiableNodeDescriptions(); }
		
		
		public PersistentNode createChild(String kind, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.createChild(kind, layoutBound, outermostAffectedRef); }
		
		
		public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.deleteChild(child, outermostAffectedRef); }
			
		
		public void deleteChild(PersistentNode child)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.deleteChild(child); }
			
			
		public Template findTemplate(String templatePath)
		throws
			ElementNotFound,
			ParameterError
		{ return HierarchyDomainImpl.super.findTemplate(templatePath); }
		
		
		public Class getNodeKind(String className)
		throws
			ClassNotFoundException,
			ParameterError
		{ return HierarchyDomainImpl.super.getNodeKind(className); }
			
		
		public Class getSerClass()
		{ return HierarchyDomainImpl.super.getSerClass(); }
		
		
		public Set<Attribute> getAttributes()
		{ return HierarchyDomainImpl.super.getAttributes(); }
		
		
		public String getTagName()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Attribute getAttribute(String attrName)
		{ return HierarchyDomainImpl.super.getAttribute(attrName); }
		
		
		public int getAttributeSeqNo(Attribute attr)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.getAttributeSeqNo(attr); }
		

		public void setAttributeSeqNo(Attribute attr, int seqNo)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.setAttributeSeqNo(attr, seqNo); }
				
				
		public void setAttributeAsFirstInSeq(Attribute attr)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.setAttributeAsFirstInSeq(attr); }
		
		
		public void setAttributeAsLastInSeq(Attribute attr)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.setAttributeAsLastInSeq(attr); }
		
		
		public void moveAttributeBefore(Attribute attr, Attribute nextAttr)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.moveAttributeBefore(attr, nextAttr); }
		
		
		public void moveAttributeAfter(Attribute attr, Attribute priorAttr)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.moveAttributeAfter(attr, priorAttr); }
	
		
		public int getFirstAttributeSeqNo()
		{ return HierarchyDomainImpl.super.getFirstAttributeSeqNo(); }
		
		
		public int getLastAttributeSeqNo()
		{ return HierarchyDomainImpl.super.getLastAttributeSeqNo(); }
		
		
		public int getNumberOfAttributes()
		{ return HierarchyDomainImpl.super.getNumberOfAttributes(); }


		public List<Attribute> getAttributesInSequence()
		{ return HierarchyDomainImpl.super.getAttributesInSequence(); }
		
		
		public Attribute getFirstAttribute()
		{ return HierarchyDomainImpl.super.getFirstAttribute(); }
		
		
		public Attribute getLastAttribute()
		{ return HierarchyDomainImpl.super.getLastAttribute(); }

		
		public Attribute getAttributeAtSeqNo(int seqNo)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.getAttributeAtSeqNo(seqNo); }


		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.constructAttribute(name, defaultValue); }
		
	
		public Attribute constructAttribute(String name)
		{ return HierarchyDomainImpl.super.constructAttribute(name); }
		
	
		public Attribute createAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		
		public Attribute createAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.createAttribute(layoutBound, outermostAffectedRef); }
	
	
		public Attribute createAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.createAttribute(name, layoutBound, outermostAffectedRef); }
		
		
		public void deleteAttribute(Attribute attribute)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.deleteAttribute(attribute); }
			
			
		public void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.deleteAttribute(attribute, outermostAffectedRef); }
			
			
		public boolean isOwnedByMotifDef()
		{ return HierarchyDomainImpl.super.isOwnedByMotifDef(); }
		
		
		public boolean isOwnedByActualTemplateInstance()
		{ return HierarchyDomainImpl.super.isOwnedByActualTemplateInstance(); }
		
		
		public boolean actuallyUsesMotif(MotifDef md)
		{ return HierarchyDomainImpl.super.actuallyUsesMotif(md); }
		
		
		public Image getLocalStoredImage(String handle)
		{ return HierarchyDomainImpl.super.getLocalStoredImage(handle); }
		
		
		public void deleteFromAllNodesTable()
		{ HierarchyDomainImpl.super.deleteFromAllNodesTable(); }
		
		
		public String getName()
		{ return HierarchyDomainImpl.super.getName(); }
		
		
		public void setHTMLDescription(String html)
		{ HierarchyDomainImpl.super.setHTMLDescription(html); }
		
		
		public String getHTMLDescription()
		{ return HierarchyDomainImpl.super.getHTMLDescription(); }
		
		
		public String getFullName()
		{ return HierarchyDomainImpl.super.getFullName(); }
		
		
		public String getNameRelativeTo(PersistentNode parent)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.getNameRelativeTo(parent); }
	
	
		public void setName(String name)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.setName(name); }
		
		
		public String setNameAndNotify(String newName)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.setNameAndNotify(newName); }
		
		
		public void renameChild(PersistentNode child, String newName)
		throws
			ParameterError
		{ HierarchyDomainImpl.super.renameChild(child, newName); }
		
		
		public Domain getDomain()
		{ return HierarchyDomainImpl.super.getDomain(); }
		
		
		public String createUniqueChildName()
		{ return HierarchyDomainImpl.super.createUniqueChildName(); }
	
		
		public String createUniqueChildName(String baseName)
		{ return HierarchyDomainImpl.super.createUniqueChildName(baseName); }
		
		
		public SortedSet<PersistentNode> getChildren()
		{ return HierarchyDomainImpl.super.getChildren(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive()
		{ return HierarchyDomainImpl.super.getChildrenRecursive(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive(Class kind)
		{ return HierarchyDomainImpl.super.getChildrenRecursive(kind); }
		
		
		public PersistentNode getChild(String name)
		throws
			ElementNotFound
		{ return HierarchyDomainImpl.super.getChild(name); }
	
	
		public PersistentNode getParent()
		{ return HierarchyDomainImpl.super.getParent(); }
		
		
		public void setParent(PersistentNode parent)
		{ HierarchyDomainImpl.super.setParent(parent); }
		
		
		public boolean isDescendantOf(PersistentNode ancestor)
		{ return HierarchyDomainImpl.super.isDescendantOf(ancestor); }
		
		
		public PersistentNode findNode(String qualifiedName)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.findNode(qualifiedName); }
	
	
		public PersistentNode findNode(String[] pathParts)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.findNode(pathParts); }
			
		
		public PersistentNode findFirstNestedNode(String name)
		throws
			ElementNotFound
		{ return HierarchyDomainImpl.super.findFirstNestedNode(name); }
		
		
		public PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.getChildByRelativeQualifiedName(pathParts); }
		
		
		public void prepareForDeletion()
		{ HierarchyDomainImpl.super.prepareForDeletion(); }
		
		
		public Display getDisplay()
		{ return HierarchyDomainImpl.super.getDisplay(); }
		
		
		public void setVisible(boolean vis)
		{ HierarchyDomainImpl.super.setVisible(vis); }
		
		
		public boolean isVisible()
		{ return HierarchyDomainImpl.super.isVisible(); }
		
		
		public void layout()
		{ HierarchyDomainImpl.super.layout(); }
		
		
		public Set<PersistentNode> getNodesWithSpecialLayout()
		{ return HierarchyDomainImpl.super.getNodesWithSpecialLayout(); }
		
		
		public void conditionalLayout()
		{ HierarchyDomainImpl.super.conditionalLayout(); }
		
		
		public PersistentNode layoutUpward(PersistentNode layoutBound)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.layoutUpward(layoutBound); }
	
		
		public void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		{ HierarchyDomainImpl.super.layout(layoutBound, outermostAffectedRef); }
	
	
		public void setPredefined(boolean pre)
		{ HierarchyDomainImpl.super.setPredefined(pre); }
		
		
		public boolean isPredefined()
		{ return HierarchyDomainImpl.super.isPredefined(); }
		
		
		public void setDeletable(boolean del)
		{ HierarchyDomainImpl.super.setDeletable(del); }
		
		
		public boolean isDeletable()
		{ return HierarchyDomainImpl.super.isDeletable(); }
		
		
		public void setMovable(boolean m)
		{ HierarchyDomainImpl.super.setMovable(m); }
		
		
		public boolean isMovable()
		{ return HierarchyDomainImpl.super.isMovable(); }
		
		
		public void setResizable(boolean yes)
		{ HierarchyDomainImpl.super.setResizable(yes); }
		
		
		public boolean isResizable()
		{ return HierarchyDomainImpl.super.isResizable(); }
		
		
		public double getWidth()
		{return HierarchyDomainImpl.super.getWidth();  }
	
	
		public double getHeight()
		{ return HierarchyDomainImpl.super.getHeight(); }
		
		
		public void setWidth(double width)
		{ HierarchyDomainImpl.super.setWidth(width); }
	
	
		public void setHeight(double height)
		{ HierarchyDomainImpl.super.setHeight(height); }
		
		
		public boolean useBorderWhenIconified()
		{ return HierarchyDomainImpl.super.useBorderWhenIconified(); }
		
		
		public void setUseBorderWhenIconified(boolean yesOrNo)
		{ HierarchyDomainImpl.super.setUseBorderWhenIconified(yesOrNo); }
		
		
		public Image getIconImage()
		{ return HierarchyDomainImpl.super.getIconImage(); }
		
		
		public void setIconImage(String path)
		throws
			IOException
		{ HierarchyDomainImpl.super.setIconImage(path); }
		
		
		public ImageIcon getImageIcon()
		{ return HierarchyDomainImpl.super.getImageIcon(); }
		
		
		public String getIconImageHandle()
		{ return HierarchyDomainImpl.super.getIconImageHandle(); }
		
		
		public double getIconWidth()
		{ return HierarchyDomainImpl.super.getIconWidth(); }
		
		
		public double getIconHeight()
		{ return HierarchyDomainImpl.super.getIconHeight(); }
		
		
		public void setLayoutManager(LayoutManager lm)
		{ HierarchyDomainImpl.super.setLayoutManager(lm); }
		
		
		public LayoutManager getLayoutManager()
		{ return HierarchyDomainImpl.super.getLayoutManager(); }
		
		
		public String getDefaultViewTypeName()
		{ throw new RuntimeException("Should not be called"); }
		
		public void setViewClassName(String name)
		{ HierarchyDomainImpl.super.setViewClassName(name); }
		
		public String getViewClassName()
		{ return HierarchyDomainImpl.super.getViewClassName(); }

		
		public boolean contains(Point p)
		{ return HierarchyDomainImpl.super.contains(p); }
		
		
		public boolean contains(double x, double y)
		{ return HierarchyDomainImpl.super.contains(x, y); }
		
		
		public Face[] getFaces(double x, double y)
		{ return HierarchyDomainImpl.super.getFaces(x, y); }
		
		
		public Face[] getIconFaces(double x, double y)
		{ return HierarchyDomainImpl.super.getIconFaces(x, y); }
		
		
		public Position findSideClosestToInternalPoint(double x, double y)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.findSideClosestToInternalPoint(x, y); }
		
		
		public Position findSideClosestToExternalPoint(Point location)
		{ return HierarchyDomainImpl.super.findSideClosestToExternalPoint(location); }
		
		
		public void setSizeToStandard()
		{ HierarchyDomainImpl.super.setSizeToStandard(); }
		
		
		public double getHeaderLabelHeight()
		{ return HierarchyDomainImpl.super.getHeaderLabelHeight(); }
		
		
		public int getNoOfHeaderRows()
		{ throw new RuntimeException("Should not be called"); }
	
	
		public SortedSet<PersistentNode> getPredefinedNodes()
		{ return HierarchyDomainImpl.super.getPredefinedNodes(); }
		
		
		public double getMinimumWidth()
		{ return HierarchyDomainImpl.super.getMinimumWidth(); }
		
		
		public double getMinimumHeight()
		{ return HierarchyDomainImpl.super.getMinimumHeight(); }
		
		
		public double getPreferredWidthInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		public double getPreferredHeightInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		
		public int getPreferredHeaderRowHeightInPixels()
		{ return HierarchyDomainImpl.super.getPreferredHeaderRowHeightInPixels(); }
		
		
		public double getX() { return HierarchyDomainImpl.super.getX(); }
		public double getY() { return HierarchyDomainImpl.super.getY(); }
		public double getScale() { return HierarchyDomainImpl.super.getScale(); }
		public double getOrientation() { return HierarchyDomainImpl.super.getOrientation(); }
		public void setX(double x) { HierarchyDomainImpl.super.setX(x); }
		public void setY(double y) { HierarchyDomainImpl.super.setY(y); }
		public void setLocation(double x, double y) { HierarchyDomainImpl.super.setLocation(x, y); }
		public void setScale(double scale) { HierarchyDomainImpl.super.setScale(scale); }
		public void setOrientation(double angle) { HierarchyDomainImpl.super.setOrientation(angle); }
		
		
		public boolean allowShift(PersistentNode childNode)
		{ return HierarchyDomainImpl.super.allowShift(childNode); }
		
		
		public long getLastUpdated()
		{ return HierarchyDomainImpl.super.getLastUpdated(); }
		
		
		public int getVersionNumber()
		{ return HierarchyDomainImpl.super.getVersionNumber(); }
		
		
		public String getClientThatLastModified()
		{ return HierarchyDomainImpl.super.getClientThatLastModified(); }
		
		
		public void update()
		{ HierarchyDomainImpl.super.update(); }
		
	
		public Date getDeletionDate()
		{ return HierarchyDomainImpl.super.getDeletionDate(); }
		
	
		public PersistentNode getPriorVersion()
		{ return HierarchyDomainImpl.super.getPriorVersion(); }
		
		
		public String getPriorVersionNodeId()
		{ return HierarchyDomainImpl.super.getPriorVersionNodeId(); }
		
		
		public String getNodeId()
		{ return HierarchyDomainImpl.super.getNodeId(); }
		
		
		public String[] getNodeIdPath()
		{ return HierarchyDomainImpl.super.getNodeIdPath(); }
		
		
		public String getNodeIdPathString()
		{ return HierarchyDomainImpl.super.getNodeIdPathString(); }
		
		
		public void setWatch(boolean enable)
		{ HierarchyDomainImpl.super.setWatch(enable); }
		
		
		public boolean getWatch()
		{ return HierarchyDomainImpl.super.getWatch(); }
	
	
		public NodeSer externalize()
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.externalize(); }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.externalize(nodeSer); }
		
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{ HierarchyDomainImpl.super.writeAsXML(writer, indentation); }
		
		
		public void writeSpecializedXMLAttributes(PrintWriter writer)
		throws
			IOException
		{ HierarchyDomainImpl.super.writeSpecializedXMLAttributes(writer); }
		
		
		public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
		throws
			IOException
		{ HierarchyDomainImpl.super.writeSpecializedXMLElements(writer, indentation); }
		
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		{ return HierarchyDomainImpl.super.getAvailableHTTPFormats(descriptions); }
		
		
		public String getWebURLString(String format)
		throws
			ParameterError
		{ return HierarchyDomainImpl.super.getWebURLString(format); }
		
		
		public byte[] renderAsSVG(double wCmMax, double hCmMax)
		throws
			IOException
		{ return HierarchyDomainImpl.super.renderAsSVG(wCmMax, hCmMax); }
	
		
		public java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
			double pixPerModel)
		{ return HierarchyDomainImpl.super.renderHierarchyAsLeaves(g, x, y, pixPerModel); }
		
		
		public java.awt.Dimension render(Graphics2D g, double pixPerModel)
		{ return HierarchyDomainImpl.super.render(g, pixPerModel); }
	
	
		public void renderAsChild(Graphics2D g, double pixPerModel)
		{ HierarchyDomainImpl.super.renderAsChild(g, pixPerModel); }
		
		
		public byte[] renderAsHTML()
		throws
			ParameterError,
			IOException
		{ return HierarchyDomainImpl.super.renderAsHTML(); }
		
	
		public void notifyAllListeners()
		throws
			Exception
		{ HierarchyDomainImpl.super.notifyAllListeners(); }
		
		
		public void notifyNodeDeleted()
		throws
			Exception
		{ HierarchyDomainImpl.super.notifyNodeDeleted(); }
		
		
		public void notifyAllListeners(PeerNotice notice)
		throws
			Exception
		{ HierarchyDomainImpl.super.notifyAllListeners(notice); }
		
		
		public String[] createNodeIdArray(Collection col)
		{ return HierarchyDomainImpl.super.createNodeIdArray(col); }
		
		
		public String[] createNodeIdArray(Map map)
		{ return HierarchyDomainImpl.super.createNodeIdArray(map); }
	
	
		public String getNodeIdOrNull(PersistentNode node)
		{ return HierarchyDomainImpl.super.getNodeIdOrNull(node); }
	
	
		public void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
		{ HierarchyDomainImpl.super.setRefArgIfNotNull(ar, value); }
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws
			CloneNotSupportedException
		{ return HierarchyDomainImpl.super.clone(cloneMap, cloneParent); }
		
		
		public void dump(int indentation)
		{ HierarchyDomainImpl.super.dump(indentation); }
		
		
		public String identify()
		{ return HierarchyDomainImpl.super.identify(); }
		
		
		public void printChildren(int indentationLevel)
		{ HierarchyDomainImpl.super.printChildren(indentationLevel); }
		
		
		public void printChildren(int indentationLevel, boolean recursive)
		{ HierarchyDomainImpl.super.printChildren(indentationLevel, recursive); }
	
	
		public void printChildrenRecursive(int indentationLevel)
		{ HierarchyDomainImpl.super.printChildrenRecursive(indentationLevel); }
	}
}

