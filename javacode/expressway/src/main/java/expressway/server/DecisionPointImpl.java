/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.SortedSet;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.IOException;


public class DecisionPointImpl extends DecisionElementImpl implements DecisionPoint
{
	/** Ownership. */
	Set<Parameter> parameters = new TreeSetNullDisallowed<Parameter>();

	Set<PrecursorRelationship> precursorDecisionPointRelns = new TreeSetNullDisallowed<PrecursorRelationship>();

	Set<PrecursorRelationship> subordinateDecisionPointRelns = new TreeSetNullDisallowed<PrecursorRelationship>();

	Set<Decision> decisions = new TreeSetNullDisallowed<Decision>();

	protected Class nativeImplementationClass = null;


	public DecisionPointImpl(String name, DecisionDomain dd)
	throws
		ParameterError
	{
		super(dd);
		setName(name);
		setResizable(false);
	}
	
	
	public String getTagName() { return "decision_point"; }


	public int getNoOfHeaderRows() { return 0; }
	
	
	public void setIconImageToDefault() throws IOException { setIconImage(NodeIconImageNames.DecisionPointIconImageName); }


	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		if (child instanceof Parameter)
			if (parameters.contains(child))
				child.setNameAndNotify(newName);
			else
				super.renameChild(child, newName);
		else
			super.renameChild(child, newName);
	}


	public Class getSerClass() { return DecisionPointSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((DecisionPointSer)nodeSer).parameterNodeIds = createNodeIdArray(parameters);
		((DecisionPointSer)nodeSer).decisionNodeIds = createNodeIdArray(decisions);

		((DecisionPointSer)nodeSer).precursorDecisionPointNodeIds = 
			createNodeIdArray(getPrecursorDecisionPoints());
		((DecisionPointSer)nodeSer).subordinateDecisionPointNodeIds = 
			createNodeIdArray(getSubordinateDecisionPoints());
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DecisionPointImpl newInstance = (DecisionPointImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.parameters = cloneNodeSet(parameters, cloneMap, newInstance);
		newInstance.precursorDecisionPointRelns = cloneNodeSet(precursorDecisionPointRelns, cloneMap, newInstance);
		newInstance.subordinateDecisionPointRelns = cloneNodeSet(subordinateDecisionPointRelns, cloneMap, newInstance);
		newInstance.decisions = cloneNodeSet(decisions, cloneMap, newInstance);
		
		return newInstance;
	}
	
	
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		Set<PersistentNode> nodes = getChildren();
		for (PersistentNode node : nodes)
			if (node.getName().equals(name)) return node;
		
		throw new ElementNotFound("Node with name '" + name + "'");
	}
	
	
	public SortedSet<PersistentNode> getChildren()
	{
		SortedSet<PersistentNode> nodes = super.getChildren();
		nodes.addAll(parameters);
		
		return nodes;
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		try { super.deleteChild(child, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			if (child instanceof Parameter)
			{
				if (! parameters.remove(child)) throw new RuntimeException(
					"Node '" + child.getName() + "' not found within '" + getFullName() + "'");
			}
			else
				throw pe;
			
			setRefArgIfNotNull(outermostAffectedRef, this);
		}
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		Set<Parameter> parametersCopy = new TreeSetNullDisallowed<Parameter>(parameters);
		for (Parameter p : parametersCopy) deleteParameter(p);
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	public Set<Parameter> getParameters()  // decision determined by these.
	{
		return parameters;
	}


	public Variable getVariable(String name)
	throws
		ParameterError
	{
		for (Parameter parameter : getParameters())
		{
			if (parameter instanceof Variable)
			{
				if (parameter.getName().equals(name))
				{
					return (Variable)parameter;
				}
			}
		}

		return null;
	}


	public Variable createVariable(String name)
	throws
		ParameterError
	{
		Variable v = new VariableImpl(name, this);
		addParameter(v);
		return v;
	}


	public DecisionFunction createDecisionFunction(String name)
	throws
		ParameterError
	{
		DecisionFunction df = new DecisionFunctionImpl(name, this);
		addParameter(df);
		return df;
	}


	public ParameterAlias createParameterAlias(String name)
	throws
		ParameterError
	{
		ParameterAlias pa = new ParameterAliasImpl(name, this);
		addParameter(pa);
		return pa;
	}


	public ConstantValue createConstantValue(String name)
	throws
		ParameterError
	{
		ConstantValue cv = new ConstantValueImpl(name, this);
		addParameter(cv);
		return cv;
	}


	public void deleteParameter(Parameter param)
	{
		try { deleteChild(param); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}
	
	
	public Set<Decision> getDecisions()
	{
		return decisions;
	}


	public void addDecision(Decision decision)
	{
		decisions.add(decision);
		decision.setDecisionPoint(this);
	}


	public void removeDecision(Decision decision)
	{
		decisions.remove(decision);
		if (decision != null) decision.setDecisionPoint(null);
	}


	public Set<DecisionPoint> getPrecursorDecisionPoints()
	{
		Set<DecisionPoint> dps = new TreeSetNullDisallowed<DecisionPoint>();
		for (PrecursorRelationship reln : precursorDecisionPointRelns)
			try { dps.add(reln.getPrecursorDecisionPoint()); }
			catch (ValueNotSet w) {}
		return dps;
	}


	public Set<DecisionPoint> getSubordinateDecisionPoints()
	{
		Set<DecisionPoint> dps = new TreeSetNullDisallowed<DecisionPoint>();
		for (PrecursorRelationship reln : subordinateDecisionPointRelns)
			try { dps.add(reln.getSubordinateDecisionPoint()); }
			catch (ValueNotSet w) {}
		return dps;
	}
	
	
	void removePrecursorReln(PrecursorRelationship pr)
	{
		precursorDecisionPointRelns.remove(pr);
	}
	
	
	void removeSubordinateReln(PrecursorRelationship pr)
	{
		subordinateDecisionPointRelns.remove(pr);
	}


	public Set<Decision> evaluate(DecisionScenario decisionScenario)
	throws
		ModelContainsError
	{
		/*
		 * Check that this DecisionPoint is in the DecisionDomain that owns the
		 * DecisionScenario.
		 */

		if (! decisionScenario.getDecisionDomain().getDecisionPoints().contains(this))
			throw new RuntimeException(
				"DecisionPoint is NOT in the DecisionDomain that owns the DecisionScenario.");


		/*
		 * Evaluate the rules of this DecisionPoint. This produces a Set of Decisions.
		 */

		Set<Decision> decisions = null;

		if (nativeImplementationClass != null)
		{
			NativeDecisionPointImplementation nativeImplementation =
				decisionScenario.getNativeImplementation(this);
			
			if (nativeImplementation != null)
				decisions = nativeImplementation.evaluate(this, decisionScenario);
				// may be null, indicating that there are no decisions.
		}

		if (decisions == null) return new TreeSetNullDisallowed<Decision>();
			// just return an empty Set.


		/*
		 * Implement Decisions: make sure that they are connected to the appropriate
		 * DecisionScenario, DecisionPoint, and Variable.
		 */

		for (Decision decision : decisions)
		{
			// Check that the Decision has a DecisionPoint. (Only Choice Decisions are
			// permitted to not have a DecisionPoint, and this method may not produce
			// Choice Decisions.)

			DecisionPoint dp = null;
			try { dp = decision.getDecisionPoint(); }
			catch (ValueNotSet w)
			{
				// Connect the Decision to this DecisionPoint.
				decisions.add(decision);
				decision.setDecisionPoint(this);
			}


			// Check that the Decision is owned by the DecisionScenario.

			DecisionScenario ds = decision.getDecisionScenario();
			if (ds == null)
			{
				// Connect the Decision to the DecisionScenario.

				decisionScenario.getDecisions().add(decision);
				decision.setDecisionScenario(decisionScenario);
			}
			else if (! (decisionScenario == ds))
			{
				throw new ModelContainsError(
					"DecisionPoint produced a Decision that is not owned by " +
					"the right DecisionScenario.");
			}


			// Check that the Decision references a Variable.

			Variable variable = null;
			try { variable = decision.getVariable(); }
			catch (ValueNotSet w)
			{
				throw new ModelContainsError(
					"DecisionPoint " + getName() +
					" produced a Decision that does not reference a Variable.");
			}


			// Check that variable is in the DecisionPoint's set of Parameters.

			if (! parameters.contains(variable))
			{
				throw new ModelContainsError(
					"DecisionPoint produced a Decision that references a " +
					"Variable that is not in the DecisionPoint's Parameter set.");
			}
		}


		return decisions;
	}


	public void setNativeImplementationClass(Class implClass)
	throws
		ParameterError
	{
		this.nativeImplementationClass = implClass;
		//impl.setDecisionElement(this);
		
		if (implClass == null) return;
		
		if (! DecisionPoint.NativeDecisionPointImplementation.class.isAssignableFrom(implClass))
			throw new ParameterError(implClass.getName() + 
				" must implement NativeDecisionPointImplementation");

		//GlobalConsole.println("333Called setDecisionElement for impl " +
		//	"to set it to " + getName());
	}


	public Class getNativeImplementationClass()
	{
		return nativeImplementationClass;
	}


	public void dump(int indentation)
	{
		String padding = "";
		for (int i = 1; i <= indentation; i++) padding +="\t";
		GlobalConsole.println(padding + super.toString() + ": " + getName());
		GlobalConsole.println(padding + "\tParameters:");
		for (Parameter parameter : parameters) parameter.dump(indentation+2);
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		setResizable(false);
	}
	

	/*
	 * ------------------------------------------------------------------------
	 * Protected methods.
	 */

	protected void addParameter(Parameter p)
	{
		parameters.add(p);
	}
}
