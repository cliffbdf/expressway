/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.ser.EventSer;
import java.io.IOException;
import java.io.Serializable;
import java.util.SortedSet;
import java.util.Date;


/**
 * Signals a change to a State. Events travel along Conduits.
 */

public interface Event
{
	EventSer externalize() throws ModelContainsError;
	
	
	Class getSerClass();
	
	
	void prepareForDeletion();
	
	
	State getState();
	
	
	/** Return true if this Event has zero duration, representing a 'pulse'. */
	boolean isPulse();
	
	
	/** Return true if this Event will be followed by a compensation Event. */
	void setPulse(boolean p);
	
	
	/** The time at which the Event should occur.
		null indicates that the event should happen in the next epoch. */
	Date getTimeToOccur();
	
	
	Serializable getNewValue();
	
	
	/**
	 * Sorted according to increasing time. Events at the same time may occur
	 * in any order relative to each other. Two Events in the same time are
	 * only equal if all fields are equal. A null-time Event occurs before any
	 * non-null time Event. A Startup Event occurs before any non-Startup Event.
	 */

	public interface SortedEventSet<T extends Event> extends SortedSet<T>
	{
		public Object clone();
	}


	/**
	 * An Event that is dispatched during simulation, rather than being a static
	 * Model Element. All Events propagated by the simulator are Simulation Time
	 * Events, including Startup Events.
	 * A Simulation Time Event is associated with a Simulation Run. 
	 */
	 
	public interface SimulationTimeEvent extends Event, Comparable
	{
		/** Unique among the set of Events for the owning SimulationRun. */
		long getId();
		
		
		SimulationRun getSimulationRun();
		
		
		/** The epoch in which this Event actually occurred, or null if it has
			not yet occurred. */
		Epoch getEpoch();
		
		
		void setEpoch(Epoch epoch);
		
		
		/** The iteration in which this Event occurs. Should not be called if
			the Event has not yet occurred. */
		int getIteration();
		
		
		/** Write the Event as a String. Used for archiving Events in human readable
			format, according to the syntax rules in the slide "Archive Syntax". */
		String encodeAsString()
		throws
			IOException;
	}


	/**
	 * An special Event that signals an EventProducer to start driving
	 * its States. Startup Events are not part of the Event model: they are
	 * only a convenience for the simulator implementation. Therefore, they should
	 * not appear in any Event histories provided to the user. Also, a Startup Event
	 * is not associated with a State, and so the method getState() returns null.
	 */

	public interface StartupEvent extends SimulationTimeEvent
	{
		/** The Event Producer that should be notified. */
		ModelComponent getComponent();
	}


	/**
	 * An Event that is dynamically generated during a Simulation Run. This includes
	 * all Simulated Events that are part of the Event model. (Thus, Startup Events
	 * are not Generated Events.) Only GeneratedEvents are passed to an Activity
	 * or Function's respond(...) method.
	 */

	public interface GeneratedEvent extends SimulationTimeEvent
	{
		/** The epoch in which this Event was scheduled. Null for Events that
			are created to implement pre-defined Event. */
			
		Epoch getWhenScheduled();
		
		
		/** The Event that caused this Event to be scheduled. If there is more
			that one triggering Event, only one is identified. This method is
			used primarily for diagnosing simulation errors. Null for Events that
			are created to implement pre-defined Event. */
			
		SortedEventSet<GeneratedEvent> getTriggeringEvents();
		
		
		void setTriggeringEvents(SortedEventSet<GeneratedEvent> events);
		
		
		/** Return true if this Event was automatically generated to compensate for
			an Event that occurred in the prior Epoch. */
			
		boolean isCompensation();
		
		
		void setCompensation(boolean comp);
	}
}



