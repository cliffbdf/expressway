/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.PeerNotice;
import expressway.server.MotifElement.Template;
import expressway.server.MotifElement.MotifDef;
import expressway.geometry.Point;
import expressway.geometry.Face;
import java.util.*;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.ImageIcon;


/**
 * Supertype for all persistent objects, maintained by an object database or
 * an object-relational mapping to another kind of database. Not all implementations
 * of this type are persistent: this is the abstract supertype of the objects that
 * are persisted.
 */

public interface PersistentNode
extends
	Comparable<PersistentNode>,
	Display,
	Cloneable
{
	ModelEngineLocal getModelEngine();


	interface AttributeValidator extends Serializable
	{
		void validate(Serializable newValue)
		throws
			ParameterError;  // if newValue is not valid under the rules
							// of this Validator.
							
		AttributeValidator makeCopy() throws CloneNotSupportedException;
	}
	
	
	interface AttributeChangeListener
	{
		void attributeDeleted(Attribute attribute);
		void attributeNameChanged(Attribute attribute, String oldName);
	}
	
	
	/** Return the Class names of child Nodes that can be instantiated into
		this Node. Entries for Templates are at the end. */
	List<String> getInstantiableNodeKinds();
	
	
	/** Return the names of the methods used to instantiate
		Node of the kinds returned by the getInstantiableNodeKinds()
		method. Templates do not have entries. */
		
	List<String> getStaticCreateMethodNames();
	
	
	/** Return a description for each Node kind returned by getInstantiableNodeKinds.
		Include entries for Templates. An entry that begins with a '@'is
		treated as a page name. See HelpfulMenuItem. Entries for Templates 
		are at the end. */
	List<String> getInstantiableNodeDescriptions();
	
	
	/** Create a Node of the specified kind, as an owned Node (child) of this
		Node. The kind must be a kind returned by the getInstantiableNodeKinds() method. */
		
	PersistentNode createChild(String kind, 
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;  // if the kind cannot be instantiated in this Node.
	
	
	/** See slide "The Delete Pattern".
		Remove the specified child Node from this Node. Return in outermostAffectedRef[0]
		the outermost containing Node that is visually affected. Call prepareForDeletion()
		on the child. */
	 
	void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;  // if this Node does not own the specified child Node.
		
	
	void deleteChild(PersistentNode child)
	throws
		ParameterError;  // if this Node does not own the specified child Node.
		
		
	Template findTemplate(String templatePath)
	throws
		ElementNotFound,
		ParameterError;
	
	
	/** Find the specified Node class. May be an abstract class or interface. */
	
	Class getNodeKind(String className)
	throws
		ClassNotFoundException,  // not found.
		ParameterError;  // an error with the class name.
		
	
	Class getSerClass();
	
	
	Set<Attribute> getAttributes();
	
	
	String getTagName();
	
	
	Attribute getAttribute(String attrName);
	
	
	/** ************************************************************************
	 * Return the ordinal sequence of the specified Attribute among the Attributes
	 * owned by this Node. The sequence number should be the same on
	 * successive calls, but it may change if an Attribute is added to or removed
	 * from this Node. The sequence number starts with 1 (not 0).
	 */
	 
	int getAttributeSeqNo(Attribute attr)
	throws
		ParameterError;  // if the Attribute does not belong to this Node.
		
		
	/** ************************************************************************
	 * See also getAttributeSeqNo(Attribute).
	 */
	 
	void setAttributeSeqNo(Attribute attr, int seqNo)
	throws
		ParameterError;  // If the Attribute does not belong to this Node, or
			// if the seqNo is out of bounds.
			
			
	void setAttributeAsFirstInSeq(Attribute attr)
	throws
		ParameterError;
	
	
	void setAttributeAsLastInSeq(Attribute attr)
	throws
		ParameterError;
	
	
	void moveAttributeBefore(Attribute attr, Attribute nextAttr)
	throws
		ParameterError;
	
	
	void moveAttributeAfter(Attribute attr, Attribute priorAttr)
	throws
		ParameterError;

	
	/**
	 * Return the seq no of the first Attribute of this PersistentNode, or 0
	 * if this Node has no Attributes.
	 */
	 
	int getFirstAttributeSeqNo();
	
	
	/**
	 * Return the seq no of the last Attribute, or 0 if this Node has no Attributes.
	 */
	 
	int getLastAttributeSeqNo();
	
	
	int getNumberOfAttributes();

	
	/** ************************************************************************
	 * Return this Node's Attributes in a deterministic sequence that is the same
	 * each time this method is called. However, adding or removing an Attribute
	 * may change the sequence for all Attributes.
	 */
	 
	List<Attribute> getAttributesInSequence();
	
	
	Attribute getFirstAttribute();
	
	
	Attribute getLastAttribute();
	
	
	Attribute getAttributeAtSeqNo(int seqNo)
	throws
		ParameterError;  // if there is no Attribute at the seqNo.
	
	
	/** ************************************************************************
	 * Construct an Attribute for this Node. Merely construct it: do not perform
	 * any initialization that the constructor does not perform.
	 */
	 
	Attribute constructAttribute(String name, Serializable defaultValue)
	throws
		ParameterError;
	

	/** ************************************************************************
	 * Same as constructAttribute(String, Serializable), but allow the constructor
	 * to choose the default value.
	 */
	 
	Attribute constructAttribute(String name);
	

	/** ************************************************************************
	 * Create a new Attribute, owned by this Node, with the specified
	 * name and default value.
	 */
	 
	Attribute createAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;  // if the name is already in use, or if this Node
			// cannot have Attributes.
	
	
	/** ************************************************************************
	 * Create a new Attribute, owned by this Node. A name will be created
	 * for the Attribute and it will be assigned a null value.
	 */
	 
	Attribute createAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;  // if the name is already in use, or if this Node
			// cannot have Attributes.


	/** ************************************************************************
	 * Create a new Attribute, owned by this Node, with the specified
	 * name.The Attribute and it will be assigned a null value.
	 */
	 
	Attribute createAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;  // if the name is already in use, or if this Node
			// cannot have Attributes.
	
	
	/** ************************************************************************
	 * Remove the specified Attribute. Notify all AttributeChangeListeners that
	 * that reference the Attribute that the Attribute has been deleted from its
	 * owning Node.
	 */
	 
	void deleteAttribute(Attribute attribute)
	throws
		ParameterError;  // if this Node does not own the Attribute.
		
		
	void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;  // if this Node does not own the Attribute.
		
		
	/** Return true if this Node belongs to a Domain that is a MotifDef. */
	boolean isOwnedByMotifDef();
	
	
	/** Return true if this Node belongs to an instance of TemplateInstance
		that references a Template. */
	boolean isOwnedByActualTemplateInstance();
	
	
	/** Determine if this ModelComponent or any of its sub-Components actually
		makes reference to any Templates of the specified motif. */
	boolean actuallyUsesMotif(MotifDef md);


	Image getLocalStoredImage(String handle);
	
	
	/**
	 * Should be called by a derived class when an instance is deleted from its
	 * owner and not assigned to a new owner.
	 */
	 
	void deleteFromAllNodesTable();
	
	
	/** The name of this instance. The name must be unique within the containing
	 Node, if any. The following special characters are not allowed:
	 	|  ,  @  >  -  )  (  :  ;  .  /  \  "  '  *  $  <nl>
		*/
		
	String getName();
	
	
	/** HTML-formatted description, containing only the contents of the body of
		the HTML page. I.e., do not include <html>, <head>, or <body> tags. 
		May include expressway: hyperlinks. See the slide "Expressway URLs". */
	void setHTMLDescription(String html);
	
	
	String getHTMLDescription();
	
	
	/** The fully qualified canonical name, including the names of all containing
	 components.*/
	 
	String getFullName();
	
	
	/** Return the name of this Node relative to the specified Node. */
	
	String getNameRelativeTo(PersistentNode parent)
	throws
		ParameterError;  // if this Node is not owned by 'parent' or one of
						// the ancestors of 'parent'.


	/** Accessor. Should not be called unless the behavior of renameChild is not desired. */
	void setName(String name)
	throws
		ParameterError;
	
	
	/** Set the name of this Node and notify ChangeListeners that the name has
		been changed. Return the old name. Subclasses that have ChangeListeners
		should override this. */
		
	String setNameAndNotify(String newName)
	throws
		ParameterError;
	
	
	/** See the slide "The RenameChild Pattern".
		Request that this Node rename the specified direct child Node. Any Maps or Sets
		that sort or index the child using a key that is a function of the child's name
		must be adjusted, including Maps or Sets that are redundant (containing 
		reference aliases) and not owned by this Node. */
		
	void renameChild(PersistentNode child, String newName)
	throws
		ParameterError;  // If 'child' is not a direct child Node, or if 'newName' is invalid.
	
	
	Domain getDomain();
	
	
  /* Ownership */
	
	
	String createUniqueChildName();

	
	String createUniqueChildName(String baseName);
	
	
	/** Return all immediate owned Nodes. The Set returned should be newly constructed
		so that it can be safely modified (added to) by derived classes that extend
		this method. */
	 
	SortedSet<PersistentNode> getChildren();
	
	
	/** Return all children, including children's children, down through the hierarchy.
		The Set returned should be newly constructed
		so that it can be safely modified (added to) by derived classes that extend
		this method. */
	
	SortedSet<PersistentNode> getChildrenRecursive();
	
	
	/** Return all children of the specified type, including children's children,
		down through the hierarchy. The Set returned should be newly constructed
		so that it can be safely modified (added to) by derived classes that extend
		this method. */
	
	SortedSet<PersistentNode> getChildrenRecursive(Class kind);
	
	
	/** Return the immediate child Node that has the specified name. */
	
	PersistentNode getChild(String name)
	throws
		ElementNotFound;


	/** Return the Node that owns this Node. */
	
	PersistentNode getParent();
	
	
	void setParent(PersistentNode parent);
	
	
	/** Return true if this Node is a child or further descendant of the specified
		Node. Note that a Node is not a descendant of itself. The argument must
		not be null. */
		
	boolean isDescendantOf(PersistentNode ancestor);
	
	
	/**
	 * Retrieve the PersistentNode named by 'qualifiedName'. The qualified name
	 * fully specifies the path for the PersistentNode. The qualified name may be
	 * relative to (a child of) this PersistentNode or it may be absolute.
	 * The Domain name may be omitted. Thus, a qualified name is of the form:
		qualified_name	:	[domain_name '.'] node_path ;
		node_path		:	node_path '.' simple_name
						|	;
		simple_name		:	{ a-z, A-Z } { a-z, A-Z, 0-9 }*
	 * If the specified PersistentNode is not found, return null.
	 * The scope of the search includes the current Node.
	 * The qualifiedName may not be null but it may be an empty String.
	 */

	PersistentNode findNode(String qualifiedName)
	throws
		ParameterError;  // if the qualifiedName is mal-formed.


	PersistentNode findNode(String[] pathParts)
	throws
		ParameterError;
		
	
	/** ************************************************************************
	 * Find the first occurrence of a Node with the specified name,
	 * beginning with this Node and traversing downward through all
	 * nested Nodes.
	 */
	 
	PersistentNode findFirstNestedNode(String name)
	throws
		ElementNotFound;
	
	
	/**
	 * Return the specified Node, with a name that is relative to this Node.
	 * The first name part must be the name of an immediate child of this
	 * Node. Return null if no match found.
	 */
	 
	PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
	throws
		ParameterError;
	
	
  /* Other */

		
	/** Implement the Delete Pattern as described in the slide "The Delete Pattern". */
	
	void prepareForDeletion();
	
	
	/** The location, orientation, and scale within this Node's Node Container.
	 If the Node has a border, the border is considered to be part of the Node's
	 display space, and so the origin is the origin of the border. */
	 
	Display getDisplay(); // of the starting point for rendering.
	
	
	/** Set whether this Node is intended to be visible to the user when rendered. */
	
	void setVisible(boolean vis);
	
	
	/** Return the value set by setVisible(boolean). */
	
	boolean isVisible();
	
	
	/** Implement any layout rules that this Node might have. That is, if this Node
		has a LayoutManager, call its layout method. Not recursive. */
		
	void layout();
	
	
	/** Return the sub-Nodes of this Node that have specialized layout rules
		that must be implemented by a customized Layout Manager in a subclass.
		These Nodes will be ignored by the layout algorithm of the default
		Layout Manager. May return null. */
		
	Set<PersistentNode> getNodesWithSpecialLayout();
	
	
	/** Perform layout only if this Node is not user-resizable. */
	
	void conditionalLayout();
	
	
	/** Call layout() on this Node, and all parents upward until there are no
		more parents, until layoutBound is reached (do not layout layoutBound),
		or until a parent does not have a Layout Manager - whichever occurs first.
		If layoutBound is null continue through the outermost parent that has a
		Layout Manager. Returns the outermost Node that has its layout redone. */
		
	PersistentNode layoutUpward(PersistentNode layoutBound)
	throws
		ParameterError;  // if this Node is the layoutBound.

	
	/**
	 * Layout this Node, but only if this Node is not the layoutBound, which is
	 * the Node containing the outermost Node that should be layed out. If layoutBound
	 * is null then there is no outermost bound and layout will proceed up to
	 * the outermost Node that has a Layout Manager.
	 *
	 * If outermostAffectedRef is not null, then return the actual outermost
	 * layed out Node in the first element of the outermostAffectedRef array.
	 * This can be used by the Model Engine to inform clients (through a Notice)
	 * what the outermost affected Node is, so that that Node and its sub-Nodes
	 * can be re-rendered.
	 */
	 
	void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef);


	/** Set whether this Node is predefined: that is, whether it is automatically
	 generated and not defined by the user. */
	 
	void setPredefined(boolean pre);
	
	
	/** Return the value set by setPredefined(boolean). */
	
	boolean isPredefined();
	
	
	/** Set whether this Node can be deleted by the user, or if it is needed for
	 the operation of its parent Node. This is normally set by the XMLParser.
	 The value of the 'deletable' flag is independent of the 'predefined' flag.
	 In general, predefined Nodes are not deletable, regardless of whether their
	 deletable flag is set. */
	
	void setDeletable(boolean del);
	
	
	/** Return the value set by setDeletable. */
	
	boolean isDeletable();
	
	
	/** Set whether this Node can be moved from the position set when it is created. */
	
	void setMovable(boolean m);
	
	
	/** Return the value set by setMovable. */
	
	boolean isMovable();
	
	
	/** Set whether this Node's size or shape can be modified by the user. */
	
	void setResizable(boolean yes);
	
	
	/** Return whether this Node's size or shape can be modified by the user */
	
	boolean isResizable();
	
	
	/** Width of this Node, including its border (if any), when fully expanded. */
	
	double getWidth();


	/** Height of this Node, including its border (if any), when fully expanded. */
	
	double getHeight();
	
	
	void setWidth(double width);


	void setHeight(double height);
	
	
	/** Return true if this Node should be displayed with a visible border when
		iconified. Regardless, an icon should always be displayed with a name title. */
		
	boolean useBorderWhenIconified();
	
	
	void setUseBorderWhenIconified(boolean yesOrNo);
	
	
	Image getIconImage();
	
	
	/** An ImageIcon is obtained from the MotifClassLoader and stored in the server's
		cache of ImageIcons. The image name should be compatible with the namespace
		of resources in the classpath. */
	
	void setIconImage(String path)
	throws
		IOException;
	
	
	ImageIcon getImageIcon();
	
	
	String getIconImageHandle();
	
	
	/** Width of this Node when represented as an icon (not expanded). */
	
	double getIconWidth();
	
	
	/** Height of this Node when represented as an icon (not expanded). */
	
	double getIconHeight();
	
	
	/** If this Node is iconified (not expanded), return true. */
	
	//boolean isIconified();
	
	
	void setLayoutManager(LayoutManager lm);
	
	
	LayoutManager getLayoutManager();
	
	
	/** See the slide "View Types". */
	
	String getDefaultViewTypeName();
	
	void setViewClassName(String name);
	
	String getViewClassName();
	
	
	/**
	 * Return true if this Node contains the specified Point, in this Node's
	 * reference frame.
	 */
	 
	boolean contains(Point p);
	
	
	boolean contains(double x, double y);
	
	
	Face[] getFaces(double x, double y);
	
	
	Face[] getIconFaces(double x, double y);
	
	
	/**
	 * Find the side of this Node that is geometrically closest to the specified
	 * location, as specified in the coordinates of this Node. It is assumed that
	 * the location is contained by this Node.
	 */
	 
	Position findSideClosestToInternalPoint(double x, double y)
	throws
		ParameterError;  // if the location is not inside this Node.
	
	
	/**
	 * Determine the Position (side) that is closest to the specified Point.
	 * The location is specified in the parent's coordinate
	 * system.
	 */
	 
	Position findSideClosestToExternalPoint(Point location);
	
	
	/**
	 * Set the size of this Node so that it is the standard size for the kind
	 * of specialized Node. The size is determined based on the expected scale
	 * ratio between the Node's Domain and pixel size in the most typical View
	 * of the Domain.
	 */
	 
	void setSizeToStandard();
	
	
	/**
	 * Return the height required for the informational header at the top of
	 * this Node.
	 */
	 
	double getHeaderLabelHeight();
	
	
	/**
	 * Return the number of rows of information that must be displayed about this
	 * Node by its Visual. This is used to compute the offset of the Node's sub-Nodes
	 * that is needed to accommodate the header information. Header information
	 * generally includes default values for a Node's Attributes. It does not
	 * include the Node's name, which is rendered in a Visual's border area.
	 * Each subclass should override this method to return the number of rows
	 * of header information that are required.
	 */
	 
	int getNoOfHeaderRows();


	/**
	 * Return the sub-Nodes that are predefined as part of this Node.
	 */
	 
	SortedSet<PersistentNode> getPredefinedNodes();
	
	
	/** Returnt the minimum width allowed for this Node, regardless of how many
		sub-Nodes it contains. */
		
	double getMinimumWidth();
	
	
	/** Returnt the minimum height allowed for this Node, regardless of how many
		sub-Nodes it contains. */
		
	double getMinimumHeight();
	
	
	/**
	 * Return the standard width of this Node, when displayed in a View. This
	 * is a "hint" that is used to scale standard components: actual sizes are
	 * not guranteed to be identical to this value.
	 */
	 
	double getPreferredWidthInPixels();

	/**
	 * Return the standard height of this Node, when displayed in a View. This
	 * is a "hint" that is used to scale standard components: actual sizes are
	 * not guranteed to be identical to this value.
	 */
	 
	double getPreferredHeightInPixels();

	
	int getPreferredHeaderRowHeightInPixels();
	
	
	double getX();
	double getY();
	double getScale();
	double getOrientation();
	void setX(double x);
	void setY(double y);
	void setLocation(double x, double y);
	void setScale(double scale);
	void setOrientation(double angle);
	
	
	boolean allowShift(PersistentNode childNode);
	
	
	/** ************************************************************************
	 * The time in ms when this Node was last updated.
	 */
	 
	long getLastUpdated();
	
	
	/** ************************************************************************
	 * Reports current version number of this Persistent Node. The version
	 * number ncreases monotonically each time the Node is modified.
	 */
	 
	int getVersionNumber();
	
	
	/** Return the ID of the session that modified (or created) this version of
		this Node. */
	String getClientThatLastModified();
	
	
	/** ************************************************************************
	 * This method must be called whenever the state of this instance is modified.
	 * This method is responsible for managing the lifecycle of a Node: its version
	 * number, which client updated it, etc.
	 */
	 
	void update();
	

	Date getDeletionDate();
	

	PersistentNode getPriorVersion();
	
	
	String getPriorVersionNodeId();
	
	
	/** ************************************************************************
	 * The Node ID uniquely identifies the server-side Persistent Node,
	 * within the scope and lifetime of its ModelEngine. Spaces are not allowed.
	 */

	String getNodeId();
	
	
	/** ************************************************************************
	 * Return their hierarchy of Nodes (by Id) ending in this Node.
	 */
	 
	String[] getNodeIdPath();
	
	
	String getNodeIdPathString();
	
	
	/** ************************************************************************
	 * Flag this node to be "watched", such that changes to its state and its
	 * actions are reported to the client. It is up to superclasses to implement
	 * the watch mechanisms in any way that is appropriate.
	 */
	 
	void setWatch(boolean enable);
	
	
	boolean getWatch();


	/** ************************************************************************
	 * Convert this object to a form that is amenable to externalization and
	 * return it. For example, OpenLaszlo RPC will not properly transport Java
	 * Sets, so the externalize() method might convert all Sets to arrays.
	 */

	NodeSer externalize()
	throws
		ParameterError;  // If the Node cannot be externalized.
	
	
	/** ************************************************************************
	 * Populate fields of the NodeSer, which is required to be of the Ser type
	 * corresponding to this Node. This method must be overrided for each 
	 * PersistentNode class.
	 */
	 
	NodeSer externalize(NodeSer nodeSer)
	throws
		ParameterError;
	
	
	/** ************************************************************************
	 * Write this Node's definition in XML format to the specified output.
	 * 'indentation' specifies how many tabs to indent the output lines.
	 */
	 
	void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException;
	
	
	/**
	 * Specialized (derived) classes must implement this method to write the XML
	 * representation of any additional XML attriubtes that this Node
	 * must have. If there are none, then the method should do nothing.
	 */
	 
	void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException;
	
	
	/**
	 * Specialized (derived) classes must implement this method to write the XML
	 * representation of any additional XML elements that this Node
	 * must have. If there are none, then the method should do nothing.
	 */
	 
	void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException;
	
	
	/** ************************************************************************
	 * Return the formats into which this Node may be rendered and sent via
	 * HTTP. Formats include 'svg', 'html', and 'applet'.
	 */
	 
	String[] getAvailableHTTPFormats(String[] descriptions);
	
	
	/** ************************************************************************
	 * Return an HTTP URL that can be sent to Expressway's HTTP server to request
	 * a depiction of this Node in the specified format.
	 */
	 
	String getWebURLString(String format)
	throws
		ParameterError;
	
	
	/** ************************************************************************
	 * Render this Node as SVG. Connections to the Node from outside the Node
	 * are ignored. The width and height (in cm) are used to define the maximum
	 * boundary of the image that is produced: the image will be scaled to fit
	 * within that boundary, but will not be distorted.
	 */
	 
	byte[] renderAsSVG(double wCmMax, double hCmMax)
	throws
		IOException;

	
	/**
	 * Recursively draw this Node and each child Node, recursively through all
	 * child Nodes, but each Node's children external to it, offset to the right
	 * and below, as leaves on a tree. Most implementations rely on recursive
	 * method RenderingHelper.drawChildHierarchy to render the child Nodes.
	 */
	 
	java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
		double pixPerModel);
	
	
	/**
	 * Draw this Node and each child Node, with the child Nodes depicted as icons.
	 * Draw the child Nodes internal to this Node.
	 */
	 
	java.awt.Dimension render(Graphics2D g, double pixPerModel);


	/** ************************************************************************
	 * Draw this Node as a child Node (e.g., inconified) in the specified
	 * Graphics. Use the scale factor 'pixPerModel' as the number of device
	 * pixels per model unit. Do not recursively draw this Node's children.
	 */
	 
	void renderAsChild(Graphics2D g, double pixPerModel);
	
	
	/** ************************************************************************
	 * Generate an HTML table representation of this Node. A column is created
	 * for each field of this Node for which there is a public accessor.
	 * Each child Node is indented relative to its parent.
	 */
	 
	byte[] renderAsHTML()
	throws
		ParameterError,
		IOException;
	

	/** ************************************************************************
	 * Notify this node's client-side peers, using a basic Notice.
	 */
	 
	void notifyAllListeners()
	throws
		Exception;
	
	
	/** ************************************************************************
	 * Notify peers that this Node has been deleted from its owner.
	 */
	 
	void notifyNodeDeleted()
	throws
		Exception;
	
	
	/** ************************************************************************
	 * Notify this node's client-side peers, using the specified Notice..
	 */
	 
	void notifyAllListeners(PeerNotice notice)
	throws
		Exception;
	
	
	/** ************************************************************************
	 * Create an array of Node Ids (Strings) representing the elements of the
	 * specified Collection. If the Collection is a List, preserve the order.
	 */
	 
	String[] createNodeIdArray(Collection<? extends PersistentNode> col);
	
	
	/** ************************************************************************
	 * Create an array of Node Ids (Strings) representing the values of the
	 * specified Map.
	 */
	 
	String[] createNodeIdArray(Map<String, ? extends PersistentNode> map);


	/** ************************************************************************
	 * If the specified PersistentNode is null, return null; otherwise return
	 * the PersistentNode's node Id.
	 */
	 
	String getNodeIdOrNull(PersistentNode node);


	void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value);
	
	
	/** ************************************************************************
	 * Implement the CloneMap pattern. See slide "Clone Map Pattern".
	 * Used to create an independent copy of a Node. The caller must change
	 * the Node's name (and possibly other attributes) to make it unique within
	 * the Model Engine. If this Node is already in the CloneMap, do not create
	 * a new entry: return the existing entry.
	 */
	 
	PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws
		CloneNotSupportedException;
	
	
  /* Diagnostic methods. */

	void dump(int indentation);
	
	
	/** Return the name and Node id of this Node. */
	String identify();
	
	
	void printChildren(int indentationLevel);
	
	
	void printChildren(int indentationLevel, boolean recursive);


	void printChildrenRecursive(int indentationLevel);
}

