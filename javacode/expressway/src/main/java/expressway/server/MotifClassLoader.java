/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import java.net.URLClassLoader;
import java.net.URL;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.File;
import java.util.Vector;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import expressway.common.*;


/**
 * Provide a ClassLoader to which URLs can be added, so that motif JARs can be
 * added to the search path.
 */
 
public class MotifClassLoader extends URLClassLoader
{
	private List<Class> motifClasses = new Vector<Class>();
	private Map<String, byte[]> motifClassBytes = new HashMap<String, byte[]>();
	private Map<String, byte[]> motifResources = new HashMap<String, byte[]>();
	
	
	public MotifClassLoader(ClassLoader currentClassLoader)
	{
		super(new URL[0], currentClassLoader);
	}


	public MotifClassLoader()
	{
		super(new URL[0], Thread.currentThread().getContextClassLoader());
	}

	
	public Class defineMotifClass(byte[] b, int off, int len)
	throws ClassFormatError
	{
		Class c = super.defineClass(null, b, off, len);
		motifClasses.add(c);
		String name = c.getName();
		name = name.replace('.', File.separatorChar) + ".class";
		motifClassBytes.put(name, b);
		return c;
	}
	
	
	public List<Class> getMotifClasses() { return motifClasses; }
	
	
	public void addMotifResource(String name, byte[] data)
	{
		motifResources.put(name, data);
	}
	
	
	public void addMotifResources(Map<String, byte[]> map)
	{
		motifResources.putAll(map);
	}
	
	
	public byte[] getMotifClassBytes(String name)
	throws
		IOException
	{
		if (name.startsWith("/")) name = name.substring(1);
		byte[] bytes = motifClassBytes.get(name);
		return bytes;
	}
	
	
	public byte[] getMotifResource(String name)
	throws
		IOException
	{
		if (name.startsWith("/")) name = name.substring(1);
		
		byte[] bytes = motifResources.get(name);
		if (bytes == null)
		{
			InputStream is = getResourceAsStream(name);
			if (is == null)
			{
				ClassLoader parent = this.getParent();
				is = parent.getResourceAsStream(name);
			}
			
			if (is != null)
			{
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				byte[] buffer = new byte[100];
				for (;;)
				{
					int n = is.read(buffer);
					if (n <= 0) break;
					os.write(buffer, 0, n);
				}
				
				bytes = os.toByteArray();
			}
		}
		
		return bytes;
	}
	
	
	public void removeMotifResource(String name)
	{
		motifResources.remove(name);
	}
	
	
	/** Override to make public. */
	
	public Package getPackage(String name) { return super.getPackage(name); }
	
	
	/** Override to make public. 
	
	public void addURL(URL url)
	{
		super.addURL(url);
	}*/
}

