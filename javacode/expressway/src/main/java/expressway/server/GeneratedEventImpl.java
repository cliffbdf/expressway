/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.OperationMode.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.SortedSet;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Date;
import java.io.*;

	
public class GeneratedEventImpl extends SimulationTimeEventImpl implements GeneratedEvent
{
	/** The Epoch in which the Event was scheduled. Must correspond to an Epoch
		instance in the SimulationRun's epochs list. */
	public Epoch whenScheduled;
	
	
	public boolean compensation = false;
	

	private SortedEventSet<GeneratedEvent> triggeringEvents;
	
	
	public EventSer externalize() throws ModelContainsError
	{
		synchronized (getState().getDomain())
		{
		EventSer eventSer = super.externalize();
		
		// Set all transient externalizable Node fields.
		
		long[] triggeringEventIds = new long[(triggeringEvents == null? 0 : triggeringEvents.size())];
		int i = 0;
		if (triggeringEvents != null)
			for (GeneratedEvent e : triggeringEvents)
				triggeringEventIds[i++] = e.getId();
		
		
		// Set Ser fields.
		
		((GeneratedEventSer)eventSer).whenScheduled = whenScheduled;
		((GeneratedEventSer)eventSer).compensation = this.compensation;
		((GeneratedEventSer)eventSer).triggeringEvents = triggeringEventIds;
		
		return eventSer;
		}
	}
	
	
	public Class getSerClass() { return GeneratedEventSer.class; }
	
		
	//public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	//{
	//	GeneratedEventImpl newInstance = (GeneratedEventImpl)(super.clone(cloneMap, cloneParent));
	//	
	//	newInstance.triggeringEvents = (SortedEventSet)(triggeringEvents.clone());
	//	
	//	return newInstance;
	//}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		whenScheduled = null;
		triggeringEvents = null;
	}
	
	
	GeneratedEventImpl(State state, Date time, Epoch whenScheduled, Serializable newValue,
		SimulationRun simRun)
	throws
		ParameterError
	{
		super(state, time, newValue, simRun);

		this.whenScheduled = whenScheduled;
	}
	
	
	public Epoch getWhenScheduled() { return whenScheduled; }


	public SortedEventSet<GeneratedEvent> getTriggeringEvents() { return triggeringEvents; }
		
	
	public void setTriggeringEvents(SortedEventSet<GeneratedEvent> events) 
	{ this.triggeringEvents = events; }


	public boolean isCompensation() { return compensation; }
	
	
	public void setCompensation(boolean c) { this.compensation = c; }
	
	
	public int getNoOfHeaderRows() { return 2 + super.getNoOfHeaderRows(); }
	
	
	public String toString()
	{
		String trigString = "";
		boolean firstTime = true;
		if (triggeringEvents != null) for (GeneratedEvent trigEvent : triggeringEvents)
		{
			if (firstTime) firstTime = false;
			else trigString += ", ";
			
			trigString += trigEvent.getState().getFullName();
		}
		
		return
			super.toString() +
			(compensation? " (compensation)" : "") +
			(" scheduled at " + (whenScheduled == null? "" : 
				(whenScheduled.getDate() + " (iteration " + whenScheduled.getIteration() + ")"))) +
			(trigString.equals("")? "" : " as a result of Event(s) on " + trigString)
			;
	}


	public static void dump(SortedEventSet<GeneratedEvent> events, int indentation, String title)
	{
		for (int i = 1; i <= indentation; i++) GlobalConsole.print("\t");
		GlobalConsole.println(title);
		for (GeneratedEvent event : events)
		{
			((EventImpl)event).dump(indentation+1);
		}
	}
}

