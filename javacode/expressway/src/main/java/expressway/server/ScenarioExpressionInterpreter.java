/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ExpressionInterpreter.*;
import expressway.ser.*;
import expressway.common.ClientModel.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.tools.lexer.*;
import expressway.tools.parser.*;
import expressway.tools.node.*;
import expressway.tools.analysis.*;
import expressway.generalpurpose.XMLTools;
import expressway.generalpurpose.DateAndTimeUtils;
import java.text.ParseException;
import java.text.DateFormat;
import java.util.*;
import java.io.*;
import org.apache.commons.math.distribution.*;
import org.apache.commons.math.MathException;


/**
 * For computing post-simulation expressions in the context of a ModelScenario's
 * prior Simulation Runs.
 */
	 
public class ScenarioExpressionInterpreter extends ExpressionInterpreter
{
	private ModelScenario scenario;
	private DistributionOwner statisticalContext;
	
	
	ScenarioExpressionInterpreter(InterpreterScope.ClassScope classScope, ModelScenario scenario,
		DistributionOwner statisticalContext)
	{
		super(classScope);
		this.scenario = scenario;
		this.statisticalContext = statisticalContext;
	}
	
	
	/**
	 * 
	 * Predefined functions are:
		PLessThanMeanAtLeast(%)
		PGreaterThanMeanAtLeast(%)
		PFromMeanAtLeast(%, %)
		PFromMeanAtMost(%, %)
	 */
	 
	protected double callBuiltinFunction(String name, List argObjects)
	throws
		ModelContainsError, ElementNotFound
	{
		Set<State> states = scenario.identifyTaggedStates(Types.State.Financial.NetValue.class, true);

		if (states.size() == 0) throw new ModelContainsError(
			"Attribute of type Types.State.Financial.NetValue not found");
		
		if (states.size() > 1) throw new ModelContainsError(
			"More than one Attribute of type Types.State.Financial.NetValue found");
		
		State state = (states.toArray(new State[1]))[0];
		
		if (name.equals("PLessThanMeanByAtLeast"))
		{
			if (argObjects.size() != 1) throw new InterpretationError(
				argObjects.size() + " is the wrong number of arguments for " +
					"PLessThanMeanByAtLeast: requires one.");
			
			if (! ((argObjects.get(0)) instanceof Number)) throw new InterpretationError(
				"Argument 1 for PLessThanMeanByAtLeast must be numeric.");
			
			double arg1 = ((Number)(argObjects.get(0))).doubleValue();
			
			return PLessThanMeanByAtLeast(state, arg1);
		}
		else if (name.equals("PGreaterThanMeanByAtLeast"))
		{
			if (argObjects.size() != 1) throw new InterpretationError(
				argObjects.size() + " is the wrong number of arguments for " +
					"PGreaterThanMeanByAtLeast: requires one.");
			
			if (! ((argObjects.get(0)) instanceof Number)) throw new InterpretationError(
				"Argument 1 for PGreaterThanMeanByAtLeast must be numeric.");
			
			double arg1 = ((Number)(argObjects.get(0))).doubleValue();
			
			return PGreaterThanMeanByAtLeast(state, arg1);
		}
		else if (name.equals("PFromMeanByAtLeast"))
		{
			if (argObjects.size() != 2) throw new InterpretationError(
				argObjects.size() + " is the wrong number of arguments for " +
					"PFromMeanByAtLeast: requires two.");
			
			if (! ((argObjects.get(0)) instanceof Number)) throw new InterpretationError(
				"Argument 1 for PFromMeanByAtLeast must be numeric.");
			
			if (! ((argObjects.get(1)) instanceof Number)) throw new InterpretationError(
				"Argument 2 for PFromMeanByAtLeast must be numeric.");
			
			double arg1 = ((Number)(argObjects.get(0))).doubleValue();
			double arg2 = ((Number)(argObjects.get(1))).doubleValue();
			
			return PFromMeanByAtLeast(state, arg1, arg2);
		}
		else if (name.equals("PFromMeanByAtMost"))
		{
			if (argObjects.size() != 2) throw new InterpretationError(
				argObjects.size() + " is the wrong number of arguments for " +
					"PFromMeanByAtMost: requires two.");
			
			if (! ((argObjects.get(0)) instanceof Number)) throw new InterpretationError(
				"Argument 1 for PFromMeanByAtMost must be numeric.");
			
			if (! ((argObjects.get(1)) instanceof Number)) throw new InterpretationError(
				"Argument 2 for PFromMeanByAtMost must be numeric.");
			
			double arg1 = ((Number)(argObjects.get(0))).doubleValue();
			double arg2 = ((Number)(argObjects.get(1))).doubleValue();
			
			return PFromMeanByAtMost(state, arg1, arg2);
		}
		else
			throw new InterpretationError("Unrecognized function: " + name);
	}
	
	
	/** Compute the probability that the State is less than the State's mean value
		by at least 'pct' percent. */
		
	protected double PLessThanMeanByAtLeast(State state, double pct)
	throws
		ModelContainsError, ElementNotFound
	{
		//if (scenario.getModelScenarioSet() == null) throw new ModelContainsError(
		//	"No Scenario Set for Scenario " + scenario.getFullName());
		
		ContinuousDistribution distribution = 
			statisticalContext.getDistributionOfFinalValueFor(state);
		
		//ContinuousDistribution distribution = 
		//	scenario.getModelScenarioSet().getDistributionOfMeanFor(state);
		
		
		// Use the distribution to compute the probability that the State value
		// is less than the State mean by the specified percent.
		
		double mean = Mean(state);
		try { return distribution.cumulativeProbability(mean - (pct / 100.0) * mean); }
		catch (org.apache.commons.math.MathException ex) { throw new ModelContainsError(ex); }
	}
	
	
	protected double PGreaterThanMeanByAtLeast(State state, double pct)
	throws
		ModelContainsError, ElementNotFound
	{
		ContinuousDistribution distribution = 
			statisticalContext.getDistributionOfFinalValueFor(state);
		
		double mean = Mean(state);
		try { return 1.0 - distribution.cumulativeProbability(mean + (pct / 100.0) * mean); }
		catch (org.apache.commons.math.MathException ex) { throw new ModelContainsError(ex); }
	}
	
	
	/** Compute the probability that the State is less than the State's mean value
		by at least 'pctBelow' percent or above the mean by 'pctAbove' percent. */
		
	protected double PFromMeanByAtLeast(State state, double pctBelow, double pctAbove)
	throws
		ModelContainsError, ElementNotFound
	{
		ContinuousDistribution distribution = 
			statisticalContext.getDistributionOfFinalValueFor(state);
		
		double mean = Mean(state);
		double deltaBelow = (pctBelow/100.0) * mean;
		double deltaAbove = (pctAbove/100.0) * mean;
		try { return 1.0 - distribution.cumulativeProbability(mean - deltaBelow, mean + deltaAbove); }
		catch (org.apache.commons.math.MathException ex) { throw new ModelContainsError(ex); }
	}
	
	
	protected double PFromMeanByAtMost(State state, double pctBelow, double pctAbove)
	throws
		ModelContainsError, ElementNotFound
	{
		ContinuousDistribution distribution = 
			statisticalContext.getDistributionOfFinalValueFor(state);
		
		double mean = Mean(state);
		double deltaBelow = (pctBelow/100.0) * mean;
		double deltaAbove = (pctAbove/100.0) * mean;
		try { return distribution.cumulativeProbability(mean - deltaBelow, mean + deltaAbove); }
		catch (org.apache.commons.math.MathException ex) { throw new ModelContainsError(ex); }
	}
	
	
	/**
	 * 
	 * Predefined identifiers are:
	 	Mean
		Std
	 */
	 
	protected double getPredefinedPathValue(List<String> pathParts)
	throws
		ModelContainsError, ElementNotFound
	{		
		double value;
		
		try
		{
			value = super.getPredefinedPathValue(pathParts);
			return value;
		}
		catch (ElementNotFound enf) {}
			
		if (pathParts.size() != 1) throw new RuntimeException(
			"Illegal name: " + PathUtilities.assemblePath(pathParts));
		
		String name = pathParts.get(0);
		
		Set<State> states = scenario.identifyTaggedStates(Types.State.Financial.NetValue.class, true);
		
		if (states.size() == 0) throw new ModelContainsError(
			"Attribute of type Types.State.Financial.NetValue not found");
		
		if (states.size() > 1) throw new ModelContainsError(
			"More than one Attribute of type Types.State.Financial.NetValue found");
		
		State state = (states.toArray(new State[1]))[0];
		
		
		// Identify the id. It must be either a predefined identfier.
		
		if (name.equalsIgnoreCase("Mean"))
		{
			value = Mean(state);
		}
		else if (name.equalsIgnoreCase("Std"))
		{
			value = Std(state);
		}
		else throw new InterpretationError("Unrecognized: " + name);
		
		return value;
	}
	
	
	/** Compute the mean of the State, using all Simulation Runs for the Scenario. */
		
	protected double Mean(State state)
	throws
		ModelContainsError, ElementNotFound
	{
		return statisticalContext.getMeanFor(state);
	}
	
	
	protected double Std(State state)
	throws
		ModelContainsError, ElementNotFound
	{
		return Math.sqrt(statisticalContext.getVarianceFor(state));
	}
}

