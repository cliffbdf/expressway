/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.generalpurpose.ThrowableUtil;
import java.io.*;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;


public class JarProcessorImpl
	extends XMLProcessorImpl
	implements ModelEngineLocal.JarProcessor
{
	private byte[] byteAr;
	
	private List<String> classNames = new Vector<String>();
	private List<byte[]> classBytes = new Vector<byte[]>();
	private Map<String, byte[]> resources = new HashMap<String, byte[]>();

	private JarInputStream jis;
	
	
	JarProcessorImpl(
		ModelEngineLocal modelEngine,
		boolean replace,  // replace classes, resources, or motifs if already present.
		PeerListener peerListener,
		String clientId,
		byte[] byteAr
		)
	{
		super(modelEngine, replace, peerListener, clientId, null);
		this.byteAr = byteAr;
	}
	
	
	public int processJar(boolean allowXML)
	{
		int result = -1;
		try
		{
			// Read in the JAR file, but do not process its contents yet.
		
			readJar(this.byteAr, new XMLEntryRecognizer(), new ClassEntryRecognizer(),
				new ResourceRecognizer());


			// Perform processing. The contents of the JAR are analyzed and acted up,
			// resulting in changes to the server's classes, resources, and Nodes.
			
			processResources();
			processClasses(allowXML);
			
			if (allowXML)
				result = processXML();  // performs its own error handling.
			else
				result = 0;
		}
		catch (Exception ex)
		{
			try { ServiceContext.getServiceContext().notifyClient(
				new PeerNoticeBase.ImportErrorNotice(
					ThrowableUtil.getAllMessagesAsHTML(ex))); }
			catch (Exception ex2)
			{
				GlobalConsole.printStackTrace(ex2);
			}
		}
		finally
		{
			return result;
		}
	}
	
	
	public int processJar()
	{
		return processJar(true);
	}
	
	
	protected void processResources()
	throws
		Exception
	{
		Set<String> names = getResourceNames();
		
		for (String name : names)
		{
			if (! getReplace())  // then resource name collisions are an error.
			{
				if (getModelEngine().getLocalStoredImageIcon(name) != null)
					throw new Exception(
					"A resource with the name '" + name + "' already exists in the server.");
			}
			
			byte[] bytes = getResource(name);
			
			getModelEngine().createLocalImageIcon(bytes, name);
				// Adds image data resource to Motif Class Loader.
		}
	}
	
	
	protected void processClasses(boolean persistClasses)
	throws
		Exception
	{
		ClassLoader currentClassLoader = Thread.currentThread().getContextClassLoader();
		if (! (currentClassLoader instanceof MotifClassLoader))
			throw new RuntimeException("Class loader is not a MotifClassLoader");
		
		MotifClassLoader motifClassLoader = (MotifClassLoader)currentClassLoader;
		
		int i = 0;
		for (String name : this.classNames)
		{
			if (! getReplace())  // then class name collisions are an error.
			{
				try
				{
					motifClassLoader.loadClass(name);
					throw new IOException(
						"Class " + name + " already present and cannot be replaced");
				}
				catch (ClassNotFoundException ex) {}  // ok
			}
			
			byte[] bytes = getClassBytes(i++);
			Class c = motifClassLoader.defineMotifClass(bytes, 0, bytes.length);
			
			// Add to the List of class byte arrays. These get persisted
			// when the Model Engine is persisted. When the Model Engine is
			// reloaded, these classes are restored by the defineMotifClasses()
			// method.
			getModelEngine().addMotifClassByteArray(bytes);
		}
	}
	
	
	/**
	 * Extend to maintain the MotifDef classNames attribute.
	 */
	 
	public int processXML()
	{
		int result = -1;
		if ((result = super.processXML()) != 0) return result;
		
		
		// Set the JarFile attribute of each MotifDef.
		
		List<PersistentNode> rootNodes = getRootNodesCreated();
		if (rootNodes != null)
		{
			for (PersistentNode node : rootNodes)
			{
				if (node instanceof MotifDef)
				{
					//((MotifDef)node).setJarFile(this.resourceFile);
					((MotifDef)node).setMotifClassNames(
						getClassNames().toArray(new String[this.getClassNames().size()]));
				}
			}
		}
		
		result = 0;
		return result;
	}
	
	
	/**
	 * Read each entry of the Jar file, but do not process the entry. Entries are
	 * read before they are processed because the processing of some entries depends
	 * on other entries, so they must all be read first.
	 */
	 
	protected void readJar(byte[] byteAr, JarEntryRecognizer... recognizers)
	throws
		Exception
	{
		this.jis = new JarInputStream(new ByteArrayInputStream(byteAr));
		
		// Retrieve XML file from the JAR file.
		
		for (;;)
		{
			JarEntry entry = jis.getNextJarEntry();
			if (entry == null) break;
			
			String name = entry.getName();
			
			if (entry.isDirectory())  continue;
			
			boolean recognized = false;
			for (JarEntryRecognizer recognizer : recognizers)
			{
				if (recognized = recognizer.recognizeEntry(entry)) break;
			}
		}
		
		jis.close();
	}


	interface JarEntryRecognizer
	{
		/** Attempt to recognize the type of Java entry. Return false if
			not recognized. If an entry is recognized, prepare this JarProcessor
			to process it. This recognizer itself should not make any database
			changes. The reason is because some entries may require other
			entries to have been read before they can be processed. */
		boolean recognizeEntry(JarEntry entry)
		throws
			Warning,  // if the entry should be processed, but the user should
				// be notified of an issue.
			Exception;  // if there is an error and the entry should not be
				// processed.
	}
	
	
	class XMLEntryRecognizer implements JarEntryRecognizer
	{
		public boolean recognizeEntry(JarEntry entry)
		throws
			Warning,
			Exception
		{
			String name = entry.getName();
			if (! (name.endsWith(".xml") || name.endsWith(".XML"))) return false;
			
			// Read into char array.
			
			int MaxBytesToRead = 500;
			byte[] buffer = new byte[MaxBytesToRead];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			for (;;)  
			{
				if (jis.available() == 0) break;
				int bytesRead = jis.read(buffer, 0, MaxBytesToRead);
				
				if (bytesRead == 0) continue;
				if (bytesRead < 0) break;
				baos.write(buffer, 0, bytesRead);
			}
			
			char[] xmlCharAr = baos.toString().toCharArray();
			baos.close();

			// Pass XML file to the processor
			if (getCharAr() != null) throw new Exception(
				"More than one XML file entry in Jar");
			setCharAr(xmlCharAr);
			
			return true;
		}
	}
	
	
	class ClassEntryRecognizer implements JarEntryRecognizer
	{
		public boolean recognizeEntry(JarEntry entry)
		throws
			Warning,
			Exception
		{
			String name = entry.getName();
			if (! name.endsWith(".class")) return false;

			addClassName(name);

			// Read into byte array.
			
			int MaxBytesToRead = 500;
			byte[] buffer = new byte[MaxBytesToRead];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			for (;;)  
			{
				if (jis.available() == 0) break;
				int bytesRead = jis.read(buffer, 0, MaxBytesToRead);
				
				if (bytesRead == 0) continue;
				if (bytesRead < 0) break;
				baos.write(buffer, 0, bytesRead);
			}
			
			byte[] bytes = baos.toByteArray();
			baos.close();

			addClassBytes(bytes);
			
			return true;
		}
	}
	
	
	class ResourceRecognizer implements JarEntryRecognizer
	{
		public boolean recognizeEntry(JarEntry entry)
		throws
			Warning,
			Exception
		{
			// Read into byte array.
			
			int MaxBytesToRead = 500;
			byte[] buffer = new byte[MaxBytesToRead];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			for (;;)  
			{
				if (jis.available() == 0) break;
				int bytesRead = jis.read(buffer, 0, MaxBytesToRead);
				
				if (bytesRead == 0) continue;
				if (bytesRead < 0) break;
				baos.write(buffer, 0, bytesRead);
			}
			
			byte[] bytes = baos.toByteArray();
			baos.close();
			
			addResource(entry.getName(), bytes);
			
			return true;
		}
	}
	
	
	/*
	 * Accessors.
	 */
	
	protected byte[] getByteAr() { return byteAr; }
	
	protected List<String> getClassNames() { return classNames; }
	
	protected void addClassName(String name) { classNames.add(name); }
	
	protected List<byte[]> getClassBytes() { return classBytes; }
	
	protected void addClassBytes(byte[] bytes) { classBytes.add(bytes); }
	
	protected byte[] getClassBytes(int no) { return classBytes.get(no); }
	
	protected Map<String, byte[]> getResources() { return resources; }
	
	protected void addResource(String name, byte[] bytes) { resources.put(name, bytes); }
	
	protected byte[] getResource(String name) { return resources.get(name); }
	
	protected Set<String> getResourceNames() { return resources.keySet(); }
}

