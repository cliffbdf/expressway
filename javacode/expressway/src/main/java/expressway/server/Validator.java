/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import java.util.Set;
import java.util.List;
import java.util.Date;
import java.util.Vector;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Map;
import java.util.HashMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Serializable;


/**
 * Utility for regression testing, to compare the Event history of a Set of
 * Simulation Runs with the Event history of a List of Simulation Runs previously
 * written to a file.
 */

public class Validator
{
	/**
	 * Verify that the Event history of all Simulation Runs in the specified file.
	 * If they match, return. If they do not
	 * match, throw an Exception that contains the Simulation Run number,
	 * epoch and ModelElement of the first mismatch detected. The States to
	 * validate are determined based on the paths that are present in the event
	 * history archive.
	 */
	
	public static void validate(String clientId, ModelEngineLocal modelEngine, 
		String modelDomainName,
		String modelScenarioName, String filePath)
	throws
		ResultMismatch,  // if the Simulation Run does not match the Event history
						// in the specified File.
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		IOException
	{
		validate(clientId, modelEngine, modelDomainName, modelScenarioName,
			null, filePath);
	}
	
	
	/**
	 * Same as the other validate(...), but with a paths argument. Only the
	 * States in the path are validated.
	 */
	
	public static void validate(String clientId, ModelEngineLocal modelEngine, 
		String modelDomainName,
		String modelScenarioName, List<String> statePaths, String filePath)
	throws
		ResultMismatch,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		IOException
	{
		List<String> simRunNames = null;
		try { simRunNames = modelEngine.getSimulationRuns(clientId, modelDomainName,
			modelScenarioName); }
		catch (IOException ioe) { throw new RuntimeException(ioe); }
		
		File file = new File(filePath);
		if (! file.exists()) throw new ParameterError(file + " not found");
		
		validateSimRuns(clientId, modelEngine, modelDomainName, modelScenarioName, 
			simRunNames, statePaths, file);
	}


	/**
	 * Verify that the Event histories of the specified Simulation Runs match the
	 * Event histories in the specified file. If they match, return. If they do not
	 * match, throw an Exception that contains the epoch and ModelElement of the
	 * first mismatch detected.
	 */

	static void validateSimRuns(String clientId, ModelEngineLocal modelEngine, 
		String modelDomainName,
		String modelScenarioName, List<String> simRunNames, List<String> statePaths, File file)
	throws
		ResultMismatch,  // if the Simulation Run does not match the Event history
						// in the specified File.
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		IOException,
		InternalEngineError
	{
		if (simRunNames == null) throw new ParameterError("Simulation runs is null");
		if ((file == null) || (! file.exists())) throw new ParameterError(
			"Null or non-existent file");
		
		FileReader fr = null;
		try { fr = new FileReader(file); }
		catch (FileNotFoundException fnfe) { throw new ParameterError(fnfe); }
		
		BufferedReader br = new BufferedReader(fr);
		
		try
		{
			ModelDomain domain = modelEngine.getModelDomainPersistentNode(modelDomainName);
			if (domain == null) throw new ElementNotFound(modelDomainName);
			
			ModelScenario scenario = domain.getModelScenario(modelScenarioName);
			
			
			// Identfiy States based on the paths provided.
			
			Set<State> states = null;
			if (statePaths != null)
			{
				new TreeSetNullDisallowed<State>();
				for (String path : statePaths)
				{
					ModelElement elt = domain.findModelElement(path);
					if (! (elt instanceof State)) throw new ParameterError(
						path + " is not a State");
					
					State state = (State)elt;
					states.add(state);
				}
			}
			
			
			//
			// Parse the file and build a List of Simulation Runs from it.
			//
			
			EventArchiver.ArchiveReader archiveReader = EventArchiver.getArchiveReader(br, modelEngine);
			List<SimulationRun> simRuns = archiveReader.readSimRunList();
			int i = 0;
			for (String simRunName : simRunNames)
			{
				SimulationRun newSimRun = scenario.getSimulationRun(simRunName);
				SimulationRun oldSimRun;
				try { oldSimRun = simRuns.get(i); }
				catch (IndexOutOfBoundsException ex) { throw new ResultMismatch(
					"Different number of simulations in the old and new results"); }
				
				validateSimRun(oldSimRun, newSimRun, states);
			}
		}
		finally
		{
			try { br.close(); }
			catch (IOException ioe) { GlobalConsole.println(ioe); }
		}
	}
	
	
	/**
	 * If 'states' is not null, then only compare based on the specified Set of
	 * States.
	 */
	 
	static void validateSimRun(SimulationRun oldSimRun, SimulationRun newSimRun,
		Set<State> states)
	throws
		ResultMismatch
	{
		// Determine the Set of States that are present in the old Events.
		// Any States other than hese will be ignored in the new Events.
		
		Set<State> oldStates = new TreeSetNullDisallowed<State>();
		
		int iteration = 0;
		for (;;)
		{
			iteration++;
			
			SortedEventSet<GeneratedEvent> oldEventSet = null;
			try { oldEventSet = oldSimRun.getGeneratedEventsAtIteration(iteration); }
			catch (ParameterError pe) { throw new ResultMismatch(pe); }
			
			if (oldEventSet.size() == 0) break;
				
			for (Event e : oldEventSet) oldStates.add(e.getState());
		}
		
		
		// Compare each iteration.
		
		Map<State, Serializable> oldStateValues = new HashMap<State, Serializable>();
		Map<State, Serializable> newStateValues = new HashMap<State, Serializable>();
		
		iteration = 0;
		for (;;)
		{
			iteration++;
			
			SortedEventSet<GeneratedEvent> oldEventSet = null;
			SortedEventSet<GeneratedEvent> newEventSet = null;
			
			try { oldEventSet = oldSimRun.getGeneratedEventsAtIteration(iteration); }
			catch (ParameterError pe) {}
			
			try { newEventSet = newSimRun.getGeneratedEventsAtIteration(iteration); }
			catch (ParameterError pe) {}
			
			if ((oldEventSet == null) && (newEventSet != null))
				throw new ResultMismatch(
				"Event Set for new Simulation Run has Events at iteration " + iteration);
			
			if ((oldEventSet != null) && (newEventSet == null))
				throw new ResultMismatch(
				"Event Set for new Simulation Run has no Events at iteration " + iteration);
			
			if ((oldEventSet == null) && (newEventSet == null)) return;  // validated
			
			
			// Determine if each Event in one list has a counterpart in
			// the other. (Note: Allow ordering of Events in each List to differ,
			// since the order is meaningless.)
			
			SortedEventSet<GeneratedEvent> oldEventSetCopy = 
				new SortedEventSetImpl<GeneratedEvent>(oldEventSet);
				
			SortedEventSet<GeneratedEvent> newEventSetCopy = 
				new SortedEventSetImpl<GeneratedEvent>(newEventSet);
			
			
			// Remove Events for States that are not of interest (those whose States
			// are not specified in the 'states' argument).
			
			if (states != null)
			{
				for (GeneratedEvent e : oldEventSet)
					if (e.getState() != null)
						if (! states.contains(e.getState())) oldEventSetCopy.remove(e);
				
				for (GeneratedEvent e : newEventSet)
					if (e.getState() != null)
						if (! states.contains(e.getState())) newEventSetCopy.remove(e);
			}
			

			// Remove new Events whose States are not in the old Events.
			
			for (GeneratedEvent e : newEventSet)
				if (e.getState() != null)
					if (! oldStates.contains(e.getState())) newEventSetCopy.remove(e);
			
			
			// Ignore events that do not change the value of the state. A null
			// value does not change the state if the state has not yet had a
			// non-null valued event.
			
			for (GeneratedEvent e : oldEventSet)
			{
				State state = e.getState();
				Serializable newValue = e.getNewValue();
				
				Serializable curValue = oldStateValues.get(state);
				if (newValue == null)
				{
					if (curValue == null)  // ignore the Event and continue.
						oldEventSetCopy.remove(e);
					
					continue;  // don't ignore the Event.
				}
				
				if (newValue.equals(curValue))
					oldEventSetCopy.remove(e);
				
				oldStateValues.put(state, newValue);
			}
			
			for (GeneratedEvent e : newEventSet)
			{
				State state = e.getState();
				Serializable newValue = e.getNewValue();
				
				Serializable curValue = newStateValues.get(state);
				if (newValue == null)
				{
					if (curValue == null)  // ignore the Event and continue.
						newEventSetCopy.remove(e);
					
					continue;  // don't ignore the Event.
				}
				
				if (newValue.equals(curValue))
					newEventSetCopy.remove(e);
				
				newStateValues.put(state, newValue);
			}
			
			
			// Compare the old and new Sets of Events for this iteration.
			
			SortedEventSet<GeneratedEvent> oldEventSetCopyCopy = 
				new SortedEventSetImpl<GeneratedEvent>(oldEventSetCopy);
				
			SortedEventSet<GeneratedEvent> newEventSetCopyCopy = 
				new SortedEventSetImpl<GeneratedEvent>(newEventSetCopy);
			
			for (GeneratedEvent oldEvent : oldEventSetCopy)  // each Event of the old List.
			{
				// Determine if the old Event exists within the new Set of Events
				// for this iteration.
				
				for (GeneratedEvent e : newEventSetCopy)
				{
					if // e is equivalent to oldEvent
					(
						(e.getState() == (oldEvent.getState()))
						&&
						(ObjectValueComparator.compare(e.getNewValue(), oldEvent.getNewValue()))
					)
					{
						// Remove the Event from both copy Sets.
						
						oldEventSetCopyCopy.remove(oldEvent);
						newEventSetCopyCopy.remove(e);
					}
				}
			}
			
			if (oldEventSetCopyCopy.size() > 0)
			{
				GlobalConsole.println("Events missing from iteration " +
					iteration + " of new Results:");
					
				for (Event e : oldEventSetCopyCopy)
					GlobalConsole.println("\t" + e.toString());
				
				throw new ResultMismatch("Iteration " + iteration +
					" is missing " + oldEventSetCopyCopy.size() +
					" events that are in the old result.");
			}
			
			if (newEventSetCopyCopy.size() > 0)
			{
				GlobalConsole.println("Extraneous events from iteration " +
					iteration + " in new Results:");
					
				for (Event e : newEventSetCopyCopy)
					GlobalConsole.println("\t" + e.toString());
				
				throw new ResultMismatch("Iteration " + iteration +
					" contains " + newEventSetCopyCopy.size() +
					" events that are not in the old result.");
			}
		}
	}
}

