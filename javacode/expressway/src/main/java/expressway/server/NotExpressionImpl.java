/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;
import expressway.common.ClientModel.*;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.Event.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


public class NotExpressionImpl extends ActivityBase implements NotExpression
{
	/** reference only. */
	public Port inputPort = null;
	
	/** reference only. */
	public Port outputPort = null;
	
	/** reference only. */
	public State state = null;  // the output state.
	
	public String getInputPortNodeId() { return getNodeIdOrNull(inputPort); }
	public String getOutputPortNodeId() { return getNodeIdOrNull(outputPort); }
	public String getStateNodeId() { return getNodeIdOrNull(state); }
	

	public Class getSerClass() { return NotExpressionSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((NotExpressionSer)nodeSer).inputPortNodeId = this.getInputPortNodeId();
		((NotExpressionSer)nodeSer).outputPortNodeId = this.getOutputPortNodeId();
		((NotExpressionSer)nodeSer).stateNodeId = this.getStateNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		NotExpressionImpl newInstance = (NotExpressionImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.inputPort = cloneMap.getClonedNode(inputPort);
		newInstance.outputPort = cloneMap.getClonedNode(outputPort);
		newInstance.state = cloneMap.getClonedNode(state);
		
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<not name=\"" + this.getName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
		
		writeXMLAttributesForPredefElements(writer, indentation+1);


		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</not>");
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(inputPort, null);
		deletePort(outputPort, null);
		deleteState(state, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		inputPort = null;
		outputPort = null;
		state = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public NotExpressionImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		init();
	}
	
	
	public NotExpressionImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init();
	}
	
	
	public String getTagName() { return "expression"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.NotExpressionIconImageName);
	}
	
	
	private void init()
	{
		setResizable(false);
		setLayoutManager(DefaultLayoutManager);
		
		try
		{
			inputPort = super.createPort("input_port", PortDirectionType.input, 
				Position.left, this, null);
			outputPort = super.createPort("output_port", PortDirectionType.output, 
				Position.right, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		
		//inputPort.setSide(Position.left);
		//outputPort.setSide(Position.right);


		// Automatically bind the output Port to the internal State.

		try { state = super.createState("OutputState", this, null); }
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}


		try { state.bindPort(outputPort); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		inputPort.setMovable(false);
		outputPort.setMovable(false);
		state.setMovable(false);
		
		state.setVisible(false);
		
		inputPort.setPredefined(true);
		outputPort.setPredefined(true);
		state.setPredefined(true);
	}
	
	
	public int getNoOfHeaderRows() { return 1; }
	
	
	private static final double PreferredWidthInPixels = 60.0;
	private static final double PreferredHeightInPixels = 20.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change Port direction for a standard component");
	}


	public Port getInputPort() { return inputPort; }
	public Port getOutputPort() { return outputPort; }
	
	State getState() { return state; }


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init();
	}
}


class NotExpressionNativeImpl extends ActivityNativeImplBase
{
	boolean stopped = false;
	

	public synchronized void start(ActivityContext context) throws Exception
	{
		if (stopped) throw new RuntimeException("Already stopped");
		super.start(context);
	}


	public synchronized void stop()
	{
		stopped = true;
		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		//if (OperationMode.TestMode)
		//	System.out.println(getExpression().getName() + " responding...");
		
		super.respond(events);
		
		// Check that at least one input port has an event. Include startup
		// events.
		
		boolean hasInputEvents = false;
		for (Port p : getExpression().getPorts())
		{
			if (p == getOutputPort()) continue;  // don't respond to Events on the output.
			if (portHasEvent(events, p))
			{
				hasInputEvents = true;
				break;
			}
		}
		
		// Check if there are either no events (the initialization condition) or
		// there is at least one Event on an input Port.
		
		if (! ((events.size() == 0) || hasInputEvents)) return;


		/*
		 * Evaluate the expression.
		 */
		
		Port inputPort = null;
		try { inputPort = getPort("input_port"); }
		catch (ElementNotFound enf) { throw new ModelContainsError(
			"Unable to find port 'input_port' in Expression"); }
			
		Serializable inVal = getActivityContext().getStateValue(inputPort);
		
		Serializable newValue;
		if (inVal == null)
		{
			newValue = null;
		}
		else if (inVal instanceof Boolean)
		{
			Boolean inputValue = (Boolean)inVal;
			newValue = new Boolean(! (inputValue.booleanValue()));
		}
		else
		{
			SortedEventSet<GeneratedEvent> eventsOnPort = getEventsOnPort(inputPort, events);
			String sourceListString = "";
			for (GeneratedEvent e : eventsOnPort)
			{
				sourceListString = 
					sourceListString + e.getState().getEventProducer().getFullName() +
					", ";
			}
			
			throw new ModelContainsError(
			"Value of input to a Not Expression must be Boolean; found " +
				inVal + "(" + inVal.getClass().getName() + "), originating from " +
				sourceListString);
		}
		
		// Update output value.
		updateOutput(newValue, events);		
	}
	
	
	/**
	 * Generate an output event to match the current driven state of the input
	 * port.
	 */
	
	protected void updateOutput(Serializable newValue, SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		// Drive the output port.
		getActivityContext().setState(getExpression().getState(), newValue, events);
	}
	
	
	protected Port getPort(String name)
	throws ElementNotFound
	{
		ModelElement me = getExpression().getSubcomponent(name);
		if (! (me instanceof Port)) throw new ElementNotFound();
		
		return (Port)me;
	}
	
	
	protected NotExpressionImpl getExpression() { return (NotExpressionImpl)(getActivity()); }
	protected Port getInputPort() { return getExpression().getInputPort(); }
	protected Port getOutputPort() { return getExpression().getOutputPort(); }
}

