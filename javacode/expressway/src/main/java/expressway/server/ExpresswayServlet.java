/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.generalpurpose.ThrowableUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class ExpresswayServlet extends HttpServlet
{
	private ModelEngineLocal modelEngine;
	
	
	public ExpresswayServlet(ModelEngineLocal me) { this.modelEngine = me; }
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException
	{
		String reqStr = request.getPathTranslated();
		String queryStr = request.getQueryString();
		String[] nodeIds = new String[2];
		String[] parts;
		try { parts = URLHelper.getRequestStringParts(reqStr, queryStr, nodeIds); }
		catch (ParameterError pe) { throw new ServletException(pe); }
		
		String viewName = parts[0];
		
		if (nodeIds.length != 1) throw new ServletException(
			"Expected one Node Id argument");
		
		String nodeId = null;
		for (String n : nodeIds) nodeId = n;
		
		if (viewName.equals("list"))
		{
			response.setContentType("text/html");
			
			byte[] bytes;
			try { bytes = getModelEngine().renderHTMLList(nodeId); }
			catch (Exception ex)
			{
				throw new ServletException(ThrowableUtil.getAllMessages(ex));
			}
			
			response.setContentLength(bytes.length);
			response.setStatus(HttpServletResponse.SC_OK);
			
			ServletOutputStream os = response.getOutputStream();
			os.write(bytes);
			os.flush();
			return;
		}
		
		
		String formatName = parts[1];
		String queryString = parts[2];

		String[] queries = (queryString == null? null : queryString.split(";"));

		if (viewName.equals("xml"))
		{
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ServletException("Node with Id " + nodeId +
				" not found");
			
			response.setContentType("text/xml");
			
			XMLWriter writer = new XMLWriter(getModelEngine(), ModelAPITypes.ServletClientId);
			
			ServletOutputStream os = response.getOutputStream();
			PrintWriter pw = new PrintWriter(os);
			
			writer.writeNode(pw, node);
			pw.flush();
			return;
		}
		else if (! viewName.equals("")) throw new ServletException(
			"View type '" + viewName + "' not supported yet.");
		
		if (formatName.equals("applet"))
		{
			response.setContentType("application/x-java-applet;version=" +
				System.getProperty("java.version"));
			
			throw new ServletException("Applet format is not supported yet");
			//response.setStatus(HttpServletResponse.SC_OK);
		}
		else if (formatName.equals("svg"))
		{
			response.setContentType("image/svg+xml");
			
			double w = 0.0;
			double h = 0.0;
			if (queries != null)
			{
				for (String query : queries)
				{
					String[] queryParts = query.split("=");
					if (queryParts.length < 2) throw new ServletException(
						"Invalid query: " + query);
					
					if (queryParts[0].equals("size")) try
					{
						if (queryParts.length < 2) throw new ServletException(
							"Invalid size specification: " + query);
						
						String wh = queryParts[1];
						String[] whParts = wh.split(",");
						if (whParts.length < 2) throw new ServletException(
							"Invalid size specification: " + query);
						w = Double.parseDouble(whParts[0]);
						h = Double.parseDouble(whParts[1]);
					}
					catch (NumberFormatException ex)
					{
						throw new ServletException("Invalid size specification: " + query);
					}
				}
				
			}
			
			byte[] bytes;
			try { bytes = getModelEngine().renderAsSVG(nodeId, w, h); }
			catch (Exception ex)
			{
				throw new ServletException(ThrowableUtil.getAllMessages(ex));
			}
			
			response.setContentLength(bytes.length);
			response.setStatus(HttpServletResponse.SC_OK);
			
			ServletOutputStream os = response.getOutputStream();
			os.write(bytes);
			os.flush();
		}
		else if (formatName.equals("html"))
		{
			response.setContentType("text/html");
			
			byte[] bytes;
			try { bytes = getModelEngine().renderAsHTML(nodeId); }
			catch (Exception ex)
			{
				throw new ServletException(ThrowableUtil.getAllMessages(ex));
			}
			
			response.setContentLength(bytes.length);
			response.setStatus(HttpServletResponse.SC_OK);
			
			ServletOutputStream os = response.getOutputStream();
			os.write(bytes);
			os.flush();
		}
		else
		{
			throw new ServletException("Unrecognized content format: " + formatName);
		}
	}
	
	
	ModelEngineLocal getModelEngine() { return this.modelEngine; }
}

