/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.io.Serializable;


public class DecisionActivityContextImpl implements DecisionActivityContext
{
	DecisionActivityContextImpl()
	{
	}


	public Serializable getBoundAttributeValue(Variable variable)
	throws
		ModelContainsError
	{
		Serializable value = null;

		for (VariableAttributeBinding binding : variable.getAttributeBindings())
		{
			ModelElement.ModelAttribute attribute = null;
			try { attribute = binding.getAttribute(); }
			catch (ValueNotSet w) { throw new ModelContainsError(w); }

			ModelElement.ModelDomain attrModelDomain = attribute.getModelDomain();

			ModelElement.ModelScenario modelScenario = attrModelDomain.getCurrentModelScenario();

			if (modelScenario == null) continue;  // skip this ModelDomain.

			Serializable v = null;
			try
			{
				v = modelScenario.getAttributeValue(attribute);
			}
			catch (ParameterError ex)
			{
				throw new ModelContainsError(ex);
			}

			if (v != null)
			{
				//System.out.println("\t\t\tv=" + v.toString());

				if (value != null) throw new ModelContainsError(
					"There are multiple values for Attribute " + attribute.getName());

				value = v;
			}
		}

		return value;
	}
}
