/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import expressway.server.ModelElement.ModelDomain;
import expressway.ser.*;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.IOException;


public class MotifAttributeImpl extends MotifElementBase implements MotifAttribute
{
	//private Serializable value;
	private Serializable defaultValue;
	private AttributeValidator validator;
	
	
	MotifAttributeImpl(String name, MotifElementBase owner, Serializable defaultValue)
	throws
		ParameterError
	{
		this(name, owner);
		this.defaultValue = defaultValue;
		//this.value = defaultValue;
	}
	

	MotifAttributeImpl(String name, MotifElementBase owner)
	throws
		ParameterError
	{
		super(name, owner);
	}
	

	public void prepareForDeletion()
	{
		super.prepareForDeletion();
	}
	
		
	public String getTagName() { return "attribute"; }
	
	

  /* ***************************************************************************
   * From Hierarchy:
   */

	public Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		try { return new MotifAttributeImpl(name, this); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	
  /* ***************************************************************************
   * From Attribute:
   */

	public Serializable getDefaultValue()
	{
		return defaultValue;
	}
	

	public void setDefaultValue(Serializable defaultValue)
	{
		this.defaultValue = defaultValue;
	}
	

	public void setValue(Scenario scenario, Serializable value)
	throws
		ParameterError
	{
		throw new ParameterError("A Motif has no Scenarios");
	}
	
	
	public Serializable getValue(Scenario scenario)
	throws
		ParameterError
	{
		throw new ParameterError("A Motif has no Scenarios");
	}
	
	
	public void setValidator(AttributeValidator validator)
	{
		this.validator = validator;
	}
	
	
	public AttributeValidator getValidator()
	{
		return this.validator;
	}
	
	
	public void validate(Serializable newValue)
	throws
		ParameterError
	{
		if (validator != null) validator.validate(newValue);
	}


	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{}
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{}
	
	
	public void writeAsXML(PrintWriter writer, int indentation, PersistentNode appliesTo)
	throws
		IOException
	{
		AttributeHelper.writeAsXML(this, writer, indentation, appliesTo);
	}


	/**
	 * Return true if the specified Object is of a type that is allowed in a
	 * MotifAttribute expression.
	 */
	 
	protected boolean isParsableJavaType(Object obj)
	{
		return (obj instanceof String) || (obj instanceof Number) || (obj instanceof Character);
	}
	
	

  /* ***************************************************************************
   * From PersistentNodeImpl:
   */

	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		writeAsXML(writer, indentation, null);
	}


	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.MotifAttributeIconImageName);
	}


	public int getNoOfHeaderRows() { return 1; }
	
	
	public SortedSet<PersistentNode> getPredefinedNodes() { return new TreeSetNullDisallowed<PersistentNode>(); }
	
	
	public double getPreferredWidthInPixels() { return 50; }
	

	public double getPreferredHeightInPixels() { return 50; }
	
	
	//public Set<PersistentNode> getImmediateOwnedNodes() { return new TreeSetNullDisallowed<PersistentNode>(); }
	
	
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		throw new ElementNotFound(name);
	}
}

