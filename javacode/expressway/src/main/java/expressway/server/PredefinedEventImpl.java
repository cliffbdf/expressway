/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.ser.PredefinedEventSer;
import java.util.Date;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.Serializable;


public class PredefinedEventImpl extends ModelElementImpl implements PredefinedEvent
{
	public boolean pulse = false;
	
	
	public static final AttributeValidator TimeValidator = new AttributeValidatorBase()
	{
		public synchronized void validate(Serializable newValue)
		throws
			ParameterError
		{
			if (! (newValue instanceof String)) throw new ParameterError(
				"Time value must be a String");
			
			String timeString = ((String)newValue).trim();
			
			long timeLong = 0;
			try { timeLong = Long.parseLong(timeString); }
			catch (NumberFormatException nfe) { throw new ParameterError(
				timeString + " is not a valid long value"); }
		}
	};
	
	
	/** Ownership. */
	public ModelAttribute timeAttr = null;
	
	
	/** Ownership. */
	public ModelAttribute valueAttr = null;
	
	
	/** Ownership. */
	public String timeExprString = null;
	
	
	/** Ownership. */
	public String valueExprString = null;
	
	
	public transient String timeAttrNodeId = null;
	public transient String valueAttrNodeId = null;


	public PredefinedEventImpl(String name, State state, String timeExprString,
		String valueExprString)
	//public PredefinedEventImpl(State state, Date time, Serializable newValue)
	throws
		ParameterError
	{
		super(name, state, state.getModelDomain());
		//super(state, time, newValue, state);
		
		this.timeExprString = timeExprString;
		this.valueExprString = valueExprString;
		
		if (this.timeExprString == null) this.timeExprString = "0";
		
		init();
		//init(0);
	}
	
	
	public PredefinedEventImpl(String name, State state, String timeExprString,
		String valueExprString, ModelAttribute timeAttr, ModelAttribute valueAttr)
	throws
		ParameterError
	{
		this(name, state, timeExprString, valueExprString);
		
		this.timeAttr = timeAttr;
		this.valueAttr = valueAttr;
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init();
	}
	
	
	public String getTagName() { return "pre_event"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.PredefinedEventIconImageName);
	}
	
	
	public Class getSerClass() { return PredefinedEventSer.class; }


	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		timeAttrNodeId = getNodeIdOrNull(timeAttr);
		valueAttrNodeId = getNodeIdOrNull(valueAttr);

		// Set Ser fields.
		
		((PredefinedEventSer)nodeSer).pulse = pulse;
		((PredefinedEventSer)nodeSer).timeAttrNodeId = this.timeAttrNodeId;
		((PredefinedEventSer)nodeSer).valueAttrNodeId = this.valueAttrNodeId;
		((PredefinedEventSer)nodeSer).timeExprString = this.timeExprString;
		((PredefinedEventSer)nodeSer).valueExprString = this.valueExprString;
		
		return super.externalize(nodeSer);
	}
	

	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		PredefinedEventImpl newEvent = (PredefinedEventImpl)(super.clone(cloneMap, cloneParent));
		
		newEvent.timeAttr = PersistentNodeImpl.cloneNodeOrNull(timeAttr, cloneMap, newEvent);
		newEvent.valueAttr = PersistentNodeImpl.cloneNodeOrNull(valueAttr, cloneMap, newEvent);
		
		return newEvent;
	}
	
		
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deleteAttribute(timeAttr);
		deleteAttribute(valueAttr);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<pre_event name=\"" + this.getName() + "\" "
			+ (timeExprString == null? "" : "time=\"" + timeExprString + "\" ")
			+ "value=\"" + valueExprString + "\" "
			+ (this.isPulse() ? ("pulse=\"true\" ") : "")
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
		
		
		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs)
		{
			if (attr == timeAttr) continue;
			if (attr == valueAttr) continue;
			
			attr.writeAsXML(writer, indentation+1);
		}

		
		writer.println(indentString + "</pre_event>");
	}


	private void init()
	{
		setLayoutManager(DefaultLayoutManager);
		
		
		// Create internal structure of this PredefinedEvent.
		
		try
		{
			if (timeAttr == null)
				timeAttr = createModelAttribute("time", this.timeExprString, this, null);
			
			if (valueAttr == null)
				valueAttr = createModelAttribute("value", this.valueExprString, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}

		timeAttr.setPredefined(true);
		timeAttr.setMovable(false);

		valueAttr.setPredefined(true);
		valueAttr.setMovable(false);
	}




	public boolean isPulse() { return pulse; }
	
	public void setPulse(boolean p) { this.pulse = p; }

	public State getState() { return (State)(this.getParent()); }
	
	public ModelAttribute getTimeAttribute() { return timeAttr; }
	
	public ModelAttribute getValueAttribute() { return valueAttr; }

	public String getDefaultTimeExpression() { return this.timeExprString; }
	
	public void setDefaultTimeExpression(String expr) { this.timeExprString = expr; }
	
	public String getDefaultValueExpression() { return this.valueExprString; }
	
	public void setDefaultValueExpression(String expr) { this.valueExprString = expr; }
	
	
	public int getNoOfHeaderRows() { return 3; }
	
	
	private static final double PreferredWidthInPixels = 120.0;
	private static final double PreferredHeightInPixels = 100.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	

	public ModelAttribute getValueAttr() { return valueAttr; }
	
	
	public String toString()
	{
		return
			super.toString() +
			(pulse ? " pulse" : " flow") +
			", time expr=" + timeExprString + ", value expr=" + valueExprString;
	}
}
