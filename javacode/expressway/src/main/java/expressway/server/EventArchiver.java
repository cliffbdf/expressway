/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.io.*;
import java.net.URLEncoder;


/**
 * For writing events to persistent storage for archive, and reading them back.
 * See the slide "Archive Syntax".
 * See also the SimulationRunImpl methods writeToArchive and readFromArchive.
 * See also ObjectValueReaderWriter.
 */
 
public class EventArchiver
{
	interface ArchiveWriter
	{
		void writeSimRunList(List<SimulationRun> simRuns) throws IOException;
		void writeSimRun(SimulationRun simRun) throws IOException;
		void writeSimRunHeader(SimulationRun simRun) throws IOException;
		void writeSimRunErrors(SimulationRun simRun) throws IOException;
		void writeValueHistory(ValueHistory vh) throws IOException;
		void writeEventHistory(SimulationRun simRun) throws IOException;
		void writeStateMap(ValueHistory vh) throws IOException;
		void writeEvent(Event event) throws IOException;
		void writeEvent(ValueHistory vh, EventSer eventSer) throws IOException;
	}
	
	
	interface ArchiveReader
	{
		List<SimulationRun> readSimRunList() throws IOException;
		SimulationRun readSimRun() throws IOException;
		SimulationRun readSimRunHeader() throws IOException;
		void readSimRunErrors(SimulationRun simRun) throws IOException;
		ValueHistory readValueHistory() throws IOException;
		void readStateMap() throws IOException;
		void readEventHistory(SimulationRun simRun) throws IOException;
	}
	
	
	public static ArchiveWriter getArchiveWriter(final PrintWriter pw)
	{
		return new ArchiveWriter()
		{
			private int lineno = 0;
			
			
			void println(String line)
			{
				pw.println(line);
				this.lineno++;
			}
			
			
			void println()
			{
				pw.println();
				this.lineno++;
			}
			
			
			void print(String s)
			{
				pw.print(s);
			}
			
			
			void throwIOException(String msg) throws IOException
			{
				EventArchiver.throwIOException(this.lineno, msg);
			}
			
			
			void throwIOException(Exception ex) throws IOException
			{
				EventArchiver.throwIOException(this.lineno, ex);
			}
			
			
			public void writeSimRunList(List<SimulationRun> simRuns) throws IOException
			{
				for (SimulationRun simRun : simRuns)
				{
					writeSimRun(simRun);
				}
			}
			
			
			public void writeSimRun(SimulationRun simRun) throws IOException
			{
				println("simulation_run");
				
				writeSimRunHeader(simRun);
				writeSimRunErrors(simRun);
				writeValueHistory(simRun.getExternalStateValues());
				writeEventHistory(simRun);
				
				println("end simulation_run");
			}
			
			
			public void writeSimRunHeader(SimulationRun simRun) throws IOException
			{
				println("Domain/Scenario/Run:" +
					simRun.getModelDomain().getName() + "/" + 
					simRun.getModelScenario().getName() + "/" + simRun.getName());
				println("duration=" + simRun.getDuration());
				println("iterationLimit=" + simRun.getIterationLimit());
				println("startingDate=" + 
					(simRun.getStartingDate() == null ? "unspecified" 
						: simRun.getStartingDate().getTime()));
				println("finalDate=" + 
					(simRun.getFinalDate() == null ? "unspecified" 
						: simRun.getFinalDate().getTime()));
				println("randomSeed=" + simRun.getRandomSeed());
				println("completedNorm=" + simRun.getCompletedNorm());
			}
			
			
			public void writeSimRunErrors(SimulationRun simRun) throws IOException
			{
				println("simulation_errors");
				List<Exception> simTimeErrors = simRun.getErrors();
				for (Exception simError : simTimeErrors)
				{
					print("simulation_error:");
					println(URLEncoder.encode(simError.toString(), "UTF-8"));
				}
				
				println("end simulation_errors");
			}
			
			
			public void writeValueHistory(ValueHistory vh) throws IOException
			{
				println("value_history from " + vh.getStartTime() +
					" to " + vh.getEndTime() + " by " + vh.getDeltaTime());
				
				writeStateMap(vh);
				
				Set<String> stateIds = vh.getStateIds();
				for (String stateId : stateIds)
				{
					print(stateId + " : ");
					EventSer[] eventSers = vh.getEventSersForStateId(stateId);
					boolean firstTime = true;
					for (EventSer eventSer : eventSers)
					{
						if (firstTime) firstTime = false;
						else print(";");
						
						writeEvent(vh, eventSer);
						//writeEvent(vh, eventSer, false /* don't write state */, true);
					}
				}
				
				println("end value_history");
			}
			
			
			public void writeEventHistory(SimulationRun simRun) throws IOException
			{
				println("event_history");
				
				writeStateMap(simRun);
				
				int noOfIterations = simRun.getIterations().size();
				
				for (int iter = 1;; iter++)
				{
					SortedEventSet<GeneratedEvent> events = null;
					try { events = simRun.getGeneratedEventsAtIteration(iter); }
					catch (ParameterError pe) { break; }
					
					if (events.size() == 0)  // startup epoch
					{
						if (iter == 1) continue;  // startup epoch
						if (iter > noOfIterations) break;
						
						throw new RuntimeException(
							"No Generated Events for iteration " + iter + 
							" out of " + noOfIterations + " iterations");
					}
					
					
					long time = events.first().getTimeToOccur().getTime();
					
					print(iter + " ");
					print(time + " ");
					
					boolean firstTimeThrough = true;
					
					for (Event event : events)
					{
						if (firstTimeThrough) firstTimeThrough = false;
						else print(";");  // separator between Events on a line.
						
						if (event instanceof StartupEvent)  // don't write these.
						{
						}
						else if (event instanceof GeneratedEvent)
						{
							writeEvent(event);
							//writeEvent(simRun, event, true, false /* don't write iter/time */);
						}
						//else if (event instanceof NoChangeEvent)  // don't write
						//{
						//}
						else
							throwIOException("Encountered a non-Generated Event: is a "
								+ event.getClass().getName());
					}
					
					println();
				}
				
				println("end event_history");
			}
			
			
			public void writeStateMap(ValueHistory vh) throws IOException
			{
				println("state_map");
				
				Set<String> stateIds = vh.getStateIds();
				for (String id : stateIds)
				{
					PersistentNode node = PersistentNodeImpl.getNode(id);
					if (node == null) throwIOException(
						"Cannot identify State with Id " + id);
					
					if (! (node instanceof State)) throwIOException(
						"Node with Id " + id + " is not a State");
					
					State state = (State)node;
					String stateFullName = state.getFullName();
					print(id + stateFullName);
				}
				
				println("end state_map");
			}
			
			
			/**
			 * Write a State Map archive for all of the States that have Generated
			 * Events in the Simulation Run. Used by writeEventHistory(SimulationRun).
			 */
			 
			protected void writeStateMap(SimulationRun simRun) throws IOException
			{
				println("state_map");
				
				Set<State> states = new TreeSetNullDisallowed<State>();
				
				for (int iter = 1;; iter++)
				{
					SortedEventSet<GeneratedEvent> events = null;
					try { events = simRun.getGeneratedEventsAtIteration(iter); }
					catch (ParameterError pe) { break; }
					
					if (events == null) throw new RuntimeException("events is null");
					if (states == null) throw new RuntimeException("states is null");
					
					for (Event event : events)
					{
						if (event.getState() == null) throw new RuntimeException(
							"event State is null");
						
						states.add(event.getState());
					}
				}
				
				for (State state : states)
				{
					println(state.getNodeId() + " : " + state.getFullName());
				}
				
				println("end state_map");
			}
			
			
			public void writeEvent(Event event) throws IOException
			{
				/* don't write iter/time */
				EventSer eventSer = null;
				try { eventSer = (EventSer)(event.externalize()); }
				catch (ModelContainsError pe) { throwIOException(pe); }
				
				print(encodeAsString(lineno, null, eventSer, true, true, true));
			}
			
			
			public void writeEvent(ValueHistory vh, EventSer eventSer) throws IOException
			{
				/* don't write state */		
				print(encodeAsString(lineno, null, eventSer, true, false, false));
			}
		};
	}
	
	
	public static ArchiveReader getArchiveReader(final BufferedReader br,
		final ModelEngineLocal modelEngine)
	{
		return new ArchiveReader()
		{
			private int lineno = 0;
			private String currentLine = null;
			
			
			String readLine() throws IOException
			{
				this.currentLine = br.readLine();
				this.lineno++;
				return this.currentLine;
			}
			
			
			String getCurrentLineValue()
			{
				if (currentLine == null) return "<null>";
				return this.currentLine;
			}
			
			
			void throwIOException2(String msg) throws IOException
			{
				GlobalConsole.println("Current line is " + getCurrentLineValue());
					
				EventArchiver.throwIOException2(this.lineno, getCurrentLineValue(), msg);
			}
			
			
			void throwIOException2(Exception ex) throws IOException
			{
				GlobalConsole.println("Current line is " + getCurrentLineValue());
					
				EventArchiver.throwIOException2(this.lineno, getCurrentLineValue(), ex);
			}
			
			
			public List<SimulationRun> readSimRunList()
			throws IOException
			{
				List<SimulationRun> simRuns = new Vector<SimulationRun>();
				for (;;)
				{
					SimulationRun simRun;
					try { simRun = readSimRun(); }
					catch (Empty e) { break; }
					
					simRuns.add(simRun);
				}
				
				return simRuns;
			}
			
			
			public SimulationRun readSimRun() throws IOException
			{
				String line;
				for (;;)  // skip empty lines.
				{
					line = readLine();
					if (line == null) throw new Empty();
					if (line.trim().equals("")) continue;
					break;
				}
				
				if (! line.equals("simulation_run")) throwIOException2(
					"Expected 'simulation_run'");
				
				SimulationRun simRun = readSimRunHeader();
				readSimRunErrors(simRun);
				ValueHistory vh = readValueHistory();
				if (simRun.getExternalStateValues() == null)
					((SimulationRunImpl)simRun).setExternalStateValues(vh);
				readEventHistory(simRun);
				
				line = readLine();
				if (! line.equals("end simulation_run")) throwIOException2(
					"Expected 'end simulation_run'");
				
				List<SortedEventSet<SimulationTimeEvent>> eventsByIteration =
					simRun.getEventsByIteration();
				
				if (eventsByIteration == null) throw new RuntimeException(
					"eventsByIteration is null");
				
				System.out.println("For Sim Run " + simRun.getFullName() +
					", loaded " + eventsByIteration.size() + " iterations.");
				// end debug
				
				
				return simRun;
			}
			
			
			public SimulationRun readSimRunHeader() throws IOException
			{
				long durationNew;
				int iterationLimitNew;
				Date startingDateNew;
				Date finalDateNew;
				long randomSeedNew;
				boolean completedNormNew;
				
				
				// Read name
				
				String modelDomainName;
				String scenarioName;
				String simRunName;
				
				String[] domScenRunNameAr = new String[3];

				String line = readLine();
				if (line == null) throwIOException2("input file is empty");
				
				String[] parts = line.split(":");
				if (! parts[0].equals("Domain/Scenario/Run")) throwIOException2(
					"On first line, did not find 'Domain/Scenario/Run'");
				
				String allPartsString = parts[1];
				parts = allPartsString.split("/");
				modelDomainName = parts[0];
				scenarioName = parts[1];
				simRunName = parts[2];
				
				
				ModelDomain domain = null;
				try { domain = modelEngine.getModelDomainPersistentNode(modelDomainName); }
				catch (Exception ex) { throwIOException2("Cannot find domain " +
					modelDomainName); }
				
				ModelScenario scenario = null;
				try { scenario = domain.getModelScenario(scenarioName); }
				catch (Exception ex) { throwIOException2(ex.getMessage()); }

				
				// Read duration.
				
				line = readLine();
				parts = line.split("=");
				if (! (parts[0].equals("duration"))) throwIOException2(
					"Expected 'duration='");
				
				durationNew = Long.parseLong(parts[1]);
				
				
				// Read iterationLimit.
				
				line = readLine();
				parts = line.split("=");
				if (! (parts[0].equals("iterationLimit"))) throwIOException2(
					"Expected 'iterationLimit='");
				
				iterationLimitNew = Integer.parseInt(parts[1]);
				
				
				// Read startingDate.
				
				line = readLine();
				parts = line.split("=");
				if (! (parts[0].equals("startingDate"))) throwIOException2(
					"Expected 'startingDate='");
				
				long startingDateTime = Long.parseLong(parts[1]);
				
				startingDateNew = new Date(startingDateTime);
				
				
				// Read finalDate.
				
				line = readLine();
				parts = line.split("=");
				if (! (parts[0].equals("finalDate"))) throwIOException2(
					"Expected 'finalDate='");
				
				long finalDateTime = Long.parseLong(parts[1]);
				
				finalDateNew = new Date(finalDateTime);
				
						
				// Read randomSeed.
				
				line = readLine();
				parts = line.split("=");
				if (! (parts[0].equals("randomSeed"))) throwIOException2(
					"Expected 'randomSeed='");
				
				randomSeedNew = Long.parseLong(parts[1]);
				
								
				// Read completedNorm.
				
				line = readLine();
				parts = line.split("=");
				if (! (parts[0].equals("completedNorm"))) throwIOException2(
					"Expected 'completedNorm='");
				
				completedNormNew = Boolean.parseBoolean(parts[1]);
				
				
				// Identify or re-create Simulation Run.
				
				SimulationRun simRun = scenario.getSimulationRun(simRunName);
				if (simRun == null) simRun = scenario.createSimulationRun(
					simRunName,
					null,
					scenario, 
					domain, 
					null,  // no callback because we will not be simulating.
					startingDateNew,
					finalDateNew,
					iterationLimitNew,
					durationNew,
					randomSeedNew
					);
				
				return simRun;
			}
			
			
			public void readSimRunErrors(SimulationRun simRun) throws IOException
			{
				boolean addErrors = false;
				if (simRun.getErrors() == null) addErrors = true;
				
				List<Exception> simErrorsNew = new Vector<Exception>();
				
				String line = readLine();
				if (line == null) throwIOException2("Expected 'simulation_errors'");
				
				for (;;)
				{
					line = readLine();
					if (line == null) throwIOException2("Ended early: no Events.");
					
					if (line.equals("end simulation_errors")) break;
					
					if (! (line.startsWith("simulation_error:"))) throwIOException2(
						"Expected 'simulation_error:...'; found " + line);
					
					String errorString = line.substring(17);
					
					SimulationError simError = null;
					try { simError = SimulationError.parseSimulationErrorString(errorString); }
					catch (IllegalArgumentException ex) { throwIOException2(
						"Simulation Error string is invalid: " + errorString); }
					
					simErrorsNew.add(simError);
				}
				
				if (addErrors)
					((SimulationRunImpl)simRun).addSimulationTimeErrors(simErrorsNew);
			}
			
			
			public ValueHistory readValueHistory() throws IOException
			{
				ValueHistoryImpl vh = new ValueHistoryImpl();
				
				String line = readLine();
				
				if (line == null) throwIOException2("Expected 'value_history'");
				
				int pos = line.indexOf("value_history");
				if (pos < 0) throwIOException2("Expected 'value_history'");
				
				String restOfLine = line.substring(pos+13).trim();
				
				pos = restOfLine.indexOf("from");
				if (pos < 0) throwIOException2("Expected 'from'");
				
				restOfLine = restOfLine.substring(pos+4).trim();
				
				
				// Get 'from' value string.
				
				pos = restOfLine.indexOf("to");
				if (pos < 0) throwIOException2("Expected <long> to <long>");
				
				String fromString = restOfLine.substring(0, pos).trim();
				
				restOfLine = restOfLine.substring(pos+2).trim();
				
				
				// Get 'to' value string.
				
				pos = restOfLine.indexOf("by");
				if (pos < 0) throwIOException2("Expected <long> by <long>");
				
				String toString = restOfLine.substring(0, pos).trim();
				
				restOfLine = restOfLine.substring(pos+2).trim();
				
				
				// Get 'by' value string.
				
				String byString = restOfLine.trim();
				
				
				// Parse values.
				
				try { vh.setStartTime(Long.parseLong(fromString)); }
				catch (NumberFormatException ex) { throwIOException2(ex); }
				
				try { vh.setEndTime(Long.parseLong(toString)); }
				catch (NumberFormatException ex) { throwIOException2(ex); }
				
				try { vh.setDeltaTime(Long.parseLong(byString)); }
				catch (NumberFormatException ex) { throwIOException2(ex); }
				
				readStateMap();
					
				for (;;)
				{
					line = readLine();
					if (line == null) throwIOException2("Ended early: no Events.");
					
					if (line.equals("end value_history")) break;
					
					int colonPos = line.indexOf(':');
					
					String stateId = line.substring(0, colonPos).trim();
					restOfLine = line.substring(colonPos+1).trim();
					
					String[] eventStrings = restOfLine.split(";");
					
					EventSer[] eventSers = 
						new EventSer[eventStrings.length];
					
					int i = 0;
					for (String eventString : eventStrings)  // each Event on the line.
					{
						Event event = parseEncodedEvent(lineno, getCurrentLineValue(),
							eventString, false);
						
						try { eventSers[i++] = (EventSer)(event.externalize()); }
						catch (ModelContainsError pe) { throwIOException2(pe); }
					}
					
					vh.addStateHistory(stateId, eventSers);
				}
				
				return vh;
			}
			
			
			public void readStateMap() throws IOException
			{
				String line = readLine();
				if ((line == null) || (! line.equals("state_map"))) throwIOException2(
					"Expected 'state_map'");
				
				for (;;)
				{
					line = readLine();
					if (line == null) throwIOException2("Unexpected end of file");
					if (line.equals("end state_map")) break;
					
					String[] parts = line.split(":");
					if (parts.length != 2) throwIOException2(
						"Ill-formatted state map entry: " + line);
					
					String stateId = parts[0].trim();
					String statePath = parts[1].trim();
				}
				
				// discard data. It was only read to validate it and to read past it.
			}
			
			
			public void readEventHistory(SimulationRun simRun) throws IOException
			{
				boolean addEvents = false;
				if (simRun.getEvents() == null) addEvents = true;
				
				String line = readLine();
				if ((line == null) || (! line.equals("event_history")))
					throwIOException2("Expected 'event_history'");
				
				readStateMap();
				
				for (;;)
				{
					line = readLine();
					if (line == null) throwIOException2("Unexpected end of input");
					if (line.equals("end event_history")) break;
					
					int firstSpacePos = line.indexOf(' ');
					if (firstSpacePos < 0) throwIOException2("Expected <int>");
					
					String iterString = line.substring(0, firstSpacePos);
					
					String restOfLine = line.substring(firstSpacePos+1).trim();
					int secondSpacePos = restOfLine.indexOf(' ');
					if (secondSpacePos < 0) throwIOException2("Expected <long>");
					
					String timeString = restOfLine.substring(0, secondSpacePos);
					String eventsString = restOfLine.substring(secondSpacePos+1).trim();
					
					int iter = 0;
					try { iter = Integer.parseInt(iterString); }
					catch (NumberFormatException ex) { throwIOException2(
						"Ill-formatted iteration value: '" + iterString + "'"); }
					
					long time = 0;
					try { time = Long.parseLong(timeString); }
					catch (NumberFormatException ex) { throwIOException2(
						"Ill-formatted time value: '" + timeString + "'"); }
					
					String[] eventStrings = eventsString.split(";");
					for (String eventString : eventStrings)
					{
						Event event = parseEncodedEvent(lineno, getCurrentLineValue(),
							eventString, addEvents);
						
						if (! (event instanceof SimulationTimeEvent)) throwIOException2(
							"Event '" + event.toString() + "' is not a Simulation Time Event");
						
						SimulationTimeEvent simEvent = (SimulationTimeEvent)event;
						
						if (simEvent.getTimeToOccur().getTime() != time)
							throwIOException2("Event time does not equal iteration time");
						
						if (simEvent instanceof GeneratedEvent)
							if (((GeneratedEvent)simEvent).getEpoch().getIteration() != iter)
								throwIOException2("Event iteration does not equal iteration");
					}
				}
			}
		};
	}
	
	
	public static String encodeAsString(int lineno, String currentLine, EventSer eventSer, 
		boolean writeIterAndTime, boolean writeState, boolean writeWhenSch)
	throws
		IOException
	{
		try
		{
			String result = "";
			
			if (eventSer instanceof SimulationTimeEventSer)
				result += ((SimulationTimeEventSer)eventSer).simRunNodeId + "|";
	
			result += eventSer.getEventClassName() + ",";
			
			if (writeState)
				result += eventSer.stateNodeId;
			
			if (writeIterAndTime)
			{
				if (eventSer instanceof GeneratedEventSer)
				{
					GeneratedEventSer genEventSer = (GeneratedEventSer)eventSer;
					result += "@" 
							+ (genEventSer.epoch == null? 
								"null" : genEventSer.epoch.getDate().getTime() + " (" +
									genEventSer.epoch.getIteration() + ")"
								);
				}
				else
					result += "@" + eventSer.timeToOccur.getTime();
			}
			
			result += "->";
			
			Serializable value = eventSer.newValue;
			if (value == null)
			{
				result += "Null:";
			}
			else if (value instanceof Integer)
			{
				result += "Integer:";
			}
			else if (value instanceof Float)
			{
				result += "Float:";
			}
			else if (value instanceof Double)
			{
				result += "Double:";
			}
			else if (value instanceof Boolean)
			{
				result += "Boolean:";
			}
			else throwIOException2(lineno, currentLine,
				"Cannot write Events that have value of type " +
					value.getClass().getName());
				
			result += value;
			
			if (writeWhenSch)
			{
				if (eventSer instanceof GeneratedEventSer)
				{
					Epoch whenScheduled = ((GeneratedEventSer)eventSer).whenScheduled;
					if (whenScheduled != null)
					{
						result += " (from " + whenScheduled.getIteration() + ")";
					}
				}
			}
			
			return result;
		}
		catch (RuntimeException re) { throwIOException2(lineno, currentLine, re); }
		throw new RuntimeException("Should not get here");
	}
	
	
	public static Event parseEncodedEvent(int lineno, String currentLine, 
		String eventString, boolean addEvents)
	throws
		IOException
	{
		try
		{
			Event event = null;
			
			// Obtain owner Node Id spec, if any.
			String ownerId = null;
			String[] parts = eventString.split("\\|");
			if (parts.length == 1) throwIOException2(lineno, currentLine, "No Sim Run Id: " + eventString);
			else if (parts.length == 2) ownerId = parts[0]; // an owner specification
			else { throwIOException2(lineno, currentLine, "Multiple occurrences of '|'"); }
			
			PersistentNode node = PersistentNodeImpl.getNode(ownerId);
			if (node == null) throwIOException2(lineno, currentLine, 
				"Simulation Run with Id " + ownerId + " not found");
			if (! (node instanceof SimulationRun)) throwIOException2(lineno, currentLine, 
				"Owner Id '" + ownerId + "' is not a SimulationRun");
			SimulationRunImpl simRun = (SimulationRunImpl)node;
			
			parts = parts[1].split(",");
			String eventClassName = parts[0];
			
			Class eventClass = null;
			try { eventClass = Class.forName(eventClassName); }
			catch (ClassNotFoundException cnfe) {
				throwIOException2(lineno, currentLine, "Could not load class '" + eventClassName + "';", cnfe); }
			
			parts = parts[1].split("@");
			String stateId = parts[0];
			node = PersistentNodeImpl.getNode(stateId);
			if (node == null) throwIOException2(lineno, currentLine, 
				"Node with Id " + stateId + " not found, in input '" + eventString + "'.");
			if (! (node instanceof State)) throwIOException2(lineno, currentLine, 
				"Node with Id " + stateId + " does not represent a State");
			State state = (State)node;
			
			parts = parts[1].split("->");
			String epochString = parts[0].trim();  // "null" or <long> (<int>)
			if (epochString.equalsIgnoreCase("null")) throwIOException2(lineno, currentLine, 
				"Unexpected null epoch");
			
			String[] epochParts = epochString.split("\\(");
			String timeString = epochParts[0].trim();
			String iterationString = null;
			try { iterationString = epochParts[1].substring(0, epochParts[1].length()-1); }
			catch (Exception ex) { throwIOException2(lineno, currentLine, 
				"Error in epoch specification: " + epochString, ex); }
			
			long time = 0;
			try { time = Long.parseLong(timeString); }
			catch (NumberFormatException nfe) { throwIOException2(lineno, currentLine, 
				"in '" + epochString + "', parsing time: '" + timeString + "'", nfe); }
			
			int iteration = 0;
			try { iteration = Integer.parseInt(iterationString); }
			catch (NumberFormatException nfe) { throwIOException2(lineno, currentLine, 
				"in '" + epochString + "', after retrieving time value " + time +
				" ms, parsing iteration '" + iterationString + "'", nfe); }
			
			Date date = new Date(time);		
			Epoch currentEpoch = new EpochImpl(time, iteration);
				//simRun.getOrAppendEpochForIteration(iteration, date.getTime());
				
			parts = parts[1].split(":");
			
			String valueTypeDesc = parts[0].trim();
			
			String valueAndRemainder = parts[1].trim();
			
			//Date scheduledDate = null;
			Epoch scheduledEpoch = null;
			
			String valueString;
			
			parts = valueAndRemainder.split("\\(");
			
			if (parts.length > 1)  // there is a 'from' clause.
			{
				if (parts.length > 2) throwIOException2(lineno, currentLine, 
					"Ill-formed 'from' section: " + valueAndRemainder);
				
				valueString = parts[0].trim();
				
				parts = parts[1].split("from");
				
				if (parts.length != 2) 
				{
					//System.out.println("parts:");
					//for (String p : parts) System.out.println("\t'" + p + "'");
					throwIOException2(lineno, currentLine, 
					"Ill-formed 'from' section: " + valueAndRemainder);
				}
				
				parts = parts[1].split("\\)");
				if (parts.length != 1) 
				{
					//System.out.println("parts:");
					//for (String p : parts) System.out.println("\t'" + p + "'");
					throwIOException2(lineno, currentLine, 
					"Ill-formed 'from' section: " + valueAndRemainder);
				}
				
				//System.out.println("Parsing int from " + parts[0]);
				int fromIter = Integer.parseInt(parts[0].trim());
				//scheduledDate = new Date(fromTime);
				try { scheduledEpoch = simRun.getEpochForIteration(fromIter); }
				catch (ParameterError pe) { throwIOException2(lineno, currentLine, pe); }
			}
			else
				valueString = parts[0].trim();
			
			
			Serializable value = null;
			if (valueTypeDesc.equals("Null"))
				value = null;
			else if (valueTypeDesc.equals("Integer"))
				value = Integer.parseInt(valueString);
			else if (valueTypeDesc.equals("Float"))
				value = Float.parseFloat(valueString);
			else if (valueTypeDesc.equals("Double"))
				value = Double.parseDouble(valueString);
			else if (valueTypeDesc.equals("Boolean"))
				value = Boolean.parseBoolean(valueString);
			else
				throwIOException2(lineno, currentLine, 
					"Unrecognized value type: " + valueTypeDesc);
			
			if (eventClass.equals(GeneratedEventImpl.class))
			{
				try { event = new GeneratedEventImpl(state, date, scheduledEpoch, value, simRun); }
				catch (ParameterError pe) { throwIOException2(lineno, currentLine, pe); }
	
				((GeneratedEventImpl)event).setEpoch(currentEpoch);
				
				// Insert the Event into the set of Events for the Sim Run.
				if (addEvents) simRun.reAddEvent((GeneratedEventImpl)event);
			}
			else if (eventClass.equals(EventConflict.class))
			{
				if (ownerId == null) throwIOException2(lineno, currentLine, 
					"An EventConflict without an owner Id specification");
				
				PersistentNode owner = PersistentNodeImpl.getNode(ownerId);
				if (owner == null) throwIOException2(lineno, currentLine, 
					"Owner Id " + ownerId + " not found");
				
				try { event = new EventConflict(state, date, value, simRun); }
				catch (ParameterError pe) { throwIOException2(lineno, currentLine, pe); }
	
				((GeneratedEventImpl)event).setEpoch(currentEpoch);
				
				// Insert the Event into the set of Events for the Sim Run.
				if (addEvents) simRun.reAddEvent((GeneratedEventImpl)event);
			}
			else if (eventClass.equals(EventImpl.class))
			{
				try { event = new GeneratedEventImpl(state, date, null, value, simRun); }
				catch (ParameterError pe) { throwIOException2(lineno, currentLine, pe); }
			}
			else throwIOException2(lineno, currentLine, 
				"Unrecognized class name: " + eventClassName);
			
			return event;
		}
		catch (RuntimeException re) { throwIOException2(lineno, currentLine, re); }
		throw new RuntimeException("Should not get here");
	}
	
	
	static void throwIOException(int lineno, String msg)
	throws
		IOException
	{
		throw new IOException("At line " + lineno + ", " + msg);
	}
	
	
	static void throwIOException(int lineno, Exception ex)
	throws
		IOException
	{
		throw new IOException("At line " + lineno, ex);
	}
	
	
	static void throwIOException(int lineno, String msg, Exception ex)
	throws
		IOException
	{
		throw new IOException("At line " + lineno + ", " + msg, ex);
	}
	
	
	static void throwIOException2(int lineno, String currentLine, String msg)
	throws
		IOException
	{
		GlobalConsole.println("Current line is " + getCurrentLineValue(currentLine));
					
		throw new IOException("At line " + lineno + ", " + msg);
	}
	
	
	static void throwIOException2(int lineno, String currentLine, Exception ex)
	throws
		IOException
	{
		GlobalConsole.println("Current line is " + getCurrentLineValue(currentLine));
					
		throw new IOException("At line " + lineno, ex);
	}
	
	
	static void throwIOException2(int lineno, String currentLine, String msg, Exception ex)
	throws
		IOException
	{
		GlobalConsole.println("Current line is " + getCurrentLineValue(currentLine));
					
		throw new IOException("At line " + lineno + ", " + msg, ex);
	}
	
	
	static String getCurrentLineValue(String curLine)
	{
		if (curLine == null) return "<null>";
		return curLine;
	}
}

