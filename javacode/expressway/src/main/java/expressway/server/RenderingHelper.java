/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.ser.*;
import expressway.server.HierarchyElement.*;
import expressway.generalpurpose.SVGUtils;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Color;
import java.awt.GraphicsConfiguration;
import org.apache.batik.svggen.SVGGraphics2D;


public class RenderingHelper
{
	public static byte[] renderHierarchyAsSVG(PersistentNode node, double wCmMax, double hCmMax)
	throws
		IOException
	{
		SVGGraphics2D g = SVGUtils.getSVGGraphics();
		double pixPerModel = computePixPerModel(g, node, wCmMax, hCmMax);

		// Draw background.
		
		g.setColor(Color.white);
		GraphicsConfiguration gc = g.getDeviceConfiguration();
		Rectangle bounds = gc.getBounds();
		g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);

		// Draw a hierarchy.
		
		int x = 100;
		int y = 100;
		node.renderHierarchyAsLeaves(g, x, y, pixPerModel);
		
		// Write to SVG stream.
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Writer writer = new OutputStreamWriter(baos);
		g.stream(writer);
		writer.flush();
		return baos.toByteArray();
	}
	
	
	public static double computePixPerModel(Graphics2D g, PersistentNode node,
		double wCmMax, double hCmMax)
	{
		// Determine a scale factor that will map the Node to the Graphics area,
		// and that will also fit within the specified max width and height.
		
		GraphicsConfiguration gc = g.getDeviceConfiguration();
		Rectangle bounds = gc.getBounds();
		int x = bounds.x;
		int y = bounds.y;
		
		double wM = node.getWidth();
		double hM = node.getHeight();
		
		double xCmPerModel = wCmMax / wM;
		double yCmPerModel = hCmMax / hM;
		double cmPerModel = Math.min(xCmPerModel, yCmPerModel);
		
		double xPixPerModel = bounds.width / wM;
		double yPixPerModel = bounds.height / hM;
		
		double pixPerModel = Math.max(xPixPerModel, yPixPerModel);
		
		double wCmAct = bounds.width / (pixPerModel / cmPerModel);
		double fx = wCmMax / wCmMax;
		double fy = hCmMax / hCmMax;
		double f = Math.max(fx, fy);
		if (f > 1.0) pixPerModel = pixPerModel / f;
		
		return pixPerModel;
	}
	
	
	public static java.awt.Dimension drawChildHierarchy(PersistentNode node, 
		int elementHeight, Graphics2D g, int x, int y, double pixPerModel)
	{
		final int InterElementYSpacing = 8;
		final int xIndentation = 20;
		final int yIndentation = elementHeight + InterElementYSpacing;
		Set<PersistentNode> children = node.getChildren();
		int xSpace = xIndentation;
		int ySpace = yIndentation;
		for (PersistentNode child : children)
		{
			int cX = x + xIndentation;
			int cY = y + yIndentation;
			Dimension d = child.renderHierarchyAsLeaves(g, cX, cY, pixPerModel);
			
			xSpace += d.width;
			ySpace += d.height;
		}
		
		return new Dimension(xSpace, ySpace);
	}
}

