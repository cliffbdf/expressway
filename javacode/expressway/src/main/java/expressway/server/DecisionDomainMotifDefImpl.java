/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.server.MotifElement.*;
import expressway.ser.*;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.io.File;
import java.io.IOException;

	
public class DecisionDomainMotifDefImpl extends DecisionDomainImpl
	implements DecisionDomainMotifDef
{
	/** ownwership. */
	private String[] classNames = null;


	public DecisionDomainMotifDefImpl(String name)
	throws
		ParameterError
	{
		super(name);
	}


	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.MotifDefIconImageName);
	}
	
	
	public Class getSerClass() { return DecisionDomainMotifDefSer.class; }
	
	
	public String getTagName() { return "decision_motif"; }


	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		DecisionDomainMotifDefImpl newInstance =
			(DecisionDomainMotifDefImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.classNames = new String[classNames.length];
		int i = 0;
		for (String className : classNames) newInstance.classNames[i++] = className;
		
		return newInstance;
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		this.classNames = null;
	}
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		// (There are none.)
		
		// Set Ser fields.
		ModelDomainMotifDefSer ser = (ModelDomainMotifDefSer)nodeSer;
		ser.motifClassNames = this.classNames;
		MenuOwnerHelper.copySerValues(this, (ModelDomainMotifDefSer)nodeSer);
		
		return super.externalize(nodeSer);
	}
	
	
  /* From MotifDef */
	
	
	public Set<Template> getTemplates()
	{
		Set<Template> templates = new HashSet<Template>();
		Set<PersistentNode> elts = super.getChildren();
		for (PersistentNode elt : elts)
		{
			if (elt instanceof Template)
				templates.add((Template)elt);
			else throw new RuntimeException(
				"MotifDef contains a DecisionElement that is not a Template");
		}
		
		return templates;
	}
	
	
	public Template getTemplate(String name)
	throws
		ElementNotFound
	{
		Set<Template> templates = getTemplates();
		for (Template t : templates)
		{
			if (t.getName().equals(name)) return t;
		}
		
		throw new ElementNotFound("Template named '" + name + "'");
	}
		
	
	public Set<Domain> getReferencingDomains()
	{
		List<DecisionDomain> mds;
		try { mds = getModelEngine().getDecisionDomainPersistentNodes(); }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		Set<Domain> refDomains = new TreeSetNullDisallowed<Domain>();
		for (DecisionDomain d : mds)
		{
			if (d.usesMotif(this)) refDomains.add(d);
		}
		
		return refDomains;
	}
	
	
	public void setMotifClassNames(String[] classNames) {this.classNames = classNames; }

	
  /* Implementation */
	

	public String[] getMotifClassNames() { return this.classNames; }
}

