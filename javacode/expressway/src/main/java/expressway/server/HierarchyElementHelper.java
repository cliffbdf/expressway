/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.ser.*;
import expressway.server.HierarchyElement.*;
import java.util.Set;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.awt.Dimension;
import java.awt.Graphics2D;


public class HierarchyElementHelper
{
	public static HierarchyAttribute createHierarchyAttribute(HierarchyElement elt,
		HierarchyAttribute definingAttr,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		HierarchyAttribute newAttr =
			(HierarchyAttribute)(elt.createAttribute(layoutBound, outermostAffectedRef));
		newAttr.setDefinedBy(definingAttr);
		
		Set<PersistentNode> children = elt.getChildren();
		for (PersistentNode child : children)
		{
			if (child instanceof Attribute) continue;
			if (! (child instanceof HierarchyElement)) continue;
			HierarchyElement he = (HierarchyElement)child;
			he.createHierarchyAttribute(definingAttr, layoutBound, outermostAffectedRef);
		}
		
		return newAttr;
	}
}

