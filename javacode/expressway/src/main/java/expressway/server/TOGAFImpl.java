/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.awt.Graphics2D;


public class TOGAFImpl implements TOGAF
{
	private PersistentNode rootNode;
	private Graphics2D graphics;
	
	
	public TOGAFImpl(PersistentNode node, Graphics2D g)
	{
		this.rootNode = node;
		this.graphics = g;
	}
	
	
	public void renderOrgActorCatalog()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderDriverGoalObjCatalog()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderRoleCatalog()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderServiceFuncCatalog()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderProcessEventControlProductCatalog()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderContractMeasureCatalog()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderBusinessInteractionMatrix()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderActorRoleMatrix()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderBusinessFootprintDiagram()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderServiceInformationDiagram()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderFuncDecompDiagram()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderProductLifecycleDiagram()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderGoalObjServiceDiagram()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderBusinessUseCaseDiagram()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderOrgDecompDiagram()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderProcessFlowDiagram()
	{
		throw new RuntimeException("Not implemented yet");
	}
	

	public void renderEventDiagram()
	{
		throw new RuntimeException("Not implemented yet");
	}
}

