/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import java.io.IOException;


public class ConstantValueImpl extends ParameterImpl implements ConstantValue
{
	public ConstantValueImpl(String name, DecisionPoint dp)
	throws
		ParameterError
	{
		super(name, dp);
		setResizable(false);
	}


	public Class getSerClass() { return ConstantValueSer.class; }
	
	
	public String getTagName() { return "constant"; }


	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ConstantValueImpl newInstance = (ConstantValueImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}


	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ConstantValueIconImageName);
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		setResizable(false);
	}
}
