/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;

import expressway.ser.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.geometry.*;
import java.util.Collection;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import java.util.List;
import java.util.Vector;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


public class PortImpl extends ModelComponentImpl implements Port
{
	private static final PortDirectionType DefaultDirection = PortDirectionType.bi;
	
	private boolean isBlack = false;
	
	PortDirectionType direction = DefaultDirection;
	Position currentSide = Position.top;

	Set<Conduit> sourceConnections = new TreeSetNullDisallowed<Conduit>();
	Set<Conduit> destinationConnections = new TreeSetNullDisallowed<Conduit>();
	
	/** Location of this Port when its owner is iconified. */
	PointImpl pointInIcon = new PointImpl(0.0, 0.0);
	
	public String[] getSourceConnectionNodeIds() { return createNodeIdArray(sourceConnections); }
	public String[] getDestinationConnectionNodeIds() { return createNodeIdArray(destinationConnections); }
	

	public Class getSerClass() { return PortSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((PortSer)nodeSer).xInIcon = getXInIcon();
		((PortSer)nodeSer).yInIcon = getYInIcon();
		((PortSer)nodeSer).parentNodeId = this.getParent().getNodeId();
		((PortSer)nodeSer).isBlack = this.getBlack();
		((PortSer)nodeSer).direction = this.getDirection();
		((PortSer)nodeSer).currentSide = this.getSide();
		((PortSer)nodeSer).sourceConnectionNodeIds = this.getSourceConnectionNodeIds();
		((PortSer)nodeSer).destinationConnectionNodeIds = this.getDestinationConnectionNodeIds();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		PortImpl newInstance = (PortImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.sourceConnections = new TreeSetNullDisallowed<Conduit>();
		for (Conduit c : sourceConnections)
		{
			newInstance.sourceConnections.add((Conduit)(cloneMap.getClonedNode(c)));
		}
		
		newInstance.destinationConnections = new TreeSetNullDisallowed<Conduit>();
		for (Conduit c : destinationConnections)
		{
			newInstance.destinationConnections.add((Conduit)(cloneMap.getClonedNode(c)));
		}
		
		newInstance.pointInIcon = (PointImpl)(pointInIcon.deepCopy());
		
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<port name=\"" + this.getName() + "\" "
			+ (this.getBlack() ? ("black=\"" + this.getBlack() + "\" ") : "")
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "direction=\"" + direction + "\" "
			+ "side=\"" + currentSide + "\">");
		
		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</port>");
	}
	

	public PortImpl(String name, PortedContainer parent, ModelDomain domain,
		PortDirectionType direction)
	{
		super(name, parent, domain);
		init(direction);
	}


	public PortImpl(String name, PortedContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		init(DefaultDirection);
	}
	
	
	public PortImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init(DefaultDirection);
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init(DefaultDirection);
	}
	

	public String getTagName() { return "port"; }
	
	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		Set<Conduit> sourcesCopy = new TreeSetNullDisallowed<Conduit>(sourceConnections);
		for (Conduit sc : sourcesCopy) sc.portDeleted(this);
		
		Set<Conduit> destCopy = new TreeSetNullDisallowed<Conduit>(destinationConnections);
		for (Conduit dc : destCopy) dc.portDeleted(this);
	
		State boundState = this.getBoundState();
		boundState.portDeleted(this);
		
		// Nullify all references.
	}
	
	
	public String setNameAndNotify(String newName)
	throws
		ParameterError
	{
		String oldName = super.setNameAndNotify(newName);
		
		Set<Conduit> sourcesCopy = new TreeSetNullDisallowed<Conduit>(sourceConnections);
		for (Conduit sc : sourcesCopy) sc.portNameChanged(this, oldName);
		
		Set<Conduit> destCopy = new TreeSetNullDisallowed<Conduit>(destinationConnections);
		for (Conduit dc : destCopy) dc.portNameChanged(this, oldName);
	
		State boundState = this.getBoundState();
		if (boundState != null) boundState.portNameChanged(this, oldName);
		
		return oldName;
	}
	

	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.PortIconImageName);
	}
	
	
	private void init(PortDirectionType direction)
	{
		setResizable(false);
		this.direction = direction;
		this.setWidth(0.0);
		this.setHeight(0.0);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 0.0;
	private static final double PreferredHeightInPixels = 0.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public PortedContainer getContainer() { return (PortedContainer)(getParent()); }


	public State getBoundState()
	{
		PortedContainer container = this.getContainer();
		List<State> states = container.getStates();
		for (State state : states)
		{
			Set<Port> stateBoundPorts = state.getPortBindings();
			if (stateBoundPorts.contains(this)) return state;
		}
		
		return null;
	}


	public PortDirectionType getDirection() { return direction; }


	public void setDirection(PortDirectionType dir)
	{
		this.direction = dir;
	}
	
	
	public void setBlack(boolean black) { this.isBlack = black; }
	
	
	public boolean getBlack() { return isBlack; }
	
	
	public void setSide(Position pos)
	{
		setSide_NoReroute(pos);
		
		
		// Reroute any connected Conduits.
		
		try
		{
			for (Conduit c : sourceConnections) c.reroute();
			for (Conduit c : destinationConnections) c.reroute();
		}
		catch (Exception ex) { throw new RuntimeException(
			"Should not happen: error while re-routing a Conduit after merely " +
			"changing a Port's side.",ex);
		}
	}
	
	
	void setSide_NoReroute(Position pos)
	{
		ModelContainer container = getContainer();
		double containerWidth = container.getWidth();  // may be 0
		double containerHeight = container.getHeight();  // may be 0
		
		if ((containerWidth == 0.0) || (containerHeight == 0.0))
		{
			this.currentSide = pos;
			return;
		}
		
		double containerIconWidth = container.getIconWidth();
		double containerIconHeight = container.getIconHeight();
		
		Position originalSide = this.currentSide;
		this.currentSide = pos;
				
		Display display = this.getDisplay();
		List<Port> portsOnSide = getContainer().getPorts(pos);
		double spacing;
		double iconSpacing;
		double px, py;
		double pxIcon, pyIcon;
		
		switch (pos)
		{
			case top:
				setY(containerHeight - this.getHeight());
				setYInIcon(containerIconHeight - this.getHeight());
				
				// Arrange all the Ports that are along this side so that they
				// are equidistance from each other and from the corners.
				
				spacing = containerWidth/portsOnSide.size();
				iconSpacing = containerIconWidth/portsOnSide.size();
				
				px = spacing/2.0;
				pxIcon = iconSpacing/2.0;
				for (Port p : portsOnSide)
				{
					p.setX(px);
					p.setXInIcon(pxIcon);
					px += spacing;
					px += iconSpacing;
				}

				break;
			case bottom:
				setY(0.0);
				setYInIcon(0.0);

				spacing = containerWidth/portsOnSide.size();
				iconSpacing = containerIconWidth/portsOnSide.size();
				
				px = spacing/2.0;
				pxIcon = iconSpacing/2.0;
				for (Port p : portsOnSide)
				{
					p.setX(px);
					p.setXInIcon(pxIcon);
					px += spacing;
					pxIcon += iconSpacing;
				}

				break;
			case left:
				setX(0.0);
				setXInIcon(0.0);

				spacing = containerHeight/portsOnSide.size();
				iconSpacing = containerIconHeight/portsOnSide.size();
				
				py = spacing/2.0;
				pyIcon = iconSpacing/2.0;
				
				for (Port p : portsOnSide)
				{
					p.setY(py);
					p.setYInIcon(pyIcon);
					py += spacing;
					pyIcon += iconSpacing;
				}

				break;
			case right:
				setX(containerWidth - this.getWidth());
				setXInIcon(containerIconWidth - this.getWidth());

				spacing = containerHeight/portsOnSide.size();
				iconSpacing = containerIconHeight/portsOnSide.size();
				
				py = spacing/2.0;
				pyIcon = iconSpacing/2.0;
				
				for (Port p : portsOnSide)
				{
					p.setY(py);
					p.setYInIcon(pyIcon);
					py += spacing;
					pyIcon += iconSpacing;
				}

				break;			

			default: throw new RuntimeException("Unexpected Position value.");
		}
	}
	
		
	public Position getSide() { return this.currentSide; }
	
	
	public Point getLocationInIcon() { return pointInIcon; }
	

	public double getXInIcon() { return pointInIcon.getX(); }
	

	public double getYInIcon() { return pointInIcon.getY(); }
	
	
	public void setXInIcon(double x) { pointInIcon.setX(x); }
	
	
	public void setYInIcon(double y)
	{
		pointInIcon.setY(y);
	}
	
	
	public void setX(double x)
	{
		if (x > this.getContainer().getWidth()) throw new RuntimeException(
			"Attempt to set a Port's location to outside of its Container");
		
		if (x < 0.0) throw new RuntimeException(
			"Attempt to set a Port to a negative location");
		
		super.setX(x);
	}
	
	
	public void setY(double y)
	{
		if (y > this.getContainer().getHeight()) return;
		
		if (y < 0.0) return;
		
		super.setY(y);
	}
	
	
	public boolean allowsInputEvents()
	{
		return (direction == PortDirectionType.input) || (direction == PortDirectionType.bi);
	}
	

	public boolean allowsOutputEvents()
	{
		return (direction == PortDirectionType.output) || (direction == PortDirectionType.bi);
	}
	

	public void addSourceConnection(Conduit c)
	{
		sourceConnections.add(c);
	}


	public void addDestinationConnection(Conduit c)
	{
		destinationConnections.add(c);
	}
	
	
	public void removeSourceConnection(Conduit c)
	{
		sourceConnections.remove(c);
	}


	public void removeDestinationConnection(Conduit c)
	{
		destinationConnections.remove(c);
	}


	public Set<Conduit> getSourceConnections() { return sourceConnections; }


	public Set<Conduit> getDestinationConnections() { return destinationConnections; }
	
	
	public Set<Conduit> getExternalSourceConnections()
	{
		Set<Conduit> conduits = new TreeSetNullDisallowed<Conduit>();
		for (Conduit conduit : sourceConnections)
		{
			if (conduit.getModelContainer() != this.getContainer())
				conduits.add(conduit);
		}
		
		return conduits;
	}
	
	
	public Set<Conduit> getExternalDestinationConnections()
	{
		Set<Conduit> conduits = new TreeSetNullDisallowed<Conduit>();
		for (Conduit conduit : destinationConnections)
		{
			if (conduit.getModelContainer() != this.getContainer())
				conduits.add(conduit);
		}
		
		return conduits;
	}
	
	
	public Set<Conduit> getExternalConnections()
	{
		Set<Conduit> sourceConduits = getExternalSourceConnections();
		Set<Conduit> destConduits = getExternalDestinationConnections();
		Set<Conduit> conduits = new TreeSetNullDisallowed<Conduit>(sourceConduits);
		conduits.addAll(destConduits);
		return conduits;
	}

	
	public Rectangle getRectangle(boolean inIcon)
	{
		if (inIcon)
			return new Rectangle(
				new PointImpl(this.getXInIcon() - this.getWidth()/2.0, 
					this.getYInIcon() - this.getHeight()/2.0), this.getWidth(), this.getHeight());
		else
			return new Rectangle(
				new PointImpl(this.getX() - this.getWidth()/2.0, 
					this.getY() - this.getHeight()/2.0), this.getWidth(), this.getHeight());
	}
	
	
	public Point getCenterPoint()
	{
		return new PointImpl(this.getDisplay().getX()  //+ this.width/2.0
			, this.getDisplay().getY()
			//+ this.height/2.0
			);
	}
	
	
	public ModelContainer chooseContainerForConduit(Port target)
	throws
		ParameterError
	{
		ModelContainer container = null;

		final String PortConnectionErrorMessage = 
			"Port " + this.getFullName() + " may not be connected to " + target.getFullName();

		PortedContainer p1Container = this.getContainer();
		PortedContainer p2Container = target.getContainer();
		
		if (p1Container.getParent() == null) throw new RuntimeException(
			"Unexpected: '" + p1Container.getFullName() + "' is not within a Container.");
		
		if (p2Container.getParent() == null) throw new RuntimeException(
			"Unexpected: '" + p2Container.getFullName() + "' is not within a Container.");
		
		
		if (p1Container == p2Container) return p1Container.getModelContainer();
		else
			// containers not the same
			
			if (p1Container.contains(p2Container)) return p1Container;
			else
				// p1Container does NOT immediately contain p2Container
				
				if (p2Container.contains(p1Container)) return p2Container;
				else
					// p2Container does NOT immediately contain p1Container
					
					if (p1Container.getParent() == p2Container.getParent())

						return p1Container.getModelContainer();
					else
						throw new ParameterError(PortConnectionErrorMessage);
	}


	/**
	 * Determine the side of its Container that this Port occupies, based on its
	 * geometric location.
	 */
	 
	protected Position getApparentSide()
	{
		double x = getDisplay().getX();
		double y = getDisplay().getY();
		
		double containerWidth = getContainer().getWidth();
		double containerHeight = getContainer().getHeight();
		
		// Allow a 5% tolerance.
		double toleranceX = getContainer().getWidth() * 0.05;
		double toleranceY = getContainer().getHeight() * 0.05;
		
		// See if y is along the bottom face, and x is between the two bottom corners.
		
		if (LineSegment.isBetween(y, - toleranceY, toleranceY))
		{
			if (LineSegment.isBetween(x, 0.0, containerWidth))
				return Position.bottom;
			else
				return null;
		}

		// See if x is along the right face, and y is between the two right corners.
		
		if (LineSegment.isBetween(x, containerWidth - this.getWidth() - toleranceX,
			containerWidth - this.getWidth() + toleranceX))
		{
			if (LineSegment.isBetween(y, 0.0, containerHeight))
				return Position.right;
			else
				return null;
		}
		
		// See if y is along the top face, and x is between the two top corners.
		
		if (LineSegment.isBetween(y, containerHeight - this.getHeight() - toleranceY, 
			containerHeight - this.getHeight() + toleranceY))
		{
			if (LineSegment.isBetween(x, 0.0, containerWidth))
				return Position.top;
			else
				return null;
		}

		// See if x is along the left face, and y is between the two left corners.
		
		if (LineSegment.isBetween(x, - toleranceX, toleranceX))
		{
			if (LineSegment.isBetween(y, 0.0, containerHeight))
				return Position.left;
			else
				return null;
		}
		
		return null;
	}
	
	
	/**
	 * Return true if the specified Ports face each other: that is, if a line could
	 * be drawn from one Port to the other without passing through or along an edge
	 * of either Port's parent Component, and the faces containing each Port are
	 * either both horizontal or both vertical. The Ports must either be in the
	 * same Container, or the the Ports must each be in Components that are in
	 * the same Container - i.e., it must be possible to connect the two Ports
	 * by a straight or curved line without crossing a Component boundary.
	 */
	 
	static boolean portsFaceEachOther(Port p1, Port p2)
	{
		Position p1Side = p1.getSide();
		Position p2Side = p2.getSide();
		
		if (p1Side == p2Side) return false;  // same side: can't be opposing.
		
		
		ModelContainer p1Container = p1.getContainer();
		ModelContainer p2Container = p2.getContainer();
		
		
		if (p1Side == Position.top && p2Side == Position.bottom)
		{
			double p1TopY = p1Container.getDisplay().getY() + p1Container.getHeight();
			double p2BotY = p2Container.getDisplay().getY();
			
			if // p1 top face is lower than p2 bottom face
				(p1TopY < p2BotY)
				return true;
				
			return false;
		}
		
		if (p1Side == Position.bottom && p2Side == Position.top)
		{
			double p2TopY = p2Container.getDisplay().getY() + p2Container.getHeight();
			double p1BotY = p1Container.getDisplay().getY();
			
			if // p2 top face is lower than p1 bottom face
				(p2TopY < p1BotY)
				return true;
				
			return false;
		}
		
		if (p1Side == Position.right && p2Side == Position.left)
		{
			double p1RightX = p1Container.getDisplay().getX() + p1Container.getWidth();
			double p2LeftX = p2Container.getDisplay().getX();
			
			if // p1 right face is left of p2 left face
				(p1RightX < p2LeftX)
				return true;
				
			return false;
		}
		
		if (p1Side == Position.left && p2Side == Position.right)
		{
			double p2RightX = p2Container.getDisplay().getX() + p2Container.getWidth();
			double p1LeftX = p1Container.getDisplay().getX();
			
			if // p2 right face is left of p1 left face
				(p2RightX < p1LeftX)
				return true;
				
			return false;
		}
		
		return false;
	}
	
	
	/*public String toString()
	{
		return "[ Port: " + getFullName() + ": at " + getDisplay().getX() +
			", " + getDisplay().getY() + " ]";
	}*/
	
	
	/**
	 * Check if there is room for this Port on the side on which it is positioned.
	 * The criteria for overlap is that two Ports may not be closer than the
	 * typical Port diameter. If this Port is not arranged on a side of its
	 * Container, throw a ParameterError.
	 *
	 
	protected void checkOverlap()
	throws
		ParameterError  // if there is not sufficient room, or if this Port is
			// not on a side of its Container.
	{
		double portDiameter = 
			this.getModelDomain().getVisualGuidance().getProbablePortDiameter();
		
		
		// Check if there is room for the Ports on the Component's face.
		
		Position side = this.getSide();
		if (side == null) throw new ParameterError(
			"Port is not on a side of its Container");
		
		PortedContainer container = this.getContainer();
		double spaceAvailable = 0.0;
		switch (side)
		{
			case Top:
			case Bottom: spaceAvailable = container.getWidth();
				break;
				
			case Left:
			case Right: spaceAvailable = container.getHeight();
				break;

			default: throw new RuntimeException("Unexpected Position value.");
		}
		
		
		// Check if there is room for this Port.
		
		double spaceNeeded = 0.0;
		Set<Port> ports = this.getContainer().getPorts();
		List<Port> portsOnSameSide = new Vector<Port>();
		int posOfThisPort = 0;
		int pNo = 0;
		for (Port p : ports)
		{
			if (p == this) posOfThisPort = pNo;
			
			Position pSide = p.getSide();
			if (pSide == pos)  // skip other Ports that are not on a side.
			{
				portsOnSameSide.add(p);
				if (pSide == side) spaceNeeded += portDiameter;
				
				if (spaceNeeded > spaceAvailable) throw new ParameterError(
					"Not enough room for Port '" + this.getFullName() + "'");
			}
			
			pNo++;
		}
	}*/
	
	
	/**
	 * Compute the Set of States that are bound to this Port.
	 *
	 
	Set<State> getBoundStates()
	{
		PortedContainer container = this.getContainer();
		Set<State> boundStates = new TreeSetNullDisallowed<State>();
		Set<State> states = container.getStates();
		for (State state : states)
		{
			Set<Port> stateBoundPorts = state.getPortBindings();
			if (stateBoundPorts.contains(this)) boundStates.add(state);
		}
		
		return boundStates;
	}*/
}
