/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import java.util.*;
import expressway.common.GlobalConsole;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement;
import expressway.server.ModelElement.*;



/**
 * Base class for layout of Nodes.
 */
 
public abstract class LayoutManager implements java.io.Serializable, Cloneable
{
	public LayoutManager() {}
	
	
	public void layout(PersistentNode node)
	{
		layout(node, false);
	}
	
	
	public void layout(PersistentNode node, boolean recursive)
	{
		layout(node, recursive, node.getNodesWithSpecialLayout());
	}


	public void layout(PersistentNode node, boolean recursive, 
		Set<PersistentNode> nodesToIgnore)
	{
		layout(node, recursive, 
			(nodesToIgnore == null? null : 
				nodesToIgnore.toArray(new PersistentNode[nodesToIgnore.size()])));
	}
	
	
	/**
	 * Arrange the subordinate Nodes of 'node'; and resize 'node' so that it can
	 * accommodate its subordinate Nodes. Ignore Node in 'nodesToIgnore'.
	 * If 'recursive' is true, do this for each sub-Node as well.
	 */
	 
	public abstract void layout(PersistentNode node, boolean recursive, 
		PersistentNode[] nodesToIgnore);
	
	
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
}

