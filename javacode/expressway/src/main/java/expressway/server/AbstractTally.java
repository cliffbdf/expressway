/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public abstract class AbstractTally extends ActivityBase implements Tally
{
	public static final String stateName = "TallyState";
	public static final Number DefaultInitValue = new Double(0.0);
	public static final boolean DefaultDoubleCount = false;
	
	
	/** reference only. */
	public Port[] inputPorts = null;
	
	/** reference only. */
	public Port outputPort = null;

	/** reference only. */
	public State state = null;

	/** Ownership. */
	public Number defaultInitValue = null;
	
	/** reference only. */
	public ModelAttribute initValueAttr = null;
	
	public boolean defaultDoubleCount;
	
	/** reference only. */
	public ModelAttribute doubleCountAttr = null;
	
	
	public String[] getInputPortNodeIds()
	{
		String[] inputPortNodeIds;
		int noOfInputPorts = inputPorts.length;
		inputPortNodeIds = new String[noOfInputPorts];
		for (int i = 0; i < noOfInputPorts; i++)
			inputPortNodeIds[i] = getNodeIdOrNull(inputPorts[i]);
		return inputPortNodeIds;
	}
	
	public String getOutputPortNodeId() { return getNodeIdOrNull(outputPort); }
	public String getStateNodeId() { return getNodeIdOrNull(state); }
	public String getInitValueAttrNodeId() { return getNodeIdOrNull(initValueAttr); }
	public String getDoubleCountAttrNodeId() { return getNodeIdOrNull(doubleCountAttr); }

	
	public Class getSerClass() { return AbstractTallySer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((AbstractTallySer)nodeSer).inputPortNodeIds = this.getInputPortNodeIds();
		((AbstractTallySer)nodeSer).outputPortNodeId = this.getOutputPortNodeId();
		((AbstractTallySer)nodeSer).stateNodeId = this.getStateNodeId();
		((AbstractTallySer)nodeSer).defaultInitValue = this.getDefaultInitValue();
		((AbstractTallySer)nodeSer).initValueAttrNodeId = this.getInitValueAttrNodeId();
		((AbstractTallySer)nodeSer).defaultDoubleCount = this.getDefaultDoubleCount();
		((AbstractTallySer)nodeSer).doubleCountAttrNodeId = this.getDoubleCountAttrNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		AbstractTally newInstance = (AbstractTally)(super.clone(cloneMap, cloneParent));
		
		newInstance.inputPorts = PersistentNodeImpl.cloneNodeArray(inputPorts, cloneMap, newInstance);
		newInstance.outputPort = cloneMap.getClonedNode(outputPort);
		newInstance.state = cloneMap.getClonedNode(state);
		newInstance.initValueAttr = cloneMap.getClonedNode(initValueAttr);
		newInstance.doubleCountAttr = cloneMap.getClonedNode(doubleCountAttr);
		
		return newInstance;
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		for (Port p : inputPorts) deletePort(p, null);
		deletePort(outputPort, null);
		deleteState(state, null);
		deleteAttribute(initValueAttr, null);
		deleteAttribute(doubleCountAttr, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		inputPorts = null;
		outputPort = null;
		state = null;
		initValueAttr = null;
		doubleCountAttr = null;
		defaultInitValue = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public AbstractTally(String name, ModelContainer parent, ModelDomain domain,
		Number initValue, String[] inputPortNames, boolean doubleCount)
	{
		super(name, parent, domain);
		
		init(initValue, inputPortNames, doubleCount);
	}


	public AbstractTally(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init(DefaultInitValue, new String[0], DefaultDoubleCount);
	}
	
	
	public String getTagName() { return "tally"; }
	
	
	private void init(Number initValue, String[] inputPortNames, boolean doubleCount)
	{
		setResizable(false);
		setUseBorderWhenIconified(true);
		setLayoutManager(DefaultLayoutManager);
		
		this.defaultInitValue = initValue;
		this.defaultDoubleCount = doubleCount;

		try
		{
			this.initValueAttr = createModelAttribute("init_value", initValue, this, null);
			this.doubleCountAttr = createModelAttribute("double_count", 
				new Boolean(doubleCount), this, null);
			outputPort = super.createPort("output_port", PortDirectionType.output, 
				Position.right, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}

		
		// Automatically bind the output Port to the internal State.

		try { state = super.createState("TallyState", this, null); }
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		try { state.bindPort(outputPort); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		
		// Create each input Port.
		
		if (inputPortNames == null) throw new RuntimeException("Null input ports");
		
		Set<Port> portSet = new TreeSetNullDisallowed<Port>();
		for (String portName : inputPortNames)
		{
			if (portName == null) throw new RuntimeException("Null input port name");
			if (portName.equals("")) throw new RuntimeException("Empty input port name");
			
			Port inputPort;
			try { portSet.add(inputPort = createPort(portName, PortDirectionType.input, 
				Position.left, this, null)); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
			
			//inputPort.setSide(Position.left);
			
			inputPort.setMovable(false);
		}
		
		inputPorts = portSet.toArray(new Port[portSet.size()]);
		
		outputPort.setMovable(false);
		state.setMovable(false);
		initValueAttr.setMovable(false);
		doubleCountAttr.setMovable(false);
		
		outputPort.setPredefined(true);
		state.setPredefined(true);
		initValueAttr.setPredefined(true);
		doubleCountAttr.setPredefined(true);
	}
	
	
	public int getNoOfHeaderRows() { return 2; }
	
	
	private static final double PreferredWidthInPixels = 60.0;
	private static final double PreferredHeightInPixels = 30.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	
	
	public double getPreferredHeightInPixels()
	{
		return Math.max(PreferredHeightInPixels, inputPorts.length * 20 + 10);
	}
	
	
	public State getTallyState() { return state; }

	public Port getOutputPort() { return outputPort; }

	public Number getDefaultInitValue() { return defaultInitValue; }
	
	public ModelAttribute getInitValueAttr() { return initValueAttr; }
	
	public boolean getDefaultDoubleCount() { return defaultDoubleCount; }
	
	public ModelAttribute getDoubleCountAttr() { return doubleCountAttr; }
	
	
	/** Override createPort so that all Ports created are input. */
	public Port createPort(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createNewInputPort(name, layoutBound, outermostAffectedRef);
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (child == outputPort) throw new ParameterError(
			"Cannot delete the output port of a Tally.");
		
		super.deleteChild(child, outermostAffectedRef);
	}
	
	
	public void deletePort(Port port, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError  // if the Port is not a Port for this PortedContainer.
	{
		deleteChild(port, outermostAffectedRef);
	}
	

	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change the direction of a Tally Port");
	}


	/** ************************************************************************
	 * Create a new input Port for this Tally.
	 */
	 
	Port createNewInputPort(String portName, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError  // if a Port with the specified name already exists.
	{
		double portDiam = getModelDomain().getVisualGuidance().getProbablePortDiameter();
		this.setHeight(this.getHeight() + (portDiam * 2.0));
		
		Port newPort = createPort(portName, PortDirectionType.input, Position.left,
			layoutBound, outermostAffectedRef);
		
		Port[] newInputPorts = new Port[inputPorts.length+1];
		for (int i = 0; i < inputPorts.length; i++) newInputPorts[i] = inputPorts[i];
		newInputPorts[inputPorts.length] = newPort;
		inputPorts = newInputPorts;
		newPort.setMovable(false);

		
		// Reroute and reposition all Conduits connected to this Tally, both
		// internally and externally.
		
		for (Port p : inputPorts) p.setSide(Position.left);
		outputPort.setSide(Position.right);
		
		return newPort;
	}
	

	/** ************************************************************************
	 * Remove and destroy the specified Port in this Tally.
	 * If the Port is referenced by any Conduits, it must be disconnected from
	 * them.
	 */
	 
	void deleteInputPort(Port port, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError  // if the Port is not an input Port for this Tally.
	{
		// Verify that the Port is an input Port.
		
		boolean found = false;
		for (int i = 0; i < inputPorts.length; i++) if (inputPorts[i] == port)
		{
			found = true; break;
		}
			
		if (! found) throw new ParameterError(
			"Port " + port.getName() + " is not an input Port of Tally " + this.getFullName());
		
		
		super.deletePort(port, outermostAffectedRef);  // defined in PortedContainer.
	}
}


class AbstractTallyNativeImpl extends ActivityNativeImplBase
{
	private State state = null;

	boolean stopped = false;
	double initValue;
	boolean doubleCount;
	

	public synchronized void start(ActivityContext context) throws Exception
	{
		if (stopped) throw new RuntimeException("Already stopped");
		super.start(context);

		state = context.getState(AbstractTally.stateName);
		this.initValue = getAttributeDoubleValue(getTally().getInitValueAttr());
		this.doubleCount = getAttributeBooleanValue(getTally().getDoubleCountAttr());

		context.getSimulationRun().setInitialStateValue(state, initValue);
	}


	public synchronized void stop()
	{
		//System.out.println("eeee stopping " + getTally().getName());
		stopped = true;

		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		super.respond(events);

		if (events.size() == 0) return; // startup

		
		// Get the current tally.
		
		Serializable o = getActivityContext().getStateValue(state);
		Number num = null;
		try { num = (Number)o; }
		catch (ClassCastException cce) { throw new ModelContainsError(
			"Tally state value is not numeric"); }
			
		if (num == null) num = initValue;
		double currentTotal = num.doubleValue();

		
		// Get each input Event and update the tally state.

		List<Port> inputPorts = getInputPorts();
		for (Port inputPort : inputPorts)
		{
			SortedEventSet<GeneratedEvent> inputEvents = getEventsOnPort(inputPort, events);
			SortedEventSet<GeneratedEvent> eventsTallied = null;
			if (! doubleCount) eventsTallied = new SortedEventSetImpl();
			
			for (GeneratedEvent event : inputEvents)
			{
				if (event.isCompensation()) continue;  // don't count compensating Events.
				
				if (! doubleCount)  // check if the Event has already been tallied.
				{
					if (eventsTallied.contains(event)) continue;
					eventsTallied.add(event);
				}
				
				Serializable eventValue = event.getNewValue();
				if (eventValue == null) continue; // ignore null inputs.
					
				Number numericValue = null;
				try { numericValue = (Number)eventValue; }
				catch (ClassCastException cce) { throw new ModelContainsError(
					"An input Event to a Tally is non-numeric"); }
					
				currentTotal += ((Number)numericValue).doubleValue();
			}
		}
		
		num = new Double(currentTotal);
		
		getActivityContext().setState(this.state, num, events);
	}


	protected State getState() { return state; }
	
	protected String getStateName() { return AbstractTally.stateName; }
	
	protected AbstractTally getTally() { return (AbstractTally)(getActivity()); }
	
	protected List<Port> getInputPorts() { return getTally().getInputPorts(); }
	
	protected Port getOutputPort() { return getTally().getOutputPort(); }
	
	//protected Number getInitialValue() { return getTally().getInitialValue(); }

	//protected boolean getDoubleCount() { return getTally().getDoubleCount(); }
}
