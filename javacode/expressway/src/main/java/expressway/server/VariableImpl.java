/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.server.ModelElement.ModelAttribute;
import expressway.server.Event.*;
import expressway.ser.*;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.IOException;


public class VariableImpl extends ParameterImpl implements Variable
{
	/** Primary Node Reference. */
	Set<VariableAttributeBinding> varAttrBindings =
		new TreeSetNullDisallowed<VariableAttributeBinding>();

	Set<DecisionFunction> referencedBy = new TreeSetNullDisallowed<DecisionFunction>();
	
	Set<Decision> decisions = new TreeSetNullDisallowed<Decision>();
	
	
	public transient String[] varAttrBindingNodeIds = null;
	public transient String[] decisionNodeIds = null;
	

	/**
	 * Constructor.
	 */

	VariableImpl(String name, DecisionPoint dp)
	throws
		ParameterError
	{
		super(name, dp);
		setResizable(false);
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		setResizable(false);
	}
	
	
	public String getTagName() { return "variable"; }


	public String[] getVarAttrBindingNodeIds() { return varAttrBindingNodeIds; }
	public String[] getDecisionNodeIds() { return decisionNodeIds; }
	
	
	public SortedSet<PersistentNode> getChildren() // must extend
	{
		SortedSet<PersistentNode> children = super.getChildren();
		children.addAll(varAttrBindings);
		return children;
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		// Delete (per the Delete Pattern) or disconnect the Node from any other
		// child Nodes known to this class that depend on the existence of the Node
		// that is being deleted. These are normally siblings of the Node that
		// is being deleted.
		
		// Call super.deleteChild.
		try { super.deleteChild(child, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			// Nullify references to the Node that this class knows about.
			if (! this.varAttrBindings.remove(child)) throw pe;
			
			// Set outermostAffectedRef argument, if any.
			setRefArgIfNotNull(outermostAffectedRef, this);
		}
	}
	
	
	public PersistentNode getChild(String name) // must extend
	throws
		ElementNotFound
	{
		try { return super.getChild(name); }
		catch (ElementNotFound ex)
		{
			for (VariableAttributeBinding b : varAttrBindings)
				if (b.getName().equals(name)) return b;
			throw ex;
		}
	}
	

	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		super.renameChild(child, newName);
	}
	

	public Class getSerClass() { return VariableSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		varAttrBindingNodeIds = createNodeIdArray(varAttrBindings);
		decisionNodeIds = createNodeIdArray(decisions);
		
		
		// Set Ser fields.
		
		((VariableSer)nodeSer).varAttrBindingNodeIds = this.getVarAttrBindingNodeIds();
		((VariableSer)nodeSer).decisionNodeIds = this.getDecisionNodeIds();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		VariableImpl newInstance = (VariableImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.varAttrBindings = PersistentNodeImpl.cloneNodeSet(varAttrBindings, cloneMap, newInstance);
		newInstance.referencedBy = cloneNodeSet(referencedBy, cloneMap, newInstance);
		newInstance.decisions = cloneNodeSet(decisions, cloneMap, newInstance);
		
		return newInstance;
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
			Set<VariableAttributeBinding> varAttrBindingsCopy = 
				new TreeSetNullDisallowed<VariableAttributeBinding>(varAttrBindings);
			for (VariableAttributeBinding b : varAttrBindingsCopy) deleteAttributeBinding(b);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		for (DecisionFunction f : referencedBy) f.variableDeleted(this);
		
		// Nullify all references.
	}
	
	
	public String setNameAndNotify(String newName)
	throws
		ParameterError
	{
		String oldName = super.setNameAndNotify(newName);
		
		for (DecisionFunction f : referencedBy) f.variableNameChanged(this, oldName);
		
		return oldName;
	}
	

	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.VariableIconImageName);
	}
	
	
	void addReferencedBy(DecisionFunction decisionFunction)
	{
		referencedBy.add(decisionFunction);
	}
	
	
	void removeReferencedBy(DecisionFunction decisionFunction)
	{
		referencedBy.remove(decisionFunction);
	}
	
	
	public Set<DecisionFunction> getReferencedBy()
	{
		return referencedBy;
	}


	public Set<VariableAttributeBinding> getAttributeBindings()
	{
		return varAttrBindings;
	}


	public VariableAttributeBinding bindToAttribute(ModelAttribute a)
	{
		// Check that the binding does not exist for this Variable.

		for (VariableAttributeBinding binding : varAttrBindings) try
		{
			if (binding.getAttribute() == a) return binding;
		}
		catch (ValueNotSet w) {} // ok


		// Check that the binding does not exist for the Attribute.

		VariableAttributeBinding attrBinding = a.getParameterBinding();

		try
		{
			if ((attrBinding != null) && (attrBinding.getVariable() == this))
				//(attrBinding.getVariable().equals(this))) return attrBinding;
				return attrBinding;
		}
		catch (ValueNotSet w) {} // ok


		// Create a new binding.

		VariableAttributeBinding varBinding = new VariableAttributeBindingImpl(this, a);


		// Add the new binding to both the Variable's and Attribute's set
		// of bindings.

		varAttrBindings.add(varBinding);
		a.setParameterBinding(varBinding);


		// Maintain redundant list of VariableAttributeBindings in the
		// Attribute's ModelDomain.

		a.getModelDomain().addVariableAttributeBinding(varBinding);

		return varBinding;
	}


	public void deleteAttributeBinding(VariableAttributeBinding binding)
	throws
		ParameterError
	{
		deleteChild(binding);
	}
	
	
	public Set<Decision> getDecisions()
	{
		return decisions;
	}


	public void addDecision(Decision decision)
	{
		//GlobalConsole.println("444Entered addDecision of Variable " + getName());

		/*
		 * Add the Decision to this Variable's Set of Decisions.
		 */

		// Check that the decision does not belong to the same DecisionScenario
		// of any other Decisions that are connected to this Variable.

		DecisionScenario dScenario = decision.getDecisionScenario();

		for (Decision d : decisions)
		{
			if (dScenario == d.getDecisionScenario()) throw new RuntimeException(
			//if (dScenario.equals(d.getDecisionScenario())) throw new RuntimeException(
				"Attempt to add a Decision to Variable " + getName() +
				" when the Variable already " +
				"has a Decision in the Decision's DecisionScenario.");
		}

		// Ok to add it.

		decisions.add(decision);


		/*
		 * If the decision is not a Choice, then add it to the list of Decisions
		 * for the DecisionPoint that owns this Variable.
		 *

		if (! (decision instanceof Choice))
		{
			DecisionPoint dp = getDecisionPoint();

			dp.addDecision(decision);
		}
		 */
	}


	public void removeDecision(Decision decision)
	{
		decisions.remove(decision);
		if (decision != null) decision.setVariable(null);
	}

	
	public void decisionFunctionDeleted(DecisionFunction df)
	{
	}
	

	public void decisionFunctionNameChanged(DecisionFunction df, String oldName)
	{
	}
	
	
	public void dump(int indentation)
	{
		String padding = "";
		for (int i = 1; i <= indentation; i++) padding +="\t";
		GlobalConsole.println(padding + super.toString() + ": " + getName());
	}
}
