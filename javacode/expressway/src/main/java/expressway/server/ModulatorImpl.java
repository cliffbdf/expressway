/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.Event.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


public class ModulatorImpl extends ActivityBase implements Modulator
{
	public static final Double DefaultDefaultFactor = 1.0;
	
	/** reference only. */
	public Port inputPort = null;

	/** reference only. */
	public Port outputPort = null;
	
	/** reference only. */
	public Port controlPort = null;
	
	/** reference only. */
	public State state = null;  // the output state.
	
	/** Ownership. */
	public Double defaultFactor = DefaultDefaultFactor;  // Initial value of factorAttr.
	
	/** reference only. */
	public ModelAttribute factorAttr = null;  // Scenario-specific. Overrides Default.
	
	
	public String getInputPortNodeId() { return getNodeIdOrNull(inputPort); }
	public String getOutputPortNodeId() { return getNodeIdOrNull(outputPort); }
	public String getControlPortNodeId() { return getNodeIdOrNull(controlPort); }
	public String getStateNodeId() { return getNodeIdOrNull(state); }
	public String getFactorAttrNodeId() { return getNodeIdOrNull(factorAttr); }
	
	
	public Class getSerClass() { return ModulatorSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((ModulatorSer)nodeSer).inputPortNodeId = this.getInputPortNodeId();
		((ModulatorSer)nodeSer).outputPortNodeId = this.getOutputPortNodeId();
		((ModulatorSer)nodeSer).controlPortNodeId = this.getControlPortNodeId();
		((ModulatorSer)nodeSer).stateNodeId = this.getStateNodeId();
		((ModulatorSer)nodeSer).defaultFactor = this.getDefaultFactor();
		((ModulatorSer)nodeSer).factorAttrNodeId = this.getFactorAttrNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ModulatorImpl newInstance = (ModulatorImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.inputPort = cloneMap.getClonedNode(inputPort);
		newInstance.outputPort = cloneMap.getClonedNode(outputPort);
		newInstance.controlPort = cloneMap.getClonedNode(controlPort);
		newInstance.state = cloneMap.getClonedNode(state);
		newInstance.factorAttr = cloneMap.getClonedNode(factorAttr);
		
		return newInstance;
	}

	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		
		writer.println(indentString + "<modulator name=\"" + this.getName() 
			+ "\" factor=\"" + this.defaultFactor + "\""
			+ " width=\"" + this.getWidth() 
			+ "\" height=\"" + this.getHeight() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\">");
		
		
		writeXMLAttributesForPredefElements(writer, indentation+1);


		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);


		writer.println(indentString + "</modulator>");
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(inputPort, null);
		deletePort(outputPort, null);
		deletePort(controlPort, null);
		deleteState(state, null);
		deleteAttribute(factorAttr, null);
		defaultFactor = null;
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		inputPort = null;
		outputPort = null;
		controlPort = null;
		state = null;
		factorAttr = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public ModulatorImpl(String name, ModelContainer parent, ModelDomain domain,
		Double factor)
	{
		super(name, parent, domain);
		init(factor);
	}
	
	
	public ModulatorImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init(DefaultDefaultFactor);
	}
	
	
	public String getTagName() { return "modulator"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ModulatorIconImageName);
	}
	
	
	private void init(Double factor)
	{
		setResizable(false);
		setLayoutManager(DefaultLayoutManager);
		
		this.defaultFactor = factor;
		
		try
		{
			if (factor == null)
				this.factorAttr = createModelAttribute("factor", 
					new Double(DefaultDefaultFactor), this, null);
			else
				this.factorAttr = createModelAttribute("factor", new Double(factor), this, null);

			inputPort = super.createPort("input_port", PortDirectionType.input, 
				Position.left, this, null);
			outputPort = super.createPort("output_port", PortDirectionType.output, 
				Position.right, this, null);
			if (factor == null) 
				controlPort = super.createPort("control_port", PortDirectionType.input, 
					Position.top, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}


		// Automatically bind the output Port to the internal State.

		try { state = super.createState("OutputState", this, null); }
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}


		try { state.bindPort(outputPort); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		inputPort.setMovable(false);
		outputPort.setMovable(false);
		if (controlPort != null) controlPort.setMovable(false);
		state.setMovable(false);
		factorAttr.setMovable(false);
		
		state.setVisible(false);
		
		inputPort.setPredefined(true);
		outputPort.setPredefined(true);
		if (controlPort != null) controlPort.setPredefined(true);
		state.setPredefined(true);
		factorAttr.setPredefined(true);
	}
	
	
	public int getNoOfHeaderRows() { return 1; }
	
	
	private static final double PreferredWidthInPixels = 60.0;
	private static final double PreferredHeightInPixels = 20.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change Port direction for a standard component");
	}


	public Port getInputPort() { return inputPort; }

	public Port getOutputPort() { return outputPort; }
	
	public Port getControlPort() { return controlPort; }
	
	public State getState() { return state; }
	
	public ModelAttribute getFactorAttr() { return factorAttr; }
	
	public Double getDefaultFactor() { return defaultFactor; }


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init(DefaultDefaultFactor);
	}
}


class ModulatorNativeImpl extends ActivityNativeImplBase
{
	boolean stopped = false;
	double factor;
	State state;
	

	public synchronized void start(ActivityContext context) throws Exception
	{
		if (stopped) throw new RuntimeException("Already stopped");
		super.start(context);
		
		this.factor = getAttributeDoubleValue(getModulator().getFactorAttr());
		this.state = getModulator().getState();
	}


	public synchronized void stop()
	{
		stopped = true;

		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		//if (OperationMode.TestMode)
		//	System.out.println(getModulator().getName() + " responding...");
		
		super.respond(events);

		if (events.size() == 0) return;


		/*
		 * If there is an event on the control port (including a startup event),
		 *	Read the control port and update the modulation factor..
		 *
		 * Create an event on the output state, representing the value of the
		 *	input port modulated by the value on the control port.
		 */
		
		if (getControlPort() != null)
		{
			boolean controlIsNull = true;
			if (portHasNonCompensatingEvent(events, getControlPort()))
			{
				//System.out.println("port has event");
				Serializable cVal = getActivityContext().getStateValue(getControlPort());
				if (cVal == null) controlIsNull = true;
				else
				{
					controlIsNull = false;
					if (! (cVal instanceof Number)) throw new ModelContainsError(
						"Control value of a Modulator must be Numeric");
					
					factor = ((Number)cVal).doubleValue();
				}
			}
			
			if (controlIsNull) return;  // Do not respond when the control input is null.
		}
		
		
		// Check if there is an event on either the control port or the input port.
		if
		(
			((getControlPort() != null) && portHasNonCompensatingEvent(events, 
				getControlPort()))
			  || 
				portHasEvent(events, getInputPort())
		)
		{		
			// Get input value.
			Serializable inVal = getActivityContext().getStateValue(getInputPort());
			if (! (inVal instanceof Number)) throw new ModelContainsError(
				"Value of input to a Modulator must be numeric");
			Number inputValue = (Number)inVal;
			
			
			// Update output value.
			
			Double newValue = new Double(inputValue.doubleValue() * factor);
			
			if (portHasNonCompensatingEvent(events, getInputPort()))
				getActivityContext().setState(this.state, newValue, events);
			else
				getActivityContext().scheduleCompensationEvent(this.state, newValue,
					events);

		}
	}
	
	
	/**
	 * Generate an output event to match the current driven state of the input
	 * port.
	 *
	
	protected void updateOutput(Double newValue, SortedEventSet events)
	throws
		ModelContainsError
	{
		// Drive the output port.
		getActivityContext().setState(getModulator().getState(), newValue, events);
	}*/
	
	
	protected Port getInputPort() { return getModulator().getInputPort(); }
	protected Port getControlPort() { return getModulator().getControlPort(); }

	
	protected ModulatorImpl getModulator() { return (ModulatorImpl)(getActivity()); }
}

