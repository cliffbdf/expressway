/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import expressway.server.ModelElement.*;
import expressway.ser.*;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.*;


public abstract class MenuOwnerBase extends MotifElementBase implements MenuOwner
{
	/** Primary Node Reference. */
	private List<MenuItem> menuItems = new Vector<MenuItem>();
	
	
	public Class getSerClass() { return MenuOwnerBaseSer.class; }
	
	
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		for (MenuItem mi : menuItems) if (mi.getName().equals(name)) return mi;
		return super.getChild(name);
	}
	
	
	public SortedSet<PersistentNode> getChildren()
	{
		SortedSet<PersistentNode> children = new TreeSetNullDisallowed<PersistentNode>(super.getChildren());
		children.addAll(menuItems);
		return children;
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		try { super.deleteChild(child, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			if (child instanceof MenuItem)
			{
				if (! menuItems.remove(child)) throw new RuntimeException(
					"Node '" + child.getName() + "' not found within '" + getFullName() + "'");
			}
			else
				throw pe;

			setRefArgIfNotNull(outermostAffectedRef, this);
		}
	}
	
	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		super.renameChild(child, newName);
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		for (MenuItem mi : menuItems) try { deleteMenuItem(mi); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		this.menuItems = null;
	}
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		MenuOwnerBaseSer menuOwnerSer = (MenuOwnerBaseSer)nodeSer;
		
		
		// Set all transient externalizable Node fields.
		
		// Set Ser fields.
		
		MenuOwnerHelper.copySerValues(this, menuOwnerSer);
		
		
		return super.externalize(nodeSer);
	}


	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		MenuOwnerBase newInstance = (MenuOwnerBase)(super.clone(cloneMap, cloneParent));
		
		newInstance.menuItems = cloneNodeList(menuItems, cloneMap, newInstance);
		
		return newInstance;
	}
	

	MenuOwnerBase(String baseName, Hierarchy parent)
	{
		super(baseName, parent);
	}
	
	
	/*
	public Action createAction(int position, String text, String desc, String javaMethodName)
	{
		Action action = new ActionImpl(text, this, this);
		action.setHTMLDescription(desc);
		action.setJavaMethodName(javaMethodName);
		menuItems.add(position, action);
		return action;
	}
	
	
	public Action createAction(String text, String desc, String javaMethodName)
	{
		return createAction(0, text, desc, javaMethodName);
	}
	
	
	public MenuTree createMenuTree(int position, String text, String desc)
	{
		MenuTree menuTree = new MenuTreeImpl(text, this, this);
		menuTree.setHTMLDescription(desc);
		menuItems.add(position, menuTree);
		return menuTree;
	}
	
	
	public MenuTree createMenuTree(String text, String desc)
	{
		return createMenuTree(0, text, desc);
	}
	
	
	public void deleteMenuItem(MenuItem menuItem)
	{
		try { deleteChild(menuItem); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}
	
	
	public void deleteMenuItem(int menuItemPos)
	{
		deleteMenuItem(menuItems.get(menuItemPos));
	}
	
	
	public List<MenuItem> getMenuItems()
	{
		return menuItems;
	}
	
	
	public int getIndexOf(MenuItem mi)
	{
		return menuItems.indexOf(mi);
	}*/
}

