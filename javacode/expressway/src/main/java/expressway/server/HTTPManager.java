/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import java.net.*;


public class HTTPManager
{
	private ModelEngineLocal modelEngine;
	private InetSocketAddress inetSocketAddress;
	private Server server;
	
	
	public HTTPManager(ModelEngineLocal me) { this.modelEngine = me; }
	
	
	public InetSocketAddress getInetSocketAddress() { return inetSocketAddress; }
	
	
	public void startHTTPServer(InetAddress iaddr, int port)
	throws
		Exception
	{
		this.inetSocketAddress = new InetSocketAddress(iaddr, port);
		this.server = new Server(this.inetSocketAddress);
 
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		//context.setContextPath(root.getAbsolutePath());
		//context.setResourceBase();
		context.addServlet(new ServletHolder(new ExpresswayServlet(this.modelEngine)), "/*");
		
 		server.setHandler(context);
		server.start();
	}
	
	
	public void stopHTTPServer()
	throws
		Exception
	{
		if (server == null) return;
		server.stop();
		server = null;
	}
}

