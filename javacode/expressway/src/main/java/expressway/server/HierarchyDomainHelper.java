/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.ser.*;
import expressway.server.HierarchyElement.*;
import expressway.server.MotifElement.*;
import java.util.SortedSet;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import java.util.List;
import java.util.Vector;


public class HierarchyDomainHelper extends DomainHelper
{
	private HierarchyHelper hierHelper;
	
	
	public HierarchyDomainHelper(HierarchyDomain domain, MenuOwner superDomain,
		HierarchyHelper hierHelper)
	{
		super(domain, superDomain);
		this.hierHelper = hierHelper;
	}
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		HierarchyDomainSer ser = (HierarchyDomainSer)nodeSer;
		hierHelper.externalize(ser);
		return super.externalize(ser);
	}


	public List<HierarchyScenario> getHierarchyScenarios()
	{
		List<Scenario> ss = getDomain().getScenarios();
		List<HierarchyScenario> hss = new Vector<HierarchyScenario>();
		for (Scenario s : ss) hss.add((HierarchyScenario)s);
		return hss;
	}
	
	
	public HierarchyScenario getHierarchyScenario(String name)
	throws
		ElementNotFound, // if the HierarchyScenario cannot be found.
		ParameterError
	{
		return (HierarchyScenario)(getDomain().getScenario(name));
	}
	
	
	public void addHierarchyScenario(HierarchyScenario scenario)
	{
		getDomain().addScenario(scenario);
	}
	
	
	public HierarchyScenarioSet getHierarchyScenarioSet(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		return (HierarchyScenarioSet)(getDomain().getScenarioSet(name));
	}
		
		
	public List<HierarchyScenarioSet> getHierarchyScenarioSets()
	{
		List<ScenarioSet> sss = getDomain().getScenarioSets();
		List<HierarchyScenarioSet> hsss = new Vector<HierarchyScenarioSet>();
		for (ScenarioSet ss : sss) hsss.add((HierarchyScenarioSet)ss);
		return hsss;
	}
	
	
	public void addHierarchyScenarioSet(HierarchyScenarioSet s)
	{
		getDomain().addScenarioSet(s);
	}
	
	
	public void deleteHierarchyScenarioSet(HierarchyScenarioSet s)
	throws
		ParameterError
	{
		getDomain().deleteScenarioSet(s);
	}


	public HierarchyScenario getCurrentHierarchyScenario()
	{
		return (HierarchyScenario)(getDomain().getCurrentScenario());
	}
	
	
	public void setCurrentHierarchyScenario(HierarchyScenario scenario)
	throws
		ElementNotFound,
		ParameterError
	{
		getDomain().setCurrentScenario(scenario);
	}
}

