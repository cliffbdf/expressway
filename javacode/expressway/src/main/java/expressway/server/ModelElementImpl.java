/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import expressway.server.DecisionElement.VariableAttributeBinding;
import expressway.server.Event.*;
import expressway.ser.*;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.IOException;
import java.lang.reflect.Method;


public abstract class ModelElementImpl extends TemplatizableNode
	implements ModelElement
{
	/** Initialization data for the instantiable Element Lists. */
	private static final Object[][] instantiableElements =
	{
		{ NodeKindNames.ModelAttribute, "createAttribute", "@Attributes" }
	};
	
	
	protected static List<String> staticInstantiableNodeKinds;
	protected static List<String> staticCreateMethodNames;
	protected static List<String> staticNodeDescriptions;
	
	
	static
	{
		staticInstantiableNodeKinds = 
			new Vector<String>(PersistentNodeImpl.staticInstantiableNodeKinds);
		staticCreateMethodNames = 
			new Vector<String>(PersistentNodeImpl.staticCreateMethodNames);
		staticNodeDescriptions = 
			new Vector<String>(PersistentNodeImpl.staticNodeDescriptions);
		
		for (Object[] oa : instantiableElements)
			staticInstantiableNodeKinds.add((String)(oa[0]));
		
		for (Object[] oa : instantiableElements)
			staticCreateMethodNames.add((String)(oa[1]));
		
		for (Object[] oa : instantiableElements)
			staticNodeDescriptions.add((String)(oa[2]));
	}
	
	
	private boolean lockedForSimulation = false;
	private String viewClassName = null;
	
	
	/**
	 * Constructor.
	 */

	public ModelElementImpl(String name, ModelElement parent, ModelDomain domain)
	{
		super(parent);
		try
		{
			if (parent == null) setName(name);
			else setName(parent.createUniqueChildName(name));
		}
		catch (ParameterError pe) { throw new RuntimeException("Should not occur"); }
		
		if ((parent == null) && (! (this instanceof ModelDomain)))
			throw new RuntimeException("Only a Domain can have a null parent");
	}
	
	
	/**
	 * Used by ModelContainer to instantiate new Elements
	 * that will then be attributed by the user. This constructor must provide
	 * reasonable values for any required fields.
	 */
	 
	ModelElementImpl(ModelElement parent, String baseName)
	{
		super(parent);
		try
		{
			if (parent == null) setName(baseName);
			else setName(parent.createUniqueChildName(baseName));
		}
		catch (ParameterError pe) { throw new RuntimeException("Should not occur"); }
	}

	
  /* From PersistentNode */


	public Class getSerClass() { return ModelElementSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		String modelDomainNodeId = getNodeIdOrNull(getDomain());

		// Set Ser fields.
		
		ModelElementSer ser = (ModelElementSer)nodeSer;
		ser.setViewClassName(viewClassName);

		return super.externalize(nodeSer);
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ModelElementImpl newInstance = (ModelElementImpl)(super.clone(cloneMap, cloneParent));
		newInstance.lockedForSimulation = false;
		return newInstance;
	}

	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		//<DirectedRelation>.modelElementDeleted(this); not implemented yet.
		
		// Nullify all references.
	}
	
	
	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{}
	

	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{}
	
	
	public PersistentNode getChild(String name) // must extend
	throws
		ElementNotFound
	{
		try { return super.getChild(name); }
		catch (ElementNotFound ex)
		{
			Set<PersistentNode> shs = getChildren();
			for (PersistentNode h : shs) if (h.getName().equals(name)) return h;
			throw ex;
		}
	}
	
	
	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		Set<PersistentNode> elts = this.getChildren();
		SortedSet<PersistentNode> predefSet = new TreeSetNullDisallowed<PersistentNode>();
		for (PersistentNode elt : elts)
			if (elt.isPredefined()) predefSet.add(elt);
		
		return predefSet;
	}
	
	
	public Attribute constructAttribute(String name, Serializable defaultValue)
	throws
		ParameterError
	{
		return new ModelAttributeImpl(name, this, defaultValue);
	}
	

	public Attribute constructAttribute(String name)
	{
		return new ModelAttributeImpl(this, name);
	}
	
	
	public void dump() { dump(0); }
	
	
  /* From ModelElement */

	
	public void lockForSimulation(boolean lockValue)
	throws
		CannotObtainLock
	{
		ModelElementHelper.lockForSimulation(this, lockValue);
	}
	
	
	public boolean isLockedForSimulation() { return lockedForSimulation; }


	public void setLockedForSimulation(boolean locked) { lockedForSimulation = locked; }
	
	
	public ModelElement findModelElement(String qualifiedName)
	throws
		ParameterError
	{
		return ModelElementHelper.findModelElement(this, qualifiedName);
	}
	
	
	public ModelElement findModelElement(String[] pathParts)
	throws
		ParameterError
	{
		return ModelElementHelper.findModelElement(this, pathParts);
	}
	
	
	public void shiftAllChildren(double dx, double dy)
	{
		ModelElementHelper.shiftAllChildren(this, dx, dy);
	}

	
	public ModelAttribute createModelAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return ModelElementHelper.createModelAttribute(this, name, defaultValue, layoutBound,
			outermostAffectedRef);
	}
	
	
	public ModelAttribute createModelAttribute(PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return ModelElementHelper.createModelAttribute(this, layoutBound, outermostAffectedRef);
	}
	
	
	public ModelAttribute createModelAttribute(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return ModelElementHelper.createModelAttribute(this, name, layoutBound, outermostAffectedRef);
	}


	public final ModelDomain getModelDomain()
	{
		return (ModelDomain)(getDomain());
	}


	public ModelAttribute getModelAttribute(String name)
	{
		return ModelElementHelper.getModelAttribute(this, name);
	}
	

	public SortedSet<ModelAttribute> getModelAttributes()
	{
		return ModelElementHelper.getModelAttributes(this);
	}


	public ModelElement findFirstNestedElement(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		return ModelElementHelper.findFirstNestedElement(this, name);
	}
	
	
	/**
	 * Deprecated.
	 */
	public ModelElement getModelElement()
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError
	{
		return this;
	}
	
	
	public List<String> getInstantiableNodeKinds()
	{
		List<String> names = staticInstantiableNodeKinds;
		getDynamicInstantiableNodeKind(names);
		return names;
	}
	
	
	public List<String> getStaticCreateMethodNames()
	{
		return staticCreateMethodNames;
	}
	
	
	public List<String> getInstantiableNodeDescriptions()
	{
		return staticNodeDescriptions;
	}
	
	
  /* From Hierarchy: */
	
	
	public Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		throw new DisallowedOperation();
	}
	

  /* Implementation */
	
	
	public String toString()
	{
		return "[ " + this.getClass().getName() + ": " + getFullName() +
			" (" + this.getNodeId() + ") at " + this.getX() + ", " + this.getY() + "]";
	}
	
	
	List<String> getStaticNodeKinds()
	{
		return staticInstantiableNodeKinds;
	}
	
	
	/** Return the Help page names that are available for the Nodes
		that can be instantiated within a Node. See the HelpWindow class
		for an explanation of what a page name is. */
	
	List<String> getStaticNodeDescriptions()
	{
		return staticNodeDescriptions;
	}
}

