/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.Event.*;
import expressway.server.ModelElement.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import expressway.common.ClientModel.*;
import expressway.statistics.HistogramDomainParameters;
import expressway.statistics.TimeSeriesParameters;
import expressway.generalpurpose.ThrowableUtil;
import java.io.File;
import java.io.Serializable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import javax.swing.ImageIcon;
import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.stat.descriptive.SummaryStatistics;
import org.apache.commons.math.stat.descriptive.SummaryStatisticsImpl;
import org.apache.commons.math.stat.regression.SimpleRegression;
import org.xml.sax.SAXParseException;


/** ****************************************************************************
 * Implements the ModelEngine interface using a Java object without any
 * persistence. Methods are synchronized to provide guarded re-entrancy.
 *
 * This implementation requires the user to identify a 'current' Decision
 * Domain, and that is used as the Decision Domain for all requests until
 * the user specifies another Decision Domain.
 * 
 * The option -import=<file_path> may be specified. This causes the Model Engine
 * to load the specified model definition file at startup.
 */

public class ModelEngineRMIPojoImpl extends UnicastRemoteObject
	implements ModelEngineRMI
{
	public static final String FileSeparator = System.getProperty("file.separator");
	
	private ModelEngineLocalPojoImpl modelEngineLocal;
	
	
	
	/** ************************************************************************
	 * Main method for Expressway server when server is run from command line.
	 
	 NOT NEEDED ANYMORE.
	 *
	 
	public static void main(String[] args)
	{
		// Obtain command line options.
		
		int noOfOptions = 0;
		String importFile = null;
		String dbdir = null;
		//long randomSeed = 0;
		//boolean randomSeedProvided = false;
		
		for (int i = 0; i < args.length; i++)
		{
			if (args[i].substring(0,1).equals("-"))
			{
				noOfOptions++;
				String optionString = args[i].substring(1);
				String[] optionParts = optionString.split("=");
				
				if (optionParts[0].equals("import"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"The import option must specify an XML file path");
						return;
					}
					
					importFile = optionParts[1];
				}
				else if (optionParts[0].equals("dbdir"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"The dbdir option must specify a directory path");
						return;
					}
					
					dbdir = optionParts[1];
				}
				//else if (optionParts[0].equals("random_seed"))
				//{
				//	if (optionParts.length != 2)
				//	{
				//		GlobalConsole.println(
				//			"Option random_seed must specify a seed value");
				//		return;
				//	}
				//	
				//	try { randomSeed = Long.parseLong(optionParts[1]); }
				//	catch (NumberFormatException nfe)
				//	{
				//		GlobalConsole.println(
				//			"Option random_seed must specify a long value");
				//		return;
				//	}
				//	
				//	randomSeedProvided = true;
				//}
			}
			else break;  // past the options.
		}

		
		GlobalConsole.println("Loading Expressway server...");
		
		
		// Load the database specified by the -dbdir option, if any.
		
		ModelEngineLocalPojoImpl modelEngineLocal = null;
		if (dbdir == null)
		{
			GlobalConsole.print(
				"Warning: No -dbdir option, so you will not be able to save the database. ");
			GlobalConsole.println("Proceed? ");
			String line;
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try { line = br.readLine(); }
			catch (IOException ex) { throw new RuntimeException(ex); }
			if (! (line.equalsIgnoreCase("y") || line.equalsIgnoreCase("yes"))) return;			
			
			try 
			{
				//if (randomSeedProvided) 
				//	modelEngineLocal = new ModelEngineLocalPojoImpl(null, randomSeed);
				//else 
					modelEngineLocal = new ModelEngineLocalPojoImpl(null);
			}
			catch (RemoteException re)
			{
				GlobalConsole.println("While instantiating local server instance: " +
					ThrowableUtil.getAllMessages(re));
				return;
			}
		}
		else  // read in and reconstitute a Model Engine previously serialized.
		{
			File dbDirFile = new File(dbdir);
			if (dbDirFile.exists()) try
			{
				File dbfile = ModelEngineLocalPojoImpl.findDatabaseFile(dbDirFile);
				modelEngineLocal = ModelEngineLocalPojoImpl.loadDatabase(dbfile, new File(dbDirFile, "simrundir"));
			}
			catch (Exception ex)  // did not find a database file
			{
				try
				{
					//if (randomSeedProvided) 
					//	modelEngineLocal = new ModelEngineLocalPojoImpl(dbDirFile, randomSeed);
					//else
						modelEngineLocal = new ModelEngineLocalPojoImpl(dbDirFile);
				}
				catch (IOException ex2)
				{
					GlobalConsole.printStackTrace(ex2);
					return;
				}
			}
			else try // No database file exists
			{
				if ((! dbDirFile.exists()) && (! dbDirFile.mkdir())) throw new IOException(
					"Counld not create directory " + dbDirFile);
				
				//if (randomSeedProvided) 
				//	modelEngineLocal = new ModelEngineLocalPojoImpl(dbDirFile, randomSeed);
				//else
					modelEngineLocal = new ModelEngineLocalPojoImpl(dbDirFile);
			}
			catch (Exception ex)
			{
				GlobalConsole.println(
					"While creating database directory or instantiating server local instance: " +
					ThrowableUtil.getAllMessages(ex));
				GlobalConsole.printStackTrace(ex);
				return;
			}
			
			modelEngineLocal.reinitialize();
		}
		
		
		// Load the file specified by the -import option, if any.
		
		if (importFile != null)
		{
			XMLParser xmlParser = new XMLParser();
			try
			{
				xmlParser.parse(importFile);
				xmlParser.setModelEngineLocal(modelEngineLocal);
				xmlParser.setClientId(ModelAPITypes.RMIServerClientId);
				xmlParser.genDocument();
				System.err.println("Done parsing input file");
			}
			catch (SAXParseException spe)
			{
				GlobalConsole.println("At line " + spe.getLineNumber() +
					", column " + spe.getColumnNumber() + ", " + spe.getMessage());
				GlobalConsole.printStackTrace(spe);
			}
			catch (Exception ex)
			{
				GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
				GlobalConsole.printStackTrace(ex);
				return;
			}
			finally
			{
				List<String> warnings = xmlParser.getWarnings();
				GlobalConsole.println("Warnings generated:");
				for (String warning : warnings) GlobalConsole.println("\t" + warning);
			}
		}
		
		
		ModelEngineRMIPojoImpl modelEngineRMI = null;
		try { modelEngineRMI = new ModelEngineRMIPojoImpl(modelEngineLocal); }
		catch (Exception ex)
		{
			GlobalConsole.println("While instantiating RMI server: " + ThrowableUtil.getAllMessages(ex));
			return;
		}
		
		
		// Register server with RMI.
		
		try { modelEngineRMI.register(); }
		catch (Exception ex)
		{
			GlobalConsole.println("While registering RMI server: " + ThrowableUtil.getAllMessages(ex));
			return;
		}
		
		
		// Watch for commands entered on the command line console.
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		GlobalConsole.println("...server started. Type 'quit' to stop the server.");
		for (;;)
		{
			GlobalConsole.print("> ");
			String line = null;
			try { line = br.readLine(); }
			catch (IOException ioe)
			{
				GlobalConsole.println("While reading from console: " + ThrowableUtil.getAllMessages(ioe));
				continue;
			}
			
			if (line.startsWith("import "))
			{
				String[] importArgs = line.split(" ");
				if (importArgs.length != 2)
				{
					GlobalConsole.println(
						"The import command must have one file path argument.");
				}
				else
				{
					String xmlFilePathString = importArgs[1];
					
					XMLParser xmlParser = new XMLParser();
					try
					{
						xmlParser.parse(xmlFilePathString);
						xmlParser.setModelEngineLocal(modelEngineLocal);
						xmlParser.setClientId(ModelAPITypes.RMIServerClientId);
						xmlParser.genDocument();
					}
					catch (SAXParseException spe)
					{
						GlobalConsole.println("At line " + spe.getLineNumber() +
							", column " + spe.getColumnNumber() + ", " + spe.getMessage());
						GlobalConsole.printStackTrace(spe);
					}
					catch (Exception ex)
					{
						GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
						GlobalConsole.printStackTrace(ex);
					}
					finally
					{
						List<String> warnings = xmlParser.getWarnings();
						GlobalConsole.println("Warnings generated:");
						for (String warning : warnings) GlobalConsole.println("\t" + warning);
					}
				}
			}
			else if (line.equals("quit"))
			{
				//PersistentNodeImpl.dumpAllNodeIds();
				//PersistentNodeImpl.dumpAllNodes();
				
				
				// Ask if changes should be saved.
				
				GlobalConsole.print("Save changes to models? (y/n) ");
				try { line = br.readLine(); }
				catch (IOException ex) { throw new RuntimeException(ex); }
				
				List<ModelDomain> domains = null;
				if (line.equalsIgnoreCase("y") || line.equalsIgnoreCase("yes"))
				for (;;)
				{
					try
					{
						File file = null;
						GlobalConsole.print("Enter the path of the output file: ");
						line = br.readLine();
						
						file = new File(line);
						FileOutputStream fos = new FileOutputStream(file);
						ObjectOutputStream oos = new ObjectOutputStream(fos);
						
						// Copy static values to the ModelEngineLocal instance.
						//modelEngineLocal.random = RandomGenerator.getRandomGenerator();
						modelEngineLocal.priorUniqueId = PersistentNodeImpl.priorUniqueId;
						modelEngineLocal.allNodes = PersistentNodeImpl.allNodes;
						modelEngineLocal.defaultVisualGuidance = DefaultVisualGuidance.guidance;

						oos.writeObject(modelEngineLocal);
					}
					catch (Exception ex)
					{
						GlobalConsole.println(ex.getClass().getName() + ": " + ex.getMessage());
						GlobalConsole.printStackTrace(ex);
						GlobalConsole.println("Try again?");
						try { line = br.readLine(); }
						catch (IOException ex2)
						{
							break;
						}
						
						if (line.equalsIgnoreCase("y") || line.equalsIgnoreCase("yes"))
							continue;
					}
					
					break;
				}
				
				
				// Ask whether to export models.
				
				GlobalConsole.print("Export models? (y/n) ");
				try { line = br.readLine(); }
				catch (IOException ex) { throw new RuntimeException(ex); }
				
				if (line.equalsIgnoreCase("y") || line.equalsIgnoreCase("yes"))
				{
					boolean write = true;
					
					File directory = null;
					for (;;) try
					{
						GlobalConsole.print("Enter the path of the directory: ");
						line = br.readLine();
						
						directory = new File(line);
						
						if (! directory.exists())
						{
							GlobalConsole.print("Directory not found. Try again? (y/n) ");
							line = br.readLine();
							if (line.equalsIgnoreCase("y") || line.equalsIgnoreCase("yes"))
								continue;
							else
							{
								write = false;
								break;
							}
						}

						if (! directory.isDirectory())
						{
							GlobalConsole.print("Path is not a directory. Try again? (y/n) ");
							line = br.readLine();
							if (line.equalsIgnoreCase("y") || line.equalsIgnoreCase("yes"))
								continue;
							else
							{
								write = false;
								break;
							}
						}
						
						break;
					}
					catch (Exception ex)
					{
						GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
					}
					
					if (write) try
					{
						XMLWriter xmlWriter = new XMLWriter(modelEngineLocal, ModelAPITypes.RMIServerClientId);
						try { xmlWriter.writeLibrary(directory); }
						catch (Exception ex)
						{ GlobalConsole.println(ThrowableUtil.getAllMessages(ex)); }
					}
					catch (Exception ex)
					{
						GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
					}
				}
				
				
				// Ask if simulations should be saved.
				
				GlobalConsole.print("Save simulations that were run? (y/n) ");
				try { line = br.readLine(); }
				catch (IOException ex) { throw new RuntimeException(ex); }
				
				if (line.equalsIgnoreCase("y") || line.equalsIgnoreCase("yes"))
				{
					if (domains == null) try { domains = 
						modelEngineLocal.getModelDomainPersistentNodes(); }
					catch (Exception ex)
					{
						throw new RuntimeException("Cannot obtain Domains from Model Engine");
					}
					
					boolean write = true;
					
					File directory = null;
					for (;;) try
					{
						GlobalConsole.print("Enter the path of the sim directory: ");
						line = br.readLine();
						
						directory = new File(line);
						
						if (! directory.exists())
						{
							GlobalConsole.print("Directory not found. Try again? (y/n) ");
							line = br.readLine();
							if (line.equalsIgnoreCase("y") || line.equalsIgnoreCase("yes"))
								continue;
							else
							{
								write = false;
								break;
							}
						}

						if (! directory.isDirectory())
						{
							GlobalConsole.print("Path is not a directory. Try again? (y/n) ");
							line = br.readLine();
							if (line.equalsIgnoreCase("y") || line.equalsIgnoreCase("yes"))
								continue;
							else
							{
								write = false;
								break;
							}
						}
						
						break;
					}
					catch (Exception ex)
					{
						GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
					}

					if (write) for (ModelDomain domain : domains) try
					{
						Set<ModelScenario> scenarios = domain.getModelScenarios();
						
						for (ModelScenario scenario : scenarios)
						{
							SortedSet<SimulationRun> simRuns = scenario.getSimulationRuns();
							
							
							for (SimulationRun simRun : simRuns)
							{
								String fileName = domain.getName() + "." + scenario.getName() +
									"." + simRun.getName() + ".simrun";
									
								File file = new File(directory, fileName);
								
								if (file.exists())
								{
									GlobalConsole.print("File " + fileName + " already exists in "
										+ directory + "; overwrite? (y/n) ");
										
									line = br.readLine();
									if (! (line.equalsIgnoreCase("y") || line.equalsIgnoreCase("yes")))
										continue;
								}

								PrintWriter pw = new PrintWriter(new FileWriter(file));
								EventArchiver.ArchiveWriter archiveWriter = 
									EventArchiver.getArchiveWriter(pw);
								try { archiveWriter.writeSimRun(simRun); }
								catch (Exception ex)
								{
									GlobalConsole.printStackTrace(ex);
									GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
								}
								finally { pw.close(); }
							}
						}
					}
					catch (Exception ex)
					{
						GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
					}
				}
				
				
				GlobalConsole.println("Unbinding service...");

				try { LocateRegistry.getRegistry().unbind(Constants.RMIServiceName); }
				catch (Exception re) { GlobalConsole.println(
					"While getting Registry: " + ThrowableUtil.getAllMessages(re)); }
				
				GlobalConsole.println("...stopping all service threads...");
				boolean stoppedGracefully = modelEngineLocal.stopAllServiceThreads();
				GlobalConsole.println("...all threads stopped" +
					(stoppedGracefully ? " gracefully..." : ", some by force..."));
				GlobalConsole.println("...returning...");
				System.exit(stoppedGracefully ? 0 : 1);
			}
			else if (line.trim().equals(""))
				GlobalConsole.println();
			else
				GlobalConsole.println("Unrecognized command: " + line);
		}
	}*/
	
	
	/** ************************************************************************
	 * Constructor.
	 */

	public ModelEngineRMIPojoImpl(ModelEngineLocalPojoImpl modelEngineLocal)
	throws
		RemoteException
	{
		super();

		if (modelEngineLocal == null) throw new RuntimeException(
			"ModelEngineLocal is null");
		
		this.modelEngineLocal = modelEngineLocal;
	}
	
	
	/** ************************************************************************
	 * This constructor is primarily for debugging. Setting the random number
	 * seed provides repeatability. (To do: allow a seed to be specified for
	 * a model, allowing repeatability for debugging models.)
	 *
	 
	public ModelEngineRMIPojoImpl(long randomSeed)
	throws
		RemoteException
	{
		super();
		
		this.modelEngineLocal = new ModelEngineLocalPojoImpl(null, randomSeed);
		
		setRMICodebase();
	}*/
	
	
	/** ************************************************************************
	 * Obtain an ID that uniquely identifies the client. This is used to determine
	 * if two conflicting requests come from different clients: if they are from
	 * the same client then merely accept the most recent request.
	 */
	 
	synchronized String getClientId()
	throws
		IOException
	{
		try { return ((java.rmi.server.RemoteServer)this).getClientHost(); }
		catch (java.rmi.server.ServerNotActiveException ex) { throw new IOException(ex); }
	}
	
	
	synchronized void register()
	throws
		IOException
	{
		Registry registry = LocateRegistry.getRegistry();
		
		//String rmiCodebase = "file://" + System.getProperty("user.home") + 
		//	FileSeparator + "server.jar";
			
		//rmiCodebase = rmiCodebase.replace(" ", "%20");
			
		//GlobalConsole.println("Using RMI codebase " + rmiCodebase);
		//System.setProperty("java.rmi.server.codebase", rmiCodebase);
		
		GlobalConsole.print("...registering server...");
		try { registry.rebind(Constants.RMIServiceName, this); }
		catch (IOException ex) { GlobalConsole.println(); throw ex; }
		catch (RuntimeException rte) { GlobalConsole.println(); throw rte; }
		
		GlobalConsole.println("registered. Server ready.");
	}
	

	synchronized void unregister()
	throws
		IOException
	{
		//UnicastRemoteObject.unexportObject(this, true /* force */);
		try { LocateRegistry.getRegistry().unbind(Constants.RMIServiceName); }
		catch (java.rmi.NotBoundException ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	
	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Methods from ModelEngineRMI.
	 *
	 */


	public synchronized ListenerRegistrar registerPeerListener(PeerListener listener)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.registerPeerListener(getClientId(), listener);
	}
	
	
	public synchronized void unregisterPeerListener(PeerListener listener)
	throws
		ParameterError,
		IOException
	{
		modelEngineLocal.unregisterPeerListener(listener);
	}


	
	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Methods from ModelEngineRemote.
	 *
	 */


	public synchronized String ping()
	throws
		IOException
	{
		return modelEngineLocal.ping(getClientId());
	}
	
	
	public synchronized void flush()
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		modelEngineLocal.flush(getClientId());
	}


	public synchronized Integer parseXMLCharAr(char[] charAr, PeerListener peerListener)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		BaseException,
		IOException
	{
		return modelEngineLocal.parseXMLCharAr(getClientId(), charAr, peerListener);
	}


	public synchronized Integer processJARByteAr(byte[] byteAr,
		PeerListener peerListener)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		BaseException,
		IOException
	{
		return modelEngineLocal.processJARByteAr(getClientId(), byteAr, peerListener);
	}


	public synchronized ModelDomainSer getModelDomain(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getModelDomain(getClientId(), name);
	}


	public synchronized DecisionDomainSer getDecisionDomain(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getDecisionDomain(getClientId(), name);
	}


	public synchronized List<String> getDomainNames()
	throws
		IOException
	{
		return modelEngineLocal.getDomainNames(getClientId());
	}
	

	public synchronized List<NodeDomainSer> getDomains()
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getDomains(getClientId());
	}
	
		
	public synchronized List<ModelDomainSer> getModelDomains()
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getModelDomains(getClientId());
	}
	
		
	public synchronized List<ModelDomainMotifDefSer> getModelDomainMotifs()
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getModelDomainMotifs(getClientId());
	}


	public synchronized List<MotifDefSer> getMotifsUsedByDomain(String domainId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getMotifsUsedByDomain(getClientId(), domainId);
	}


	public synchronized boolean domainUsesMotif(String domainId, String motifId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.domainUsesMotif(getClientId(), domainId, motifId);
	}
	

	public synchronized void useMotif(String domainId, String motifId, boolean yes)
	throws
		ParameterError,
		Warning,
		IOException
	{
		modelEngineLocal.useMotif(getClientId(), domainId, motifId, yes);
	}

	
	public synchronized byte[] getMotifResource(String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException
	{
		return modelEngineLocal.getMotifResource(getClientId(), pathWithinMotifJar);
	}	
	
	
	public synchronized byte[] getMotifClass(String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException
	{
		return modelEngineLocal.getMotifClass(getClientId(), pathWithinMotifJar);
	}


	public synchronized List<DecisionDomainSer> getDecisionDomains()
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getDecisionDomains(getClientId());
	}
		
		
	public synchronized String exportNodeAsXML(String nodeId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.exportNodeAsXML(getClientId(), nodeId);
	}
	
	
	public synchronized DomainTypeSer[] getDomainTypes()
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getDomainTypes(getClientId());
	}
		
	
	public synchronized void createDomainForDomainType(String id)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		modelEngineLocal.createDomainForDomainType(getClientId(), id);
	}
		
	
	public synchronized DomainTypeSer[] getBaseDomainTypes()
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getBaseDomainTypes(getClientId());
	}
		
	
	public synchronized void createDomainType(String baseDomainTypeId, String... motifDefIds)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		modelEngineLocal.createDomainType(getClientId(), baseDomainTypeId, motifDefIds);
	}
		
	
	public synchronized void deleteDomainType(String domainTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		modelEngineLocal.deleteDomainType(getClientId(), domainTypeId);
	}	
		
	
	public synchronized MotifDefSer[] getMotifsForBaseDomainType(String baseDomainTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		return modelEngineLocal.getMotifsForBaseDomainType(getClientId(), baseDomainTypeId);
	}
		
	
	public synchronized MotifDefSer[] getMotifsCompatibleWithDomainId(String domainId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		return modelEngineLocal.getMotifsCompatibleWithDomainId(getClientId(), domainId);
	}


	public synchronized DomainTypeSer[] getUserDefinedDomainTypes()
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getUserDefinedDomainTypes(getClientId());
	}	
	
	
	public synchronized MotifDefSer createMotifForDomainType(String domainBaseTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		return modelEngineLocal.createMotifForDomainType(getClientId(), domainBaseTypeId);
	}
	
	
	public synchronized String getViewType(String nodeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		return modelEngineLocal.getViewType(getClientId(), nodeId);
	}
	

	public synchronized String getViewClassName(String nodeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		return modelEngineLocal.getViewClassName(getClientId(), nodeId);
	}


	public synchronized ModelDomainSer createModelDomain(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.createModelDomain(getClientId(), name, useNameExactly);
	}


	public synchronized ModelDomainSer createModelDomain()
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.createModelDomain(getClientId());
	}


	public synchronized ModelDomainMotifDefSer createModelDomainMotif(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.createModelDomainMotif(getClientId(), name, useNameExactly);
	}
		

	public synchronized ModelDomainMotifDefSer createModelDomainMotif()
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.createModelDomainMotif(getClientId());
	}
		

	public synchronized ModelDomainSer createIndependentCopy(String modelDomainId)
	throws
		ParameterError,
		ModelContainsError,
		IOException
	{
		return modelEngineLocal.createIndependentCopy(getClientId(), modelDomainId);
	}


	public synchronized DecisionDomainSer createDecisionDomain(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.createDecisionDomain(getClientId(), name, useNameExactly);
	}


	public synchronized ScenarioSer createScenario(String domainName)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.createScenario(getClientId(), domainName);
	}


	public synchronized ScenarioSer copyScenario(String scenarioId)
	throws
		ModelContainsError,
		ParameterError,
		IOException
	{
		return modelEngineLocal.copyScenario(getClientId(), scenarioId);
	}


	public synchronized ScenarioSer createScenario(String domainName, String scenarioName)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.createScenario(getClientId(), domainName, scenarioName);
	}
		

	public synchronized AttributeSer createAttribute(boolean confirm, String parentId, 
		double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.createAttribute(getClientId(), confirm, parentId, x, y);
	}
	

	public synchronized AttributeSer createAttributeBefore(boolean confirm, String nextAttrId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.createAttributeBefore(getClientId(), confirm, nextAttrId);
	}


	
	public synchronized AttributeSer createAttributeAfter(boolean confirm, String priorAttrId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.createAttributeAfter(getClientId(), confirm, priorAttrId);
	}

	
	public synchronized int getNoOfAttributes(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		return modelEngineLocal.getNoOfAttributes(getClientId(), nodeId);
	}
	
	
	public synchronized AttributeSer createAttribute(boolean confirm, String parentId, 
		Serializable value, String scenarioId, double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.createAttribute(getClientId(), confirm, parentId, value, 
			scenarioId, x, y);
	}
	

	public synchronized AttributeSer createHierarchyDomainAttributeBefore(
		boolean confirm,
		String domainId, String attrId, Serializable value, String scenarioId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.createHierarchyDomainAttributeBefore(getClientId(),
			confirm, domainId, attrId, value, scenarioId);
	}
		
		
	public synchronized AttributeSer createHierarchyDomainAttributeAfter(
		boolean confirm,
		String domainId, String attrId, Serializable value, String scenarioId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.createHierarchyDomainAttributeBefore(getClientId(),
			confirm, domainId, attrId, value, scenarioId);
	}

	
	public synchronized AttributeStateBindingSer bindAttributeToState(boolean confirm, String attrId, 
		String stateFullName)
	throws
		Warning,
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.bindAttributeToState(getClientId(), confirm, attrId, 
			stateFullName);
	}
	

	public synchronized void deleteAttributeStateBinding(boolean confirm, String bindingNodeId)
	throws
		Warning,
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.deleteAttributeStateBinding(getClientId(), confirm, bindingNodeId);
	}


	public synchronized void setStateBindingParameters(boolean confirm, String domainOrScenarioId, Boolean type,
		TimePeriodType period)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.setStateBindingParameters(getClientId(), confirm, domainOrScenarioId,
			type, period);
	}
	

	public synchronized void bindScenarioToScenarioForDomain(boolean confirm, 
		String bindingScenarioId, String boundScenarioId, String boundDomainId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.bindScenarioToScenarioForDomain(getClientId(), confirm, 
			bindingScenarioId, boundScenarioId, boundDomainId);
	}


	public synchronized AttributeStateBindingSer getAttributeStateBinding(String attrId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.getAttributeStateBinding(getClientId(), attrId);
	}


	public synchronized ModelDomainSer[] getBoundDomains(String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.getBoundDomains(getClientId(), modelDomainId);
	}


	public synchronized ModelDomainSer[] getBindingDomains(String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.getBindingDomains(getClientId(), modelDomainId);
	}


	public synchronized ModelScenarioSer getBoundScenarioForBoundDomain(
		String bindingScenarioId, String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.getBoundScenarioForBoundDomain(getClientId(), 
			bindingScenarioId, modelDomainId);
	}


	public synchronized StateSer[] getStatesForModelDomain(String modelDomainName)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.getStatesForModelDomain(getClientId(), modelDomainName);
	}


	public synchronized StateSer[] getTallyStatesForModelDomain(String modelDomainName)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.getTallyStatesForModelDomain(getClientId(), modelDomainName);
	}


	public synchronized NodeSer createListNodeBefore(boolean confirm, String parentId,
		String nextNodeId, String scenarioId, String copiedNodeId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.createListNodeBefore(getClientId(), confirm, parentId,
			nextNodeId, scenarioId, copiedNodeId);
	}

	
	public synchronized NodeSer createListNodeAfter(boolean confirm, String parentId,
		String priorNodeId, String scenarioId, String copiedNodeId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.createListNodeAfter(getClientId(), confirm, parentId,
			priorNodeId, scenarioId, copiedNodeId);
	}

	
	public synchronized NodeSer cloneListNodeAfter(boolean confirm, String nodeId)
	throws
		Warning,
		ParameterError,
		InternalEngineError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.cloneListNodeAfter(getClientId(), confirm, nodeId);
	}
	
	
	public synchronized String[] getInterfaceNodeKinds()
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getInterfaceNodeKinds(getClientId());
	}
	

	public synchronized String[] getInstantiableNodeKinds(String parentId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getInstantiableNodeKinds(getClientId(), parentId);
	}


	public synchronized String[] getInstantiableNodeDescriptions(String parentId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getInstantiableNodeDescriptions(getClientId(), parentId);
	}


	public synchronized ModelElementSer createModelElement(boolean confirm, 
		String parentId, String kind,
		double x, double y)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.createModelElement(getClientId(), confirm, parentId, kind, x, y);
	}


	public synchronized void deleteNode(boolean confirm, String elementId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.deleteNode(getClientId(), confirm, elementId);
	}


	public synchronized void demoteListNode(boolean confirm, String nodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.demoteListNode(getClientId(), confirm, nodeId);
	}
	
	
	public synchronized void promoteListNode(boolean confirm, String nodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.promoteListNode(getClientId(), confirm, nodeId);
	}
		

	public synchronized void createNamedReference(boolean confirm, String domainId,
		String name, String desc, String fromTypeClassName, String toTypeClassName)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.createNamedReference(getClientId(), confirm, domainId,
			name, desc, fromTypeClassName, toTypeClassName);
	}
	
	
	public synchronized void deleteNamedReference(boolean confirm, String namedRefId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.deleteNamedReference(getClientId(), confirm, namedRefId);
	}	
		

	public synchronized void createCrossReference(boolean confirm, //String domainId,
		String namedRefName, String fromNodeId, String toNodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.createCrossReference(getClientId(), confirm, //domainId,
			namedRefName, fromNodeId, toNodeId);
	}


	public synchronized void deleteCrossReference(boolean confirm, String crossRefId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.deleteCrossReference(getClientId(), confirm, crossRefId);
	}
	
	
	public synchronized NodeSer[] getNamedRefFromNodes(String nodeId, String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		return modelEngineLocal.getNamedRefFromNodes(getClientId(), nodeId, namedRefName);
	}
	

	public synchronized NodeSer[] getNamedRefToNodes(String nodeId, String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		return modelEngineLocal.getNamedRefToNodes(getClientId(), nodeId, namedRefName);
	}
	

	public synchronized String[] getNamedReferences(String domainId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		return modelEngineLocal.getNamedReferences(getClientId(), domainId);
	}
	

	public synchronized NamedReferenceSer getNamedReferenceForDomain(String domainId,
		String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		return modelEngineLocal.getNamedReferenceForDomain(getClientId(), domainId, namedRefName);
	}
	

	public synchronized CrossReferenceSer[] getCrossReferences(String namedRefId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		return modelEngineLocal.getCrossReferences(getClientId(), namedRefId);
	}
	

	public synchronized CrossReferenceSer[] getCrossReferences(String fromId, String toId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		return modelEngineLocal.getCrossReferences(getClientId(), fromId, toId);
	}
	

	public synchronized void bindStateAndPort(boolean confirm, String stateId, String portId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		modelEngineLocal.bindStateAndPort(getClientId(), confirm, stateId, portId);
	}


	public synchronized void unbindPort(boolean confirm, String portId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		modelEngineLocal.unbindPort(getClientId(), confirm, portId);
	}


	public synchronized StateSer getPortBinding(String portId)
	throws
		ParameterError,
		ElementNotFound,
		IOException
	{
		return modelEngineLocal.getPortBinding(getClientId(), portId);
	}
	
	
	public synchronized void setPredefEventDefaultTime(boolean confirm, 
		String predEventNodeId, String timeExpr)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		modelEngineLocal.setPredefEventDefaultTime(getClientId(), confirm,
			predEventNodeId, timeExpr);
	}
	
	
	public synchronized void setPredefEventDefaultValue(boolean confirm, 
		String predEventNodeId, String valueExpr)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		modelEngineLocal.setPredefEventDefaultValue(getClientId(), confirm,
			predEventNodeId, valueExpr);
	}


	public synchronized void setPredefEventIsPulse(boolean confirm,
		String predEventNodeId, boolean pulse)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		modelEngineLocal.setPredefEventIsPulse(getClientId(), confirm, predEventNodeId,
			pulse);
	}


	public synchronized void setNodeName(boolean confirm, String nodeId, String newName)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.setNodeName(getClientId(), confirm, nodeId, newName);
	}


	public synchronized void setNodeHTMLDescription(String nodeId, String newHTMLDesc)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.setNodeHTMLDescription(getClientId(), nodeId, newHTMLDesc);
	}


	public synchronized void setPortReflectivity(boolean confirm, String portId, boolean black)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.setPortReflectivity(getClientId(), confirm, portId, black);
	}


	public synchronized void setPortDirection(boolean confirm, String portId, PortDirectionType dir)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ModelContainsError,
		IOException
	{
		modelEngineLocal.setPortDirection(getClientId(), confirm, portId, dir);
	}


	public synchronized ConduitSer createConduit(boolean confirm, String parentId, String portAId, String portBId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.createConduit(getClientId(), confirm, parentId, portAId, portBId);
	}
	

	public synchronized ConduitSer createConduit(boolean confirm, String portAId, String portBId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.createConduit(getClientId(), confirm, portAId, portBId);
	}
	

	public synchronized void rerouteConduit(String conduitId)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.rerouteConduit(getClientId(), conduitId);
	}


	public synchronized void rerouteConduits(String containerId, String[] conduitIds)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		modelEngineLocal.rerouteConduits(getClientId(), containerId, conduitIds);
	}


	public synchronized NodeSer getNode(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getNode(getClientId(), nodeId);
	}

	
	public synchronized NodeSer getNode(String nodeId, Class nodeSerKind)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getNode(getClientId(), nodeId, nodeSerKind);
	}
	
	
	public synchronized NodeSer getChild(String parentId, String childName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getChild(getClientId(), parentId, childName);
	}
	
	
	/*public synchronized NodeSer updateNode(boolean confirm, NodeSer nodeSer)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		return modelEngineLocal.updateNode(getClientId(), confirm, nodeSer);
	}*/


	public synchronized String[] getAvailableHTTPFormats(String nodeId, String[] descriptions)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		return modelEngineLocal.getAvailableHTTPFormats(getClientId(), nodeId, descriptions);
	}
	
	
	public synchronized String getWebURLString(String nodeId, String format)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		return modelEngineLocal.getWebURLString(getClientId(), nodeId, format);
	}
	
	
	public synchronized int getHTTPPort()
	throws
		IOException
	{
		return modelEngineLocal.getHTTPPort(getClientId());
	}
	

	public synchronized ScenarioSer updateScenario(boolean confirm, 
		ScenarioSer scenarioSer)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		return modelEngineLocal.updateScenario(getClientId(), confirm, scenarioSer);
	}


	public synchronized void setModelScenarioTimeParams(boolean confirm, String scenarioId,
		Date newFromDate, Date newToDate, long newDuration, int newIterLimit)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		modelEngineLocal.setModelScenarioTimeParams(getClientId(), confirm,
			scenarioId, newFromDate, newToDate, newDuration, newIterLimit);
	}


	public synchronized NodeSer incrementNodeSize(int lastRefreshed, String nodeId,
		double dw, double dh, double childShiftX, double childShiftY, boolean notify)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		return modelEngineLocal.incrementNodeSize(getClientId(), lastRefreshed, nodeId, dw,
			dh, childShiftX, childShiftY, notify);
	}

	
	public synchronized NodeSer incrementNodeLocation(int lastRefreshed, String nodeId,
		double dx, double dy, boolean notify)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		return modelEngineLocal.incrementNodeLocation(getClientId(), lastRefreshed, nodeId, 
			dx, dy, notify);
	}
	

	public synchronized NodeSer changeNodeSize(int lastRefreshed, String nodeId,
		double newWidth, double newHeight, double childShiftX, double childShiftY)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		return modelEngineLocal.changeNodeSize(getClientId(), lastRefreshed, nodeId, newWidth,
			newHeight, childShiftX, childShiftY);
	}

	
	public synchronized NodeSer changeNodeLocation(int lastRefreshed, String nodeId,
		double newX, double newY)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		return modelEngineLocal.changeNodeLocation(getClientId(), lastRefreshed, nodeId, 
			newX, newY);
	}
	

	public synchronized String resolveNodePath(String path)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.resolveNodePath(getClientId(), path);
	}


	public synchronized String[] getChildNodeIds(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getChildNodeIds(getClientId(), nodeId);
	}
	
	
	public synchronized String[] getSubcomponentNodeIds(String modelContainerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getSubcomponentNodeIds(getClientId(), modelContainerId);
	}


	public synchronized AttributeSer getAttribute(String nodeId, String attrName)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getAttribute(getClientId(), nodeId, attrName);
	}
	
	
	public synchronized AttributeSer[] getAttributes(String nodeId, String className,
		String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getAttributes(getClientId(), nodeId, className, scenarioId);
	}


	public synchronized NodeSer[] getNodesWithAttributeDeep(String nodeId, 
		String className, String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getNodesWithAttributeDeep(getClientId(), 
			nodeId, className, scenarioId);
	}
	

	public synchronized AttributeSer[] getAttributesDeep(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getAttributesDeep(getClientId(), nodeId);
	}


	public synchronized List<String> getProgress(int callbackId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getProgress(getClientId(), callbackId);
	}


	public synchronized List<String> getMessages(int callbackId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getMessages(getClientId(), callbackId);
	}


	public synchronized boolean isComplete(int callbackId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.isComplete(getClientId(), callbackId);
	}
	

	public synchronized boolean isAborted(int callbackId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.isAborted(getClientId(), callbackId);
	}
	

	public synchronized void abort(int callbackId)
	throws
		ParameterError,
		IOException
	{
		modelEngineLocal.abort(getClientId(), callbackId);
	}


	public synchronized Integer simulate(String modelDomainName, String scenarioName, 
		Date initialEpoch, Date finalEpoch,
		int iterations, boolean step, int runs, long duration, PeerListener peerListener,
		boolean repeatable)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.simulate(getClientId(), modelDomainName, scenarioName,
			initialEpoch, finalEpoch, iterations, step, runs, duration, peerListener,
			repeatable);
	}


	public synchronized ModelScenarioSetSer createScenariosOverRange(String baseScenId,
		String attrId, Serializable[] values)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.createScenariosOverRange(getClientId(), baseScenId,
			attrId, values);
	}
	
	
	public synchronized Integer simulateScenarioSet(String scenSetId,
		Date initialEpoch, Date finalEpoch,
		int iterations, boolean step, int noOfRuns, long duration, 
		PeerListener peerListener, boolean repeatable
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.simulateScenarioSet(getClientId(), scenSetId, 
			initialEpoch, finalEpoch, iterations, step, noOfRuns, duration,
			peerListener, repeatable);
	}
	
		
	public synchronized double[] computeAssurances(String scenSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.computeAssurances(getClientId(), scenSetId);
	}
		

	public synchronized Integer[] updateVariable(int lastRefreshed,
		String decisionDomainName,
		String scenarioName, String decisionPointName, String variableName,
		Vector<Serializable> valueVector)
	throws
		ClientIsStale,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.updateVariable(getClientId(), lastRefreshed, decisionDomainName,
			scenarioName, decisionPointName, variableName, valueVector);
	}
	
	
	public synchronized List<double[]> getResultStatisticsForScenario(List<String> nodePaths,
		String modelDomainName, String modelScenarioName, String[] optionsStrings)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getResultStatisticsForScenario(getClientId(), nodePaths, modelDomainName,
			modelScenarioName, optionsStrings);
	}

	
	public synchronized List<double[]> getResultStatisticsById(List<String> nodeIds,
		String distOwnerId, String[] options)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getResultStatisticsById(getClientId(), nodeIds, distOwnerId,
			options);
	}


	public synchronized double[] getResultStatisticsById(String nodeId,
		String distOwnerId, String[] options)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getResultStatisticsById(getClientId(), nodeId, distOwnerId,
			options);
	}


	public synchronized List<int[]> getHistograms(List<String> statePaths,
		String modelDomainName, String modelScenarioName, double bucketSize,
		double bucketStart, int noOfBuckets, boolean allValues)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getHistograms(getClientId(), statePaths, modelDomainName,
			modelScenarioName, bucketSize, bucketStart, noOfBuckets, allValues);
	}
	

	public synchronized int[] getHistogramById(String stateId,
		String distOwnerId, double bucketSize, double bucketStart, int noOfBuckets,
		boolean allValues)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getHistogramById(getClientId(), stateId, distOwnerId,
			bucketSize, bucketStart, noOfBuckets, allValues);
	}

	
	public synchronized HistogramDomainParameters defineOptimalHistogramById(
		String stateId, String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.defineOptimalHistogramById(getClientId(), stateId, distOwnerId);
	}


	public synchronized TimeSeriesParameters defineOptimalValueHistoryById(
		String stateId, String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.defineOptimalValueHistoryById(getClientId(), 
			stateId, distOwnerId);
	}
		
		
	public synchronized double[][] getExpectedValueHistory(String stateId, 
		String distOwnerId, Date startingEpoch, int noOfPeriods, long periodInDays)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getExpectedValueHistory(getClientId(), stateId, distOwnerId,
			startingEpoch, noOfPeriods, periodInDays);
	}

	
	public synchronized double[] getCorrelations(List<String[]> statePaths,
		String modelDomainName, String modelScenarioName, String optionString)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getCorrelations(getClientId(), statePaths, modelDomainName,
			modelScenarioName, optionString);
	}
	

	public synchronized List<String> getSimulationRuns(String modelDomainName,
		String modelScenarioName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getSimulationRuns(getClientId(), modelDomainName, 
			modelScenarioName);
	}
	

	public synchronized List<SimulationRunSer> getSimulationRuns(ModelScenarioSer s)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getSimulationRuns(getClientId(), s);
	}


	public synchronized List<SimulationRunSer> getSimulationRuns(String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getSimulationRuns(getClientId(), distOwnerId);
	}


	public synchronized ModelScenarioSer[] getScenariosForScenarioSetId(String scenSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getScenariosForScenarioSetId(getClientId(), scenSetId);
	}
	
	
	public synchronized SimulationRunSer[] getSimulationRunsForSimRunSetId(String simRunSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getSimulationRunsForSimRunSetId(getClientId(), simRunSetId);
	}	


	public synchronized SimRunSetSer[] getSimRunSetsForScenarioId(String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getSimRunSetsForScenarioId(getClientId(), scenarioId);
	}
		
		
	public synchronized List<GeneratedEventSer> getEventsForSimNodeForIteration(
		String simRunId, int iteration)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getEventsForSimNodeForIteration(getClientId(), 
			simRunId, iteration);
	}


	public synchronized List<GeneratedEventSer>[] getEventsByEpoch(String modelDomainName,
		String modelScenarioName,
		String simRunName, List<String> paths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getEventsByEpoch(getClientId(), modelDomainName, modelScenarioName,
			simRunName, paths);
	}
	
	
	public synchronized List<GeneratedEventSer>[] getEventsForSimNodeByEpoch(String simNodeId, 
		List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getEventsForSimNodeByEpoch(getClientId(),
			simNodeId, statePaths);
	}


	public synchronized List<GeneratedEventSer>[] getEventsForSimNodeByEpoch(String simNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getEventsForSimNodeByEpoch(getClientId(), simNodeId);
	}


	public synchronized Object[] getEventsForSimNodeByEpoch(String simNodeId,
		List<String> statePaths, Class[] stateTags)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		List<String>[] filteredPathsHolder = new List[] { null, null };
		List<String>[] filteredIdsHolder = new List[] { null, null };
		
		List<GeneratedEventSer>[] eventSers = modelEngineLocal.getEventsForSimNodeByEpoch(
			getClientId(), simNodeId, statePaths, stateTags, filteredPathsHolder,
			filteredIdsHolder);
		
		Object[] result = new Object[3];
		
		result[0] = eventSers;
		result[1] = filteredPathsHolder[0];
		result[2] = filteredIdsHolder[0];
		
		return result;
	}


	public synchronized StateSer[] getStatesRecursive(String modelContainerNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getStatesRecursive(getClientId(), modelContainerNodeId);
	}
	
	
	public synchronized String getEventsByEpochAsHTML(String simRunNodeId, 
		List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getEventsByEpochAsHTML(getClientId(), simRunNodeId,
			statePaths);
	}


	public synchronized String getEventsByEpochAsHTML(String simRunNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getEventsByEpochAsHTML(getClientId(), simRunNodeId);
	}


	public synchronized Serializable getAttributeValue(String attrPath, String scenarioName)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getAttributeValue(getClientId(), attrPath, scenarioName);
	}

	
	public synchronized Serializable getAttributeValueForId(String attrId, String scenarioName)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getAttributeValueForId(getClientId(), attrId, scenarioName);
	}

	
	public synchronized Serializable getAttributeValueById(String attrId, String scenarioId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getAttributeValueById(getClientId(), attrId, scenarioId);
	}


	public synchronized Serializable[] getAttributeValuesForNode(String nodeId, String scenarioId)
	throws
		ElementNotFound,  // if the Node does not exist.
		ParameterError,  // if the Scenario does not exist
		IOException
	{
		return modelEngineLocal.getAttributeValuesForNode(getClientId(), nodeId, scenarioId);
	}
	
		
	public synchronized Serializable getAttributeDefaultValueById(String attrId)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException
	{
		return modelEngineLocal.getAttributeDefaultValueById(getClientId(), attrId);
	}
		
		
	public synchronized Serializable[] getAttributeDefaultValuesForNode(String nodeId)
	throws
		ElementNotFound,  // if the Node does not exist.
		ParameterError,  // if the Scenario does not exist
		IOException
	{
		return modelEngineLocal.getAttributeDefaultValuesForNode(getClientId(), nodeId);
	}
	
	
	public synchronized void setAttributeDefaultValue(boolean confirm, 
		String attrId, Serializable value)
	throws
		Warning,
		CannotObtainLock,
		ParameterError,
		IOException
	{
		modelEngineLocal.setAttributeDefaultValue(getClientId(), confirm, attrId, value);
	}
	
	
	public synchronized Serializable getAttributeDefaultValue(String attrPath)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getAttributeDefaultValue(getClientId(), attrPath);
	}
		
	
	public synchronized Object[] getStateValuesForIteration(String simRunId,
		int iteration)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.getStateValuesForIteration(getClientId(),
			simRunId, iteration);
	}
		
		
	public synchronized ScenarioSer[] getScenarios(String domainName)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getScenarios(getClientId(), domainName);
	}
	
	
	public synchronized ScenarioSer[] getScenariosForDomainId(String domainId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getScenariosForDomainId(getClientId(), domainId);
	}
	
	
	public synchronized ModelScenarioSer getModelScenario(String modelDomainName, String modelScenarioName)
	throws
		ParameterError,
		ElementNotFound,
		IOException
	{
		return modelEngineLocal.getModelScenario(getClientId(), modelDomainName, modelScenarioName);
	}
		
	
	public synchronized ModelScenarioSetSer[] getScenarioSetsForDomainId(String domainId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.getScenarioSetsForDomainId(getClientId(), domainId);
	}


	public synchronized AttributeSer[] getAttributes(String elementPath)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getAttributes(getClientId(), elementPath);
	}

	
	public synchronized AttributeSer[] getAttributesForId(String nodeId)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getAttributesForId(getClientId(), nodeId);
	}


	public synchronized boolean isGeneratorRepeating(String genNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return modelEngineLocal.isGeneratorRepeating(getClientId(), genNodeId);
	}
		
	
	public synchronized void makeGeneratorRepeating(boolean confirm, String genNodeId,
		boolean repeating)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		modelEngineLocal.makeGeneratorRepeating(getClientId(), confirm, genNodeId, repeating);
	}
		
		
	public synchronized void setGeneratorIgnoreStartup(boolean confirm, String genNodeId,
		boolean ignore)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		modelEngineLocal.setGeneratorIgnoreStartup(getClientId(), confirm, genNodeId, ignore);
	}


	public synchronized DecisionSer[] getDecisions(String decisionPointPath, String scenarioName)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getDecisions(getClientId(), decisionPointPath, scenarioName);
	}


	public synchronized ImageIcon getStoredImageIcon(String handle)
	throws
		ParameterError,
		IOException
	{
		return modelEngineLocal.getStoredImageIcon(getClientId(), handle);
	}

	
	public synchronized Serializable[] getFinalStateValues(String statePath, String scenarioName)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.getFinalStateValues(getClientId(), statePath, scenarioName);
	}
	

	public synchronized Serializable[] getFinalStateValuesForId(String stateId, String scenarioId)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return modelEngineLocal.getFinalStateValuesForId(getClientId(), stateId, scenarioId);
	}
	

	public synchronized void setAttributeValue(boolean confirm, String attrNodeId, String scenarioId,
		Serializable value)
	throws
		Warning,
		CannotObtainLock,
		ParameterError,
		IOException
	{
		modelEngineLocal.setAttributeValue(getClientId(), confirm, attrNodeId, scenarioId, value);
	}
		
		
	/*public synchronized void setAttributeValue(boolean confirm, String attrNodeId, Serializable value)
	throws
		Warning,
		CannotObtainLock,
		ParameterError,
		IOException
	{
		modelEngineLocal.setAttributeValue(getClientId(), confirm, attrNodeId, value);
	}*/
		
		
	public synchronized void watchNode(boolean enable, String nodePath)
	throws
		ParameterError,
		IOException
	{
		modelEngineLocal.watchNode(getClientId(), enable, nodePath);
	}
}
