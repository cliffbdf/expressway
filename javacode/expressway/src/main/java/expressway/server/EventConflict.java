/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import java.util.Date;
import java.util.Set;
import java.io.*;


/**
 * A container for a set of Events that conflict because they occur on the same
 * State during the same epoch. The value of the Event is considered to be 
 * ambiguous and is represented here as the value null. Such a situation
 * indicates an error in the model that generated the Events. Event Conflicts
 * may be written to an archive.
 */
 
public class EventConflict extends GeneratedEventImpl
{
	public EventSer externalize() throws ModelContainsError
	{
		EventSer eventSer = super.externalize();
		
		// Set all transient externalizable Node fields.
		
		
		
		// Set Ser fields.
		
		
		return eventSer;
	}
	
	
	public Class getSerClass() { return EventConflictSer.class; }
	
		
	public EventConflict(GeneratedEvent conflictingEvent)
	throws
		ParameterError
	{
		this(conflictingEvent.getState(), conflictingEvent.getTimeToOccur(), 
			conflictingEvent.getNewValue(),
			conflictingEvent.getSimulationRun());
	}


	public EventConflict(State state, Date time, Serializable newValue,
		SimulationRun simRun)
	throws
		ParameterError
	{
		super(state, time, null, newValue, simRun);
		
		this.setNewValue(null);
	}
}


