/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


/**
 * A Node that can be manipulated in a graphical manner.
 */
 
public interface PersistentGraphicNode extends PersistentNode
{
}

