/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;
import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.AbstractDelaySer;

import java.util.Set;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public abstract class AbstractDelay extends ActivityBase implements Delay
{
	public static final long DefaultDelay = 0;


	public static final AttributeValidator DelayParamValidator = new AttributeValidatorBase()
	{
		public synchronized void validate(Serializable newValue)
		throws
			ParameterError
		{
			if (! (newValue instanceof Number)) throw new ParameterError(
				"Delay parameter value must be a Number");
			
			Double value = ((Number)newValue).doubleValue();
			
			if (value <= 0.0) throw new ParameterError("Delay must be positive");
		}
	};


	/** reference only. */
	public Port inputPort = null;

	/** reference only. */
	public Port outputPort = null;

	/** reference only. */
	public State state = null;

	public long defaultDelay;
	
	/** reference only. */
	public ModelAttribute delayAttr = null;
		
	
	public transient String inputPortNodeId = null;
	public transient String outputPortNodeId = null;
	public transient String stateNodeId = null;
	public transient String delayAttrNodeId = null;


	public String getInputPortNodeId() { return inputPortNodeId; }
	public String getOutputPortNodeId() { return outputPortNodeId; }
	public String getStateNodeId() { return stateNodeId; }
	public String getDelayAttrNodeId() { return delayAttrNodeId; }

	
	public Class getSerClass() { return AbstractDelaySer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		inputPortNodeId = getNodeIdOrNull(inputPort);
		outputPortNodeId = getNodeIdOrNull(outputPort);
		stateNodeId = getNodeIdOrNull(state);
		delayAttrNodeId = getNodeIdOrNull(delayAttr);
		
		
		// Set Ser fields.
		
		((AbstractDelaySer)nodeSer).inputPortNodeId = this.getInputPortNodeId();
		((AbstractDelaySer)nodeSer).outputPortNodeId = this.getOutputPortNodeId();
		((AbstractDelaySer)nodeSer).stateNodeId = this.getStateNodeId();
		((AbstractDelaySer)nodeSer).defaultDelay = this.getDefaultDelay();
		((AbstractDelaySer)nodeSer).delayAttrNodeId = this.getDelayAttrNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		AbstractDelay newDelay = (AbstractDelay)(super.clone(cloneMap, cloneParent));
		
		newDelay.inputPort = cloneMap.getClonedNode(inputPort);
		newDelay.outputPort = cloneMap.getClonedNode(outputPort);
		newDelay.state = cloneMap.getClonedNode(state);
		newDelay.delayAttr = cloneMap.getClonedNode(delayAttr);
		
		return newDelay;
	}
	
		
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(inputPort, null);
		deletePort(outputPort, null);
		deleteState(state, null);
		deleteAttribute(delayAttr, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		inputPort = null;
		outputPort = null;
		state = null;
		delayAttr = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public AbstractDelay(String name, ModelContainer parent, ModelDomain domain,
		long delay)
	{
		super(name, parent, domain);
		
		init(delay);
	}
	
	
	public AbstractDelay(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		
		init(DefaultDelay);
	}
	
	
	public String getTagName() { return "delay"; }
	
	
	private void init(long delay)
	{
		setResizable(false);
		setLayoutManager(DefaultLayoutManager);
		
		this.defaultDelay = delay;
		
		
		// Create internal structure of this Delay.
		
		try
		{
			delayAttr = createModelAttribute("delay", new Long(delay), this, null);
			inputPort = super.createPort("input_port", PortDirectionType.input, Position.left, this, null);
			outputPort = super.createPort("output_port", PortDirectionType.output, Position.right, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		this.delayAttr.setValidator(DelayParamValidator);

		
		// Automatically bind the output Port to the internal State.

		try { state = super.createState("DelayState", this, null); }
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}

		state.setPredefined(true);
		inputPort.setPredefined(true);
		outputPort.setPredefined(true);
		delayAttr.setPredefined(true);

		try { state.bindPort(outputPort); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		inputPort.setMovable(false);
		outputPort.setMovable(false);
		state.setMovable(false);
		delayAttr.setMovable(false);
	}
	
	
	public int getNoOfHeaderRows() { return 1; }
	
	
	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	

	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change Port direction for a standard component");
	}


	public Port getInputPort() { return inputPort; }

	public Port getOutputPort() { return outputPort; }
	
	public long getDefaultDelay() { return defaultDelay; }
	
	public ModelAttribute getDelayAttr() { return delayAttr; }
}


class AbstractDelayNativeImpl extends ActivityNativeImplBase
{
	private State state = null;
	private String stateName = "DelayState";
	private long delay;

	boolean stopped = false;


	public synchronized void start(ActivityContext context) throws Exception
	{
		if (stopped) throw new RuntimeException("Already stopped");
		super.start(context);

		state = context.getState(stateName);
		
		this.delay = getAttributeLongValue(getDelay().getDelayAttr());
	}


	public synchronized void stop()
	{
		//System.out.println("eeee stopping " + getDelay().getName());
		stopped = true;

		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		super.respond(events);

		if (events.size() == 0) return;  // startup: do nothing.
		

		// Check that there is an event on the input port.
		
		if (portHasNonCompensatingEvent(events, getInputPort()))
		{
			// Get input value.
			Serializable inVal = getActivityContext().getStateValue(getInputPort());
			GeneratedEvent event = null;
			try
			{
				//System.out.println(getDelay().getName() + ": scheduling event...");
				
				event = getActivityContext().scheduleDelayedStateChange(
						this.state, this.delay, inVal, events);
				
				//System.out.println(getDelay().getName() + ": scheduled event: " + event.toString());
			}
			catch (ParameterError ex)
			{
				throw new ModelContainsError(
					"When scheduling delayed state change", ex);
			}
		}
	}


	protected State getState() { return state; }
	
	protected String getStateName() { return stateName; }
	
	protected AbstractDelay getDelay() { return (AbstractDelay)(getActivity()); }
	protected Port getInputPort() { return getDelay().getInputPort(); }
	protected Port getOutputPort() { return getDelay().getOutputPort(); }
}
