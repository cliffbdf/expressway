/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import java.util.Date;
import java.util.Set;
import java.util.Collection;
import java.io.*;

public abstract class SimulationTimeEventImpl extends EventImpl implements SimulationTimeEvent
{
	private long id;
	private SimulationRun simRun;



	/** The Epoch when the Event actually occurred. Must correspond to an Epoch
		instance in the SimulationRun's epochs list. */
	public Epoch epoch;


	public long getId() { return id; }
	
	
	public SimulationRun getSimulationRun() { return simRun; }


	public int getIteration() { return epoch.getIteration(); }
	
	
	public Epoch getEpoch() { return epoch; }


	public void setEpoch(Epoch epoch) { this.epoch = epoch; }


	public EventSer externalize() throws ModelContainsError
	//public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		synchronized (this.getState().getDomain())
		{
		EventSer eventSer = super.externalize();
		
		// Set all transient externalizable Node fields.
		
		
		// Set Ser fields.
		
		((SimulationTimeEventSer)eventSer).id = this.getId();
		((SimulationTimeEventSer)eventSer).simRunNodeId = getSimulationRun().getNodeId();
		((SimulationTimeEventSer)eventSer).epoch = this.epoch;
		
		return eventSer;
		}
	}
	
	
	public static long[] createIdArray(Collection<SimulationTimeEvent> col)
	{
		int eventNo = 0;
		Object[] objects = col.toArray();
		Object[] objectsCopy = java.util.Arrays.copyOf(objects, objects.length);
		
		int noOfEvents = objectsCopy.length;
		long[] result = new long[noOfEvents];
		
		for (Object o : objectsCopy)
		{
			SimulationTimeEvent event = (SimulationTimeEvent)o;
			result[eventNo++] = event.getId();
		}
		
		return result;
	}
	
		
	public int compareTo(Object arg)
	{
		// Returns a negative integer, zero, or a positive integer as this object
		// is less than, equal to, or greater than the specified object.
		
		long thisId = this.id;
		long argId = ((SimulationTimeEvent)arg).getId();
		
		if (thisId < argId) return -1;
		if (thisId == argId) return 0;
		return 1;
	}
	

	public Class getSerClass() { return SimulationTimeEventSer.class; }
	
	
	//public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	//throws
	//	CloneNotSupportedException
	//{
	//	SimulationTimeEventImpl newInstance = (SimulationTimeEventImpl)(super.clone(cloneMap, cloneParent));
	//	
	//	newInstance.newValue = PersistentNodeImpl.copyObject(newValue);
	//	
	//	return newInstance;
	//}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		this.simRun = null;
		this.state = null;
		this.epoch = null;
	}
	
	
	public SimulationTimeEventImpl(State state, Date time, Serializable newValue,
		SimulationRun simRun, Epoch epoch)
	throws
		ParameterError
	{
		super(state, time, newValue);
		
		this.id = simRun.createNewEventId();
		this.simRun = simRun;
		this.epoch = epoch;
	}
	
	
	protected SimulationTimeEventImpl(State state, Date time, Serializable newValue,
		SimulationRun simRun)
	throws
		ParameterError
	{
		this(state, time, newValue, simRun, null);
	}


	public int getNoOfHeaderRows() { return 2 + super.getNoOfHeaderRows(); }
	
	
	public Object clone() throws CloneNotSupportedException
	{
		SimulationTimeEventImpl newEvent = (SimulationTimeEventImpl)(super.clone());

		newEvent.newValue = PersistentNodeImpl.copyObject(newValue);

		return newEvent;
	}
	
	
	public String encodeAsString()
	throws
		IOException
	{
		SimulationTimeEventSer eventSer;
		try { eventSer = (SimulationTimeEventSer)(this.externalize()); }
		catch (ModelContainsError pe) { throw new IOException(pe); }
		
		return EventArchiver.encodeAsString(0, null, eventSer, true, true, true);
	}


	public String toString()
	{
		return
			super.toString() +
			", id=" + id +
			(simRun == null? "simRun=null" : (", simRun id=" + simRun.getNodeId())) +
			", epoch=" + (epoch == null? "null" : epoch.toString())
			;
	}
}

