/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.IOException;


public class DecisionFunctionImpl extends ParameterImpl implements DecisionFunction
{
	private Set<Variable> variableRefs = new TreeSetNullDisallowed<Variable>();


	public DecisionFunctionImpl(String name, DecisionPoint dp)
	throws
		ParameterError
	{
		super(name, dp);
	}


	public Class getSerClass() { return DecisionFunctionSer.class; }
	
	
	public String getTagName() { return "decision_function"; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((DecisionFunctionSer)nodeSer).variableRefNodeIds = createNodeIdArray(variableRefs);
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DecisionFunctionImpl newInstance = (DecisionFunctionImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.variableRefs = cloneNodeSet(variableRefs, cloneMap, newInstance);
		
		return newInstance;
	}


	public void setIconImageToDefault() throws IOException { setIconImage(NodeIconImageNames.DecisionFunctionIconImageName); }

	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		for (Variable v : variableRefs) v.decisionFunctionDeleted(this);
		
		// Nullify all references.
		variableRefs = null;
	}
	
	
	public String setNameAndNotify(String newName)
	throws
		ParameterError
	{
		String oldName = super.setNameAndNotify(newName);
		
		for (Variable v : variableRefs) v.decisionFunctionNameChanged(this, oldName);
		
		return oldName;
	}
	

	public void addVariableRef(Variable variable)
	{
		variableRefs.add(variable);
		((VariableImpl)variable).addReferencedBy(this);
	}
	
		
	public void removeVariableRef(Variable variable)
	{
		VariableImpl v = (VariableImpl)variable;
		((VariableImpl)v).removeReferencedBy(this);
		variableRefs.remove(v);
	}
	
		
	public Set<Variable> getVariableRefs()
	{
		return variableRefs;
	}
	
	
	public void variableDeleted(Variable variable)
	{
		variableRefs.remove(variable);
	}


	public void variableNameChanged(Variable var, String oldName)
	{
	}	


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
	}
}
