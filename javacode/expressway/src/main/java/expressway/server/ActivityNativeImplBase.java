/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.MathException;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class ActivityNativeImplBase implements Activity.JavaActivityImplementation
{
	private ActivityContext context = null;
	private ModelComponent modelComponent = null;
		// Should identical to context.getComponent().


	public void start(ActivityContext context) throws Exception
	{
		this.context = context;
		
		if (watch())
		{
			notifyAllListeners(new PeerNoticeBase.SimActivityStartNotice(getActivity().getNodeId()));
		}
	}


	public void stop()
	{
		if (watch())
		{
			notifyAllListeners(new PeerNoticeBase.SimActivityStopNotice(getActivity().getNodeId()));
		}
	}


	public SortedEventSet<GeneratedEvent> getAndPurgeNewEvents()
	{
		return context.getAndPurgeNewEvents();
	}


	public void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		if (watch())
		{
			notifyAllListeners(new PeerNoticeBase.SimActivityRespondingNotice(getActivity().getNodeId()));
		}
	}


	public ActivityBase getActivity() { return (ActivityBase)(context.getComponent()); }
	
	public ModelComponent getModelComponent() { return modelComponent; }
	
	public void setModelComponent(ModelComponent comp) { this.modelComponent = comp; }
	
	public ModelContext getModelContext() { return context; }


	/**
	 * Return true of the owning component is current in "watch" mode, which
	 * requires that changes to its state and its actions are reported to
	 * the client.
	 */
	 
	protected boolean watch()
	{
		return getActivity().getWatch();
	}
	
	
	protected void notifyAllListeners(PeerNotice peerNotice)
	{
		try { getActivity().notifyAllListeners(peerNotice); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}


	protected ActivityContext getActivityContext() { return context; }


	/**
	 * Return the Events that are entering through the specified Port
	 * in this epoch.
	 */
	 
	protected SortedEventSet<GeneratedEvent> getEventsOnPort(Port port, 
		SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		SortedEventSet<GeneratedEvent> portEvents = new SortedEventSetImpl<GeneratedEvent>();
		
		for (GeneratedEvent event : events)
		{
			State state = event.getState();
			if (state == null) continue; // skip Startup events.
			
			Set<Port> connectedPorts = 
				getActivityContext().getSensitivePorts(state);
				//getActivityContext().getConnectedPorts(state);
			
			//if ((connectedPorts.contains(port)) && port.allowsInputEvents())
			if (connectedPorts.contains(port))
				portEvents.add(event);
		}
		
		return portEvents;
	}
	
	
	Serializable getStateValue(Port port)
	{
		return context.getStateValue(port);
	}
	
	
	/**
	 * If there is an Event on the specified Port in this epoch, return true.
	 */
	 
	protected boolean portHasEvent(SortedEventSet<GeneratedEvent> events, Port port)
	throws
		ModelContainsError
	{
		SortedEventSet<GeneratedEvent> portEvents = getEventsOnPort(port, events);
		
		return (portEvents.size() > 0);
	}


	/**
	 * Return the value that is currently driving each Conduit that is connected
	 * to the specified Port. If the Port is a "black" Port, then the values may
	 * conflict; otherwise they may not. The Set that is returned may contain null
	 * entries, indicating null State values.
	 */
	 
	protected Set<Serializable> readAllConduits(Port port)
	throws
		ModelContainsError,
		ParameterError
	{
		Set<Serializable> values = new HashSet<Serializable>();
		Set<Conduit> externalConduits = port.getExternalConnections();
		for (Conduit conduit : externalConduits)
		{
			Port otherPort;
			try
			{
				if (conduit.getPort1() == port) otherPort = conduit.getPort2();
				else otherPort = conduit.getPort1();
			}
			catch (ValueNotSet w) { throw new ModelContainsError("Dangling Conduit", w); }
			
			State state = context.getCurrentDriver(otherPort);
			values.add(context.getStateValue(state));
		}
		
		return values;
	}


	/**
	 * If there is an Event on the specified Port in this epoch, return true.
	 */
	 
	protected boolean portHasNonCompensatingEvent(SortedEventSet<GeneratedEvent> events, 
		Port port)
	throws
		ModelContainsError
	{
		SortedEventSet<GeneratedEvent> portEvents = getEventsOnPort(port, events);
		
		if (portEvents.size() == 0) return false;
		
		for (GeneratedEvent event : portEvents)
			if (! event.isCompensation()) return true;
		
		return false;
	}


	/**
	 * Retrieve the (String) value of the specified Attribute, in the context of
	 * the current Scenario that is being simulated.
	 */
	 
	protected String getAttributeStringValue(ModelAttribute attr)
	{
		ModelScenario scenario = getActivityContext().getSimulationRun().getModelScenario();
		Serializable value = null;
		try { value = scenario.getAttributeValue(attr); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		if (! (value instanceof String)) throw new RuntimeException(
			"Attribute " + attr.getFullName() + " is not a String: is " +
				(value == null ? "null" : value.toString() + " (" +
					value.getClass().getName() + ")"));
		
		return (String)value;
	}


	/**
	 * Retrieve the (double) value of the specified Attribute, in the context of
	 * the current Scenario that is being simulated.
	 */
	 
	protected boolean getAttributeBooleanValue(ModelAttribute attr)
	{
		ModelScenario scenario = getActivityContext().getSimulationRun().getModelScenario();
		Serializable value = null;
		try { value = scenario.getAttributeValue(attr); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		if (! (value instanceof Boolean)) throw new RuntimeException(
			"Attribute " + attr.getFullName() + " is not a boolean: is " +
				(value == null ? "null" : value.toString() + " (" +
					value.getClass().getName() + ")"));
		
		return ((Boolean)value).booleanValue();
	}


	/**
	 * Retrieve the (double) value of the specified Attribute, in the context of
	 * the current Scenario that is being simulated.
	 */
	 
	protected double getAttributeDoubleValue(ModelAttribute attr)
	{
		ModelScenario scenario = getActivityContext().getSimulationRun().getModelScenario();
		Serializable value = null;
		try { value = scenario.getAttributeValue(attr); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		if (! (value instanceof Number)) throw new RuntimeException(
			"Attribute " + attr.getFullName() + " is not a Number: is " +
				(value == null ? "null" : value.toString() + " (" +
					value.getClass().getName() + ")"));
		
		return ((Number)value).doubleValue();
	}


	/**
	 * Retrieve the (long) value of the specified Attribute, in the context of
	 * the current Scenario that is being simulated.
	 */
	 
	protected long getAttributeLongValue(ModelAttribute attr)
	{
		ModelScenario scenario = getActivityContext().getSimulationRun().getModelScenario();
		Serializable value = null;
		try { value = scenario.getAttributeValue(attr); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		if (! (value instanceof Number)) throw new RuntimeException(
			"Attribute " + attr.getFullName() + " is not a Number: is " +
				(value == null ? "null" : value.toString() + " (" +
					value.getClass().getName() + ")"));
		
		return ((Number)value).longValue();
	}
}
