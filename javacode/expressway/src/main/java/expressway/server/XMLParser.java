/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import expressway.server.DecisionElement.*;
import expressway.server.NamedReference.*;
import expressway.common.ClientModel.PeerListener;
import expressway.ser.*;
import expressway.server.Event.*;
import expressway.awt.AWTTools;
import expressway.generalpurpose.ClassNameParser;
import expressway.generalpurpose.ThrowableUtil;
import expressway.generalpurpose.DateAndTimeUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Attr;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.apache.xerces.parsers.DOMParser;
import java.text.ParseException;
import java.util.Date;
import java.util.Vector;
import java.util.List;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.io.File;
import java.io.PrintWriter;
import java.io.PrintStream;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.ObjectOutputStream;
import java.io.CharArrayReader;
import java.io.Reader;
import java.io.IOException;
import java.io.File;
import java.io.Serializable;
import java.text.NumberFormat;
import java.text.DateFormat;
import java.net.URLClassLoader;
import java.net.URI;
import java.net.URL;
import java.net.MalformedURLException;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.awt.Image;
import javax.swing.ImageIcon;


/**
 * Parse an XML file defining one or more Domains and build those Domains.
 */
 
public class XMLParser implements ErrorHandler
{
	private Document document = null;
	private ModelEngineLocal modelEngine = null;
	private String clientId = null;
	private PeerListener peerListener = null;
	private DOMParser parser = null;
	private Map<String, Integer> taskHandles = new HashMap<String, Integer>();
	private Map<String, String> definitions = new HashMap<String, String>();
	private List<String> warnings = new Vector<String>();
	private List<PersistentNode> rootNodes = new Vector<PersistentNode>();

	private String globalSourceName;
	private String globalSourceURLStr;
	private String globalVersion;
	
	/** If false, a colliding name generates an Exception. If true, a colliding
		name is modified so that it is unique and the creation of the Domain is
		allowed. */
	private boolean collidingDomainNameGeneratesException = true;
	
	
	/** When the ModelEngine rejects a parsed Domain name and replaces it with a different
		name, the mapping between the parsed name and the new name is maintained here. 
		The mapping must be applied wherever a Domain name is recognized, including
		in Node name paths. */
	private Map<String, String> domainNameSubstitutions = new HashMap<String, String>();
	
	
	public XMLParser()
	{
		parser = new DOMParser();
		
		try
		{
			parser.setFeature( "http://xml.org/sax/features/validation", false);
		}
		catch (SAXException e)
		{
			System.err.println("error in setting up parser feature");
		}

		parser.setErrorHandler(this);
	}
	
	
	protected Document getDocument() { return document; }
	protected ModelEngineLocal getModelEngineLocal() { return modelEngine; }
	protected String getClientId() { return clientId; }
	protected DOMParser getDOMParser() { return parser; }
	protected void addRootNode(PersistentNode node)
	{
		if (node == null) throw new RuntimeException(
			"Attempt to add null root node");
		
		rootNodes.add(node);
	}
	
	
	public Map<String, Integer> getTaskHandles() { return taskHandles; }
	public List<PersistentNode> getRootNodesCreated() { return rootNodes; }


	/** Does not ever return null. */
	public List<String> getWarnings() { return warnings; }
	
	
	protected void addWarning(String msg) { warnings.add(msg); }
	
	
	protected void addWarning(Element element, String msg)
	{
		warnings.add("At element <" + element.getTagName() + 
			(element.getAttribute("name").equals("") ? "" :
				(" name=\"" + element.getAttribute("name") + "\"")) + 
			">, " +
			msg);
	}
	
	
	public synchronized void setModelEngineLocal(ModelEngineLocal me)
	{
		this.modelEngine = me;
	}
	
	
	public synchronized void setClientId(String clientId) { this.clientId = clientId; }
	
	
	public synchronized void setPeerListener(PeerListener peerListener)
	{
		this.peerListener = peerListener;
	}
	
	
	public synchronized void setCollidingDomainNameGeneratesException(boolean yes)
	{
		this.collidingDomainNameGeneratesException = yes;
	}
	
	public synchronized boolean getCollidingDomainNameGeneratesException()
	{
		return this.collidingDomainNameGeneratesException;
	}
	
	
	/**
	 * Parse the specified URI and create a Document.
	 */

	public synchronized void parse(String uri)
	throws
		SAXException,
		IOException
	{
		getDOMParser().parse(uri);
		this.document = getDOMParser().getDocument();
	}


	public synchronized void parse(char[] charAr)
	throws
		SAXException,
		IOException
	{
		Reader charReader = new CharArrayReader(charAr);
		getDOMParser().parse(new InputSource(charReader));
		this.document = getDOMParser().getDocument();
	}
	

	public synchronized void parse(File file)
	throws
		SAXException,
		IOException
	{
		Reader fileReader = new FileReader(file);
		getDOMParser().parse(new InputSource(fileReader));
		this.document = getDOMParser().getDocument();
	}
	

	public synchronized void setFeature(String featureId, boolean state)
	throws 
		SAXNotRecognizedException,
		SAXNotSupportedException
	{
		parser.setFeature( featureId, state );
	}


	public synchronized void genDocument()
	throws GenException
	{
		Element rootElement = document.getDocumentElement();
			// Should be an <expressway> element.
		
		String tagName = rootElement.getTagName();
		if (tagName.equals("expressway")) genExpressway(rootElement);
		else throw new GenException("Expected an <expressway> tag.");
	}
	
	
	/*
	 * Define a generation method for each concrete PersistentNode type.
	 */


	protected void genPersistentNode(Element element, PersistentNode thisNode)
	throws GenException
	{
		// Generate HTML description.
		
		NodeList nodes = getElements(element, "description");
		if (nodes.getLength() > 1) throw new GenException(element,
			"Multiple descriptions");
		
		String htmlDescription = null;
		for (int i = 0; i < nodes.getLength(); i++)
		{
			htmlDescription = getNodeTreeAsText(nodes.item(i));
			break;
		}
		
		if (htmlDescription != null)
			thisNode.setHTMLDescription(htmlDescription);
		
		setViewClassName(element, thisNode);
		
		// Generate Attributes.
		
		nodes = getElements(element, "attribute");
		for (int i = 0; i < nodes.getLength(); i++)
			// each <attribute> tag of the <element>,
		{
			Node node = nodes.item(i);
			//Attribute a = 
				genAttribute((Element)node, thisNode);
		}
		
		nodes = getElements(element, "reference_creation_handler");
		if ((nodes.getLength() > 0) && (! (thisNode instanceof CrossReferenceable)))
			throw new GenException(element,
				"Node containing reference listener tag is not a CrossReferenceable");
		
		for (int i = 0; i < nodes.getLength(); i++)
		{
			Node node = nodes.item(i);
			genReferenceCreationListener((Element)node, (CrossReferenceable)thisNode);
		}
		
		nodes = getElements(element, "reference_deletion_handler");
		if ((nodes.getLength() > 0) && (! (thisNode instanceof CrossReferenceable)))
			throw new GenException(element,
				"Node containing reference listener tag is not a CrossReferenceable");
		
		for (int i = 0; i < nodes.getLength(); i++)
		{
			Node node = nodes.item(i);
			genReferenceDeletionListener((Element)node, (CrossReferenceable)thisNode);
		}

		String xString = translateAttribute(element.getAttribute("x"));
		String yString = translateAttribute(element.getAttribute("y"));
		String widthString = translateAttribute(element.getAttribute("width"));
		String heightString = translateAttribute(element.getAttribute("height"));
		String scaleString = translateAttribute(element.getAttribute("scale"));
		String orientationString = translateAttribute(element.getAttribute("orientation"));
		String deletableString = translateAttribute(element.getAttribute("deletable"));

		double x = 0.0;
		double y = 0.0;
		double width = 0.0;
		double height = 0.0;
		double scale = 1.0;
		double orientation = 0.0;
		boolean deletable = true;

		String valueString = translateAttribute(element.getAttribute("x"));
		if (! valueString.equals(""))
			try { x = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element, 
				thisNode.getFullName(),
				"Parameter '" + valueString + "' must be a double"); }
		
		valueString = translateAttribute(element.getAttribute("y"));
		if (! valueString.equals(""))
			try { y = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element, 
				"Parameter '" + valueString + "' must be a double"); }
		
		valueString = translateAttribute(element.getAttribute("width"));
		if (! valueString.equals(""))
			try { width = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element, 
				"Parameter '" + valueString + "' must be a double"); }

		valueString = translateAttribute(element.getAttribute("height"));
		if (! valueString.equals(""))
			try { height = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element, 
				"Parameter '" + valueString + "' must be a double"); }
		
		valueString = translateAttribute(element.getAttribute("scale"));
		if (! valueString.equals(""))
			try { scale = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element, 
				"Parameter '" + valueString + "' must be a double"); }
		
		valueString = translateAttribute(element.getAttribute("orientation"));
		if (! valueString.equals(""))
			try { orientation = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element, 
				"Parameter '" + valueString + "' must be a double"); }

		valueString = translateAttribute(element.getAttribute("deletable"));
		if (! valueString.equals(""))
			try { deletable = Boolean.parseBoolean(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element, 
				"Parameter '" + valueString + "' must be a boolean"); }

		
		if (! widthString.equals("")) thisNode.setWidth(width);
		if (! heightString.equals("")) thisNode.setHeight(height);
		if (! heightString.equals("")) thisNode.setScale(scale);
		if (! heightString.equals("")) thisNode.setOrientation(orientation);
		thisNode.setDeletable(deletable);
		if ((! (thisNode instanceof Conduit)) 
			&&
			(! (thisNode instanceof Port))
			&&
			(! (thisNode instanceof ModelDomain)))
		{
			thisNode.setX(x);
			thisNode.setY(y);
		}
	}
	
	
	protected void genModelElement(Element element, ModelElement thisModelElement)
	throws GenException
	{
		genPersistentNode(element, thisModelElement);
		thisModelElement.conditionalLayout();
	}
	

	protected void genHierarchyElement(Element element, HierarchyElement thisHierarchyElt)
	throws GenException
	{
		genPersistentNode(element, thisHierarchyElt);
		thisHierarchyElt.conditionalLayout();
	}
	

	protected void genDecisionElement(Element element, DecisionElement thisDecElt)
	throws GenException
	{
		genPersistentNode(element, thisDecElt);
		thisDecElt.conditionalLayout();
	}
	
	
	protected Attribute genAttribute(Element element, PersistentNode parent)
	throws GenException
	{
		if (parent instanceof ModelElement)
			return genModelAttribute(element, (ModelElement)parent);
		else if (parent instanceof Hierarchy)
			return genHierarchyAttribute(element, (Hierarchy)parent);
		else if (parent instanceof DecisionElement)
			return genDecisionAttribute(element, (DecisionElement)parent);
		else throw new RuntimeException("Unexpected Node type");
	}
	
	
	protected Attribute genAttribute_Common(Element element, PersistentNode parent)
	throws GenException
	{
		String name = element.getAttribute("name");
		if (name.equals("")) name = null;
		
		Class defaultClass = null;
		String typeName = element.getAttribute("default_type");
		if (typeName.equals("")) typeName = null;
		else try
		{
			defaultClass = ClassNameParser.recognizeQualifiedClassName(typeName);
		}
		catch (ClassNotFoundException cnfe)
		{
			throw new GenException(element, parent.getFullName(), cnfe);
		}
		
		Serializable defaultValue = null;
		String defaultValueString = element.getAttribute("default");
		
		String forValueString = element.getAttribute("for");
		/* The specified element must be a pre-defined element within 'parent'. */

		if (defaultClass == null)
			defaultValue = getValueFromString(defaultValueString);
		else
			// Get the fields of the default value, which implements the
			// specified interface or class.
		{
			// Create new instance of the default Class. This instance is a
			// container for the field values that are set below.
			
			/*
			If a default value is specified, attempt to use its String representation
			in the constructor; if this fails, attempt with a zero-argument constructor.
			Note that only the Character type does not have a Constructor with a
			String argument: therefore, if a Character, use the Character(char)
			constructor.
			*/
			
			if ((defaultValueString != null) && (! (defaultValueString.equals(""))))
			{
				// Default value was specified, so assume the constructor requries
				// a value.
				
				if (defaultClass == java.lang.Character.class)
				{
					// Attempt to convert the default value to a character.
					if (defaultValueString.length() != 1) throw new GenException(
						element, parent.getFullName(),
						"Default value '" + defaultValueString + 
						"' cannot be used for a Character-valued Attribute.");
					
					defaultValue = new Character(defaultValueString.charAt(0));
				}
				else  // not a Character
				{
					try
					{
						// Instantiate with String-argument constructor.
						java.lang.reflect.Constructor constructor = 
							defaultClass.getConstructor(String.class);
						
						defaultValue = (Serializable)(constructor.newInstance(defaultValueString));
					}
					catch (Exception ex)
					{
						try
						{
							// Instantiate with zero-argument constructor.
							defaultValue = (Serializable)(defaultClass.newInstance());
						}
						catch (Exception ex2) { throw new GenException(
							element, parent.getFullName(),
							"Unable to instantiate class " + defaultClass.getName()); }
					}
				}
			}
			else try // no default value was specified: use a zero-argument constructor.
			{
				defaultValue = (Serializable)(defaultClass.newInstance());
			}
			catch (Exception ex) { throw new GenException(element, parent.getFullName(),
				"Unable to instantiate class " + defaultClass.getName()); }
				
			// Check that default value class is Serializable. It must be, in
			// order to be able to be sent to the client in an ModelAttributeSer.
			
			if (! (defaultValue instanceof Serializable)) throw new GenException(
				element, parent.getFullName(),
				"Class " + defaultClass.getName() + " is not Serializable.");
			
			// Set field values in the new default Class instance.
			
			NodeList fields = getElements(element, "field");
			for (int i = 0; i < fields.getLength(); i++)
			{
				Element fldElt = (Element)fields.item(i);
				String fieldName = fldElt.getAttribute("name");
				String fieldValueString = fldElt.getAttribute("value");
				
				Serializable fieldValue = getValueFromString(fieldValueString);
				
				// The interface identified by typeName must define a get and
				// set method for this field. Use the set method to set the
				// value in the default value object.
				
				String setAccessorName = "set" + 
					fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
					
				Method method = null;
				try { method = defaultClass.getMethod(setAccessorName, fieldValue.getClass()); }
				catch (NoSuchMethodException nsme) { throw new GenException(
					element, parent.getFullName(),
					"Class " + typeName + " does not have a " + setAccessorName); }
					
				try { method.invoke(defaultValue, fieldValue); }
				catch (Exception ex) { throw new GenException(element, parent.getFullName(),
					"When invoking " + setAccessorName + 
					" for instance of class " + typeName, ex); }
			}
		}
		
		Attribute attribute = null;
		if ((forValueString == null) || forValueString.equals(""))
			try { attribute = parent.createAttribute(name, defaultValue, 
				parent, null); }
			catch (ParameterError pe) { throw new GenException(element, parent.getFullName(),
				pe); }
		else
		{
			// Identify the 'for' Node. At this point, 'parent' has been
			// fully elaborated, so we can walk its tree. The 'for' Node is
			// any Node with the name specified by 'forValueString', nested
			// at any level within 'parent'.
			
			PersistentNode forNode = null;
			try { forNode = parent.findFirstNestedNode(forValueString); }
			catch (ElementNotFound enf)
			{
				throw new GenException(element, parent.getFullName(),
					"Node '" + forValueString + "' in 'for' clause not found");
			}
			
			// Attach the Attribute to it.
			
			try
			{
				attribute = forNode.createAttribute(name, defaultValue, forNode, null);
				forNode.layout();
			}
			catch (ParameterError pe)
			{
				throw new GenException(element, parent.getFullName(), pe);
			}
		}
		
		return attribute;
	}
	
	
	protected Attribute genModelAttribute(Element element, ModelElement parent)
	throws GenException
	{
		ModelAttribute attribute = (ModelAttribute)(genAttribute_Common(element, parent));
		genModelElement(element, attribute);
		

		// Generate Attribute-State bindings.
		
		NodeList nodes = getElements(element, "state_binding");
		if (nodes.getLength() > 1) throw new GenException(element, parent.getFullName(),
			"At most one <state_binding> allowed per <attribute>");
		for (int i = 0; i < nodes.getLength(); i++)
			genAttributeStateBinding((Element)(nodes.item(i)), attribute);
		
		parent.layout();
		
		return attribute;
	}
	
	
	protected Attribute genHierarchyAttribute(Element element, Hierarchy parent)
	throws GenException
	{
		HierarchyAttribute attribute = 
			(HierarchyAttribute)(genAttribute_Common(element, parent));
		genHierarchyElement(element, attribute);
		parent.layout();
		return attribute;
	}


	protected Attribute genDecisionAttribute(Element element, DecisionElement parent)
	throws GenException
	{
		DecisionAttribute attribute = 
			(DecisionAttribute)(genAttribute_Common(element, parent));
		genDecisionElement(element, attribute);
		parent.layout();
		return attribute;
	}
	
	
	protected void genExpressway(Element element)
	throws GenException
	{
		globalSourceName = translateAttribute(element.getAttribute("source_name"));
		if (globalSourceName.equals("")) globalSourceName = null;
		
		globalSourceURLStr = translateAttribute(element.getAttribute("source_url"));
		if (globalSourceURLStr.equals("")) globalSourceURLStr = null;
		
		globalVersion = translateAttribute(element.getAttribute("version"));
		if (globalVersion.equals("")) globalVersion = null;

		NodeList nodes = null;
		int noOfMotifs = 0;
		

		// Get child elements.
		
		nodes = getElements(element, "define");
		for (int i = 0; i < nodes.getLength(); i++)
			genDefine((Element)(nodes.item(i)));
		
		nodes = getElements(element, "import");
		for (int i = 0; i < nodes.getLength(); i++)
			genImport((Element)(nodes.item(i)));
		
		nodes = getElements(element, "domain_type");
		for (int i = 0; i < nodes.getLength(); i++)
			genDomainType((Element)(nodes.item(i)));
		
		nodes = getElements(element, "model_motif");
		noOfMotifs += nodes.getLength();
		if (noOfMotifs > 1) throw new GenException(element,
			"Only one motif may be defined in an XML file.");
		for (int i = 0; i < nodes.getLength(); i++)
		{
			ModelDomain motifDef = genModelDomain((Element)(nodes.item(i)), true);
			addRootNode(motifDef);
		}
		
		nodes = getElements(element, "model_domain");
		for (int i = 0; i < nodes.getLength(); i++)
		{
			ModelDomain modelDomain = genModelDomain((Element)(nodes.item(i)), false);
			addRootNode(modelDomain);
		}
		
		nodes = getElements(element, "hierarchy_domain");
		for (int i = 0; i < nodes.getLength(); i++)
		{
			HierarchyDomain hd = genHierarchyDomain((Element)(nodes.item(i)), false);
			addRootNode(hd);
		}
		
		nodes = getElements(element, "hierarchy_motif");
		noOfMotifs += nodes.getLength();
		if (noOfMotifs > 1) throw new GenException(element,
			"Only one motif may be defined in an XML file.");
		for (int i = 0; i < nodes.getLength(); i++)
		{
			HierarchyDomain rd = genHierarchyDomain((Element)(nodes.item(i)), true);
			addRootNode(rd);
		}
		
		nodes = getElements(element, "decision_domain");
		for (int i = 0; i < nodes.getLength(); i++)
		{
			DecisionDomain dd = genDecisionDomain((Element)(nodes.item(i)), false);
			addRootNode(dd);
		}
		
		nodes = getElements(element, "decision_motif");
		noOfMotifs += nodes.getLength();
		if (noOfMotifs > 1) throw new GenException(element,
			"Only one motif may be defined in an XML file.");
		for (int i = 0; i < nodes.getLength(); i++)
		{
			DecisionDomain dd = genDecisionDomain((Element)(nodes.item(i)), true);
			addRootNode(dd);
		}
		
		nodes = getElements(element, "scenario");
		for (int i = 0; i < nodes.getLength(); i++)
			genScenario((Element)(nodes.item(i)));
		
		nodes = getElements(element, "scenario_set");
		for (int i = 0; i < nodes.getLength(); i++)
			genScenarioSet((Element)(nodes.item(i)));
		
		nodes = getElements(element, "scenario");
		for (int i = 0; i < nodes.getLength(); i++)
			genScenario_sets((Element)(nodes.item(i)));
		
		if (! getClientId().equals("StandaloneClient")) return;

		
		// The elements below are only processed by the standalone program.
		
		nodes = getElements(element, "simulate");
		for (int i = 0; i < nodes.getLength(); i++)
			genSimulate((Element)(nodes.item(i)));
		
		nodes = getElements(element, "update_variable");
		for (int i = 0; i < nodes.getLength(); i++)
			genUpdateVariable((Element)(nodes.item(i)));
		
		nodes = getElements(element, "export_events");
		for (int i = 0; i < nodes.getLength(); i++)
			genExportEvents((Element)(nodes.item(i)));
		
		nodes = getElements(element, "archive_simulation");
		for (int i = 0; i < nodes.getLength(); i++)
			genArchiveSimulation((Element)(nodes.item(i)));
		
		nodes = getElements(element, "restore_simulation");
		for (int i = 0; i < nodes.getLength(); i++)
			genRestoreSimulation((Element)(nodes.item(i)));
		
		nodes = getElements(element, "export_final_states");
		for (int i = 0; i < nodes.getLength(); i++)
			genExportFinalStates((Element)(nodes.item(i)));
		
		nodes = getElements(element, "export_stats");
		for (int i = 0; i < nodes.getLength(); i++)
			genExportStats((Element)(nodes.item(i)));
		
		nodes = getElements(element, "export_histograms");
		for (int i = 0; i < nodes.getLength(); i++)
			genExportHistograms((Element)(nodes.item(i)));
		
		nodes = getElements(element, "export_correlations");
		for (int i = 0; i < nodes.getLength(); i++)
			genExportCorrelations((Element)(nodes.item(i)));
	}
	
	
	protected void genDefine(Element element)
	throws GenException
	{
		String name = element.getAttribute("name");
		if (name.equals("")) throw new GenException(element,
			"No name attribute specified for define");
		
		String value = translateAttribute(element.getAttribute("value"));
		if (value.equals("")) addWarning(
			"Warning: value for define '" + name + "' is blank");
		
		String priorValue = definitions.get(name);
		if ((priorValue != null) && (! value.equals(priorValue)))
			addWarning("Warning: replacing value of definition '" + name + "'");
		
		definitions.put(name, value);
	}
	
	
	protected void genImport(Element element)
	throws GenException
	{
		String urlString = element.getAttribute("url");
		if (urlString.equals("")) urlString = null;
		
		String pathString = element.getAttribute("path");
		if (pathString.equals("")) pathString = null;
		
		if ((urlString == null) && (pathString == null)) throw new GenException(
			element, "Neither url nor path specified");
		
		if ((urlString != null) && (pathString != null)) throw new GenException(
			element, "One one of url or path may be specified");
		
		XMLParser importParser = new XMLParser();

		importParser.modelEngine = this.modelEngine;
		importParser.clientId = this.clientId;
		importParser.warnings = this.warnings;
		importParser.definitions = this.definitions;
		importParser.taskHandles = this.taskHandles;
		importParser.peerListener = this.peerListener;

		try
		{
			if (urlString != null)
				importParser.parse(urlString);
			else if (pathString != null)
				importParser.parse(new File(pathString));
		}
		catch (Exception ex) { throw new GenException(element, ex); }
		
		importParser.genDocument();
	}
	
	
	/**
	 * Identify any definition references embedded in the specified attribute
	 * value and replace them with the value of the referenced definition.
	 */
	 
	protected String translateAttribute(String value)
	throws GenException
	{
		// Recognize occurrences of "$" followed by "("...")".
		// Scan forward; if a "$" found and it is followed by a "(", assume a 
		// reference has been found. At that point, scan forward to find the end
		// of the reference. Once the definition name has been identified, replace
		// the occurence of the reference with the definition's value.
		
		String translatedValue = value;  // initial value.
		
		int posInTranslatedValue = 0;
		for (;;)
		{
			// Find next occurrence of "$", if any.
			
			int dollarPosInTranslatedValue = translatedValue.indexOf('$', posInTranslatedValue);
			if (dollarPosInTranslatedValue == -1) break;  // done.
			
			
			// Found a dollar sign. Now find the start and end of the definition name.
			
			if (translatedValue.length() < (dollarPosInTranslatedValue+2)) break;  // done.
			
			char nextChar = value.charAt(dollarPosInTranslatedValue+1);
			
			if (nextChar != '(')  // not a def name
			{
				posInTranslatedValue = dollarPosInTranslatedValue + 1;
				continue;
			}
				
			String defName = null;
			
			
			// Find the following ')'
			
			int defNameStart = dollarPosInTranslatedValue + 2;
			int defNameEnd = defNameStart - 1;  // inclusive.
			
			int terminatingParenPos = translatedValue.indexOf(')', defNameStart);  // inclusive
			if (terminatingParenPos == -1) throw new GenException(
				"Unterminated definition reference found when translating '" + value + "'");
			
			defName = translatedValue.substring(defNameStart, terminatingParenPos);
			if (defName.length() == 0) throw new GenException(
				"Zero length definition name reference found when translating '" + value + "'");
			
			// Replace the reference with the definition value.
			
			String replacementValue = definitions.get(defName);
			if (replacementValue == null) throw new GenException(
				"A definition for reference '" + defName + "' does not exist");
			
			translatedValue = translatedValue.substring(0, dollarPosInTranslatedValue) +
				replacementValue + translatedValue.substring(terminatingParenPos+1);
			
			posInTranslatedValue = dollarPosInTranslatedValue + replacementValue.length();
		}
		
		return translatedValue;
	}
	
	
	/**
	 * The 'path' argument is the path of a Node in a ModelDomain. Decompose the
	 * path and perform any translations or substitutions, including Domain name
	 * substitutions. Re-assemble the translated path and return it.
	 */
	 
	protected String translateNodePath(String path)
	throws
		GenException
	{
		String[] parts = path.split("\\.");
		String newPath = "";
		boolean firstTime = true;
		for (String part : parts)
		{
			String partTrans = translateAttribute(part);

			if (firstTime)
			{
				String sub = domainNameSubstitutions.get(partTrans);
				
				if (sub != null) newPath += sub;
				else newPath += partTrans;
				
				firstTime = false;
			}
			else
				newPath += "." + partTrans;
		}
		
		return newPath;
	}
	
	
	protected NamedReference genNamedReference(Element element, Domain parent)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element,
			"<references> tag without a name");
		
		String fromTypeName = translateAttribute(element.getAttribute("from_type"));
		if (fromTypeName.equals("")) throw new GenException(element,
			"<references> without a from_type");
		
		String toTypeName = translateAttribute(element.getAttribute("to_type"));
		if (toTypeName.equals("")) throw new GenException(element,
			"<references> without a to_type");
		
		NodeList nodes = getElements(element, "description");
		if (nodes.getLength() > 1) throw new GenException(element,
			"Multiple descriptions");
		
		String htmlDescription = null;
		for (int i = 0; i < nodes.getLength(); i++)
		{
			htmlDescription = getNodeTreeAsText(nodes.item(i));
			break;
		}
		
		Class fromType;
		Class toType;
		try { fromType = Class.forName(fromTypeName); }
		catch (ClassNotFoundException ex) { throw new GenException(element,
				"Unrecognized class name: '" + fromTypeName + "'."); }
		try { toType = Class.forName(toTypeName); }
		catch (ClassNotFoundException ex) { throw new GenException(element,
				"Unrecognized class name: '" + toTypeName + "'."); }
		
		NamedReference namedRef;
		try { namedRef = parent.createNamedReference(name, htmlDescription,
			fromType, toType); }
		catch (ParameterError pe) { throw new GenException(element, pe); }
		
		NodeList descriptions = getElements(element, "description");
		NodeList references = getElements(element, "reference");
		
		for (int i = 0; i < references.getLength(); i++)
			genCrossReference((Element)(references.item(i)), namedRef);
		
		genPersistentNode(element, namedRef);
		
		return namedRef;
	}
	
	
	protected CrossReference genCrossReference(Element element, NamedReference parent)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) name = null;
		
		String fromPath = translateAttribute(element.getAttribute("from"));
		if (fromPath.equals("")) throw new GenException(element,
			"<reference> without a from");
		
		String toPath = translateAttribute(element.getAttribute("to"));
		if (toPath.equals("")) throw new GenException(element,
			"<reference> without a to");
		
		PersistentNode fromNode;
		try { fromNode = parent.findNode(fromPath); }
		catch (ParameterError pe) { throw new GenException(element, pe); }
		if (fromNode == null) throw new GenException(element, "Not found: " + fromPath);
		if (! (fromNode instanceof CrossReferenceable)) throw new GenException(
			fromNode.getFullName() + " is not a CrossReferenceable Node");
		CrossReferenceable fromCr = (CrossReferenceable)fromNode;

		PersistentNode toNode;
		try { toNode = parent.findNode(toPath); }
		catch (ParameterError pe) { throw new GenException(element, pe); }
		if (toNode == null) throw new GenException(element, "Not found: " + toPath);
		if (! (toNode instanceof CrossReferenceable)) throw new GenException(
			toNode.getFullName() + " is not a CrossReferenceable Node");
		CrossReferenceable toCr = (CrossReferenceable)toNode;
		
		CrossReference cr;
		try { cr = parent.link(fromCr, toCr); }
		catch (ParameterError pe) { throw new GenException(element, pe); }
		
		genPersistentNode(element, cr);
		
		return cr;
	}
	
	
	protected CrossReferenceCreationListener genReferenceCreationListener(
		Element element, CrossReferenceable crAble)
	throws
		GenException
	{
		// Instantiate a new Listener that delegates to the user-provided method.
		
		Method handlerMethod = getReferenceHandlerMethod(element, crAble);
		Class handlerClass = handlerMethod.getClass();
		Object handler;
		try { handler = handlerClass.newInstance(); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		CrossReferenceCreationListener listener =
			new CrossReferenceCreationListenerImpl(handler, handlerMethod);
		
		// Add the new Listener to the CrossReferenceable.
		
		crAble.addCrossReferenceCreationListener(listener);
		
		return listener;
	}


	protected Method getReferenceHandlerMethod(Element element, CrossReferenceable crAble)
	throws
		GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		
		String referencesName = translateAttribute(element.getAttribute("references_name"));
		if (referencesName.equals("")) throw new GenException(element,
			"No 'reference_name' specified");
		
		String javaMethodName = translateAttribute(element.getAttribute("handler"));
		if(javaMethodName.equals("")) throw new GenException(element,
			"No 'handler' specified");
		
		// Determine the name of the motif-defined class for the CrossReferenceListener.
		
		String[] parts;
		int perPos = javaMethodName.lastIndexOf('.');
		if (perPos < 0) throw new GenException(element, "Java method name " + javaMethodName +
			" does not appear to have the format <class>.<method>");
						
		String className = javaMethodName.substring(0, perPos);

		// Load the motif-defined class for the user-provided handler method that
		// will handle CrossReference creation events.
		
		Class handlerClass = null;
		try
		{
			handlerClass = ServiceContext.getMotifClassLoader().loadClass(className);
		}
		catch (ClassNotFoundException ex) { throw new GenException(element, 
			"CrossReference handler class '" + className + "' not found", ex); }
		catch (Throwable t) { throw new GenException(element, t); }
		
		Method handlerMethod;
		try { handlerMethod = handlerClass.getMethod(javaMethodName); }
		catch (NoSuchMethodException ex) { throw new GenException(element, ex); }
		return handlerMethod;
	}
	
	
	public class CrossReferenceCreationListenerImpl implements CrossReferenceCreationListener
	{
		private final Object handler;
		private final Method handlerMethod;
		
		public CrossReferenceCreationListenerImpl(Object handler, Method handlerMethod)
		{
			this.handler = handler;
			this.handlerMethod = handlerMethod;
		}
		
		public void invokeHandler(CrossReferenceCreationContext context)
		throws
			Exception
		{
			try { handlerMethod.invoke(handler, context); }
			catch (InvocationTargetException ex)
			{
				Throwable t = ex.getTargetException();
				try { throw t; }
				catch (RuntimeException re) { throw (RuntimeException)t; }
				catch (Error e) { throw (Error)t; }
				catch (Throwable th) { throw (Exception)t; }
			}
		}
	}
	
	
	protected CrossReferenceDeletionListener genReferenceDeletionListener(
		Element element, CrossReferenceable crAble)
	throws
		GenException
	{
		// Instantiate a new Listener that delegates to the user-provided method.
		
		Method handlerMethod = getReferenceHandlerMethod(element, crAble);
		Class handlerClass = handlerMethod.getClass();
		Object handler;
		try { handler = handlerClass.newInstance(); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		CrossReferenceDeletionListener listener =
			new CrossReferenceDeletionListenerImpl(handler, handlerMethod);
		
		// Add the new Listener to the CrossReferenceable.
		
		crAble.addCrossReferenceDeletionListener(listener);
		
		return listener;
	}
	
	
	public class CrossReferenceDeletionListenerImpl implements CrossReferenceDeletionListener
	{
		private final Object handler;
		private final Method handlerMethod;
		
		public CrossReferenceDeletionListenerImpl(Object handler, Method handlerMethod)
		{
			this.handler = handler;
			this.handlerMethod = handlerMethod;
		}
		
		public void invokeHandler(CrossReferenceDeletionContext context)
		throws
			Exception
		{
			try { handlerMethod.invoke(handler, context); }
			catch (InvocationTargetException ex)
			{
				Throwable t = ex.getTargetException();
				try { throw t; }
				catch (RuntimeException re) { throw (RuntimeException)t; }
				catch (Error e) { throw (Error)t; }
				catch (Throwable th) { throw (Exception)t; }
			}
		}
	}
	
	
	void setViewClassName(Element element, PersistentNode node)
	throws
		GenException
	{
		String viewClassName = translateAttribute(element.getAttribute("view"));
		if (! viewClassName.equals("")) node.setViewClassName(viewClassName);
	}
	
	
	protected void genDomainType(Element element)
	throws GenException
	{
		String baseDomainTypeName = translateAttribute(element.getAttribute("base_domain_type"));
		if (baseDomainTypeName.equals("")) throw new GenException("No base_domain_type specified");
		
		DomainType baseDomainType = modelEngine.getDomainType(baseDomainTypeName);
		if (baseDomainType == null) throw new GenException("Domain type '" +
			baseDomainTypeName + "' not found");
		DomainType domainType = modelEngine.createDomainTypeNode(baseDomainType);

		NodeList subElements = getElements(element, "uses");

		for (int i = 0; i < subElements.getLength(); i++)
			genUsesMotif((Element)(subElements.item(i)), domainType);
		
		genPersistentNode(element, domainType);
	}
	
	
	protected void genUsesMotif(Element element, DomainType domainType)
	throws GenException
	{
		String motifName = translateAttribute(element.getAttribute("motif"));
		if (motifName.equals("")) throw new GenException(element,
			"A 'uses' tag without a motif name");
		
		MotifDef motifDef;
		try { motifDef = modelEngine.getMotifDefPersistentNode(motifName); }
		catch (Exception ex) { throw new GenException(element, ex); }
		if (motifDef == null) throw new GenException("Motif '" + motifName + "' not found");
		domainType.addDefaultMotif(motifDef);
	}

	
	protected ModelDomain genModelDomain(Element element, boolean isMotif)
	throws GenException
	{
		String domainName = translateAttribute(element.getAttribute("name"));
		if (domainName.equals("")) throw new GenException(element,
			"Model Domain without a name");
		
		String sourceName = translateAttribute(element.getAttribute("source_name"));
		if (sourceName.equals("")) sourceName = globalSourceName;
		
		String sourceURLStr = translateAttribute(element.getAttribute("source_url"));
		if (sourceURLStr.equals("")) sourceURLStr = globalSourceURLStr;
		
		URL sourceURL;
		if (sourceURLStr == null) sourceURL = null;
		else try { sourceURL = new URL(sourceURLStr); }
		catch (java.net.MalformedURLException ex) { throw new GenException(element,
			"source_url=\"" + sourceURLStr + "\"", ex); }
		
		String version = translateAttribute(element.getAttribute("version"));
		if (version.equals("")) version = globalVersion;
		
		String motifsStr = translateAttribute(element.getAttribute("motifs"));
		Set<String> motifNames = null;
		if (! motifsStr.equals(""))
		{
			String[] motifStrs = motifsStr.split(",");
			motifNames = new HashSet<String>();
			for (String motifStr : motifStrs) motifNames.add(translateAttribute(motifStr));
		}
		
		String stateBindingTypeStr = translateAttribute(element.getAttribute(
			"external_state_binding_type"));
		
		boolean stateBindingType;
		if (stateBindingTypeStr.equals(""))
			stateBindingType = true;
		else if (stateBindingTypeStr.equalsIgnoreCase("average"))
			stateBindingType = true;
		else if (stateBindingTypeStr.equalsIgnoreCase("actual"))
			stateBindingType = false;
		else
			throw new GenException(element, "Unrecognized state binding type: '" +
				stateBindingTypeStr + "'.");
		
		String stateBindingGranularityStr = translateAttribute(element.getAttribute(
			"external_state_time_granularity"));
		
		TimePeriodType stateBindingGranularity;
		if (stateBindingGranularityStr.equals(""))
			stateBindingGranularity = TimePeriodType.unspecified;
		else
			try { stateBindingGranularity = 
				Enum.valueOf(TimePeriodType.class, stateBindingGranularityStr); }
		catch (IllegalArgumentException ex) { throw new GenException(element,
			"unrecognized value for state binding granularity: '" +
			stateBindingGranularityStr + "'"); }

		NodeList menuItems = getElements(element, "menuitem");
		
		ModelDomain modelDomain = null;
		try
		{
			if (isMotif)
				modelDomain = modelEngine.createModelDomainMotifDefPersistentNode(domainName,
					getCollidingDomainNameGeneratesException());
			else
				modelDomain = modelEngine.createModelDomainPersistentNode(domainName,
					getCollidingDomainNameGeneratesException());
		}
		catch (Exception pe)
		{
			GlobalConsole.printStackTrace(pe);
			throw new GenException(element, pe);
		}
		
		modelDomain.setSourceName(sourceName);
		modelDomain.setSourceURL(sourceURL);
		modelDomain.setDeclaredName(domainName);
		modelDomain.setDeclaredVersion(version);

		// Set LayoutManager class (if specified).
		
		String layoutClassName = translateAttribute(element.getAttribute("layout"));
		if (layoutClassName.equals("")) layoutClassName = null;
		
		Class c = null;
		if (layoutClassName != null) try
		{
			c = ServiceContext.getMotifClassLoader().loadClass(layoutClassName);
		}
		catch (ClassNotFoundException ex) { throw new GenException(element, 
			"LayoutManager class not found", ex); }
		catch (Throwable t) { throw new GenException(element, t); }
		
		if (! LayoutManager.class.isAssignableFrom(c)) throw new GenException(
			element, "Class " + layoutClassName + " is not a LayoutManager");
		
		LayoutManager layoutManager;
		try { layoutManager = (LayoutManager)(c.newInstance()); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		modelDomain.setLayoutManager(layoutManager);
		modelDomain.conditionalLayout();
		
		modelDomain.setExternalStateBindingType(new Boolean(stateBindingType));
		modelDomain.setExternalStateGranularity(stateBindingGranularity);
		
		String actualDomainName = modelDomain.getName();
		if (! actualDomainName.equals(domainName))
		{
			domainNameSubstitutions.put(domainName, actualDomainName);
		}
		
		// Add to this Domain the motifs that this Domain claims to use.
		
		if (motifNames != null)
		{
			for (String motifName : motifNames)
			{
				MotifDef md;
				
				String substitutedName = domainNameSubstitutions.get(motifName);
				if (substitutedName != null) motifName = substitutedName;
				
				try { md = getModelEngineLocal().getMotifDefPersistentNode(motifName); }
				catch (ElementNotFound enf) { throw new GenException(
					element, "Motif '" + motifName + "' not found"); }
				catch (Exception ex) { throw new GenException(element, ex); }
				
				if (! (md instanceof ModelDomainMotifDef)) throw new GenException(
					"Motif '" + motifName + "' is not a ModelDomainMotifDef");
				
				modelDomain.addMotifDef(md);
			}
		}
		
		if (isMotif && (menuItems != null))
		{
			for (int i = 0; i < menuItems.getLength(); i++)
				genMenuItem((Element)(menuItems.item(i)), (MotifDef)modelDomain);
		}
		
		genModelElement(element, modelDomain);
		
		NodeList subElements = getElements(element, "activity");
		NodeList functions = getElements(element, "function");
		NodeList terminals = getElements(element, "terminal");
		NodeList generators = getElements(element, "generator");
		NodeList delays = getElements(element, "delay");
		NodeList sustains = getElements(element, "sustain");
		NodeList tallys = getElements(element, "tally");
		NodeList switches = getElements(element, "switch");
		NodeList oneways = getElements(element, "oneway");
		NodeList discriminators = getElements(element, "discriminator");
		NodeList modulators = getElements(element, "modulator");
		NodeList expressions = getElements(element, "expression");
		NodeList nots = getElements(element, "not");
		NodeList summations = getElements(element, "summation");
		NodeList conduits = getElements(element, "conduit");
		NodeList references = getElements(element, "references");

		for (int i = 0; i < subElements.getLength(); i++)
			genActivity((Element)(subElements.item(i)), modelDomain);
		
		for (int i = 0; i < functions.getLength(); i++)
			genFunction((Element)(functions.item(i)), modelDomain);

		for (int i = 0; i < terminals.getLength(); i++)
			genTerminal((Element)(terminals.item(i)), modelDomain);

		for (int i = 0; i < generators.getLength(); i++)
			genGenerator((Element)(generators.item(i)), modelDomain);

		for (int i = 0; i < delays.getLength(); i++)
			genDelay((Element)(delays.item(i)), modelDomain);

		for (int i = 0; i < sustains.getLength(); i++)
			genSustain((Element)(sustains.item(i)), modelDomain);

		for (int i = 0; i < tallys.getLength(); i++)
			genTally((Element)(tallys.item(i)), modelDomain);

		for (int i = 0; i < switches.getLength(); i++)
			genSwitch((Element)(switches.item(i)), modelDomain);

		for (int i = 0; i < oneways.getLength(); i++)
			genOneWay((Element)(oneways.item(i)), modelDomain);

		for (int i = 0; i < discriminators.getLength(); i++)
			genDiscriminator((Element)(discriminators.item(i)), modelDomain);

		for (int i = 0; i < modulators.getLength(); i++)
			genModulator((Element)(modulators.item(i)), modelDomain);

		for (int i = 0; i < expressions.getLength(); i++)
			genDoubleExpression((Element)(expressions.item(i)), modelDomain);

		for (int i = 0; i < nots.getLength(); i++)
			genNotExpression((Element)(nots.item(i)), modelDomain);

		for (int i = 0; i < summations.getLength(); i++)
			genSummation((Element)(summations.item(i)), modelDomain);

		for (int i = 0; i < conduits.getLength(); i++)
			genConduit((Element)(conduits.item(i)), modelDomain);
		
		for (int i = 0; i < references.getLength(); i++)
			genNamedReference((Element)(references.item(i)), modelDomain);

		return modelDomain;
	}
	
	
	protected HierarchyDomain genHierarchyDomain(Element element, boolean isMotif)
	throws GenException
	{
		String domainName = translateAttribute(element.getAttribute("name"));
		if (domainName.equals("")) throw new GenException(element,
			"Hierarchy Domain without a name");
		
		String sourceName = translateAttribute(element.getAttribute("source_name"));
		if (sourceName.equals("")) sourceName = globalSourceName;
		
		String sourceURLStr = translateAttribute(element.getAttribute("source_url"));
		if (sourceURLStr.equals("")) sourceURLStr = globalSourceURLStr;
		
		URL sourceURL;
		if (sourceURLStr == null) sourceURL = null;
		else try { sourceURL = new URL(sourceURLStr); }
		catch (java.net.MalformedURLException ex) { throw new GenException(element,
			"source_url=\"" + sourceURLStr + "\"", ex); }
		
		String version = translateAttribute(element.getAttribute("version"));
		if (version.equals("")) version = globalVersion;


		String motifsStr = translateAttribute(element.getAttribute("motifs"));
		Set<String> motifNames = null;
		if (! motifsStr.equals(""))
		{
			String[] motifStrs = motifsStr.split(",");
			motifNames = new HashSet<String>();
			for (String motifStr : motifStrs) motifNames.add(translateAttribute(motifStr));
		}
		
		NodeList menuItems = getElements(element, "menuitem");
		
		HierarchyDomain domain = null;
		try
		{
			if (isMotif)
				domain = modelEngine.createHierarchyDomainMotifDefPersistentNode(domainName,
					getCollidingDomainNameGeneratesException());
			else
				domain = modelEngine.createHierarchyDomainPersistentNode(domainName,
					getCollidingDomainNameGeneratesException());
		}
		catch (Exception pe)
		{
			GlobalConsole.printStackTrace(pe);
			throw new GenException(element, pe);
		}
		
		domain.setSourceName(sourceName);
		domain.setSourceURL(sourceURL);
		domain.setDeclaredName(domainName);
		domain.setDeclaredVersion(version);
		
		String layoutClassName = translateAttribute(element.getAttribute("layout"));
		if (layoutClassName.equals("")) layoutClassName = null;
		
		Class c = null;
		if (layoutClassName != null) try
		{
			c = ServiceContext.getMotifClassLoader().loadClass(layoutClassName);
		}
		catch (ClassNotFoundException ex) { throw new GenException(element, 
			"LayoutManager class not found", ex); }
		catch (Throwable t) { throw new GenException(element, t); }
		
		if (! LayoutManager.class.isAssignableFrom(c)) throw new GenException(
			element, "Class " + layoutClassName + " is not a LayoutManager");
		
		LayoutManager layoutManager;
		try { layoutManager = (LayoutManager)(c.newInstance()); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		domain.setLayoutManager(layoutManager);
		domain.conditionalLayout();
		
		String actualDomainName = domain.getName();
		if (! actualDomainName.equals(domainName))
		{
			domainNameSubstitutions.put(domainName, actualDomainName);
		}
		
		// Add to this Domain the motifs that this Domain claims to use.
		
		if (motifNames != null)
		{
			for (String motifName : motifNames)
			{
				MotifDef md;
				
				String substitutedName = domainNameSubstitutions.get(motifName);
				if (substitutedName != null) motifName = substitutedName;
				
				try { md = getModelEngineLocal().getMotifDefPersistentNode(motifName); }
				catch (ElementNotFound enf) { throw new GenException(
					element, "Motif '" + motifName + "' not found"); }
				catch (Exception ex) { throw new GenException(element, ex); }
				
				if (! (md instanceof MotifDef)) throw new GenException(
					"Motif '" + motifName + "' is not a MotifDef");
				if (! (md instanceof HierarchyDomain)) throw new GenException(
					"Motif '" + motifName + "' is not a HierarchyDomain.");
				
				domain.addMotifDef(md);
			}
		}
		
		if (isMotif && (menuItems != null))
		{
			for (int i = 0; i < menuItems.getLength(); i++)
				genMenuItem((Element)(menuItems.item(i)), (MotifDef)domain);
		}
		
		genHierarchyElement(element, domain);
		
		NodeList references = getElements(element, "references");
		for (int i = 0; i < references.getLength(); i++)
			genNamedReference((Element)(references.item(i)), domain);

		NodeList subElts = getElements(element, "hierarchy");
		for (int i = 0; i < subElts.getLength(); i++)
			genHierarchy((Element)(subElts.item(i)), domain);
		
		return domain;
	}
	
	
	protected <H extends Hierarchy> H genHierarchy(Element element, H parent)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, parent.getFullName(),
			"hierarchy without a name");
		
		String templateFullName = translateAttribute(element.getAttribute("type"));
		if (templateFullName.equals("")) templateFullName = null;
		
		Template template = resolveTemplateName(element, parent, templateFullName);
		if (! (template instanceof HierarchyTemplate)) throw new GenException(
			"Template '" + templateFullName + "' is not a HierarchyTemplate");
		HierarchyTemplate hTemplate = (HierarchyTemplate)template;
		
		int position = parent.getNoOfHierarchies();
		
		Hierarchy thisHierarchy = null;
		try
		{
			if (template == null)
				thisHierarchy = parent.createSubHierarchy(name);
			else
				thisHierarchy = parent.createSubHierarchy(name, position, null, hTemplate);
		}
		catch (ParameterError pe) { throw new GenException(
			element, parent.getFullName(), pe); }
		
		NodeList nodes = getElements(element, "description");
		if (nodes.getLength() > 1) throw new GenException(element,
			"Multiple descriptions");
		
		String htmlDescription = null;
		for (int i = 0; i < nodes.getLength(); i++)
		{
			htmlDescription = getNodeTreeAsText(nodes.item(i));
			break;
		}
		
		thisHierarchy.setHTMLDescription(htmlDescription);
		
		//typeSpecificAttributer.setAttributes(element, parent, thisHierarchy);

		String iconName = translateAttribute(element.getAttribute("icon"));
		if (iconName.equals("")) iconName = null;
		
		if (iconName != null)
		{
			try { thisHierarchy.setIconImage(iconName); }
			catch (Exception ex) { throw new GenException(element, parent.getFullName(), ex); }
		}

		String layoutClassName = translateAttribute(element.getAttribute("layout"));
		if (layoutClassName.equals("")) layoutClassName = null;
		
		Class c = null;
		if (layoutClassName != null) try
		{
			c = ServiceContext.getMotifClassLoader().loadClass(layoutClassName);
		}
		catch (ClassNotFoundException ex) { throw new GenException(element, 
			"LayoutManager class not found", ex); }
		catch (Throwable t) { throw new GenException(element, t); }
		
		if (! LayoutManager.class.isAssignableFrom(c)) throw new GenException(
			element, parent.getFullName(), ", Class " + layoutClassName +
			" is not a LayoutManager");
		
		LayoutManager layoutManager;
		try { layoutManager = (LayoutManager)(c.newInstance()); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		thisHierarchy.setLayoutManager(layoutManager);
		thisHierarchy.conditionalLayout();

		genHierarchyElement(element, thisHierarchy);
		
		
		// Add sub-elements.

		NodeList subElts = getElements(element, "hierarchy");
		for (int i = 0; i < subElts.getLength(); i++)
			genHierarchy((Element)(subElts.item(i)), thisHierarchy);

		return (H)thisHierarchy;
	}
	
	
	/*
	protected RequirementDomain genRequirementDomain(Element element, boolean isMotif)
	throws GenException
	{
		String domainName = translateAttribute(element.getAttribute("name"));
		if (domainName.equals("")) throw new GenException(element,
			"Requirement Domain without a name");
		
		String sourceName = translateAttribute(element.getAttribute("source_name"));
		if (sourceName.equals("")) sourceName = globalSourceName;
		
		String sourceURLStr = translateAttribute(element.getAttribute("source_url"));
		if (sourceURLStr.equals("")) sourceURLStr = globalSourceURLStr;
		
		URL sourceURL;
		if (sourceURLStr == null) sourceURL = null;
		else try { sourceURL = new URL(sourceURLStr); }
		catch (java.net.MalformedURLException ex) { throw new GenException(element,
			"source_url=\"" + sourceURLStr + "\"", ex); }
		
		String version = translateAttribute(element.getAttribute("version"));
		if (version.equals("")) version = globalVersion;


		String motifsStr = translateAttribute(element.getAttribute("motifs"));
		Set<String> motifNames = null;
		if (! motifsStr.equals(""))
		{
			String[] motifStrs = motifsStr.split(",");
			motifNames = new HashSet<String>();
			for (String motifStr : motifStrs) motifNames.add(translateAttribute(motifStr));
		}
		
		NodeList menuItems = getElements(element, "menuitem");
		
		RequirementDomain rqmtDomain = null;
		try
		{
			if (isMotif)
				rqmtDomain = modelEngine.createRequirementDomainMotifDefPersistentNode(domainName,
					getCollidingDomainNameGeneratesException());
			else
				rqmtDomain = modelEngine.createRequirementDomainPersistentNode(domainName,
					getCollidingDomainNameGeneratesException());
		}
		catch (Exception pe)
		{
			GlobalConsole.printStackTrace(pe);
			throw new GenException(element, pe);
		}
		
		rqmtDomain.setSourceName(sourceName);
		rqmtDomain.setSourceURL(sourceURL);
		rqmtDomain.setDeclaredName(domainName);
		rqmtDomain.setDeclaredVersion(version);
		
		String layoutClassName = translateAttribute(element.getAttribute("layout"));
		if (layoutClassName.equals("")) layoutClassName = null;
		
		Class c = null;
		if (layoutClassName != null) try
		{
			c = ServiceContext.getMotifClassLoader().loadClass(layoutClassName);
		}
		catch (ClassNotFoundException ex) { throw new GenException(element, 
			"LayoutManager class not found", ex); }
		catch (Throwable t) { throw new GenException(element, t); }
		
		if (! LayoutManager.class.isAssignableFrom(c)) throw new GenException(
			element, "Class " + layoutClassName + " is not a LayoutManager");
		
		LayoutManager layoutManager;
		try { layoutManager = (LayoutManager)(c.newInstance()); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		rqmtDomain.setLayoutManager(layoutManager);
		rqmtDomain.conditionalLayout();
		
		String actualDomainName = rqmtDomain.getName();
		if (! actualDomainName.equals(domainName))
		{
			domainNameSubstitutions.put(domainName, actualDomainName);
		}
		
		// Add to this Domain the motifs that this Domain claims to use.
		
		if (motifNames != null)
		{
			for (String motifName : motifNames)
			{
				MotifDef md;
				
				String substitutedName = domainNameSubstitutions.get(motifName);
				if (substitutedName != null) motifName = substitutedName;
				
				try { md = getModelEngineLocal().getMotifDefPersistentNode(motifName); }
				catch (ElementNotFound enf) { throw new GenException(
					element, "Motif '" + motifName + "' not found"); }
				catch (Exception ex) { throw new GenException(element, ex); }
				
				if (! (md instanceof RequirementDomainMotifDef)) throw new GenException(
					"Motif '" + motifName + "' is not a RequirementDomainMotifDef");
				
				rqmtDomain.addMotifDef(md);
			}
		}
		
		if (isMotif && (menuItems != null))
		{
			for (int i = 0; i < menuItems.getLength(); i++)
				genMenuItem((Element)(menuItems.item(i)), (MotifDef)rqmtDomain);
		}
		
		genRequirementElement(element, rqmtDomain);
		
		NodeList subElements = getElements(element, "requirement");
		NodeList references = getElements(element, "references");
		
		for (int i = 0; i < references.getLength(); i++)
			genNamedReference((Element)(references.item(i)), rqmtDomain);

		for (int i = 0; i < subElements.getLength(); i++)
			genRequirement((Element)(subElements.item(i)), rqmtDomain);
		
		return rqmtDomain;
	}
	
	
	protected Requirement genRequirement(Element element, Requirement parent)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, parent.getFullName(),
			"Requirement with no name attribute");
		
		String summary = translateAttribute(element.getAttribute("summary"));
		if (summary.equals("")) summary = null;
		
		String templateFullName = translateAttribute(element.getAttribute("type"));
		if (templateFullName.equals("")) templateFullName = null;
		
		Template template = resolveTemplateName(element, parent, templateFullName);
		if (! (template instanceof RequirementTemplate)) throw new GenException(
			"Template '" + templateFullName + "' is not a Requirement Template");
		RequirementTemplate rtemplate = (RequirementTemplate)template;
		
		int position = parent.getNoOfRequirements();
		
		Requirement thisRqmt = null;
		try
		{
			if (template == null)
				thisRqmt = parent.createSubRequirement(name);
			else
				thisRqmt = parent.createSubRequirement(name, position, null, rtemplate);
		}
		catch (ParameterError pe) { throw new GenException(
			element, parent.getFullName(), pe); }
		
		thisRqmt.setHTMLSummary(summary);
		
		String iconName = translateAttribute(element.getAttribute("icon"));
		if (iconName.equals("")) iconName = null;
		
		if (iconName != null)
		{
			try { thisRqmt.setIconImage(iconName); }
			catch (Exception ex) { throw new GenException(element, parent.getFullName(), ex); }
		}

		String layoutClassName = translateAttribute(element.getAttribute("layout"));
		if (layoutClassName.equals("")) layoutClassName = null;
		
		Class c = null;
		if (layoutClassName != null) try
		{
			c = ServiceContext.getMotifClassLoader().loadClass(layoutClassName);
		}
		catch (ClassNotFoundException ex) { throw new GenException(element, 
			"LayoutManager class not found", ex); }
		catch (Throwable t) { throw new GenException(element, t); }
		
		if (! LayoutManager.class.isAssignableFrom(c)) throw new GenException(
			element, parent.getFullName(), ", Class " + layoutClassName +
			" is not a LayoutManager");
		
		LayoutManager layoutManager;
		try { layoutManager = (LayoutManager)(c.newInstance()); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		thisRqmt.setLayoutManager(layoutManager);
		thisRqmt.conditionalLayout();


		genRequirementElement(element, thisRqmt);
		
		// Add sub-elements.

		NodeList subRqmts = getElements(element, "requirement");

		for (int i = 0; i < subRqmts.getLength(); i++)
			genRequirement((Element)(subRqmts.item(i)), thisRqmt);

		return thisRqmt;
	}
	
	
	protected Strategy genStrategyDomain(Element element, boolean isMotif)
	{
		String domainName = translateAttribute(element.getAttribute("name"));
		if (domainName.equals("")) throw new GenException(element,
			"Strategy Domain without a name");
		
		String sourceName = translateAttribute(element.getAttribute("source_name"));
		if (sourceName.equals("")) sourceName = globalSourceName;
		
		String sourceURLStr = translateAttribute(element.getAttribute("source_url"));
		if (sourceURLStr.equals("")) sourceURLStr = globalSourceURLStr;
		
		URL sourceURL;
		if (sourceURLStr == null) sourceURL = null;
		else try { sourceURL = new URL(sourceURLStr); }
		catch (java.net.MalformedURLException ex) { throw new GenException(element,
			"source_url=\"" + sourceURLStr + "\"", ex); }
		
		String version = translateAttribute(element.getAttribute("version"));
		if (version.equals("")) version = globalVersion;


		String motifsStr = translateAttribute(element.getAttribute("motifs"));
		Set<String> motifNames = null;
		if (! motifsStr.equals(""))
		{
			String[] motifStrs = motifsStr.split(",");
			motifNames = new HashSet<String>();
			for (String motifStr : motifStrs) motifNames.add(translateAttribute(motifStr));
		}
		
		NodeList menuItems = getElements(element, "menuitem");
		
		StrategyDomain stratDomain = null;
		try
		{
			if (isMotif)
				stratDomain = modelEngine.createStrategyDomainMotifDefPersistentNode(domainName,
					getCollidingDomainNameGeneratesException());
			else
				stratDomain = modelEngine.createStrategyDomainPersistentNode(domainName,
					getCollidingDomainNameGeneratesException());
		}
		catch (Exception pe)
		{
			GlobalConsole.printStackTrace(pe);
			throw new GenException(element, pe);
		}
		
		stratDomain.setSourceName(sourceName);
		stratDomain.setSourceURL(sourceURL);
		stratDomain.setDeclaredName(domainName);
		stratDomain.setDeclaredVersion(version);
		
		String layoutClassName = translateAttribute(element.getAttribute("layout"));
		if (layoutClassName.equals("")) layoutClassName = null;
		
		Class c = null;
		if (layoutClassName != null) try
		{
			c = ServiceContext.getMotifClassLoader().loadClass(layoutClassName);
		}
		catch (ClassNotFoundException ex) { throw new GenException(element, 
			"LayoutManager class not found", ex); }
		catch (Throwable t) { throw new GenException(element, t); }
		
		if (! LayoutManager.class.isAssignableFrom(c)) throw new GenException(
			element, "Class " + layoutClassName + " is not a LayoutManager");
		
		LayoutManager layoutManager;
		try { layoutManager = (LayoutManager)(c.newInstance()); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		stratDomain.setLayoutManager(layoutManager);
		stratDomain.conditionalLayout();
		
		String actualDomainName = stratDomain.getName();
		if (! actualDomainName.equals(domainName))
		{
			domainNameSubstitutions.put(domainName, actualDomainName);
		}
		
		// Add to this Domain the motifs that this Domain claims to use.
		
		if (motifNames != null)
		{
			for (String motifName : motifNames)
			{
				MotifDef md;
				
				String substitutedName = domainNameSubstitutions.get(motifName);
				if (substitutedName != null) motifName = substitutedName;
				
				try { md = getModelEngineLocal().getMotifDefPersistentNode(motifName); }
				catch (ElementNotFound enf) { throw new GenException(
					element, "Motif '" + motifName + "' not found"); }
				catch (Exception ex) { throw new GenException(element, ex); }
				
				if (! (md instanceof StrategyDomainMotifDef)) throw new GenException(
					"Motif '" + motifName + "' is not a StrategyDomainMotifDef");
				
				stratDomain.addMotifDef(md);
			}
		}
		
		if (isMotif && (menuItems != null))
		{
			for (int i = 0; i < menuItems.getLength(); i++)
				genMenuItem((Element)(menuItems.item(i)), (MotifDef)stratDomain);
		}
		
		genStrategyElement(element, stratDomain);
		
		NodeList subElements = getElements(element, "strategy");
		NodeList references = getElements(element, "references");
		
		for (int i = 0; i < references.getLength(); i++)
			genNamedReference((Element)(references.item(i)), stratDomain);

		for (int i = 0; i < subElements.getLength(); i++)
			genStrategy((Element)(subElements.item(i)), stratDomain);
		
		return stratDomain;
	}
	
	
	protected Strategy genStrategy(Element element, Strategy parent)
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, parent.getFullName(),
			"Strategy with no name attribute");
		
		String summary = translateAttribute(element.getAttribute("summary"));
		if (summary.equals("")) summary = null;
		
		String templateFullName = translateAttribute(element.getAttribute("type"));
		if (templateFullName.equals("")) templateFullName = null;
		
		Template template = resolveTemplateName(element, parent, templateFullName);
		if (! (template instanceof StrategyTemplate)) throw new GenException(
			"Template '" + templateFullName + "' is not a Strategy Template");
		StrategyTemplate rtemplate = (StrategyTemplate)template;
		
		int position = parent.getNoOfStrategies();
		
		Strategy thisStrat = null;
		try
		{
			if (template == null)
				thisStrat = parent.createSubStrategy(name);
			else
				thisStrat = parent.createSubStrategy(name, position, null, rtemplate);
		}
		catch (ParameterError pe) { throw new GenException(
			element, parent.getFullName(), pe); }
		
		thisStrat.setHTMLSummary(summary);
		
		String iconName = translateAttribute(element.getAttribute("icon"));
		if (iconName.equals("")) iconName = null;
		
		if (iconName != null)
		{
			try { thisStrat.setIconImage(iconName); }
			catch (Exception ex) { throw new GenException(element, parent.getFullName(), ex); }
		}

		String layoutClassName = translateAttribute(element.getAttribute("layout"));
		if (layoutClassName.equals("")) layoutClassName = null;
		
		Class c = null;
		if (layoutClassName != null) try
		{
			c = ServiceContext.getMotifClassLoader().loadClass(layoutClassName);
		}
		catch (ClassNotFoundException ex) { throw new GenException(element, 
			"LayoutManager class not found", ex); }
		catch (Throwable t) { throw new GenException(element, t); }
		
		if (! LayoutManager.class.isAssignableFrom(c)) throw new GenException(
			element, parent.getFullName(), ", Class " + layoutClassName +
			" is not a LayoutManager");
		
		LayoutManager layoutManager;
		try { layoutManager = (LayoutManager)(c.newInstance()); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		thisStrat.setLayoutManager(layoutManager);
		thisStrat.conditionalLayout();


		genStrategyElement(element, thisStrat);
		
		// Add sub-elements.

		NodeList subStrats = getElements(element, "strategy");

		for (int i = 0; i < subStrats.getLength(); i++)
			genStrategy((Element)(subStrats.item(i)), thisStrat);

		return thisStrat;
	}*/
	

	protected Activity genActivity(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"Activity with no name attribute");
		
		String templateFullName = translateAttribute(element.getAttribute("type"));
		if (templateFullName.equals("")) templateFullName = null;
		
		String nativeString = translateAttribute(element.getAttribute("native"));
		String nativePathString = translateAttribute(element.getAttribute("native_path"));
		
		String iconName = translateAttribute(element.getAttribute("icon"));
		if (iconName.equals("")) iconName = null;
		
		URLClassLoader userClassLoader = null;		
		
		if (! nativePathString.equals(""))
		{
			if (templateFullName != null) throw new GenException(
				"Cannot specify both an Activity type and a native class path");
			
			// Parse the user-specified classpath (if provided).
			
			List<URL> urls = new Vector<URL>();
			
			String[] pathStrings = nativePathString.split(":");
			for (String pathString : pathStrings)
			{
				File f = new File(pathString);
				if (! f.exists()) throw new GenException(element, modelContainer.getFullName(),
					"native_path specified for activity does not exist: " +
						f.toString());
				
				String urlString = null;
				URL url = null;
				try {
					urlString = "file://" + f.getCanonicalPath() + "/";
					url = (new URI(urlString)).toURL();
				}
				catch (Exception ex) { throw new GenException(element, modelContainer.getFullName(),
					ex.getMessage() + ", for Activity classpath URL: " + urlString); }
				
				urls.add(url);
			}

			userClassLoader = new URLClassLoader(urls.toArray(new URL[1]));
		}
		
		Class nativeImplClass = null;
		if (! nativeString.equals(""))
		{
			if (templateFullName != null) throw new GenException(
				"Cannot specify both an Activity type and a native class");
			
			try
			{ 
				if (userClassLoader == null)
					nativeImplClass = Class.forName(nativeString);
				else
					nativeImplClass = Class.forName(nativeString, true, userClassLoader);
			}
			catch (ClassNotFoundException cnfe)
			{
				String urlString = null;
				
				ClassLoader classLoader = null;
				if (userClassLoader == null)
					classLoader = this.getClass().getClassLoader();
				else
					classLoader = userClassLoader;
				
				if (classLoader instanceof java.net.URLClassLoader)
				{
					URL[] urls = ((java.net.URLClassLoader)classLoader).getURLs();
					boolean first = true;
					urlString = "";
					for (URL url : urls)
					{
						if (first) first = false;
						else urlString += ":";
						
						urlString += url.toString();
					}
				}
				
				throw new GenException(element, modelContainer.getFullName(),
					"In 'native' attribute for '" + name + ", could not find class " +
						nativeString + (urlString == null? "" : (" in classpath " + urlString)));
			}
			
			if (! Activity.NativeActivityImplementation.class.isAssignableFrom(nativeImplClass))
			{
				throw new GenException(element, modelContainer.getFullName(),
					"'native' must specify a class that implements " +
					"'Activity.NativeActivityImplementation'");
			}
		}
		
		
		Template template = resolveTemplateName(element, modelContainer, templateFullName);
		
		Activity thisActivity = null;
		try
		{
			if (template == null)
				thisActivity = modelContainer.createActivity(name, nativeImplClass, 
					modelContainer, null);
			else
				thisActivity = modelContainer.createActivity(name, 
						template, modelContainer, null);
		}
		catch (ParameterError pe) { throw new GenException(
			element, modelContainer.getFullName(), pe); }
		
		if (iconName != null)
		{
			try { thisActivity.setIconImage(iconName); }
			catch (Exception ex) { throw new GenException(
				element, modelContainer.getFullName(), ex); }
		}


		String layoutClassName = translateAttribute(element.getAttribute("layout"));
		if (layoutClassName.equals("")) layoutClassName = null;
		
		Class c = null;
		if (layoutClassName != null) try
		{
			c = ServiceContext.getMotifClassLoader().loadClass(layoutClassName);
		}
		catch (ClassNotFoundException ex) { throw new GenException(element, 
			"LayoutManager class not found", ex); }
		catch (Throwable t) { throw new GenException(element, t); }
		
		if (! LayoutManager.class.isAssignableFrom(c)) throw new GenException(
			element, modelContainer.getFullName(), ", Class " + layoutClassName +
			" is not a LayoutManager");
		
		LayoutManager layoutManager;
		try { layoutManager = (LayoutManager)(c.newInstance()); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		thisActivity.setLayoutManager(layoutManager);
		thisActivity.conditionalLayout();


		genModelElement(element, thisActivity);
		
		// Add sub-elements.

		NodeList ports = getElements(element, "port");
		NodeList states = getElements(element, "state");
		NodeList activities = getElements(element, "activity");
		NodeList functions = getElements(element, "function");
		NodeList terminals = getElements(element, "terminal");
		NodeList generators = getElements(element, "generator");
		NodeList delays = getElements(element, "delay");
		NodeList sustains = getElements(element, "sustain");
		NodeList tallys = getElements(element, "tally");
		NodeList switches = getElements(element, "switch");
		NodeList oneways = getElements(element, "oneway");
		NodeList discriminators = getElements(element, "discriminator");
		NodeList modulators = getElements(element, "modulator");
		NodeList expressions = getElements(element, "expression");
		NodeList nots = getElements(element, "not");
		NodeList summations = getElements(element, "summation");
		NodeList conduits = getElements(element, "conduit");
		
		for (int i = 0; i < ports.getLength(); i++)
		{
			Port port = genPort((Element)(ports.item(i)), thisActivity);
			port.setMovable(false);
		}

		for (int i = 0; i < states.getLength(); i++)
			genState((Element)(states.item(i)), thisActivity);

		for (int i = 0; i < activities.getLength(); i++)
			genActivity((Element)(activities.item(i)), thisActivity);

		for (int i = 0; i < functions.getLength(); i++)
			genFunction((Element)(functions.item(i)), thisActivity);

		for (int i = 0; i < terminals.getLength(); i++)
			genTerminal((Element)(terminals.item(i)), thisActivity);

		for (int i = 0; i < generators.getLength(); i++)
			genGenerator((Element)(generators.item(i)), thisActivity);

		for (int i = 0; i < delays.getLength(); i++)
			genDelay((Element)(delays.item(i)), thisActivity);

		for (int i = 0; i < sustains.getLength(); i++)
			genSustain((Element)(sustains.item(i)), thisActivity);

		for (int i = 0; i < tallys.getLength(); i++)
			genTally((Element)(tallys.item(i)), thisActivity);

		for (int i = 0; i < switches.getLength(); i++)
			genSwitch((Element)(switches.item(i)), thisActivity);

		for (int i = 0; i < oneways.getLength(); i++)
			genOneWay((Element)(oneways.item(i)), thisActivity);

		for (int i = 0; i < discriminators.getLength(); i++)
			genDiscriminator((Element)(discriminators.item(i)), thisActivity);

		for (int i = 0; i < modulators.getLength(); i++)
			genModulator((Element)(modulators.item(i)), thisActivity);

		for (int i = 0; i < expressions.getLength(); i++)
			genDoubleExpression((Element)(expressions.item(i)), thisActivity);

		for (int i = 0; i < nots.getLength(); i++)
			genNotExpression((Element)(nots.item(i)), thisActivity);

		for (int i = 0; i < summations.getLength(); i++)
			genSummation((Element)(summations.item(i)), thisActivity);

		for (int i = 0; i < conduits.getLength(); i++)
			genConduit((Element)(conduits.item(i)), thisActivity);
		
		thisActivity.conditionalLayout();
		
		
		if (thisActivity instanceof Template)
		{
			NodeList menuItems = getElements(element, "menuitem");
			for (int i = 0; i < menuItems.getLength(); i++)
				genMenuItem((Element)(menuItems.item(i)), (Template)thisActivity);
		}
		
		if (thisActivity.getModelDomain() instanceof MotifDef)
		{
			String wStr = translateAttribute(element.getAttribute("width"));
			String hStr = translateAttribute(element.getAttribute("height"));
			
			if (wStr.equals("") || hStr.equals("")) throw new GenException(element,
				"A Template must have its width and height specified.");

			String xStr = translateAttribute(element.getAttribute("x"));
			String yStr = translateAttribute(element.getAttribute("y"));
			
			if (xStr.equals("") || yStr.equals("")) throw new GenException(element,
				"A Template must have its x and y locations within its Motif specified.");
		}
		

		return thisActivity;
	}
	
	
	protected Template resolveTemplateName(Element element, PersistentNode parent,
		String templateFullName)
	throws GenException
	{
		if (templateFullName == null) return null;
		
		Template template = null;
			
		String[] parts = templateFullName.split("\\.");
		if (parts.length != 2) throw new GenException(element,
			parent.getFullName(), "Ill-formed motif activity name: " +
			templateFullName);
		
		String motifDefName = translateNodePath(parts[0]);
		String templateName = parts[1];
		
		MotifDef motifDef;
		try { motifDef = getModelEngineLocal().getMotifDefPersistentNode(motifDefName); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		if (! parent.getDomain().usesMotif(motifDef))
			throw new GenException("Reference to a Motif (" + motifDefName +
				") that is not explicitly declared to be used by Model Domain " +
				parent.getDomain().getName());
		
		try { template = motifDef.getTemplate(templateName); }
		catch (ElementNotFound enf) { throw new GenException(element, enf); }
		
		return template;
	}
	
	
	protected MenuItem genMenuItem(Element element, MenuOwner menuOwner)
	throws GenException
	{
		String text = translateAttribute(element.getAttribute("text"));
		if (text.equals("")) throw new GenException(element, menuOwner.getFullName(),
			"Menu item with no text attribute");
		
		NodeList nodes = getElements(element, "description");
		if (nodes.getLength() > 1) throw new GenException(element,
			"Multiple descriptions");
		
		String htmlDescription = null;
		for (int i = 0; i < nodes.getLength(); i++)
		{
			htmlDescription = getNodeTreeAsText(nodes.item(i));
			break;
		}
		
		String actionStr = translateAttribute(element.getAttribute("action"));
		if (actionStr.equals("")) actionStr = null;
		
		NodeList subMenuitems = getElements(element, "menuitem");
		
		MenuItem menuItem = null;
		
		if (actionStr == null)
		{
			if (subMenuitems.getLength() > 0)
			{
				try { menuItem = menuOwner.createMenuTree(text, htmlDescription); }
				catch (ParameterError pe) { throw new GenException(element, pe); }
				
				for (int i = 0; i < subMenuitems.getLength(); i++)
					genMenuItem((Element)(subMenuitems.item(i)), (MenuTree)menuItem);
			}
			else
			{
				addWarning("Warning: At element <" + element.getTagName() + 
					(element.getAttribute("name").equals("") ? "" :
						(" name=\"" + element.getAttribute("name") + "\"")) + 
					">, for " + menuOwner.getName() + ", No action and no sub-menus.");
				
				try { menuItem = menuOwner.createAction(text, htmlDescription, actionStr); }
				catch (ParameterError pe) { throw new GenException(element, pe); }
			}
		}
		else
		{
			if (subMenuitems.getLength() > 0) throw new GenException(
				"action is not allowed when a menuitem has sub-menus.");
			else
				try { menuItem = menuOwner.createAction(text, htmlDescription, actionStr); }
				catch (ParameterError pe) { throw new GenException(element, pe); }
		}
		
		if (htmlDescription != null)
			menuItem.setHTMLDescription(htmlDescription);
		
		return menuItem;
	}
	
	
	int connectorNumber = 0;
	
	
	protected Generator genGenerator(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"Activity with no name attribute");
		
		String valueString = null;
		
		boolean variable = false;
		valueString = translateAttribute(element.getAttribute("variable"));
		if (valueString.equals("")) variable = false;
		else variable = Boolean.parseBoolean(valueString);
		
		boolean deterministicTime = false;
		
		double timeAttribute = 0.0;
		
		valueString = translateAttribute(element.getAttribute("time"));
		if (! valueString.equals(""))
		{
			deterministicTime = true;
			try { timeAttribute = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element,
				modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be a double"); }
			
			if (timeAttribute <= 0) throw new GenException(element, modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be positive");
		}
		
		double timeDistShape = 0.0;

		valueString = translateAttribute(element.getAttribute("time_shape"));
		if (! valueString.equals(""))
		{
			if (deterministicTime) throw new GenException(element, modelContainer.getFullName(),
				"A generator cannot have both a 'time' and a 'time_shape' attribute");
			
			try { timeDistShape = Double.parseDouble(valueString); }
			catch ( NumberFormatException nfe) { throw new GenException(element, 
				modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be a double"); }
			
			if (timeDistShape <= 0) throw new GenException(element, modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be positive");
		}
			
		double timeDistScale = 0.0;

		valueString = translateAttribute(element.getAttribute("time_scale"));
		if (! valueString.equals(""))
		{
			if (deterministicTime) throw new GenException(element, modelContainer.getFullName(),
				"A generator cannot have both a 'time' and a 'time_scale' attribute");
			
			try { timeDistScale = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element,
				modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be a double"); }
			
			if (timeDistScale <= 0) throw new GenException(element, modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be positive");
		}
			
		boolean deterministicValue = false;
		
		double valueAttribute = 0.0;
		
		valueString = translateAttribute(element.getAttribute("value"));
		if (! valueString.equals(""))
		{
			deterministicValue = true;
			try { valueAttribute = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element,
				modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be a double"); }
			
			if (valueAttribute <= 0) throw new GenException(element, modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be positive");
		}
		
		double valueDistShape = 0.0;

		valueString = translateAttribute(element.getAttribute("value_shape"));
		if (! valueString.equals(""))
		{
			if (deterministicValue) throw new GenException(element, modelContainer.getFullName(),
				"A generator cannot have both a 'value' and a 'value_shape' attribute");
			
			try { valueDistShape = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element, 
				modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be a double"); }
			
			if (valueDistShape <= 0) throw new GenException(element, modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be positive");
		}
		
		double valueDistScale = 0.0;

		valueString = translateAttribute(element.getAttribute("value_scale"));
		if (! valueString.equals(""))
		{
			if (deterministicValue) throw new GenException(element, modelContainer.getFullName(),
				"A generator cannot have both a 'value' and a 'value_scale' attribute");
			
			try { valueDistScale = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element, 
				modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be a double"); }
			
			if (valueDistScale <= 0) throw new GenException(element, modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be positive");
		}
		
		if (deterministicTime)
		{
			timeDistShape = 0;
			timeDistScale = timeAttribute;
		}

		if (deterministicValue)
		{
			valueDistShape = 0;
			valueDistScale = valueAttribute;
		}
		
		boolean ignoreStartup = false;
		valueString = translateAttribute(element.getAttribute("ignore_startup"));
		if (valueString.equals("")) ignoreStartup = false;
		else if (valueString.equals("false")) ignoreStartup = false;
		else if (valueString.equals("true")) ignoreStartup = true;
		else throw new GenException(element, modelContainer.getFullName(), 
			"Value of 'ignore_startup' attribute unrecognized");
		
		boolean pulse = false;
		valueString = translateAttribute(element.getAttribute("pulse"));
		if (valueString.equals("")) pulse = false;
		else if (valueString.equals("false")) pulse = false;
		else if (valueString.equals("true")) pulse = true;
		else throw new GenException(element, modelContainer.getFullName(),
			"Value of 'pulse' attribute unrecognized");
		
		
		// Create a Generator Model Element.
		
		Generator thisGenerator = null;
		try
		{
			thisGenerator = modelContainer.createGenerator(name, variable,
				timeDistShape, timeDistScale, valueDistShape, valueDistScale, 
				ignoreStartup, pulse, modelContainer, null);
		}
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
			pe); }
		
		genModelElement(element, thisGenerator);

		
		// If a repeating Generator, generate a Conduit to connect the Count Port
		// back to the Start Port.
		
		valueString = translateAttribute(element.getAttribute("repeating"));
		if (valueString.equals("true"))
		{
			// Generate a conduit connecting the Count port to the Input port.
			// Thus, each time the generator fires an event, it will trigger itself,
			// causing it to schedule another firing.
			
			try { modelContainer.makeGeneratorRepeating(thisGenerator, true); }
			catch (Exception pe) { throw new GenException(element, modelContainer.getFullName(),
				pe); }
		}
		else if ((! valueString.equals("false")) && (! valueString.equals("")))
			throw new GenException(element, modelContainer.getFullName(),
				"Generator 'repeating' attribute must be 'true' or 'false'");
		
		
		// If a Pulse Generator, generate a Conduit to connect the Rise Out Port
		// to the Rise In Port.
		
		Conduit riseConduit = null;
		if (thisGenerator instanceof PulseGenerator)
		{
			PulseGenerator pulseGenerator = (PulseGenerator)thisGenerator;
			
			// Generate a conduit connecting the Count port to the Input port.
			// Thus, each time the generator fires an event, it will trigger itself,
			// causing it to schedule another firing.
			try { riseConduit = modelContainer.createConduit(
				"_riseout_to_risein_" + connectorNumber++, pulseGenerator.getRiseOut(), 
					pulseGenerator.getRiseIn(), modelContainer, null); }
			catch (ParameterError pe) { 
				GlobalConsole.printStackTrace(pe);
				throw new GenException(element, modelContainer.getFullName(),
				"Unable to create conduit connecting generator Rise out and Rise in ports",
					pe); }
			
			riseConduit.setVisible(false);
			riseConduit.setPredefined(true);
		}
		
		thisGenerator.conditionalLayout();
				
		return thisGenerator;
	}


	protected Function genFunction(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"Function with no name attribute");
		
		String templateFullName = translateAttribute(element.getAttribute("type"));
		if (templateFullName.equals("")) templateFullName = null;
		
		String evalsString = translateAttribute(element.getAttribute("max_evals"));
		if (evalsString.equals("")) evalsString = null;
		int maxEvals = 5;  // default.
		if (evalsString != null) try { maxEvals = Integer.parseInt(evalsString); }
		catch ( NumberFormatException nfe) { throw new GenException(element, 
			modelContainer.getFullName(), "Parameter 'max_evals' must be an integer"); }
			
		if (maxEvals <= 0) throw new GenException(element, modelContainer.getFullName(),
			"Generator parameter 'max_evals' must be > 0");
		
		String nativeString = translateAttribute(element.getAttribute("native"));
		String nativePathString = translateAttribute(element.getAttribute("native_path"));
		
		String iconName = translateAttribute(element.getAttribute("icon"));
		if (iconName.equals("")) iconName = null;
		
		URLClassLoader userClassLoader = null;		
		
		if (! nativePathString.equals(""))
		{
			// Parse the user-specified classpath (if provided).
			
			List<URL> urls = new Vector<URL>();
			
			String[] pathStrings = nativePathString.split(":");
			for (String pathString : pathStrings)
			{
				File f = new File(pathString);
				if (! f.exists()) throw new GenException(element, modelContainer.getFullName(),
					"native_path specified for activity does not exist: " +
						f.toString());
				
				String urlString = null;
				URL url = null;
				try {
					urlString = "file:" + f.getCanonicalPath();
					url = (new URI(urlString)).toURL();
				}
				catch (Exception ex) { throw new GenException(element, modelContainer.getFullName(),
					"for Activity classpath URL: " + urlString, ex); }
				
				urls.add(url);
			}

			userClassLoader = new URLClassLoader(urls.toArray(new URL[1]));
		}

		Class nativeImplClass = null;
		if (! nativeString.equals(""))
		{
			try { if (userClassLoader == null) nativeImplClass = Class.forName(nativeString);
					else nativeImplClass = Class.forName(nativeString, true, userClassLoader); }
			catch (ClassNotFoundException cnfe) { throw new GenException(element, 
				modelContainer.getFullName(), "Could not find 'native' attribute for '" + name); }
			if (! Function.NativeFunctionImplementation.class.isAssignableFrom(nativeImplClass))
				throw new GenException(element, modelContainer.getFullName(),
					"Attribute 'native' must specify a class " +
					"that implements Function.NativeFunctionImplementation");
		}
		
		Template template = resolveTemplateName(element, modelContainer, templateFullName);
		
		Function thisFunction = null;
		try
		{
			if (template == null)
				thisFunction = modelContainer.createFunction(name, nativeImplClass,
					modelContainer, null);
			else
				thisFunction = modelContainer.createFunction(name, 
						template, modelContainer, null);
		}
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(), pe); }
		thisFunction.setMaxEvals(maxEvals);

		if (iconName != null)
		{
			try { thisFunction.setIconImage(iconName); }
			catch (Exception ex) { throw new GenException(element, modelContainer.getFullName(), ex); }
		}

		genModelElement(element, thisFunction);
		
		// Add sub-elements.

		NodeList states = getElements(element, "state");
		NodeList functions = getElements(element, "function");
		NodeList terminals = getElements(element, "terminal");
		NodeList ports = getElements(element, "port");
		NodeList conduits = getElements(element, "conduit");
		
		for (int i = 0; i < functions.getLength(); i++)
			genFunction((Element)(functions.item(i)), thisFunction);

		for (int i = 0; i < terminals.getLength(); i++)
			genTerminal((Element)(terminals.item(i)), thisFunction);

		for (int i = 0; i < ports.getLength(); i++)
		{
			Port port = genPort((Element)(ports.item(i)), thisFunction);
			port.setMovable(false);
		}
		
		for (int i = 0; i < states.getLength(); i++)
			genState((Element)(states.item(i)), thisFunction);

		for (int i = 0; i < conduits.getLength(); i++)
			genConduit((Element)(conduits.item(i)), thisFunction);
		
		
		if (thisFunction instanceof Template)
		{
			NodeList menuItems = getElements(element, "menuitem");
			for (int i = 0; i < menuItems.getLength(); i++)
				genMenuItem((Element)(menuItems.item(i)), (Template)thisFunction);
		}
		
		String layoutClassName = translateAttribute(element.getAttribute("layout"));
		if (layoutClassName.equals("")) layoutClassName = null;
		
		Class c = null;
		if (layoutClassName != null) try
		{
			c = ServiceContext.getMotifClassLoader().loadClass(layoutClassName);
		}
		catch (ClassNotFoundException ex) { throw new GenException(element, 
			"LayoutManager class not found", ex); }
		catch (Throwable t) { throw new GenException(element, t); }
		
		if (! LayoutManager.class.isAssignableFrom(c)) throw new GenException(
			element, modelContainer.getFullName(), ", Class " + layoutClassName +
			" is not a LayoutManager");
		
		LayoutManager layoutManager;
		try { layoutManager = (LayoutManager)(c.newInstance()); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		thisFunction.setLayoutManager(layoutManager);
		thisFunction.conditionalLayout();
		
		
		if (thisFunction instanceof Template)
		{
			NodeList menuItems = getElements(element, "menuitem");
			for (int i = 0; i < menuItems.getLength(); i++)
				genMenuItem((Element)(menuItems.item(i)), (Template)thisFunction);
		}
		
		if (thisFunction.getModelDomain() instanceof MotifDef)
		{
			String wStr = translateAttribute(element.getAttribute("width"));
			String hStr = translateAttribute(element.getAttribute("height"));
			
			if (wStr.equals("") || hStr.equals("")) throw new GenException(element,
				"A Template must have its width and height specified.");

			String xStr = translateAttribute(element.getAttribute("x"));
			String yStr = translateAttribute(element.getAttribute("y"));
			
			if (xStr.equals("") || yStr.equals("")) throw new GenException(element,
				"A Template must have its x and y locations within its Motif specified.");
		}
		

		return thisFunction;
	}

	
	protected Terminal genTerminal(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(), 
			"Terminal with no name attribute");
		
		Terminal thisTerminal = null;
		try { thisTerminal = modelContainer.createTerminal(name, modelContainer, null); }
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
			pe); }
		
		genModelElement(element, thisTerminal);
		
		thisTerminal.conditionalLayout();
		
		return thisTerminal;
	}
	
	
	protected Delay genDelay(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"Delay with no name attribute");
		
		String delayString = translateAttribute(element.getAttribute("time_delay"));
		if (delayString.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"A delay must have a time_delay parameter");
		
		long delayValue = 0;
		try { delayValue = Long.parseLong(delayString); }
		catch (NumberFormatException nfe) { throw new GenException(element, modelContainer.getFullName(),
			"Unable to parse " + delayString + " as a long value"); }
			
		delayValue = delayValue * DateAndTimeUtils.MsInADay;  // Convert from days to ms.
		
		Delay thisDelay = null;
		try { thisDelay = modelContainer.createDelay(name, delayValue, modelContainer, null); }
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(), pe); }
		
		genModelElement(element, thisDelay);
		
		//thisDelay.dump();
		
		return thisDelay;
	}
	
	
	protected Sustain genSustain(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(), 
			"Sustain with no name attribute");
		
		boolean deterministicTime = false;
		
		double timeAttribute = 0.0;
		
		String valueString = translateAttribute(element.getAttribute("time"));
		if (! valueString.equals(""))
		{
			deterministicTime = true;
			try { timeAttribute = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element,
				modelContainer.getFullName(), "Generator parameter '" + valueString + 
				"' must be a double"); }
			
			if (timeAttribute <= 0) throw new GenException(element, modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be positive");
		}
		
		double timeDistShape = 0.0;

		valueString = translateAttribute(element.getAttribute("time_shape"));
		if (! valueString.equals(""))
		{
			if (deterministicTime) throw new GenException(element, modelContainer.getFullName(),
				"A generator cannot have both a 'time' and a 'time_shape' attribute");
			
			try { timeDistShape = Double.parseDouble(valueString); }
			catch ( NumberFormatException nfe) { throw new GenException(element, 
				modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be a double"); }
			
			if (timeDistShape <= 0) throw new GenException(element, modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be positive");
		}
			
		double timeDistScale = 0.0;

		valueString = translateAttribute(element.getAttribute("time_scale"));
		if (! valueString.equals(""))
		{
			if (deterministicTime) throw new GenException(element, modelContainer.getFullName(),
				"A generator cannot have both a 'time' and a 'time_scale' attribute");
			
			try { timeDistScale = Double.parseDouble(valueString); }
			catch (NumberFormatException nfe) { throw new GenException(element,
				modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be a double"); }
			
			if (timeDistScale <= 0) throw new GenException(element, modelContainer.getFullName(),
				"Generator parameter '" + valueString + "' must be positive");
		}

		
		if (deterministicTime)
		{
			timeDistShape = 0;
			timeDistScale = timeAttribute;
		}

		// Create a Sustain Model Element.
		
		Sustain thisSustain = null;
		try
		{
			thisSustain = modelContainer.createSustain(name,
				timeDistShape, timeDistScale, modelContainer, null);
		}
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
			pe); }
		
		
		genModelElement(element, thisSustain);
		
		//thisSustain.dump();
		
		return thisSustain;
	}
	
	
	protected Tally genTally(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"Tally with no name attribute");
		
		String initValueString = translateAttribute(element.getAttribute("init_value"));
		double initValue = 0.0;
		if (! initValueString.equals(""))
			try { initValue = Double.parseDouble(initValueString); }
			catch (NumberFormatException nfe) { throw new GenException(element,
				modelContainer.getFullName(),
				"Unable to parse " + initValueString + " as a double value"); }
		
		boolean doubleCount = false;
		String doubleCountString = translateAttribute(element.getAttribute("double_count"));
		if (doubleCountString.equals("") || doubleCountString.equals("false")) doubleCount = false;
		else if (doubleCountString.equals("true")) doubleCount = true;
		else throw new GenException(element, modelContainer.getFullName(),
			"Unrecognized value for 'double_count' attribute");
		
		NodeList nodes = getElements(element, "input_port");
		Set<String> inputPortNameSet = new TreeSetNullDisallowed<String>();
		for (int i = 0; i < nodes.getLength(); i++)
		{
			String inputPortName =
				translateAttribute(((Element)(nodes.item(i))).getAttribute("name"));
				
			if (inputPortName.equals("")) throw new GenException(element, modelContainer.getFullName(),
				"Empty input port name");
				
			inputPortNameSet.add(inputPortName);
		}

		String[] inputPortNames = inputPortNameSet.toArray(new String[inputPortNameSet.size()]);
		if (inputPortNames.length == 0)
			addWarning("Warning: no input ports specified for Tally");

		Tally thisTally = null;
		try { thisTally = modelContainer.createTally(name, new Double(initValue),
			inputPortNames, doubleCount, modelContainer, null); }
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
			pe); }
		
		
		genModelElement(element, thisTally);
				
		return thisTally;
	}


	protected Switch genSwitch(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"Switch with no name attribute");
		
		boolean conducting = false;
		String initValueString = translateAttribute(element.getAttribute("start_conducting"));
		if (initValueString.equals("")) conducting = false;
		else if (initValueString.equals("true")) conducting = true;
		else if (initValueString.equals("false")) conducting = false;
		else throw new GenException(element, modelContainer.getFullName(),
			"A 'start_conducting' attribute must be 'true' or 'false'");

		Switch thisSwitch = null;
		try { thisSwitch = modelContainer.createSwitch(name, conducting, modelContainer, null); }
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
			pe); }
		
		genModelElement(element, thisSwitch);
		
		return thisSwitch;
	}


	protected OneWay genOneWay(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"OneWay with no name attribute");
		
		OneWay oneWay = null;
		try { oneWay = modelContainer.createOneWay(name, modelContainer, null); }
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
			pe); }
		
		genModelElement(element, oneWay);
		
		return oneWay;
	}


	protected Discriminator genDiscriminator(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"Discriminator with no name attribute");
		
		String threshValStr = translateAttribute(element.getAttribute("threshold"));
		Double threshold = null;
		if (! threshValStr.equals(""))
		{
			double th = 0.0;
			try { th = Double.parseDouble(threshValStr); }
			catch (NumberFormatException nfe) { throw new GenException(element,
				modelContainer.getFullName(),
				"Non-numeric value for threshold parameter"); }
				
			threshold = new Double(th);
		}
		
		Discriminator discriminator = null;
		try { discriminator = modelContainer.createDiscriminator(name, threshold, modelContainer, null); }
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
			pe); }

		genModelElement(element, discriminator);
		
		return discriminator;
	}


	protected Modulator genModulator(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"Modulator with no name attribute");
		
		String facValStr = translateAttribute(element.getAttribute("factor"));
		Double factor = null;
		if (! facValStr.equals(""))
		{
			double fa = 0.0;
			try { fa = Double.parseDouble(facValStr); }
			catch (NumberFormatException nfe) { throw new GenException(element,
				modelContainer.getFullName(),
				"Non-numeric value for Modulator '" + name + "' factor parameter: " + facValStr); }
				
			factor = new Double(fa);
		}
		
		Modulator modulator = null;
		try { modulator = modelContainer.createModulator(name, factor, modelContainer, null); }
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
			pe); }
		
		genModelElement(element, modulator);
		
		return modulator;
	}


	protected DoubleExpression genDoubleExpression(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"Expression with no name attribute");
		
		String exprStr = translateAttribute(element.getAttribute("expr"));
		if (exprStr.equals("")) addWarning(
			"Warning: Expression '" + name + "' in '" + modelContainer.getFullName() +
				"' has no expr attribute");
		
		NodeList nodes = getElements(element, "input_port");
		Set<String> inputPortNameSet = new TreeSetNullDisallowed<String>();
		for (int i = 0; i < nodes.getLength(); i++)
		{
			String inputPortName =
				translateAttribute(((Element)(nodes.item(i))).getAttribute("name"));
				
			if (inputPortName.equals("")) throw new GenException(element, modelContainer.getFullName(),
				"Empty input port name");
				
			inputPortNameSet.add(inputPortName);
		}

		String[] inputPortNames = inputPortNameSet.toArray(new String[inputPortNameSet.size()]);
		if (inputPortNames.length == 0)
			addWarning("Warning: no input ports specified for Tally");

		DoubleExpression expression = null;
		try { expression = modelContainer.createDoubleExpression(name, exprStr, 
			inputPortNames, modelContainer, null); }
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
			pe); }
		
		genModelElement(element, expression);
		
		NodeList ports = getElements(element, "port");
		for (int i = 0; i < ports.getLength(); i++)
			genPort((Element)(ports.item(i)), expression);
		
		expression.conditionalLayout();

		return expression;
	}


	protected NotExpression genNotExpression(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"Expression with no name attribute");
		
		NotExpression expression = null;
		try { expression = modelContainer.createNotExpression(name, modelContainer, null); }
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
			pe); }

		genModelElement(element, expression);
		
		return expression;
	}


	protected Summation genSummation(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, modelContainer.getFullName(),
			"Summation with no name attribute");
		
		Summation summation = null;
		try { summation = modelContainer.createSummation(name, modelContainer, null); }
		catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
			pe); }

		genModelElement(element, summation);
		
		return summation;
	}


	protected Conduit genConduit(Element element, ModelContainer modelContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		
		String templateFullName = translateAttribute(element.getAttribute("type"));
		if (templateFullName.equals("")) templateFullName = null;
		
		String portAPath = null;
		String portAStr = element.getAttribute("portA");
		if (! portAStr.equals("")) portAPath = translateNodePath(portAStr);
		
		String portBPath = null;
		String portBStr = element.getAttribute("portB");
		if (! portBStr.equals("")) portBPath = translateNodePath(portBStr);
		
		String portAName = "Unidentified Port A";
		String portBName = "Unidentified Port B";
		

		// Try to find a PortedContainer named <subCompName> within the modelContainer.
		
		ModelElement me = null;
		
		Port portA = null;
		if (portAPath != null)
		{
			try { me = modelContainer.findModelElement(portAPath); }
			catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
				portAPath, pe); }
	
			if (me == null) throw new GenException(element, modelContainer.getFullName(),
				"Port " + portAPath + " not found");
			
			if (! (me instanceof Port)) throw new GenException(element, modelContainer.getFullName(),
				portAPath + " is not a Port");
			
			portA = (Port)me;
			portAName = portA.getName();
		}

		Port portB = null;
		if (portBPath != null)
		{
			try { me = modelContainer.findModelElement(portBPath); }
			catch (ParameterError pe) { throw new GenException(
				element, modelContainer.getFullName(), portBPath, pe); }
			
			if (me == null) throw new GenException(element, modelContainer.getFullName(),
				"Port " + portBPath + " not found");
			
			if (! (me instanceof Port)) throw new GenException(element, modelContainer.getFullName(),
				portBPath + " is not a Port");
			
			portB = (Port)me;
			portBName = portB.getName();
		}
		
		if (name.equals("")) name = portAName + "_to_" + portBName 
			+ "_" + String.valueOf(connectorNumber++);

		Template template = resolveTemplateName(element, modelContainer, templateFullName);
		
		Conduit conduit = null;
		try
		{
			if (template == null)
				conduit = modelContainer.createConduit(name, portA, portB, modelContainer, null);
			else
				conduit = modelContainer.createConduit(name, 
						template, portA, portB, modelContainer, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); throw new GenException(
			element, modelContainer.getFullName(), pe); }
		
		genModelElement(element, conduit);
		
		if (conduit instanceof Template)
		{
			NodeList menuItems = getElements(element, "menuitem");
			for (int i = 0; i < menuItems.getLength(); i++)
				genMenuItem((Element)(menuItems.item(i)), (MenuOwner)conduit);
		}

		String inflectionString = translateAttribute(element.getAttribute("inflections"));
		if (! inflectionString.equals(""))
		{
			// Parse a series of 1 or more pairs of real numbers. Each pair is
			// separated from the next pair by a semicolon. The numbers making a
			// pair are separated by a comma.
			
			String[] pairs = inflectionString.split(";");
			
			int noOfPairs = 0;
			for (String pair : pairs)
			{
				noOfPairs++;
				
				String[] numbers = pair.split(",");
				if (numbers.length != 2) throw new GenException(element, modelContainer.getFullName(),
					"Number pair " + numbers + " does not have two numbers.");
				
				double x = 0;
				double y = 0;
			
				try { x = Double.parseDouble(numbers[0]); }
				catch (NumberFormatException nfe) { throw new GenException(element, 
					modelContainer.getFullName(), "Number '" + numbers[0] + "' must be a double"); }
			
				try { y = Double.parseDouble(numbers[1]); }
				catch (NumberFormatException nfe) { throw new GenException(element, 
					modelContainer.getFullName(), "Number '" + numbers[1] + "' must be a double"); }
					
				try { conduit.insertInflectionPointAt(x, y, noOfPairs-1); }
				catch (ParameterError pe) { throw new GenException(element, modelContainer.getFullName(),
					pe); }
			}
		}

		if (conduit instanceof Template)
		{
			NodeList menuItems = getElements(element, "menuitem");
			for (int i = 0; i < menuItems.getLength(); i++)
				genMenuItem((Element)(menuItems.item(i)), (Template)conduit);
		}

		return conduit;
	}
	
	
	/**
	 * Convenience method to find a Port identified by a <conduit>.tag.
	 *
	 * portPath has two possible forms: (1) �compABC.port123�, where �compABC� is
	 * a sub-component of the container that owns the conduit; or (2) �port123�,
	 * where �port123� belongs to the enclosing ModelContainer.
	 */
	 
	protected Port getPort(ModelContainer modelContainer, String portPath)
	throws
		ElementNotFound
	{
		if (portPath.equals("")) throw new ElementNotFound(
			"Port not specified for conduit " + portPath);

		String[] portPathParts = portPath.split("\\.");
		String portName = null;
		PortedContainer subComp = null;
		
		if (portPathParts.length == 1) // port belongs to modelContainer.
		{
			portName = portPathParts[0];
			try { subComp = (PortedContainer)modelContainer; }
			catch (ClassCastException cce) { throw new ElementNotFound(
				modelContainer.getName() + " must be a ported container " +
					"(an Activity or a Function) to have ports"); }
		}
		else if (portPathParts.length == 2) // port belongs to a sub-component
			// of modelContainer.
		{
			String subCompName = portPathParts[0];
			portName = portPathParts[1];

			ModelElement me = null;
			try { me = modelContainer.getSubcomponent(subCompName); }
			catch (ElementNotFound enf) { throw new ElementNotFound(
				"Component " + subCompName + " not found within " +
					modelContainer.getName()); }
			
			try { subComp = (PortedContainer)me; }
			catch (ClassCastException cce) { throw new ElementNotFound(
				"Component " + subCompName + " does cannot have ports"); }
		}
		else throw new ElementNotFound(
			"In a Conduit, a Port must be specified using the syntax <comp>.<port>;" +
				" found " + portPath);


		// Try to find a Port named <portName> within the sub-component.
		
		ModelElement me = null;
		try { me = subComp.getSubcomponent(portName); }
		catch (ElementNotFound enf) { throw new ElementNotFound(
			"Port '" + portName + "' not found in component '" + subComp.getName() + "'"); }
		
		Port port = null;
		try { port = (Port)me; }
		catch (ClassCastException cce) { throw new ElementNotFound(
			portName + " is not a Port"); }
		
		return port;
	}
	
	
	protected Port genPort(Element element, PortedContainer portedContainer)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) name = null;
		
		if ((! translateAttribute(element.getAttribute("width")).equals("")) ||
			(! translateAttribute(element.getAttribute("height")).equals("")))
			throw new GenException(element, portedContainer.getFullName(),
				"Cannot set the height or width of a Port.");

		if ((! translateAttribute(element.getAttribute("x")).equals("")) ||
			(! translateAttribute(element.getAttribute("y")).equals("")))
			throw new GenException(element, portedContainer.getFullName(),
				"Cannot set the x or y location of a Port; use the 'side' attribute.");
		
		String blackString = translateAttribute(element.getAttribute("black"));
		boolean black = false;
		if (blackString.equals("true")) black = true;

		PortDirectionType direction = PortDirectionType.bi;
		String directionString = translateAttribute(element.getAttribute("direction"));
		if (directionString.equals("")) direction = PortDirectionType.bi;
		else if (directionString.equals("input")) direction = PortDirectionType.input;
		else if (directionString.equals("output")) direction = PortDirectionType.output;
		else if (directionString.equals("bi")) direction = PortDirectionType.bi;
		else throw new GenException(element, portedContainer.getFullName(),
			"Unrecognized port direction: " + directionString);

		String sideString = translateAttribute(element.getAttribute("side"));
		if (sideString.equals("")) throw new GenException(element, portedContainer.getFullName(),
			"Must specify the side for a port");
		Position side = Position.top;
		if (sideString.equals("top")) side = Position.top;
		else if (sideString.equals("bottom")) side = Position.bottom;
		else if (sideString.equals("left")) side = Position.left;
		else if (sideString.equals("right")) side = Position.right;
		else throw new GenException(element, portedContainer.getFullName(),
			"Unrecognized value for 'side'");

		Port port = null;
		try { port = portedContainer.createPort(name, black, direction, side, portedContainer, null); }
		catch (ParameterError pe) { throw new GenException(element, portedContainer.getFullName(),
			pe); }
		
		genModelElement(element, port);
		
		return port;
	}
	
	
	protected State genState(Element element, EventProducer eventProducer)
	throws GenException
	{
		if (! (eventProducer instanceof PortedContainer)) throw new GenException(
			element, eventProducer.getFullName(), "In order to have state, event producer '" +
			eventProducer.getName() + "' must be a PortedContainer");
		
		PortedContainer stateOwner = (PortedContainer)eventProducer;
		
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) name = null;
		
		State state = null;
		try { state = stateOwner.createState(name, stateOwner, null); }
		catch (ParameterError pe) { throw new GenException(element, eventProducer.getFullName(),
			pe); }
		
		genModelElement(element, state);
		
		// Generate predefined events, if any.
		
		NodeList nodes = getElements(element, "pre_event");
		for (int i = 0; i < nodes.getLength(); i++)
			genPredefinedEvent((Element)(nodes.item(i)), state);
		
		// Generate port bindings, if any.
		
		nodes = getElements(element, "port_binding");
		for (int i = 0; i < nodes.getLength(); i++)
			genPortBinding((Element)(nodes.item(i)), state);
		
		state.conditionalLayout();
		
		return state;
	}
	
	
	protected void genPortBinding(Element element, State state)
	throws GenException
	{
		String portString = translateAttribute(element.getAttribute("port"));
		if (portString.equals("")) throw new GenException(element, state.getFullName(),
			"No port specified for port binding");
		
		EventProducer eventProducer = state.getEventProducer();
		
		if (! (eventProducer instanceof PortedContainer)) throw new GenException(
			element, state.getFullName(),
			"State is not owned by a Ported Container, so it cannot have a port binding");
		
		ModelElement me = null;
		try { me = ((PortedContainer)eventProducer).getSubcomponent(portString); }
		catch (ElementNotFound enf) { throw new GenException(element, state.getFullName(),
			"Unable to find " + portString + " within " + eventProducer.getName()); }
		
		if (! (me instanceof Port)) throw new GenException(element, state.getFullName(),
			me.getName() + " is not a Port");
		
		Port port = (Port)me;
		
		try { state.bindPort(port); }
		catch (ParameterError pe) { throw new GenException(element, state.getFullName(), pe); }
	}
	
	
	private DateFormat dateFormat = DateFormat.getInstance();
		// Format is completely numeric, such as 12.13.52 or 3:30pm
	
	
	protected void genPredefinedEvent(Element element, State state)
	throws GenException
	{
		PredefinedEvent event = null;
		
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) name = null;
		
		String timeString = translateAttribute(element.getAttribute("time"));
		if (timeString.equals("")) timeString = "0";
		
		String valueString = translateAttribute(element.getAttribute("value"));
		if (valueString.equals("")) throw new GenException(element, state.getFullName(),
			"A predefined event must have a value");
		
		boolean pulse = false;
		String pulseString = translateAttribute(element.getAttribute("pulse"));
		if (pulseString.equalsIgnoreCase("true")) pulse = true;
		
		PredefinedEvent preEvent = null;
		try { preEvent = state.predefineEvent(name, timeString, valueString, state, null); }
		catch (ParameterError pe) { throw new GenException(element, state.getFullName(),
			pe); }
		
		preEvent.setPulse(pulse);

		genModelElement(element, preEvent);
	}
	
	
	protected AttributeStateBinding genAttributeStateBinding(Element element,
		ModelAttribute attribute)
	throws GenException
	{
		String statePath = translateNodePath(element.getAttribute("state"));
		if (statePath.equals("")) throw new GenException(element, attribute.getFullName(),
			"binding must specify a state");
		
		//String kindString = translateAttribute(element.getAttribute("kind"));
		//if (kindString.equals("")) throw new GenException(element, attribute.getFullName(),
		//	"binding must specify a kind (average or most_recent)");
		
		//AttrStateBindingType kind = AttrStateBindingType.average;
		//if (kindString.equals("average")) kind = AttrStateBindingType.average;
		//else if (kindString.equals("most_recent")) kind = AttrStateBindingType.most_recent;
		//else throw new GenException(element, attribute,
		//	"Unrecognized attribute state binding kind: " + kindString);
		
		ModelDomain attrDomain = attribute.getModelDomain();
		
		PersistentNode elt = null;
		try { elt = modelEngine.getNodeForPath(statePath); }
		catch (ElementNotFound enf) { throw new GenException(element, attribute.getFullName(),
			"State '" + statePath + "' not found. The State must exist in the database\n" +
			"or, if defined in the XML being read, must be in an earlier <model_domain>"); }
		catch (ParameterError pe) { throw new GenException(element, attribute.getFullName(),
			pe); }
		
		State state = null;
		try { state = (State)elt; }
		catch (ClassCastException cce) { throw new GenException(element, attribute.getFullName(),
			"Element '" + statePath + "' is not a state"); }
		
		if (state.getModelDomain() == attrDomain) throw new GenException(element, attribute.getFullName(),
			"The State of the Attribute-State binding must be in a different Domain than the Attribute");
		
		AttributeStateBinding binding = null;
		
		try { binding = attribute.bindToState(state, attribute, null); }
		catch (ParameterError pe) { throw new GenException(element, attribute.getFullName(),
			pe); }
		
		genModelElement(element, binding);
		
		return binding;
	}
	
	
	protected DecisionDomain genDecisionDomain(Element element, boolean isMotif)
	throws GenException
	{
		String domainName = translateAttribute(element.getAttribute("name"));
		if (domainName.equals("")) throw new GenException(element, 
			"A decision domain must have a name");
		
		String sourceName = translateAttribute(element.getAttribute("source_name"));
		if (sourceName.equals("")) sourceName = globalSourceName;
		
		String sourceURLStr = translateAttribute(element.getAttribute("source_url"));
		if (sourceURLStr.equals("")) sourceURLStr = globalSourceURLStr;
		
		URL sourceURL;
		if (sourceURLStr == null) sourceURL = null;
		else try { sourceURL = new URL(sourceURLStr); }
		catch (java.net.MalformedURLException ex) { throw new GenException(element,
			"source_url", ex); }
		
		String version = translateAttribute(element.getAttribute("version"));
		if (version.equals("")) version = globalVersion;

		String motifsStr = translateAttribute(element.getAttribute("motifs"));
		Set<String> motifNames = null;
		if (! motifsStr.equals(""))
		{
			String[] motifStrs = motifsStr.split(",");
			motifNames = new HashSet<String>();
			for (String motifStr : motifStrs) motifNames.add(translateAttribute(motifStr));
		}
		
		NodeList menuItems = getElements(element, "menuitem");
		
		DecisionDomain dd = null;
		try
		{
			if (isMotif)
				dd = modelEngine.createDecisionDomainMotifDefPersistentNode(domainName,
					getCollidingDomainNameGeneratesException());
			else
				dd = modelEngine.createDecisionDomainPersistentNode(domainName,
					getCollidingDomainNameGeneratesException());
		}
		catch (Exception pe) { throw new GenException(element, pe); }
		
		dd.setDeclaredName(domainName);
		dd.setSourceName(sourceName);
		dd.setSourceURL(sourceURL);
		dd.setDeclaredVersion(version);
		
		String actualDecisionDomainName = dd.getName();
		if (! actualDecisionDomainName.equals(domainName))
		{
			domainNameSubstitutions.put(domainName, actualDecisionDomainName);
		}
		
		if (isMotif && (menuItems != null))
		{
			for (int i = 0; i < menuItems.getLength(); i++)
				genMenuItem((Element)(menuItems.item(i)), (MotifDef)dd);
		}
		
		// Add to this Domain the motifs that this Domain claims to use.
		
		if (motifNames != null)
		{
			for (String motifName : motifNames)
			{
				MotifDef md;
				
				String substitutedName = domainNameSubstitutions.get(motifName);
				if (substitutedName != null) motifName = substitutedName;
				
				try { md = getModelEngineLocal().getMotifDefPersistentNode(motifName); }
				catch (ElementNotFound enf) { throw new GenException(
					element, "Motif '" + motifName + "' not found"); }
				catch (Exception ex) { throw new GenException(element, ex); }
				
				if (! (md instanceof DecisionDomainMotifDef)) throw new GenException(
					"Motif '" + motifName + "' is not a DecisionDomainMotifDef");
				
				dd.addMotifDef(md);
			}
		}

		genDecisionElement(element, dd);
		
		NodeList nodes = getElements(element, "references");
		for (int i = 0; i < nodes.getLength(); i++)
			genNamedReference((Element)(nodes.item(i)), dd);

		// Generate DecisionPoints.
		nodes = getElements(element, "decision_point");
		for (int i = 0; i < nodes.getLength(); i++)
			genDecisionPoint((Element)(nodes.item(i)), dd);
		
		nodes = getElements(element, "decision");
		for (int i = 0; i < nodes.getLength(); i++)
			genDecision((Element)(nodes.item(i)), dd);
		
		// Generate Precursor Relations.
		nodes = getElements(element, "precursor");
		for (int i = 0; i < nodes.getLength(); i++)
			genPrecursorRelation((Element)(nodes.item(i)), dd);

		// Generate Dependencies.
		nodes = getElements(element, "dependency");
		for (int i = 0; i < nodes.getLength(); i++)
			genDependency((Element)(nodes.item(i)), dd);

		return dd;
	}
	
	
	protected DecisionPoint genDecisionPoint(Element element,
		DecisionDomain decisionDomain)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) name = null;
		
		String nativeImplString = translateAttribute(element.getAttribute("native"));
		if (nativeImplString.equals("")) throw new GenException(element, decisionDomain.getFullName(),
			"A decision point requires a 'native' attribute");
		
		Class nativeImplClass = null;
		try { nativeImplClass = Class.forName(nativeImplString); }
		catch (ClassNotFoundException cnfe) { throw new GenException(element, decisionDomain.getFullName(),
			nativeImplString + " not found"); }

		DecisionPoint decisionPoint = null;
		try { decisionPoint = decisionDomain.createDecisionPoint(name,
			nativeImplClass); }
		catch (ParameterError pe) { throw new GenException(element, decisionDomain.getFullName(),
			pe); }
		
		genDecisionElement(element, decisionPoint);
		
		// Generate Parameters.

		NodeList nodes = getElements(element, "decision_function");
		for (int i = 0; i < nodes.getLength(); i++)
			genDecisionFunction((Element)(nodes.item(i)), decisionPoint);
		
		nodes = getElements(element, "constant");
		for (int i = 0; i < nodes.getLength(); i++)
			genConstant((Element)(nodes.item(i)), decisionPoint);
		
		nodes = getElements(element, "variable");
		for (int i = 0; i < nodes.getLength(); i++)
			genVariable((Element)(nodes.item(i)), decisionPoint);
		
		nodes = getElements(element, "parameter_alias");
		for (int i = 0; i < nodes.getLength(); i++)
			genParameterAlias((Element)(nodes.item(i)), decisionPoint);
		
		return decisionPoint;
	}
	
	
	protected Decision genDecision(Element element,
		DecisionDomain decisionDomain)
	throws GenException
	{
		throw new GenException("Decisions not implemented yet");
	}


	protected Variable genVariable(Element element, DecisionPoint decisionPoint)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, decisionPoint.getFullName(),
			"No name specified for Variable");
		
		Variable variable;
		try { variable = decisionPoint.createVariable(name); }
		catch (ParameterError pe) { throw new GenException(element, pe); }

		genDecisionElement(element, variable);
		
		// Generate Variable-Attribute bindings.
		NodeList nodes = getElements(element, "attr_binding");
		for (int i = 0; i < nodes.getLength(); i++)
			genVariableAttributeBinding((Element)(nodes.item(i)), variable);
		
		return variable;
	}
	
	
	protected ConstantValue genConstant(Element element, DecisionPoint decisionPoint)
	throws GenException
	{
		throw new GenException("Constants not supported yet");
	}
	
	
	protected ConstantValue genParameterAlias(Element element, DecisionPoint decisionPoint)
	throws GenException
	{
		throw new GenException("ParameterAlias not supported yet");
	}
	
	
	protected PrecursorRelationship genPrecursorRelation(Element element,
		DecisionDomain decisionDomain)
	throws GenException
	{
		String dpAName = translateAttribute(element.getAttribute("source"));
		if (dpAName.equals("")) throw new GenException(element, decisionDomain.getFullName(),
			"Decision point source name not specified");
		
		String dpBName = translateAttribute(element.getAttribute("target"));
		if (dpAName.equals("")) throw new GenException(element, decisionDomain.getFullName(),
			"Decision point target name not specified");
		
		DecisionPoint decisionPointA = null;
		try { decisionPointA = decisionDomain.getDecisionPoint(dpAName); }
		catch (ParameterError pe) { throw new GenException(element, decisionDomain.getFullName(),
			pe); }

		if (decisionPointA == null) throw new GenException(element, decisionDomain.getFullName(),
			"Decision Point '" + dpAName + "' not found");
		
		DecisionPoint decisionPointB = null;
		try { decisionPointB = decisionDomain.getDecisionPoint(dpBName); }
		catch (ParameterError pe) { throw new GenException(element, decisionDomain.getFullName(),
			pe); }
		
		if (decisionPointB == null) throw new GenException(element, decisionDomain.getFullName(),
			"Decision Point '" + dpBName + "' not found");
		
		PrecursorRelationship reln = decisionDomain.createPrecursorRelationship(
			decisionPointA, decisionPointB);
		
		genDecisionElement(element, reln);
		
		return reln;
	}
	
	
	protected Dependency genDependency(Element element, DecisionDomain decisionDomain)
	throws GenException
	{
		String kind = translateAttribute(element.getAttribute("kind"));
		
		String dpAString = translateAttribute(element.getAttribute("source"));
		if (dpAString.equals("")) throw new GenException(element, decisionDomain.getFullName(),
			"A precludes must have a 'source' attribute");
		
		String dpBString = translateAttribute(element.getAttribute("target"));
		if (dpBString.equals("")) throw new GenException(element, decisionDomain.getFullName(),
			"A precludes must have a 'target' attribute");
		
		DecisionPoint dpA = null;
		try { dpA = decisionDomain.getDecisionPoint(dpAString); }
		catch (ParameterError pe) { throw new GenException(element, decisionDomain.getFullName(),
			pe); }

		if (dpA == null) throw new GenException(element, decisionDomain.getFullName(),
			"Decision Point '" + dpAString + "' not found");
		
		DecisionPoint dpB = null;
		try { dpB = decisionDomain.getDecisionPoint(dpBString); }
		catch (ParameterError pe) { throw new GenException(element, decisionDomain.getFullName(),
			pe); }
		
		if (dpB == null) throw new GenException(element, decisionDomain.getFullName(),
			"Decision Point '" + dpBString + "' not found");
		
		Dependency dependency = null;
		
		if (kind.equals("precludes")) dependency =
			decisionDomain.createPrecludes(dpA, dpB);
		//else if (kind.equals("requires")) dependency =
		//	decisionDomain.createRequires(dpA, dpB);
		//else if (kind.equals("modifies")) dependency =
		//	decisionDomain.createModifies(dpA, dpB);
		//else if (kind.equals("co-depends-on")) dependency =
		//	decisionDomain.createCoDependency(dpA, dpB);
		else throw new GenException(element, decisionDomain.getFullName(),
			"Unrecognized dependency kind");
		
		genDecisionElement(element, dependency);
		
		return dependency;
	}
	
	
	protected DecisionFunction genDecisionFunction(Element element, DecisionPoint decisionPoint)
	throws GenException
	{
		throw new GenException("Decision Functions are not supported yet");
	}
	
	
	protected VariableAttributeBinding genVariableAttributeBinding(Element element,
		Variable variable)
	throws GenException
	{
		String attrPath = translateNodePath(element.getAttribute("attribute"));
		if (attrPath.equals("")) throw new GenException(element, variable.getFullName(),
			"binding must specify an 'attribute'");
		
		ModelAttribute attr = null;
		PersistentNode node = null;
		
		try { node = ((ModelEngineLocalPojoImpl)modelEngine).findNode(attrPath); }
		catch (ParameterError pe) { throw new GenException(element, variable.getFullName(),
			pe); }
		
		if (node == null) throw new GenException(element, variable.getFullName(),
			"Attribute '" + attrPath + "' not found");
		
		try { attr = (ModelAttribute)node; }
		catch (ClassCastException cce) { throw new GenException(element, variable.getFullName(),
			"Attribute '" + attrPath + "' not found"); }
		
		VariableAttributeBinding binding = variable.bindToAttribute(attr);

		genDecisionElement(element, binding);
		
		return binding;
	}
	
	
	protected void genScenario(Element element)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, 
			"No name specified for model scenario");
		
		String domainName = translateAttribute(element.getAttribute("domain"));
		if (domainName.equals("")) throw new GenException(element, 
			"No model domain specified for model scenario");
		
		String substitutedName = domainNameSubstitutions.get(domainName);
		if (substitutedName != null) domainName = substitutedName;
		
		Domain domain = null;
		try
		{
			domain = modelEngine.getDomainPersistentNode(domainName);
		}
		catch (Exception ioe) { throw new GenException(ioe.getMessage()); }

		if (domain == null) throw new GenException(element, 
			"Domain " + domainName + " not found");
		
		Scenario scenario = null;
		try { scenario = domain.createScenario(name); }
		catch (ParameterError pe) { 
			GlobalConsole.printStackTrace(pe);
			throw new GenException(element, pe); }

		if (domain instanceof ModelDomain)
		{
			genModelElement(element, (ModelScenario)scenario);
			
			String stateBindingTypeStr = translateAttribute(element.getAttribute(
				"external_state_binding_type"));
	
			Boolean stateBindingType;
			if (stateBindingTypeStr.equals(""))
				stateBindingType = null;
			else if (stateBindingTypeStr.equalsIgnoreCase("average"))
				stateBindingType = new Boolean(true);
			else if (stateBindingTypeStr.equalsIgnoreCase("final"))
				stateBindingType = new Boolean(false);
			else
				throw new GenException(element, "Unrecognized state binding type: '" +
					stateBindingTypeStr + "'.");
			
			String stateBindingGranularityStr = translateAttribute(element.getAttribute(
				"external_state_time_granularity"));
			
			TimePeriodType stateBindingGranularity = TimePeriodType.unspecified;
			if (! stateBindingGranularityStr.equals(""))
				try { stateBindingGranularity = 
					Enum.valueOf(TimePeriodType.class, stateBindingGranularityStr); }
				catch (IllegalArgumentException ex) { throw new GenException(element,
					"unrecognized value for state binding granularity: '" +
					stateBindingGranularityStr + "'"); }

			if (stateBindingType != null)
				((ModelScenario)scenario).setExternalStateBindingType(stateBindingType);
			((ModelScenario)scenario).setExternalStateGranularity(stateBindingGranularity);
		}
		else
			genPersistentNode(element, scenario);
		
		
		// Handle 'based_on' spec.
		
		String baseScenarioName = translateAttribute(element.getAttribute("based_on"));
		if (! baseScenarioName.equals(""))
		{
			Scenario basedOnScenario = null;

			try { basedOnScenario = domain.getScenario(baseScenarioName); }
			catch (Exception ex) { throw new GenException(element, ex); }
			
			
			// Copy Attributes of base Scenario into the new Scenario.
			
			Set<Attribute> attrs = basedOnScenario.getAttributesWithValues();
			for (Attribute attr : attrs)
			{
				Serializable attrValue = null;
				try { attrValue = basedOnScenario.getAttributeValue(attr); }
				catch (Exception ex) { throw new GenException(element, ex); }
				
				try { scenario.setAttributeValue(attr, attrValue); }
				catch (Exception ex) { throw new GenException(element, ex); }
			}
			
			
			// Set the 'derivedFrom' field.
			
			scenario.setDerivedFrom(basedOnScenario);
		}

		if (domain instanceof ModelDomain)
		{
			ModelScenario modelScenario = (ModelScenario)scenario;
			
			DateFormat df = DateAndTimeUtils.DateTimeFormat;
			
			String startDateStr = translateAttribute(element.getAttribute("start_date"));
			if (! startDateStr.equals(""))
			{
				Date startDate;
				try { startDate = df.parse(startDateStr); }
				catch (ParseException pe) { throw new GenException(element, pe.getMessage()); }
				
				modelScenario.setStartingDate(startDate);
			}
			
			String endDateStr = translateAttribute(element.getAttribute("end_date"));
			if (! endDateStr.equals(""))
			{
				Date endDate;
				try { endDate = df.parse(endDateStr); }
				catch (ParseException pe) { throw new GenException(element, pe.getMessage()); }
				
				modelScenario.setStartingDate(endDate);
			}
			
			String iterLimitStr = translateAttribute(element.getAttribute("iteration_limit"));
			if (! iterLimitStr.equals(""))
			{
				int iterLimit;
				try { iterLimit = Integer.parseInt(iterLimitStr); }
				catch (NumberFormatException ex) { throw new GenException(element, ex.getMessage()); }
				
				modelScenario.setIterationLimit(iterLimit);
			}
			
			String durationStr = translateAttribute(element.getAttribute("duration"));
			if (! durationStr.equals(""))
			{
				long duration;
				try { duration = Long.parseLong(durationStr); }
				catch (NumberFormatException ex) { throw new GenException(element, ex.getMessage()); }
				
				modelScenario.setDuration(duration);
			}
		}
		
		// Handle <attribute_value>.
		
		NodeList nodes = getElements(element, "attribute_value");
		for (int i = 0; i < nodes.getLength(); i++)
			genScenarioAttributeValue((Element)(nodes.item(i)), scenario);
		
		
		if (domain instanceof ModelDomain)
		{
			// Handle <use>.
			
			nodes = getElements(element, "use");
			for (int i = 0; i < nodes.getLength(); i++)
				genUse((Element)(nodes.item(i)), (ModelScenario)scenario);
		
			// Handle <attribute_distribution>.
		
			nodes = getElements(element, "attribute_distribution");
			for (int i = 0; i < nodes.getLength(); i++)
				genAttributeDistribution((Element)(nodes.item(i)), (ModelScenario)scenario);
		}
	}
	
	
	/**
	 * Associates Scenarios with their ScenarioSet, if any.
	 * Must be called AFTER both genScenario and genScenarioSet have
	 * been called for the Element.
	 */
	 
	protected void genScenario_sets(Element element)
	throws GenException
	{
		String domainName = translateAttribute(element.getAttribute("domain"));
		if (domainName.equals("")) throw new GenException(element, 
			"No model domain specified for model scenario");
		
		String substitutedName = domainNameSubstitutions.get(domainName);
		if (substitutedName != null) domainName = substitutedName;
		
		Domain domain = null;
		try
		{
			domain = modelEngine.getDomainPersistentNode(domainName);
		}
		catch (Exception ioe) { throw new GenException(ioe.getMessage()); }

		if (domain == null) throw new GenException(element, 
			"Domain " + domainName + " not found");
		
		String scenarioSetName = translateAttribute(element.getAttribute("scenario_set"));
		if (scenarioSetName.equals("")) scenarioSetName = null;
		
		ScenarioSet scenarioSet = null;
		if (scenarioSetName == null) return;
		
		try { scenarioSet = domain.getScenarioSet(scenarioSetName); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		String scenarioName = translateAttribute(element.getAttribute("name"));
		if (scenarioName.equals("")) throw new GenException(element, 
			"No name specified for scenario");
		
		Scenario scenario = null;
		try { scenario = domain.getScenario(scenarioName); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		// Verify that ScenarioSet's 'scenarioThatWasCopied' field agrees with the
		// 'based_on' Scenario asserted by this scenario.
		
		if (scenarioSet.derivedFrom() != scenario.derivedFrom())
			throw new GenException(
				"Scenario 'based_on' does not agree with Scenario Set 'based_on'");

		scenarioSet.addScenario(scenario);
	}


	protected void genScenarioSet(Element element)
	throws GenException
	{
		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) throw new GenException(element, 
			"No name specified for model scenario");
		
		String domainName = translateAttribute(element.getAttribute("domain"));
		if (domainName.equals("")) throw new GenException(element, 
			"No model domain specified for model scenario");
		
		String substitutedName = domainNameSubstitutions.get(domainName);
		if (substitutedName != null) domainName = substitutedName;
		
		Domain domain = null;
		try
		{
			domain = modelEngine.getModelDomainPersistentNode(domainName);
		}
		catch (Exception ioe) { throw new GenException(ioe.getMessage()); }

		if (domain == null) throw new GenException(element, 
			"Model domain " + domainName + " not found");

		
		// Handle 'based_on' spec.
		
		String baseScenarioName = translateAttribute(element.getAttribute("based_on"));
		if (baseScenarioName.equals("")) throw new GenException(element, 
			"No 'based_on' specified for model scenario set");
		
		Scenario basedOnScenario = null;

		try { basedOnScenario = domain.getScenario(baseScenarioName); }
		catch (Exception ex) { throw new GenException(element, ex); }

		String attrPathString = translateNodePath(element.getAttribute("variable_attribute"));
		if (attrPathString.equals("")) throw new GenException(element, 
			"No 'variable_attribute' specified for model scenario set");
		
		
		// Find the specified Attribute
		
		PersistentNode node = null;
		try { node = domain.findNode(attrPathString); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		catch (ClassCastException cce) { GlobalConsole.printStackTrace(cce); }
		
		if (node == null) throw new GenException(element,
			"Cannot find attribute '" + attrPathString + "'");
		
		if (! (node instanceof Attribute)) throw new GenException(
			"Model element with path '" + attrPathString + "' is not an Attribute");
		
		Attribute attrToVary = (Attribute)node;
		
		
		// Identify values for 

		NodeList nodes = getElements(element, "value");
		Serializable[] values = new Serializable[nodes.getLength()];
		for (int i = 0; i < nodes.getLength(); i++)
		{
			Element valueElt = (Element)(nodes.item(i));
			String valueString = translateAttribute(valueElt.getAttribute("value"));
			values[i] = getValueFromString(valueString);
		}

		ScenarioSet scenarioSet = null;
		try { scenarioSet = domain.createScenarioSet(name, basedOnScenario, attrToVary, null); }
		catch (Exception ex) { throw  new GenException(element, ex); }
		
		
		if (domain instanceof ModelDomain)
		{
			// Get values for start date, end date, iteration limit, and duration.
			
			ModelScenarioSet mss = (ModelScenarioSet)scenarioSet;
			DateFormat df = DateAndTimeUtils.DateTimeFormat;
			
			String startDateStr = translateAttribute(element.getAttribute("start_date"));
			if (! startDateStr.equals(""))
			{
				Date startDate;
				try { startDate = df.parse(startDateStr); }
				catch (ParseException pe) { throw new GenException(element, pe.getMessage()); }
				
				mss.setStartingDate(startDate);
			}
			
			String endDateStr = translateAttribute(element.getAttribute("end_date"));
			if (! endDateStr.equals(""))
			{
				Date endDate;
				try { endDate = df.parse(endDateStr); }
				catch (ParseException pe) { throw new GenException(element, pe.getMessage()); }
				
				mss.setStartingDate(endDate);
			}
			
			String iterLimitStr = translateAttribute(element.getAttribute("iteration_limit"));
			if (! iterLimitStr.equals(""))
			{
				int iterLimit;
				try { iterLimit = Integer.parseInt(iterLimitStr); }
				catch (NumberFormatException ex) { throw new GenException(element, ex.getMessage()); }
				
				mss.setIterationLimit(iterLimit);
			}
			
			String durationStr = translateAttribute(element.getAttribute("duration"));
			if (! durationStr.equals(""))
			{
				int duration;
				try { duration = Integer.parseInt(durationStr); }
				catch (NumberFormatException ex) { throw new GenException(element, ex.getMessage()); }
				
				mss.setDuration(duration);
			}

			genModelElement(element, (ModelScenarioSet)scenarioSet);
		}
		else
			genPersistentNode(element, scenarioSet);
	}
	
	
	protected void genUse(Element element, ModelScenario modelScenario)
	throws GenException
	{
		String scenarioName = translateNodePath(element.getAttribute("scenario"));
		if (scenarioName.equals("")) throw new GenException(element, modelScenario.getFullName(),
			"No Scenario specified in use clause.");
		
		String domainName = translateNodePath(element.getAttribute("for"));
		if (domainName.equals("")) throw new GenException(element, modelScenario.getFullName(),
			"No Domain specified in use clause.");
		
		ModelDomain domain = null;
		try { domain = modelEngine.getModelDomainPersistentNode(domainName); }
		catch (Exception ex) { throw new GenException(element, modelScenario.getFullName(), ex); }
		
		ModelScenario boundScenario = null;
		try { boundScenario = domain.getModelScenario(scenarioName); }
		catch (Exception ex) { throw new GenException(element, modelScenario.getFullName(), ex); }
		
		modelScenario.addBoundScenario(boundScenario);
	}


	protected void genScenarioAttributeValue(Element element, Scenario scenario)
	throws GenException
	{
		String path = translateNodePath(element.getAttribute("path"));
		if (path.equals("")) throw new GenException(element, scenario.getFullName(),
			"No path specified for attribute_value");
		
		String valueString = translateAttribute(element.getAttribute("value"));
		if (valueString.equals("")) valueString = null;
		
		Domain domain = scenario.getDomain();
		PersistentNode node = null;
		try { node = (Attribute)(domain.findNode(path)); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		catch (ClassCastException cce) { GlobalConsole.printStackTrace(cce); }
		
		if (node == null) throw new GenException(element, scenario.getFullName(),
			"Cannot find attribute '" + path + "'");
		
		if (! (node instanceof Attribute)) throw new GenException(
			"Node with path '" + path + "' is not an Attribute");
		
		Attribute attribute = (Attribute)node;
		
		Serializable value = getValueFromString(valueString);
		
		// Note: For DoubleExpression, the value should not be parsed, but should
		// be set as a String. This works ok because the parser will return a String
		// if it cannot recognize the type. In cases where the String value would
		// be recognized (erroneously) as another type, the XML should use the 
		// syntax "new String(\"the actual string\")"

		
		try { scenario.setAttributeValue(attribute, value); }
		catch (ParameterError pe) { throw new GenException(element, scenario.getFullName(),
			"path=\"" + path + "\"", pe); }
	}
	
		
	protected void genAttributeDistribution(Element element, ModelScenario modelScenario)
	throws GenException
	{
		throw new GenException(element, modelScenario.getFullName(),
			"<attribute_distribution> not supported yet.");
	}
	
		
	protected void genSimulate(Element element)
	throws GenException
	{
		// Wait for task to complete.
		
		String waitforString = translateAttribute(element.getAttribute("waitfor"));
		if (! waitforString.equals(""))
		{
			try { waitForTask(waitforString); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); throw new GenException(
				element, "While waiting for task " + waitforString + " to complete", ex); }
		}
		

		String name = translateAttribute(element.getAttribute("name"));
		if (name.equals("")) name = null;
		if ((! (name == null)) && (taskHandles.containsKey(name))) throw new GenException(
			element, "The task name " + name + " is already in use");
		
		String domainName = translateAttribute(element.getAttribute("model_domain"));
		if (domainName.equals("")) throw new GenException(element, 
			"No model_domain specified in simulate statement.");
		
		String substitutedName = domainNameSubstitutions.get(domainName);
		if (substitutedName != null) domainName = substitutedName;
		
		String scenarioName = translateAttribute(element.getAttribute("model_scenario"));
		if (scenarioName.equals("")) throw new GenException(element, 
			"No model_scenario specified in simulate statement.");

		NodeList nodes = getElements(element, "set_current_scenario");
		for (int i = 0; i < nodes.getLength(); i++)
			genSetCurrentScenario((Element)(nodes.item(i)));
		
		int iterations = 0;
		String iterString = translateAttribute(element.getAttribute("max_iterations"));
		if (! iterString.equals("")) try
		{
			iterations = Integer.parseInt(iterString);
		}
		catch (NumberFormatException nfe)
		{
			throw new GenException(element, "max_iterations is not a valid integer");
		}
		
		int runs = 1;
		String runString = translateAttribute(element.getAttribute("runs"));
		if (! runString.equals("")) try
		{
			runs = Integer.parseInt(runString);
		}
		catch (NumberFormatException nfe)
		{
			throw new GenException(element, "runs is not a valid integer");
		}
		
		long duration = 0;
		String durationString = translateAttribute(element.getAttribute("max_sim_time"));
		if (! durationString.equals("")) try
		{
			duration = Long.parseLong(durationString);
		}
		catch (NumberFormatException nfe)
		{
			throw new GenException(element, "max_sim_time is not a valid long integer");
		}
		
		String epochString = translateAttribute(element.getAttribute("initial_epoch"));
		if (epochString.equals("")) epochString = null;
		Date initialEpoch = null;
		if (epochString != null) try
		{
			initialEpoch = dateFormat.parse(epochString);
		}
		catch (ParseException pe) { throw new GenException(element, 
			"Ill-formatted date string: " + epochString); }
		
		epochString = translateAttribute(element.getAttribute("final_epoch"));
		if (epochString.equals("")) epochString = null;
		Date finalEpoch = null;
		if (epochString != null) try
		{
			finalEpoch = dateFormat.parse(epochString);
		}
		catch (ParseException pe) { throw new GenException(element, 
			"Ill-formatted date string: " + epochString); }
		
		boolean repeatable = true;
		String repeatableString = translateAttribute(element.getAttribute("repeatable"));
		if (repeatableString.equals("")) repeatable = true;
		else if (repeatableString.equals("false")) repeatable = false;
		else if (repeatableString.equals("true")) repeatable = true;
		else throw new GenException(element, 
			"Value of 'repeatable' attribute unrecognized");
		
		Integer handle = null;
		long durationInMs = duration * DateAndTimeUtils.MsInADay;
		try { handle =
			modelEngine.simulate(this.getClientId(), domainName, scenarioName, 
				initialEpoch, finalEpoch,
				iterations, false, runs, durationInMs, this.peerListener, repeatable); }
		catch (Exception be) { 
			GlobalConsole.printStackTrace(be);
			throw new GenException(element, be); }
		
		if (name != null)  // record the name and map it to the handle.
		{
			taskHandles.put(name, handle);
		}
	}
	
	
	protected void genSetCurrentScenario(Element element)
	throws GenException
	{
		String domainName = translateAttribute(element.getAttribute("domain"));
		if (domainName.equals("")) throw new GenException(element, 
			"No domain specified in set_current_scenario statement.");
		
		String substitutedName = domainNameSubstitutions.get(domainName);
		if (substitutedName != null) domainName = substitutedName;
		
		String scenarioName = translateAttribute(element.getAttribute("scenario"));
		if (scenarioName.equals("")) throw new GenException(element, 
			"No scenario specified in set_current_scenario statement.");

		//ModelDomainSer modelDomainSer = null;
		ModelDomain modelDomain = null;
		try
		{
			modelDomain = modelEngine.getModelDomainPersistentNode(domainName);
			//modelDomain =
			//	(ModelDomain)(modelEngine.getPersistentNode(modelDomainSer.getNodeId()));
		}
		catch (Exception ioe) { throw new GenException(element, ioe); }
		
		if (modelDomain == null) throw new GenException(element,
			"Model domain " + domainName + " not found");
		
		ModelScenario modelScenario = null;
		try { modelScenario = modelDomain.getModelScenario(scenarioName); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		try { modelDomain.setCurrentModelScenario(modelScenario); }
		catch (Exception ex) { throw new GenException(element, ex); }
	}
	

	protected void genUpdateVariable(Element element)
	throws GenException
	{
		String domainName = translateAttribute(element.getAttribute("decision_domain"));
		if (domainName.equals("")) throw new GenException(element, 
			"No decision_domain specified in simulate statement.");
		
		String substitutedName = domainNameSubstitutions.get(domainName);
		if (substitutedName != null) domainName = substitutedName;
		
		String scenarioName = translateAttribute(element.getAttribute("decision_scenario"));
		if (scenarioName.equals("")) throw new GenException(element, 
			"No decision_scenario specified in update_variable statement.");
		
		String decisionPointName = translateAttribute(element.getAttribute("decision_point"));
		if (decisionPointName.equals("")) throw new GenException(element, 
			"No decision point specified in update_variable statement");
		
		String variableName = translateAttribute(element.getAttribute("variable"));
		if (variableName.equals("")) throw new GenException(element, 
			"No variable specified in update_variable statement");
		
		Vector valueVector = new Vector<Serializable>();
		
		Integer[] handles = null;
		try { handles = modelEngine.updateVariable(this.getClientId(), 0, domainName, 
			scenarioName, decisionPointName, variableName, valueVector); }
		catch (Exception be) { throw new GenException(element, be); }
	}
	
	
	protected void genExportEvents(Element element)
	throws GenException
	{
		// Wait for task to complete.
		
		String waitforString = translateAttribute(element.getAttribute("waitfor"));
		if (! waitforString.equals(""))
		{
			try { waitForTask(waitforString); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); throw new GenException(
				element, "While waiting for task " + waitforString + " to complete", ex); }
		}
		

		boolean tabbed = true;  // default.
		boolean html = false;
		boolean arc = false;
		
		String formatString = translateAttribute(element.getAttribute("format"));
		
		// Parse the format string into its separate components.
		
		String[] formatStrings = formatString.split(",");
		int n = formatStrings.length;
		if (n > 0) tabbed = false;
		for (int i = 0; i < n; i++)
		{
			formatStrings[i] = formatStrings[i].trim().toLowerCase();
			if (formatStrings[i].equals("tabbed")) tabbed = true;
			else if (formatStrings[i].equals("html")) html = true;
			else if (formatStrings[i].equals("archive")) arc = true;
			else if (formatStrings[i].equals("")) tabbed = true;
			else throw new GenException(element,
				"Unrecognized output format type: " + formatStrings[i]);
		}
		
		String filePath = translateAttribute(element.getAttribute("file"));
		if (filePath.equals("")) throw new GenException(element, 
			"No file path specified for export statement");
		
		String valFilePath = translateAttribute(element.getAttribute("val_archive_file"));
		if (valFilePath.equals("")) valFilePath = null;
		
		
		/*
		If archive is specified and if the archive file exists and is the file
		specified by the val_archive_file attribute, it will not be over-written.
		 */
		
		File arcFile = null;
		File valFile = null;
		
		if (arc) arcFile = new File(filePath + ".archive");
		if (valFilePath != null) valFile = new File(valFilePath);
		
		if ((arcFile != null) && (valFile != null))
		{
			if (arcFile.exists())
			{
				if (arcFile.equals(valFile))
				{
					addWarning(
						"Warning: arc is specified but val_archive_file is also\n" +
						"specified and they are the same file; file will NOT be\n" +
						"over-written");
						
					arc = false;
				}
			}
		}

		
		String modelDomainString = translateAttribute(element.getAttribute("model_domain"));
		if (modelDomainString.equals("")) throw new GenException(element,
			"Must specify a model domain");		
		
		String modelScenarioString = translateAttribute(element.getAttribute("model_scenario"));
		if (modelScenarioString.equals("")) throw new GenException(element,
			"Must specify a model scenario");
		
		ModelDomain domain;
		try { domain = modelEngine.getModelDomainPersistentNode(modelDomainString); }
		catch (Exception ex) { throw new GenException(element, ex); }
		ModelScenario scenario;
		try { scenario = domain.getModelScenario(modelScenarioString); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		NodeList nodes = getElements(element, "path");
		Vector<String> pathStrings = new Vector<String>();
		Vector<String> stateIds = new Vector<String>();
		for (int i = 0; i < nodes.getLength(); i++)  // each path
		{
			String p = translateNodePath(((Element)(nodes.item(i))).getAttribute("path"));
			if (p.equals("")) throw new GenException(element, "Empty string for path");
			else
			{
				pathStrings.add(p);
				
				// Identify the State.
				
				PersistentNode elt = null;
				try
				{
					try { elt = modelEngine.getNodeForPath(p); }
					catch (ElementNotFound enf)
					{
						elt = modelEngine.getNodeForPath(domain.getName() + "." + p);
					}
				}
				catch (Exception ex)
				{
					throw new GenException("for path '" + p + "'", ex);
				}
				
				if (! (elt instanceof State)) throw new GenException(
					"Path '" + p + "' does not specify a State");
				
				stateIds.add(elt.getNodeId());
			}
		}
		
		NumberFormat numberFormat = NumberFormat.getInstance();
		String maxFractionDigitsString = translateAttribute(element.getAttribute("max_fraction_digits"));
		if (! maxFractionDigitsString.equals(""))
		{
			Number nn = null;
			try { nn = numberFormat.parse(maxFractionDigitsString); }
			catch (ParseException pe) { throw new GenException(element,
				"Unable to parse " + maxFractionDigitsString); }
			
			numberFormat.setMaximumFractionDigits(nn.intValue());
		}
		
		
		// Get the names of the simulation runs that exist.
		List<String> simRunNames = null;
		try { simRunNames = modelEngine.getSimulationRuns(this.getClientId(),
			modelDomainString, modelScenarioString); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		
		// Open the output files.

		PrintWriter tabbedpw = null;
		PrintWriter htmlpw = null;
		PrintWriter arcos = null;
		
		if (tabbed)
			try { tabbedpw = new PrintWriter(new BufferedWriter(
				new FileWriter(new File(filePath + ".tab")))); }
			catch (Exception ex) { throw new GenException(element, 
				"Error writing output file", ex); }

		if (html)
			try { htmlpw = new PrintWriter(new BufferedWriter(
				new FileWriter(new File(filePath + ".html")))); }
			catch (Exception ex) { throw new GenException(element, 
				"Error writing output file", ex); }

		if (arc)
			try { arcos = new PrintWriter(new BufferedWriter(
				new FileWriter(new File(filePath + ".arc")))); }
			catch (Exception ex) { throw new GenException(element, 
				"Error writing output file", ex); }

		try  // Get the event data and write it out to the specified file.
		{
			EventArchiver.ArchiveWriter archiveWriter = null;
			if (arc)
			{
				archiveWriter = EventArchiver.getArchiveWriter(arcos);
			}
			
			if (html) htmlpw.println("<html><body>");
		
			if (tabbed) tabbedpw.println("Simulation Event History, created " + new Date());
			if (html) htmlpw.println("Simulation Event History, created " + new Date());
			
			for (String simRunName : simRunNames)  // each sim run.
			{
				// Write the simulation archive.
				
				if (arc)
				{
					SimulationRun simRun = scenario.getSimulationRun(simRunName);
					if (simRun == null)
					{
						GlobalConsole.println(
							"Error: could not find Simulation Run '" + simRunName +
							"' for Scenario '" + scenario.getFullName() + "'");
						continue;
					}
					
					try { archiveWriter.writeSimRun(simRun); }
					catch (IOException ex) { throw new GenException(element, ex); }
				}


				// Get the event data for the sim run.
				
				List<GeneratedEvent>[] eventListArray = null;
				try { eventListArray = modelEngine.getEventNodesByEpoch(modelDomainString,
					modelScenarioString, simRunName, pathStrings); }
				catch (Exception ex) { throw new GenException(element, ex); }
				

				// Write events in HTML and/or tabbed format.
				
				if (html) try { EventWriter.writeEvents(EventWriter.FormatType.html, 
					simRunName, pathStrings, stateIds, eventListArray, htmlpw); }
				catch (IOException ioe) { 
					GlobalConsole.printStackTrace(ioe);
					throw new GenException(element,
						"Error while writing html file: " + ioe.getMessage()); }
				
				if (tabbed) try { EventWriter.writeEvents(EventWriter.FormatType.tabbed, 
					simRunName, pathStrings, stateIds, eventListArray, tabbedpw); }
				catch (IOException ioe) { 
					GlobalConsole.printStackTrace(ioe);
					throw new GenException(element,
						"Error while writing tabbed file: " + ioe.getMessage()); }
			}
			
			if (html) htmlpw.println("</body></html>");
		}
		finally
		{
			if (tabbed) tabbedpw.close();
			if (html) htmlpw.close();
			if (arc) arcos.close();
			GlobalConsole.println("Done writing output");
		}
		
		
		// Validate the output against a saved file from a prior run.
		
		if ((! arc) && (valFilePath != null)) try
		{
			Validator.validate(this.getClientId(), modelEngine, modelDomainString, 
				modelScenarioString, pathStrings, valFilePath);
		}
		catch (Exception ex)
		{
			GlobalConsole.println("Validation found a problem or discrepancy: " + 
				ThrowableUtil.getAllMessages(ex));
				
			GlobalConsole.println("Validation file path=" + valFilePath);
			GlobalConsole.printStackTrace(ex);
		}
	}
	

	protected void genArchiveSimulation(Element element)
	throws GenException
	{
		// Wait for task to complete.
		
		String waitforString = translateAttribute(element.getAttribute("waitfor"));
		if (! waitforString.equals(""))
		{
			try { waitForTask(waitforString); }
			catch (Exception ex) { throw new GenException(
				"While waiting for task " + waitforString + " to complete", ex); }
		}
		
		
		String domainName = translateAttribute(element.getAttribute("model_domain"));
		if (domainName.equals("")) throw new GenException(element, 
			"No model_domain specified for archive_simulation statement");
		
		String substitutedName = domainNameSubstitutions.get(domainName);
		if (substitutedName != null) domainName = substitutedName;
		
		String scenarioName = translateAttribute(element.getAttribute("model_scenario"));
		if (scenarioName.equals("")) throw new GenException(element, 
			"No model_scenario specified for archive_simulation statement");
		
		String simRunName = translateAttribute(element.getAttribute("sim_run"));
		if (simRunName.equals("")) throw new GenException(element, 
			"No simulation run specified for archive_simulation statement");
		
		String filePath = translateAttribute(element.getAttribute("file"));
		if (filePath.equals("")) throw new GenException(element, 
			"No file path specified for archive_simulation statement");
		
		// Identify all of the States within the Domain.
		
		ModelDomain domain = null;
		try { domain = modelEngine.getModelDomainPersistentNode(domainName); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		ModelScenario scenario = null;
		try { scenario = domain.getModelScenario(scenarioName); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		SimulationRun run = null;
		try { run = scenario.getSimulationRun(simRunName); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		
		// Write events in archive format.
		
		PrintWriter arcos = null;
		try { arcos = new PrintWriter(new FileOutputStream(filePath)); }
		catch (Exception ex) { throw new GenException(element, 
			"Error writing output file", ex); }
		
		EventArchiver.ArchiveWriter archiveWriter = EventArchiver.getArchiveWriter(arcos);
		try { archiveWriter.writeSimRun(run); }
		catch (IOException ioe) { 
			GlobalConsole.printStackTrace(ioe);
			throw new GenException(element, "Error while writing archive file", ioe); }
		finally
		{
			arcos.close();
			GlobalConsole.println("Done writing output");
		}
	}
	
	
	/**
	 * Recursively identify all of the States nested within the specified Model
	 * Container. The result is added to the 'states' argument.
	 */
	 
	protected void getStatesRecursive(ModelContainer container, Set<State> states)
	{
		if (container instanceof PortedContainer)
		{
			states.addAll(((PortedContainer)container).getStates());
		}
		
		Set<ModelComponent> children = container.getSubcomponents();
		
		for (ModelComponent child : children)
		{
			if (child instanceof ModelContainer)
				getStatesRecursive((ModelContainer)child, states);
		}
	}


	protected void genRestoreSimulation(Element element)
	throws GenException
	{
		String filePath = translateAttribute(element.getAttribute("file"));
		if (filePath.equals("")) throw new GenException(element, 
			"No file path specified for restore_simulation statement");
		
		File file = new File(filePath);
		FileReader fr = null;
		try { fr = new FileReader(file); }
		catch (FileNotFoundException fnfe) { throw new GenException(element, fnfe); }
		
		BufferedReader br = new BufferedReader(fr);
		
		EventArchiver.ArchiveReader archiveReader = 
			EventArchiver.getArchiveReader(br, modelEngine);
		
		try { archiveReader.readSimRun(); }
		catch (IOException ex) { throw new GenException(element, ex); }
	}


	protected void genExportFinalStates(Element element)
	throws GenException
	{
		// Wait for task to complete.
		
		String waitforString = translateAttribute(element.getAttribute("waitfor"));
		if (! waitforString.equals(""))
		{
			try { waitForTask(waitforString); }
			catch (Exception ex) { throw new GenException(element,
				"While waiting for task " + waitforString + " to complete", ex); }
		}
		
		
		boolean tabbed = true;  // default.
		boolean html = false;
		
		String formatString = translateAttribute(element.getAttribute("format"));
		
		// Parse the format string into its separate components.
		
		String[] formatStrings = formatString.split(",");
		int n = formatStrings.length;
		if (n > 0) tabbed = false;
		for (int i = 0; i < n; i++)
		{
			formatStrings[i] = formatStrings[i].trim().toLowerCase();
			if (formatStrings[i].equals("tabbed")) tabbed = true;
			else if (formatStrings[i].equals("html")) html = true;
			else if (formatStrings[i].equals("")) tabbed = true;
			else throw new GenException(element, 
				"Unrecognized output format type: " + formatStrings[i]);
		}
		
		String filePath = translateAttribute(element.getAttribute("file"));
		if (filePath.equals("")) throw new GenException(element, 
			"No file path specified for export_stats statement");
		
		String domainName = translateAttribute(element.getAttribute("model_domain"));
		if (domainName.equals("")) throw new GenException(element, 
			"No model_domain specified for export_stats statement");
		
		String substitutedName = domainNameSubstitutions.get(domainName);
		if (substitutedName != null) domainName = substitutedName;
		
		String scenarioName = translateAttribute(element.getAttribute("model_scenario"));
		if (scenarioName.equals("")) throw new GenException(element, 
			"No model_scenario specified for export_stats statement");
		
		NumberFormat numberFormat = NumberFormat.getInstance();
		String maxFractionDigitsString = translateAttribute(element.getAttribute("max_fraction_digits"));
		if (! maxFractionDigitsString.equals(""))
		{
			Number m = null;
			try { m = numberFormat.parse(maxFractionDigitsString); }
			catch (ParseException pe) { throw new GenException(element, 
				"Unable to parse " + maxFractionDigitsString); }
			
			numberFormat.setMaximumFractionDigits(m.intValue());
		}
		
		NodeList nodes = getElements(element, "path");
		Vector<String> nodePathStrings = new Vector<String>();
		for (int i = 0; i < nodes.getLength(); i++)
		{
			String p = translateNodePath(((Element)(nodes.item(i))).getAttribute("path"));
			if (p.equals("")) throw new GenException(element, "Empty string for node path");
			else nodePathStrings.add(p);
		}
		
		
		// Get the data.
		
		List<Serializable[]> resultsList = new Vector<Serializable[]>();
		int noOfRuns = 0;  // no. of simulation runs.
		for (String nodePath : nodePathStrings)  // each State.
		{
			Serializable[] results = null;
			try { results = modelEngine.getFinalStateValues(this.getClientId(), 
				nodePath, scenarioName); }  // result contains one Object for each run.
			catch (Exception ex) { throw new GenException(element, ex); }
			
			// Check that each result has the same number of elements (no of runs).
			if (resultsList.size() == 0) // first time through.
				noOfRuns = results.length;
			else
				if (results.length != noOfRuns) throw new GenException(element,
					"Internal error: no of runs differs for some States");
			
			// Add result array to the List of results arrays for all States.
			resultsList.add(results);
		}


		// Open the output files.

		PrintWriter tabbedpw = null;
		PrintWriter htmlpw = null;

		if (tabbed)
			try { tabbedpw = new PrintWriter(new BufferedWriter(
				new FileWriter(new File(filePath + ".tab")))); }
			catch (Exception ex) { throw new GenException(element, 
				"Error writing output file", ex); }

		if (html)
			try { htmlpw = new PrintWriter(new BufferedWriter(
				new FileWriter(new File(filePath + "html")))); }
			catch (Exception ex) { throw new GenException(element, 
				"Error writing output file", ex); }


		// Generate the output files.
		
		try
		{
			// Write table header row.
			
			if (html)
			{
				htmlpw.println("<html><body>");
				htmlpw.println("<table cellspacing=\"4\">");
		
				htmlpw.print("<tr>");
				htmlpw.print("<td></td>");  // leave a column for the row title.
				for (String title : nodePathStrings)  // each State.
				{
					htmlpw.print("<td>" + title.replace(".", ". ") + "</td>");
				}
				htmlpw.println("</tr>");
			}
			
			// Write table body.
			for (int runNo = 1; runNo <= noOfRuns; runNo++)  // each run
			{
				if (html) htmlpw.print("<tr>");

				// Write row title.
				if (html) htmlpw.print("<td>" + runNo + "</td>");
				
				// Write row's data.
				for (Serializable[] results : resultsList)  // each State
				{
					if (html) htmlpw.print("<td>");

					Serializable value = results[runNo-1];
					if (value == null)
					{
						if (tabbed) tabbedpw.print("(null) \t");
						if (html) htmlpw.print("(null) \t");
					}
					else
					{
						String s = formatObjectValue(value, numberFormat);
						if (tabbed) tabbedpw.print(s + " \t");
						if (html) htmlpw.print(s + " \t");
					}

					if (html) htmlpw.print("</td>");
				}

				if (html) htmlpw.print("</tr>");
				if (tabbed) tabbedpw.println();
				if (html) htmlpw.println();
			}

			if (html)
			{
				htmlpw.println("</table>");
				htmlpw.println("</body></html>");
			}
		}
		finally
		{
			if (tabbed) tabbedpw.close();
			if (html) htmlpw.close();
		}
	}


	protected void genExportStats(Element element)
	throws GenException
	{
		// Wait for task to complete.
		
		String waitforString = translateAttribute(element.getAttribute("waitfor"));
		if (! waitforString.equals(""))
		{
			try { waitForTask(waitforString); }
			catch (Exception ex) { throw new GenException(element,
				"While waiting for task " + waitforString + " to complete", ex); }
		}
		
		
		boolean tabbed = true;  // default.
		boolean html = false;
		
		String formatString = translateAttribute(element.getAttribute("format"));
		
		// Parse the format string into its separate components.
		
		String[] formatStrings = formatString.split(",");
		int mm = formatStrings.length;
		if (mm > 0) tabbed = false;
		for (int i = 0; i < mm; i++)
		{
			formatStrings[i] = formatStrings[i].trim().toLowerCase();
			if (formatStrings[i].equals("tabbed")) tabbed = true;
			else if (formatStrings[i].equals("html")) html = true;
			else if (formatStrings[i].equals("")) tabbed = true;
			else throw new GenException(element,
				"Unrecognized output format type: " + formatStrings[i]);
		}
		
		String filePath = translateAttribute(element.getAttribute("file"));
		if (filePath.equals("")) throw new GenException(element, 
			"No file path specified for export_stats statement");
		
		String domainName = translateAttribute(element.getAttribute("model_domain"));
		if (domainName.equals("")) throw new GenException(element, 
			"No model_domain specified for export_stats statement");
		
		String substitutedName = domainNameSubstitutions.get(domainName);
		if (substitutedName != null) domainName = substitutedName;
		
		String scenarioName = translateAttribute(element.getAttribute("model_scenario"));
		if (scenarioName.equals("")) throw new GenException(element, 
			"No model_scenario specified for export_stats statement");
		
		String optionsString = translateAttribute(element.getAttribute("options")).trim();
		if (optionsString.equals("")) throw new GenException(element,
			"At least one options must be specified");
		
		// Parse the options string into its separate components.
		
		String[] optionsStrings = optionsString.split(",");
		int nn = optionsStrings.length;
		for (int i = 0; i < nn; i++) optionsStrings[i] = 
			optionsStrings[i].trim().toLowerCase();
		
		NumberFormat numberFormat = NumberFormat.getInstance();
		String maxFractionDigitsString = translateAttribute(element.getAttribute("max_fraction_digits"));
		if (! maxFractionDigitsString.equals(""))
		{
			Number num = null;
			try { num = numberFormat.parse(maxFractionDigitsString); }
			catch (ParseException pe) { throw new GenException(element,
				"Unable to parse " + maxFractionDigitsString); }
			
			numberFormat.setMaximumFractionDigits(num.intValue());
		}
		
		// Get the node paths.
		
		NodeList nodes = getElements(element, "path");
		Vector<String> nodePathStrings = new Vector<String>();
		for (int i = 0; i < nodes.getLength(); i++)
		{
			String p = translateNodePath(((Element)(nodes.item(i))).getAttribute("path"));
			if (p.equals("")) throw new GenException(element, "Empty string for node path");
			else nodePathStrings.add(p);
		}
		
		
		// Get the data.
		
		List<double[]> results = null;
		try { results = modelEngine.getResultStatisticsForScenario(this.getClientId(), 
			nodePathStrings, domainName, scenarioName, optionsStrings); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		if (results.size() != nodePathStrings.size()) throw new GenException(
			element, "Unexpected server error: no. of results rows != no. of paths");
		
		
		// Open the output files.

		PrintWriter tabbedpw = null;
		PrintWriter htmlpw = null;

		if (tabbed)
			try { tabbedpw = new PrintWriter(new BufferedWriter(
				new FileWriter(new File(filePath + ".tab")))); }
			catch (Exception ex) { throw new GenException(element, 
				"Error writing output file", ex); }

		if (html)
			try { htmlpw = new PrintWriter(new BufferedWriter(
				new FileWriter(new File(filePath + ".html")))); }
			catch (Exception ex) { throw new GenException(element, 
				"Error writing output file", ex); }


		// Generate the output file.
		
		try
		{
			// Write table header row.
			if (html)
			{
				htmlpw.println("<html><body>");
				htmlpw.println("<table cellspacing=\"4\">");
		
				htmlpw.print("<tr>");
				htmlpw.print("<td></td>");  // leave a column for the row title.
				for (int i = 0; i < optionsStrings.length; i++)
				{
					htmlpw.print("<td>" + optionsStrings[i] + "</td>");
				}
				htmlpw.println("</tr>");
			}
			
			// Write table body.
			int nodeNo = 0;
			for (double[] nList : results)  // each node (row).
			{
				nodeNo++;
				
				// Write row title.
				if (html)
				{
					htmlpw.print("<tr>");
					htmlpw.print("<td>" + nodePathStrings.get(nodeNo-1) + "</td>");
				}
				
				// Write row's data.
				for (double na : nList)  // each statistic (column).
				{
					if (html) htmlpw.print("<td>");

					String s = formatObjectValue(na, numberFormat);
					if (tabbed) tabbedpw.print(s + " \t");
					if (html) htmlpw.print(s + " \t");

					if (html) htmlpw.print("</td>");
				}

				if (html) htmlpw.print("</tr>");
				if (tabbed) tabbedpw.println();
				if (html) htmlpw.println();
			}

			if (html)
			{
				htmlpw.println("</table>");
				htmlpw.println("</body></html>");
			}
		}
		finally
		{
			if (tabbed) tabbedpw.close();
			if (html) htmlpw.close();
		}
	}


	protected void genExportHistograms(Element element)
	throws GenException
	{
		// Wait for task to complete.
		
		String waitforString = translateAttribute(element.getAttribute("waitfor"));
		if (! waitforString.equals(""))
		{
			try { waitForTask(waitforString); }
			catch (Exception ex) { throw new GenException(
				"While waiting for task " + waitforString + " to complete", ex); }
		}
		
		
		boolean tabbed = true;  // default.
		boolean html = false;
		
		String formatString = translateAttribute(element.getAttribute("format"));
		
		// Parse the format string into its separate components.
		
		String[] formatStrings = formatString.split(",");
		int n = formatStrings.length;
		if (n > 0) tabbed = false;
		for (int i = 0; i < n; i++)
		{
			formatStrings[i] = formatStrings[i].trim().toLowerCase();
			if (formatStrings[i].equals("tabbed")) tabbed = true;
			else if (formatStrings[i].equals("html")) html = true;
			else if (formatStrings[i].equals("")) tabbed = true;
			else throw new GenException(element,
				"Unrecognized output format type: " + formatStrings[i]);
		}
		
		String filePath = translateAttribute(element.getAttribute("file"));
		if (filePath.equals("")) throw new GenException(element, 
			"No file path specified for export_histograms statement");
		
		String domainName = translateAttribute(element.getAttribute("model_domain"));
		if (domainName.equals("")) throw new GenException(element, 
			"No model_domain specified for export_histograms statement");
		
		String substitutedName = domainNameSubstitutions.get(domainName);
		if (substitutedName != null) domainName = substitutedName;
		
		String scenarioName = translateAttribute(element.getAttribute("model_scenario"));
		if (scenarioName.equals("")) throw new GenException(element, 
			"No model_scenario specified for export_histograms statement");
		
		String allValuesString = translateAttribute(element.getAttribute("all_values"));
		boolean allValues = true;
		if (allValuesString.equals("false"))
		{
			allValues = false;
		}
		else if ((! allValuesString.equals("true")) && (! allValuesString.equals("")))
			throw new GenException(element, 
				"An 'all_values' attribute must be 'true' or 'false'");


		NumberFormat numberFormat = NumberFormat.getInstance();
		String maxFractionDigitsString = translateAttribute(element.getAttribute("max_fraction_digits"));
		if (! maxFractionDigitsString.equals(""))
		{
			Number num = null;
			try { num = numberFormat.parse(maxFractionDigitsString); }
			catch (ParseException pe) { throw new GenException(element,
				"Unable to parse " + maxFractionDigitsString); }
			
			numberFormat.setMaximumFractionDigits(num.intValue());
		}
		
		double bucketSize = 0.0;
		String bucketSizeString = translateAttribute(element.getAttribute("bucket_size"));
		if (bucketSizeString.equals("")) throw new GenException(element, 
			"No bucket_size specified for export_histograms statement");
		else
		{
			Number num = null;
			try { num = numberFormat.parse(bucketSizeString); }
			catch (ParseException pe) { throw new GenException(
				"Unable to parse " + bucketSizeString); }
			
			bucketSize = num.doubleValue();
		}
		
		double bucketStart = 0.0;
		String bucketStartString = translateAttribute(element.getAttribute("bucket_start"));
		if (bucketStartString.equals("")) throw new GenException(element, 
			"No bucket_start specified for export_histograms statement");
		else
		{
			Number num = null;
			try { num = numberFormat.parse(bucketStartString); }
			catch (ParseException pe) { throw new GenException(
				"Unable to parse " + bucketStartString); }
			
			bucketStart = num.doubleValue();
		}
		
		int noOfBuckets = 0;
		String noOfBucketsString = translateAttribute(element.getAttribute("no_of_buckets"));
		if (noOfBucketsString.equals("")) throw new GenException(element, 
			"No no_of_buckets specified for export_histograms statement");
		else
		{
			Number num = null;
			try { num = numberFormat.parse(noOfBucketsString); }
			catch (ParseException pe) { throw new GenException(
				"Unable to parse " + noOfBucketsString); }
			
			noOfBuckets = num.intValue();
		}
		
		
		// Get the State paths.
		
		List<String> statePaths = new Vector<String>();
		NodeList nodes = getElements(element, "path");
		//Vector<String> nodePathStrings = new Vector<String>();
		for (int i = 0; i < nodes.getLength(); i++)
		{
			String path = translateNodePath(((Element)(nodes.item(i))).getAttribute("path"));
			if (path.equals("")) throw new GenException(element, "Empty string for path");
			
			statePaths.add(path);
		}
		
		
		// Get the data.
		
		List<int[]> results = null;
		try { results = modelEngine.getHistograms(this.getClientId(), statePaths, 
			domainName, scenarioName, bucketSize, bucketStart, noOfBuckets,
			allValues); }
			
			// returns a List of arrays, each of which is a histogram
			// for a State. Each of the histograms is aligned, in that
			// each starts at the same lowest value and has the same
			// number of buckets.
			
		catch (Exception ex) { throw new GenException(element, ex); }
		
		if (results.size() != statePaths.size()) throw new GenException(element,
			"Unexpected server error: no. of results rows != no. of paths");
		
		
		// Open the output files.

		PrintWriter tabbedpw = null;
		PrintWriter htmlpw = null;

		if (tabbed)
			try { tabbedpw = new PrintWriter(new BufferedWriter(
				new FileWriter(new File(filePath + ".tab")))); }
			catch (Exception ex) { throw new GenException(element, 
				"Error writing output file", ex); }

		if (html)
			try { htmlpw = new PrintWriter(new BufferedWriter(
				new FileWriter(new File(filePath + ".html")))); }
			catch (Exception ex) { throw new GenException(element, 
				"Error writing output file", ex); }


		// Generate the output file.
		
		try
		{
			// Write table header row.
			if (html)
			{
				htmlpw.println("<html><body>");
				htmlpw.println("<table cellspacing=\"4\">");
		
				htmlpw.print("<tr>");
				htmlpw.print("<td></td>");  // leave a column for the row title.
				double bucketMax = bucketStart;
				for (int i = 0; i < noOfBuckets; i++)
				{
					bucketMax += bucketSize;
					htmlpw.print("<td> <=" + bucketMax + "</td>");
				}
				htmlpw.println("</tr>");
			}
			
			// Write table body.
			
			int stateNo = 0;
			for (int[] histogram : results) // each State
			{
				String statePath = statePaths.get(stateNo++);
				
				if (html) htmlpw.print("<tr>");
				if (html)
				{
					htmlpw.print("<td>" + statePath + "</td>");
				}

				for (int value : histogram) // each column of the histogram
				{
					if (html) htmlpw.print("<td>");
					if (tabbed) tabbedpw.print(value + "\t");
					if (html) htmlpw.print(value + "\t");
					
					if (html) htmlpw.print("</td>");
				}

				if (html) htmlpw.println("</tr>");
			}

			if (html)
			{
				htmlpw.println("</table>");
				htmlpw.println("</body></html>");
			}
		}
		finally
		{
			if (tabbed) tabbedpw.close();
			if (html) htmlpw.close();
		}
	}
	

	protected void genExportCorrelations(Element element)
	throws GenException
	{
		// Wait for task to complete.
		
		String waitforString = translateAttribute(element.getAttribute("waitfor"));
		if (! waitforString.equals(""))
		{
			try { waitForTask(waitforString); }
			catch (Exception ex) { throw new GenException(element,
				"While waiting for task " + waitforString + " to complete", ex); }
		}
		
		
		boolean tabbed = true;  // default.
		boolean html = false;
		
		String formatString = translateAttribute(element.getAttribute("format"));
		
		// Parse the format string into its separate components.
		
		String[] formatStrings = formatString.split(",");
		int n = formatStrings.length;
		if (n > 0) tabbed = false;
		for (int i = 0; i < n; i++)
		{
			formatStrings[i] = formatStrings[i].trim().toLowerCase();
			if (formatStrings[i].equals("tabbed")) tabbed = true;
			else if (formatStrings[i].equals("html")) html = true;
			else if (formatStrings[i].equals("")) tabbed = true;
			else throw new GenException(element,
				"Unrecognized output format type: " + formatStrings[i]);
		}
		
		String filePath = translateAttribute(element.getAttribute("file"));
		if (filePath.equals("")) throw new GenException(element, 
			"No file path specified for export_stats statement");
		
		String domainName = translateAttribute(element.getAttribute("model_domain"));
		if (domainName.equals("")) throw new GenException(element, 
			"No model_domain specified for export_stats statement");
		
		String substitutedName = domainNameSubstitutions.get(domainName);
		if (substitutedName != null) domainName = substitutedName;
		
		String scenarioName = translateAttribute(element.getAttribute("model_scenario"));
		if (scenarioName.equals("")) throw new GenException(element, 
			"No model_scenario specified for export_correlations statement");
		
		File file = new File(filePath);
		
		String optionsString = translateAttribute(element.getAttribute("options")).trim();

		NumberFormat numberFormat = NumberFormat.getInstance();
		String maxFractionDigitsString = translateAttribute(element.getAttribute("max_fraction_digits"));
		if (! maxFractionDigitsString.equals(""))
		{
			Number num = null;
			try { num = numberFormat.parse(maxFractionDigitsString); }
			catch (ParseException pe) { throw new GenException(element,
				"Unable to parse " + maxFractionDigitsString); }
			
			numberFormat.setMaximumFractionDigits(num.intValue());
		}
		
		
		// Get the State paths.
		
		List<String[]> statePaths = new Vector<String[]>();
		NodeList nodes = getElements(element, "correlated_states");
		Vector<String> nodePathStrings = new Vector<String>();
		for (int i = 0; i < nodes.getLength(); i++)
		{
			String p1 = translateNodePath(((Element)(nodes.item(i))).getAttribute("path1"));
			if (p1.equals("")) throw new GenException(element, "Empty string for path1");
			
			String p2 = translateNodePath(((Element)(nodes.item(i))).getAttribute("path2"));
			if (p2.equals("")) throw new GenException(element, "Empty string for path2");
			
			String[] pathPair = new String[] { p1, p2 };
			statePaths.add(pathPair);
		}
		
		
		// Compute the correlations.
		
		double[] correlations = null;
		try { correlations = modelEngine.getCorrelations(this.getClientId(), 
			statePaths, domainName, scenarioName, optionsString); }
		catch (Exception ex) { throw new GenException(element, ex); }
		
		
		// Open the output files.

		PrintWriter tabbedpw = null;
		PrintWriter htmlpw = null;

		if (tabbed)
			try { tabbedpw = new PrintWriter(new BufferedWriter(
				new FileWriter(new File(filePath + ".tab")))); }
			catch (Exception ex) { throw new GenException(element, 
				"Error writing output file", ex); }

		if (html)
			try { htmlpw = new PrintWriter(new BufferedWriter(
				new FileWriter(new File(filePath + ".html")))); }
			catch (Exception ex) { throw new GenException(element, 
				"Error writing output file", ex); }


		// Generate the output file.
		
		try
		{
			// Write table header row.
			if (html)
			{
				htmlpw.println("<html><body>");
				htmlpw.println("<table cellspacing=\"4\">");
		
				htmlpw.print("<tr>");
				htmlpw.print("<td></td>");  // leave a column for the row title.
				htmlpw.print("<td>" + optionsString + "</td>");
				htmlpw.println("</tr>");
			}
			
			// Write table body.
			int pairNo = 0;
			for (String[] pathPair : statePaths)  // each State pair (row).
			{
				pairNo++;
				
				// Write row title.
				if (html)
				{
					htmlpw.print("<tr>");
					htmlpw.print("<td>" + pathPair[0] + ", " +
						pathPair[1] + "</td>");
				}
				
				// Write row's data.
				if (html) htmlpw.print("<td>");

				String s = formatObjectValue(correlations[pairNo-1], numberFormat);
				if (tabbed) tabbedpw.print(s + " \t");
				if (html) htmlpw.print(s + " \t");

				if (html) htmlpw.print("</td>");

				if (html) htmlpw.print("</tr>");
				if (tabbed) tabbedpw.println();
				if (html) htmlpw.println();
			}

			if (html)
			{
				htmlpw.println("</table>");
				htmlpw.println("</body></html>");
			}
		}
		finally
		{
			if (tabbed) tabbedpw.close();
			if (html) htmlpw.close();
		}
	}
	
	
	//
	// Utility methods
	//


	protected String formatObjectValue(Serializable value, NumberFormat numberFormat)
	{
		if (value == null) return("(null)");
		else if (value instanceof Number) try {
			return numberFormat.format(value); }
		catch (IllegalArgumentException iae)
		{
			System.err.println("Error formatting Number");
			return "<err>";
		}
		else if (value instanceof Boolean) {
			if (value.equals(Boolean.TRUE)) return "true";
			else return "false"; }
		else {
			System.err.println("Unable to format object of type " +
				value.getClass().getName());
			return "(" + value.toString() + ")"; }
	}
	

	/**
	 * 
	 */
	 
	protected Serializable getValueFromString(String valueString)
	throws GenException
	{
		try { return ObjectValueWriterReader.parseObjectFromString(valueString); }
		catch (ParameterError pe) { return valueString; }
	}
	
	
	/** Warning. */
	public void warning(SAXParseException ex)
	{
		System.err.println(
			"[Warning] "+ getLocationString(ex)+": "+ ex.getMessage());
    }

	/** Error. */
	public void error(SAXParseException ex)
	{
		System.err.println(
			"[Error] "+ getLocationString(ex)+": "+ ex.getMessage());
	}

	/** Fatal error. */
	public void fatalError(SAXParseException ex)
	throws
		SAXException
	{
		System.err.println(
			"[Fatal Error] "+ getLocationString(ex)+": "+ ex.getMessage());
			throw ex;
	}


	protected void waitForTask(String taskIdString)
	throws
		Exception  // fatal error.
	{
		// Identify the task to wait for.
		
		Integer handle = taskHandles.get(taskIdString);
		if (handle == null) throw new Exception(
			"The specified task " + taskIdString + " cannot be found");
		
		// Block until the waitforTask has completed. Make sure to release
		// monitors while waiting.
		
		for (;;)
		{
			if (modelEngine.isComplete(this.getClientId(), handle) || 
				modelEngine.isAborted(this.getClientId(), handle))
				break;
			
			this.wait(1000);
		}
	}
	

	/**
	 * Returns a string of the location.
	 */
	protected String getLocationString(SAXParseException ex)
	{
		StringBuffer str = new StringBuffer();

		String systemId = ex.getSystemId();
		if (systemId != null)
		{
			int index = systemId.lastIndexOf('/');
			if (index != -1) systemId = systemId.substring(index + 1);
			str.append(systemId);
		}
		str.append(':');
		str.append(ex.getLineNumber());
		str.append(':');
		str.append(ex.getColumnNumber());

		return str.toString();
	}
	
	
	protected static NodeList getElements(Element element, String tagName)
	{
		NodeList childNodes = element.getChildNodes();
		NodeListImpl nodes = new NodeListImpl();
		for (int i = 0; i < childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			if (node instanceof Element)
			{
				Element e = (Element)node;
				if (e.getTagName().equals(tagName)) nodes.add(node);
			}
		}
		
		return nodes;
	}
	
	
	static class NodeListImpl extends Vector<Node> implements NodeList
	{
		public int getLength()
		{
			return size();
		}
		
		
		public Node item(int index)
		{
			return get(index);
		}
	}
	
	
	/**
	 * Use the getData() method on each Text Node.
	 */
	 
	String getNodeTreeAsText(Node node)
	{
		String text = "<" + node.getNodeName() + " ";

		NamedNodeMap attrMap = node.getAttributes();
		if (attrMap != null)
		{
			for (int i = 0; i < attrMap.getLength(); i++)
			{
				Node a = attrMap.item(i);
				if (a instanceof Attr)
				{
					Attr attr = (Attr)a;
					text += attr.getName() + "=\"" + attr.getValue() + "\"";
				}
				else throw new RuntimeException(
					"Unexpected non-Attr Node returned from getAttributes");
			}
		}

		NodeList children = node.getChildNodes();
		
		if (children.getLength() == 0) text += "/>";
		else
		{
			text += ">";
			
			for (int i = 0; i < children.getLength(); i++)
			{
				Node child = children.item(i);
				if (child instanceof Text)
				{
					Text t = (Text)child;
					text += t.getTextContent();
				}
				else
					text += getNodeTreeAsText(child);
			}
			
			text += "</" + node.getNodeName() + ">";
		}
		
		return text;
	}
}

