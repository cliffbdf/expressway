/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.io.Serializable;
import java.io.IOException;
import org.apache.commons.math.distribution.*;


public class SimRunSetImpl extends ModelElementImpl implements SimRunSet
	// is a DistributionOwner
	// is a SimulationRunChangeListener
{
	private SortedSet<SimulationRun> simRunSet = new TreeSetNullDisallowed<SimulationRun>();
	private ModelScenario modelScenario;
	
	public Date startingDate;  // starting time specified. May be null.
	public Date finalDate;
	public long duration;
	public int iterationLimit;
	
	private Double computedNetValueMean = null;
	private Double computedNetValueVariance = null;
	private Double computedNetValueAssurance = null;
	
		
	public Class getSerClass() { return SimRunSetSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		String[] simRunIds = new String[simRunSet.size()];
		((SimRunSetSer)nodeSer).simRunIds = simRunIds;
			
		((SimRunSetSer)nodeSer).startingDate = this.startingDate;
		((SimRunSetSer)nodeSer).finalDate = this.finalDate;
		((SimRunSetSer)nodeSer).duration = this.duration;
		((SimRunSetSer)nodeSer).iterationLimit = this.iterationLimit;

		int i = 0;
		for (SimulationRun simRun : simRunSet) simRunIds[i++] = simRun.getNodeId();
		
		((SimRunSetSer)nodeSer).scenarioId = this.modelScenario.getNodeId();
		
		try { ((SimRunSetSer)nodeSer).computedNetValueMean = this.getNetValueMean(); }
		catch (Exception ex) {  }
		
		try { ((SimRunSetSer)nodeSer).computedNetValueVariance = this.getNetValueVariance(); }
		catch (Exception ex) {  }
		
		try { ((SimRunSetSer)nodeSer).computedNetValueAssurance = this.getNetValueAssurance(); }
		catch (Exception ex) {  }
		
		return super.externalize(nodeSer);
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		SimRunSetImpl newInstance = (SimRunSetImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.simRunSet = cloneNodeSortedSet(simRunSet, cloneMap, newInstance);
		newInstance.modelScenario = cloneMap.getClonedNode(modelScenario);
		
		return newInstance;
	}
	
	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		for (SimulationRun simRun : simRunSet) simRun.simRunSetDeleted(this);
		
		// Nullify all references.
	}
	
		
	public void simulationRunDeleted(SimulationRun simRun)
	{
		// Reset cached values.
		this.computedNetValueMean = null;
		this.computedNetValueVariance = null;
		this.computedNetValueAssurance = null;
		
		// Remove the Simulation Run from this SimRunSet's Set.
		simRunSet.remove(simRun);
	}
	
	
	public SimRunSetImpl(String name, ModelScenario scenario,
		Date startDate, Date endDate, long duration, int iterLimit)
	{
		super(name, scenario, scenario.getModelDomain());
		
		this.modelScenario = scenario;
		this.startingDate = startDate;
		this.finalDate = endDate;
		this.duration = duration;
		this.iterationLimit = iterLimit;
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
	}
	
	
	public String getTagName() { return "sim_run_set"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.SimRunSetIconImageName);
	}
	
	
	public SortedSet<SimulationRun> getSimulationRuns()
	{
		return simRunSet;
	}
	
	
	public ModelScenario getModelScenario()
	{
		return this.modelScenario;
	}
	
	
	public void addSimRun(SimulationRun simRun)
	{
		simRunSet.add(simRun);
		simRun.setSimRunSet(this);
	}
	
	
	public void removeSimRun(SimulationRun simRun)
	{
		simRunSet.remove(simRun);
		simRun.setSimRunSet(null);
		updateStatistics();
	}


	public Date getStartingDate() { return startingDate; }
	
	
	public void setStartingDate(Date newDate) { this.startingDate = newDate; }
	
	
	public Date getFinalDate() { return finalDate; }
	
	
	public void setFinalDate(Date newDate) { this.finalDate = newDate; }
	
	
	public long getDuration() { return duration; }
	
	
	public void setDuration(long dur) { this.duration = dur; }
	
	
	public int getIterationLimit() { return iterationLimit; }
	
	
	public void setIterationLimit(int limit) { this.iterationLimit = limit; }


	public Serializable[] getStateFinalValues(State state)
	throws
		ParameterError
	{
		int nRuns = simRunSet.size();

		if (nRuns < 1) throw new ParameterError(
			"Value of " + state.getName() + 
				" is indeterminate in SimRunSet " + this.getName() +
				": simulation has not run yet.");

		Serializable[] finalValues = new Serializable[nRuns];
		int i = 0;
		for (SimulationRun simRun : simRunSet)
		{
			finalValues[i++] = simRun.getStateFinalValue(state);
		}

		return finalValues;
	}
	
	
	public ContinuousDistribution getDistributionOfFinalValueFor(State state)
	throws
		ModelContainsError,
		ElementNotFound
	{
		// Fit a gamma Distribution to the mean value of the State for each
		// SimulationRun in this Scenario.
		
		double mean = this.getMeanFor(state);
		double variance = this.getVarianceFor(state);
		
		// Properties of Gamma:
		// sigma^2 = scale^2 * shape
		// mean = scale * shape
		
		double shape = mean * mean / variance;
		double scale = mean / shape;
		
		if (shape <= 0.0) throw new ModelContainsError(
			"Gamma distribution shape is negative");
		
		GammaDistributionImpl dist = new GammaDistributionImpl(shape, scale);
		return dist;
	}
	
	
	public double getMeanFor(State state)
	throws
		ModelContainsError,
		ElementNotFound
	{
		return ((ModelScenarioImpl)(this.modelScenario)).computeMeanFor(state, this);
	}


	public double getVarianceFor(State state)
	throws
		ModelContainsError,
		ElementNotFound
	{
		return ((ModelScenarioImpl)(this.modelScenario)).computeVarianceFor(state, this);
	}


	public double getAssuranceFor(State state)
	throws
		ModelContainsError
	{
		return ((ModelScenarioImpl)(this.modelScenario)).computeAssuranceFor(state, this);
	}
	
	
	public double getNetValueMean()
	throws
		ModelContainsError,
		ElementNotFound
	{
		if (this.computedNetValueMean == null)
			this.computedNetValueMean = 
				((ModelScenarioImpl)(this.modelScenario)).computeNetValueMean(this);
		
		return this.computedNetValueMean.doubleValue();
	}
	
	
	public double getNetValueVariance()
	throws
		ModelContainsError,
		ElementNotFound
	{
		if (this.computedNetValueVariance == null)
			this.computedNetValueVariance = 
				((ModelScenarioImpl)(this.modelScenario)).computeNetValueVariance(this);
		
		return this.computedNetValueVariance.doubleValue();
	}
		
		
	public double getNetValueAssurance()
	throws
		ModelContainsError,
		ElementNotFound
	{
		if (this.computedNetValueAssurance == null)
			this.computedNetValueAssurance = 
				((ModelScenarioImpl)(this.modelScenario)).computeNetValueAssurance(this);
		
		return this.computedNetValueAssurance.doubleValue();
	}

	
	public void updateStatistics()
	{
		try
		{
		this.computedNetValueMean = null;
		this.computedNetValueVariance = null;
		this.computedNetValueAssurance = null;
		
		getNetValueMean();
		getNetValueVariance();
		getNetValueAssurance();
		}
		catch (Exception ex) {}
	}

	

  /* From PersistentNodeImpl */
  
	public int getNoOfHeaderRows() { return 3; }
	
	public double getPreferredWidthInPixels() { return 10; }

	public double getPreferredHeightInPixels() { return 10; }
}

