/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public abstract class DirectedRelationImpl extends ConstraintImpl implements DirectedRelation
{
	Set<ModelElement> subjects = new TreeSetNullDisallowed<ModelElement>();
	Set<ModelElement> directObjects = new TreeSetNullDisallowed<ModelElement>();

	public String[] getSubjectNodeIds() { return createNodeIdArray(subjects); }
	
	public String[] getDirectObjectNodeIds() { return createNodeIdArray(directObjects); }

	public Class getSerClass() { return DirectedRelationSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((DirectedRelationSer)nodeSer).subjectNodeIds = this.getSubjectNodeIds();
		((DirectedRelationSer)nodeSer).directObjectNodeIds = this.getDirectObjectNodeIds();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DirectedRelationImpl newInstance = (DirectedRelationImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.subjects = new TreeSetNullDisallowed<ModelElement>();
		for (ModelElement subject : subjects)
			newInstance.subjects.add((ModelElement)(cloneMap.getClonedNode(subject)));
		
		newInstance.directObjects = new TreeSetNullDisallowed<ModelElement>();
		for (ModelElement dirObj : directObjects)
			newInstance.directObjects.add((ModelElement)(cloneMap.getClonedNode(dirObj)));
		
		return newInstance;
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	DirectedRelationImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		setResizable(false);
	}


	DirectedRelationImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		setResizable(false);
	}
	
	
	private static final double PreferredWidthInPixels = 20.0;
	private static final double PreferredHeightInPixels = 20.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public Set<ModelElement> getSubjects()
	{
		return subjects;
	}


	public Set<ModelElement> getDirectObjects()
	{
		return directObjects;
	}
	
	
  /* From ModelElementChangeListener */


	public void modelElementDeleted(ModelElement element)
	{
		// Nullify any aliases (references) that the target has to the deleted Node,
		// which is passed as an argument.
		subjects.remove(element);
		directObjects.remove(element);
	}
	
	
	public void modelElementNameChanged(ModelElement element, String oldName)
	{
	}
}
