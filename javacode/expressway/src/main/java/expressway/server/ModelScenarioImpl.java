/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ExpressionInterpreter.*;
import expressway.ser.*;
import expressway.common.ClientModel.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.tools.lexer.*;
import expressway.tools.parser.*;
import expressway.tools.node.*;
import expressway.tools.analysis.*;
import expressway.server.NamedReference.*;
import expressway.geometry.*;
import expressway.generalpurpose.XMLTools;
import expressway.generalpurpose.DateAndTimeUtils;
import expressway.generalpurpose.TreeSetNullDisallowed;
import expressway.generalpurpose.StringUtils;
import java.text.ParseException;
import java.text.DateFormat;
import java.util.*;
import java.util.Collection;
import java.util.Map;
import java.util.Date;
import java.io.*;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import org.apache.commons.math.distribution.*;
import org.apache.commons.math.MathException;


public class ModelScenarioImpl
	extends
		ModelElementImpl
	implements
		ModelScenario
		// is a Hierarchy (so that it can belong to a Hierarchy)
		// is a DistributionOwner
		// is a ModelScenarioChangeListener, ModelScenarioSetChangeListener
{
	HierarchyScenarioHelper helper;
	
	public Date startingDate = null;
	public Date finalDate = null;
	public int iterationLimit = 0;
	public long duration = 0;

	private Set<ModelScenario> boundScenarios = new TreeSetNullDisallowed<ModelScenario>();
	private Set<ModelScenario> bindingScenarios = new TreeSetNullDisallowed<ModelScenario>();

	/** Primary Node Reference. */
	Set<PredefinedEvent> predefinedEvents = new HashSet<PredefinedEvent>();

	/** Primary Node Reference. */
	SortedSet<SimulationRun> simRuns = new TreeSetNullDisallowed<SimulationRun>();
	
	/** Primary Node Reference. */
	SortedSet<SimRunSet> simRunSets = new TreeSetNullDisallowed<SimRunSet>();
	
	/** Ownership. */
	Boolean externalStateBindingType = null;
	
	/** Ownership. */
	TimePeriodType externalStateGranularity = TimePeriodType.unspecified;
	
	private boolean lockedForSimulation = false;
	
	private Double computedNetValueMean = null;
	private Double computedNetValueVariance = null;
	private Double computedNetValueAssurance = null;
	
	
	ModelScenarioImpl(String name, ModelDomain domain)
	{
		super(domain, name);
		this.helper = new HierarchyScenarioHelper(this, new SuperScenario());
	}
	
	
	public String getTagName() { return "model_scenario"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ModelScenarioIconImageName);
	}
	
	
	public Class getSerClass() { return ModelScenarioSer.class; }
	
	
  /* From PersistentNode */
	
	
	public int getNoOfHeaderRows() { return helper.getNoOfHeaderRows(); }
	
	
	public double getPreferredWidthInPixels() { return helper.getPreferredWidthInPixels(); }
	public double getPreferredHeightInPixels() { return helper.getPreferredHeightInPixels(); }

	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		helper.renameChild(child, newName);
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		ModelScenarioImpl newInstance = (ModelScenarioImpl)(helper.clone(cloneMap, cloneParent));
		
		newInstance.predefinedEvents = PersistentNodeImpl.cloneNodeSet(predefinedEvents, cloneMap, cloneParent);
		newInstance.simRuns = new TreeSetNullDisallowed<SimulationRun>();
		newInstance.simRunSets = new TreeSetNullDisallowed<SimRunSet>();
		
		newInstance.setBoundScenarios(PersistentNodeImpl.cloneNodeSet(boundScenarios, cloneMap, newInstance));
		newInstance.setBindingScenarios(PersistentNodeImpl.cloneNodeSet(bindingScenarios, cloneMap, newInstance));

		return newInstance;
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		helper.writeAsXML(writer, indentation);
	}
	
	
	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{
		super.writeSpecializedXMLAttributes(writer);

		DateFormat df = DateAndTimeUtils.DateTimeFormat;
		writer.print(
			  (externalStateBindingType == null? "" : ("external_state_binding_type=\"" +
				(externalStateBindingType.booleanValue() ? "average" : "actual") + "\" "))
			
			+ (((externalStateGranularity == null) || 
					(externalStateGranularity == TimePeriodType.unspecified))
				? "" : ("external_state_time_granularity=\"" +
					(externalStateGranularity.toString()) + "\" "))
			
			+ (this.startingDate == null? "" : ("start_date=\"" + df.format(this.startingDate) + "\" "))
			+ (this.finalDate == null? "" : ("end_date=\"" + df.format(this.finalDate) + "\" "))
			+ "iteration_limit=\"" + iterationLimit + "\" "
			+ "duration=\"" + duration + "\" ");
	}
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{
		super.writeSpecializedXMLElements(writer, indentation);

		// Write bound Scenarios.
		String indentString = StringUtils.getIndentString(indentation);
		for (Scenario bs : this.boundScenarios)
		{
			writer.println(indentString + "<use scenario=\"" + bs.getName() +
				"\" for=\"" + bs.getDomain().getName() + "\"/>");
		}

	}
	
	
	public void prepareForDeletion()
	{
		helper.prepareForDeletion();
		
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		Set<ModelScenario> bs = getBindingScenarios();
		for (ModelScenario s : bs) ((HierarchyScenario)s).hierarchyScenarioDeleted(this);

		Set<PredefinedEvent> preEventsCopy = new HashSet<PredefinedEvent>(predefinedEvents);
		for (PredefinedEvent e : preEventsCopy) deletePredefinedEvent(e);

		List<SimulationRun> simRunsCopy = new Vector<SimulationRun>(simRuns);
		for (SimulationRun sr : simRunsCopy) deleteSimulationRun(sr);
		
		Set<SimRunSet> simRunSetsCopy = new TreeSetNullDisallowed<SimRunSet>(this.simRunSets);
		for (SimRunSet simRunSet : simRunSetsCopy) deleteSimRunSet(simRunSet);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }

		// Nullify all references.
		predefinedEvents = null;
		simRuns = null;
		simRunSets = null;
	}
	
	
	public PersistentNode getChild(String name) // must extend
	throws
		ElementNotFound
	{
		try { return helper.getChild(name); }
		catch (ElementNotFound ex)
		{
			// Add SimRuns, SimRunSets, and PredefinedEvents
			for (PredefinedEvent pe : predefinedEvents)
				if (pe.getName().equals(name)) return pe;
			
			for (SimulationRun simRun : simRuns)
				if (simRun.getName().equals(name)) return simRun;
			
			for (SimRunSet simRunSet : simRunSets)
				if (simRunSet.getName().equals(name)) return simRunSet;
			
			throw ex;
		}
	}
	
	
	public SortedSet<PersistentNode> getChildren() // must extend
	{
		SortedSet<PersistentNode> children = helper.getChildren();
		children.addAll(predefinedEvents);
		children.addAll(simRuns);
		children.addAll(simRunSets);
		return children;
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		// Delete (per the Delete Pattern) or disconnect the Node from any other
		// child Nodes known to this class that depend on the existence of the Node
		// that is being deleted. These are normally siblings of the Node that
		// is being deleted.
		
		// Call super.deleteChild.
		try { helper.deleteChild(child, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			// Nullify references to the Node that this class knows about.
			if (child instanceof PredefinedEvent)
			{
				if (! removePredefinedEvent((PredefinedEvent)child)) throw new ParameterError(
					"Child not found for '" + this.getFullName() + "': '" + child.getName() + "'");
			}
			else if (child instanceof SimulationRun)
			{
				if (! simRuns.remove((SimulationRun)child)) throw new ParameterError(
					"Child not found for '" + this.getFullName() + "': '" + child.getName() + "'");
			}
			else if (child instanceof SimRunSet)
			{
				if (! simRunSets.remove((SimRunSet)child)) throw new ParameterError(
					"Child not found for '" + this.getFullName() + "': '" + child.getName() + "'");
			}
			else
				throw pe;
		
			updateStatistics();
		
			// Set outermostAffectedRef argument, if any.
			setRefArgIfNotNull(outermostAffectedRef, this);
		}
	}

	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		ModelScenarioSer ser = (ModelScenarioSer)(helper.externalize(nodeSer));

		ser.startingDate = this.startingDate;
		ser.finalDate = this.finalDate;
		ser.iterationLimit = this.getIterationLimit();
		ser.duration = this.getDuration();
		ser.predefinedEventNodeIds = createNodeIdArray(predefinedEvents);
		ser.simRunNodeIds = createNodeIdArray(simRuns);
		ser.simRunSetNodeIds = createNodeIdArray(simRunSets);
		ser.externalStateBindingType = externalStateBindingType;
		ser.externalStateGranularity = this.externalStateGranularity;

		Set<Attribute> attrs = null;
		try { attrs = this.getAttributesWithValuesOfType(this.getModelDomain(), 
			Types.State.Financial.NetValue.class, true); }
		catch (ModelContainsError mce)
		{
			GlobalConsole.printStackTrace(mce);
		}
		
		if (attrs != null && attrs.size() != 0) while (true)
		{
			Attribute[] attrAr = attrs.toArray(new Attribute[1]);
			Attribute attr = attrAr[0];
			ser.netValueAttrId = attr.getNodeId();

			PersistentNode elt = attr.getParent();
			if (!(elt instanceof State))
			{
				GlobalConsole.printStackTrace(new Exception(
					"Attribute does not belong to a State"));
				break;
			}
			
			ser.netValueStatePath = ((State)elt).getFullName();
			
			break;
		}

		try { ser.computedNetValueMean = this.getNetValueMean(); }
		catch (Exception ex) {  }
		
		try { ser.computedNetValueVariance = this.getNetValueVariance(); }
		catch (Exception ex) {  }
		
		try { ser.computedNetValueAssurance = this.getNetValueAssurance(); }
		catch (Exception ex) {  }
		
		return helper.externalize(ser);
	}
	
	
	public Attribute constructAttribute(String name, Serializable defaultValue)
	throws
		ParameterError
	{
		return new ModelAttributeImpl(name, this, defaultValue);
	}
	

	public Attribute constructAttribute(String name)
	{
		return new ModelAttributeImpl(this, name);
	}
	
	
  /* From PersistentNodeImpl */
	
	
	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		return new TreeSetNullDisallowed<PersistentNode>();
	}
	
	
  /* From ModelElement */
	
	
	public void lockForSimulation(boolean lockValue)
	throws
		CannotObtainLock
	{
		ModelElementHelper.lockForSimulation(this, lockValue);
	}
	
	
	public boolean isLockedForSimulation() { return lockedForSimulation; }
	
	
	public void setLockedForSimulation(boolean lock) { this.lockedForSimulation = lock; }


	public ModelElement findModelElement(String qualifiedName)
	throws
		ParameterError
	{
		return ModelElementHelper.findModelElement(this, qualifiedName);
	}
	
	
	public ModelElement findModelElement(String[] pathParts)
	throws
		ParameterError
	{
		return ModelElementHelper.findModelElement(this, pathParts);
	}
	
	
	public void shiftAllChildren(double dx, double dy)
	{
		ModelElementHelper.shiftAllChildren(this, dx, dy);
	}

	
	public ModelAttribute createModelAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return ModelElementHelper.createModelAttribute(this, name, defaultValue, layoutBound,
			outermostAffectedRef);
	}
	
	
	public ModelAttribute createModelAttribute(PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return ModelElementHelper.createModelAttribute(this, layoutBound, outermostAffectedRef);
	}
	
	
	public ModelAttribute createModelAttribute(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return ModelElementHelper.createModelAttribute(this, name, layoutBound, outermostAffectedRef);
	}


	public ModelAttribute getModelAttribute(String name)
	{
		return ModelElementHelper.getModelAttribute(this, name);
	}
	

	public SortedSet<ModelAttribute> getModelAttributes()
	{
		return ModelElementHelper.getModelAttributes(this);
	}


	public ModelElement findFirstNestedElement(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		return ModelElementHelper.findFirstNestedElement(this, name);
	}
	
	
	public void dump() { dump(0); }
	
	
  /* From Scenario */
	
		
	public Scenario update(ScenarioSer scenarioSer)
	throws
		ParameterError,
		ModelContainsError,
		InternalEngineError
	{
		if (! (scenarioSer instanceof ModelScenarioSer)) throw new ParameterError(
			"ScenarioSer '" + scenarioSer.getFullName() + "' is not a ModelScenarioSer");
		ModelScenarioSer msSer = (ModelScenarioSer)scenarioSer;
		helper.update(msSer);
		this.startingDate = msSer.startingDate;
		this.finalDate = msSer.finalDate;
		this.iterationLimit = msSer.iterationLimit;
		this.duration = msSer.duration;
		return this;
	}
	
	
	public void setDate(Date d) { helper.setDate(d); }
	
	
	public Date getDate() { return helper.getDate(); }
	
	
	public Scenario derivedFrom() { return helper.derivedFrom(); }
	
	
	public void setDerivedFrom(Scenario originalScenario)
	{
		helper.setDerivedFrom(originalScenario);
	}
	
	
	public void setAttributeValue(Attribute attribute, Serializable value)
	throws
		ParameterError
	{
		helper.setAttributeValue(attribute, value);
	}
	
	
	public Serializable getAttributeValue(Attribute attribute)
	throws
		ParameterError
	{
		return helper.getAttributeValue(attribute);
	}


	public void setAttributeValues(Map<Attribute, Serializable> attrValues)
	{
		helper.setAttributeValues(attrValues);
	}


	public Set<Attribute> getAttributesWithValues()
	{
		return helper.getAttributesWithValues();
	}
	
	
	public Set<Attribute> getAttributesWithValuesOfType(PersistentNode elt, 
		Class type, boolean onlyOne)
	throws
		ModelContainsError
	{
		return helper.getAttributesWithValuesOfType(elt, type, onlyOne);
	}
		
	
	public void setScenarioSet(ScenarioSet s) { helper.setScenarioSet(s); }
	
	
	public ScenarioSet getScenarioSet() { return helper.getScenarioSet(); }
	
	
	public void setScenarioThatWasCopied(Scenario scen) { helper.setScenarioThatWasCopied(scen); }
	
	
	public Scenario getScenarioThatWasCopied() { return helper.getScenarioThatWasCopied(); }
	

	public ModelScenario createIndependentCopy()
	throws
		CloneNotSupportedException
	{
		ModelScenario newScenario = (ModelScenario)(helper.createIndependentCopy());
		return newScenario;
	}


	public SortedSet<Attribute> identifyTags(Class type, boolean onlyOne)
	throws
		ModelContainsError
	{
		return helper.identifyTags(type, onlyOne);
	}
	
	
	public ScenarioHelper getHelper() { return helper; }


  /* From HierarchyScenario */
	
	
	public HierarchyScenarioSet getHierarchyScenarioSet()
	{
		return helper.getHierarchyScenarioSet();
	}
	
	
	public void setHierarchyScenarioSet(HierarchyScenarioSet s) { helper.setHierarchyScenarioSet(s); }
	
	
	public HierarchyScenarioHelper getHierarchyScenarioHelper() { return helper; }


  /* From HierarchyElement */
	
	
	public HierarchyAttribute createHierarchyAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef));
	}
	
	
	public HierarchyAttribute createHierarchyAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(layoutBound, outermostAffectedRef));
	}


	public HierarchyAttribute createHierarchyAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(name, layoutBound, outermostAffectedRef));
	}
	
	
	public HierarchyAttribute createHierarchyAttribute(HierarchyAttribute attr,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return HierarchyElementHelper.createHierarchyAttribute(this, attr, layoutBound,
			outermostAffectedRef);
	}
	
	
	public HierarchyDomain getHierarchyDomain()
	{
		return (HierarchyDomain)(getDomain());
	}
	
	
  /* From PersistentListNode */
	
	
	public PersistentNode getOrderedChild(String name) throws ElementNotFound
	{ throw new RuntimeException("Should not be called"); }
	
	public PersistentNode getOrderedChildAt(int index) throws ParameterError
	{ throw new ArrayIndexOutOfBoundsException("Scenarios do not have ordered children"); }
	
	public int getIndexOfOrderedChild(PersistentNode child)
	{ throw new ArrayIndexOutOfBoundsException("Scenarios do not have ordered children"); }
	
	public void insertOrderedChildAt(PersistentNode child, int index) throws ParameterError
	{ throw new ArrayIndexOutOfBoundsException("Scenarios do not have ordered children"); }
	
	public boolean appendOrderedChild(PersistentNode child) throws ParameterError
	{ throw new ArrayIndexOutOfBoundsException("Scenarios do not have ordered children"); }
	
	public boolean removeOrderedChild(PersistentNode child) throws ParameterError
	{ throw new ArrayIndexOutOfBoundsException("Scenarios do not have ordered children"); }
	
	public List<PersistentNode> getOrderedNodesOfKind(Class c)
	{ throw new RuntimeException("Should not be called"); }
	
	public void removeOrderedNodesOfKind(Class c)
	{ throw new RuntimeException("Should not be called"); }
	
	public boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError
	{ throw new RuntimeException("Should not be called"); }

	public List<PersistentNode> getOrderedChildren()
	{ return new Vector<PersistentNode>(); }
	
	public int getNoOfOrderedChildren() { return 0; }


	public PersistentNode getFirstOrderedChild()
	{
		return ListNodeHelper.getFirstOrderedChild(this);
	}
	
	
	public PersistentNode getLastOrderedChild()
	{
		return ListNodeHelper.getLastOrderedChild(this);
	}
	
	
	public int getOrderedChildPosition(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildPosition(this, child);
	}


	public PersistentNode getOrderedChildBefore(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildBefore(this, child);
	}
	
	
	public PersistentNode getOrderedChildAfter(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildAfter(this, child);
	}
	
	
	public PersistentNode createListChild(PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return ListNodeHelper.createListChild(this, nodeBefore, nodeAfter,
			scenario, copiedNode);
	}


	public void addOrderedChildBefore(PersistentNode child, PersistentNode nextChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildBefore(this, child, nextChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}


	public void addOrderedChildAfter(PersistentNode child, PersistentNode priorChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildAfter(this, child, priorChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}
	
	
  /* From Hierarchy */
	
	
	public int getNoOfSubHierarchies() { return helper.getNoOfSubHierarchies(); }
	
	public Hierarchy getChildHierarchyAt(int index) throws ParameterError
	{ return helper.getChildHierarchyAt(index); }
	
	public int getIndexOfChildHierarchy(Hierarchy child) { return helper.getIndexOfChildHierarchy(child); }
	
	public void insertChildHierarchyAt(Hierarchy child, int index) throws ParameterError
	{ helper.insertChildHierarchyAt(child, index); }
	
	public boolean appendChildHierarchy(Hierarchy child) throws ParameterError
	{ return helper.appendChildHierarchy(child); }
		
	public boolean removeChildHierarchy(Hierarchy child) throws ParameterError
	{ return helper.removeChildHierarchy(child); }
		
		
	public void setReasonableLocation()
	{
		HierarchyHelper.setReasonableLocation(this);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			layoutBound, outermostAffectedRef);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position);
	}
		
		
	public Hierarchy createSubHierarchy(String name)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, copiedNode);
	}
	
	
	public Hierarchy createSubHierarchy(int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, position, scenario, copiedNode);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position, 
		Scenario scenario, HierarchyTemplate template)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, template);
	}
	
	
	public void deleteHierarchy(Hierarchy hier)
	throws
		ParameterError
	{
		HierarchyHelper.deleteHierarchy(this, hier);
	}
	
	
	public void changePosition(int pos)
	throws
		ParameterError
	{
		HierarchyHelper.changePosition(this, pos);
	}
	
	
	public int getPosition()
	throws
		ParameterError
	{
		return HierarchyHelper.getPosition(this);
	}
	
	
	public Hierarchy getPriorHierarchy()
	{
		return HierarchyHelper.getPriorHierarchy(this);
	}
	
	
	public Hierarchy getNextHierarchy()
	{
		return HierarchyHelper.getNextHierarchy(this);
	}
	
	
	public int getNoOfHierarchies()
	{
		return HierarchyHelper.getNoOfHierarchies(this);
	}
	
	
	public Hierarchy getParentHierarchy()
	{
		return HierarchyHelper.getParentHierarchy(this);
	}
	
	
	public void setParentHierarchy(Hierarchy parent)
	throws
		ParameterError
	{
		HierarchyHelper.setParentHierarchy(this, parent);
	}
	
	
	public final List<Hierarchy> getSubHierarchies()
	{
		return new Vector<Hierarchy>();
	}
	
	
	public List<Hierarchy> getSubHierarchies(Class kind)
	{
		return HierarchyHelper.getSubHierarchies(this, kind);
	}
	
	
	public Hierarchy getSubHierarchy(String name)
	throws
		ElementNotFound
	{
		return HierarchyHelper.getSubHierarchy(this, name);
	}
	
	
  /* From HierarchyScenarioChangeListener */
	

	public void hierarchyScenarioDeleted(HierarchyScenario scen)
	{
		helper.hierarchyScenarioDeleted(scen);
		if (scen instanceof ModelScenario) removeBoundScenario((ModelScenario)scen);
	}
	
	
  /* From HierarchyScenarioSetChangeListener */
	
	
	public void hierarchyScenarioSetDeleted(HierarchyScenarioSet scenSet)
	{
		helper.hierarchyScenarioSetDeleted(scenSet);
	}
	
	
  /* From ModelScenario */
	

	public void setBoundScenarios(Set<ModelScenario> scens) { this.boundScenarios = scens; }


	public Set<ModelScenario> getBoundScenarios() { return this.boundScenarios; }
	
	
	public void setBindingScenarios(Set<ModelScenario> scens) { this.bindingScenarios = scens; }
	
	
	public Set<ModelScenario> getBindingScenarios() { return this.bindingScenarios; }
	
	
	public ModelScenario getBoundScenarioForDomain(ModelDomain domain)
	throws
		ParameterError
	{
		for (ModelScenario boundScenario : this.boundScenarios)
		{
			ModelDomain d = boundScenario.getModelDomain();
			if (d == domain) return boundScenario;
		}
		
		return null;
	}
	
	
	public void addBoundScenario(ModelScenario s)
	{
		this.boundScenarios.add(s);
		((ModelScenarioImpl)s).bindingScenarios.add((ModelScenario)(helper.getScenario()));
	}
	
	
	public void removeBoundScenario(ModelScenario s)
	{
		this.boundScenarios.remove(s);
		((ModelScenarioImpl)s).bindingScenarios.remove(this);
	}


	public ModelScenarioSet createModelScenarioSet(String name,
		ModelAttribute attr, Serializable[] values)
	throws
		ParameterError,
		CloneNotSupportedException
	{
		return (ModelScenarioSet)(this.getModelDomain().createScenarioSet(name, this, attr, values));
	}
	
	
	public ModelScenarioSet getModelScenarioSet() { return (ModelScenarioSet)(getScenarioSet()); }
	
	
	public void setModelScenarioSet(ModelScenarioSet s) { setScenarioSet(s); }


	public SimulationRun getSimulationRun(String name)
	{
		for (SimulationRun simRun : simRuns)
		{
			if (simRun.getName().equals(name)) return simRun;
		}
		
		return null;
	}
	
	
	public SortedSet<SimulationRun> getSimulationRuns() { return simRuns; }
	
	
	public boolean hasSimulationRuns() { return simRuns.size() > 0; }


	public Set<SimulationRun> getDependentSimulationRuns()
	{
		return getDependentSimulationRuns(new TreeSetNullDisallowed<SimulationRun>());
	}


	public boolean dependentSimulationRunsExist()
	{
		if (this.hasSimulationRuns()) return true;
		
		Set<ModelScenario> boundScenarios = this.getBindingScenarios();
		for (ModelScenario ms : boundScenarios)
			if (ms.dependentSimulationRunsExist()) return true;
		
		return false;
	}
	
		
	public Set<ModelScenario> deleteDependentSimulationRuns()
	{
		Set<ModelScenario> dependentScenarios = new TreeSetNullDisallowed<ModelScenario>();
		dependentScenarios.add(this);
		
		this.deleteSimulationRuns();
		
		Set<ModelScenario> boundScenarios = this.getBindingScenarios();
		for (ModelScenario ms : boundScenarios)
		{
			dependentScenarios.addAll(ms.deleteDependentSimulationRuns());
		}
		
		return dependentScenarios;
	}
		
		
	public Set<PredefinedEvent> getPredefinedEvents()
	{
		return predefinedEvents;
	}
	
	
	public void addPredefinedEvent(PredefinedEvent event)
	{
		predefinedEvents.add(event);
	}
	
	
	public void deletePredefinedEvent(PredefinedEvent event)
	{
		try { deleteChild(event); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}


	public ValueHistoryFactory getValueHistoryFactory(boolean averages,
		long startTime, long deltaTime, long endTime)
	throws
		ParameterError,
		UndefinedValue,
		ModelContainsError
	{
		Set<State> boundStates = this.getModelDomain().getBoundStates();
		
		if (averages)
		{
			return new ValueHistoryFactoryAverages(boundStates, startTime, deltaTime, endTime);
		}
		else
		{
			return new ValueHistoryFactoryActuals(boundStates, startTime, deltaTime, endTime);
		}
	}


	public boolean getExternalStateBindingType()
	{
		if (externalStateBindingType == null)
			return getModelDomain().getExternalStateBindingType();
		
		return this.externalStateBindingType.booleanValue();
	}
		
		
	public void setExternalStateBindingType(Boolean averages)
	{
		this.externalStateBindingType = averages;
	}
	
	
	public TimePeriodType getExternalStateGranularity()
	{
		if (externalStateGranularity == TimePeriodType.unspecified)
			return getModelDomain().getExternalStateGranularity();
		
		return this.externalStateGranularity;
	}
	
	
	public void setExternalStateGranularity(TimePeriodType periodType)
	{
		this.externalStateGranularity = periodType;
	}


	public SimulationRun createSimulationRun(
		String simRunName,
		ValueHistory externalStateValues,
		ModelScenario modelScenario, ModelDomain modelDomain, 
		SimCallback callback, Date initialEpoch, Date finalEpoch, int iterationLimit,
		long duration, long randomSeed)
	{
		SimulationRun simRun = new SimulationRunImpl(
			simRunName,
			externalStateValues,
			this, getModelDomain(),
			callback, initialEpoch, finalEpoch, iterationLimit, duration,
			randomSeed);

		simRuns.add(simRun);
		
		return simRun;
	}


	public void deleteSimulationRuns()
	{
		SortedSet<SimulationRun> simRunsCopy = new TreeSetNullDisallowed<SimulationRun>(simRuns);
		for (SimulationRun simRun : simRunsCopy)
			try { deleteChild(simRun); }
			catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		SortedSet<SimRunSet> simRunSetsCopy = new TreeSetNullDisallowed<SimRunSet>(simRunSets);
		for (SimRunSet simRunSet : simRunSetsCopy)
			try { deleteChild(simRunSet); }
			catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }

		updateStatistics();
	}


	public void deleteSimulationRun(SimulationRun simRun)
	throws
		ParameterError
	{
		deleteChild(simRun);
	}


	public SimRunSet getSimRunSet(String name)
	{
		for (SimRunSet simRunSet : simRunSets)
			if (simRunSet.getName().equals(name)) return simRunSet;
		
		return null;
	}
	
	
	public SortedSet<SimRunSet> getSimRunSets()
	{
		return simRunSets;
	}


	public SimRunSet createSimRunSet(Date startDate, Date endDate, long duration, int iterLimit)
	{
		SimRunSet simRunSet = new SimRunSetImpl(createUniqueSimRunSetName(), this,
			startDate, endDate, duration, iterLimit);
		simRunSets.add(simRunSet);
		return simRunSet;
	}


	public void addSimRunSet(SimRunSet simRunSet)
	{
		simRunSets.add(simRunSet);
	}
	
	
	public void deleteSimRunSet(SimRunSet simRunSet)
	{
		try { deleteChild(simRunSet); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}
	
	
	public SortedSet<State> identifyTaggedStates(Class type, boolean onlyOne)
	throws
		ModelContainsError
	{
		// Note: See also the method ModelEngineRemote.getElementsWithAttributeDeep.
		
		SortedSet<State> states = new TreeSetNullDisallowed<State>();
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs)
		{
			Serializable value;
			try { value = helper.getAttributeValue(attr); }
			catch (ParameterError pe) { throw new ModelContainsError(pe); }
			
			Attribute attrOfType = null;
			
			if (value instanceof Class)
			{
				if (type.isAssignableFrom((Class)value))
					attrOfType = attr;
			}
			else
				if (type.isAssignableFrom(value.getClass())) // value is a 'type'
					attrOfType = attr;
			
			if (attrOfType != null)
			{
				PersistentNode parent = attrOfType.getParent();
				if (parent instanceof State) states.add((State)parent);
			}
		}
		
		return states;
	}
	
	
	public ModelScenario cloneForSimulation()
	throws
		CloneNotSupportedException
	{
		ModelScenarioImpl newModelScenario = 
			(ModelScenarioImpl)(this.clone(new CloneMap(this), getModelDomain()));
			
		newModelScenario.setDerivedFrom(this);

		return newModelScenario;
	}


	public Date getStartingDate() { return startingDate; }
	
	
	public void setStartingDate(Date newDate) { this.startingDate = newDate; }
	
	
	public Date getFinalDate() { return finalDate; }
	
	
	public void setFinalDate(Date newDate) { this.finalDate = newDate; }
	
	
	public long getDuration() { return duration; }
	
	
	public void setDuration(long dur) { this.duration = dur; }
	
	
	public int getIterationLimit() { return iterationLimit; }
	
	
	public void setIterationLimit(int limit) { this.iterationLimit = limit; }
	
	
	public Date getApplicableStartingDate()
	{
		if (this.startingDate == null)
			if (getModelScenarioSet() == null)
				return null;
			else
				return getModelScenarioSet().getApplicableStartingDate();
		else
			return this.startingDate;
	}
	
	
	public Date getApplicableFinalDate()
	{
		if (this.finalDate == null)
			if (getModelScenarioSet() == null)
				return null;
			else
				return getModelScenarioSet().getApplicableFinalDate();
		else
			return this.finalDate;
	}
	
	
	public long getApplicableDuration()
	{
		if (this.duration == 0)
			if (getModelScenarioSet() == null)
				return 0;
			else
				return getModelScenarioSet().getApplicableDuration();
		else
			return this.duration;
	}
	
	
	public int getApplicableIterationLimit()
	{
		if (this.iterationLimit == 0)
			if (getModelScenarioSet() == null)
				return 0;
			else
				return getModelScenarioSet().getApplicableIterationLimit();
		else
			return this.iterationLimit;
	}

	
	public ModelScenario getBoundScenarioForState(State state)
	throws
		ParameterError
	{
		ModelDomain stateDomain = state.getModelDomain();
		return getBoundScenarioForDomain(stateDomain);
	}
	
	
  /* From ModelContainer


	public Set<ModelComponent> getSubcomponents()
	{
		Set<ModelComponent> children = new TreeSetNullDisallowed<ModelComponent>();
		
		//children.addAll(simRuns);
		//children.addAll(simRunSets);
		
		return children;
	}
	
	
	public void deleteSubcomponent(ModelElement child)
	throws
		ParameterError
	{
		deleteChild(child);
	}*/
	
	
  /* From ModelElement ChangeListeners */

	
	public void modelScenarioDeleted(ModelScenario scenario)
	{
		if (scenario == derivedFrom()) setDerivedFrom(null);
		this.removeBoundScenario(scenario);
	}
	
	
	public void modelScenarioSetDeleted(ModelScenarioSet scenSet)
	{
		if (scenSet == getScenarioSet()) helper.setScenarioSet(null);
	}
	
	
  /* From DistributionOwner */


	public ModelScenario getModelScenario() { return this; }
   

	public Serializable[] getStateFinalValues(State state)
	throws
		ParameterError
	{
		int nRuns = simRuns.size();

		if (nRuns < 1) throw new ParameterError(
			"Value of " + state.getName() + 
				" is indeterminate in Scenario " + this.getName() +
				": simulation has not run yet.");

		Serializable[] finalValues = new Serializable[nRuns];
		int i = 0;
		for (SimulationRun simRun : simRuns)
		{
			finalValues[i++] = simRun.getStateFinalValue(state);
		}

		return finalValues;
	}


	public ContinuousDistribution getDistributionOfFinalValueFor(State state)
	throws
		ModelContainsError,
		ElementNotFound
	{
		// Fit a gamma Distribution to the mean value of the State for each
		// SimulationRun in this Scenario.
		
		double mean = this.getMeanFor(state);
		double variance = this.getVarianceFor(state);
		
		// Properties of Gamma:
		// sigma^2 = scale^2 * shape
		// mean = scale * shape
		
		double shape = mean * mean / variance;
		double scale = mean / shape;
		
		if (shape <= 0.0) throw new ModelContainsError(
			"Gamma distribution shape is negative");
		
		GammaDistributionImpl dist = new GammaDistributionImpl(shape, scale);
		return dist;
	}
	

	public double getMeanFor(State state)
	throws
		ModelContainsError,
		ElementNotFound
	{
		return computeMeanFor(state, this);
	}


	public double getVarianceFor(State state)
	throws
		ModelContainsError,
		ElementNotFound
	{
		return computeVarianceFor(state, this);
	}


	public double getAssuranceFor(State state)
	throws
		ModelContainsError
	{
		return computeAssuranceFor(state, this);
	}
	
	
	double computeMeanFor(State state, DistributionOwner statisticalContext)
	throws
		ModelContainsError
	{
		SortedSet<SimulationRun> simRuns = statisticalContext.getSimulationRuns();
		if (simRuns.size() == 0) throw new ModelContainsError(
			"No Simulation Runs for " + statisticalContext.getFullName());
		
		
		// Get State final value for each Sim Run.
		
		int i = 0;
		double sum = 0.0;
		boolean atLeastOneCompletedNormally = false;
		for (SimulationRun simRun : simRuns)
		{
			if (! simRun.completedNormally()) continue;
			atLeastOneCompletedNormally = true;
			
			Serializable value = null;
			try { value = simRun.getStateFinalValue(state); }
			catch (ParameterError pe) { throw new ModelContainsError(pe); }
			
			if (! (value instanceof Number)) throw new ModelContainsError(
				"Value of State " + state.getFullName() + " at end of sim run " +
				simRun.getFullName() + " is non-Numeric");
				
			i++;
			sum += ((Number)value).doubleValue();
		}
		
		if (! atLeastOneCompletedNormally) throw new ModelContainsError(
			"No simulation runs completed normally, so cannot compute statistics.");
		
		// Compute the State mean value.
		
		double mean = sum / ((double)i);
		
		return mean;
	}
	
	
	double computeVarianceFor(State state, DistributionOwner statisticalContext)
	throws
		ModelContainsError
	{
		double mean = 0.0;
		try { mean = getMeanFor(state); }
		catch (ElementNotFound ex) { throw new ModelContainsError(ex); }
		
		SortedSet<SimulationRun> simRuns = statisticalContext.getSimulationRuns();
		
		double[] doubleValues = new double[simRuns.size()];
		int i = 0;
		boolean atLeastOneCompletedNormally = false;
		for (SimulationRun simRun : simRuns)
		{
			if (! simRun.completedNormally()) continue;
			atLeastOneCompletedNormally = true;
			
			Serializable value = null;
			try { value = simRun.getStateFinalValue(state); }
			catch (ParameterError pe) { throw new ModelContainsError(pe); }
			
			if (value == null) throw new ModelContainsError(
				"Final value of " + state.getFullName() + " is null in Simulation Run " + 
				simRun.getName());
			
			if (! (value instanceof Number)) throw new ModelContainsError(
				"Final value of " + state.getFullName() + " is not a Number in Simulation Run " +
				simRun.getName());
			
			double doubleValue = ((Number)value).doubleValue();
			doubleValues[i++] = doubleValue;
		}
		
		if (! atLeastOneCompletedNormally) throw new ModelContainsError(
			"No simulation runs completed normally, so cannot compute statistics.");
		
		double variance = 0.0;
		for (double finalValue : doubleValues)
		{
			double delta = mean - finalValue;
			variance += (delta * delta);
		}
		
		variance /= doubleValues.length;
		return variance;
	}
	
	
	double computeAssuranceFor(State state, DistributionOwner statisticalContext)
	throws
		ModelContainsError
	{
		String expr = null;
		
		// Get all Attributes of the State that have a value of type Types.State.
		Set<Attribute> attrs = this.getAttributesWithValuesOfType(state, Types.State.class, true);
		if (attrs.size() > 0)
		{
			for (Attribute attr : attrs)
			{
				Types.State value = null;
				try { value = (Types.State)(this.getAttributeValue(attr)); }
				catch (ParameterError pe) { throw new ModelContainsError(pe); }
				
				if (value.assuranceExpression == null) continue;
				if (expr == null)
				{
					expr = value.assuranceExpression;
					continue;
				}
				
				if (value.assuranceExpression.equals(expr)) continue;
				throw new ModelContainsError(
					"Found conflicting value for Assurance Expression: " + 
						value.assuranceExpression);
			}
		}
		
		if (expr == null) expr = Types.State.DefaultAssuranceExpression;
		
		
		// Evaluate the assuranceExpression.
		
		Start ast = null;

		// Create expression lexer.
		Lexer lexer = new Lexer(new PushbackReader(new StringReader(expr)));

		// Parse expression string.
		Parser parser = new Parser(lexer);
		try { ast = parser.parse(); }
		catch (Exception ex) { throw new ModelContainsError(ex); }

		InterpreterScope.ClassScope classScope = new InterpreterScope.ClassScope()
		{
			public Package findJavaPackage(String name)
			{
				return ServiceContext.getMotifClassLoader().getPackage(name);
			}
		};

		ExpressionInterpreter interpreter = 
			new ScenarioExpressionInterpreter(classScope, this, statisticalContext);
		
		ast.apply(interpreter);
		double result = interpreter.getFinalDoubleResult();
		
		return result;
	}
	
	
	public double getNetValueMean()
	throws
		ModelContainsError,
		ElementNotFound
	{
		if (this.computedNetValueMean == null)
			this.computedNetValueMean = computeNetValueMean(this);
		
		return this.computedNetValueMean.doubleValue();
	}
	
	
	public double getNetValueVariance()
	throws
		ModelContainsError,
		ElementNotFound
	{
		if (this.computedNetValueVariance == null)
			this.computedNetValueVariance = computeNetValueVariance(this);
		
		return this.computedNetValueVariance.doubleValue();
	}
	
	
	public double getNetValueAssurance()
	throws
		ModelContainsError,
		ElementNotFound
	{
		if (this.computedNetValueAssurance == null)
			this.computedNetValueAssurance = computeNetValueAssurance(this);
		
		return this.computedNetValueAssurance.doubleValue();
	}


	public void updateStatistics()
	{
		this.computedNetValueMean = null;
		this.computedNetValueVariance = null;
		this.computedNetValueAssurance = null;
		
		try { getNetValueMean(); } catch (Exception ex) {}
		try { getNetValueVariance(); } catch (Exception ex) {}
		try { getNetValueAssurance(); } catch (Exception ex) {}
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		this.helper = new HierarchyScenarioHelper(this, new SuperScenario());
	}
	
	
  /* Internal */
	
	
	protected Set<SimulationRun> getDependentSimulationRuns(Set<SimulationRun> simRuns)
	{
		// A SimulationRun is dependent on this Scenario if it belongs to a Scenario
		// that is bound to this Scenario, directly or indirectly.
		
		simRuns.addAll(this.getSimulationRuns());
		
		Set<ModelScenario> boundScenarios = this.getBindingScenarios();
		for (ModelScenario s : boundScenarios)
		{
			((ModelScenarioImpl)s).getDependentSimulationRuns(simRuns);
		}
		
		return simRuns;
	}
	
	
	boolean removePredefinedEvent(PredefinedEvent event)
	{
		return predefinedEvents.remove(event);
	}
	
	
	protected String createUniqueSimRunSetName()
	{
		return createUniqueChildName("Simulation ");
	}
	

	protected double computeNetValueMean(DistributionOwner statisticalContext)
	throws
		ModelContainsError
	{
		try { return computeMeanFor(getNetValueState(), statisticalContext); }
		catch (ElementNotFound ex) { throw new ModelContainsError(ex); }
	}
	
	
	protected double computeNetValueVariance(DistributionOwner statisticalContext)
	throws
		ModelContainsError
	{
		try { return computeVarianceFor(getNetValueState(), statisticalContext); }
		catch (ElementNotFound ex) { throw new ModelContainsError(ex); }
	}
	
	
	protected double computeNetValueAssurance(DistributionOwner statisticalContext)
	throws
		ModelContainsError
	{
		try { return computeAssuranceFor(getNetValueState(), statisticalContext); }
		catch (ElementNotFound ex) { throw new ModelContainsError(ex); }
	}
	
		
	/**
	 * Identify the Attribute of type Types.State.Financial.NetValue. 
	 * (It is an error if there is more than one.)
	 */
	 
	State getNetValueState()
	throws
		ElementNotFound,
		ModelContainsError  // if there is more than one State, or Element
			// is not a State.
	{
		Set<Attribute> attrs = identifyTags(Types.State.Financial.NetValue.class, true);
		if (attrs.size() == 0) throw new ElementNotFound(
			"There are no Attributes with a value of type Types.State.Financial.NetValue");
		
		if (attrs.size() > 1) throw new ModelContainsError(
			"Found more than one Attribute with a value of type Types.State.Financial.NetValue");
		
		Attribute[] attrAr = attrs.toArray(new ModelAttribute[1]);
		Attribute attr = attrAr[0];
		if (attr == null) throw new RuntimeException("attr is null");
		
		PersistentNode elt = attr.getParent();
		if (! (elt instanceof State)) throw new ModelContainsError(
			"Attribute with value NetValue is not a State");
		
		return (State)elt;
	}
	
		
	/** For debug */
	public void dumpAttrValues()
	{
		// Traverse every Component of the Domain, dumping each Attribute value.
		
		GlobalConsole.println("For Scenario " + this.getName() + ":");
		dumpAttrValues(this.getModelDomain());
	}
	
	
	/** For debug */
	void dumpAttrValues(ModelElement elt)
	{
		// Dump Attribute values for elt.
		
		Set<Attribute> attrs = new TreeSetNullDisallowed<Attribute>(elt.getAttributes());
		for (Attribute attr : attrs) try
		{
			Serializable value = getAttributeValue((ModelAttribute)attr);
			GlobalConsole.println(attr.getFullName() + "=" + value.toString());
		}
		catch (Exception ex)
		{
			GlobalConsole.println("***Could not obtain value for Attribute " + attr.getFullName());
		}
		
		
		// Recursively traverse each nested ModelElement.
		
		Set<PersistentNode> subElts = elt.getChildren();
		for (PersistentNode subElt : subElts)
		{
			if (subElt instanceof ModelElement)
				dumpAttrValues((ModelElement)subElt);
		}
	}
	
	
	/**
	 * A factory that computes the average of the value of each bound state at
	 * successive points in simulated time.
	 */
	 
	class ValueHistoryFactoryAverages implements ValueHistoryFactory
	{
		private ValueHistoryImpl valueHistory = null;
		private long startTime;
		private long deltaTime;
		private long endTime;
		
		
		ValueHistoryFactoryAverages(Set<State> boundStates,
			long startTime, long deltaTime, long endTime)
		throws
			ParameterError,
			UndefinedValue,
			ModelContainsError
		{
			this.startTime = startTime;
			this.deltaTime = deltaTime;
			this.endTime = endTime;
			
			this.valueHistory = new ValueHistoryImpl(startTime, deltaTime, endTime);
			
			long duration = endTime - startTime;
			long noOfTimePointsL = (duration / deltaTime) + 1;
			
			if (noOfTimePointsL > Integer.MAX_VALUE) throw new ParameterError(
				"Delta time is too small relative to duration: too many data points");
			int noOfTimePoints = (int)noOfTimePointsL;
			if ((deltaTime * noOfTimePoints + startTime) != endTime) noOfTimePoints++;
			
			for (State state : boundStates)
			{
				// Identify the bound Scenario for the State's Domain

				ModelScenario boundScenario = ModelScenarioImpl.this.getBoundScenarioForState(state);
				
				if (boundScenario == null)
				{
					throw new ParameterError(
						"No Scenario has been selected for the Domain of State '" +
						state.getFullName() + "' when simulating Scenario '" +
						ModelScenarioImpl.this.getName() + "' of Domain '" + 
						ModelScenarioImpl.this.getModelDomain().getName() + "'.");
				}
				
				// Identify all SimulationRuns for the bound Scenario.
				
				Set<SimulationRun> boundSimRuns = boundScenario.getSimulationRuns();
				
				
				// Iterate over each time interval, as specified by the caller.
				
				EventSer[] eSers = new EventSer[noOfTimePoints];
				long time = startTime;
				EventSer priorSer = null;
				for (int i = 0;; i++) // each time point i
				{
					if (time > endTime)  // Replicate the Ser through the rest of the array.
					{
						for (int j = i; j < noOfTimePoints; j++) eSers[j] = priorSer;
						break;
					}

					
					// Compute the average for the interval across all of 
					// the SimulationRuns. If the event source is pulse,
					// the average should be the average amplitude,
					// occurring after the average delay from the beginning
					// of the period, and should be followed by a compensation event.
					
					Double averageValue = null;
					boolean isPulse = true;
					long averageDelay = 0;
					double sum = 0.0;
					long delaySum = 0;
					int nRuns = 0;
					for (SimulationRun boundSimRun : boundSimRuns)
					{
						nRuns++;
						
						Date date = new Date(time);
						//Serializable[] values = boundSimRun.getStateValuesAtDate(state, date);
						
						boolean[] isPulseRef = new boolean[1];
						long averageDelayRef[] = new long[1];
						
						
						// Get the value of the State in the bound Sim Run, averaged
						// over the interval.
						
						Serializable value = // never returns null
							boundSimRun.getAverageRisingValueForPeriod(state, time, time + deltaTime,
								isPulseRef, averageDelayRef);
						
						
						// A single flow value trumps all pulses.
						if (! isPulseRef[0]) isPulse = false;
						
						//Object value = null;
						//if (values.length > 0) value = values[values.length-1];
						
						if (value == null)
						{
							averageValue = null;
							break;
						}
						else
						{
							if (! (value instanceof Number)) throw new ParameterError(
								"Cannot compute an average: value of " + state.getFullName() +
								" at time " + (new Date(time)).toString() + " is not a Number. " +
								"It is a " + value.getClass().getName() + ".");
								
							sum += ((Number)value).doubleValue();
							//delaySum += averageDelayRef[0];
						}
					}
					
					if (nRuns > 0)
					{
						averageValue = new Double(sum / ((double)nRuns));
						averageDelay = delaySum / nRuns;
					}
					
					// Add the computed average to the history for the State.
					Date date = new Date(time + averageDelay);
					EventSer eSer = new EventSer(state.getNodeId(), date, averageValue);
					
					if (isPulse) eSer.pulse = true;
					
					eSers[i] = eSer;
					priorSer = eSer;
					
					time += deltaTime;
				}
				
				valueHistory.addStateHistory(state.getNodeId(), eSers);
			}
		}
		
		
		public ValueHistory getValueHistory()
		throws
			ParameterError,
			UndefinedValue,
			ModelContainsError
		{
			return this.valueHistory;
		}
	}


	/**
	 * A factory that rotates among the SimulationRuns for each bound Scenario.
	 */
	
	class ValueHistoryFactoryActuals implements ValueHistoryFactory
	{
		private ValueHistoryImpl valueHistory = null;
		private Set<State> boundStates;
		private long startTime;
		private long deltaTime;
		private long endTime;
		
		private Map<ModelScenario, Enumeration<SimulationRun>> boundSimRuns =
			new HashMap<ModelScenario, Enumeration<SimulationRun>>();
		
		
		ValueHistoryFactoryActuals(Set<State> boundStates,
			long startTime, long deltaTime, long endTime)
		throws
			ParameterError
		{
			this.boundStates = boundStates;
			this.startTime = startTime;
			this.deltaTime = deltaTime;
			this.endTime = endTime;

			Set<ModelScenario> boundScenarios = new TreeSetNullDisallowed<ModelScenario>();
			for (State boundState : boundStates)
			{
				ModelScenario boundScenario = 
					ModelScenarioImpl.this.getBoundScenarioForState(boundState);
				
				boundScenarios.add(boundScenario);
			}
			
			for (ModelScenario boundScenario : boundScenarios)
			{
				// An Enumeration that cycles through each SimulationRun and then
				// starts over from the first.
				
				SortedSet<SimulationRun> simRunSet = boundScenario.getSimulationRuns();
				final SimulationRun[] sRunAr = simRunSet.toArray(new SimulationRun[simRuns.size()]);
				
				Enumeration<SimulationRun> simRunEnum = new Enumeration<SimulationRun>()
				{
					int i = 0;
					
					public boolean hasMoreElements() { return (sRunAr.length > 0); }
					
					public SimulationRun nextElement()
					{
						if (i > sRunAr.length-1) throw new NoSuchElementException(
							"There are no SimulationRuns");
						
						SimulationRun simRun = sRunAr[i];
						i = (i+1) % sRunAr.length;
						return simRun;
					}
				};
				
				boundSimRuns.put(boundScenario, simRunEnum);
			}
		}
		
		
		public ValueHistory getValueHistory()
		throws
			ParameterError,
			UndefinedValue,
			ModelContainsError
		{
			ValueHistoryImpl valueHistory = new ValueHistoryImpl(startTime, deltaTime, endTime);
			
			long duration = endTime - startTime;
			long noOfTimePointsL = duration / deltaTime + 1;
			if (noOfTimePointsL > Integer.MAX_VALUE) throw new ParameterError(
				"Too many intervals: " + noOfTimePointsL);
			int noOfTimePoints = (int)noOfTimePointsL;
			
			if ((deltaTime * noOfTimePoints + startTime) != endTime) noOfTimePoints++;

			for (State boundState : this.boundStates)
			{
				ModelScenario boundScenario = 
					ModelScenarioImpl.this.getBoundScenarioForState(boundState);
				EventSer[] eSers = new EventSer[noOfTimePoints];
				
				long time = startTime;
				for (int i = 0;; i++) // each time point 
				{
					if (time > endTime) break;
					
					Enumeration<SimulationRun> simRunEnumeration = boundSimRuns.get(boundScenario);
					SimulationRun boundSimRun = simRunEnumeration.nextElement();
					
					boolean[] isPulseRef = new boolean[1];
					long[] averageDelayRef = new long[1];
					
					Serializable value = boundSimRun.getAverageRisingValueForPeriod(
						boundState, time,
						time + deltaTime, isPulseRef, averageDelayRef);
					
					Serializable newValue = null;
					if (value != null)
					{
						if (! (value instanceof Number)) throw new ParameterError(
							"Cannot compute an average: value of " + boundState.getFullName() +
							" at time " + (new Date(time)).toString() + " is not a Number. " +
							"It is a " + value.getClass().getName() + ".");
						
						newValue = new Double(((Number)value).doubleValue());
					}
					
					Date newDate = new Date(time);
					//Date newDate = new Date(time + averageDelayRef[0]);
					
					EventSer eSer = new EventSer(
						boundState.getNodeId(), newDate, newValue, isPulseRef[0]);
					eSers[i] = eSer;
					
					time += deltaTime;
				}
				
				valueHistory.addStateHistory(boundState.getNodeId(), eSers);
			}

			
			return valueHistory;
		}
	}
	
	
  /* Proxy classes needed by HierarchyScenarioHelper */

	
	private class SuperScenario extends SuperPersistentNode
	{
	}
	
	
	private class SuperPersistentNode implements PersistentNode
		//Comparable<PersistentNode>,
		//Display (implements Point)
		//Cloneable
	{
		public int compareTo(PersistentNode node)
		{
			throw new RuntimeException("Should not be called");
		}
		
		
		public Object clone() throws CloneNotSupportedException
		{
			throw new RuntimeException("Should not be called");
		}

		
	  /* From PersistentNode */
		
		
		public ModelEngineLocal getModelEngine()
		{ return ModelScenarioImpl.super.getModelEngine(); }
	
	
		public List<String> getInstantiableNodeKinds()
		{ return ModelScenarioImpl.super.getInstantiableNodeKinds(); }
		
		
		public List<String> getStaticCreateMethodNames()
		{ return ModelScenarioImpl.super.getStaticCreateMethodNames(); }
		
		
		public List<String> getInstantiableNodeDescriptions()
		{ return ModelScenarioImpl.super.getInstantiableNodeDescriptions(); }
		
		
		public PersistentNode createChild(String kind, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.createChild(kind, layoutBound, outermostAffectedRef); }
		
		
		public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ ModelScenarioImpl.super.deleteChild(child, outermostAffectedRef); }
			
		
		public void deleteChild(PersistentNode child)
		throws
			ParameterError
		{ ModelScenarioImpl.super.deleteChild(child); }
			
			
		public Template findTemplate(String templatePath)
		throws
			ElementNotFound,
			ParameterError
		{ return ModelScenarioImpl.super.findTemplate(templatePath); }
		
		
		public Class getNodeKind(String className)
		throws
			ClassNotFoundException,
			ParameterError
		{ return ModelScenarioImpl.super.getNodeKind(className); }
			
		
		public Class getSerClass()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Set<Attribute> getAttributes()
		{ return ModelScenarioImpl.super.getAttributes(); }
		
		
		public String getTagName()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Attribute getAttribute(String attrName)
		{ return ModelScenarioImpl.super.getAttribute(attrName); }
		
		
		public int getAttributeSeqNo(Attribute attr)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.getAttributeSeqNo(attr); }
		

		public void setAttributeSeqNo(Attribute attr, int seqNo)
		throws
			ParameterError
		{ ModelScenarioImpl.super.setAttributeSeqNo(attr, seqNo); }
				
				
		public void setAttributeAsFirstInSeq(Attribute attr)
		throws
			ParameterError
		{ ModelScenarioImpl.super.setAttributeAsFirstInSeq(attr); }
		
		
		public void setAttributeAsLastInSeq(Attribute attr)
		throws
			ParameterError
		{ ModelScenarioImpl.super.setAttributeAsLastInSeq(attr); }
		
		
		public void moveAttributeBefore(Attribute attr, Attribute nextAttr)
		throws
			ParameterError
		{ ModelScenarioImpl.super.moveAttributeBefore(attr, nextAttr); }
		
		
		public void moveAttributeAfter(Attribute attr, Attribute priorAttr)
		throws
			ParameterError
		{ ModelScenarioImpl.super.moveAttributeAfter(attr, priorAttr); }
	
		
		public int getFirstAttributeSeqNo()
		{ return ModelScenarioImpl.super.getFirstAttributeSeqNo(); }
		
		
		public int getLastAttributeSeqNo()
		{ return ModelScenarioImpl.super.getLastAttributeSeqNo(); }
		
		
		public int getNumberOfAttributes()
		{ return ModelScenarioImpl.super.getNumberOfAttributes(); }


		public List<Attribute> getAttributesInSequence()
		{ return ModelScenarioImpl.super.getAttributesInSequence(); }
		
		
		public Attribute getFirstAttribute()
		{ return ModelScenarioImpl.super.getFirstAttribute(); }
		
		
		public Attribute getLastAttribute()
		{ return ModelScenarioImpl.super.getLastAttribute(); }

		
		public Attribute getAttributeAtSeqNo(int seqNo)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.getAttributeAtSeqNo(seqNo); }


		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{ throw new RuntimeException("Should not be called"); }
		
	
		public Attribute constructAttribute(String name)
		{ throw new RuntimeException("Should not be called"); }
		
	
		public Attribute createAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		
		public Attribute createAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.createAttribute(layoutBound, outermostAffectedRef); }
	
	
		public Attribute createAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.createAttribute(name, layoutBound, outermostAffectedRef); }
		
		
		public void deleteAttribute(Attribute attribute)
		throws
			ParameterError
		{ ModelScenarioImpl.super.deleteAttribute(attribute); }
			
			
		public void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ ModelScenarioImpl.super.deleteAttribute(attribute, outermostAffectedRef); }
			
			
		public boolean isOwnedByMotifDef()
		{ return ModelScenarioImpl.super.isOwnedByMotifDef(); }
		
		
		public boolean isOwnedByActualTemplateInstance()
		{ return ModelScenarioImpl.super.isOwnedByActualTemplateInstance(); }
		
		
		public boolean actuallyUsesMotif(MotifDef md)
		{ return ModelScenarioImpl.super.actuallyUsesMotif(md); }
		
		
		public Image getLocalStoredImage(String handle)
		{ return ModelScenarioImpl.super.getLocalStoredImage(handle); }
		
		
		public void deleteFromAllNodesTable()
		{ ModelScenarioImpl.super.deleteFromAllNodesTable(); }
		
		
		public String getName()
		{ return ModelScenarioImpl.super.getName(); }
		
		
		public void setHTMLDescription(String html)
		{ ModelScenarioImpl.super.setHTMLDescription(html); }
		
		
		public String getHTMLDescription()
		{ return ModelScenarioImpl.super.getHTMLDescription(); }
		
		
		public String getFullName()
		{ return ModelScenarioImpl.super.getFullName(); }
		
		
		public String getNameRelativeTo(PersistentNode parent)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.getNameRelativeTo(parent); }
	
	
		public void setName(String name)
		throws
			ParameterError
		{ ModelScenarioImpl.super.setName(name); }
		
		
		public String setNameAndNotify(String newName)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.setNameAndNotify(newName); }
		
		
		public void renameChild(PersistentNode child, String newName)
		throws
			ParameterError
		{ ModelScenarioImpl.super.renameChild(child, newName); }
		
		
		public Domain getDomain()
		{ return ModelScenarioImpl.super.getDomain(); }
		
		
		public String createUniqueChildName()
		{ return ModelScenarioImpl.super.createUniqueChildName(); }
	
		
		public String createUniqueChildName(String baseName)
		{ return ModelScenarioImpl.super.createUniqueChildName(baseName); }
		
		
		public SortedSet<PersistentNode> getChildren()
		{ return ModelScenarioImpl.super.getChildren(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive()
		{ return ModelScenarioImpl.super.getChildrenRecursive(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive(Class kind)
		{ return ModelScenarioImpl.super.getChildrenRecursive(kind); }
		
		
		public PersistentNode getChild(String name)
		throws
			ElementNotFound
		{ return ModelScenarioImpl.super.getChild(name); }
	
	
		public PersistentNode getParent()
		{ return ModelScenarioImpl.super.getParent(); }
		
		
		public void setParent(PersistentNode parent)
		{ ModelScenarioImpl.super.setParent(parent); }
		
		
		public boolean isDescendantOf(PersistentNode ancestor)
		{ return ModelScenarioImpl.super.isDescendantOf(ancestor); }
		
		
		public PersistentNode findNode(String qualifiedName)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.findNode(qualifiedName); }
	
	
		public PersistentNode findNode(String[] pathParts)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.findNode(pathParts); }
			
		
		public PersistentNode findFirstNestedNode(String name)
		throws
			ElementNotFound
		{ return ModelScenarioImpl.super.findFirstNestedNode(name); }
		
		
		public PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.getChildByRelativeQualifiedName(pathParts); }
		
		
		public void prepareForDeletion()
		{ ModelScenarioImpl.super.prepareForDeletion(); }
		
		
		public Display getDisplay()
		{ return ModelScenarioImpl.super.getDisplay(); }
		
		
		public void setVisible(boolean vis)
		{ ModelScenarioImpl.super.setVisible(vis); }
		
		
		public boolean isVisible()
		{ return ModelScenarioImpl.super.isVisible(); }
		
		
		public void layout()
		{ ModelScenarioImpl.super.layout(); }
		
		
		public Set<PersistentNode> getNodesWithSpecialLayout()
		{ return ModelScenarioImpl.super.getNodesWithSpecialLayout(); }
		
		
		public void conditionalLayout()
		{ ModelScenarioImpl.super.conditionalLayout(); }
		
		
		public PersistentNode layoutUpward(PersistentNode layoutBound)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.layoutUpward(layoutBound); }
	
		
		public void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		{ ModelScenarioImpl.super.layout(layoutBound, outermostAffectedRef); }
	
	
		public void setPredefined(boolean pre)
		{ ModelScenarioImpl.super.setPredefined(pre); }
		
		
		public boolean isPredefined()
		{ return ModelScenarioImpl.super.isPredefined(); }
		
		
		public void setDeletable(boolean del)
		{ ModelScenarioImpl.super.setDeletable(del); }
		
		
		public boolean isDeletable()
		{ return ModelScenarioImpl.super.isDeletable(); }
		
		
		public void setMovable(boolean m)
		{ ModelScenarioImpl.super.setMovable(m); }
		
		
		public boolean isMovable()
		{ return ModelScenarioImpl.super.isMovable(); }
		
		
		public void setResizable(boolean yes)
		{ ModelScenarioImpl.super.setResizable(yes); }
		
		
		public boolean isResizable()
		{ return ModelScenarioImpl.super.isResizable(); }
		
		
		public double getWidth()
		{return ModelScenarioImpl.super.getWidth();  }
	
	
		public double getHeight()
		{ return ModelScenarioImpl.super.getHeight(); }
		
		
		public void setWidth(double width)
		{ ModelScenarioImpl.super.setWidth(width); }
	
	
		public void setHeight(double height)
		{ ModelScenarioImpl.super.setHeight(height); }
		
		
		public boolean useBorderWhenIconified()
		{ return ModelScenarioImpl.super.useBorderWhenIconified(); }
		
		
		public void setUseBorderWhenIconified(boolean yesOrNo)
		{ ModelScenarioImpl.super.setUseBorderWhenIconified(yesOrNo); }
		
		
		public Image getIconImage()
		{ return ModelScenarioImpl.super.getIconImage(); }
		
		
		public void setIconImage(String path)
		throws
			IOException
		{ ModelScenarioImpl.super.setIconImage(path); }
		
		
		public ImageIcon getImageIcon()
		{ return ModelScenarioImpl.super.getImageIcon(); }
		
		
		public String getIconImageHandle()
		{ return ModelScenarioImpl.super.getIconImageHandle(); }
		
		
		public double getIconWidth()
		{ return ModelScenarioImpl.super.getIconWidth(); }
		
		
		public double getIconHeight()
		{ return ModelScenarioImpl.super.getIconHeight(); }
		
		
		public void setLayoutManager(LayoutManager lm)
		{ ModelScenarioImpl.super.setLayoutManager(lm); }
		
		
		public LayoutManager getLayoutManager()
		{ return ModelScenarioImpl.super.getLayoutManager(); }
		
		
		public String getDefaultViewTypeName()
		{ return ModelScenarioImpl.super.getDefaultViewTypeName(); }
		
		public void setViewClassName(String name)
		{ ModelScenarioImpl.super.setViewClassName(name); }
		
		public String getViewClassName()
		{ return ModelScenarioImpl.super.getViewClassName(); }

		
		public boolean contains(Point p)
		{ return ModelScenarioImpl.super.contains(p); }
		
		
		public boolean contains(double x, double y)
		{ return ModelScenarioImpl.super.contains(x, y); }
		
		
		public Face[] getFaces(double x, double y)
		{ return ModelScenarioImpl.super.getFaces(x, y); }
		
		
		public Face[] getIconFaces(double x, double y)
		{ return ModelScenarioImpl.super.getIconFaces(x, y); }
		
		
		public Position findSideClosestToInternalPoint(double x, double y)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.findSideClosestToInternalPoint(x, y); }
		
		
		public Position findSideClosestToExternalPoint(Point location)
		{ return ModelScenarioImpl.super.findSideClosestToExternalPoint(location); }
		
		
		public void setSizeToStandard()
		{ ModelScenarioImpl.super.setSizeToStandard(); }
		
		
		public double getHeaderLabelHeight()
		{ return ModelScenarioImpl.super.getHeaderLabelHeight(); }
		
		
		public int getNoOfHeaderRows()
		{ throw new RuntimeException("Should not be called"); }
	
	
		public SortedSet<PersistentNode> getPredefinedNodes()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public double getMinimumWidth()
		{ return ModelScenarioImpl.super.getMinimumWidth(); }
		
		
		public double getMinimumHeight()
		{ return ModelScenarioImpl.super.getMinimumHeight(); }
		
		
		public double getPreferredWidthInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		
		public double getPreferredHeightInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		
		public int getPreferredHeaderRowHeightInPixels()
		{ return ModelScenarioImpl.super.getPreferredHeaderRowHeightInPixels(); }
		
		
		public double getX() { return ModelScenarioImpl.super.getX(); }
		public double getY() { return ModelScenarioImpl.super.getY(); }
		public double getScale() { return ModelScenarioImpl.super.getScale(); }
		public double getOrientation() { return ModelScenarioImpl.super.getOrientation(); }
		public void setX(double x) { ModelScenarioImpl.super.setX(x); }
		public void setY(double y) { ModelScenarioImpl.super.setY(y); }
		public void setLocation(double x, double y) { ModelScenarioImpl.super.setLocation(x, y); }
		public void setScale(double scale) { ModelScenarioImpl.super.setScale(scale); }
		public void setOrientation(double angle) { ModelScenarioImpl.super.setOrientation(angle); }
		
		
		public boolean allowShift(PersistentNode childNode)
		{ return ModelScenarioImpl.super.allowShift(childNode); }
		
		
		public long getLastUpdated()
		{ return ModelScenarioImpl.super.getLastUpdated(); }
		
		
		public int getVersionNumber()
		{ return ModelScenarioImpl.super.getVersionNumber(); }
		
		
		public String getClientThatLastModified()
		{ return ModelScenarioImpl.super.getClientThatLastModified(); }
		
		
		public void update()
		{ ModelScenarioImpl.super.update(); }
		
	
		public Date getDeletionDate()
		{ return ModelScenarioImpl.super.getDeletionDate(); }
		
	
		public PersistentNode getPriorVersion()
		{ return ModelScenarioImpl.super.getPriorVersion(); }
		
		
		public String getPriorVersionNodeId()
		{ return ModelScenarioImpl.super.getPriorVersionNodeId(); }
		
		
		public String getNodeId()
		{ return ModelScenarioImpl.super.getNodeId(); }
		
		
		public String[] getNodeIdPath()
		{ return ModelScenarioImpl.super.getNodeIdPath(); }
		
		
		public String getNodeIdPathString()
		{ return ModelScenarioImpl.super.getNodeIdPathString(); }
		
		
		public void setWatch(boolean enable)
		{ ModelScenarioImpl.super.setWatch(enable); }
		
		
		public boolean getWatch()
		{ return ModelScenarioImpl.super.getWatch(); }
	
	
		public NodeSer externalize()
		throws
			ParameterError
		{ return ModelScenarioImpl.super.externalize(); }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.externalize(nodeSer); }
		
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{ ModelScenarioImpl.super.writeAsXML(writer, indentation); }
		
		
		public void writeSpecializedXMLAttributes(PrintWriter writer)
		throws
			IOException
		{ ModelScenarioImpl.super.writeSpecializedXMLAttributes(writer); }
		
		
		public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
		throws
			IOException
		{ ModelScenarioImpl.super.writeSpecializedXMLElements(writer, indentation); }
		
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		{ return ModelScenarioImpl.super.getAvailableHTTPFormats(descriptions); }
		
		
		public String getWebURLString(String format)
		throws
			ParameterError
		{ return ModelScenarioImpl.super.getWebURLString(format); }
		
		
		public byte[] renderAsSVG(double wCmMax, double hCmMax)
		throws
			IOException
		{ return ModelScenarioImpl.super.renderAsSVG(wCmMax, hCmMax); }
	
		
		public java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
			double pixPerModel)
		{ return ModelScenarioImpl.super.renderHierarchyAsLeaves(g, x, y, pixPerModel); }
		
		
		public java.awt.Dimension render(Graphics2D g, double pixPerModel)
		{ return ModelScenarioImpl.super.render(g, pixPerModel); }
	
	
		public void renderAsChild(Graphics2D g, double pixPerModel)
		{ ModelScenarioImpl.super.renderAsChild(g, pixPerModel); }
		
		
		public byte[] renderAsHTML()
		throws
			ParameterError,
			IOException
		{ return ModelScenarioImpl.super.renderAsHTML(); }
		
	
		public void notifyAllListeners()
		throws
			Exception
		{ ModelScenarioImpl.super.notifyAllListeners(); }
		
		
		public void notifyNodeDeleted()
		throws
			Exception
		{ ModelScenarioImpl.super.notifyNodeDeleted(); }
		
		
		public void notifyAllListeners(PeerNotice notice)
		throws
			Exception
		{ ModelScenarioImpl.super.notifyAllListeners(notice); }
		
		
		public String[] createNodeIdArray(Collection col)
		{ return ModelScenarioImpl.super.createNodeIdArray(col); }
		
		
		public String[] createNodeIdArray(Map map)
		{ return ModelScenarioImpl.super.createNodeIdArray(map); }
	
	
		public String getNodeIdOrNull(PersistentNode node)
		{ return ModelScenarioImpl.super.getNodeIdOrNull(node); }
	
	
		public void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
		{ ModelScenarioImpl.super.setRefArgIfNotNull(ar, value); }
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws
			CloneNotSupportedException
		{ return ModelScenarioImpl.super.clone(cloneMap, cloneParent); }
		
		
		public void dump(int indentation)
		{ ModelScenarioImpl.super.dump(indentation); }
		
		
		public String identify()
		{ return ModelScenarioImpl.super.identify(); }
		
		
		public void printChildren(int indentationLevel)
		{ ModelScenarioImpl.super.printChildren(indentationLevel); }
		
		
		public void printChildren(int indentationLevel, boolean recursive)
		{ ModelScenarioImpl.super.printChildren(indentationLevel, recursive); }
	
	
		public void printChildrenRecursive(int indentationLevel)
		{ ModelScenarioImpl.super.printChildrenRecursive(indentationLevel); }
	}
}

