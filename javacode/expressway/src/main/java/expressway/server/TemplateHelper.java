/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import java.util.List;
import java.util.Set;


/**
 * A helper that contains most of the code needed to implement a Motif Template, a
 * TemplateInstance, and a MenuOwner.
 */
 
public class TemplateHelper
{
	public static Action createAction(MenuOwner mo, int position, String text,
		String desc, String javaMethodName)
	throws
		ParameterError
	{
		return mo.createAction(position, text, desc, javaMethodName);
	}
	
	
	public static Action createAction(MenuOwner mo, String text, 
		String desc, String javaMethodName)
	throws
		ParameterError
	{
		return mo.createAction(text, desc, javaMethodName);
	}
	
	
	public static MenuTree createMenuTree(MenuOwner mo, int position, String text, String desc)
	throws
		ParameterError
	{
		return mo.createMenuTree(position, text, desc);
	}
	
	
	public static MenuTree createMenuTree(Domain domain, String text, String desc)
	throws
		ParameterError
	{
		if (! (domain instanceof MenuOwner)) throw new RuntimeException(
			"Domain is not a MenuOwner");
		
		return createMenuTree((MenuOwner)domain, 0, text, desc);
	}
	
	
	/*public void deleteMenuItem(MenuOwner menuOwner, MenuItem menuItem)
	{
		// Delete (per the Delete Pattern) any other child Nodes that depend on
		// the existence of the Node that is being deleted.

		// Call super.delete<super-Node-type>.
		menuOwner.deleteNode(menuItem);danger

		// Nullify the owning reference to the Node being deleted.
		menuOwner.getMenuItems().remove(menuItem);
	}
	
	
	public void deleteMenuItem(MenuOwner menuOwner, int menuItemPos)
	{
		deleteMenuItem(menuOwner, menuOwner.getMenuItems().get(menuItemPos));
	}*/
	
	
	public static TemplateInstance createInstance(Set<TemplateInstance> templateInstances,
		Template template, String name, PersistentNode parent)
	throws
		ParameterError
	{
		CloneMap cloneMap = new CloneMap(template);
		TemplateInstance newInstance;
		if (! (template instanceof TemplateInstance)) throw new ParameterError(
			"Template is not also a TemplateInstance");
		
		try { newInstance = (TemplateInstance)(template.clone(cloneMap, parent)); }
		catch (CloneNotSupportedException ex) { throw new RuntimeException(ex); }
		
		//newInstance.setConstructionParameters(parent, name);
		newInstance.setTemplate(template);
		if (newInstance instanceof MenuOwner)
		{
			List<MenuItem> mis = ((MenuOwner)newInstance).getMenuItems();
			for (MenuItem mi : mis) mi.setDefinedBy(template);
		}
		
		templateInstances.add(newInstance);
		
		return newInstance;
	}
	
	
	public static void templateInstanceDeleted(Set<TemplateInstance> templateInstances,
		TemplateInstance inst)
	{
		templateInstances.remove(inst);
	}
	
	
	public static void verifyAgainstTemplate(TemplateInstance inst)
	throws
		PatternViolation
	{
		PatternHelper.verifyStructuralConformity(inst, inst.getTemplate());
	}
}


