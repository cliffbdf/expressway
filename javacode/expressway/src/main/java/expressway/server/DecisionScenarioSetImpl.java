/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.server.NamedReference.*;
import expressway.common.ClientModel.*;
import expressway.server.Event.*;
import expressway.ser.*;
import expressway.geometry.*;
import expressway.generalpurpose.XMLTools;
import expressway.generalpurpose.DateAndTimeUtils;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.io.*;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Map;
import java.util.Date;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import org.apache.commons.math.distribution.*;


public class DecisionScenarioSetImpl extends DecisionElementImpl implements DecisionScenarioSet
	// is a ModelScenarioChangeListener
{
	private ScenarioSetHelper helper;
	
			
	public DecisionScenarioSetImpl(String name, DecisionAttribute attr)
	throws
		ParameterError
	{
		super(attr.getDecisionDomain());
		this.setName(name);
		helper = new ScenarioSetHelper(this, new SuperScenarioSet());
		helper.setVariableAttribute(attr);
	}
	
	
	public DecisionScenarioSetImpl(DecisionScenario scenarioCopied,
		String name, DecisionAttribute attr, SortedSet<DecisionScenario> scenarios)
	throws
		ParameterError
	{
		this(name, attr);
		this.setScenarioThatWasCopied(scenarioCopied);
		for (DecisionScenario scenario : scenarios) this.addScenario(scenario);
	}


	DecisionScenarioSetImpl(DecisionScenario scenarioCopied, String name, DecisionAttribute attr)
	throws
		ParameterError
	{
		this(name, attr);
		this.setScenarioThatWasCopied(scenarioCopied);
	}
	
	
	public String getTagName() { return "model_scenario_set"; }
	

	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.DecisionScenarioSetIconImageName);
	}
	
	
	public Class getSerClass() { return ModelScenarioSetSer.class; }

	
  /* From PersistentNode */


	public int getNoOfHeaderRows() { return helper.getNoOfHeaderRows(); }
	
	
	public double getPreferredWidthInPixels() { return helper.getPreferredWidthInPixels(); }
	

	public double getPreferredHeightInPixels() { return helper.getPreferredHeightInPixels(); }
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		DecisionScenarioSetImpl newInstance = (DecisionScenarioSetImpl)(helper.clone(cloneMap, cloneParent));
		return newInstance;
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
	}
	

	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{
		super.writeSpecializedXMLAttributes(writer);
	}
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{
		super.writeSpecializedXMLElements(writer, indentation);
	}
	

	public void prepareForDeletion()
	{
		helper.prepareForDeletion();
		
		List<Scenario> ss = getScenarios();
		for (Scenario s : ss) ((DecisionScenario)s).decisionScenarioSetDeleted(this);
		
		// Nullify all references.
		helper = null;
	}

	
  /* From PersistentNodeImpl */


	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		DecisionScenarioSetSer ser = (DecisionScenarioSetSer)nodeSer;
		String[] scenarioIds = new String[getScenarios().size()];
		ser.setScenarioIds(scenarioIds);
			
		int i = 0;
		List<Scenario> scenarios = getScenarios();
		for (Scenario scen : scenarios) scenarioIds[i++] = scen.getNodeId();
		
		return helper.externalize(nodeSer);
	}
	
	
  /* From PersistentListNode */


	public PersistentNode getOrderedChild(String name) throws ElementNotFound
	{ return helper.getOrderedChild(name); }
	
	public PersistentNode getOrderedChildAt(int index) throws ParameterError
	{ return helper.getOrderedChildAt(index); }
	
	public int getIndexOfOrderedChild(PersistentNode child)
	{
		if ((child != null) & (! (child instanceof Scenario))) throw new RuntimeException(
			"A ScenarioSet may only have Scenarios are ordered children");
		return helper.getIndexOfOrderedChild(child);
	}
	
	public void insertOrderedChildAt(PersistentNode child, int index) throws ParameterError
	{
		if ((child != null) & (! (child instanceof Scenario))) throw new RuntimeException(
			"A ScenarioSet may only have Scenarios are ordered children");
		helper.insertOrderedChildAt((Scenario)child, index);
	}
	
	public boolean appendOrderedChild(PersistentNode child) throws ParameterError
	{
		if ((child != null) & (! (child instanceof Scenario))) throw new RuntimeException(
			"A ScenarioSet may only have Scenarios are ordered children");
		return helper.appendOrderedChild((Scenario)child);
	}
	
	public boolean removeOrderedChild(PersistentNode child) throws ParameterError
	{
		if ((child != null) & (! (child instanceof Scenario))) throw new RuntimeException(
			"A ScenarioSet may only have Scenarios are ordered children");
		return helper.removeOrderedChild(child);
	}
	
	public List<PersistentNode> getOrderedNodesOfKind(Class c)
	{
		return helper.getOrderedNodesOfKind(c);
	}
	
	public void removeOrderedNodesOfKind(Class c) { helper.removeOrderedNodesOfKind(c); }
	
	public boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError
	{
		return helper.addAllAsOrderedNodes(nodes);
	}

	public List<PersistentNode> getOrderedChildren()
	{
		return helper.getOrderedChildren();
	}
	
	public int getNoOfOrderedChildren() { return helper.getNoOfOrderedChildren(); }


	public PersistentNode getFirstOrderedChild()
	{
		return ListNodeHelper.getFirstOrderedChild(this);
	}
	
	
	public PersistentNode getLastOrderedChild()
	{
		return ListNodeHelper.getLastOrderedChild(this);
	}
	
	
	public int getOrderedChildPosition(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildPosition(this, child);
	}


	public PersistentNode getOrderedChildBefore(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildBefore(this, child);
	}
	
	
	public PersistentNode getOrderedChildAfter(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildAfter(this, child);
	}
	
	
	public PersistentNode createListChild(PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return ListNodeHelper.createListChild(this, nodeBefore, nodeAfter,
			scenario, copiedNode);
	}


	public void addOrderedChildBefore(PersistentNode child, PersistentNode nextChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildBefore(this, child, nextChild);
	}


	public void addOrderedChildAfter(PersistentNode child, PersistentNode priorChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildAfter(this, child, priorChild);
	}
	
		
  /* From ScenarioSet */


	public Scenario derivedFrom()
	{
		return helper.derivedFrom();
	}
	
	
	public void setVariableAttribute(Attribute attr)
	{
		helper.setVariableAttribute(attr);
	}
	
	
	public Attribute getVariableAttribute()
	{
		return helper.getVariableAttribute();
	}
	
	
	public void setScenarios(List<Scenario> scens)
	{
		helper.setScenarios(scens);
	}


	public List<Scenario> getScenarios()
	{
		return helper.getScenarios();
	}
	
	
	public void addScenario(Scenario scen)
	{
		helper.addScenario(scen);
		((DecisionScenario)scen).setDecisionScenarioSet(this);
	}
	

	public void removeScenario(Scenario scen)
	{
		helper.removeScenario(scen);
		((DecisionScenario)scen).setDecisionScenarioSet(null);
	}
	
	
	public void setScenarioThatWasCopied(Scenario scenario) { helper.setScenarioThatWasCopied(scenario); }
	
	
	public Scenario getScenarioThatWasCopied() { return helper.getScenarioThatWasCopied(); }

	
  /* From DecisionScenarioSet */
	
	
	public SortedSet<DecisionScenario> getDecisionScenarios()
	{
		SortedSet<DecisionScenario> dss = new TreeSetNullDisallowed<DecisionScenario>();
		List<Scenario> ss = getScenarios();
		for (Scenario s : ss) dss.add((DecisionScenario)s);
		return dss;
	}
	
	
  /* From DecisionScenarioChangeListener */
	
	
	public void decisionScenarioDeleted(DecisionScenario ds)
	{
		helper.removeScenario(ds);
	}
	
	
	public void decisionScenarioNameChanged(DecisionScenario ds, String oldName)
	{
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		helper = new ScenarioSetHelper(this, new SuperScenarioSet());
	}

	
  /* Proxy classes needed by ScenarioSetHelper */

	
	private class SuperScenarioSet extends SuperPersistentNode
	{
	}
	
	
	private class SuperPersistentNode implements PersistentNode
	{
		public int compareTo(PersistentNode node)
		{
			throw new RuntimeException("Should not be called");
		}
		
		
		public Object clone() throws CloneNotSupportedException
		{
			throw new RuntimeException("Should not be called");
		}

		
	  /* From PersistentNode */
		
		
		public ModelEngineLocal getModelEngine()
		{ return DecisionScenarioSetImpl.super.getModelEngine(); }
	
	
		public List<String> getInstantiableNodeKinds()
		{ return DecisionScenarioSetImpl.super.getInstantiableNodeKinds(); }
		
		
		public List<String> getStaticCreateMethodNames()
		{ return DecisionScenarioSetImpl.super.getStaticCreateMethodNames(); }
		
		
		public List<String> getInstantiableNodeDescriptions()
		{ return DecisionScenarioSetImpl.super.getInstantiableNodeDescriptions(); }
		
		
		public PersistentNode createChild(String kind, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.createChild(kind, layoutBound, outermostAffectedRef); }
		
		
		public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ DecisionScenarioSetImpl.super.deleteChild(child, outermostAffectedRef); }
			
		
		public void deleteChild(PersistentNode child)
		throws
			ParameterError
		{ DecisionScenarioSetImpl.super.deleteChild(child); }
			
			
		public Template findTemplate(String templatePath)
		throws
			ElementNotFound,
			ParameterError
		{ return DecisionScenarioSetImpl.super.findTemplate(templatePath); }
		
		
		public Class getNodeKind(String className)
		throws
			ClassNotFoundException,
			ParameterError
		{ return DecisionScenarioSetImpl.super.getNodeKind(className); }
			
		
		public Class getSerClass()
		{ return DecisionScenarioSetImpl.super.getSerClass(); }
		
		
		public Set<Attribute> getAttributes()
		{ return DecisionScenarioSetImpl.super.getAttributes(); }
		
		
		public String getTagName()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Attribute getAttribute(String attrName)
		{ return DecisionScenarioSetImpl.super.getAttribute(attrName); }
		
		
		public int getAttributeSeqNo(Attribute attr)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.getAttributeSeqNo(attr); }

		
		public void setAttributeSeqNo(Attribute attr, int seqNo)
		throws
			ParameterError
		{ DecisionScenarioSetImpl.super.setAttributeSeqNo(attr, seqNo); }
				
				
		public void setAttributeAsFirstInSeq(Attribute attr)
		throws
			ParameterError
		{ DecisionScenarioSetImpl.super.setAttributeAsFirstInSeq(attr); }
		
		
		public void setAttributeAsLastInSeq(Attribute attr)
		throws
			ParameterError
		{ DecisionScenarioSetImpl.super.setAttributeAsLastInSeq(attr); }
		
		
		public void moveAttributeBefore(Attribute attr, Attribute nextAttr)
		throws
			ParameterError
		{ DecisionScenarioSetImpl.super.moveAttributeBefore(attr, nextAttr); }
		
		
		public void moveAttributeAfter(Attribute attr, Attribute priorAttr)
		throws
			ParameterError
		{ DecisionScenarioSetImpl.super.moveAttributeAfter(attr, priorAttr); }
	
		
		public int getFirstAttributeSeqNo()
		{ return DecisionScenarioSetImpl.super.getFirstAttributeSeqNo(); }
		
		
		public int getLastAttributeSeqNo()
		{ return DecisionScenarioSetImpl.super.getLastAttributeSeqNo(); }
		
		
		public int getNumberOfAttributes()
		{ return DecisionScenarioSetImpl.super.getNumberOfAttributes(); }


		public List<Attribute> getAttributesInSequence()
		{ return DecisionScenarioSetImpl.super.getAttributesInSequence(); }
		
		
		public Attribute getFirstAttribute()
		{ return DecisionScenarioSetImpl.super.getFirstAttribute(); }
		
		
		public Attribute getLastAttribute()
		{ return DecisionScenarioSetImpl.super.getLastAttribute(); }

		
		public Attribute getAttributeAtSeqNo(int seqNo)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.getAttributeAtSeqNo(seqNo); }


		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.constructAttribute(name, defaultValue); }
		
	
		public Attribute constructAttribute(String name)
		{ return DecisionScenarioSetImpl.super.constructAttribute(name); }
		
	
		public Attribute createAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		
		public Attribute createAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.createAttribute(layoutBound, outermostAffectedRef); }
	
	
		public Attribute createAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.createAttribute(name, layoutBound, outermostAffectedRef); }
		
		
		public void deleteAttribute(Attribute attribute)
		throws
			ParameterError
		{ DecisionScenarioSetImpl.super.deleteAttribute(attribute); }
			
			
		public void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ DecisionScenarioSetImpl.super.deleteAttribute(attribute, outermostAffectedRef); }
			
			
		public boolean isOwnedByMotifDef()
		{ return DecisionScenarioSetImpl.super.isOwnedByMotifDef(); }
		
		
		public boolean isOwnedByActualTemplateInstance()
		{ return DecisionScenarioSetImpl.super.isOwnedByActualTemplateInstance(); }
		
		
		public boolean actuallyUsesMotif(MotifDef md)
		{ return DecisionScenarioSetImpl.super.actuallyUsesMotif(md); }
		
		
		public Image getLocalStoredImage(String handle)
		{ return DecisionScenarioSetImpl.super.getLocalStoredImage(handle); }
		
		
		public void deleteFromAllNodesTable()
		{ DecisionScenarioSetImpl.super.deleteFromAllNodesTable(); }
		
		
		public String getName()
		{ return DecisionScenarioSetImpl.super.getName(); }
		
		
		public void setHTMLDescription(String html)
		{ DecisionScenarioSetImpl.super.setHTMLDescription(html); }
		
		
		public String getHTMLDescription()
		{ return DecisionScenarioSetImpl.super.getHTMLDescription(); }
		
		
		public String getFullName()
		{ return DecisionScenarioSetImpl.super.getFullName(); }
		
		
		public String getNameRelativeTo(PersistentNode parent)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.getNameRelativeTo(parent); }
	
	
		public void setName(String name)
		throws
			ParameterError
		{ DecisionScenarioSetImpl.super.setName(name); }
		
		
		public String setNameAndNotify(String newName)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.setNameAndNotify(newName); }
		
		
		public void renameChild(PersistentNode child, String newName)
		throws
			ParameterError
		{ DecisionScenarioSetImpl.super.renameChild(child, newName); }
		
		
		public Domain getDomain()
		{ return DecisionScenarioSetImpl.super.getDomain(); }
		
		
		public String createUniqueChildName()
		{ return DecisionScenarioSetImpl.super.createUniqueChildName(); }
	
		
		public String createUniqueChildName(String baseName)
		{ return DecisionScenarioSetImpl.super.createUniqueChildName(baseName); }
		
		
		public SortedSet<PersistentNode> getChildren()
		{ return DecisionScenarioSetImpl.super.getChildren(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive()
		{ return DecisionScenarioSetImpl.super.getChildrenRecursive(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive(Class kind)
		{ return DecisionScenarioSetImpl.super.getChildrenRecursive(kind); }
		
		
		public PersistentNode getChild(String name)
		throws
			ElementNotFound
		{ return DecisionScenarioSetImpl.super.getChild(name); }
	
	
		public PersistentNode getParent()
		{ return DecisionScenarioSetImpl.super.getParent(); }
		
		
		public void setParent(PersistentNode parent)
		{ DecisionScenarioSetImpl.super.setParent(parent); }
		
		
		public boolean isDescendantOf(PersistentNode ancestor)
		{ return DecisionScenarioSetImpl.super.isDescendantOf(ancestor); }
		
		
		public PersistentNode findNode(String qualifiedName)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.findNode(qualifiedName); }
	
	
		public PersistentNode findNode(String[] pathParts)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.findNode(pathParts); }
			
		
		public PersistentNode findFirstNestedNode(String name)
		throws
			ElementNotFound
		{ return DecisionScenarioSetImpl.super.findFirstNestedNode(name); }
		
		
		public PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.getChildByRelativeQualifiedName(pathParts); }
		
		
		public void prepareForDeletion()
		{ DecisionScenarioSetImpl.super.prepareForDeletion(); }
		
		
		public Display getDisplay()
		{ return DecisionScenarioSetImpl.super.getDisplay(); }
		
		
		public void setVisible(boolean vis)
		{ DecisionScenarioSetImpl.super.setVisible(vis); }
		
		
		public boolean isVisible()
		{ return DecisionScenarioSetImpl.super.isVisible(); }
		
		
		public void layout()
		{ DecisionScenarioSetImpl.super.layout(); }
		
		
		public Set<PersistentNode> getNodesWithSpecialLayout()
		{ return DecisionScenarioSetImpl.super.getNodesWithSpecialLayout(); }
		
		
		public void conditionalLayout()
		{ DecisionScenarioSetImpl.super.conditionalLayout(); }
		
		
		public PersistentNode layoutUpward(PersistentNode layoutBound)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.layoutUpward(layoutBound); }
	
		
		public void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		{ DecisionScenarioSetImpl.super.layout(layoutBound, outermostAffectedRef); }
	
	
		public void setPredefined(boolean pre)
		{ DecisionScenarioSetImpl.super.setPredefined(pre); }
		
		
		public boolean isPredefined()
		{ return DecisionScenarioSetImpl.super.isPredefined(); }
		
		
		public void setDeletable(boolean del)
		{ DecisionScenarioSetImpl.super.setDeletable(del); }
		
		
		public boolean isDeletable()
		{ return DecisionScenarioSetImpl.super.isDeletable(); }
		
		
		public void setMovable(boolean m)
		{ DecisionScenarioSetImpl.super.setMovable(m); }
		
		
		public boolean isMovable()
		{ return DecisionScenarioSetImpl.super.isMovable(); }
		
		
		public void setResizable(boolean yes)
		{ DecisionScenarioSetImpl.super.setResizable(yes); }
		
		
		public boolean isResizable()
		{ return DecisionScenarioSetImpl.super.isResizable(); }
		
		
		public double getWidth()
		{return DecisionScenarioSetImpl.super.getWidth();  }
	
	
		public double getHeight()
		{ return DecisionScenarioSetImpl.super.getHeight(); }
		
		
		public void setWidth(double width)
		{ DecisionScenarioSetImpl.super.setWidth(width); }
	
	
		public void setHeight(double height)
		{ DecisionScenarioSetImpl.super.setHeight(height); }
		
		
		public boolean useBorderWhenIconified()
		{ return DecisionScenarioSetImpl.super.useBorderWhenIconified(); }
		
		
		public void setUseBorderWhenIconified(boolean yesOrNo)
		{ DecisionScenarioSetImpl.super.setUseBorderWhenIconified(yesOrNo); }
		
		
		public Image getIconImage()
		{ return DecisionScenarioSetImpl.super.getIconImage(); }
		
		
		public void setIconImage(String path)
		throws
			IOException
		{ DecisionScenarioSetImpl.super.setIconImage(path); }
		
		
		public ImageIcon getImageIcon()
		{ return DecisionScenarioSetImpl.super.getImageIcon(); }
		
		
		public String getIconImageHandle()
		{ return DecisionScenarioSetImpl.super.getIconImageHandle(); }
		
		
		public double getIconWidth()
		{ return DecisionScenarioSetImpl.super.getIconWidth(); }
		
		
		public double getIconHeight()
		{ return DecisionScenarioSetImpl.super.getIconHeight(); }
		
		
		public void setLayoutManager(LayoutManager lm)
		{ DecisionScenarioSetImpl.super.setLayoutManager(lm); }
		
		
		public LayoutManager getLayoutManager()
		{ return DecisionScenarioSetImpl.super.getLayoutManager(); }
		
		
		public String getDefaultViewTypeName()
		{ return DecisionScenarioSetImpl.super.getDefaultViewTypeName(); }
		
		public void setViewClassName(String name)
		{ DecisionScenarioSetImpl.super.setViewClassName(name); }
		
		public String getViewClassName()
		{ return DecisionScenarioSetImpl.super.getViewClassName(); }

		
		public boolean contains(Point p)
		{ return DecisionScenarioSetImpl.super.contains(p); }
		
		
		public boolean contains(double x, double y)
		{ return DecisionScenarioSetImpl.super.contains(x, y); }
		
		
		public Face[] getFaces(double x, double y)
		{ return DecisionScenarioSetImpl.super.getFaces(x, y); }
		
		
		public Face[] getIconFaces(double x, double y)
		{ return DecisionScenarioSetImpl.super.getIconFaces(x, y); }
		
		
		public Position findSideClosestToInternalPoint(double x, double y)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.findSideClosestToInternalPoint(x, y); }
		
		
		public Position findSideClosestToExternalPoint(Point location)
		{ return DecisionScenarioSetImpl.super.findSideClosestToExternalPoint(location); }
		
		
		public void setSizeToStandard()
		{ DecisionScenarioSetImpl.super.setSizeToStandard(); }
		
		
		public double getHeaderLabelHeight()
		{ return DecisionScenarioSetImpl.super.getHeaderLabelHeight(); }
		
		
		public int getNoOfHeaderRows()
		{ throw new RuntimeException("Should not be called"); }
	
	
		public SortedSet<PersistentNode> getPredefinedNodes()
		{ return DecisionScenarioSetImpl.super.getPredefinedNodes(); }
		
		
		public double getMinimumWidth()
		{ return DecisionScenarioSetImpl.super.getMinimumWidth(); }
		
		
		public double getMinimumHeight()
		{ return DecisionScenarioSetImpl.super.getMinimumHeight(); }
		
		
		public double getPreferredWidthInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		public double getPreferredHeightInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		
		public int getPreferredHeaderRowHeightInPixels()
		{ return DecisionScenarioSetImpl.super.getPreferredHeaderRowHeightInPixels(); }
		
		
		public double getX() { return DecisionScenarioSetImpl.super.getX(); }
		public double getY() { return DecisionScenarioSetImpl.super.getY(); }
		public double getScale() { return DecisionScenarioSetImpl.super.getScale(); }
		public double getOrientation() { return DecisionScenarioSetImpl.super.getOrientation(); }
		public void setX(double x) { DecisionScenarioSetImpl.super.setX(x); }
		public void setY(double y) { DecisionScenarioSetImpl.super.setY(y); }
		public void setLocation(double x, double y) { DecisionScenarioSetImpl.super.setLocation(x, y); }
		public void setScale(double scale) { DecisionScenarioSetImpl.super.setScale(scale); }
		public void setOrientation(double angle) { DecisionScenarioSetImpl.super.setOrientation(angle); }
		
		
		public boolean allowShift(PersistentNode childNode)
		{ return DecisionScenarioSetImpl.super.allowShift(childNode); }
		
		
		public long getLastUpdated()
		{ return DecisionScenarioSetImpl.super.getLastUpdated(); }
		
		
		public int getVersionNumber()
		{ return DecisionScenarioSetImpl.super.getVersionNumber(); }
		
		
		public String getClientThatLastModified()
		{ return DecisionScenarioSetImpl.super.getClientThatLastModified(); }
		
		
		public void update()
		{ DecisionScenarioSetImpl.super.update(); }
		
	
		public Date getDeletionDate()
		{ return DecisionScenarioSetImpl.super.getDeletionDate(); }
		
	
		public PersistentNode getPriorVersion()
		{ return DecisionScenarioSetImpl.super.getPriorVersion(); }
		
		
		public String getPriorVersionNodeId()
		{ return DecisionScenarioSetImpl.super.getPriorVersionNodeId(); }
		
		
		public String getNodeId()
		{ return DecisionScenarioSetImpl.super.getNodeId(); }
		
		
		public String[] getNodeIdPath()
		{ return DecisionScenarioSetImpl.super.getNodeIdPath(); }
		
		
		public String getNodeIdPathString()
		{ return DecisionScenarioSetImpl.super.getNodeIdPathString(); }
		
		
		public void setWatch(boolean enable)
		{ DecisionScenarioSetImpl.super.setWatch(enable); }
		
		
		public boolean getWatch()
		{ return DecisionScenarioSetImpl.super.getWatch(); }
	
	
		public NodeSer externalize()
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.externalize(); }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.externalize(nodeSer); }
		
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{ DecisionScenarioSetImpl.super.writeAsXML(writer, indentation); }
		
		
		public void writeSpecializedXMLAttributes(PrintWriter writer)
		throws
			IOException
		{ DecisionScenarioSetImpl.super.writeSpecializedXMLAttributes(writer); }
		
		
		public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
		throws
			IOException
		{ DecisionScenarioSetImpl.super.writeSpecializedXMLElements(writer, indentation); }
		
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		{ return DecisionScenarioSetImpl.super.getAvailableHTTPFormats(descriptions); }
		
		
		public String getWebURLString(String format)
		throws
			ParameterError
		{ return DecisionScenarioSetImpl.super.getWebURLString(format); }
		
		
		public byte[] renderAsSVG(double wCmMax, double hCmMax)
		throws
			IOException
		{ return DecisionScenarioSetImpl.super.renderAsSVG(wCmMax, hCmMax); }
	
		
		public java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
			double pixPerModel)
		{ return DecisionScenarioSetImpl.super.renderHierarchyAsLeaves(g, x, y, pixPerModel); }
		
		
		public java.awt.Dimension render(Graphics2D g, double pixPerModel)
		{ return DecisionScenarioSetImpl.super.render(g, pixPerModel); }
	
	
		public void renderAsChild(Graphics2D g, double pixPerModel)
		{ DecisionScenarioSetImpl.super.renderAsChild(g, pixPerModel); }
		
		
		public byte[] renderAsHTML()
		throws
			ParameterError,
			IOException
		{ return DecisionScenarioSetImpl.super.renderAsHTML(); }
		
	
		public void notifyAllListeners()
		throws
			Exception
		{ DecisionScenarioSetImpl.super.notifyAllListeners(); }
		
		
		public void notifyNodeDeleted()
		throws
			Exception
		{ DecisionScenarioSetImpl.super.notifyNodeDeleted(); }
		
		
		public void notifyAllListeners(PeerNotice notice)
		throws
			Exception
		{ DecisionScenarioSetImpl.super.notifyAllListeners(notice); }
		
		
		public String[] createNodeIdArray(Collection col)
		{ return DecisionScenarioSetImpl.super.createNodeIdArray(col); }
		
		
		public String[] createNodeIdArray(Map map)
		{ return DecisionScenarioSetImpl.super.createNodeIdArray(map); }
	
	
		public String getNodeIdOrNull(PersistentNode node)
		{ return DecisionScenarioSetImpl.super.getNodeIdOrNull(node); }
	
	
		public void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
		{ DecisionScenarioSetImpl.super.setRefArgIfNotNull(ar, value); }
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws
			CloneNotSupportedException
		{ return DecisionScenarioSetImpl.super.clone(cloneMap, cloneParent); }
		
		
		public void dump(int indentation)
		{ DecisionScenarioSetImpl.super.dump(indentation); }
		
		
		public String identify()
		{ return DecisionScenarioSetImpl.super.identify(); }
		
		
		public void printChildren(int indentationLevel)
		{ DecisionScenarioSetImpl.super.printChildren(indentationLevel); }
		
		
		public void printChildren(int indentationLevel, boolean recursive)
		{ DecisionScenarioSetImpl.super.printChildren(indentationLevel, recursive); }
	
	
		public void printChildrenRecursive(int indentationLevel)
		{ DecisionScenarioSetImpl.super.printChildrenRecursive(indentationLevel); }
	}
}

