/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import java.awt.Font;


/**
 * View space constants. These are not used to determine Model Element dimensions.
 */
 
public interface ViewConstants
{
	int getPixelsPerInch();
	int getPixelsPerCm();
	int getNormalBorderThickness();
	int getTopBorderThickness();
	int getIconWidth();
	int getIconHeight();
	int getBorderFontPointSize();
	int getConduitThickness();
	int getPortDiameter();
	//int getAttributeHeight();
	
	void setNormalBorderThickness(int x);
	void setTopBorderThickness(int x);
	void setIconWidth(int x);
	void setIconHeight(int x);
	void setBorderFontPointSize(int x);
	//void setAttributeHeight(int h);
	
	/** ************************************************************************
	 * Return the Font to be used for rendering the title on Components.
	 */
	 
	Font getComponentTitleFont();
}

