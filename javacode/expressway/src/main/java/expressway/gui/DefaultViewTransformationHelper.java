/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.ModelAPITypes.*;



/** ****************************************************************************
 * Provide absolute transformations between a View space and a model space.
 */
 
public class DefaultViewTransformationHelper
{
	/**
	 * Transform an absolute X coordinate in the model Domain to an absolute X
	 * coordinate in this View.
	 */
	 
	public static int transformNodeXCoordToView(double modelXCoord, double pixelXSize,
		int xOriginOffset  // location of model space origin in the View coord system.
		)
	throws
		ParameterError
	{
		double d = (double)xOriginOffset + modelXCoord / pixelXSize;
		
		if ((d > Integer.MAX_VALUE)
			|| (d < Integer.MIN_VALUE)) throw new ParameterError(
				d + " is out of range of the display space");
				
		return (int)d;
	}
	
	
	/**
	 * Transform an absolute Y coordinate in the model Domain to an absolute Y
	 * coordinate in this View.
	 */
	 
	public static int transformNodeYCoordToView(double modelYCoord, double pixelYSize,
		int yOriginOffset  // location of model space origin in the View coord system.
		)
	throws
		ParameterError
	{
		double d = (double)yOriginOffset - modelYCoord / pixelYSize;

		if ((d > Integer.MAX_VALUE)
			|| (d < Integer.MIN_VALUE)) throw new ParameterError(
				d + " is out of range of the display space");
				
		return (int)d;
	}
	
	
	/**
	 * Transform a distance along X in the model Domain to a distance along X
	 * in this View. The distance is not directional: a positive input distance
	 * always produces a positive transformed distance.
	 */
	 
	public static int transformNodeDXToView(double nodeWidth, double pixelXSize)
	throws
		ParameterError
	{
		double d = nodeWidth / pixelXSize;
		//double d = pixelXSize * nodeWidth;
		
		if ((d > Integer.MAX_VALUE)
			|| (d < Integer.MIN_VALUE)) throw new ParameterError(
				d + " is out of range of the display space");
				
		return (int)d;
	}
		
		
	/**
	 * Transform a distance along Y in the model Domain to a distance along Y
	 * in this View. The distance is not directional: a positive input distance
	 * always produces a positive transformed distance.
	 */
	 
	public static int transformNodeDYToView(double nodeHeight, double pixelYSize)
	throws
		ParameterError
	{
		double d = nodeHeight / pixelYSize;
		//double d = pixelYSize * nodeHeight;
		
		if ((d > Integer.MAX_VALUE)
			|| (d < Integer.MIN_VALUE)) throw new ParameterError(
				d + " is out of range of the display space");
				
		return (int)d;
	}

	
  /* Reverse transformations. */


	public static double transformViewXCoordToNode(int viewXCoord, double pixelXSize,
		int xOriginOffset)
	throws
		ParameterError
	{
		return ((double)viewXCoord - (double)xOriginOffset) * pixelXSize;
	}
	
	
	public static double transformViewYCoordToNode(int viewYCoord, double pixelYSize,
		int yOriginOffset)
	throws
		ParameterError
	{
		return - ((double)viewYCoord - (double)yOriginOffset) * pixelYSize;
	}
		
		
	public static double transformViewDXToNode(int viewWidth, double pixelXSize)
	throws
		ParameterError
	{
		return (double)viewWidth * pixelXSize;
	}
		
		
	public static double transformViewDYToNode(int viewHeight, double pixelYSize)
	throws
		ParameterError
	{
		return (double)viewHeight * pixelYSize;
	}
}

