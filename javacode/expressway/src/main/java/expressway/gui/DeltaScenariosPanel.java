/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import javax.swing.*;
import java.awt.FlowLayout;
import java.awt.event.*;
import java.io.Serializable;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.ser.ScenarioSer;
import expressway.generalpurpose.*;


public class DeltaScenariosPanel extends JFrame
{
	private final String attrId;
	private final ModelEngineRMI modelEngine;
	
	private JLabel descriptionLabelA;
	private JLabel descriptionLabelB;
	private JLabel descriptionLabelC;
	private JTextField lowerBoundTextField;
	private JTextField upperBoundTextField;
	private JButton createButton;
	private JButton cancelButton;
	
	
	public DeltaScenariosPanel(final String modelScenarioId, AttributeVisual attrVisual)
	{
		super("Define Delta Scenarios");
		this.setAlwaysOnTop(true);
		
		try { this.attrId = attrVisual.getNodeId(); }
		catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }
		
		this.modelEngine = attrVisual.getClientView().getModelEngine();
		
		this.setSize(400, 200);
		
		this.setLayout(new FlowLayout());
		
		add(descriptionLabelA = new JLabel("Two new Scenarios will be created that are "));
		add(lowerBoundTextField = new JTextField());
		add(descriptionLabelB = new JLabel("% below and "));
		add(upperBoundTextField = new JTextField());
		add(descriptionLabelC = new JLabel("% above the current value of Attribute " +
			attrVisual.getName()));
		
		add(createButton = new JButton("Create"));
		add(cancelButton = new JButton("Cancel"));
		
		lowerBoundTextField.setColumns(2);
		//lowerBoundTextField.setSize(20, 14);
		
		upperBoundTextField.setColumns(2);
		//upperBoundTextField.setSize(20, 14);
		
		lowerBoundTextField.setText("10");
		upperBoundTextField.setText("10");
		
		
		createButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				double lowerBound;  // a percent
				double upperBound;  // a percent
				String text = null;
				
				try
				{
					text = lowerBoundTextField.getText();
					lowerBound = Double.parseDouble(text);
					
					text = upperBoundTextField.getText();
					upperBound = Double.parseDouble(text);
				}
				catch (NumberFormatException nfe)
				{
					JOptionPane.showMessageDialog(DeltaScenariosPanel.this,
						"Not numeric: " + text, "Error", JOptionPane.ERROR_MESSAGE);
						return;
				}
				
				
				try
				{
					// Get Attribute value
					
					Serializable value = modelEngine.getAttributeValueById(
						DeltaScenariosPanel.this.attrId, modelScenarioId);
					if (! (value instanceof Number))
					{
						JOptionPane.showMessageDialog(DeltaScenariosPanel.this,
							"Attribute's current value is not Numeric: it is a " +
							value.getClass().getName(), "Error", JOptionPane.ERROR_MESSAGE);
							return;
					}
					
					double doubleValue = ((Number)value).doubleValue();
					
					
					// Make two copies of the Scenario.
					
					ScenarioSer copy1Ser = modelEngine.copyScenario(modelScenarioId);
					
					ScenarioSer copy2Ser = modelEngine.copyScenario(modelScenarioId);
				
					modelEngine.setAttributeValue(true, attrId, copy1Ser.getNodeId(), 
						new Double(doubleValue - (lowerBound * doubleValue / 100.0)));
					
					modelEngine.setAttributeValue(true, attrId, copy2Ser.getNodeId(), 
						new Double(doubleValue + (upperBound * doubleValue / 100.0)));
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(DeltaScenariosPanel.this, ex);
						return;
				}
				
				JOptionPane.showMessageDialog(DeltaScenariosPanel.this,
					"Scenarios created", "Scenarios Created", 
					JOptionPane.INFORMATION_MESSAGE);
				
				DeltaScenariosPanel.this.dispose();
			}
		});
		
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				DeltaScenariosPanel.this.dispose();
			}
		});
		
		this.show();
	}
}

