/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.gui.View.*;
import expressway.gui.GraphicView.*;
import expressway.common.VisualComponent.*;
import expressway.help.*;
import expressway.ser.*;
import java.awt.event.*;
import javax.swing.*;
import java.lang.reflect.*;
import java.rmi.server.RMIClassLoader;


public class MenuItemHelper
{
	public static void addMenuOwnerItems(final View view, final VisualComponent containerVisual,
		MenuOwnerSer menuOwnerSer, JPopupMenu popup, final int x, final int y)
	{
		String[] menuItemTextAr = menuOwnerSer.getMenuItemTextAr();
		if (menuItemTextAr == null) return;
		
		boolean grayOutActionItems = containerVisual.isOwnedByMotifDef();
		int i = 0;
		for (String text : menuItemTextAr)
		{
			final HelpfulMenuItem mi = new HelpfulMenuItem(text,
				false, menuOwnerSer.getMenuItemDescAr()[i]);
	
			final String javaMethodName = menuOwnerSer.getJavaMethodNames()[i];
			final String menuTreeNodeId = menuOwnerSer.getMenuTreeNodeIds()[i];
			
			if (menuTreeNodeId == null)  // must be an action.
			{
				if (grayOutActionItems)
				{
					mi.setEnabled(false);
					popup.add(mi);
					i++;
					continue;
				}
				
				// Add action listener to perform the specified Java method,
				// in the context of the client View.
				
				final MenuItemContext context = new MenuItemContext()
				{
					public View getClientView()
					{
						return view;
					}
					
					public VisualComponentDisplayArea getDisplayArea()
					{
						return view.getDisplayArea();
					}
					
					public ScenarioVisual getCurrentScenarioVisual()
					{
						if (view instanceof ScenarioVisual)
							return (ScenarioVisual)(view.getCurrentScenarioVisual());
						else
							return null;
					}
					
					public VisualComponent getVisualComponent()
					{
						return containerVisual;
					}
					
					public HelpfulMenuItem getMenuItem()
					{
						return mi;
					}
					
					public ModelEngineRMI getModelEngine()
					{
						return view.getModelEngine();
					}
				};
				
				mi.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						if (javaMethodName == null)
						{
							JOptionPane.showMessageDialog(containerVisual.getContainer(),
								"No action is associated with this Menu Item");
							return;
						}

						String[] parts;
						int perPos = javaMethodName.lastIndexOf('.');
						if (perPos < 0)
						{
							ErrorDialog.showErrorDialog(containerVisual.getContainer(),
								"Java method name " + javaMethodName +
									" does not appear to have the format <class>.<method>");
							return;
						}
						
						String className = javaMethodName.substring(0, perPos);
						Class c;
						
						
						// Use the ClientSideClassLoader to load the Java action class.
						
						ClassLoader cl = Thread.currentThread().getContextClassLoader();
						ClientSideClassLoader clientSideClassLoader;
						if (cl instanceof ClientSideClassLoader)
							clientSideClassLoader = (ClientSideClassLoader)cl;
						else
						{
							clientSideClassLoader = new ClientSideClassLoader(view.getModelEngine(), cl);
							Thread.currentThread().setContextClassLoader(clientSideClassLoader);
						}
						
						try { c = clientSideClassLoader.loadClass(className, true); }
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(containerVisual.getContainer(),
								"Unable to load class " + className,
								"Cannot load class", ex);
							return;
						}
						
						String methodName = javaMethodName.substring(perPos+1);
						Method method;
						try { method = c.getMethod(methodName, MenuItemContext.class); }
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(containerVisual.getContainer(),
								"Unable to find method " + methodName +
									" in class " + className,
								"Cannot find method", ex);
							return;
						}
						
						try { method.invoke(null, context); }
						catch (Exception ex)
						{
							ErrorDialog.showThrowableDialog(containerVisual.getContainer(),
								"While executing template method " + methodName,
								"Error executing template method", ex);
						}
					}
				});
			}
			else // must be a MenuTree
			{
				if (javaMethodName != null)
				{
					JOptionPane.showMessageDialog(containerVisual.getContainer(),
						"Menu Item '" + text + "' has a menu tree but also an action " +
						"(which will be ignored)");
				}
				
				mi.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						MenuTreeSer menuTreeSer = null;
						try { menuTreeSer =
							(MenuTreeSer)(view.getModelEngine().getNode(menuTreeNodeId));
						}
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(containerVisual.getContainer(), ex);
							return;
						}
						
						// Create a popup menu for the MenuTree.
						
						JPopupMenu subMenu = new JPopupMenu();
						
						addMenuOwnerItems(view, containerVisual, menuTreeSer, subMenu, x, y);
						
						subMenu.show(containerVisual.getContainer(), x, y);
					}
				});
			}
				
			popup.add(mi);
			i++;
		}
	}
}

