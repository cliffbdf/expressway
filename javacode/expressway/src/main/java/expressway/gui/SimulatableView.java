/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import javax.swing.*;
import java.util.Set;
import java.awt.Font;
import javax.swing.JPanel;
import java.awt.Component;
import java.awt.Container;


/**
 * A View that can be used to initiate simulations of the Nodes that it depicts.
 */
 
public interface SimulatableView extends NodeView
{
	/** ************************************************************************
	 * Return all of the SimulationVisuals owned by this View and that monitor
	 * the specified server provided callback id.
	 */
	 
	Set<SimulationVisual> getSimulationVisuals(Integer callbackId);
	
	
	/** ************************************************************************
	 * Return all of the SimulationVisuals owned by this View.
	 */
	 
	Set<SimulationVisual> getSimulationVisuals();
	
	
	void addSimVisual(Integer id, SimulationVisual simVisualForId);
	
	
	void addSimVisuals(Integer id, Set<SimulationVisual> newSimVisualsForId);
}

