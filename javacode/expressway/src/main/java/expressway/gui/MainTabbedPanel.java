/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.gui.PanelManager.*;
import expressway.swing.MultiHierarchyPanel;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.event.*;
import javax.swing.JMenuBar;
import java.util.Set;
import java.util.List;
import java.util.Vector;
import java.util.HashSet;
import java.awt.*;


/**
 * The main tabbed panel for the Expressway GUI application.
 */
 
public class MainTabbedPanel extends JTabbedPane implements PanelManager
{
	private final JFrame frame;
	private final ClientMainApplication mainApplication;
	private int curSelIndex = 0;
	
	/** Windows other than the main Window, contining Panels that do not belong
		to the JTabbedPane. */
	private List<Window> disembodiedWindows = new Vector<Window>();
	
	
	MainTabbedPanel(JFrame frame, ClientMainApplication mainApplication)
	{
		this.frame = frame;
		this.mainApplication = mainApplication;
		

		// Re-position sub-components.
		
		validate();
		
		
		// Respond to changes in the selected tab.
		
		addChangeListener(new ChangeListener()
		{
			public void stateChanged(ChangeEvent e)
			{
				Component compToSave = null;
				
				synchronized (MainTabbedPanel.this)
				{
					int newSelIndex = MainTabbedPanel.this.getSelectedIndex();
					if (MainTabbedPanel.this.curSelIndex != newSelIndex)
					{
						if (MainTabbedPanel.this.getTabCount() > MainTabbedPanel.this.curSelIndex)
							compToSave = getTabComponentAt(MainTabbedPanel.this.curSelIndex);
					}
					
					MainTabbedPanel.this.curSelIndex = newSelIndex;
				}
				
				if (compToSave != null)
				{
					((View)compToSave).saveFocusFieldChanges();
				}
			}
		});


		// Make visible.

		setVisible(true);
	}
	
	
	public JFrame getFrame() { return frame; }
	
	
	public Component getComponent() { return this; }
	
	
	public Container getContainer() { return this; }
	
	
	public JComponent getJComponent() { return this; }
	
	
	public ClientMainApplication getClientMainApplication() { return mainApplication; }

	
	public String getName() { return super.getName(); }
	
	
	public int getRecommendedPanelAreaWidth() { return this.getWidth(); }
	
	
	public int getRecommendedPanelAreaHeight() { return this.getHeight(); }
	
	
	public void addPanel(String title, Container panel // typically a ViewContainer
		, boolean asPopup)
	{
		Window newWindow = null;
		
		if (asPopup)
			newWindow = addDisembodiedPanel(title, panel);
		else
			this.addTab(title, panel);
		
		//if (panel instanceof ViewPanelBase)
		//	try { ((ViewPanelBase)panel).postConstructionSetup(); }
		//	catch (Exception ex) { ErrorDialog.showReportableDialog(this, ex); }
		
		if (asPopup) newWindow.show();

	}
	
	
	protected Window addDisembodiedPanel(String title, Container panel)
	{
		Insets insets = this.getRootPane().getInsets();
		int panelWidth = 800;
		int controlAreaHeight = 100;
		int panelHeight = 500;
		
		if (panel instanceof NodeViewPanelBase)
		{
			NodeViewPanelBase view = (NodeViewPanelBase)panel;
			NodeSer nodeSer = view.getOutermostNodeSer();
			
			view.setIsPopup(true);
			
			if (view instanceof GraphicView) try
			{
				GraphicView gview = (GraphicView)view;
				
				panelWidth = gview.transformNodeDXToView(nodeSer.getWidth()) +
					insets.left + insets.right + 20;
				
				controlAreaHeight = getRootPane().getHeight() -
					gview.getDisplayArea().getHeight();
				
				panelHeight = gview.transformNodeDYToView(
					nodeSer.getHeight()) + insets.top + insets.bottom + 
					controlAreaHeight;
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(this, ex);
			}
		}
		
		panel.setSize(panelWidth, panelHeight);
		
		Window w = getDisembodiedWindowContainingPanel(panel);
		if (w != null) return w;
		
		JFrame newFrame = new JFrame(title);
		
		JMenuBar menuBar = this.getRootPane().getJMenuBar();
		
		int newFrameWidth = Math.max(getMinimumViewFrameWidth(),
			panel.getWidth() + insets.left + insets.right);
		
		int newFrameHeight = 
			panel.getHeight() + insets.top + insets.bottom +
			(menuBar == null? 0 : menuBar.getHeight());
		
		newFrame.setSize(newFrameWidth, newFrameHeight);
		
		newFrame.add(panel, BorderLayout.CENTER);
		disembodiedWindows.add(newFrame);

		return newFrame;
	}
	
	
	/**
	 * Return the minimum width that a Frame containing a GraphicView should be
	 * in order to ensure that all controls have sufficient room.
	 */
	 
	protected int getMinimumViewFrameWidth()
	{
		return this.frame.getMinimumSize().width;
	}
	
	
	protected Window getDisembodiedWindowContainingPanel(Component panel)
	{
		for (Window w : disembodiedWindows)
		{
			if (containsRecursive(w, panel)) return w;
		}
		
		return null;
	}
	
	
	protected boolean containsRecursive(Container container, Component component)
	{
		Component[] components = container.getComponents();
		for (Component c : components)
		{
			if (c == component) return true;
			
			if (c instanceof Container)
				if (containsRecursive((Container)c, component)) return true;
		}
		
		return false;
	}
	
	
	public void removePanel(Component panel)
	{
		Component comp = panel;
		for (;;)
		{
			Container parent = comp.getParent();
			if (parent instanceof MultiHierarchyPanel)
			{
				parent.remove(comp);
				if (((MultiHierarchyPanel)parent).getNoOfTreeTableComponents() == 0)
				{
					// go on to remove the Window or the tab from the PanelManager.
				}
				else
					return;
			}
			else if (parent instanceof PanelManager)
			{
				parent.remove(comp);
				return;
			}
			else if (parent instanceof Window)
			{
				if (getDisembodiedWindowContainingPanel(comp) == null)
					throw new RuntimeException("Encountered Window not known to PanelManager");
				disembodiedWindows.remove((Window)parent);
				((Window)parent).dispose();
				return;
			}
			
			comp = parent;
		}
	}
	
	
	/** For debug only. */
	void printChildrenRecursively(int level, Component comp)
	{
		for (int i = 0; i < level; i++) GlobalConsole.print("\t");
		GlobalConsole.println(comp.getName() + " (" + comp.getClass().getName() + ")");
		if (comp instanceof Container)
		{
			Component[] children = ((Container)comp).getComponents();
			for (Component c : children)
			{
				printChildrenRecursively(level+1, c);
			}
		}
	}
	
	
	public boolean isPopupView(View view)
	{
		return disembodiedWindows.contains(view.getWindow());
	}
	
	
	public void setTitleAt(int id, String title)
	{
		if (id >= 1000)
		{
			Window w = null;
			try { w = disembodiedWindows.get(id-1000); }
			catch (IndexOutOfBoundsException ex) { return; }
			
			w.setName(title);
			if (w instanceof Frame) ((Frame)w).setTitle(title);
			
			return;
		}
		
		super.setTitleAt(id, title);
	}

	
	public List<NodeView> getNodeViews()
	{
		return new Vector<NodeView>(getNodeViewsForNode(null));
	}
	
	
	public Set<NodeView> getNodeViewsForDomain(String domainId)
	{
		// Identify the popup Views for the Domain.
		
		Set<NodeView> nodeViews = new HashSet<NodeView>();
		
		for (Window w : disembodiedWindows)
		{
			Component[] components = w.getComponents();
			for (Component c : components)
			{
				if (c instanceof NodeViewContainer)
				{
					NodeViewContainer nvContainer = (NodeViewContainer)c;
					Set<NodeView> nvs = nvContainer.getNodeViews(null);
				
					for (NodeView nv : nvs)
						if (nv.getOutermostNodeSer().getDomainId().equals(domainId))
							nodeViews.add(nv);
				}
			}
		}
		
		// Identify the docked Views as well.
		
		for (int index = 0; index < this.getTabCount(); index++)
		{
			Component c = this.getComponentAt(index);
			if (c instanceof NodeViewContainer)
			{
				NodeViewContainer nvContainer = (NodeViewContainer)c;
				Set<NodeView> nvs = nvContainer.getNodeViews(null);
				
				for (NodeView nv : nvs)
					if (nv.getOutermostNodeSer().getDomainId().equals(domainId))
						nodeViews.add(nv);
			}
		}
		
		return nodeViews;
	}
	
	
	public Set<NodeView> getNodeViewsForNode(String nodeId)
	{
		// Identify the popup Views that match the Node Id.
		
		Set<NodeView> nodeViews = new HashSet<NodeView>();
		
		for (Window w : disembodiedWindows)
		{
			Component[] components = w.getComponents();
			for (Component c : components)
			{
				if (c instanceof NodeViewContainer)
				{
					NodeViewContainer nvContainer = (NodeViewContainer)c;
					nodeViews.addAll(nvContainer.getNodeViews(nodeId));
				}
			}
		}
		
		// Identify the docked Views as well.
		
		for (int index = 0; index < this.getTabCount(); index++)
		{
			Component c = this.getComponentAt(index);
			if (c instanceof NodeViewContainer)
			{
				NodeViewContainer nvContainer = (NodeViewContainer)c;
				nodeViews.addAll(nvContainer.getNodeViews(nodeId));
			}
		}
		
		return nodeViews;
	}
	
	
	public Single_NodeView_Container getNodeViewContainer(String nodeId)
	{
		for (Window w : disembodiedWindows)
		{
			Component[] components = w.getComponents();
			for (Component c : components)
			{
				if (c instanceof Single_NodeView_Container)
				{
					Single_NodeView_Container snvc = (Single_NodeView_Container)c;
					NodeView nv = snvc.getNodeView();
					if ((nv != null) && nv.getOutermostNodeId().equals(nodeId)) return snvc;
				}
			}
		}
		
		for (int index = 0; index < this.getTabCount(); index++)
		{
			Component c = this.getComponentAt(index);
			if (c instanceof Single_NodeView_Container)
			{
				Single_NodeView_Container snvc = (Single_NodeView_Container)c;
				NodeView nv = snvc.getNodeView();
				if ((nv != null) && nv.getOutermostNodeId().equals(nodeId)) return snvc;
			}
		}

		return null;		
	}
	
	
	public Set<Multi_NodeView_Container> getNodeViewContainers(String nodeId)
	{
		Set<Multi_NodeView_Container> nodeViewContainers = new HashSet<Multi_NodeView_Container>();
		
		for (Window w : disembodiedWindows)
		{
			Component[] components = w.getComponents();
			for (Component c : components)
			{
				if (c instanceof Multi_NodeView_Container)
				{
					nodeViewContainers.add((Multi_NodeView_Container)c);
				}
			}
		}
		
		for (int index = 0; index < this.getTabCount(); index++)
		{
			Component c = this.getComponentAt(index);
			if (c instanceof Multi_NodeView_Container)
			{
				nodeViewContainers.add((Multi_NodeView_Container)c);
			}
		}

		return nodeViewContainers;		
	}


	public int idOfPanel(Container panel)
	{
		int index = 1000;
		for (Window w : disembodiedWindows)
		{
			Component[] components = w.getComponents();
			for (Component c : components)
			{
				if (c == panel) return index;
			}

			index++;
		}
		
		return super.indexOfComponent(panel);
	}
	
	
	public void setSelectedComponent(Component c) { super.setSelectedComponent(c); }
	
	
	public void recomputeLayout()
	{
		this.validate();
	}
}



