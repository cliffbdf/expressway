/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.Serializable;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.graph.*;
import expressway.generalpurpose.*;
import expressway.graph.*;


public class ScenarioSetGraphDefPanel extends JFrame
{
	private final static String NullEntry = "(none)";
	private final static Font TitleFont = new Font("Arial", Font.BOLD, 16);
	private final static FontMetrics TitleFontMetrics = 
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(TitleFont);

	//private static final Font NormalFont = new Font("Arial", Font.BOLD, 16);
	//private static final FontMetrics NormalFontMetrics = 
	//	java.awt.Toolkit.getDefaultToolkit().getFontMetrics(NormalFont);


	private PanelManager panelManager;
	private ModelScenarioSetVisual visual;
	private final ModelEngineRMI modelEngine;
	
	
	public ScenarioSetGraphDefPanel(final PanelManager panelManager, 
		final ModelScenarioSetVisual visual)
	{
		super("Graph Scenario Set");
		this.setAlwaysOnTop(true);
		this.panelManager = panelManager;
		this.visual = visual;
		this.modelEngine = visual.getClientView().getModelEngine();

		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		
		setSize(760, 270);
		setLocation(100, 50);
		
		final ChooserPanel panel1 = new ChooserPanel();
		final JPanel panel2 = new JPanel();
		final ChooserPanel panel3 = new ChooserPanel();
		final JPanel panel4 = new JPanel();
		
		add(panel1);
		add(panel2);
		add(panel3);
		add(panel4);
		
		panel2.add(new JLabel("versus"));
		
		JButton okButton = new JButton("OK");
		JButton cancelButton = new JButton("Cancel");
		
		panel4.add(okButton);
		panel4.add(cancelButton);
	
		okButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				double[] xValues = null;
				double[] yValues = null;
				try
				{
					Object elt1 = panel1.getChoice();
					Object elt2 = panel3.getChoice();
					
					if (elt1 == null) throw new RuntimeException("null elt1");
					if (elt2 == null) throw new RuntimeException("null elt2");
					
					if (elt1.equals(elt2)) throw new Exception(
						"May not choose the same element for both axes");

					xValues = this.getValues(elt1, panel1);
					yValues = this.getValues(elt2, panel3);

					double[][] values = new double[2][];
					values[0] = xValues;
					values[1] = yValues;
					
					String elt1Name = ((NodeSer)elt1).getFullName();
					String elt2Name = ((NodeSer)elt2).getFullName();
												
					
					// Format values into the structures required for the Graph methods.
					
					GraphInput.sortValues(values, 0);
					GraphInput graphInput = new GraphInput();
					graphInput.formatInput(xValues, yValues);
					
					
					// Determine the scale and dimensions of the Graph, based on the
					// range of values and the size of the display.
					
					int graphDisplayWidth = panelManager.getRecommendedPanelAreaWidth();
					int graphDisplayHeight = panelManager.getRecommendedPanelAreaHeight();
					GraphDimensions graphDimensions = new GraphDimensions();
					
					graphDimensions.getOptimalDimensions(graphInput,
						graphDisplayWidth, graphDisplayHeight);

					SmoothGraph curveGraph = new SmoothGraph();
					
					curveGraph.setXValues(graphInput.xValues);
					curveGraph.setYValues(graphInput.yValues);
					curveGraph.setXAxisLabel(elt1Name);
					curveGraph.setYAxisLabel(elt2Name);
					
					curveGraph.setPixelXSize(graphDimensions.pixelXSize);
					curveGraph.setPixelYSize(graphDimensions.pixelYSize);
					curveGraph.setXStartLogical(graphDimensions.xStartLogical);
					curveGraph.setYStartLogical(graphDimensions.yStartLogical);
					curveGraph.setXEndLogical(graphDimensions.xEndLogical);
					curveGraph.setYEndLogical(graphDimensions.yEndLogical);
					curveGraph.setXDeltaLogical(graphDimensions.xDeltaLogical);
					curveGraph.setYDeltaLogical(graphDimensions.yDeltaLogical);
					
					GraphPanel graphPanel = new GraphPanel(panelManager, curveGraph);
					graphPanel.setName("Graph Scenario Set");
					graphPanel.setTitle(elt1Name + " versus " + elt2Name);
					
					panelManager.addPanel(graphPanel.getName(), graphPanel, false);
					
					graphPanel.computeGraphSize(curveGraph);  // to verify graph.
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(ScenarioSetGraphDefPanel.this, ex);
					if (xValues != null) for (double x : xValues) GlobalConsole.println(x + ", ");
					if (yValues != null) for (double y : yValues) GlobalConsole.println(y + ", ");
				}
			}


			private double[] getValues(Object choice,
				ChooserPanel chooserPanel)
			throws
				Exception
			{
				String[] scenarioIds =
					((ModelScenarioSetSer)(visual.getNodeSer())).scenarioIds;
					
				double[] values = new double[scenarioIds.length];
				
				if (choice == null) throw new RuntimeException("Choice is null");
				else if (choice instanceof StateSer)
				{
					StateSer state1Ser = (StateSer)choice;
					
					if (chooserPanel.isMeanSelected())
					{
						int i = 0;
						for (String scenId : scenarioIds)
							// each Scenario scenarioId of the Scenario Set,
						{
							// Compute the mean of the final value of the State.
							// These are the x values.
							
							String[] options = { "mean" };
							
							double[] stats = modelEngine.getResultStatisticsById(
								state1Ser.getNodeId(), scenId, options);
							
							double mean = stats[0];
							values[i++] = mean;
						}
					}
					else if (chooserPanel.isAssuranceSelected())  // Compute State's assurance
					{
						values = 
							modelEngine.computeAssurances(visual.getNodeId());
					}
					else throw new RuntimeException(
						"Neither Mean or Assurance is selected");
				}
				else if (choice instanceof AttributeSer)
				{
					AttributeSer attrSer = (AttributeSer)choice;
					
					int i = 0;
					for (String scenId : scenarioIds)
						// each Scenario scenarioId of the Scenario Set,
					{
						// Obtain the value of the variable Attribute.
						
						Serializable value = 
							modelEngine.getAttributeValueById(
								attrSer.getNodeId(), scenId);
								
						if (! (value instanceof Number)) throw new Exception(
							"Attribute value is not numeric");
						
						values[i++] = ((Number)value).doubleValue();
					}
				}
				else if (choice == NullEntry) throw new Exception(
					"Choose a State or Attribute for top Panel");
				else throw new RuntimeException(
					"Unexpected Choice kind");
				
				return values;
			}
		});
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				ScenarioSetGraphDefPanel.this.dispose();
			}
		});
		
		this.show();
	}
	
	
	final static Dimension chooserMinSize = new Dimension(350, 80);
	
	
	class ChooserPanel extends JPanel
	{
		JRadioButton meanButton;
		JRadioButton assuranceButton;
		JCheckBox attrCheckbox;
		JComboBox stateComboBox;
		NodeSer attrSer;
		
		
		ChooserPanel()
		{
			JPanel leftPanel = new JPanel();
			JPanel rightPanel = new JPanel();
			
			leftPanel.setLayout(null);
			rightPanel.setLayout(null);
			
			leftPanel.setMinimumSize(chooserMinSize);
			rightPanel.setMinimumSize(chooserMinSize);
			
			leftPanel.setPreferredSize(chooserMinSize);
			rightPanel.setPreferredSize(chooserMinSize);
			
			leftPanel.setSize(chooserMinSize);
			rightPanel.setSize(chooserMinSize);
			
			
			// Define contents of left Panel.
			
			JLabel leftTitleLabel = new JLabel("Choose State");
			leftTitleLabel.setFont(TitleFont);
			leftTitleLabel.setSize(220, 15);
			leftPanel.add(leftTitleLabel);
			leftTitleLabel.setLocation(10, 10);
			
			String domainId = null;
			try { domainId = ((ModelElementSer)(visual.getNodeSer())).domainId; }
			catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }
			
			StateSer[] stateSers = null;
			try { stateSers = modelEngine.getStatesRecursive(domainId); }
			catch (Exception ex) {
				ErrorDialog.showReportableDialog(ScenarioSetGraphDefPanel.this, ex);
			};
			
			stateComboBox = new JComboBox(stateSers);
			stateComboBox.insertItemAt(NullEntry, 0);
			stateComboBox.setSelectedIndex(0);
			stateComboBox.setSize(330, 20);
			leftPanel.add(stateComboBox);
			stateComboBox.setLocation(10, 30);
			
			meanButton = new JRadioButton("Mean");
			assuranceButton = new JRadioButton("Assurance");
			
			final ButtonGroup group = new ButtonGroup();
			group.add(assuranceButton);
			group.add(meanButton);
			
			meanButton.setSize(150, 15);
			meanButton.setLocation(10, 60);
			
			assuranceButton.setSize(150, 15);
			assuranceButton.setLocation(100, 60);
			
			leftPanel.add(assuranceButton);
			leftPanel.add(meanButton);

			meanButton.setSelected(true);
			
			
			// Define contents of right Panel.
			
			JLabel rightTitleLabel = new JLabel("Choose Attribute");
			rightTitleLabel.setFont(TitleFont);
			rightTitleLabel.setSize(150, 15);
			rightTitleLabel.setLocation(10, 10);
			rightPanel.add(rightTitleLabel);
			
			String attrId = null;
			try { attrId = ((ModelScenarioSetSer)(visual.getNodeSer())).attributeId; }
			catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }
			
			this.attrSer = null;
			try { this.attrSer = modelEngine.getNode(attrId); }
			catch (Exception ex) {
				ErrorDialog.showReportableDialog(ScenarioSetGraphDefPanel.this, ex);
			};
			
			if (this.attrSer == null) throw new RuntimeException("attrSer is null");
			
			attrCheckbox = new JCheckBox(attrSer.getFullName());
			attrCheckbox.setSize(330, 15);
			rightPanel.add(attrCheckbox);
			attrCheckbox.setLocation(10, 30);
			
			
			// Get Sers of all Attributes in the Domain. List NetValue or Cost.Total
			// tags first.
			
			//AttributeSer[] attrSers = modelEngine.getAttributesDeep(domainId);
			
			//JComboBox attrComboBox = new JComboBox(attrSers);
			//attrComboBox.insertItemAt(NullEntry, 0);
			//attrComboBox.setWidth(100);
			//attrComboBox.setLocation(10, 50);
			//leftPanel.add(attrComboBox);
			
			add(leftPanel);
			add(new JLabel("OR"));
			add(rightPanel);
		}
		
		
		Object getChoice()
		throws
			Exception
		{
			Object leftSelItem = stateComboBox.getSelectedItem();
			
			if (leftSelItem == null) throw new RuntimeException("left sel item is null");
			
			
			Object rightSelItem = NullEntry;
			
			if (attrSer == null) throw new RuntimeException("attrSer is null");
			
			if (attrIsChecked()) rightSelItem = attrSer;
			
			if ((leftSelItem == NullEntry) && (rightSelItem == NullEntry))
				throw new Exception("Both selections are '" + NullEntry + "'");
			
			if ((leftSelItem != NullEntry) && (rightSelItem != NullEntry))
				throw new Exception("May only select a State or an Attribute for an axis");
			
			if (leftSelItem == NullEntry) return rightSelItem;
			else return leftSelItem;
		}
		
		
		boolean isMeanSelected() { return meanButton.isSelected(); }
		
		boolean isAssuranceSelected() { return assuranceButton.isSelected(); }
		
		boolean attrIsChecked() { return attrCheckbox.isSelected(); }
	}
}

