/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.gui.ValueControl.*;


public class AttributeVisibilityControl extends VisibilityControl
{
	public AttributeVisibilityControl(VisibilityControlFactory factory,
		ViewPanelBase viewPanel, ControlPanel controlPanel,
		VisualComponent visual, ValueControlFactory valueControlFactory,
		boolean showInitially)
	{
		super(factory, viewPanel, controlPanel, visual, valueControlFactory, showInitially);
		setToolTipText("Click to show or hide the Attribute values for this object");
	}
}

