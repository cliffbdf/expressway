/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.gui.InfoStrip.*;


public interface NodeScenarioPanel extends ControlPanel, ScenarioVisual
{
	NodeView getNodeView();


	void setScenarioInfoStrip(ScenarioInfoStrip infoStrip);
	
	
	ScenarioInfoStrip getScenarioInfoStrip();
}

