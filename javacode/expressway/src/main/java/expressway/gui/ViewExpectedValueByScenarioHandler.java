/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import java.awt.Component;
import expressway.awt.AWTTools;
import java.util.Vector;
import java.util.List;
import expressway.graph.*;
import expressway.statistics.HistogramDomainParameters;
import expressway.generalpurpose.ThrowableUtil;
import expressway.ser.*;
import expressway.common.VisualComponent.*;
import expressway.common.*;
import expressway.gui.graph.GraphPanel;


/**
 * Create a Graph that depicts a curve showing how the expected varies
 * by Scenario cost (investment).
 */

public class ViewExpectedValueByScenarioHandler implements ActionListener
{
	private View view;
	private PanelManager container;
	private String modelDomainNodeId;
	private String stateNodeId;  // may be null
	
	
	public ViewExpectedValueByScenarioHandler(View view, PanelManager container,
		String modelDomainNodeId, String stateNodeId /* may be null*/)
	{
		this.view = view;
		this.container = container;
		this.modelDomainNodeId = modelDomainNodeId;
		this.stateNodeId = stateNodeId;  // may be null.
	}
	
	
	public void actionPerformed(ActionEvent e)
	{
		ScenarioSer[] scenarioSers = null;
		try { scenarioSers = view.getModelEngine().getScenariosForDomainId(
			modelDomainNodeId); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(null, ex); return;
		}

		
		// Identify the selected States.
		
		List<String> valueStateIds = new Vector<String>();
		if (stateNodeId != null)
		{
			valueStateIds.add(stateNodeId);
		}
		else if (modelDomainNodeId != null)
		{
			// Identify the States with the Attribute "NetValue", within the
			// Model Container.
			
			for (ScenarioSer scenarioSer : scenarioSers)
			{
				NodeSer[] nodeSers = null;
				try { nodeSers = view.getModelEngine().getNodesWithAttributeDeep(
					modelDomainNodeId, Types.State.Financial.NetValue.class.getName(),
					scenarioSer.getNodeId()); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(null, ex); return;
				}
								
				for (NodeSer nodeSer : nodeSers)
				{
					if (nodeSer instanceof StateSer)
					{
						StateSer stateSer = (StateSer)nodeSer;
						
						boolean found = false;
						for (String valueStateId : valueStateIds)
						{
							if (valueStateId.equals(stateSer.getNodeId()))
							{
								found = true;
								break;
							}
						}
						
						if (! found) valueStateIds.add(nodeSer.getNodeId());
					}
				}
			}
		}
		else
		{
			(new Exception(
				"ViewExpectedValueOverScenarioHandler can only be called on a State or Model Domain"))
				.printStackTrace();
			return;
		}
		
		
		// Identify the State to associate with investment cost.
		
		String costStateId = null;
		StateSer costStateSer = null;
		for (ScenarioSer scenarioSer : scenarioSers)
		{
			NodeSer[] nodeSers = null;
			try { nodeSers = view.getModelEngine().getNodesWithAttributeDeep(
				modelDomainNodeId, Types.State.Financial.Cost.Total.class.getName(),
				scenarioSer.getNodeId()); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(null, ex); return;
			}
			
			if (nodeSers.length == 0)
			{
				JOptionPane.showMessageDialog(null,
					"No Cost.Total Attribute found", 
					"Error", JOptionPane.ERROR_MESSAGE); return;
			}
			
			if (nodeSers.length > 1)
			{
				JOptionPane.showMessageDialog(null,
					"More than one Cost.Total Attribute", 
					"Error", JOptionPane.ERROR_MESSAGE); return;
			}
			
			if (! (nodeSers[0] instanceof StateSer))
			{
				JOptionPane.showMessageDialog(null,
					"Attribute Cost.Total is on a " + nodeSers[0].getClass().getName()
					+ "; it can only be on a State.", 
					"Error", JOptionPane.ERROR_MESSAGE); return;
			}
				
			costStateSer = (StateSer)(nodeSers[0]);
			
			if (costStateId == null) costStateId = costStateSer.getNodeId();
			else if (! costStateId.equals(costStateSer.getNodeId()))
			{
				JOptionPane.showMessageDialog(null,
					"Two Scenarios have different States with a Cost.Total Attribute; "
					+ "cannot compare Scenarios.", 
					"Error", JOptionPane.ERROR_MESSAGE); return;
			}
		}
		

		// Create a Graph for each State, depciting its expected value as a
		// function of each Scenario, ordered by the Scenario's investment cost.
		
		final String[] options = { "mean", "geomean", "max", "min", "sd" };
		
		for (String valueStateId : valueStateIds)
		{
			StateSer stateSer = null;
			ModelDomainSer domainSer = null;
			String stateName;
			String stateFullName;
			try
			{
				stateSer = (StateSer)(view.getModelEngine().getNode(valueStateId));
				domainSer = (ModelDomainSer)(view.getModelEngine().getNode(stateSer.domainId));
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(null, ex); return;
			}
			
			stateName = stateSer.getName();
			stateFullName = stateSer.getFullName();
			
			
			// Create a field of Scenario points, where each point is the
			// Scenario's expected value versus its cost.
			
			int noOfScenarios = scenarioSers.length;
			double[] expectedCosts = new double[noOfScenarios];
			double[] expectedValues = new double[noOfScenarios];
			
			int scenarioNo = 0;
			for (ScenarioSer scenarioSer : scenarioSers)
			{
				String scenarioId = scenarioSer.getNodeId();
				
				
				// Get expected cost statistics.
						
				double[] costStatistics = null;
				try { costStatistics = view.getModelEngine().getResultStatisticsById(
					costStateId,
					scenarioId, options); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(null, ex); return;
				}
				
				double expectedCost = costStatistics[0];

				
				// Get value statistics.
			
				double[] valueStatistics = null;
				try { valueStatistics = view.getModelEngine().getResultStatisticsById(
					valueStateId,
					scenarioId, options); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(null, ex); return;
				}
			
				double expectedValue = valueStatistics[0];
				
				
				expectedCosts[scenarioNo] = expectedCost;
				expectedValues[scenarioNo] = expectedValue;
				
				scenarioNo++;
			}
			
			
			// Order the values by ascending expected cost.
			
			double[][] valueField = new double[2][];
			valueField[0] = expectedCosts;
			valueField[1] = expectedValues;
							
			GraphInput.sortValues(valueField, 0);
			if (expectedCosts.length != expectedValues.length)
				throw new RuntimeException(
					"expectedCosts.length != expectedValues.length");
			for (int i = 0; i < expectedCosts.length; i++)
			{
				double x = expectedCosts[i];
				double y = expectedValues[i];
			}
			
			
			// Format the data as needed by the Graph class.
			
			GraphInput graphInput = new GraphInput();
			graphInput.formatInput(expectedCosts, expectedValues);
			
			
			// Determine the scale and dimensions of the Graph, based on the
			// range of values and the size of the display.
			
			int graphDisplayWidth = container.getRecommendedPanelAreaWidth();
			int graphDisplayHeight = container.getRecommendedPanelAreaHeight();
			GraphDimensions graphDimensions = new GraphDimensions();
			
			try { graphDimensions.getOptimalDimensions(graphInput,
				graphDisplayWidth, graphDisplayHeight); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(null, ex); return;
			}
			
			// Create a curve Graph.
			
			Graph graph;
			if (graphInput.xValues.length > 2) graph = new SmoothGraph();
			else graph = new BarGraph();
			
			graph.setXValues(graphInput.xValues);
			graph.setYValues(graphInput.yValues);
			graph.setXAxisLabel("Total Cost");
			graph.setYAxisLabel("E(" + stateName + ")");

			graph.setPixelXSize(graphDimensions.pixelXSize);
			graph.setPixelYSize(graphDimensions.pixelYSize);
			graph.setXStartLogical(graphDimensions.xStartLogical);
			graph.setYStartLogical(graphDimensions.yStartLogical);
			graph.setXEndLogical(graphDimensions.xEndLogical);
			graph.setYEndLogical(graphDimensions.yEndLogical);
			graph.setXDeltaLogical(graphDimensions.xDeltaLogical);
			graph.setYDeltaLogical(graphDimensions.yDeltaLogical);

					
			// Create Graph Panel
			
			GraphPanel graphPanel = new GraphPanel(container, graph);
			graphPanel.setName(stateFullName + " by Scenario");
			graphPanel.setTitle("Expected Value By Scenario for State " +
				stateSer.getFullName() + " of Domain " + domainSer.getName() +
				", In Order of Increasing Cost");

			
			// Add the Graph Panel to the Main Panel's tabbed Pane.
			
			container.addPanel(graphPanel.getName(), graphPanel, false);
		}
	}
}

