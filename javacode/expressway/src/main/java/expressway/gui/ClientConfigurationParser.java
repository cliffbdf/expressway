/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.apache.xerces.parsers.DOMParser;


/**
 * See slide "Client Configuration File".
 */
 
public class ClientConfigurationParser implements ErrorHandler
{
	private Document document = null;
	private DOMParser parser = null;
	private ClientMainApplication mainApp;
	private PanelManager panelManager;
	private ViewFactory viewFactory;
	
	
	public ClientConfigurationParser(ClientMainApplication mainApp,
		PanelManager panelManager, ViewFactory viewFactory)
	{
		this.mainApp = mainApp;
		this.panelManager = panelManager;
		this.viewFactory = viewFactory;
		
		parser = new DOMParser();
		
		try
		{
			parser.setFeature( "http://xml.org/sax/features/validation", false);
		}
		catch (SAXException e)
		{
			System.err.println("error in setting up parser feature");
		}

		parser.setErrorHandler(this);
	}
	
	
	protected Document getDocument() { return document; }
	protected DOMParser getDOMParser() { return parser; }
	
	
	public static void readConfiguration(ClientMainApplication mainApp,
		PanelManager panelManager, ViewFactory viewFactory)
	throws
		Exception
	{
		ClientConfigurationParser configParser = new ClientConfigurationParser(
			mainApp, panelManager, viewFactory);
		
		configParser.parse();
		configParser.genDocument();
	}
	
	
	public synchronized void parse()
	throws
		SAXException,
		IOException
	{
		String home = System.getProperty("user.home");
		File configFile = new File(home, Constants.ConfigFileName);
		if (! configFile.exists()) return;
		
		GlobalConsole.println("Reading configuration file " + configFile.getAbsolutePath());
		FileReader fileReader = new FileReader(configFile);
		getDOMParser().parse(new InputSource(fileReader));
		this.document = getDOMParser().getDocument();
	}
	

	public synchronized void setFeature(String featureId, boolean state)
	throws 
		SAXNotRecognizedException,
		SAXNotSupportedException
	{
		parser.setFeature( featureId, state );
	}


	public synchronized void genDocument()
	throws Exception
	{
		if (document == null) return;  // there was probably no configuration file.
		
		Element rootElement = document.getDocumentElement();
			// Should be an <expressway_client_configuration> element.
		
		//if (rootElement == null) return;  // there was probably no configuration file.
		
		String tagName = rootElement.getTagName();
		if (tagName.equals("expressway_client_configuration")) genConfiguration(rootElement);
		else throw new Exception("Expected an <expressway_client_configuration> tag.");
	}
	
	
	protected void genConfiguration(Element element)
	throws Exception
	{
		String s1 = element.getAttribute("server");
		String serverStr = s1;
		if (serverStr.equals("")) serverStr = "localhost";
		
		mainApp.setModelEngineNetworkPath(serverStr);
		mainApp.connectToModelEngine();
		
		
		// Parse other elements.
		
		NodeList subElements = getElements(element, "main_window");
		if (subElements.getLength() > 1) throw new Exception(
			"Multiple main windows");
		
		for (int i = 0; i < subElements.getLength(); i++)
			genMainWindow((Element)(subElements.item(i)));
		
		subElements = getElements(element, "xml");
		for (int i = 0; i < subElements.getLength(); i++)
			genXml((Element)(subElements.item(i)));
		
		subElements = getElements(element, "jar");
		for (int i = 0; i < subElements.getLength(); i++)
			genJar((Element)(subElements.item(i)));
	}
	
	
	protected void genMainWindow(Element element)
	throws Exception
	{
		String s1 = element.getAttribute("width");
		String s2 = element.getAttribute("height");
		if ((! s1.equals("")) && (! s2.equals("")))
			panelManager.getFrame().setSize(Integer.parseInt(s1), Integer.parseInt(s2));
		
		s1 = element.getAttribute("x");
		s2 = element.getAttribute("y");
		if ((! s1.equals("")) && (! s2.equals("")))
			panelManager.getFrame().setLocation(Integer.parseInt(s1), Integer.parseInt(s2));
		
		
		// Views
		
		NodeList subElements = getElements(element, "model_scenario_view");
		for (int i = 0; i < subElements.getLength(); i++)
			genModelDomainView((Element)(subElements.item(i)));
	}
	
	
	protected void genXml(Element element)
	throws Exception
	{
		String s = element.getAttribute("dir");
		if (s.equals("")) return;
		File f = new File(s);
		if (! f.exists()) return;
		if (! f.isDirectory()) return;
		
		mainApp.addXMLDir(f);
	}
	
	
	protected void genJar(Element element)
	throws Exception
	{
		String s = element.getAttribute("dir");
		if (s.equals("")) return;
		File f = new File(s);
		if (! f.exists()) return;
		if (! f.isDirectory()) return;
		
		mainApp.addJARDir(f);
	}
	
	
	protected void genModelDomainView(Element element)
	throws Exception
	{
		// Open the specified Model Domain View, but without any Scenarios.
		
		String s = element.getAttribute("element");
		if (s.equals("")) return;
		ModelDomainSer domainSer;
		ModelEngineRMI modelEngine = mainApp.getModelEngine();
		if (modelEngine == null) return;
		
		try { domainSer = modelEngine.getModelDomain(s); }
		catch (ElementNotFound enf) { return; }
		
		ModelDomainVisual view = (ModelDomainVisual)
			(viewFactory.createModelDomainViewPanel(domainSer, false, true));
		
		// Scenarios.
		
		NodeList subElements = getElements(element, "scenario");
		for (int i = 0; i < subElements.getLength(); i++)
			genScenario((Element)(subElements.item(i)), view);
	}
	
	
	protected void genScenario(Element element, ModelDomainVisual view)
	throws Exception
	{
		String s = element.getAttribute("name");
		if (s.equals("")) return;
		
		ModelEngineRMI modelEngine = mainApp.getModelEngine();
		if (modelEngine == null) return;
		
		String modelDomainName = view.getName();
		ModelScenarioSer scenSer;
		try { scenSer = modelEngine.getModelScenario(modelDomainName, s); }
		catch (ParameterError pe) { return; }
		
		
		// Open the specified Model Scenario.
		
		view.showScenario(scenSer.getNodeId());
	}
	

	/**
	 * Returns a string of the location.
	 */
	protected String getLocationString(SAXParseException ex)
	{
		StringBuffer str = new StringBuffer();

		String systemId = ex.getSystemId();
		if (systemId != null)
		{
			int index = systemId.lastIndexOf('/');
			if (index != -1) systemId = systemId.substring(index + 1);
			str.append(systemId);
		}
		str.append(':');
		str.append(ex.getLineNumber());
		str.append(':');
		str.append(ex.getColumnNumber());

		return str.toString();
	}
	
	
	/** Warning. */
	public void warning(SAXParseException ex)
	{
		System.err.println(
			"[Warning] "+ getLocationString(ex)+": "+ ex.getMessage());
    }
    

	/** Error. */
	public void error(SAXParseException ex)
	{
		System.err.println(
			"[Error] "+ getLocationString(ex)+": "+ ex.getMessage());
	}


	/** Fatal error. */
	public void fatalError(SAXParseException ex)
	throws
		SAXException
	{
		System.err.println(
			"[Fatal Error] "+ getLocationString(ex)+": "+ ex.getMessage());
			throw ex;
	}

	
	protected static NodeList getElements(Element element, String tagName)
	{
		NodeList childNodes = element.getChildNodes();
		NodeListImpl nodes = new NodeListImpl();
		for (int i = 0; i < childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			if (node instanceof Element)
			{
				Element e = (Element)node;
				if (e.getTagName().equals(tagName)) nodes.add(node);
			}
		}
		
		return nodes;
	}
	
	
	static class NodeListImpl extends Vector<Node> implements NodeList
	{
		public int getLength()
		{
			return size();
		}
		
		
		public Node item(int index)
		{
			return get(index);
		}
	}}

