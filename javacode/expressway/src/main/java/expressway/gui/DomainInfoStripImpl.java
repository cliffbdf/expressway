/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.gui.InfoStrip.*;
import expressway.help.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.awt.Component;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;


/** ************************************************************************
 * A strip of information about a ModelDomain. Button functions:
	Save [Domain].
	Close [Domain].
	New Scenario. (inherited from InfoStrip).
 */
 
public class DomainInfoStripImpl extends InfoStripImpl implements DomainInfoStrip
{
	/** Button for saving the state of the Scenario represented by the Attribute Value Panel. */
	private JButton saveButton;
	
	/** Button for closing the Attribute Value Panel. */
	private JButton closeButton;
	
	/** Button for creating a new Model Scenario. */
	private JButton newScenarioButton;
	
	/** For user help on the toolbar. */
	private HelpButton helpButton;
	
	private NodeViewPanelBase view;


	public DomainInfoStripImpl(NodeViewPanelBase viewPanel//, ControlPanel controlPanel
		)
	{
		super(viewPanel//, controlPanel
			);
		if (! (viewPanel instanceof NodeViewPanelBase)) throw new RuntimeException(
			"A DomainInfoStrip must be for a NodeViewPanelBase");
		
		this.view = viewPanel;
		
		String newName = view.getName();
		this.getNameField().setText(newName);
		
		/*long lastUpdated = view.getNodeSer().getLastUpdated();
		this.getModifiedLabel().setText("Version " + lastUpdated);*/


		this.saveButton = new JButton(ButtonIcons.SaveButtonIcon);
		saveButton.setToolTipText("Send the name change to the database");
		this.add(this.saveButton);
		
		this.closeButton = new JButton(ButtonIcons.CloseButtonIcon);
		closeButton.setToolTipText(
			((view instanceof MotifView) ?
				"Close this Motif View" : "Close this Domain View"
				));
		this.add(this.closeButton);
		
		Class thisClass = this.getClass();
		URL saveImgURL = thisClass.getResource(ButtonIcons.SaveButtonIconImagePath);
		URL closeImgURL = thisClass.getResource(ButtonIcons.CloseButtonIconImagePath);
		URL newScenarioImgURL = thisClass.getResource(ButtonIcons.NewModelScenarioButtonIconImagePath);

		this.saveButton.addActionListener(new SaveDomainActionListener());
		this.closeButton.addActionListener(new CloseDomainActionListener());

		this.helpButton = new HelpButton("Toolbar Help", HelpButton.Size.Large,
			"<p><img src=\"" + saveImgURL.toString() + "\" /> &nbsp;<b></b> " +
				"The " + 
				
				((view instanceof MotifView) ?
					HelpWindow.createHref("Motifs", "Motif")
					:
					HelpWindow.createHref("Model Domains", "Model Domain")
				) +
				
				" name is sent to the server database." +
				"</p>" +
			"<p><img src=\"" + closeImgURL.toString() + "\" /> &nbsp;<b></b> " +
				"The View of the " + 
				
				((view instanceof MotifView) ?
					HelpWindow.createHref("Motifs", "Motif")
					:
					HelpWindow.createHref("Model Domains", "Model Domain")
				) +
				
				" is closed. (This does not affect it " +
				"in the database.)" +
				"</p>" +
				
			((view instanceof MotifView) ? "" :
			(
			"<p><img src=\"" + newScenarioImgURL.toString() + "\" /> &nbsp;<b></b> " +
				"A new " + HelpWindow.createHref("Model Scenarios", "Model Scenario") +
				" is created for the " + HelpWindow.createHref("Model Domains", "Model Domain") + "." +
				"</p>")
			)
			);
		
		helpButton.setToolTipText("Show explanation of each toolbar button");

		if (! (view instanceof MotifView))
		{
			newScenarioImgURL = thisClass.getResource(ButtonIcons.NewModelScenarioButtonIconImagePath);
			this.newScenarioButton = new JButton(ButtonIcons.NewModelScenarioButtonIcon);
			newScenarioButton.setToolTipText("Create a new Scenario");
			this.add(this.newScenarioButton);
			this.newScenarioButton.addActionListener(new NewScenarioActionListener());
		}
		
		this.add(helpButton);
	}
	
	
	public void update()
	{
		//super.update();
		
		String newName = view.getName();
		this.getNameField().setText(newName);
		
		/*long lastUpdated = view.getNodeSer().getLastUpdated();
		this.getModifiedLabel().setText("Version " + lastUpdated);*/
	}
	
	
	protected ModelEngineRMI getModelEngine() { return view.getModelEngine(); }
	
	
	protected NodeSer getNodeSer() { return view.getOutermostNodeSer(); }
	
	
	protected String getNodeId() { return view.getOutermostNodeId(); }
	
	
	protected ListenerRegistrar getListenerRegistrar() { return view.getListenerRegistrar(); }


	/** ********************************************************************
	 * Handle "New Scenario" button selection actions.
	 */
	 
	class NewScenarioActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			NodeScenarioView scenarioViewPanel = (NodeScenarioView)view;
			
			// Send request to server to create a new Model Scenario, with
			// a default name. A new Scenario Panel is not created until
			// the server issues a callback to update the Model Domain.
			
			try
			{
				scenarioViewPanel.setShowScenarios(true);
			
				getModelEngine().createScenario(getNodeSer().getDomainName(), null);
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(view, ex);
			}
		}
	}


	class SaveDomainActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			// Update the server with the new name for this Domain.
			
			String nodeId = getNodeId();
			String newName = getNameField().getText();
			//System.out.println("Setting name to " + newName);
			try
			{
				try { getModelEngine().setNodeName(false, nodeId, newName); }
				catch (Warning w)
				{
					if (JOptionPane.showConfirmDialog(null, w.getMessage(),
						"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					{
						getModelEngine().setNodeName(true, nodeId, newName);
					}
				}
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(view, ex); return;
			}
			//System.out.println("Name set completed");
			try
			{
				NodeSer newNodeSer = getModelEngine().getNode(nodeId);
				//System.out.println("Name was set to " + newNodeSer.getName());
				NodeSer newNodeSer2 = getModelEngine().getModelDomain(newNodeSer.getName());
				//System.out.println("Domain name found");
			}
			catch (Exception ex) { ex.printStackTrace(); }
				
		}
	}
	
	
	class CloseDomainActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			// Unsubscribe this View's Peer Listener.
			
			try { getListenerRegistrar().unsubscribeAll(); }
			catch (IOException ex) { ex.printStackTrace(); }
			
			
			// Unregister this View's Peer Listener.
			
			if (getModelEngine() instanceof ModelEngineRMI) try
			{
				((ModelEngineRMI)(getModelEngine())).unregisterPeerListener(
					view.getPeerListener());
				view.unregisterPeerListener();
			}
			catch (ParameterError pe)
			{
				GlobalConsole.println("Internal error");
				GlobalConsole.printStackTrace(pe);
			}
			catch (IOException ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			
			
			// Remove this View from the PanelManager container.
			
			PanelManager panelManager = view.getPanelManager();
			panelManager.removePanel(view);
		}
	}
}

