/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.gui.InfoStrip.*;
import java.awt.Component;


/** ************************************************************************
 * A control panel (e.g., tabbed panel) for allowing the user to select from among
 * the set of available Scenarios for the Domain. The controls (e.g., tabs)
 * representing Scenarios should be adjacent. The controls for Show/Hide and New
 * are expected to be the zeroth and first controls, respectively.
 * Each Scenario control owns an instance of ScenarioInfoStrip.
 * Note: Implemented by ScenarioSelectorImpl.
 */
 
public interface ScenarioSelector
	extends SwingComponent  // thus, implementation must extends JComponent
{
	void setControlPanelTitle(ControlPanel controlPanel, String newName);
	
	ControlPanel getControlPanel(String name);
	
	Component getComponent();
	
	/** Create and add a DomainInfoStrip to this ScenarioSelector. */
	DomainInfoStrip createDomainInfoStrip();
	
	/** Create and add a ScenarioInfoStrip to this ScenarioSelector. */
	ScenarioInfoStrip createScenarioInfoStrip(NodeScenarioPanel panel);
	
	/** Remove the specified ScenarioInfoStrip from this ScenarioSelector. */
	void removeScenarioInfoStrip(ScenarioInfoStrip sis);
}

