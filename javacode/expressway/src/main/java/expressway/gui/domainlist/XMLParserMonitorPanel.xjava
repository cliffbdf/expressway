/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.domainlist;

import expressway.common.ModelEngineRMI;
import java.util.List;
import java.util.Vector;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;


/**
 * 
 */
 
public class XMLParserMonitorPanel extends JFrame
implements
	SimulationVisualMonitor  // so SimulationVisuals can notify this when they close.
{
	private ModelEngineRMI modelEngine;
	private Integer callbackId;
	
	private JButton abortButton;
	
	private JTextArea msgArea;
	private JScrollPane msgScrollPane;
	private JLabel msgAreaLabel;
	
	private SimulationsPanel simulationsPanel;
	
	private List<String> callbackIds = new Vector<String>();
	
	
	public XMLParserMonitorPanel(ModelEngineRMI modelEngine,
		final Integer callbackId)
	{
		super("Processing XML");
		
		this.modelEngine = modelEngine;
		this.callbackId = callbackId;
		
		setLayout(null);
		
		
		// Add Components.
		
		setBackground(Color.white);
		setSize(400, 450);
		setLocation(100, 50);
		
		add(abortButton = new JButton("Abort"));

		add(msgAreaLabel = new JLabel("Messages from XML processing:"));
		add(msgScrollPane = new JScrollPane(msgArea = new JTextArea(5, 50)));
		msgArea.setEditable(false);
		
		add(simulationsPanel = new SimulationsPanel());
		
		
		// Set Component sizes.

		Font simulationsFont = msgAreaLabel.getFont().deriveFont((float)16.0);
		simulationsLabel.setFont(simulationsFont);
		
		abortButton.setSize(100, 20);
		
		msgAreaLabel.setSize(150, 15);
		msgScrollPane.setSize(350, 100);
		
		simulationsPanel.setSize(150, 70);
		
		
		// Set Component positions.
		
		abortButton.setLocation(20, 60);
		
		msgAreaLabel.setLocation(20, 100);
		msgScrollPane.setLocation(20, 120);
		
		simulationsPanel.setLocation(100, 200);

		
		// Add handlers.
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowListener()
		{
			public void windowActivated(WindowEvent e) {}
			public void windowClosed(WindowEvent e) {}
			
			public void windowClosing(WindowEvent e)
			{
				// Ask user if processing should be aborted
				
			}
			
			public void windowDeactivated(WindowEvent e) {}
			public void windowDeiconified(WindowEvent e) {}
			public void windowIconified(WindowEvent e) {}
			public void windowOpened(WindowEvent e) {}
		});
		
		
		abortButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				for (String callbackId : callbackIds)
				{
					try
					{
						if (callbackIds.contains(callbackId))
							modelEngine.abort(callbackId);
						
						callbackIds.remove(callbackId);
					}
					catch (Exception ex)
					{
						System.out.println("Unable to abort " + callbackId);
					}
				}
			}
		});
		
		
		listSelectionModel = simulationsTable.getSelectionModel();
		
		listSelectionModel.addListSelectionListener(new ListSelectionListener()
		{
			public void valueChanged(ListSelectionEvent e)
			{
				// Create a SimulationControlPanel for the selected simulation task
				// (but only if a Panel does not already exist).
				
				if (e.getValueIsAdjusting()) return;
				
				ListSelectionModel lsm = (ListSelectionModel)(e.getSource());
				if (lsm.isSelectionEmpty()) return;

				int minIndex = lsm.getMinSelectionIndex();
				int maxIndex = lsm.getMaxSelectionIndex();
				
				for (int i = minIndex; i <= maxIndex; i++)
					if (lsm.isSelectedIndex(i))
					{
						String selectedCallbackId = ....
						if (! (panelExistsForCallbackId(selectedCallbackId)))
						{
							// The MonitorPanel must be the parent of the
							// Sim Control Panel, and the Sim Control Panel 
							// must notify its parent when it is closed.

							SimulationControlPanel panel = 
								new SimulationControlPanel(XMLParserMonitorPanel.this,
									....);
								
							callbackIds.add(selectedCallbackId);
							panel.show();
						}
					}
			}
		});
	}
	
	
	
	/*
	 * From SimulationVisualMonitor.
	 */
	
	
	public void simulationVisualClosing(SimulationVisual simVis)
	{
		callbackIds.remove(simVis.getCallbackId());
	}
	
	
	/* Protected methods. */
	
	
	protected boolean panelExistsForCallbackId(String callbackId)
	{
		return callbackIds.contains(callbackId);
	}
	
	
	class SimulationsPanel extends ViewPanelBase
	{
		private JLabel simulationsLabel;
		private JLabel taskNameLabel;
		private JLabel noOfRunsLabel;
		private JLabel statusLabel;
		private JTable simulationsTable;
		private JScrollPane simulationsScrollPane;
		
		
		SimulationsPanel()
		{
			setLayout(null);
			
			add(simulationsLabel = new JLabel("Simulations"));
			add(taskNameLabel = new JLabel("Task Name"));
			add(noOfRunsLabel = new JLabel("No. of Runs"));
			add(statusLabel = new JLabel("Status"));
			add(simulationsScrollPane = new JScrollPane(
				simulationsTable = new JTable()));
	
			simulationsLabel.setSize(150, 25);
			taskNameLabel.setSize(150, 15);
			noOfRunsLabel.setSize(150, 15);
			statusLabel.setSize(150, 15);
			simulationsScrollPane.setSize(150, 80);
			simulationsLabel.setLocation(50, 0);
			taskNameLabel.setLocation(0, 20);
			noOfRunsLabel.setLocation(50, 20);
			statusLabel.setLocation(100, 20);
			simulationsScrollPane.setLocation(0, 40);		
		}
	}
}

