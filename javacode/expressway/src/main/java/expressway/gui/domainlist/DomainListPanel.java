/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.domainlist;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.gui.PanelManager.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.Constants;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.*;
import expressway.gui.activityrisk.*;
import expressway.gui.modeldomain.*;
import expressway.gui.FileChooserUtils.*;
import expressway.gui.PanelManager.*;
import expressway.help.*;
import expressway.help.HelpfulText.*;
import expressway.generalpurpose.*;
import expressway.swing.*;
import expressway.swing.MultiHierarchyPanel.*;
import expressway.awt.AWTTools;
import java.awt.event.*;
import java.awt.Color;
import java.awt.Insets;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.LayoutManager;
import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.lang.reflect.Constructor;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.Serializable;
import java.net.URL;



/**
 * A Panel depicting a list of Domains that are available from a Model Engine
 * that is specified by the user via a JTextField.
 */
 
public class DomainListPanel extends JPanel
	implements ViewFactory, DomainSpaceVisual
{
	private PanelManager panelManager;
	private ClientMainApplication mainApplication;
	private DomainSelector domainSelector;
	private JButton bugButton = null;
	private JTextField modelEngineNetworkPathField = null;
	private JButton reconnectButton = null;
	private JButton createDomainButton = null;
	private JButton createMotifButton = null;
	private JButton importDomainButton = null;
	private JButton saveAllButton = null;
	private HelpButton helpButton = null;
	private JPanel topPanel = null;
	private JPanel centerPanel = null;
	private JPanel leftPanel;
	private JScrollPane domainTreeScrollPane = null;
	private JPanel infoPanel = null;
	private StatusPanel statusPanel = null;
	private JPanel tutorPanel = null;
	private HelpfulButton domainTypeManagerButton = null;
	
	private ActionListener copyModelDomainHandler;
	private ActionListener defineNewScenarioHandler;
	private ActionListener viewExpectedValueByScenarioHandler;
	private ActionListener copyModelScenarioHandler;
	private ActionListener viewDistributionHandler;
	private ActionListener viewExpectedValueOverTimeHandler;
	private ActionListener viewValueOverTimeHandler;
	
		
	public DomainListPanel(final ClientMainApplication mainApp, final PanelManager panelManager)
	{
		super();
		setName("Domains");
		this.mainApplication = mainApp;
		this.panelManager = panelManager;
		
		//this.peerListener = new PeerListenerImpl("Domain List");
		
		/* Set initial position and size:
		
			Instantiate sub-components:
			1. The text input field specifying network path to the ModelEngine
					server.
			2. A "Connect to server" button.
			3. the tree of Domains and their Scenarios.
		 */
		
		setLayout(new BorderLayout());
		add(topPanel = new JPanel(), BorderLayout.NORTH);
		add(centerPanel = new JPanel(), BorderLayout.CENTER);
		setPreferredSize(new java.awt.Dimension(1100, 600));
		
		JPanel infoPanel = new JPanel();
		
		centerPanel.setLayout(new BorderLayout());
		
		leftPanel = new JPanel()
		{
			public void setBounds(int x, int y, int width, int height)
			{
				super.setBounds(x, y, width, height);
				
				if (domainSelector != null)
				{
					domainSelector.setSize(this.getSize());
				}
			}
		};
		
		leftPanel.setLayout(new BorderLayout());
		leftPanel.setBackground(java.awt.Color.white);
		recreateDomainSelector();
		
		JPanel leftTopPanel = new JPanel();
		leftPanel.add(leftTopPanel, BorderLayout.NORTH);
		leftTopPanel.add(new JLabel("Domains")
		{
			public Dimension getMinimumSize() { return new Dimension(200, 15); }
			public Dimension getPreferredSize() { return new Dimension(200, 15); }
		});
		
		this.domainTypeManagerButton = new HelpfulButton("Domain Types...",
			"Manage the types of Domains and Motifs that can be instantiated."
			);
		domainTypeManagerButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				JPopupMenu popup = new JPopupMenu();
				try { DomainMenuItemHelper.addDomainMenuItems(domainSelector,
					domainTypeManagerButton, popup, domainTypeManagerButton.getWidth(), 0); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(DomainListPanel.this, ex);
					return;
				}
				popup.show(domainTypeManagerButton, domainTypeManagerButton.getWidth(), 0);
			}
		});
		enableDomainTypeButton(false);
		leftTopPanel.add(domainTypeManagerButton);
		
		recreateDomainSelector();

		centerPanel.add(leftPanel, BorderLayout.WEST);
		centerPanel.add(infoPanel, BorderLayout.CENTER);
		
		infoPanel.setLayout(new BorderLayout());
		infoPanel.add(statusPanel = new StatusPanel(), BorderLayout.CENTER);
		infoPanel.add(tutorPanel = new JPanel(), BorderLayout.EAST);
		
		tutorPanel.setLayout(new GridLayout(4, 1));
		
		statusPanel.setBackground(java.awt.Color.white);
		centerPanel.setBackground(java.awt.Color.white);
		tutorPanel.setBackground(java.awt.Color.white);		
		infoPanel.setBackground(java.awt.Color.white);
		topPanel.setBackground(java.awt.Color.white);
		
		JButton quickStartButton = new TutorButton("Quick-Start Tutorial", ReleaseInfo.QuickStartTutorialURL);
		JButton helpDirButton = new TutorButton("Online Help Directory", ReleaseInfo.OnlineHelpDirectoryURL);
		JButton userManButton = new TutorButton("Expressway User Manual", ReleaseInfo.ExpresswayUserManualURL);
		JButton valueDrivenITButton = new TutorButton("Value Driven IT Home", ReleaseInfo.ValueDrivenITURL);
		
		tutorPanel.add(quickStartButton);
		
		//tutorPanel.add(new TutorButton("Quick Reference",
		//	ReleaseInfo.QuickReferenceURL));
		
		tutorPanel.add(helpDirButton);
		
		tutorPanel.add(userManButton);
		
		tutorPanel.add(valueDrivenITButton);

		if (ClientGlobalValues.IncludeBugReportFeatures)
		{
			topPanel.add(bugButton = new JButton("Report Bug"));
			bugButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e) { (new BugReportWindow()).show(); }
			});
		}
		
		topPanel.add(new JLabel("Model Engine Server DNS Path: "));
		topPanel.add(modelEngineNetworkPathField = new JTextField());
		modelEngineNetworkPathField.setText("localhost");
		modelEngineNetworkPathField.setColumns(20);
		
		modelEngineNetworkPathField.requestFocus();
		
		topPanel.add(reconnectButton = new JButton(ButtonIcons.ConnectButtonIcon));
		reconnectButton.setToolTipText("Connect to Model Engine on server");
		
		topPanel.add(createDomainButton = new JButton(ButtonIcons.NewModelDomainButtonIcon));
		createDomainButton.setToolTipText("Create a new Domain");
		
		topPanel.add(createMotifButton = new JButton(ButtonIcons.NewMotifButtonIcon));
		createMotifButton.setToolTipText("Create a new motif (a library)");
		
		topPanel.add(importDomainButton = new JButton(ButtonIcons.ImportXMLButtonIcon));
		importDomainButton.setToolTipText("Import an Expressway XML file");
		
		topPanel.add(saveAllButton = new JButton(ButtonIcons.SyncButtonIcon));
		saveAllButton.setToolTipText("Sync server's in-memory DB to Disk");
		
		Class thisClass = this.getClass();
		URL connectImgURL = thisClass.getResource(ButtonIcons.ConnectButtonIconImagePath);
		URL newModelDomainImgURL = thisClass.getResource(ButtonIcons.NewModelDomainButtonIconImagePath);
		URL importXMLImgURL = thisClass.getResource(ButtonIcons.ImportXMLButtonIconImagePath);
		URL syncImgURL = thisClass.getResource(ButtonIcons.SyncButtonIconImagePath);
		
		topPanel.add(helpButton = new HelpButton("Toolbar Help", HelpButton.Size.Large,
			"<p><img src=\"" + connectImgURL.toString() + "\" /> &nbsp;<b>Connect to server</b> " +
				"Connect to the Expressway server specified in the 'Model Engine Server DNS Path' field. " +
				"The server may be on the same machine as you are, or it may be remote." +
				"</p>" +
			"<p><img src=\"" + newModelDomainImgURL.toString() + "\" /> &nbsp;<b>New Model Domain</b> " +
				"Create a new (empty) " +
				HelpWindow.createHref("Model Domains", "Model Domain") + "." +
				"</p>" +
			"<p><img src=\"" + importXMLImgURL.toString() + "\" /> &nbsp;<b>Import model(s) from XML</b> " +
				"Read in an Expressway XML file (that you will be prompted for), " +
				"and load the models and Scenarios defined by that file into the server database. " +
				"If the XML defines names that are already in use, the elements in the XML " +
				"will be automatically renamed as needed." +
				"</p>" +
			"<p><img src=\"" + syncImgURL.toString() + "\" /> &nbsp;<b>Sync to disk</b> " +
				"Tell the server to write its in-memory database to disk. The disk-based " +
				"data will be automatically loaded the next time the server is started. " +
				"The database file is called 'expresswaydb.ser', and it is written to the " +
				"directory as configured on the server control panel. " +
				"Simulation results are not saved in this file, however: during a sync, " +
				"they are written to the sub-directory called 'simrundir' in the same directory." +
				"</p>"));
		
		helpButton.setToolTipText("Show explanation of each toolbar button");
		
		
		createDomainButton.addActionListener(new ActionListener()
		{
			public synchronized void actionPerformed(ActionEvent e)
			{
				if (getModelEngine() == null)
				{
					JOptionPane.showMessageDialog(DomainListPanel.this,
						"Must first connect to a Model Engine",
						"Error", JOptionPane.ERROR_MESSAGE); return;
				}
				
				// Display menu of the types of Domains that one can create.
				JPopupMenu menu = new JPopupMenu("Select a Domain type");
				try { DomainMenuItemHelper.addNewDomainMenuItems(domainSelector,
					createDomainButton, menu); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(DomainListPanel.this, ex);
					return;
				}
				
				menu.show(createDomainButton, createDomainButton.getWidth(), 0);
			}
		});
		
		
		createMotifButton.addActionListener(new ActionListener()
		{
			public synchronized void actionPerformed(ActionEvent e)
			{
				if (getModelEngine() == null)
				{
					JOptionPane.showMessageDialog(DomainListPanel.this,
						"Must first connect to a Model Engine",
						"Error", JOptionPane.ERROR_MESSAGE); return;
				}

				// Display menu of the types of base Domain that one can use to
				// define a Motif..
				JPopupMenu menu = new JPopupMenu("Select a base Domain type");
				try { DomainMenuItemHelper.addNewMotifMenuItems(domainSelector,
					createDomainButton, menu); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(DomainListPanel.this, ex);
					return;
				}
				
				
				/*
				
				JPopupMenu menu = new JPopupMenu("Select a Domain type");
				HelpfulMenuItem mdmi = new HelpfulMenuItem("Model Motif", true,
					HelpWindow.createURL("Model Domains"));
				HelpfulMenuItem rdmi = new HelpfulMenuItem("Requirement Motif", true,
					HelpWindow.createURL("Requirements"));
				
				mdmi.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						try { getModelEngine().createModelDomainMotif(); }
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(DomainListPanel.this, ex);
						}
					}
				});
				
				rdmi.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						try { getModelEngine().createRequirementDomainMotif(); }
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(DomainListPanel.this, ex);
						}
					}
				});
				
				menu.add(mdmi);
				menu.add(rdmi);
				*/
				
				
				menu.show(createMotifButton, createMotifButton.getWidth(), 0);
			}
		});
		
		
		importDomainButton.addActionListener(new ActionListener()
		{
			public synchronized void actionPerformed(ActionEvent e)
			{
				if (getModelEngine() == null)
				{
					JOptionPane.showMessageDialog(DomainListPanel.this,
						"Must first connect to a Model Engine",
						"Error", JOptionPane.ERROR_MESSAGE); return;
				}
				
				try
				{
					File file = FileChooserUtils.getFileToOpen(DomainListPanel.this,
						FileType.xml, FileType.jar);
					if (file == null) return;
				
					long fileLength = file.length();
					if (fileLength > Integer.MAX_VALUE)
					{
						JOptionPane.showMessageDialog(DomainListPanel.this,
						"File too large (" + fileLength + " characters); max is " 
							+ Integer.MAX_VALUE,
						"Error", JOptionPane.ERROR_MESSAGE); return;
					}
					
					String fileName = file.getName();
					
					if (fileName.endsWith(".xml") || fileName.endsWith(".XML"))
					{
						char[] xmlCharAr = new char[(int)fileLength];
						
						GlobalConsole.println("Reading file " + file.getName() + "...");
						FileReader reader = new FileReader(file);
						int noOfChars = reader.read(xmlCharAr);
						if (noOfChars < 0) throw new IOException("Error reading file " +
							fileName);
						if (noOfChars != (int)fileLength) throw new IOException(
							"File is " + fileLength + " chars but only " + noOfChars +
							" were read.");
							
						reader.close();
						
						
						// Call server to request that the XML data be processed.
						
						GlobalConsole.println("Parsing XML data...");
						Integer callbackId = getModelEngine().parseXMLCharAr(
							xmlCharAr, 
							DomainListPanel.this.getPeerListener());
					}
					else if (fileName.endsWith(".jar") || fileName.endsWith(".JAR"))
					{
						byte[] jarByteAr = new byte[(int)fileLength];
						
						GlobalConsole.println("Reading file " + file.getName() + "...");
						FileInputStream fis = new FileInputStream(file);
						int noOfBytes = fis.read(jarByteAr);
						if (noOfBytes < 0) throw new IOException("Error reading file " +
							file.getName());
						if (noOfBytes != (int)fileLength) throw new IOException(
							"File is " + fileLength + " bytes but only " + noOfBytes +
							" were read.");
						
						fis.close();
						
						
						// Call server to request that the JAR data is processed.
						
						GlobalConsole.println("Processing JAR file...");
						Integer callbackId = getModelEngine().processJARByteAr(
							jarByteAr,
							DomainListPanel.this.getPeerListener());
					}
					else
						throw new RuntimeException("Unexpected file extension");
					
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(DomainListPanel.this, ex);
				}
			}
		});
		
		
		saveAllButton.addActionListener(new ActionListener()
		{
			public synchronized void actionPerformed(ActionEvent e)
			{
				if (getModelEngine() == null)
				{
					JOptionPane.showMessageDialog(DomainListPanel.this,
						"Must first connect to a Model Engine",
						"Error", JOptionPane.ERROR_MESSAGE); return;
				}
				
				try { getModelEngine().flush(); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(DomainListPanel.this, ex); return;
				}
			}
		});
		
		
		reconnectButton.addActionListener(new ActionListener()
		{
			public synchronized void actionPerformed(ActionEvent e)
			{
				getMainApplication().setModelEngineNetworkPath(
					modelEngineNetworkPathField.getText());
				getMainApplication().connectToModelEngine();
				recreateDomainSelector();
			}
		});
		
		
		// Give focus to the server path field.
		
		modelEngineNetworkPathField.requestFocusInWindow();
		
		
		// Add listener to the server path field so that when the user presses
		// the Enter button the Reconnect Button is effectively clicked.
		
		modelEngineNetworkPathField.addKeyListener(new KeyAdapter()
		{
			public void keyTyped(KeyEvent e)
			{
				if (e.getKeyCode() == KeyEvent.VK_ENTER) reconnectButton.doClick();
			}
		});
		
		
		// Re-position sub-components.
		
		validate();
		

		// Make visible.
		
		setVisible(true);
	}
	
	
	public void connectedToModelEngine(boolean connected)
	{
		enableDomainTypeButton(connected);
	}
	
	
	ClientMainApplication getMainApplication() { return mainApplication; }
	
	
	PanelManager getPanelManager() { return this.panelManager; }
	
	
	ModelEngineRMI getModelEngine() { return mainApplication.getModelEngine(); }


	/**
	 * To be called whenever a connection to the ModelEngine is closed or created.
	 */
	 
	public void recreateDomainSelector()
	{
		if (domainTreeScrollPane != null) leftPanel.remove(domainTreeScrollPane);
		leftPanel.repaint();
		
		if (getModelEngine() != null)
		{
			try
			{
				leftPanel.add(domainTreeScrollPane = new JScrollPane(
					domainSelector = DomainSelector.createDomainSelector(getModelEngine(),
					getPanelManager(), this, "Domain List")));
				domainSelector.postConstructionSetup();
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainListPanel.this, ex);
				return;
			}
		
			domainTreeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			domainTreeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			//domainSelector.show();

			// Create a listener to respond to changes in the tree: specifically,
			// changes that hide or expose a selected node.
			
			domainSelector.setSelectionListener(new DomainSelector.NodeSelectionListener()
			{
				public void visualSelected(VisualComponent visual)
				{
					NodeSer nodeSer = null;
					
					try
					{
						nodeSer = getModelEngine().getNode(visual.getNodeSer().getNodeId());
						visual.setNodeSer(nodeSer);
					}
					catch (Exception ex) { return; }
					
					getStatusPanel().showNodeSer(nodeSer);
				}

				public void hostSelected(MutableTreeNode root)
				{
					String hostName = 
						panelManager.getClientMainApplication().getModelEngineNetworkPath();
					int rmiPort = 1099;
					getStatusPanel().showHost(hostName, rmiPort);
				}
			});
		}
		
		leftPanel.validate();
	}
	
	
	PeerListener getPeerListener() { return this.domainSelector.getPeerListener(); }
	
	
	public void saveFocusFieldChanges() { statusPanel.checkForUpdatedGridAreaField(); }
	
	
	StatusPanel getStatusPanel() { return statusPanel; }
	
	
	NodeSer getCurrentDisplayedNodeSer()
	{
		if (this.statusPanel == null) return null;
		return statusPanel.getNodeSer();
	}


	class StatusPanel extends JPanel
	{
		/** Cache of some informational panels, particularly those that are
			costly to rebuild from scratch. */
		//final Map<String, JPanel> panelCache = new HashMap<String, JPanel>();
			
		NodeSer nodeSer;
		JPanel gridArea;
		JScrollPane messageScrollPane;
		
		boolean canEditParams = false;
		StatusTextField simFromField = null;
		StatusTextField simToField = null;
		StatusTextField durationField = null;
		StatusTextField iterationLimitField = null;
		
		
		public StatusPanel()
		{
			setLayout(new BorderLayout());
			createGridArea();
		}
		
		
		void clear()
		{
			simFromField = null;
			simToField = null;
			durationField = null;
			iterationLimitField = null;
			canEditParams = false;
		}
		
		
		void createGridArea()
		{
			if (this.gridArea != null)
			{
				this.gridArea.setVisible(false);
				this.remove(this.gridArea);
			}

			JPanel newGridArea = new JPanel();
			GridBagLayout gridLayout = new GridBagLayout();
			newGridArea.setLayout(gridLayout);
			this.gridArea = newGridArea;
			this.add(newGridArea, BorderLayout.CENTER);
		}
		
		
		void addGridComponent(Component component, int row, int col, int nRows, int nCols,
			double rowWeight, double colWeight, int anchorLocation, boolean fill)
		{
			if (component == null) throw new RuntimeException("component is null");
			
			GridBagConstraints constraints = new GridBagConstraints(
				col, //int gridx,
				row, //int gridy,
				nCols, //int gridwidth,
				nRows, //int gridheight,
				colWeight, //double weightx: 0-1
				rowWeight, //double weighty: 0-1
				anchorLocation, //int anchor: FIRST_LINE_START, PAGE_START, FIRST_LINE_END
					// LINE_START, CENTER, LINE_END
					// LAST_LINE_START, PAGE_END, LAST_LINE_END
				fill ? GridBagConstraints.BOTH : GridBagConstraints.NONE,
					// NONE, HORIZONTAL, VERTICAL, BOTH
				new Insets(0, 0, 0, 0), //Insets insets,
				10, //int ipadx,
				4 //int ipady
				);
			
			gridArea.add(component, constraints);
		}
		
		
		StatusTextField addGridStatusTextComponent(boolean canEdit, boolean isTitle,
			String text,
			String htmlHelpText,  // may be null
			int row, int col, int nRows, int nCols,
			double rowWeight, double colWeight, int anchorLocation, boolean fill)
		{
			AbstractStatusText statusText;
			if (canEdit) statusText = new StatusTextField(isTitle, text, htmlHelpText);
			else statusText = new StatusText(isTitle, text, htmlHelpText);
			
			addGridComponent(statusText, row, col, nRows, nCols, rowWeight, colWeight,
				anchorLocation, fill);
			
			if (statusText instanceof StatusTextField) return (StatusTextField)statusText;
			else return null;
		}
		
		
		//public void removeCachedPanelFor(String nodeId)
		//{
		//	JPanel panel = panelCache.remove(nodeId);
		//}
		
		
		public NodeSer getNodeSer() { return this.nodeSer; }
		
		
		public void redraw()
		{
			if (this.nodeSer == null) return;
			
			showNodeSer(this.nodeSer);
		}
		
		
		public void redrawIfNodeIdIs(String nodeId)
		{
			NodeSer currentNodeSer = getStatusPanel().getNodeSer();
			if ((currentNodeSer != null) && currentNodeSer.getNodeId().equals(nodeId))
			{
				NodeSer ns = null;
				try { ns = getModelEngine().getNode(nodeId); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(this, ex);
					return;
				}

				showNodeSer(ns);
			}
		}
		
		
		/** Refresh only the start time and end time parameters. This avoids
			any sudden changes of which Component in the StatusPanel has focus. */
			
		public void updateModelScenarioTimeParams(String scenarioId,
			Date newFrom, Date newTo, long newDuration, int newIterLimit)
		{
			NodeSer currentNodeSer = getStatusPanel().getNodeSer();
			if (currentNodeSer == null) return;
			if (! (currentNodeSer instanceof ModelScenarioSer)) return;
			
			ModelScenarioSer scenarioSer = (ModelScenarioSer)currentNodeSer;
			
			scenarioSer.startingDate = newFrom;
			scenarioSer.finalDate = newTo;
			scenarioSer.duration = newDuration;
			scenarioSer.iterationLimit = newIterLimit;
			
			if (scenarioSer.getNodeId().equals(scenarioId))
			{
				DateFormat df = DateAndTimeUtils.DateTimeFormat;
				
				this.simFromField.setText(
					(scenarioSer.startingDate == null? "" : df.format(scenarioSer.startingDate)));
				
				this.simToField.setText(
					(scenarioSer.finalDate == null? "" : df.format(scenarioSer.finalDate)));
				
				this.durationField.setText(String.valueOf(scenarioSer.duration));
				this.iterationLimitField.setText(String.valueOf(scenarioSer.iterationLimit));
			}
		}
		
		
		public void checkForUpdatedGridAreaField()
		{
			if (gridArea == null) return;
			
			gridArea.grabFocus();
		}
		
		
		public void showHost(String hostName, int portNumber)
		{
			clear();
			checkForUpdatedGridAreaField();
			
			this.nodeSer = null;
			createGridArea();
			if (messageScrollPane != null) remove(messageScrollPane);
			messageScrollPane = null;
			
			addGridComponent(new StatusText(false, 
				"Host '" + hostName + "' on port " + portNumber,
				"The DNS name of the host and port on that host at which the " +
				"Expressway server is running"
				), 0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
			
			validate();
			repaint();
		}
		
		
		public void showNodeSer(NodeSer nodeSer)
		{
			clear();
			checkForUpdatedGridAreaField();
			
			this.nodeSer = nodeSer;
			
			if (messageScrollPane != null) remove(messageScrollPane);
			messageScrollPane = null;
			
			
			// Display name, date created, date last modified.
			
			StatusText nameLabel;
			StatusText dateModifiedLabel;
			StatusText descLabel;
			
			//add(dateModifiedLabel = new StatusText("Last modified " +
			//	(new Date(nodeSer.getLastUpdated())).toString()));
			
			if (nodeSer instanceof NodeDomainSer)
			{
				createGridArea();
				
				NodeDomainSer nodeDomainSer = (NodeDomainSer)nodeSer;
				
				String motifSource = nodeDomainSer.getSourceName();
				String motifSourceURLStr = (nodeDomainSer.getSourceURL() == null? "" :
					nodeDomainSer.getSourceURL().toString());
				String motifOriginalName = nodeDomainSer.getDeclaredName();
				String motifVersion = nodeDomainSer.getDeclaredVersion();
	
				//String[] getNamedReferenceNames();
	
				addGridComponent(new StatusText(false, 
					"Selected item is a " + nodeSer.getNodeKind(),
					"The item selected in the Domain List panel is a " +
					HelpWindow.createHref(nodeSer.getNodeKind() + "s", nodeSer.getNodeKind()) + "."),
					0, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(nameLabel = new StatusText(false,
					"Name: " + nodeSer.getName(),
					"The name of the item that is selected in the Domain List panel"),
					1, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false,
					"Original Name: " + motifOriginalName + " (version " + motifVersion + ")",
					"The name that was specified in this motif's XML file. See " +
					HelpWindow.createHref("Motifs") + "."),
					2, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false,
					"Source: " + motifSource,
					"The author or provider of this motif, as documented in the " +
					"motif's XML file"),
					3, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false,
					"Source URL: " + motifSourceURLStr,
					"The URL of the source of this motif, as documented in the " +
					"motif's XML file"),
					4, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				if (nodeSer instanceof MotifDefSer)
				{
					MotifDefSer motifDefSer = (MotifDefSer)nodeSer;
					
					int noOfComponents = motifDefSer.getChildNodeIds().length;
					String[] motifClassNames = motifDefSer.getMotifClassNames();

					addGridComponent(new StatusText(false,
						"Components: " + noOfComponents,
						"The number of Templates defined in this motif"),
						5, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
					
					addGridComponent(new StatusText(false,
						"Java UI action classes:",
						"Java classes provided in the motif JAR file that implement UI " +
						"functionality required by the motif's menus."),
						6, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
					
					JTextPane textPane = new JTextPane();
					String text = "";
					if (motifClassNames != null)
					{
						for (String cName : motifClassNames) text += (cName + "\n");
					}
					
					textPane.setText(text);
					textPane.setEditable(false);
					JScrollPane scrollPane = new JScrollPane(textPane);
					
					addGridComponent(scrollPane,
						7, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				}
				else if (nodeSer instanceof ModelDomainSer)
				{
					// Add control for displaying and changing the binding type
					// for the Domain.
					
					ModelDomainSer modelDomainSer = (ModelDomainSer)nodeSer;
					BindingTypeControl bindingTypeControl = new BindingTypeControl(modelDomainSer);
					addGridComponent(bindingTypeControl,
						5, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
					
					
					// Add control for displaying a list of the Domains that contain
					// Attributes that are bound to States in this Domain.
					
					CrossDomainReferencesControl crossDomainReferencesControl =
						new CrossDomainReferencesControl(modelDomainSer);
					addGridComponent(crossDomainReferencesControl,
						6, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
					
					
					// Add control for displaying and changing the Scenario that is
					// bound to each Scenario of this Domain.
					
					ScenarioBindingControl scenarioBindingControl = 
						new ScenarioBindingControl(modelDomainSer);
					addGridComponent(scenarioBindingControl,
						7, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				}
			}
			else if (nodeSer instanceof NamedReferenceSer)
			{
				createGridArea();
				
				descLabel = new StatusText(false, 
					"Selected item is a Named Reference",
					"The item selected in the Domain List panel is a " +
					HelpWindow.createHref("Named References", "Named Reference") + "."
					);
				
				addGridComponent(descLabel,
					0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);

				addGridComponent(nameLabel = new StatusText(false,
					"Name: " + nodeSer.getName(),
					"The name of the item that is selected in the Domain List panel"
					), 0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
			}
			else if (nodeSer instanceof ModelScenarioSer)
			{
				final ModelScenarioSer scenSer = (ModelScenarioSer)nodeSer;
				
				createGridArea();

				descLabel = new StatusText(false,
					"Selected item is a Model Scenario",
					"The item selected in the Domain List panel is a " +
					HelpWindow.createHref("Model Scenarios", "Model Scenario") + "."
					);

				addGridComponent(descLabel,
					0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(nameLabel = new StatusText(false,
					"Name: " + nodeSer.getName(),
					"The name of the item that is selected in the Domain List panel"
					), 0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);


				// Display simulation parameters.
				
				if (scenSer.simRunNodeIds.length == 0) this.canEditParams = true;

				addGridComponent(new StatusText(true, "Parameters:",
					"The parameters that were set when the simulation was started."
					), 1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Sim time start date:", 1, null),
					2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				DateFormat df = DateAndTimeUtils.DateTimeFormat;
				
				if ((this.simFromField = addGridStatusTextComponent(this.canEditParams, false, 
					(scenSer.startingDate == null? "" : df.format(scenSer.startingDate)),
					"Date/time, in simulation time, at which simulation begins; " +
					"ie., the earliest epoch in the simulation. May be left unspecified (blank). " +
					"Syntax is: Jun 10, 2011 10:00:00 PM EDT"
					, 2, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true)) != null)
					this.simFromField.setTextChangedHandler(new TextChangedHandler()
					{
						public void textChanged(StatusTextField statusTextField)
						{
							Date newStartDate;
							DateFormat df = DateAndTimeUtils.DateTimeFormat;
							
							String startDateString = statusTextField.getText().trim();
							
							if (startDateString.length() == 0) newStartDate = null;
							else try { newStartDate = df.parse(startDateString); }
							catch (ParseException ex)
							{
								ErrorDialog.showReportableDialog(StatusPanel.this, ex);
								return;
							}
							
							try
							{
								try { getModelEngine().setModelScenarioTimeParams(false,
									scenSer.getNodeId(), newStartDate, scenSer.finalDate,
									scenSer.duration, scenSer.iterationLimit); }
								catch (Warning w)
								{
									if (JOptionPane.showConfirmDialog(StatusPanel.this,
										w.getMessage(), "Warning",
										JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
									{
										getModelEngine().setModelScenarioTimeParams(true,
											scenSer.getNodeId(), newStartDate, scenSer.finalDate,
											scenSer.duration, scenSer.iterationLimit);
									}
								}
							}
							catch (Exception ex)
							{
								ErrorDialog.showReportableDialog(StatusPanel.this, ex);
							}
						}
					});
					
				
				addGridComponent(new StatusText(false, "Max sim time final date: ", 1, null),
					3, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				if ((this.simToField = addGridStatusTextComponent(this.canEditParams, false, 
					(scenSer.finalDate == null? "" : df.format(scenSer.finalDate)),
					"The time end, in simulated time, as specified by " +
					"the user. May be unspecified (blank)." +
					"Syntax is: Jun 10, 2011 10:00:00 PM EDT"
					, 3, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true)) != null)
					this.simToField.setTextChangedHandler(new TextChangedHandler()
					{
						public void textChanged(StatusTextField statusTextField)
						{
							Date newEndDate;
							DateFormat df = DateAndTimeUtils.DateTimeFormat;
							
							String endDateString = statusTextField.getText().trim();
							
							if (endDateString.length() == 0) newEndDate = null;
							else try { newEndDate = df.parse(endDateString); }
							catch (ParseException ex)
							{
								ErrorDialog.showReportableDialog(StatusPanel.this, ex);
								return;
							}
							
							try
							{
								try { getModelEngine().setModelScenarioTimeParams(false,
									scenSer.getNodeId(), scenSer.startingDate, newEndDate,
									scenSer.duration, scenSer.iterationLimit); }
								catch (Warning w)
								{
									if (JOptionPane.showConfirmDialog(StatusPanel.this,
										w.getMessage(), "Warning",
										JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
									{										
										getModelEngine().setModelScenarioTimeParams(true,
											scenSer.getNodeId(), scenSer.startingDate, newEndDate,
											scenSer.duration, scenSer.iterationLimit);
									}
								}
							}
							catch (Exception ex)
							{
								ErrorDialog.showReportableDialog(StatusPanel.this, ex);
							}
						}
					});
				
				addGridComponent(new StatusText(false, "Max sim time duration (days):", 1, null),
					4, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				if ((this.durationField = addGridStatusTextComponent(canEditParams, false, 
					String.valueOf(((double)scenSer.duration)/DateAndTimeUtils.MsInADay),
					"The maximum simulated duration (in days) that the simulation " +
					"was permitted to span."
					, 4, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true)) != null)
					this.durationField.setTextChangedHandler(new TextChangedHandler()
					{
						public void textChanged(StatusTextField statusTextField)
						{
							long newDuration;							
							String durationString = statusTextField.getText().trim();
							
							if (durationString.length() == 0) newDuration = 0;
							else try { newDuration = Long.parseLong(durationString); }
							catch (NumberFormatException ex)
							{
								ErrorDialog.showReportableDialog(StatusPanel.this, ex);
								return;
							}
							
							try
							{
								try { getModelEngine().setModelScenarioTimeParams(false,
									scenSer.getNodeId(), scenSer.startingDate, scenSer.finalDate,
									newDuration, scenSer.iterationLimit); }
								catch (Warning w)
								{
									if (JOptionPane.showConfirmDialog(StatusPanel.this,
										w.getMessage(), "Warning",
										JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
										
										getModelEngine().setModelScenarioTimeParams(true,
											scenSer.getNodeId(), scenSer.startingDate, scenSer.finalDate,
											newDuration, scenSer.iterationLimit);
								}
							}
							catch (Exception ex)
							{
								ErrorDialog.showReportableDialog(StatusPanel.this, ex);
							}
						}
					});
				
				addGridComponent(new StatusText(false, "Iteration limit:", 1, null),
					5, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				if ((this.iterationLimitField = addGridStatusTextComponent(
					canEditParams, false, String.valueOf(scenSer.iterationLimit),
					"The maximum number of simulation " + 
					HelpWindow.createHref("Iterations and Epochs", "iterations (epochs)") + 
					" to permit for the simulation."
					, 5, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true)) != null)
					this.iterationLimitField.setTextChangedHandler(new TextChangedHandler()
					{
						public void textChanged(StatusTextField statusTextField)
						{
							int newIterLimit;
							String iterLimitString = statusTextField.getText().trim();
							
							if (iterLimitString.length() == 0) newIterLimit = 0;
							else try { newIterLimit = Integer.parseInt(iterLimitString); }
							catch (NumberFormatException ex)
							{
								ErrorDialog.showReportableDialog(StatusPanel.this, ex);
								return;
							}
							
							try
							{
								try { getModelEngine().setModelScenarioTimeParams(false,
									scenSer.getNodeId(), scenSer.startingDate, scenSer.finalDate,
									scenSer.duration, newIterLimit); }
								catch (Warning w)
								{
									if (JOptionPane.showConfirmDialog(StatusPanel.this,
										w.getMessage(), "Warning",
										JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
										
										getModelEngine().setModelScenarioTimeParams(true,
											scenSer.getNodeId(), scenSer.startingDate, scenSer.finalDate,
											scenSer.duration, newIterLimit);
								}
							}
							catch (Exception ex)
							{
								ErrorDialog.showReportableDialog(StatusPanel.this, ex);
							}
						}
					});
				
				
				// Display statistics, if any.
				
				String netValueStatePath = scenSer.netValueStatePath;
				addGridComponent(new StatusText(false, "NetValue State: " + 
					(netValueStatePath == null ? "null" : netValueStatePath),
						"The State that is " +
						HelpWindow.createHref("Tag Values", "tagged") +
						" with a " +
						HelpWindow.createHref("Tag Values", "Net_Value", "Net Value") +
						" Attribute in this Scenario."
						), 6, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
						
				addGridComponent(new StatusText(false, "Computed mean: " + 
					(scenSer.computedNetValueMean == null? "?" : 
						scenSer.computedNetValueMean), 1,
						"The mean (average) of the simulated Scenario's " +
						HelpWindow.createHref("Tag Values", "Net_Value", "Net Value") +
						", averaged across all of the simulations in the Scenario."
						), 7, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
						
				addGridComponent(new StatusText(false, "Computed \u03c3: " + 
					(scenSer.computedNetValueVariance == null? "?" :
						Math.sqrt(scenSer.computedNetValueVariance)), 1,
						"The standard deviation of the simulated Scenario's " +
						HelpWindow.createHref("Tag Values", "Net_Value", "Net Value") +
						", based on all of the simulations in the Scenario."
						), 8, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
						
				addGridComponent(new StatusText(false, "Computed assurance: " + 
					(scenSer.computedNetValueAssurance == null? "null" :
						scenSer.computedNetValueAssurance), 1,
						"The " +
						HelpWindow.createHref("Assurance", "assurance") +
						" of the simulated Scenario's " +
						HelpWindow.createHref("Tag Values", "Net_Value", "Net Value") +
						", based on all of the simulations in the Scenario."
						), 9, 0, 1, 3, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				// Find the Assurance Expression.
				
				final String netValueAttrId = scenSer.netValueAttrId;
				
				Serializable attrValue = null;
				
				if (netValueAttrId == null)
				{
					addGridComponent(new StatusText(false, "Assurance Expression: none", 1,
						"Scenario has no State Attribute with a value of type " +
						HelpWindow.createHref("Tag Values", "Net_Value", "Net Value")),
						5, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				}
				else
				{
					try { attrValue =
						getModelEngine().getAttributeValueById(netValueAttrId, scenSer.getNodeId()); }
					catch (Exception ex)
					{
						addGridComponent(new StatusText(false, "Assurance Expression: Error",
							ThrowableUtil.getAllMessages(ex)),
							5, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
					}
				}
				
				if (attrValue != null)
				{
					if (! (attrValue instanceof Types.State.Financial.NetValue))
					{
						GlobalConsole.printStackTrace(new Exception(
							"Attribute value is not of type NetValue"));
					}
					else
					{
						final Types.State.Financial.NetValue attrTypedValue = 
							(Types.State.Financial.NetValue)attrValue;
						
						final String assuranceExpression = 
							((Types.State.Financial.NetValue)attrValue).assuranceExpression;
						
						JPanel assuranceExprPanel = new JPanel();
						((FlowLayout)(assuranceExprPanel.getLayout())).setAlignment(FlowLayout.LEFT);
						assuranceExprPanel.setBackground(Color.white);
								
						assuranceExprPanel.add(new StatusText(false, "Assurance expression: " + 
							assuranceExpression, 1,
							"The mathematical expression that is to be used to compute the " +
							HelpWindow.createHref("Assurance") +
							" for the " +
							HelpWindow.createHref("Tag Values", "Net_Value", "Net Value") +
							". The expression is contained in the NetValue Attribute."));
							
						JButton editButton = new JButton("Edit");
						assuranceExprPanel.add(editButton);
						
						addGridComponent(assuranceExprPanel,
							5, 0, 1, 2, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
						
						//addGridComponent(editButton = new JButton("Edit"),
						//	5, 3, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
						editButton.addActionListener(new ActionListener()
						{
							public void actionPerformed(ActionEvent e)
							{
								final Component targetVisual = (Component)(e.getSource());
								
								final StringEntryPanel entryPanel = 
									new StringEntryPanel("Edit Assurance Expression", false);
								
								entryPanel.setText(assuranceExpression);
								
								entryPanel.setValueReturner(new StringEntryPanel.ValueReturner()
								{
									public void returnStringValue(String newValue)
									{
										// Update the assurance expression on the server
										
										attrTypedValue.assuranceExpression = newValue;
										
										try
										{
											try
											{
												getModelEngine().setAttributeValue(false, netValueAttrId,
													scenSer.getNodeId(), attrTypedValue);
												entryPanel.dispose();
											}
											catch (Warning w)
											{
												if (JOptionPane.showConfirmDialog(DomainListPanel.this,
													w.getMessage(), "Warning",
													JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
													
													getModelEngine().setAttributeValue(true, netValueAttrId,
														scenSer.getNodeId(), attrTypedValue);
													entryPanel.dispose();
											}
										}
										catch (Exception ex)
										{
											ErrorDialog.showReportableDialog(DomainListPanel.this, ex);
										}
									}
								});
					
								java.awt.Point targetLocation = targetVisual.getLocationOnScreen();
								
								entryPanel.setLocation(targetLocation.x + targetVisual.getWidth(),
									targetLocation.y);
								
								entryPanel.show();
							}
						});
					}
				}
			}
			else if (nodeSer instanceof HierarchyScenarioSer)
			{
				final HierarchyScenarioSer scenSer = (HierarchyScenarioSer)nodeSer;
				
				createGridArea();

				descLabel = new StatusText(false,
					"Selected item is a Hierarchy Scenario",
					"The item selected in the Domain List panel is a " +
					HelpWindow.createHref("Hierarchy Scenarios", "Hierarchy Scenario") + "."
					);

				addGridComponent(descLabel,
					0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(nameLabel = new StatusText(false,
					"Name: " + nodeSer.getName(),
					"The name of the item that is selected in the Domain List panel"
					), 0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
			}
			else if (nodeSer instanceof ModelScenarioSetSer)
			{
				ModelScenarioSetSer scenSetSer = (ModelScenarioSetSer)nodeSer;
				
				
				// Display the Attribute that was varied, and its range
				// of values.
				
				//...
				createGridArea();
				//JPanel titlePanel = new JPanel();
				
				//titlePanel.add(
				descLabel = new StatusText(false,
					"Selected item is a Model Scenario Set",
					"The item selected in the Domain List panel is a " +
					HelpWindow.createHref("Model Scenario Sets", "Model Scenario Set") + "."
					);
				
				addGridComponent(descLabel,
					0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(nameLabel = new StatusText(false,
					"Name: " + nodeSer.getName(),
					"The name of the item that is selected in the Domain List panel"
					), 0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				
				// Display simulation parameters.
				
				addGridComponent(new StatusText(true, "Parameters:",
					"The parameters that were set when the simulation was started."
					), 1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Sim time start date:", 1, null),
					2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, StringUtils.safeToString(scenSetSer.startingDate),
					"Date/time, in simulation time, at which simulation begins; " +
					"ie., the earliest epoch in the simulation. May be left unspecified."
					), 2, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Max sim time final date: ", 1, null),
					3, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, StringUtils.safeToString(scenSetSer.finalDate),
					"The time end, in simulated time, as specified by " +
					"the user. May be unspecified."
					), 3, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Max sim time duration (days):", 1, null),
					4, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, 
					String.valueOf(((double)scenSetSer.duration)/DateAndTimeUtils.MsInADay),
					"The maximum simulated duration (in days) that the simulation " +
					"was permitted to span."
					), 4, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Iteration limit:", 1, null),
					5, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, String.valueOf(scenSetSer.iterationLimit),
					"The maximum number of simulation " + 
					HelpWindow.createHref("Iterations and Epochs", "iterations (epochs)") + 
					" to permit for the simulation."
					), 5, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
			}
			else if (nodeSer instanceof HierarchyScenarioSetSer)
			{
				HierarchyScenarioSetSer scenSetSer = (HierarchyScenarioSetSer)nodeSer;
				createGridArea();
				
				descLabel = new StatusText(false,
					"Selected item is a Hierarchy Scenario Set",
					"The item selected in the Domain List panel is a " +
					HelpWindow.createHref("Hierarchy Scenario Sets", "Hierarchy Scenario Set") + "."
					);
				
				addGridComponent(descLabel,
					0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(nameLabel = new StatusText(false,
					"Name: " + nodeSer.getName(),
					"The name of the item that is selected in the Domain List panel"
					), 0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
			}
			else if (nodeSer instanceof SimulationRunSer)
			{
				SimulationRunSer simRunSer = (SimulationRunSer)nodeSer;
				
				createGridArea();
				//JPanel titlePanel = new JPanel();
				
				//titlePanel.add
				descLabel = new StatusText(false,
					"Selected item is a Simulation Run",
					"The item selected in the Domain List panel is a " +
					HelpWindow.createHref("Simulation Runs", "Simulation Run") + "."
					);
				
				addGridComponent(descLabel,
					0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(nameLabel = new StatusText(false,
					"Name: " + nodeSer.getName(),
					"The name of the item that is selected in the Domain List panel"
					), 0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);

				
				// Display simulation parameters.
				/*
				long[][] iterEventNodeIds;
				List<Epoch> epochs;
				List<SimulationError> simErrors;
				TableEntry[] finalStateValuesByNodeId;
				List<Integer> iterations;
				String modelScenarioNodeId;
				Date startingDate;
				Date finalDate;
				int iterationLimit;
				long duration;
				long actualElapsedTime;
				long actualEndTime;
				long randomSeed;
				boolean completedNorm;
				long[] genEventNodeIds;
				String simRunSetNodeId;
				*/
				
				addGridComponent(new StatusText(true, "Parameters:",
					"The parameters that were set when the simulation was started."
					), 1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Sim time start date:", 1, null),
					2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, StringUtils.safeToString(simRunSer.startingDate),
					"Date/time, in simulation time, at which simulation begins; " +
					"ie., the earliest epoch in the simulation. May be left unspecified."
					), 2, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Max sim time final date: ", 1, null),
					3, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, StringUtils.safeToString(simRunSer.finalDate),
					"The time end, in simulated time, as specified by " +
					"the user. May be unspecified."
					), 3, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Iteration limit:", 1, null),
					4, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, String.valueOf(simRunSer.iterationLimit),
					"The maximum number of simulation " + 
					HelpWindow.createHref("Iterations and Epochs", "iterations (epochs)") + 
					" to permit for the simulation."
					), 4, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Max sim time duration (days):", 1, null),
					5, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, 
					String.valueOf(((double)simRunSer.duration)/DateAndTimeUtils.MsInADay),
					"The maximum simulated duration (in days) that the simulation " +
					"was permitted to span."
					), 5, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Random seed:", 1, null),
					6, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, String.valueOf(simRunSer.randomSeed),
					"The value used to seed the random number generator for this simulation."
					), 6, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
					
				
				addGridComponent(new StatusText(true, "Results:", "What the simulation produced"),
					7, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, 
					simRunSer.epochs.size() + " epochs (iterations)", 1,
					"The actual number of simulation " +
					HelpWindow.createHref("Iterations and Epochs", "iterations (epochs)") +
					" that occurred in this simulation."
					), 8, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, 
					simRunSer.simErrors.size() + " simulation errors", 1,
					"The number of errors that were detected during simulation."
					), 8, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Actual elapsed sim time (days):", 1, null),
					9, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, 
					String.valueOf(((double)simRunSer.actualElapsedTime)/DateAndTimeUtils.MsInADay),
					"The actual duration of this simulation, in simulated time."
					), 9, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Actual final sim time:", 1, null),
					10, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, new Date(simRunSer.actualEndTime).toString(),
					"The simulated time at which the simulation ended."
					), 10, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "# of events:", 1, null),
					11, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, String.valueOf(simRunSer.noOfGeneratedEvents),
					"The number of " +
					HelpWindow.createHref("Events") +
					" that were generated during simulation."
					), 11, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				JTextArea errorMsgArea = new JTextArea(6, 60);
				errorMsgArea.setEditable(false);
				this.messageScrollPane = new JScrollPane(errorMsgArea);
				for (Exception ex : simRunSer.simErrors)
				{
					Throwable t = ex;
					String indentation = "";
					while (t != null)
					{
						errorMsgArea.append(indentation + t.getClass().getName() +
							": " + t.getMessage() + "\n");
						
						t = t.getCause();
						indentation += "  ";
					}
				}
				
				add(messageScrollPane, BorderLayout.SOUTH);
			}
			else if (nodeSer instanceof SimRunSetSer)
			{
				SimRunSetSer simRunSetSer = (SimRunSetSer)nodeSer;
				
				createGridArea();
				//JPanel titlePanel = new JPanel();
				
				//titlePanel.add(
				descLabel = new StatusText(false,
					"Selected item is a Simulation Run Set",
					"The item selected in the Domain List panel is a " +
					HelpWindow.createHref("Simulation Run Sets", "Simulation Run Set") + "."
					);
				
				addGridComponent(descLabel,
					0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(nameLabel = new StatusText(false,
					"Name: " + nodeSer.getName(),
					"The name of the item that is selected in the Domain List panel"
					), 0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);

				
				// Display simulation parameters.
				
				addGridComponent(new StatusText(true, "Parameters:",
					"The parameters that were set when the simulation was started."
					), 1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Sim time start date:", 1, null),
					2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, StringUtils.safeToString(simRunSetSer.startingDate),
					"Date/time, in simulation time, at which simulation begins; " +
					"ie., the earliest epoch in the simulation. May be left unspecified."
					), 2, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Max sim time final date: ", 1, null),
					3, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, StringUtils.safeToString(simRunSetSer.finalDate),
					"The time end, in simulated time, as specified by " +
					"the user. May be unspecified."
					), 3, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Iteration limit:", 1, null),
					4, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, String.valueOf(simRunSetSer.iterationLimit),
					"The maximum number of simulation " + 
					HelpWindow.createHref("Iterations and Epochs", "iterations (epochs)") + 
					" to permit for the simulation."
					), 4, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				addGridComponent(new StatusText(false, "Max sim time duration (days):", 1, null),
					5, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				addGridComponent(new StatusText(false, 
					String.valueOf(((double)simRunSetSer.duration)/DateAndTimeUtils.MsInADay),
					"The maximum simulated duration (in days) that the simulation " +
					"was permitted to span."
					), 5, 1, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
				
				
				// Display statistics, if any.
				
				addGridComponent(new StatusText(false, "Computed mean: " + 
					(simRunSetSer.computedNetValueMean == null? "?" : 
						simRunSetSer.computedNetValueMean), 1,
						"The mean (average) of the simulated Scenario's " +
						HelpWindow.createHref("Tag Values", "Net_Value", "Net Value") +
						", averaged across all of the simulations in the Simulation Run Set."
						), 6, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
						
				addGridComponent(new StatusText(false, "Computed \u03c3: " + 
					(simRunSetSer.computedNetValueVariance == null? "?" :
						Math.sqrt(simRunSetSer.computedNetValueVariance)), 1,
						"The standard deviation of the simulated Scenario's " +
						HelpWindow.createHref("Tag Values", "Net_Value", "Net Value") +
						", based on all of the simulations in the Simulation Run Set."
						), 7, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
						
				addGridComponent(new StatusText(false, "Computed assurance: " + 
					(simRunSetSer.computedNetValueAssurance == null? "null" :
						simRunSetSer.computedNetValueAssurance), 1,
						"The " +
						HelpWindow.createHref("Assurance", "assurance") +
						" of the simulated Scenario's " +
						HelpWindow.createHref("Tag Values", "Net_Value", "Net Value") +
						", based on all of the simulations in the Simulation Run Set."
						), 8, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, true);
			}
			
			validate();
			repaint();
		}
	}


	/** Display radio buttons for the binding types to be used when simulating
		a Domain. */
		
	class BindingTypeControl extends JPanel
	{
		NodeSer nodeSer;
		Boolean currentType;
		TimePeriodType currentPeriod;
		
		JRadioButton averagesButton = new JRadioButton("Average values");
		JRadioButton actualsButton = new JRadioButton("Actual values");
		JRadioButton clearButton = null;
		ButtonGroup group = new ButtonGroup();
		TimePeriodType[] listData = 
		{
			TimePeriodType.unspecified, TimePeriodType.daily, TimePeriodType.weekly, 
			TimePeriodType.monthly, TimePeriodType.quarterly, TimePeriodType.semiannually, 
			TimePeriodType.annually
		};
		
		JList periodList = new JList(listData);

		BindingTypeControl(NodeSer nodeSer)
		{
			this.nodeSer = nodeSer;
			
			this.setBackground(Color.white);
			
			if (nodeSer instanceof ModelDomainSer)
			{
				this.currentType = ((ModelDomainSer)nodeSer).externalStateBindingType;
				this.currentPeriod = 
					((ModelDomainSer)nodeSer).externalStateGranularity;
				
				if (this.currentType == null) throw new RuntimeException(
					"Current binding type is null");
			}
			else if (nodeSer instanceof ModelScenarioSer)
			{
				this.currentType = ((ModelScenarioSer)nodeSer).externalStateBindingType;
				this.currentPeriod = 
					((ModelScenarioSer)nodeSer).externalStateGranularity;
			}
			else
				throw new RuntimeException("NodeSer is a " + nodeSer.getClass().getName());
			
			if (nodeSer instanceof ModelScenarioSer)
				clearButton = new JRadioButton("Clear");
			
			group.add(averagesButton);
			group.add(actualsButton);
			if (clearButton != null) group.add(clearButton);
			
			// Set the radio button that reflects the binding current type
			if (currentType == null)
				clearButton.setSelected(true);
			else if (currentType.booleanValue())
				averagesButton.setSelected(true);
			else
				actualsButton.setSelected(true);
			
			periodList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			periodList.setVisibleRowCount(1);
			periodList.setSelectedValue(currentPeriod, true);
			
			
			// Arrange the positions of these elements.
			
			JPanel bindingTypePanel = new JPanel();
			bindingTypePanel.setBackground(Color.white);
			bindingTypePanel.add(new StatusText(false, 
				"For external States, use", 
				"Choose the manner in which simulations link to bound States " +
				"of other models. See " +
				HelpWindow.createHref("Binding an Attribute to a State") + "."
				));
			bindingTypePanel.add(averagesButton);
			bindingTypePanel.add(actualsButton);
			this.add(bindingTypePanel);
			
			JPanel dataFrequencyPanel = new JPanel();
			dataFrequencyPanel.setBackground(Color.white);
			dataFrequencyPanel.add(new StatusText(false, "Import data frequency:",
				"Specify the period (in simulation time) between cross-model " +
				"synchronization of bound States. See " +
				HelpWindow.createHref("Binding an Attribute to a State") + "."
				));
			dataFrequencyPanel.add(periodList);
			this.add(dataFrequencyPanel);
			
			this.validate();
			
			
			// Add Action Listeners.
			
			averagesButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					setNewType();
				}
			});
			
			actualsButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					setNewType();
				}
			});
			
			periodList.addListSelectionListener(new ListSelectionListener()
			{
				public void valueChanged(ListSelectionEvent e)
				{
					setNewType();
				}
			});
		}
		
		void setNewType()
		{
			try
			{
				Boolean newType = null;
				TimePeriodType newPeriod = TimePeriodType.unspecified;
				
				if (averagesButton.isSelected())
					newType = new Boolean(true);
				if (actualsButton.isSelected())
					newType = new Boolean(false);
				
				newPeriod = (TimePeriodType)(periodList.getSelectedValue());
				
				try
				{
					getModelEngine().setStateBindingParameters(false, nodeSer.getNodeId(),
						newType, newPeriod);
					
					this.currentType = newType;
					this.currentPeriod = newPeriod;
				}
				catch (Warning w)
				{
					if (JOptionPane.showConfirmDialog(BindingTypeControl.this,
						"Warning: simulation runs will be deleted. Continue?", "Warning",
						JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION)
					{
						getModelEngine().setStateBindingParameters(true, nodeSer.getNodeId(),
							newType, newPeriod);
						
						this.currentType = newType;
						this.currentPeriod = newPeriod;
					}
				}
			}
			catch (Exception ex)
			{
				ErrorDialog.showThrowableDialog(BindingTypeControl.this, ex);
			}
			
			if (this.currentType == null)
				group.clearSelection();
			else if (this.currentType.booleanValue())
				averagesButton.setSelected(true);
			else
				actualsButton.setSelected(true);
			
			periodList.setSelectedValue(this.currentPeriod, true);
		}
		

		boolean getBindingType() { return this.currentType; }
		
		TimePeriodType getPeriod() { return this.currentPeriod; }
	}
	
	
	class CrossDomainReferencesControl extends JPanel
	{
		CrossDomainReferencesControl(ModelDomainSer domainSer)
		{
			this.setBackground(Color.white);
			
			
			// Display a list of the Model Domains that contain
			// Attributes that are bound to States in the specified Domain.
			
			JPanel topPanel = new JPanel();

			
			// Identify the Domains that reference this Domain.
			
			ModelDomainSer[] domainSers = null;
			try { domainSers = getModelEngine().getBindingDomains(domainSer.getNodeId()); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(this, ex);
				return;
			}
			
			int noOfCols = 3;
			int noOfRows = domainSers.length / noOfCols + 1;
			JPanel domainListPanel = new JPanel();
			domainListPanel.setLayout(new GridLayout(noOfRows, noOfCols, 4, 4));
			domainListPanel.setBackground(Color.white);
			
			for (ModelDomainSer dSer : domainSers)
			{
				domainListPanel.add(new JLabel(dSer.getName()));
			}

			JScrollPane centerPanel = new JScrollPane(domainListPanel);
			centerPanel.setBackground(Color.white);

			
			// Arrange the positions of these elements.
			
			this.setLayout(new BorderLayout());
			
			this.add(topPanel, BorderLayout.NORTH);
			this.add(centerPanel, BorderLayout.CENTER);
			
			topPanel.add(new StatusText(true, "Referenced by:",
				"A list of the Domains that own Attributes that are bound " +
				"to States of this Domain. See " +
				HelpWindow.createHref("Binding an Attribute to a State") + "."
				));
		}
	}
	
	
	class ScenarioBindingControl extends JPanel implements MouseListener
	{
		ScenarioBindingControl(ModelDomainSer domainSer)
		{
			this.setBackground(Color.white);
			
			
			//
			// Display controls for editing the Scenario that is
			// bound to each Scenario of the specified Domain.
			//
			
			// Identify the Scenarios of this Model Domain.
			
			ScenarioSer[] scenarioSers = null;
			try { scenarioSers = getModelEngine().getScenarios(domainSer.getName()); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(this, ex);
				return;
			}
			
			
			// Create a Panel for managing the bindings of Scenarios and Domain.

			ModelDomainSer[] boundDomainSers = null;
			try { boundDomainSers = 
				getModelEngine().getBoundDomains(domainSer.getNodeId()); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(this, ex);
				return;
			}
			
			int nRows = scenarioSers.length + 1;
			int nCols = boundDomainSers.length + 1;
			JPanel scenarioPanel = new JPanel();
			GridLayout gridLayout = new GridLayout(nRows, nCols, 4, 4);
			gridLayout.setVgap(2);
			gridLayout.setHgap(2);
			scenarioPanel.setLayout(gridLayout);
			scenarioPanel.setBackground(Color.gray);
			
			
			// Add table heading, consisting of the name of each bound Domain.
			
			scenarioPanel.add(new JLabel());  // an empty cell
			
			for (ModelDomainSer dSer : boundDomainSers)
			{
				scenarioPanel.add(new JLabel(dSer.getName()));
			}
			
			
			// Add each table row: one for each Scenario of this Domain.
			
			if (boundDomainSers.length > 0)
			for (ScenarioSer scenarioSer : scenarioSers)
			{
				scenarioPanel.add(new JLabel(scenarioSer.getName()));
				
				
				// Add cell for each bound Domain. The cell contains the name
				// of the Scenario of that Domain that is bound to 'scenario'.
				
				for (ModelDomainSer dSer : boundDomainSers)
				{
					ModelScenarioSer boundScenarioSer = null;
					try { boundScenarioSer = 
						getModelEngine().getBoundScenarioForBoundDomain(
							scenarioSer.getNodeId(), dSer.getNodeId());
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(this, ex);
						scenarioPanel.add(new JLabel("???"));
						continue;
					}
					
					ScenarioLabel label = 
						new ScenarioLabel((ModelScenarioSer)scenarioSer, boundScenarioSer, dSer);
					scenarioPanel.add(label);
					
					label.addMouseListener(this);
					// Display a popup that allows the user to choose from
					// among the Scenarios of the bound Domain.
				}
			}


			// Arrange the positions of these elements.
			
			this.setLayout(new BorderLayout());
			JPanel topPanel = new JPanel();
			//topPanel.setBackground(Color.white);
			JScrollPane centerPanel = new JScrollPane(scenarioPanel);
			centerPanel.setBackground(Color.white);
			
			this.add(topPanel, BorderLayout.NORTH);
			this.add(centerPanel, BorderLayout.CENTER);
			
			topPanel.add(new StatusText(true, "References:",
				"A list of the Domains that own States to which Attributes in " +
				"this Domain are bound, and the Scenario of each of those Domains " +
				"that should be used when simulating each Scenario of this Domain. " +
				"Click on a Scenario to change it. See " +
				HelpWindow.createHref("Binding an Attribute to a State") + "."
				));
		}
		
		
		class ScenarioLabel extends JButton
		{
			ModelScenarioSer bindingScenarioSer = null;
			ModelScenarioSer boundScenarioSer = null;
			ModelDomainSer boundDomainSer = null;
			
			public ScenarioLabel(ModelScenarioSer bindingScenarioSer,
				ModelScenarioSer boundScenarioSer, ModelDomainSer boundDomainSer)
			{
				super(boundScenarioSer == null? "Unassigned" : boundScenarioSer.getName());
				this.bindingScenarioSer = bindingScenarioSer;
				this.boundScenarioSer = boundScenarioSer;
				this.boundDomainSer = boundDomainSer;
				
				if (boundScenarioSer == null) this.setBackground(java.awt.Color.red);
				else this.setBackground(java.awt.Color.yellow);
			}
			
			ModelScenarioSer getBindingScenario() { return this.bindingScenarioSer; }
			ModelScenarioSer getBoundScenario() { return this.boundScenarioSer; }
			ModelDomainSer getBoundDomain() { return this.boundDomainSer; }
		}
		
		
		public void mouseClicked(MouseEvent e)
		{
			ScenarioLabel label = (ScenarioLabel)(e.getComponent());
			ModelScenarioSer bindingScenarioSer = label.getBindingScenario();
			ModelScenarioSer boundScenarioSer = label.getBoundScenario();
			ModelDomainSer boundDomainSer = label.getBoundDomain();
			
			
			// Display a popup that allows the user to choose from
			// among the Scenarios of the bound Domain.
			
			(new ScenarioBindingChooser(bindingScenarioSer, boundDomainSer)).show(
				label, label.getWidth(), 0);
		}
		
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e)  {}
		
		
		class ScenarioBindingChooser extends JPopupMenu implements ActionListener
		{
			ModelScenarioSer bindingScenarioSer = null;
			ModelDomainSer boundDomainSer = null;
			ScenarioSer[] boundDomainScenarioSers = null;
			JMenuItem unassignedMenuItem;

			ScenarioBindingChooser(ModelScenarioSer bindingScenarioSer,
				ModelDomainSer boundDomainSer)
			{
				super("Select a Scenario of Domain '" + boundDomainSer.getName() + "'");
				
				this.bindingScenarioSer = bindingScenarioSer;
				this.boundDomainSer = boundDomainSer;
				
				try { boundDomainScenarioSers = 
					getModelEngine().getScenariosForDomainId(boundDomainSer.getNodeId()); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(this, ex);
					return;
				}
				
				unassignedMenuItem = new JMenuItem("None");
				this.add(unassignedMenuItem);
				unassignedMenuItem.addActionListener(this);
				
				for (ScenarioSer scenarioSer : boundDomainScenarioSers)
				{
					JMenuItem menuItem = new JMenuItem(scenarioSer.getName());
					this.add(menuItem);
					menuItem.addActionListener(this);
				}
			}
			
			
			public void actionPerformed(ActionEvent e)
			{
				// Bind the selected Scenario to 'bindingScenarioSer'
				
				String bindingScenarioId = bindingScenarioSer.getNodeId();
				String boundScenarioId = null;
				String boundDomainId = boundDomainSer.getNodeId();
				
				JMenuItem menuItem = (JMenuItem)(e.getSource());
				if (menuItem != unassignedMenuItem)
				{
					int index = this.getComponentIndex(menuItem);
					if (index > 0)
						boundScenarioId = boundDomainScenarioSers[index-1].getNodeId();
				}
				
				try
				{
					try
					{
						getModelEngine().bindScenarioToScenarioForDomain(false,
							bindingScenarioId, boundScenarioId, boundDomainId);
					}
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(ScenarioBindingControl.this,
							"Warning: simulation runs will be deleted. Continue?", "Warning",
							JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION)
						{
							getModelEngine().bindScenarioToScenarioForDomain(true, 
								bindingScenarioId, boundScenarioId, boundDomainId);
						}
					}
				}
				catch (Exception ex)
				{
					ErrorDialog.showThrowableDialog(ScenarioBindingControl.this, ex);
				}
			}
		}
	}

	
	class TutorButton extends OpenBrowserButton
	{
		java.awt.Dimension dim = new java.awt.Dimension(200, 50);
		
		TutorButton(String title, java.net.URL url)
		{
			super(title, url);
			this.setPreferredSize(dim);
			this.setMaximumSize(dim);
		}
	}


	
  /* ***************************************************************************
   * ***************************************************************************
   * From ViewFactory.
   */


	public NodeView createView(String nodeId, String... scenarioIds)
	throws
		Exception
	{
		// debug
		GlobalConsole.println("Entered createView 1...");
		// end debug
		
		
		// Determine if a NodeView of the Domain is already open.
		
		Single_NodeView_Container nvc = getPanelManager().getNodeViewContainer(nodeId);
		if (nvc != null)
		{
			getPanelManager().setSelectedComponent(nvc.getComponent());
			
			// Ensure that the specified Scenarios are loaded.
			for (String scenarioId : scenarioIds) nvc.getNodeView().showScenario(scenarioId);
			return null;
		}

		// The following will call getCurrentScenarioPanel, but the
		// controlPanelSelector has not yet been created.
		
		NodeView view = createViewWithoutScenarios(nodeId);
		for (String scenarioId : scenarioIds) view.showScenario(scenarioId);
		return view;
	}
	
	
	public NodeView createView(String nodeId, boolean loadScenarios)
	throws
		Exception
	{
		// debug
		GlobalConsole.println("Entered createView 2...");
		// end debug
		
		
		ScenarioSer[] scenarioSers = getModelEngine().getScenariosForDomainId(nodeId);

		// Determine if a NodeView of the Domain is already open.
		Single_NodeView_Container nvc = getPanelManager().getNodeViewContainer(nodeId);
		if (nvc != null)
		{
			getPanelManager().setSelectedComponent(nvc.getComponent());
			
			// Ensure that the specified Scenarios are loaded.
			for (ScenarioSer scenarioSer : scenarioSers)
				nvc.getNodeView().showScenario(scenarioSer.getNodeId());
			return null;
		}

		NodeView view = createViewWithoutScenarios(nodeId);
		for (ScenarioSer scenarioSer : scenarioSers) view.showScenario(scenarioSer.getNodeId());
		return view;
	}
	

	public ViewContainer createViewPanel(boolean asPopup, String nodeId, String... scenarioIds)
	throws
		Exception
	{
		NodeView view = createView(nodeId, scenarioIds);
		if (view == null) return null;
			
		if (view instanceof TreeTableComponent)
		{
			// Embed the View in a MultiHierarchyPanel, so that the user can later
			// add more Views and CrossMapPanels.
			ViewContainer cont = createTreeTableComponentContainer(asPopup, view);
			addViewToPanelManager(asPopup, (NodeViewContainer)cont);
			return cont;
		}
		else
		{
			ViewContainer vc = addViewToPanelManager(asPopup, (NodeViewContainer)view);
			view.postConstructionSetup();
			return vc;
		}
	}


	public ViewContainer createViewPanel(boolean asPopup, String nodeId, boolean loadScenarios)
	throws
		Exception
	{
		NodeView view = createView(nodeId, loadScenarios);
		if (view == null) return null;
			
		if (view instanceof TreeTableComponent)
		{
			// Embed the View in a MultiHierarchyPanel, so that the user can later
			// add more Views and CrossMapPanels.
			ViewContainer cont = createTreeTableComponentContainer(asPopup, view);
			addViewToPanelManager(asPopup, (NodeViewContainer)cont);
			return cont;
		}
		else
		{
			ViewContainer vc = addViewToPanelManager(asPopup, (NodeViewContainer)view);
			view.postConstructionSetup();
			return vc;
		}
	}
	
	
	public ViewContainer createDualViewPanel(boolean asPopup, String namedRefName,
		String nodeIdLeft, String nodeIdRight)
	throws
		Exception
	{
		NodeView viewLeft = createView(nodeIdLeft);
		NodeView viewRight = createView(nodeIdRight);
		
		if (! (viewLeft instanceof TreeTablePanelViewBase)) throw new Exception(
			"View of type " + viewLeft.getViewType() + " cannot be added to a Dual View Panel");
		
		if (! (viewRight instanceof TreeTablePanelViewBase)) throw new Exception(
			"View of type " + viewRight.getViewType() + " cannot be added to a Dual View Panel");
		
		String title = viewLeft.getName() + " " + namedRefName + " " + viewRight.getName();
		ScrollableMultiViewContainer panel = new ScrollableMultiViewContainer(title);
		getPanelManager().addPanel(title, panel, asPopup);
		
		TreeTableComponent ttcLeft = (TreeTablePanelViewBase)viewLeft;
		TreeTableComponent ttcRight = (TreeTablePanelViewBase)viewRight;
		
		CrossMapPanel cmp = new TreeTablePanelViewBase.CenterPanel(
			(TreeTablePanelViewBase)viewLeft, (TreeTablePanelViewBase)viewRight, namedRefName);

		panel.addTreeTableComponent(0, ttcLeft);
		panel.addTreeTableComponent(1, ttcRight);
		panel.addCrossMapPanel(0, cmp);

		try { panel.postConstructionSetup(); }
		catch (Exception ex) { ErrorDialog.showReportableDialog(this, ex); }

		return panel;
	}
   
   
	/*
	 * Deprecated methods. Calls to these should be replaced with calls to the
	 * createView and createViewPanel methods.
	 */
	
	
	public synchronized View createRiskMitigationView(ActivitySer actSer,
		String modelScenarioNodeId, boolean bringToFore)
	throws
		Exception
	{
		NodeSer nodeSer = getModelEngine().getNode(modelScenarioNodeId);
		if (! (nodeSer instanceof ModelScenarioSer)) throw new RuntimeException(
			"NodeSer is a " + nodeSer.getClass().getName() + " instead of a ModelScenarioSer");
		
		ActivityRiskMitTreeViewPanel view =
			new ActivityRiskMitTreeViewPanel(getPanelManager(), null,
				this, actSer, 
				(ModelScenarioSer)nodeSer, this.getModelEngine());

		if (bringToFore)
		{
			getPanelManager().setSelectedComponent(view);
		}
		
		return view;
	}
	
	
	public synchronized View createModelDomainViewPanel(ModelElementSer modelElementSer,
		boolean openScenarios, boolean bringToFore)
	throws
		Exception
	{
		return createModelDomainViewPanel(modelElementSer.getNodeId(),
			openScenarios, bringToFore);
	}


	public synchronized View createModelDomainViewPanel(String modelElementNodeId,
		boolean openScenarios, boolean bringToFore)
	throws
		Exception
	{
		// Determine if a View of the Domain is already open.
		
		Set<NodeView> domainViewPanels = 
			getPanelManager().getNodeViewsForNode(modelElementNodeId);
		
		if (domainViewPanels.size() > 1) throw new Exception(
			"More than one View Panel depicting Element with Id " + modelElementNodeId);
		
		if (domainViewPanels.size() == 1)
		{
			ViewPanelBase[] viewAr = domainViewPanels.toArray(new ViewPanelBase[1]);
			if (! (viewAr[0] instanceof ModelDomainViewPanel)) throw new RuntimeException(
				"View Panel is a " + viewAr[0].getClass().getName());
			
			if (bringToFore)
			{
				getPanelManager().setSelectedComponent(viewAr[0]);
			}
			
			return (ModelDomainViewPanel)(viewAr[0]);
		}

		
		// Create a View for the selected Domain.
		
		NodeSer nodeSer = getModelEngine().getNode(modelElementNodeId);
		if (! (nodeSer instanceof ModelElementSer)) throw new RuntimeException(
			"Node with ID " + modelElementNodeId + " is not a ModelElement");
		
		ModelScenarioViewPanel modelScenarioViewPanel =
			new ModelScenarioViewPanel(getPanelManager(), (ModelElementSer)nodeSer, this,
				this.getModelEngine());
		
		
		// Add the View as a new tabbed pane and make it the current tab.
		
		modelScenarioViewPanel.setVisible(true);
		
		getPanelManager().addPanel(modelScenarioViewPanel.getName(), modelScenarioViewPanel, false);
		getPanelManager().recomputeLayout();
		//modelScenarioViewPanel.postConstructionSetup();
		
		if (openScenarios) modelScenarioViewPanel.loadAllScenarios();
		
		if (bringToFore)
		{
			getPanelManager().setSelectedComponent(modelScenarioViewPanel);
		}
		
		return modelScenarioViewPanel;
	}
	
	
	/*
	public synchronized View createMotifViewPanel(NodeSer nodeSer, boolean bringToFore)
	throws
		Exception
	{
		return createMotifViewPanel(nodeSer.getNodeId(), bringToFore);
	}
	
	
	public synchronized View createMotifViewPanel(String elementId, boolean bringToFore)
	throws
		Exception
	{
		// Determine if a View of the Domain is already open.
		
		Set<NodeView> domainViewPanels = 
			getPanelManager().getNodeViewsForNode(elementId);
		
		if (domainViewPanels.size() > 1) throw new Exception(
			"More than one View Panel depicting Element with Id " + elementId);
		
		if (domainViewPanels.size() == 1)
		{
			ViewPanelBase[] viewAr = domainViewPanels.toArray(new ViewPanelBase[1]);
			if (! (viewAr[0] instanceof ModelDomainViewPanel)) throw new RuntimeException(
				"View Panel is a " + viewAr[0].getClass().getName());
			
			if (bringToFore)
			{
				getPanelManager().setSelectedComponent(viewAr[0]);
			}
			
			return (View)(viewAr[0]);
		}

		
		// Create a View for the selected Motif.
		
		NodeSer nodeSer = getModelEngine().getNode(elementId);
		if (! (nodeSer instanceof ModelElementSer)) throw new RuntimeException(
			"Node with ID " + elementId + " is not a ModelElement");
		
		MotifViewPanel motifViewPanel =
			new MotifViewPanel(getPanelManager(), (ModelElementSer)nodeSer, this,
				this.getModelEngine());
		
		
		// Add the View as a new tabbed pane and make it the current tab.
		
		motifViewPanel.setVisible(true);
		
		getPanelManager().addPanel(motifViewPanel.getName(), motifViewPanel, false);
		getPanelManager().recomputeLayout();
		//motifViewPanel.postConstructionSetup();
		
		if (bringToFore)
		{
			getPanelManager().setSelectedComponent(motifViewPanel);
		}
		
		return motifViewPanel;
	}
	*/
	
	
	public synchronized View createPopupModelDomainViewPanel(String title,
		ModelElementSer nodeSer)
	throws
		Exception
	{
		return (View)(createViewPanel(true, nodeSer.getNodeId()));
	}


	/*
	 * Implementation methods and classes.
	 */
	
	
	protected ViewContainer createTreeTableComponentContainer(boolean asPopup, NodeView view)
	throws
		Exception
	{
		// Embed the View in a MultiHierarchyPanel, so that the user can later
		// add more Views and CrossMapPanels.
		TreeTableComponent ttcLeft = (TreeTablePanelViewBase)view;
		String title = view.getName();
		ScrollableMultiViewContainer panel = new ScrollableMultiViewContainer(title);
		panel.addTreeTableComponent(0, ttcLeft);
		try { ttcLeft.postConstructionSetup(); }
		catch (Exception ex) { ErrorDialog.showReportableDialog(this, ex); }
		//getPanelManager().validate();
		return panel;
	}
	
	
	protected ViewContainer addViewToPanelManager(boolean asPopup, NodeViewContainer view)
	throws
		Exception
	{
		String title = view.getName();
		getPanelManager().addPanel(title, view.getContainer(), asPopup);
		getPanelManager().setSelectedComponent(view.getContainer());
		if (view instanceof NodeView) ((NodeView)view).resizeDomainInfoStrip();
		//view.postConstructionSetup();
		getPanelManager().validate();
		return view;
	}
	

	protected NodeView createViewWithoutScenarios(String nodeId)
	throws
		Exception
	{
		// Create a View for the selected Domain.
		
		String className = null;
		String viewTypeName = null;
		try
		{
			viewTypeName = getModelEngine().getViewType(nodeId);
			className = getModelEngine().getViewClassName(nodeId);
		}
		catch (Exception ex)
		{
			throw new Exception("While getting View name for Node Id " + 
				nodeId + " from Model Engine", ex);
		}
		
		if (className == null) throw new Exception(
			"No View class defined for the Domain of Node with Id " + nodeId);
		
		// Use the ClientSideClassLoader to load the Java View class.
		
		Class viewClass;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		ClientSideClassLoader clientSideClassLoader;
		if (cl instanceof ClientSideClassLoader)
			clientSideClassLoader = (ClientSideClassLoader)cl;
		else
		{
			clientSideClassLoader = new ClientSideClassLoader(getModelEngine(), cl);
			Thread.currentThread().setContextClassLoader(clientSideClassLoader);
		}
		
		try { viewClass = clientSideClassLoader.loadClass(className, true); }
		catch (Exception ex)
		{
			throw new Exception("Unable to load class " + className, ex);
		}
		
		// See ViewFactory.
		Constructor constructor;
		try { constructor = viewClass.getConstructor(PanelManager.class,
			NodeSer.class, ViewFactory.class, ModelEngineRMI.class); }
		catch (NoSuchMethodException ex)
		{
			throw new Exception("While getting constructor for class " +
				viewClass.getName(), ex);
		}
		
		NodeSer nodeSer;
		try { nodeSer = getModelEngine().getNode(nodeId); }
		catch (Exception ex)
		{
			throw new Exception("While getting Node with Id " + nodeId, ex);
		}
		
		NodeViewPanelBase viewPanel;
		try { viewPanel = (NodeViewPanelBase)(constructor.newInstance(getPanelManager(),
			nodeSer, this, this.getModelEngine())); }
		catch (Exception ex)
		{
			throw new Exception("While creating new instance of View " + 
				viewTypeName, ex);
		}
		
		//String[] parts = viewTypeName.split("\\.");
		viewPanel.setName(
			//parts[parts.length-1] + " " + 
			nodeSer.getFullName());
		
		
		// debug
		GlobalConsole.println("Created view panel of type " + viewPanel.getClass().getName());
		// end debug
		
		return viewPanel;
	}
	
	
	protected void enableDomainTypeButton(boolean enable)
	{
		domainTypeManagerButton.setEnabled(enable);
	}


	class ScrollableMultiViewContainer extends ScrollableMultiHierarchyPanel
		implements PanelManager.Multi_NodeView_Container
	{
		ScrollableMultiViewContainer(String name) { super(name); }
		
		public Set<NodeView> getNodeViews(String nodeId)
		{
			Set<NodeView> nodeViews = new HashSet<NodeView>();
			List<TreeTableComponent> ttcs = getMultiHierarchyPanel().getTreeTableComponents();
			for (TreeTableComponent ttc : ttcs)
			{
				if (ttc instanceof NodeView)
				{
					NodeView nodeView = (NodeView)ttc;
					if ((nodeId == null) || nodeView.getOutermostNodeId().equals(nodeId))
						nodeViews.add(nodeView);
				}
			}
			
			return nodeViews;
		}
		
		public Set<View> getViews(String nodeId)
		{
			Set<View> views = new HashSet<View>();
			List<TreeTableComponent> ttcs = getMultiHierarchyPanel().getTreeTableComponents();
			for (TreeTableComponent ttc : ttcs)
			{
				if (ttc instanceof View)
				{
					View view = (View)ttc;
					List<String> ids = view.getOutermostNodeIds();
					for (String id : ids)
					{
						if ((nodeId == null) || id.equals(nodeId))
						{
							views.add(view);
							break;
						}
					}
				}
			}
			
			return views;
		}
	
	
		public Set<View> getViews()
		{
			return getViews(null);
		}
		
		
		public int getNoOfViews()
		{
			Set<View> views = new HashSet<View>();
			List<TreeTableComponent> ttcs = getMultiHierarchyPanel().getTreeTableComponents();
			int count = 0;
			for (TreeTableComponent ttc : ttcs)
			{
				if (ttc instanceof View)
				{
					View view = (View)ttc;
					List<String> ids = view.getOutermostNodeIds();
					count += ids.size();
				}
			}
			
			return count;
		}
		
		
		public Component getComponent() { return this; }
		
		
		public Container getContainer() { return this; }
		
		
		public JComponent getJComponent() { return this; }
	}
	

	
  /* ***************************************************************************
   * From VisualComponent.DomainSpaceVisual.
   */


	public synchronized void domainCreated(String domainId)
	{
		domainSelector.domainCreated(domainId);
	}
	
	
	public synchronized void motifCreated(String motifId)
	{
		//domainSelector.domainCreated(motifId);
	}
	

	public void showScenarioCreated(String scenarioId)
	{
		domainSelector.showScenarioCreated(scenarioId);
	}
	
	
	public void showScenarioSetCreated(String scenarioSetId)
	{
		domainSelector.showScenarioSetCreated(scenarioSetId);
	}
	
	
	
  /* ***************************************************************************
   * From VisualComponent.ModelContainerVisual.
   */


	//public synchronized void modelElementAdded(String nodeId)
	//{
	//}
}

