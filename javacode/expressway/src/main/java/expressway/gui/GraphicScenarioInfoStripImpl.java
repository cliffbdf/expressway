/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.gui.InfoStrip.*;


public class GraphicScenarioInfoStripImpl extends ScenarioInfoStripImpl
	implements GraphicScenarioInfoStrip
{
	private GraphicInfoStripHelper helper;
	
	
	public GraphicScenarioInfoStripImpl(GraphicNodeScenarioPanel panel)
	{
		super(panel);  // super(NodeScenarioPanel)
		helper = new GraphicInfoStripHelper(this);
	}
}

