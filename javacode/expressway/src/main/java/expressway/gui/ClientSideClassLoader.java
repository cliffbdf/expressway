/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui;

import expressway.common.ModelEngineRMI;
import expressway.common.ModelAPITypes.*;
import javax.swing.ImageIcon;
import java.io.IOException;
import java.io.File;


public class ClientSideClassLoader extends ClassLoader
{
	private ModelEngineRMI modelEngine;
	
	
	public ClientSideClassLoader(ModelEngineRMI modelEngine, ClassLoader parent)
	{
		super(parent);
		this.modelEngine = modelEngine;
	}
	
	
	public Class findClass(String name)
	throws
		ClassNotFoundException
	{
		String pathWithinMotifJar = name.replace('.', File.separatorChar) + ".class";
		byte[] b = null;
		try { b = getMotifClass(pathWithinMotifJar); }
		catch (Exception ex) { throw new ClassNotFoundException("Error", ex); }
			
		return defineClass(name, b, 0, b.length);
	}
	
	
	public Class loadClass(String name, boolean resolve)
	throws
		ClassNotFoundException
	{
		return super.loadClass(name, resolve);
	}
	
	
	public byte[] getMotifClass(String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException
	{
		return getMotifClass(modelEngine, pathWithinMotifJar);
	}
	
	
	public byte[] getMotifResource(String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException
	{
		return getMotifResource(modelEngine, pathWithinMotifJar);
	}
	
	
	/** Override to make public. */
	
	public Package getPackage(String name) { return super.getPackage(name); }
	
	
	
	/* *************************************************************************
	 * Static methods.
	 */
	
	public static byte[] getMotifResource(ModelEngineRMI modelEngine, 
		String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException
	{
		return modelEngine.getMotifResource(pathWithinMotifJar);
	}


	public static byte[] getMotifClass(ModelEngineRMI modelEngine, 
		String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException
	{
		return modelEngine.getMotifClass(pathWithinMotifJar);
	}
}


