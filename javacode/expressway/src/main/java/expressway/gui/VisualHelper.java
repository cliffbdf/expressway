/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.help.*;
import expressway.generalpurpose.HTMLWindow;
import java.net.URL;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;


/**
 * Functionality that is provided by Visuals of many different Domain types.
 */
 
public class VisualHelper
{
	/*public static void refreshChildren(VisualComponent visual)
	throws
		ParameterError,
		Exception
	{
		 // Recreate each immediate sub-Component of the Visual. If a sub-Component
		 // is not appropriate for the View (e.g., this Visual is iconified), then
		 // do not display the sub-Component. Retain visibility state for any controls
		 // that are displayed in any overlays (e.g., in a ControlPanel), even if
		 // the controls pertain to an immediate or deeper sub-Component.
	 
		String[] childNodeIds = visual.getNodeSer().getChildNodeIds();
		for (String childNodeId : childNodeIds)
		{
			ClientView clientView = visual.getClientView();
			Set<VisualComponent> childInstances = clientView.identifyVisualComponents(childNodeId);
			
			for (VisualComponent child : children)
			{
				child.refreshChildren();
				
				// Remove and then re-add the child. Must be added in sequence.
				VisibilityControl vc = visual.getVisibilityControlFor(child);
				boolean isVisible = vc.getShow();
				clientView.removeVisual(child);
				child = clientView.addNode(childNodeId);
				vc = visual.getVisibilityControlFor(child);
				vc.setShow(isVisible);
			}
		}
	}*/
	
	
	/**
	 * Add a "What is this?" MenuItem to the specified Popup Menu.
	 * This method is overridden by the PortedContainerVisualJ class so that it
	 * can provide a "What is this?" MenuItem for a Port-State binding, which is
	 * not an actual VisualComponent but rather is merely rendered within its
	 * PortedContainerVisualJ.
	 */
	 
	public static void addWhatIsThisMenuItem(final Component component, 
		final JPopupMenu containerPopup,
		final VisualComponent visual, final int x, final int y)
	{
		JMenuItem menuItem = new JMenuItem("What is this?");
		menuItem.setBackground(Color.yellow);
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					HTMLWindow w;
					
					NodeSer nodeSer = visual.getNodeSer();
					
					String htmlDesc = nodeSer.getHtmlDescription();
					if (htmlDesc == null)
					{
						w = new HelpWindow("What is this?");
						URL url = new URL(HelpWindow.createURL(visual.getDescriptionPageName()));
						w.setURL(url);
					}
					else
					{
						View view = (View)(visual.getClientView());
						w = new DescriptionEditor("What is this?", nodeSer,
							view.getViewFactory(), view.getPanelManager(),
							view.getModelEngine());
						
						String htmlText = "<html><br>" + htmlDesc;
						
						if (nodeSer instanceof TemplateInstanceSer)
						{
							String templateName = 
								((TemplateInstanceSer)nodeSer).getTemplateFullName();
							
							if (templateName != null)
							{
								htmlText += "<br><br>";
								htmlText += "An instance of " + templateName;
							}
						}
						
						htmlText += "<br></html>";
						
						w.setText(htmlText);
					}
					
					w.setLocation(x, y);
					w.show();
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(component, ex);
				}
			}
		});
		
		containerPopup.add(menuItem);
	}
	
	
	/**
	 * Add menu items for managing the Named References that the Node depicted
	 * by the Visual is linked to.
	 */
	 
	public static void addCrossRefMenuItem(final Component component,
		final JPopupMenu containerPopup, final VisualComponent visual,
		final int x, final int y)
	{
		// Add a menu item for displaying the Named References that the Node depicted
		// by the Visual is linked to (via CrossRefrerences).
		
		JMenuItem menuItem = new JMenuItem("Show references...");
		menuItem.setBackground(Color.yellow);
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					NodeSer nodeSer = visual.getNodeSer();
					if (nodeSer instanceof CrossRefableSer)
					{
						CrossRefableSer crSer = (CrossRefableSer)nodeSer;
						String[][] crossRefs = crSer.getCrossRefs();
						
						JPopupMenu newPopup = new JPopupMenu();
						
						for (String[] crossRef : crossRefs)
						{
							String refName = crossRef[0];
							String fromId = crossRef[1];
							String toId = crossRef[2];
							
							char arrowChar;
							final String otherNodeId;
							if (fromId.equals(nodeSer.getNodeId()))
							{
								arrowChar = '\u2192';  // forward-arrow
								otherNodeId = toId;
							}
							else
							{
								arrowChar = '\u2190';  // back-arrow
								otherNodeId = fromId;
							}
							
							NodeSer otherNodeSer = 
								visual.getClientView().getModelEngine().getNode(otherNodeId);
							
							HelpfulMenuItem mi = new HelpfulMenuItem(
								refName + arrowChar + otherNodeSer.getFullName(),
								false,
								"Click to view the related Node");
							
							mi.addActionListener(new ActionListener()
							{
								public void actionPerformed(ActionEvent e)
								{
									// Open a View of the other Node.
									
									try { ((View)(visual.getClientView())).getViewFactory()
										.createViewPanel(true, otherNodeId); }
									catch (Exception ex)
									{
										ErrorDialog.showReportableDialog(component, ex);
									}
								}
							});
							
							newPopup.add(mi);
						}
						
						newPopup.show(component, x, y);
					}
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(component, 
						"While adding cross reference menu", ex);
				}
			}
		});
	}
}

