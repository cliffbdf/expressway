/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.ClientModel.*;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.awt.Color;
import java.util.Date;
import java.util.Set;



public interface GraphicVisualComponent extends VisualComponent
{
	/** ************************************************************************
	 * Return the View that owns this VisualComponent.
	 */
	 
	GraphicView getGraphicView();


	/**
	 * Set the size of this Visual within its parent's View space, based on
	 * the current NodeSer data regarding the associated Node's size within its 
	 * parent Node.
	 */
	 
	void setSize()
	throws
		Exception;
	
	
	/**
	 * Sets the location of the Visual, taking account of whether it belongs to
	 * a DisplayArea or a ControlPanel.
	 */
	 
	void setLocation()
	throws
		Exception;
	
	
	/* *************************************************************************
	 * Transform a coordinate in a Node's coordinate space to a coordinate in 
	 * the space of the Visual that depicts the Node in the View.
	 * Any visual borders are accounted for, so that  the caller need not be 
	 * concerned with border sizes.
	 * 
	 * The argument is always in the coordinate space of the Node.
	 * The result is always in the coordinate space of the Visual.
	 */
	 
	int transformNodeXCoordToView(double modelXCoord)
	throws
		ParameterError;  // if the conversion cannot be performed.
	
	
	int transformNodeYCoordToView(double modelYCoord)
	throws
		ParameterError;  // if the conversion cannot be performed.
		
		
	/** Transform a distance magnitude. Directionality (sign) is not altered. */
	int transformNodeDXToView(double nodeWidth)
	throws
		ParameterError;  // if the conversion cannot be performed.
		
		
	/** Transform a distance magnitude. Directionality (sign) is not altered. */
	int transformNodeDYToView(double nodeHeight)
	throws
		ParameterError;  // if the conversion cannot be performed.
	
	
	
  /* ***************************************************************************
	 * Same functionality as the above, but in the reverse direction.
	 */
	 
	double transformViewXCoordToNode(int viewXCoord)
	throws
		ParameterError;  // if the conversion cannot be performed.
	
	
	double transformViewYCoordToNode(int viewYCoord)
	throws
		ParameterError;  // if the conversion cannot be performed.
		
		
	/** Transform a distance magnitude. Directionality (sign) is not altered. */
	double transformViewDXToNode(int viewWidth)
	throws
		ParameterError;  // if the conversion cannot be performed.
		
		
	/** Transform a distance magnitude. Directionality (sign) is not altered. */
	double transformViewDYToNode(int viewHeight)
	throws
		ParameterError;  // if the conversion cannot be performed.
	
	
	
  /* ***************************************************************************
	 * Methods that modify data on the server.
	 */
	 
	/** ************************************************************************
	 * Modify the size of the Node that is depicted by the specified GraphicVisual.
	 * The server is updated. The Visual is later updated via a Notice sent from
	 * the server.
	 */
	 
	void resizeNode(double dw, double dh, double childShiftX,
		double childShiftY, boolean notify)
	throws
		Exception;
		
		
	/** ************************************************************************
	 * Modify the location of the Node that is depicted by the specified GraphicVisual.
	 * The server is updated. The Visual is later updated via a Notice sent from
	 * the server.
	 */
	 
	void moveNode(double dx, double dy, boolean notify)
	throws
		Exception;
}

