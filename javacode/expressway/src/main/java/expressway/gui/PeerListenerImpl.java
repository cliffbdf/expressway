/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent;
import expressway.common.VisualComponent.*;
import expressway.common.ClientModel.*;
import expressway.generalpurpose.*;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.SortedSet;
import java.util.Vector;
import javax.swing.JOptionPane;


/*
	Note: The code below should not assume that only Views have VisualComponents.
	May need to define View and NodeView separately, so that a View has Visuals
	but can represent more than one Domain and need not have an outermost Visual,
	whereas a NodeView does.
*/
	
	
	
/** ****************************************************************************
 * The Peer Listener for this View. (See design slide "PeerListener Pattern".)
 * Listens for notifcation messages from a ModelEngine, and relays those messages
 * to the visual counterparts of the server components identified in the messages.
 * This implementation implements a design for removing redundant server messages,
 * in order to prevent the GUI from refreshing unnecessarily. See the slide
 * "Paring Redundant Callbacks".
 */
 
public class PeerListenerImpl extends UnicastRemoteObject
	implements PeerListener, Runnable
{
	private ViewPanelBase viewPanelBase;
	
	/** Last item is the most recent. */
	private List<PeerNotice> queue = null;
	
	/** The maximum range of applicability of all PeerNotices contained within
		the work List. */
	private Scope scope;
	
	private String id;
	
	
	public PeerListenerImpl(ViewPanelBase viewPanel, String viewName) throws RemoteException
	{
		super();
		
		this.viewPanelBase = viewPanel;
		this.id = viewName;
		
		queue = new Vector<PeerNotice>();
		Thread thread = new Thread(this);
		thread.start();
	}
	
	
	public String getId() throws IOException { return id; }
	
	
	/**
	 * The "producer" in a producer/consumer pattern. This is called to put
	 * PeerNotices on this View's queue. The server calls this method remotely.
	 */
	 
	public void notify(PeerNotice notice)
	throws
		NotInterested,
		InconsistencyError,
		IOException
	{
		if (notice == null) throw new RuntimeException("Notice is null");
		synchronized (queue)
		{
			if (ClientGlobalValues.PrintIncomingPeerNotices)
				GlobalConsole.println(viewPanelBase.getName() + 
					": Incoming PeerNotice: " + notice.toString());
			
			queue.add(notice);  // Enqueue the PeerNotice.
			queue.notifyAll();  // Inform the waiting Thread that there is a Notice.
		}
	}
	
	
	/**
	 * The "consumer" in a producer/consumer pattern.
	 */
	 
	public void run()
	{
		while (true)
		{
			try { perform(); }
			catch (Throwable t)
			{
				GlobalConsole.printStackTrace(t);
				JOptionPane.showMessageDialog(viewPanelBase, t.getMessage(),
					"Internal Error in server listener", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	
	public void perform()
	{
		while (true)
		{
			List<PeerNotice> workList = new Vector<PeerNotice>();
			
			synchronized (queue)
			{
				if (queue.size() == 0) try
				{
					queue.wait();  // Wait for a Notice.
				}
				catch (InterruptedException ex)
				{
					GlobalConsole.printStackTrace(ex);
					return;
				}
				
				try { Thread.currentThread().sleep(100); }  // Give server a chance
													// to send more Notices.
				catch (InterruptedException ex)
				{
					System.out.println("PeerListener Thread interrupted.");
					ex.printStackTrace();
					return;
				}
				
				workList.addAll(queue);  // Copy queue to a work list.
				queue.clear();  // Clear queue.
			}
			
			
			// Reset the scope.
			
			scope = new Scope();
			
			
			// For each Notice in work list, starting with newest:
			
			for (int i = workList.size()-1; (workList.size() > 0) && (i >= 0); i--)
			{
				if (! scope.addToScope(workList.get(i)))
				{
					workList.remove(i);
				}
			}
			
			
			// Execute each Notice in the work list, starting with oldest.
			
			//SortedSet<PeerNotice> workSet = new java.util.TreeSet<PeerNotice>(workList);
			SortedSet<PeerNotice> workSet = new TreeSetNullDisallowed<PeerNotice>(workList);
			
			for (PeerNotice notice : workSet)
			{
				try
				{
					this.execute(notice);
				}
				catch (NotInterested ni)
				{ // TBD: notify the server.
				}
				catch (InconsistencyError ie)
				{
					ie.printStackTrace();
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
				}
				catch (RuntimeException re)
				{
					re.printStackTrace();
					ErrorDialog.showReportableDialog(null,
						ThrowableUtil.getAllMessages(re), "Error", re);
				}
			}
		}
	}
	
	
	/**
	 * The maximum range of applicability of all PeerNotices contained within
	 * the work List.
	 */
	 
	class Scope
	{
		private Set<VisualComponent> scopeVisuals = new HashSet<VisualComponent>();
		
		
		/**
		 * Return true if this Scope contains a Visual that is identical to
		 * or contains the specified Visual.
		 */
		 
		synchronized boolean covers(VisualComponent visual)
		{
			for (VisualComponent scopeVisual : scopeVisuals)
			{
				if (scopeVisual == visual) return true;
				
				try { if (scopeVisual.contains(visual)) return true; }
				catch (VisualComponent.NoNodeException ex) {} // fall through
			}
			
			return false;
		}
		

		/**
		 * Return true if any Visuals that depict the Notice's Node are added to
		 * the scope, indicating that the Notice should be executed.
		 *
		 * A Notice's scope depends on the type of Notice. For a NodeChangedNotice
		 * (which causes a deep refresh of the Node), the scope is the Visual Component
		 * identified by the NodeId, including all of its sub-Components (if any).
		 */
		 
		synchronized boolean addToScope(PeerNotice notice)
		{
			String nodeId = notice.getPeerId();
			if (nodeId == null)
				if (notice.isAlwaysForAVisual()) return false;
				else return true;
			
			if (notice instanceof PeerNoticeBase.ScenarioCreatedNotice) return true;

			Set<VisualComponent> noticeVisuals = null;
			try { noticeVisuals = viewPanelBase.identifyVisualComponents(nodeId); }
			catch (Exception ex)
			{
				ex.printStackTrace();
				return true;
			}

			if (noticeVisuals == null) return false;

			if (notice instanceof PeerNoticeBase.NodeChangedNotice)
			{
				// Determine if each Visual affected by the Notice is covered
				// by the current Scope.
				
				boolean mustAddToScope = false;
				for (VisualComponent noticeVisual : noticeVisuals)
					// each Visual affected by Notice
				{
					if (! this.covers(noticeVisual))
					{
						mustAddToScope = true;
						break;
					}
				}
				
				if (mustAddToScope) // for each Visual affected by Notice
				for (VisualComponent noticeVisual : noticeVisuals)
				{
					// Add noticeVisual to current Scope
					this.scopeVisuals.add(noticeVisual);
				}
				
				return mustAddToScope;
			}
			else
			{
				this.scopeVisuals.addAll(noticeVisuals);
				return true;  // keep notice
			}
		}
	}
	
	
	/**
	 * Perform the actual work required by a PeerNotice. The work must lock this
	 * View to ensure that its various steps are consistent.
	 */
	 
	protected synchronized void execute(final PeerNotice notice)
	throws
		NotInterested,
		InconsistencyError,
		IOException
	{
		if (ClientGlobalValues.PrintExecutingPeerNotices)
			GlobalConsole.println(viewPanelBase.getName() + 
				": Executing PeerNotice: " + notice.toString());

		if (notice.getPeerId() == null)
		{
			ThreadSafeWrapper runnable = new ThreadSafeWrapper(notice);
			javax.swing.SwingUtilities.invokeLater(runnable);
		}
		else
		{
			// if the notice identifies a Domain and the notice pertains
			// to scenario creation, deletion, then there might not be a visual
			// for the domain, but if the view is a NodeView, we need to make
			// sure that the NodeView's scenarioCreated/Deleted method gets
			// called.
			
			// Identify Visual Components.
			
			Set<VisualComponent> visComps = null;
			
			try { visComps = viewPanelBase.identifyVisualComponents(notice.getPeerId()); }
			catch (ParameterError ex)
			{
				ErrorDialog.showReportableDialog(viewPanelBase, ex);
				return;
			}

			boolean viewIsInterested = false;
				
			if (viewPanelBase instanceof NodeView)
			{
				if (notice.getPeerId().equals(((NodeView)viewPanelBase).getOutermostNodeId()))
				{
					// The Notice pertains to the Domain that owns the Node depicted
					// by the outermost Visual.
					viewIsInterested = true;
					ThreadSafeWrapper runnable;
					if (viewPanelBase instanceof VisualComponent)
						runnable = new ThreadSafeWrapper(notice, (VisualComponent)viewPanelBase);
					else
						runnable = new ThreadSafeWrapper(notice);
					javax.swing.SwingUtilities.invokeLater(runnable);
				}
			}
			
			if ((visComps == null) || (visComps.size() == 0))
			{
				if (! viewIsInterested) throw new NotInterested();
			}
		
		
			// Notify Visual Components.
			
			if (ClientGlobalValues.PrintTargetIdentifiedPeerNotices)
				GlobalConsole.println(viewPanelBase.getName() + 
					": PeerNotice targets identified for Notice " + notice.toString() +
					":");
			
			for (VisualComponent visComp : visComps)
			{
				if (ClientGlobalValues.PrintTargetIdentifiedPeerNotices)
					GlobalConsole.println("\t" + visComp.getFullName() + " (" +
						visComp.getClass().getName() + ")");
				
				ThreadSafeWrapper runnable = new ThreadSafeWrapper(notice, visComp);
				javax.swing.SwingUtilities.invokeLater(runnable);
			}
		}
	}
	
	
	/**
	 * For passing to the SwingUtilities.invokeLater method, which requires a
	 * Runnable. The invokeLater method must be used for asynchronous calls to 
	 * the GUI, in order to serialize GUI actions and prevent deadlock.
	 */
	 
	class ThreadSafeWrapper implements Runnable
	{
		private PeerNotice notice;
		private VisualComponent visComp = null;
		
		
		public ThreadSafeWrapper(PeerNotice notice)
		{
			this.notice = notice;
		}
		
		
		public ThreadSafeWrapper(PeerNotice notice, VisualComponent visComp)
		{
			this.notice = notice;
			this.visComp = visComp;
			if (visComp == null) throw new RuntimeException("No Visual specified");
		}
		
		
		public void run()
		{
			try { perform(); }
			catch (Throwable t)
			{
				GlobalConsole.printStackTrace(t);
				JOptionPane.showMessageDialog(viewPanelBase, t.getMessage(),
					"Internal Error in server listener", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		
		public void perform()
		{
			if (visComp == null)
			{
				if
				(! (
					(notice instanceof PeerNoticeBase.DomainCreatedNotice)
					||
					(notice instanceof PeerNoticeBase.ScenarioCreatedNotice)
					||
					(notice instanceof PeerNoticeBase.ScenarioSetCreatedNotice)
					||
					(notice instanceof PeerNoticeBase.MotifCreatedNotice)
					||
					(notice instanceof PeerNoticeBase.ImportErrorNotice)
					||
					(notice instanceof PeerNoticeBase.ImportWarningNotice)
					||
					(notice instanceof PeerNoticeBase.ScenarioSimErrorMessageNotice)
				   )
				)
				{
					return;
				}
				
				if (notice instanceof PeerNoticeBase.DomainCreatedNotice)
				{
					// New Domain created: Notify the View, which is a DomainSpaceVisual.
					
					try
					{
						((PeerNoticeBase.DomainCreatedNotice)notice).notify(viewPanelBase);
					}
					catch (Throwable t)
					{
						t.printStackTrace();
						ErrorDialog.showReportableDialog(viewPanelBase,
							ThrowableUtil.getAllMessages(t), "Error", t);
					}
				}
				else if (notice instanceof PeerNoticeBase.ScenarioCreatedNotice)
				{
					try
					{
						((PeerNoticeBase.ScenarioCreatedNotice)notice).notify(viewPanelBase);
					}
					catch (Throwable t)
					{
						t.printStackTrace();
						ErrorDialog.showReportableDialog(viewPanelBase,
							ThrowableUtil.getAllMessages(t), "Error", t);
					}
				}
				else if (notice instanceof PeerNoticeBase.ScenarioSetCreatedNotice)
				{
					try
					{
						((PeerNoticeBase.ScenarioSetCreatedNotice)notice).notify(viewPanelBase);
					}
					catch (Throwable t)
					{
						t.printStackTrace();
						ErrorDialog.showReportableDialog(viewPanelBase,
							ThrowableUtil.getAllMessages(t), "Error", t);
					}
				}
				else if (notice instanceof PeerNoticeBase.MotifCreatedNotice)
				{
					// New Motif created: Notify the View, which is a DomainSpaceVisual.
					
					try
					{
						((PeerNoticeBase.MotifCreatedNotice)notice).notify(viewPanelBase);
					
						//JOptionPane.showMessageDialog(viewPanelBase,
						//	"Motif created",
						//	"Information", JOptionPane.INFORMATION_MESSAGE);
					}
					catch (Throwable t)
					{
						t.printStackTrace();
						
						ErrorDialog.showReportableDialog(viewPanelBase,
							ThrowableUtil.getAllMessages(t), "Error", t);
					}
				}
				else if (notice instanceof PeerNoticeBase.ImportErrorNotice)
				{
					JOptionPane.showMessageDialog(viewPanelBase,
						((PeerNoticeBase.ImportErrorNotice)notice).getImportError(),
						"Error", JOptionPane.ERROR_MESSAGE);
				}
				else if (notice instanceof PeerNoticeBase.ImportWarningNotice)
				{
					JOptionPane.showMessageDialog(viewPanelBase,
						((PeerNoticeBase.ImportWarningNotice)notice).getImportWarning(),
						"Warning", JOptionPane.WARNING_MESSAGE);
				}
				else if (notice instanceof PeerNoticeBase.ScenarioSimErrorMessageNotice)
				{
					PeerNoticeBase.ScenarioSimErrorMessageNotice simErrorNotice =
						(PeerNoticeBase.ScenarioSimErrorMessageNotice)notice;
						
					if (viewPanelBase instanceof SimulatableView)
					{
						Set<SimulationVisual> svs = 
							((SimulatableView)viewPanelBase).getSimulationVisuals(simErrorNotice.requestId);
						for (SimulationVisual sv : svs)
						{
							sv.errorReportedForSim(simErrorNotice.simNodeId, simErrorNotice.msg);
						}
					}
					else
						JOptionPane.showMessageDialog(viewPanelBase,
							"Internal error: simulation results for panel that is not simulatable",
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			else
			{
				if (notice instanceof PeerNoticeBase.DomainCreatedNotice)
					throw new RuntimeException("Should not happen");
				
				
				
				//Timer timer = new Timer();
				//final Thread threadToInterrupt = Thread.currentThread();
					
				//TimerTask timerTask = new TimerTask()
				//{
				//	public void run()
				//	{
				//		threadToInterrupt.interrupt();
				//	}
				//};

				try
				{
					// Add a timeout.
				//	timer.schedule(timerTask, MsAllowedForNotifyToComplete);
						// interrupt after MsAllowedForNotifyToComplete ms.
				
					notice.notify(visComp);
				}
				catch (InconsistencyError ie)
				{
					//timerTask.cancel();
					ErrorDialog.showThrowableDialog(viewPanelBase,
						ThrowableUtil.getAllMessages(ie),
						"Check " + visComp.getName(), ie);
				}
				catch (ClassCastException cce)
				{
					//timerTask.cancel();
					ErrorDialog.showReportableDialog(viewPanelBase,
						"Server sent a refresh notice for a visual component and it " +
						"was the wrong type; " + 
						ThrowableUtil.getAllMessages(cce),
						"Internal server error", cce);
				}
				catch (Throwable t)
				{
					//timerTask.cancel();
					t.printStackTrace();
					ErrorDialog.showReportableDialog(viewPanelBase, "Error", t);
				}
				finally
				{
					//timerTask.cancel();
					//timer.purge();
					Thread.interrupted();  // clear interrupt status.
				}
				
				//timer.cancel();
			}
		}
	}

}

