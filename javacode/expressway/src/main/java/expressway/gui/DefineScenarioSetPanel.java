/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.generalpurpose.*;


public class DefineScenarioSetPanel extends JFrame
{
	private final String attrId;
	private final String baseScenarioId;
	private final ModelEngineRMI modelEngine;
	
	private JLabel descriptionLabelA;
	private JLabel descriptionLabelB;
	private JLabel descriptionLabelC;
	private JTextField lowerBoundTextField;
	private JTextField upperBoundTextField;
	private JTextField incrementTextField;
	private JButton createButton;
	private JButton cancelButton;
	
	
	public DefineScenarioSetPanel(final String baseModelScenarioId, AttributeVisual attrVisual)
	{
		super("Define Scenario Set for " + attrVisual.getName());
		this.setAlwaysOnTop(true);
		
		try { this.attrId = attrVisual.getNodeId(); }
		catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }
		
		this.baseScenarioId = baseModelScenarioId;
		this.modelEngine = attrVisual.getClientView().getModelEngine();
		
		this.setSize(400, 200);
		
		this.setLayout(new FlowLayout());
		
		add(descriptionLabelA = new JLabel("Attribute value will range from "));
		add(lowerBoundTextField = new JTextField());
		add(descriptionLabelB = new JLabel(" to "));
		add(upperBoundTextField = new JTextField());
		add(descriptionLabelC = new JLabel("in increments of "));
		add(incrementTextField = new JTextField());
		
		add(createButton = new JButton("Create"));
		add(cancelButton = new JButton("Cancel"));
		
		lowerBoundTextField.setColumns(2);
		//lowerBoundTextField.setSize(20, 14);
		
		upperBoundTextField.setColumns(2);
		//upperBoundTextField.setSize(20, 14);
		
		incrementTextField.setColumns(2);

		lowerBoundTextField.setText("0");
		upperBoundTextField.setText("0");
		incrementTextField.setText("0");
		
		createButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				double lowerBound;
				double upperBound;
				double increment;
				
				String text = null;
				
				try
				{
					text = lowerBoundTextField.getText();
					lowerBound = Double.parseDouble(text);
					
					text = upperBoundTextField.getText();
					upperBound = Double.parseDouble(text);
					
					text = incrementTextField.getText();
					increment = Double.parseDouble(text);
				}
				catch (NumberFormatException nfe)
				{
					JOptionPane.showMessageDialog(DefineScenarioSetPanel.this,
						"Not numeric: " + text, "Error", JOptionPane.ERROR_MESSAGE);
						return;
				}
				
				if (lowerBound >= upperBound)
				{
					JOptionPane.showMessageDialog(DefineScenarioSetPanel.this,
						"Lower bound must be less than upper bound", "Error", 
						JOptionPane.ERROR_MESSAGE); return;
				}
				
				if (increment <= 0.0)
				{
					JOptionPane.showMessageDialog(DefineScenarioSetPanel.this,
						"Increment must be positive", "Error", JOptionPane.ERROR_MESSAGE);
						return;
				}
				
				int noOfScenarios = (int)((upperBound - lowerBound) / increment) + 1;
				if (noOfScenarios > 30)
				{
					JOptionPane.showMessageDialog(DefineScenarioSetPanel.this,
						(noOfScenarios +
						" Scenarios would be created: this exceeds the 'safety' limit."),
						"Error", JOptionPane.ERROR_MESSAGE); return;
				}
				
				ModelScenarioSetSer modelScenarioSetSer = null;
				try
				{
					/*
					// Get Attribute value in the context of the base Scenario.
					
					Serializable value = modelEngine.getAttributeValueById(
						DefineScenarioSetPanel.this.attrId, baseScenarioId);
					
					if (value == null)
					{
						...
					}
					
					if (! (value instanceof Number))
					{
						JOptionPane.showMessageDialog(DefineScenarioSetPanel.this,
							"Attribute's current value is not Numeric: it is a " +
							value.getClass().getName(), "Error", JOptionPane.ERROR_MESSAGE);
							return;
					}
					
					double doubleValue = ((Number)value).doubleValue();
					*/
					
					// Make copies of the Scenario.
					
					Serializable[] values = new Serializable[noOfScenarios];
					
					double attrValue = lowerBound;
					int i = 0;
					for (;;)
					{
						//if (attrValue > upperBound) break;
						if (i == noOfScenarios) break;
						
						values[i++] = attrValue;
						attrValue += increment;
					}
					
					modelScenarioSetSer = 
						modelEngine.createScenariosOverRange(
							DefineScenarioSetPanel.this.baseScenarioId,
							DefineScenarioSetPanel.this.attrId, values);
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(DefineScenarioSetPanel.this, ex);
						return;
				}
				
				JOptionPane.showMessageDialog(DefineScenarioSetPanel.this,
					(modelScenarioSetSer.scenarioIds.length + 
						" Scenarios created;\nSee Domain List View"),
					"Scenarios Created", JOptionPane.INFORMATION_MESSAGE);
				
				DefineScenarioSetPanel.this.dispose();
			}
		});
		
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				DefineScenarioSetPanel.this.dispose();
			}
		});
		
		this.show();
	}
}

