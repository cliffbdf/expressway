/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.InfoStrip.*;
import java.util.Set;
import java.util.Iterator;
import java.util.Vector;
import java.util.HashSet;
import java.awt.Insets;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.BorderLayout;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;


public class ScenarioViewPanelHelper implements View.ViewHelper
{
	public static final int SelectionStripHeight = 85;

	private NodeScenarioView viewPanel;
	
	private NodeView superViewPanel;
	
	/** If true, make the current Scenario visible. Otherwise only depict the
		underlying Model Domain. */
	private boolean showScenarios = false;
	
	/** A Tabbed Pane for selecting which Scenario to show, as well as information
		about the currently displayed Scenario. */
	private ScenarioSelectorImpl scenarioSelector;
	
	
	public ScenarioViewPanelHelper(NodeScenarioView viewPanel, NodeView superViewPanel)
	{
		this.viewPanel = viewPanel;
		this.superViewPanel = superViewPanel;
	}
	
	
	public ScenarioSelectorImpl getScenarioSelector() { return scenarioSelector; }
	
	
	
  /* ***************************************************************************
   * Methods from ViewPanelBase.
   */
	
	
	public synchronized void postConstructionSetup()
	throws
		Exception
	{
		// Add a Scenario selection strip to the value Panel. The strip also
		// displays information about the current selected Scenario.
		
		resizeScenarioSelectorImpl();
		//viewPanel.computeDisplayAreaLocationAndSize(false);
		
		
		// Instantiate the Visuals.
		superViewPanel.postConstructionSetup();
	}	
	
	
	public synchronized Set<VisualComponent> addNode(String nodeId)
	throws
		Exception
	{
		Set<VisualComponent> visuals = (Set<VisualComponent>)(superViewPanel.addNode(nodeId));
			// This returns the Visuals in this View that are added to depict the
			// specified Node.
	
		if (visuals == null) return visuals;
			
		// Update the NodeScenarioPanels.
		for (VisualComponent visual : visuals)
		{
			addOrRemoveAttributeControls(visual);
		}
				
		return visuals;
	}
	
	
	public synchronized void removeVisual(VisualComponent visual)
	throws
		Exception
	{
		// Remove dependent Components from each NodeScenarioPanel.
		deleteControls(visual.getNodeId());
		
		superViewPanel.removeVisual(visual);
			// Removes the specified Visual from this View.
			// Unsubscribes it from server events if necessary.
			// Removes child Visuals owned by this ViewPanel.
		

		// Do I need this?
		if (visual instanceof ScenarioVisual)
		{
			if (visual instanceof NodeScenarioPanel)
			{
				this.removeScenarioVisual((NodeScenarioPanel)visual);
				return;
			}
		}
	}
	
	
	public synchronized void notifyScenarioNameChanged(ScenarioVisual scenVis,
		String newName)
	{
		NodeScenarioPanel panel = getScenarioPanel(scenVis.getNodeId());
		if (panel != null) panel.getInfoStrip().update();
		scenarioSelector.setScenarioName((NodeScenarioPanel)scenVis, newName);
	}


	public Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError
	{
		return superViewPanel.identifyVisualComponents(nodeId);
	}
	
	
	public synchronized VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError
	{
		Set<NodeScenarioPanel> sps = viewPanel.getScenarioPanels();
		for (NodeScenarioPanel sp : sps)
		{
			VisualComponent matchingVisual = sp.identifyVisualComponent(nodeId);
			if (matchingVisual != null) return matchingVisual;
		}
		
		try { return (VisualComponent)
			(superViewPanel.identifyVisualComponent(nodeId)); }
		catch (ParameterError pe) { throw pe; }
		catch (Exception ex) { throw new RuntimeException(ex); }
	}


	public synchronized Set<VisualComponent> identifyVisualComponents()
	throws
		ParameterError
	{
		Set<VisualComponent> visualComponents;
		try { visualComponents = (Set<VisualComponent>)
			(superViewPanel.identifyVisualComponents()); }
		catch (ParameterError pe) { throw pe; }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		Set<NodeScenarioPanel> attrValuePanels = viewPanel.getScenarioPanels();
		visualComponents.addAll(attrValuePanels);
		
		
		// Add SimulationVisuals (SimulationControlPanels), and include Visuals
		// owned by each NodeScenarioPanel
		
		Set<NodeScenarioPanel> scenarioPanels = viewPanel.getScenarioPanels();
		visualComponents.addAll(scenarioPanels);
		for (NodeScenarioPanel panel : scenarioPanels)
		{
			Set<ValueControl> acs = 
				panel.getValueControls(AttributeValueControl.class);
			for (ValueControl vc : acs)
			{
				AttributeValueControl ac = (AttributeValueControl)vc;
				AttributeVisual av = ac.getAttributeVisual();
				visualComponents.add(av);
			}
		}
		
		return visualComponents;
	}
	
	
	public ScenarioVisual getCurrentScenario()
	{
		return viewPanel.getCurrentScenarioPanel();
	}
	
	
	public ScenarioVisual getCurrentScenarioVisual() { return viewPanel.getCurrentScenarioPanel(); }
	
	
	public ScenarioVisual showScenario(String scenarioId)
	throws
		Exception
	{
		// debug
		GlobalConsole.println("Entered ScenarioViewPanelHelper.showScenario...");
		// end debug
		
		
		
		
		// Check if the Scenario is already present in the GUI. If so, do not
		// create a new one.
		
		Set<NodeScenarioPanel> attrValuePanels = viewPanel.getScenarioPanels(); 
		for (NodeScenarioPanel scenarioVisual : attrValuePanels)  // each Scenario View Panel
		{
			ScenarioSer scenarioSer = (ScenarioSer)(scenarioVisual.getNodeSer());
			
			if (scenarioSer.getNodeId().equals(scenarioId))  // Scenario matches Node Id.
			{
				viewPanel.setCurrentScenarioPanel(scenarioVisual.getNodeId());
				return scenarioVisual;
			}
		}
		
		
		// Obtain the Scenario's Ser from the server.
		
		ScenarioSer scenarioSer = null;
		try { scenarioSer = (ScenarioSer)(viewPanel.getModelEngine().getNode(scenarioId)); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(viewPanel.getComponent(), ex); return null;
		}
		
		
		// Create a Panel to depict the Attribute values for the Scenario.
		
		NodeScenarioPanel panel = (NodeScenarioPanel)(viewPanel.makeScenarioVisual(scenarioSer));
		addScenarioVisual(panel);
		try { panel.postConstructionSetup(); }
		catch (Exception ex) { ex.printStackTrace(); }
		
		
		// I have been unable for force the newly created NodeScenarioPanel
		// to repaint: therefore, its embedded controls are not visible until one
		// moves a Component 
		//this.invalidate();
		viewPanel.repaint();
		

		// Register for notifications from the server when the 
		// Scenario changes.
		
		if (viewPanel.getListenerRegistrar() != null)
			try { viewPanel.getListenerRegistrar().subscribe(scenarioId, true); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(viewPanel.getComponent(), ex);
			}

		
		return panel;
	}


	public ControlPanel getCurrentControlPanel()
	{
		return scenarioSelector.getCurrentScenarioPanel();
	}
	
	
	public synchronized void showScenarioCreated(String scenarioId)
	{
		// debug
		GlobalConsole.println("Entered ScenarioViewPanelHelper.showScenarioCreated...");
		// end debug
		
		
		
		try { viewPanel.showScenario(scenarioId); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(viewPanel.getComponent(), "Unable to show Scenario", ex);
		}
	}
	
	
	public synchronized void showScenarioDeleted(String scenarioId)
	{
		// Remove the Panel that depicts the specified Scenario.
		
		NodeScenarioPanel panel = viewPanel.getScenarioPanel(scenarioId);
		if (panel == null) return;
		
		viewPanel.removeScenarioVisual(panel);
	}
	
	
	public synchronized ScenarioVisual getScenarioVisual(String scenarioId)
	{
		return viewPanel.getScenarioPanel(scenarioId);
	}

	
	public synchronized void notifyThatDisplayAreaChanged(int x, int y, int width, int height)
	{
		// Repaint the area occupied by the Visual.
		viewPanel.getDisplayArea().repaint();
	}
	
	

  /* ***************************************************************************
   * Methods from ScenarioView.
   */

	
	public void deleteControls(String nodeId)
	{
		Set<NodeScenarioPanel> panels = viewPanel.getScenarioPanels();
		
		for (NodeScenarioPanel panel : panels)
		{
			panel.removeControls(nodeId);
		}
	}
	
	
	
  /* ***************************************************************************
   * Methods from java.awt.Component.
   */
	
	public synchronized void repaint()
	{
		superViewPanel.repaint();
		if (scenarioSelector != null) scenarioSelector.repaint();
	}
	
	
	
  /* *******************************************************************
   * Methods defined in DomainVisual.
   */
	 
	
	public void showScenarioSetCreated(String newScenSetId)
	{
		NodeSer nodeSer = null;
		try { nodeSer = getModelEngine().getNode(newScenSetId); }
		catch (Exception ex)
		{
			ex.printStackTrace(); return;
		}
		
		if (! (nodeSer instanceof ScenarioSetSer))
		{
			(new Exception("A " + nodeSer.getClass().getName() + 
				" was returned instead of a ScenarioSetSer")).printStackTrace();
			return;
		}
		
		ScenarioSetSer scenSetSer = (ScenarioSetSer)nodeSer;
		String[] scenarioIds = scenSetSer.getScenarioIds();
		for (String scenarioId : scenarioIds) try
		{
			viewPanel.showScenario(scenarioId);
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(viewPanel.getComponent(), "Unable to show Scenario", ex);
		}
	}


	public void loadAllScenarios()
	{
		// Get the Domain's Scenarios from the server.
		
		ScenarioSer[] scenarioSers = null;
		try { scenarioSers = getModelEngine().getScenarios(viewPanel.getOutermostNodeSer().getDomainName()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(viewPanel.getComponent(), ex); return;
		}


		// Create a Scenario Visual (NodeScenarioPanel) for each Scenario.
		
		View.VisualComponentDisplayArea displayArea = viewPanel.getDisplayArea();
		for (ScenarioSer scenarioSer : scenarioSers)
		{
			try { showScenario(scenarioSer.getNodeId()); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(viewPanel.getComponent(), ex);
			}
		}		
	}


	public java.util.List<ScenarioVisual> getScenarioVisuals()
	{
		return new Vector<ScenarioVisual>(viewPanel.getScenarioPanels());
	}
	
	

  /* ***************************************************************************
   * Methods from AbstractGraphicViewPanel.
   */
	
	
	public Set<VisualComponent> refreshVisuals(NodeSer node)
	throws
		Exception
	{
		Set<VisualComponent> refreshedVisuals = (Set<VisualComponent>)
			(superViewPanel.refreshVisuals(node.getNodeId()));
		
		if (! (node instanceof ScenarioSer)) return refreshedVisuals;  // done
		
		
		// Refresh any Scenarios that match the NodeSer.
		
		for (int tabNo = 1; tabNo < scenarioSelector.getTabCount(); tabNo++)
		{
			InfoStrip infoStrip =
				(InfoStrip)(scenarioSelector.getComponentAt(tabNo));
			
			if (infoStrip == null) continue;  // skip the fixed tabs.
			
			NodeScenarioPanel panel = ((ScenarioInfoStrip)infoStrip).getScenarioPanel();
			
			ScenarioSer scenarioSer = (ScenarioSer)(panel.getNodeSer());
			
			if (panel.getNodeSer().getNodeId().equals(node.getNodeId()))
			{
				panel.setNodeSer((ScenarioSer)node);
				panel.refresh();
				refreshedVisuals.add(panel);
			}
		}
		
		return refreshedVisuals;
	}
	
	
	
  /* ***************************************************************************
   * Implementation methods.
   */
	
   
	public void addOrRemoveAttributeControls(VisualComponent visual)
	{
		Set<NodeScenarioPanel> panels = viewPanel.getScenarioPanels();
		
		for (NodeScenarioPanel panel : panels)
		{
			try { panel.addOrRemoveAttributeControls(panel.attributeControlsAreVisible(), visual); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
	}
	
	
	/** ************************************************************************
	 * Toggle whether or not to display the current Scenario. If false, only
	 * the Model Domain is displayed. If true, the current Scenario is overlayed
	 * on the Model Domain. It is intended that this method be called by a menu
	 * selection or other visual control.
	 */
	 
	public void setShowScenarios(boolean show)
	//throws
	//	Exception
	{
		this.showScenarios = show;
		
		Iterator<ScenarioInfoStrip> iterator = 
			getScenarioSelector().getScenarioInfoStripIterator();
		while (iterator.hasNext())
		{
			ScenarioInfoStrip infoStrip = iterator.next();
			
			if (show && (scenarioSelector.isSelected(infoStrip)))
			{
				infoStrip.setVisible(show);
				infoStrip.getScenarioPanel().setVisible(true);
			}
			else
			{
				infoStrip.getScenarioPanel().setVisible(false);
			}
		}
	}
	
	
	/** ************************************************************************
	 * Return the current state of the toggle set by the setShowScenarios method.
	 */
	 
	public boolean getShowScenarios() { return showScenarios; }
	
	
	/** ************************************************************************
	 * Make the specified panel the currently selected Scenario Panel, and
	 * make it visible if this.showScenarios is true. If panel is null, then
	 * make no panel the current one.
	 */
	 
	public void setCurrentScenarioPanel(NodeScenarioPanel panel)
	{
		NodeScenarioPanel currentPanel = getCurrentScenarioPanel();
		
		if (panel == null)
		{
			if (currentPanel != null) currentPanel.setVisible(false);
			scenarioSelector.setCurrentScenarioPanel(null);
			return;
		}
		
		if (currentPanel != null) currentPanel.setVisible(showScenarios);
		
		scenarioSelector.setCurrentScenarioPanel(panel);
		
		panel.setVisible(showScenarios);
	}
	
	
	public void setCurrentScenarioPanel(String scenarioId)
	{
		NodeScenarioPanel sp = getScenarioPanel(scenarioId);
		setCurrentScenarioPanel(sp);
	}
	
	
	public void setCurrentScenarioPanel(ScenarioSer ser)
	{
		setCurrentScenarioPanel(ser.getNodeId());
	}
	
	
	/** ************************************************************************
	 * Return the current Scenario. May be null if no Scenario is currently visible.
	 */
	
	public NodeScenarioPanel getCurrentScenarioPanel()
	{
		if (scenarioSelector == null) return null;
		return scenarioSelector.getCurrentScenarioPanel();
	}
	
	
	public NodeScenarioPanel getScenarioPanel(String scenarioId)
	{
		return scenarioSelector.getScenarioPanelById(scenarioId);
	}
	
	
	public void notifyComponentRepainted(Component comp)
	{
		// Repain the ModelScenarioPanel if necessary.
		
		NodeScenarioPanel panel = viewPanel.getCurrentScenarioPanel();
		if (panel != null) panel.repaint();
	}
	
	
	/**
	 * If there is a current Model Scenario, return its Node Id. Otherwise return
	 * null.
	 */
	
	public String getCurrentScenarioId()
	{
		if (scenarioSelector == null) return null;
		
		NodeScenarioPanel panel = scenarioSelector.getCurrentScenarioPanel();
		
		if (panel == null) return null;
		
		return panel.getNodeSer().getNodeId();
	}
	
	
	public void dumpAttrPanels(String msg)  // for debug only
	{
		if (scenarioSelector == null) 
		{
			System.out.println("Scen sel strip is null");
			return;
		}
		
		System.out.println(msg + ": ScenarioSelectorImpl has these on " + 
			Thread.currentThread().getName());
		for (Iterator iter = scenarioSelector.getScenarioInfoStripIterator();
			iter.hasNext();)
		{
			ScenarioInfoStrip ist = (ScenarioInfoStrip)(iter.next());
			System.out.println("\t" + ist.getNameField().getText());
			NodeScenarioPanel p = ist.getScenarioPanel();
			System.out.println("\t\t" + p.getNodeSer().getName() + " " + p.hashCode());
			
			Container co = p.getParent();
			if (co == null) System.out.println("\t\tnull");
			else System.out.println("\t\t" + co.getName());
		}
	}
	
	
	/** ************************************************************************
	 * Create and instantiate a Scenario Selection Strip within this Panel.
	 * This method is intended to be called only once, or when the contents of
	 * this Panel need to be completely refreshed. This method does not
	 * set the size of the Strip: for that, call resizeScenarioSelectorImpl.
	 *
	 * Should be called after Panel construction because this method makes calls
	 * to the server to obtain information about each Scenario.
	 */
	
	public void createScenarioSelector()
	{
		// debug
		GlobalConsole.println("Entered createScenarioSelector for ScenarioViewPanel...");
		// end debug
		
		
		
		
		// Purge old strip, if any.
		
		if (scenarioSelector != null)
		{
			//if (this.getListenerRegistrar() != null)
			for (int i = 0; i < scenarioSelector.getTabCount(); i++)
				// each Scenario in the old Strip,
			{
				Component c = scenarioSelector.getComponentAt(i);
				InfoStrip infoStrip = (InfoStrip)c;
				if (infoStrip == null) continue;
				
				if (infoStrip instanceof ScenarioInfoStrip)
				{
					ScenarioInfoStrip scenarioInfoStrip = (ScenarioInfoStrip)infoStrip;
					NodeScenarioPanel panel = scenarioInfoStrip.getScenarioPanel();
					if (panel.getParent() == null) continue;
					
					// Remove the ModelScenarioPanel, and unsubscribe from 
					// the associated Scenario.
					
					viewPanel.removeScenarioVisual(panel);
				}
			}
			
			viewPanel.remove(this.scenarioSelector);
		}
		
		
		// Contruct a new selection strip and add it to this AWT Container.
		
		scenarioSelector = new ScenarioSelectorImpl(viewPanel);
		
		//((Container)viewPanel).add(scenarioSelector, BorderLayout.NORTH);
			//.... this replaces the viewPanel installed by ViewPanelBase
		viewPanel.getInfoPanel().add(scenarioSelector);
		//viewPanel.setComponentZOrder(viewPanel.getInfoPanel(), 0);
		//viewPanel.validate();


		// Add Listener to listen for changes in the size of the ScenarioSelectorImpl's
		// Container, so that the Listener can resize the ScenarioSelectorImpl.
		// (The Container is a JPanel with a null Layout Manager.)
		
		scenarioSelector.setComponentListener(new ComponentListener()
		{
			public void componentHidden(ComponentEvent e) {}
			
			public void componentMoved(ComponentEvent e) {}
			
			public void componentResized(ComponentEvent e)
			{
				resizeScenarioSelectorImpl();
			}
			
			public void componentShown(ComponentEvent e) {}
		});
		
		viewPanel.addComponentListener(scenarioSelector.getComponentListener());

		
		// Add an action listener for this tab, to perform the Hide action.
		
		scenarioSelector.addChangeListener
		(
			new ChangeListener()
			{
				public void stateChanged(ChangeEvent e)
				{
					if (e.getSource() == scenarioSelector) try
					{
						if (scenarioSelector.getSelectedIndex() == 
							scenarioSelector.getShowHideTabIndex())
						{
							setShowScenarios(false);
						}
						else
						{
							setShowScenarios(true);
						}
						
						viewPanel.getDisplayArea().repaint();
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(viewPanel.getComponent(), ex); return;
					}
				}
			}
		);
	}
	
	
	/** ************************************************************************
	 * Should be called immediately after the dimensions of this Panel are
	 * set or are changed, so that the dimensions of the Strip can be adjusted.
	 */
	 
	public void resizeScenarioSelectorImpl()
	{
		if (scenarioSelector == null) return;
		
		Insets insets = viewPanel.getInsets();
		
		int leftInset = 0;
		int rightInset = 0;
		int topInset = 0;
		int bottomInset = 0;
		
		if (insets != null)
		{
			leftInset = insets.left;
			rightInset = insets.right;
			topInset = insets.top;
			bottomInset = insets.bottom;
		}
		
		scenarioSelector.setSize(viewPanel.getWidth() - leftInset - rightInset,
			SelectionStripHeight);
			
		scenarioSelector.setLocation(leftInset, topInset);//.... But the container is BorderLayout
		
		scenarioSelector.setVisible(true);

		viewPanel.validate();
		Container infoPanel = viewPanel.getInfoPanel();
		infoPanel.setSize(viewPanel.getWidth(), scenarioSelector.getHeight());
	}
	
	
	/** ************************************************************************
	 * Add the specified ModelScenarioPanel as an InfoStrip in the Scenario selection
	 * strip tabbed pane; also add the ModelScenarioPanel to this Model Scenario
	 * View Panel and make it not visible.
	 * Do not change the current visible tab unless the new tab is the only tab.
	 */
	 
	public void addScenarioVisual(final NodeScenarioPanel panel)
	{
		// Add the ModelScenarioPanel to this Model Sceanrio View Panel.
		panel.setVisible(false);
		viewPanel.getDisplayArea().add(panel.getComponent());
		viewPanel.addComponentListener(panel.getComponentListener());
		
		// Add the strip as a tab.
		InfoStrip infoStrip = viewPanel.createScenarioInfoStrip((NodeScenarioPanel)panel);
		
		// Subscribe to server Events about the Scenario.
		if (viewPanel.getListenerRegistrar() != null) try
		{
			viewPanel.getListenerRegistrar().subscribe(panel.getNodeSer().getNodeId(), true);
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(viewPanel.getComponent(), ex);
		}
		
		scenarioSelector.setCurrentScenarioPanel(panel);
	}
	
	
	/** ************************************************************************
	 * Remove the specified Scenario as a tab in the Scenario selection strip. Do not
	 * change the current visible tab unless the tab is the current tab.
	 */
	 
	public void removeScenarioVisual(NodeScenarioPanel panel)
	{
		// Check if valueControlFieldValues contains values. If so, then
		// pop up a warning that those values will be lost.
		if (panel.containsUnsavedFieldValues())
		{
			if (JOptionPane.showConfirmDialog(panel.getJComponent(),
				"Values in scenario '" + panel.getName() + 
				"' have not been saved to the server; do you really want to close " +
				"the scenario?", "Please Confirm",
				JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) return;
		}
		
		// Remove the NodeScenarioPanel.
		panel.setVisible(false);
		panel.getParent().remove((Component)panel);

		// Call <ScenarioSelector>.removeScenarioInfoStrip. That removes
		// the tab and the InfoStrip that it owns.
		scenarioSelector.removeScenarioInfoStrip(panel.getScenarioInfoStrip());
		scenarioSelector.repaint();
		
		// Remove the Component Listener created in addScenarioVisual.
		viewPanel.removeComponentListener(panel.getComponentListener());
		
		// Unsubscribe from the Scenario.
		if (panel.getNodeSer() != null) try
		{
			if (viewPanel.getListenerRegistrar() != null)
				viewPanel.getListenerRegistrar().subscribe(
					panel.getNodeSer().getNodeId(), false);  // unsubscribe
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(viewPanel.getComponent(), ex);
		}
	}
	
	
	public Set<NodeScenarioPanel> getScenarioPanels()
	{
		Set<NodeScenarioPanel> sps = new HashSet<NodeScenarioPanel>();
		if (scenarioSelector == null) return sps;
		Iterator<NodeScenarioPanel> iterator = scenarioSelector.getScenarioPanelIterator();
		for (; iterator.hasNext();) sps.add(iterator.next());
		return sps;
	}
	
	
	/** ************************************************************************
	 * Find and return the index of the tab that contains a Visual for the
	 * specified Scenario. There should not be more than one, but if there is, 
	 * simply return the first one. If not found, return -1.
	 */
	 
	public int getScenarioTab(NodeScenarioPanel panel)
	{
		return scenarioSelector.getScenarioTab(panel);
	}
	
	
	/** ************************************************************************
	 * Verify that the Scenario name is legitimate, including from a security
	 * perspective. Intended for screening user input Scenario names.
	 */
	 
	public void validateScenarioName(String name)
	throws
		ParameterError
	{
		
	}
	
	
	protected ModelEngineRMI getModelEngine() { return viewPanel.getModelEngine(); }
}

