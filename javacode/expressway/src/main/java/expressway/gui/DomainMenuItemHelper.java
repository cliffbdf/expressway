/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.gui.View.*;
import expressway.gui.GraphicView.*;
import expressway.help.*;
import expressway.ser.*;
import java.awt.event.*;
import java.awt.Component;
import java.awt.GridLayout;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.Vector;
import javax.swing.*;
import java.lang.reflect.*;
import java.rmi.server.RMIClassLoader;


/**
 * See the slide "Instantiating Domains and Motifs".
 */
 
public class DomainMenuItemHelper
{
	public static void addDomainMenuItems(final View view, final Component component,
		JPopupMenu popup, final int x, final int y)
	throws
		Exception
	{
		if (view == null) throw new RuntimeException("view is null");
		HelpfulMenuItem mi;
		mi = new HelpfulMenuItem("New Domain...", false,
			"Instantiate a new Domain. You will be prompted for the type of Domain.");
		mi.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				JPopupMenu newPopup = new JPopupMenu("Select a Domain type");
				try { addNewDomainMenuItems(view, component, newPopup); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(component, ex);
					return;
				}
				newPopup.show(component, x, y);
			}
		});
		popup.add(mi);
		
		mi = new HelpfulMenuItem("New Motif...", false,
			"Instantiate a new Motif. You will be prompted for the type of Motif. " +
			"You do this to define a new Motif that can then be used by Domains.");
		mi.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				JPopupMenu newPopup = new JPopupMenu("Select a Motif type");
				try { addNewMotifMenuItems(view, component, newPopup); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(view.getWindow(), ex);
					return;
				}
				newPopup.show(component, x + component.getWidth(), y);
			}
		});
		popup.add(mi);	
		
		mi = new HelpfulMenuItem("Define New Domain/Motif Combination...", false,
			"Define a new type of instantiable Domain. You will be asked to select " +
			"a Domain base type, and a set of one or more motifs. Domains created of " +
			"the new type will be of the base Domain type and will automatically use " +
			"the motifs.");
		mi.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				JPopupMenu newPopup = new JPopupMenu("Select a Domain base type");
				try { addDefineDomainTypeMenuItems(view, component, newPopup, x, y); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(view.getWindow(), ex);
					return;
				}
				newPopup.show(component, x + component.getWidth(), y);
			}
		});
		popup.add(mi);

		mi = new HelpfulMenuItem("Delete Domain/Motif Combination...", false,
			"Delete a Domain/motif combination from the list of Domain types that " +
			"can be instantiated. This does not delete any Domains or motifs: it merely " +
			"removes a Domain/motif combination from the list of combinations that " +
			"that can be instantiated.");
		mi.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				JPopupMenu newPopup = new JPopupMenu("Select a Domain/Motif combination");
				try { addDeleteDomainMenuItems(view, component, newPopup); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(view.getWindow(), ex);
					return;
				}
				newPopup.show(component, x + component.getWidth(), y);
			}
		});
		popup.add(mi);
	}
	
	
	public static void addNewDomainMenuItems(final View view, final Component component,
		JPopupMenu popup)
	throws
		Exception
	{
		DomainTypeSer[] domainTypeSers = view.getModelEngine().getDomainTypes();
		for (final DomainTypeSer dtSer : domainTypeSers)
		{
			HelpfulMenuItem mi = new HelpfulMenuItem(dtSer.domainTypeName, false,
				dtSer.getHtmlDescription());
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try { view.getModelEngine().createDomainForDomainType(dtSer.getNodeId()); }
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(component, ex);
					}
				}
			});
			popup.add(mi);
		}
	}
	
	
	public static void addNewMotifMenuItems(final View view, final Component component,
		JPopupMenu popup)
	throws
		Exception
	{
		DomainTypeSer[] domainTypeSers = view.getModelEngine().getBaseDomainTypes();
		for (final DomainTypeSer dtSer : domainTypeSers)
		{
			HelpfulMenuItem mi = new HelpfulMenuItem(dtSer.domainTypeName, false,
				dtSer.getHtmlDescription());
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try { view.getModelEngine().createMotifForDomainType(dtSer.getNodeId()); }
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(component, ex);
					}
				}
			});
			popup.add(mi);
		}
	}
	
	
  /* Internal methods. */
	
	
	private static void addDefineDomainTypeMenuItems(final View view, final Component component,
		final JPopupMenu popup, final int x, final int y)
	throws
		Exception
	{
		DomainTypeSer[] bdtSers = view.getModelEngine().getBaseDomainTypes();
		for (final DomainTypeSer bdtSer : bdtSers)
		{
			HelpfulMenuItem mi = new HelpfulMenuItem("", false,
				"");
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					// Retrieve the selected motif types.
					List<MotifDefSer> motifDefSers;
					try { motifDefSers = 
						getMotifSelections(view, component, popup, x, y, bdtSer); }
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(component, ex);
						return;
					}
						
					if (motifDefSers == null) return;
					
					// Create a DomainType using the selected motif types.
					String[] selMotifDefIds = new String[motifDefSers.size()];
					int i = 0;
					for (MotifDefSer ser : motifDefSers) selMotifDefIds[i++] = ser.getNodeId();
					
					try { view.getModelEngine().createDomainType(bdtSer.getNodeId(), selMotifDefIds); }
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(component, ex);
						return;
					}
				}
			});
			popup.add(mi);
		}
	}
	
	
	private static List<MotifDefSer> getMotifSelections(final View view, 
		final Component component,
		JPopupMenu popup, final int x, final int y,
		final DomainTypeSer baseDomainTypeSer)
	throws
		Exception
	{
		MotifDefSer[] motifDefSers = 
			view.getModelEngine().getMotifsForBaseDomainType(baseDomainTypeSer.getNodeId());
		final List<MotifDefSer> selectedMotifDefSers = new Vector<MotifDefSer>();  // the return result.
		
		final JDialog dialog = 
			new JDialog(view.getWindow(), "Select motifs for base domain '" + baseDomainTypeSer.getName() + "'");
		dialog.setLocation(x, y);
		dialog.setLayout(new GridLayout(motifDefSers.length + 1, 1));
		
		class MotifCheckBox extends JCheckBox
		{
			private MotifDefSer motifDefSer;
			
			public MotifCheckBox(MotifDefSer motifDefSer)
			{
				super(motifDefSer.getName());
				this.motifDefSer = motifDefSer;
			}
			
			MotifDefSer getMotifDefSer() { return motifDefSer; }
		}

		int height = 0;
		
		for (MotifDefSer motifDefSer : motifDefSers)
		{
			MotifCheckBox checkbox = new MotifCheckBox(motifDefSer);
			dialog.add(checkbox);
			height += checkbox.getHeight();
		}
		
		JPanel buttonPanel = new JPanel();

		JButton okButton = new JButton("OK");
		JButton cancelButton = new JButton("Cancel");
		
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		dialog.add(buttonPanel);
		
		height += buttonPanel.getHeight();
		
		okButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// Determine which checkboxes are checked.
				Set<MotifDefSer> mdss = new HashSet<MotifDefSer>();
				Component[] comps = dialog.getComponents();
				for (Component comp : comps)
				{
					if (! (comp instanceof MotifCheckBox)) continue;
					MotifCheckBox cb = (MotifCheckBox)comp;
					if (cb.isSelected()) mdss.add(cb.getMotifDefSer());
				}
				
				selectedMotifDefSers.addAll(mdss);
				dialog.dispose();
			}
		});
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				dialog.dispose();
			}
		});
		
		dialog.setSize(500, height);
		dialog.setVisible(true);  // blocks.
		
		return selectedMotifDefSers;
	}
	
	
	private static void addDeleteDomainMenuItems(final View view, final Component component,
		JPopupMenu popup)
	throws
		Exception
	{
		DomainTypeSer[] uddtSers = view.getModelEngine().getUserDefinedDomainTypes();
		for (final DomainTypeSer uddtSer : uddtSers)
		{
			HelpfulMenuItem mi = new HelpfulMenuItem("", false,
				"");
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try { view.getModelEngine().deleteDomainType(uddtSer.getNodeId()); }
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(component, ex);
					}
				}
			});
			popup.add(mi);
		}
	}
}

