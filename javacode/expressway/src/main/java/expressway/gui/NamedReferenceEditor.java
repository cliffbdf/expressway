/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.help.*;
import expressway.help.HelpfulText.*;
import expressway.generalpurpose.*;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.*;
import java.io.Serializable;
import javax.swing.*;


public class NamedReferenceEditor extends JFrame
{
	private final ModelEngineRMI modelEngine;
	
	private StatusText domainLabel;
	private StatusText nameLabel;
	private StatusText descriptionLabel;
	private StatusText fromLabel;
	private StatusText toLabel;
	
	private JTextField nameTextField;
	private JTextField descriptionTextField;
	private JList fromList;
	private JList toList;
	private JButton saveButton;
	private JButton cancelButton;
	
	
	public NamedReferenceEditor(ClientMainApplication mainApp, final NodeDomainSer domainSer)
	{
		this(mainApp, domainSer, null);
	}
	
	
	public NamedReferenceEditor(ClientMainApplication mainApp,
		final NodeDomainSer domainSer, NamedReferenceSer nrSer)
	{
		super("Define a Named Reference");
		this.setAlwaysOnTop(true);
		
		this.modelEngine = mainApp.getModelEngine();
		
		String[] nodeTypes;
		try { nodeTypes = modelEngine.getInterfaceNodeKinds(); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(NamedReferenceEditor.this, ex);
			return;
		}

		String namedRefName = "";
		String namedRefDesc = "";
		String fromListValue = null;
		String toListValue = null;
		
		if (nrSer != null)
		{
			namedRefName = nrSer.getName();
			namedRefDesc = nrSer.description;
			fromListValue = nrSer.fromTypeName;
			toListValue = nrSer.toTypeName;
		}
		
		this.setSize(400, 200);
		this.setLayout(new GridBagLayout());
		
		domainLabel = new StatusText(true, "For Domain '" + domainSer.getName() + "'",
			"Create or edit the definition of a " +
				HelpWindow.createHref("Named References", "Named Reference") +
				" that is or will be owned by Domain '" + domainSer.getName() + "'");
		nameLabel = new StatusText(false, "Named Reference Name",
			"The name that the Named Reference will have");
		descriptionLabel = new StatusText(false, "Description",
			"A description of the purpose and meaning of the " +
				HelpWindow.createHref("Named References", "Named Reference"));
		fromLabel = new StatusText(false, "'From' Type", "A " +
			HelpWindow.createHref("Named References", "Named Reference") +
			" enables you to creates " + HelpWindow.createHref("Named References",
				"Cross_References", "Cross References") + " betweeb Nodes. " +
			"Each Cross Reference inter-relates two Nodes: a 'from' Node and " +
			"a 'to' Node. In the list below, select the type that the 'from' Nodes " +
			"of this Named Reference must conform to.");
		toLabel = new StatusText(false, "'From' Type", "A " +
			HelpWindow.createHref("Named References", "Named Reference") +
			" enables you to creates " + HelpWindow.createHref("Named References",
				"Cross_References", "Cross References") + " betweeb Nodes. " +
			"Each Cross Reference inter-relates two Nodes: a 'from' Node and " +
			"a 'to' Node. In the list below, select the type that the 'to' Nodes " +
			"of this Named Reference must conform to.");
		
		nameTextField = new JTextField(namedRefName);
		descriptionTextField = new JTextField(namedRefDesc);
		fromList = new JList(nodeTypes);
		toList = new JList(nodeTypes);
		saveButton = new JButton("Save");
		cancelButton = new JButton("Cancel");
		
		if (fromListValue != null) fromList.setSelectedValue(fromListValue, true);
		if (toListValue != null) toList.setSelectedValue(toListValue, true);
		
		JPanel panelA = new JPanel();
		JPanel panelB = new JPanel();
		JPanel panelC = new JPanel();
		JPanel panelD = new JPanel();
		JPanel panelE = new JPanel();
		JPanel panelF = new JPanel();

		panelA.add(domainLabel);
		panelB.add(nameLabel);
		panelB.add(nameTextField);
		panelC.add(descriptionLabel);
		panelC.add(descriptionTextField);
		panelD.add(fromLabel);
		panelD.add(fromList);
		panelE.add(toLabel);
		panelE.add(toList);
		panelF.add(cancelButton);
		panelF.add(saveButton);
		
		GridBagConstraints constraintsA = new GridBagConstraints(
			0, //int gridx,
			0, //int gridy,
			2, //int gridwidth,
			1, //int gridheight,
			1, //double weightx: 0-1
			1, //double weighty: 0-1
			GridBagConstraints.CENTER, //int anchor: FIRST_LINE_START, PAGE_START,
				// FIRST_LINE_END LINE_START, CENTER, LINE_END
				// LAST_LINE_START, PAGE_END, LAST_LINE_END
			GridBagConstraints.BOTH,
				// NONE, HORIZONTAL, VERTICAL, BOTH
			new Insets(0, 0, 0, 0), //Insets insets,
			10, //int ipadx,
			4 //int ipady
			);
		
		GridBagConstraints constraintsB = new GridBagConstraints(
			0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 10, 4);
		
		GridBagConstraints constraintsC = new GridBagConstraints(
			1, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 10, 4);
		
		GridBagConstraints constraintsD = new GridBagConstraints(
			0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 10, 4);
		
		GridBagConstraints constraintsE = new GridBagConstraints(
			1, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 10, 4);
		
		GridBagConstraints constraintsF = new GridBagConstraints(
			0, 3, 2, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 10, 4);
		
		add(panelA, constraintsA);
		add(panelB, constraintsB);
		add(panelC, constraintsC);
		add(panelD, constraintsD);
		add(panelE, constraintsE);
		add(panelF, constraintsF);
		
		saveButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String name = nameTextField.getText();
				String desc = descriptionTextField.getText();
				String fromType = (String)(fromList.getSelectedValue());
				String toType = (String)(toList.getSelectedValue());
				
				try
				{
					try { modelEngine.createNamedReference(false, domainSer.getNodeId(),
						name, desc, fromType, toType); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(NamedReferenceEditor.this,
							w.getMessage(), "Warning",
							JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION)
						{
							modelEngine.createNamedReference(true, domainSer.getNodeId(),
								name, desc, fromType, toType);
						}
					}
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(NamedReferenceEditor.this, ex);
					return;
				}
				
				dispose();
			}
		});
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				dispose();
			}
		});
		
		this.show();
	}
}

