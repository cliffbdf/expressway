/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.ListenerRegistrar;
import expressway.common.ClientModel.PeerListener;
import expressway.common.VisualComponent.SimulationVisual;
import javax.swing.*;
import java.util.Set;
import java.util.List;
import java.awt.*;
//import java.awt.Panel;


/**
 * A GUI container for a collection of Views, graphs, and other information displays
 * used by a GUI client. There should be only one PanelManager per GUI client.
 */
 
public interface PanelManager extends SwingComponent
{
	JFrame getFrame();
	
	
	ClientMainApplication getClientMainApplication();
	
	
	String getName();
	
	
	int getRecommendedPanelAreaWidth();
	
	
	int getRecommendedPanelAreaHeight();
	
	
	/** Add a new Component to this PanelManager. The added Component must be
		a Container, and it <i>may</i> be one of the ViewContainer types. */
	
	void addPanel(String title, Container panel, boolean asPopup);
	
	
	/** Remove the specified Panel from the UI. Perform all required disconnections
		and notifications. */
	void removePanel(Component panel);
	
	
	boolean isPopupView(View view);
	
	
	/** Set the title of the View specified by the View's unique id. */
	
	void setTitleAt(int id, String title);
	
	
	List<NodeView> getNodeViews();
	
	
	/** Return the NodeViews whose outermost Visual depicts a Node of the specified
		Domain. Note that a View may be contained in a Container. */
	
	Set<NodeView> getNodeViewsForDomain(String domainId);
	
	
	/** Return the NodeViews that exist for which the specified Node is the
		outermost Node that is depicted. */
		
	Set<NodeView> getNodeViewsForNode(String nodeId);
	
	
	/** Return the Single_NodeView_Container that depicts the specified Node, or
		return null if there is not one. */
		
	Single_NodeView_Container getNodeViewContainer(String nodeId);
	
	
	/** Return the Multi_NodeView_Containers that contain NodeViews depicting
		the specified Node. If there are none, return an empty Set. If nodeId is
		null, return all Multi_NodeView_Containers. */
		
	Set<Multi_NodeView_Container> getNodeViewContainers(String nodeId);
	
	
	/** Unique ID for a Panel that is contained in this client. If the return
		result is negative, it means that the Panel does not exist in this ViewManager. */
		
	int idOfPanel(Container panel);
	
	
	void setSelectedComponent(Component c);
	
	
	void recomputeLayout();
	
	
	/** Contains no more than one NodeView for a specified outermost Node. */
	
	interface Single_NodeView_Container extends NodeViewContainer
	{
		/** If nodeId is null, return null. */
		NodeView getNodeView();
	}
	
	
	/** May contain multiple NodeViews for a specified outermost Node. */
	
	interface Multi_NodeView_Container extends NodeViewContainer
	{
	}
	
	
	/** May contain multiple Views depicting the specified Node. Not currently used. */
	
	interface MultiNode_View_Container extends ViewContainer
	{
	}
	
	
	/** Base type for all NodeView Containers. */
	
	interface NodeViewContainer extends ViewContainer
	{
		/** If nodeId is null, return all NodeViews in the Container. */
		Set<NodeView> getNodeViews(String nodeId);
		
		void postConstructionSetup() throws Exception;
	}
	
	
	/** Base type for all View Containers. Note that Components managed by a
		PanelManager are not required to implement ViewContainer unless they
		depict one or more Views. */
	
	interface ViewContainer extends AWTContainer
	{
		/** If nodeId is null, return all Views in the Container. */
		Set<View> getViews(String nodeId);
		
		Set<View> getViews();
		
		int getNoOfViews();
	}
}

