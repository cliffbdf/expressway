/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.ClientModel.*;
import expressway.common.VisualComponent;
import expressway.common.ModelAPITypes.*;
import expressway.gui.InfoStrip.*;
import expressway.help.*;
import expressway.ser.*;
import java.util.Set;
import java.net.URL;
import java.io.Serializable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;



/** ************************************************************************
 * A strip of information about an NodeScenarioPanel when its tab is selected
 * in the Scenario Selection Strip.  Links to the associated ScenarioPanel.
 * An InfoStrip is instantiates as the Component in the Scenario Selection
 * Strip Tabbed Pane.
 * The informational contents of an InfoStrip are not set during construction;
 * they are set by calling ????
 * Button functions:
	Save [Scenario].
	Close [Scenario].
	New Scenario. (inherited from InfoStrip).
 */
 
public class ScenarioInfoStripImpl extends InfoStripImpl
	implements ScenarioInfoStrip
{
	/** Button for saving the state of the Scenario represented by the Attribute Value Panel. */
	private JButton saveButton;
	
	/** Button for closing the Attribute Value Panel. */
	private JButton closeButton;
	
	/** Button for creating a new  Scenario. */
	private JButton newScenarioButton;
	
	private HelpButton helpButton;
	
	private NodeScenarioPanel scenarioPanel;
	
	private URL saveImgURL;
	private URL closeImgURL;
	private URL newScenarioImgURL;
	
	
	public ScenarioInfoStripImpl(NodeScenarioPanel scenarioPanel)
	{
		super(scenarioPanel.getNodeView());
		this.scenarioPanel = scenarioPanel;
		scenarioPanel.setScenarioInfoStrip(this);

		this.saveButton = new JButton(ButtonIcons.SaveButtonIcon);
		this.saveButton.setToolTipText("Write name and Attribute value changes to database");
		this.add(this.saveButton);

		this.closeButton = new JButton(ButtonIcons.CloseButtonIcon);
		this.closeButton.setToolTipText("Close this Scenario View");
		this.add(this.closeButton);
		
		this.newScenarioButton = new JButton(ButtonIcons.NewScenarioButtonIcon);
		this.newScenarioButton.setToolTipText("Create a new Scenario");
		this.add(this.newScenarioButton);

		Class thisClass = this.getClass();
		saveImgURL = thisClass.getResource(ButtonIcons.SaveButtonIconImagePath);
		closeImgURL = thisClass.getResource(ButtonIcons.CloseButtonIconImagePath);
		newScenarioImgURL = thisClass.getResource(ButtonIcons.NewScenarioButtonIconImagePath);
		if (saveImgURL == null) throw new RuntimeException("saveImgURL is null");
		if (closeImgURL == null) throw new RuntimeException("closeImgURL is null");
		if (newScenarioImgURL == null) throw new RuntimeException("newScenarioImgURL is null");
		
		this.helpButton = new HelpButton("Toolbar Help", HelpButton.Size.Large, getHelpText());
		
		helpButton.setToolTipText("Show explanation of each toolbar button");
		this.add(helpButton);

		this.saveButton.addActionListener(new SaveScenarioActionListener());
		this.closeButton.addActionListener(new CloseScenarioActionListener());
		this.newScenarioButton.addActionListener(new NewScenarioActionListener());
		
		String newName = getScenarioPanel().getNodeSer().getName();
		this.getNameField().setText(newName);
		
		/*long lastUpdated = panel.getScenarioSer().getLastUpdated();
		this.getModifiedLabel().setText("Version: " + lastUpdated);*/
	}
	
	
	protected String getHelpText()
	{
		String s1 = "<p><img src=\"";
		s1 += saveImgURL.toString();
		s1 += "\" /> &nbsp;<b></b> ";
		s1 +=
			"The " + HelpWindow.createHref("Model Domains", "Model Domain");
		s1 +=
			" name is sent to the server database.</p>";
			
		String s2 =
		"<p><img src=\"" + closeImgURL.toString() + "\" /> &nbsp;<b></b> " +
			"The View of the " + HelpWindow.createHref("Model Domains", "Model Domain") +
			" is closed. (This does not affect it " +
			"in the database.)" +
			"</p>";
			
		String s3 =
		"<p><img src=\"" + newScenarioImgURL.toString() + "\" /> &nbsp;<b></b> " +
			"A new " + HelpWindow.createHref("Model Scenarios", "Model Scenario") +
			" is created for the " + HelpWindow.createHref("Model Domains", "Model Domain") + "." +
			"</p>";
			
		return s1 + s2 + s3;
	}
	
	
	public NodeScenarioPanel getScenarioPanel() { return scenarioPanel; }
	
	
	public void update()
	{
		String newName = getScenarioPanel().getNodeSer().getName();
		this.getNameField().setText(newName);
		
		/*long lastUpdated = panel.getScenarioSer().getLastUpdated();
		this.getModifiedLabel().setText("Version: " + lastUpdated);*/
		
		// Update the tab that contains the Scenario name.
		
		//if (controlPanelSelector == null) return;
		ScenarioSelector controlPanelSelector = getScenarioPanel().getScenarioSelector();
		if (controlPanelSelector != null)
			controlPanelSelector.setControlPanelTitle(getScenarioPanel(), newName);
		//int index = getControlpanel().getScenarioSelector().indexOfComponent(this);
		//getControlPanel().getScenarioSelector().setTitleAt(index, newName);
	}
	
	
	protected NodeScenarioView getScenarioViewPanel() { return (NodeScenarioView)(getNodeView()); }


	/** ********************************************************************
	 * Handle "Save Scenario" button selection actions.
	 */
	 
	class SaveScenarioActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			/*
			Update (to server) the currently displayed Scenario. If the
			user has entered a new Scenario name in the Name field, the Scenario
			will be renamed with the new name.
			*/
			
			// Update the Scenario name, if necessary.
			
			String newScenarioName = getNameField().getText();
			ScenarioSer scenarioSer = (ScenarioSer)(getScenarioPanel().getNodeSer());
			
			if (! newScenarioName.equals(scenarioSer.getName()))
			{
				// Parse and validate values in the fields of the Strip.
			
				try { getScenarioViewPanel().validateScenarioName(newScenarioName); }
				catch (ParameterError ex)
				{
					ErrorDialog.showThrowableDialog(ScenarioInfoStripImpl.this, ex); return;
				}
				
				
				// Update the server.
				
				try
				{
					ScenarioSer newScenarioSer = 
						(ScenarioSer)(scenarioSer.shallowCopy());
						
					newScenarioSer.setName(newScenarioName);
					
					try { scenarioSer = 
						(ScenarioSer)(getModelEngine().updateScenario(
							false, newScenarioSer)); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(null, w.getMessage(),
							"Warning", JOptionPane.YES_NO_OPTION) 
								== JOptionPane.YES_OPTION)
								
							scenarioSer = 
								(ScenarioSer)(getModelEngine().updateScenario(
									true, newScenarioSer));
									
						else return;
					}
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(ScenarioInfoStripImpl.this, ex); return;
				}
			}
			
			
			// Send the Scenario's Attribute values to the server.
			
			Set<ValueControl> attrControls = 
				getScenarioPanel().getValueControls(AttributeValueControl.class);
			for (ValueControl vc : attrControls)
			{
				AttributeValueControl attrControl = (AttributeValueControl)vc;
				AttributeVisual attrVisual = attrControl.getAttributeVisual();
				String attrId = attrVisual.getNodeId();
				Serializable newValue;
				try { newValue = attrControl.getValue(); }
				//try { newValue = attrVisual.parseValue((String)newValue); }
				catch (ParameterError pe)
				{
					if (JOptionPane.showConfirmDialog(null, 
						"Could not recognize value; treat as String?",
						"Warning", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
						
						return;
				}
				
				try
				{
					String textValue = attrControl.getValueAsString();
					try { getModelEngine().setAttributeValue(false, attrId, 
						scenarioSer.getNodeId(), textValue); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(null, w.getMessage(),
							"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
						
							getModelEngine().setAttributeValue(true, attrId, 
								scenarioSer.getNodeId(), textValue);
						
						else return;
					}
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(ScenarioInfoStripImpl.this, ex);
				}
			}
			
			// Clear the valueControlFieldValues.
			getScenarioPanel().clearFieldValues();
			//getScenarioPanel().clearValueFieldTypes();
		}
	}
	
	
	/** ********************************************************************
	 * Handle "Close Scenario" button selection actions.
	 */
	 
	class CloseScenarioActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			getScenarioViewPanel().removeScenarioVisual(getScenarioPanel());
		}
	}


	/** ********************************************************************
	 * Handle "New Scenario" button selection actions.
	 */
	 
	class NewScenarioActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			// Send request to server to create a new Scenario, with
			// a default name. A new Scenario Panel is not created until
			// the server issues a callback to update the Domain.
			
			try
			{
				getScenarioViewPanel().setShowScenarios(true);
			
				getModelEngine().createScenario(getNodeView().getOutermostNodeSer().getDomainName(), null);
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ScenarioInfoStripImpl.this, ex);
			}
		}
	}
}

