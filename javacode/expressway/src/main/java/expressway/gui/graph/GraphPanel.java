/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.graph;

import expressway.gui.PanelManager;
import expressway.graph.Graph;
import expressway.graph.Graph.InvalidGraph;
import expressway.graph.Graph.OverflowException;
import expressway.geometry.Point;
import expressway.generalpurpose.ThrowableUtil;
import expressway.generalpurpose.DateAndTimeUtils;
import java.util.List;
import java.util.Vector;
import java.awt.Color;
import java.awt.Graphics;
import java.text.NumberFormat;
import java.awt.FontMetrics;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JOptionPane;



/** ****************************************************************************
 * A graph that can be instantiated as a GUI Component.
 */
 
public class GraphPanel extends JPanel
{
	public static final int MinGraphWidth = 100;
	public static final int MinGraphHeight = 100;
	
	public static final int DefaultLeftMargin = 100;
	public static final int DefaultRightMargin = 100;
	public static final int DefaultTopMargin = 100;
	public static final int DefaultBottomMargin = 100;
	
	public static final int XAxisLabelHeight = 20;
	public static final int YAxisLabelWidth = 20;
	
	public JButton closeButton;
	
	private final PanelManager tabbedContainer; // may be null
	private final Graph graph;
	private boolean canDraw = false;
	private boolean sizeComputed = false;
	
	
	private List<Graph> otherGraphs = new Vector<Graph>();
	
	
	public GraphPanel(final PanelManager tabbedContainer /* may be null */, final Graph graph)
	{
		this.setLayout(null);
		this.tabbedContainer = tabbedContainer;
		this.graph = graph;
		setBackground(Color.WHITE);
		
		this.addComponentListener(new ComponentAdapter()
		{
			public void componentResized(ComponentEvent e)
			{
				try { computeGraphSize(graph); }  // sets canDraw
				catch (InvalidGraph ig)
				{
					ig.printStackTrace();
					/*JOptionPane.showMessageDialog(GraphPanel.this, 
						ThrowableUtil.getAllMessages(ig),
						"Error",
						JOptionPane.ERROR_MESSAGE);*/
				}
			}
		});
		
		enableEvents(java.awt.event.MouseEvent.MOUSE_DRAGGED);
		enableEvents(java.awt.AWTEvent.MOUSE_MOTION_EVENT_MASK);
		
		if (tabbedContainer != null)
		{
			add(this.closeButton = new JButton("X"));
			closeButton.setLocation(10, 10);
			closeButton.setSize(20, 20);
			
			closeButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					tabbedContainer.removePanel(GraphPanel.this);
				}
			});
		}
	}
	
	
	public Graph getGraph() { return this.graph; }
		
		
	/** ************************************************************************
	 * Add another Graph on top of the original one used to construct this Panel.
	 
	 Note: this doesn't work because the paint method clears the Graphics. I will
	 have to layer on a Panel and make it transparent.
	 */
	 
	public void layerGraph(Graph otherGraph)
	throws
		InvalidGraph
	{
		otherGraphs.add(otherGraph);
		
		computeGraphSize(otherGraph);
	}


	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		if (! sizeComputed) try { computeGraphSize(graph); }
		catch (Graph.InvalidGraph ig) { ig.printStackTrace(); }
			
		if (this.canDraw) try
		{
			graph.draw(g);
			
			for (Graph otherGraph : otherGraphs)
			{
				otherGraph.draw(g);
			}
		}
		catch (Graph.InvalidGraph ig) { ig.printStackTrace(); }
	}


	public String getTitle() { return graph.getTitle(); }
	public void setTitle(String title) { graph.setTitle(title); }
	
	public String getXAxisLabel() { return graph.getXAxisLabel(); }
	public void setXAxisLabel(String s) { graph.setXAxisLabel(s); }
	
	public String getYAxisLabel() { return graph.getYAxisLabel(); }
	public void setYAxisLabel(String s) { graph.setYAxisLabel(s); }
	
	public Color getAxisColor() { return graph.getAxisColor(); }
	public void setAxisColor(Color c) { graph.setAxisColor(c); }
	
	public Color getPointColor() { return graph.getPointColor(); }
	public void setPointColor(Color c) { graph.setPointColor(c); }
	
	public List<Point> getPoints() { return graph.getPoints(); }
	public void setPoints(List<Point> points) { graph.setPoints(points); }
	
	public double[] getXValues() { return graph.getXValues(); }
	public void setXValues(double[] xValues) { graph.setXValues(xValues); }
	
	public double[] getYValues() { return graph.getYValues(); }
	public void setYValues(double[] yValues) { graph.setYValues(yValues); }
	
	public double getXStartLogical() { return graph.getXStartLogical(); }
	public void setXStartLogical(double x) { graph.setXStartLogical(x); }
	
	public double getXEndLogical() { return graph.getXEndLogical(); }
	public void setXEndLogical(double x) { graph.setXEndLogical(x); }
	
	public double getXDeltaLogical() { return graph.getXDeltaLogical(); }
	public void setXDeltaLogical(double d) { graph.setXDeltaLogical(d); }
	
	public double getYStartLogical() { return graph.getYStartLogical(); }
	public void setYStartLogical(double y) { graph.setYStartLogical(y); }
	
	public double getYEndLogical() { return graph.getYEndLogical(); }
	public void setYEndLogical(double y) { graph.setYEndLogical(y); }
	
	public double getYDeltaLogical() { return graph.getYDeltaLogical(); }
	public void setYDeltaLogical(double d) { graph.setYDeltaLogical(d); }
	
	public double getPixelXSize() { return graph.getPixelXSize(); }
	public void setPixelXSize(double psize) { graph.setPixelXSize(psize); }
	
	public double getPixelYSize() { return graph.getPixelYSize(); }
	public void setPixelYSize(double psize) { graph.setPixelYSize(psize); }
	
	public int getMaxXAxisLength() { return graph.getMaxXAxisLength(); }
	public void setMaxXAxisLength(int max) { graph.setMaxXAxisLength(max); }

	public int getMaxYAxisLength() { return graph.getMaxYAxisLength(); }
	public void setMaxYAxisLength(int max) { graph.setMaxYAxisLength(max); }
	
	public int getXStartDisplay() { return graph.getXStartDisplay(); }
	public void setXStartDisplay(int xStart) { graph.setXStartDisplay(xStart); }
	
	public int getYStartDisplay() { return graph.getYStartDisplay(); }
	public void setYStartDisplay(int yStart) { graph.setYStartDisplay(yStart); }

	public int getXAxisNotchLength() { return graph.getXAxisNotchLength(); }
	public void setXAxisNotchLength(int notchLength) { graph.setXAxisNotchLength(notchLength); }
	
	public int getYAxisNotchLength() { return graph.getYAxisNotchLength(); }
	public void setYAxisNotchLength(int notchLength) { graph.setYAxisNotchLength(notchLength); }
	
	public int getPointRectXLength() { return graph.getPointRectXLength(); }
	public void setPointRectXLength(int rectSize) { graph.setPointRectXLength(rectSize); }

	public int getPointRectYLength() { return graph.getPointRectYLength(); }
	public void setPointRectYLength(int rectSize) { graph.setPointRectYLength(rectSize); }
	
	
	/**
	 * Compute the size of the Graph, based on the display space available.
	 */
	 
	public void computeGraphSize(Graph graph)
	throws
		InvalidGraph
	{
		if (graph.getXDeltaLogical() <= 0.0) throw new InvalidGraph(
			"XDeltaLogical is " + graph.getXDeltaLogical() + "; must be > 0");
		
		if (graph.getYDeltaLogical() <= 0.0) throw new InvalidGraph(
			"YDeltaLogical is " + graph.getYDeltaLogical() + "; must be > 0");
		
		Graphics graphics = this.getGraphics();
		if (graphics == null) return;  // not yet ready to compute size.
		
		
		this.canDraw = true;
		
		// Compute domain and range.
		
		int xEndDisplay;  // location in display space of the end of the X axis.
			
		try { xEndDisplay = graph.translateXToDisplay(getXEndLogical()); }
		catch (OverflowException oe)
		{
			throw new InvalidGraph(oe);
		}
		
		int yEndDisplay;  // location in display space of the end of the Y axis.
			
		try { yEndDisplay = graph.translateYToDisplay(getYEndLogical()); }
		catch (OverflowException oe)
		{
			throw new InvalidGraph(oe);
		}
		
		xEndDisplay = Math.min(xEndDisplay, graph.getMaxXAxisLength() + graph.getXStartDisplay());
		yEndDisplay = Math.max(yEndDisplay, graph.getYStartDisplay() - graph.getMaxYAxisLength());
		

		int leftMargin;
		
		int rightMargin = DefaultRightMargin;
		
		int topMargin = DefaultTopMargin;
		
		int bottomMargin;
		
		
		// Compute left margin needed to accommodate numbers along vertical axis.
		
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(2);
		FontMetrics fontMetrics = this.getGraphics().getFontMetrics();
		int dx = fontMetrics.charWidth('0');
		int dy = fontMetrics.getHeight() - 2;  // the -2 seems to be needed
		
		//int xNotchDisplay;
		int maxHeight = DefaultBottomMargin;
		double xLogical = graph.getXStartLogical();
		if (graph.getXDeltaLogical() <= 0.0) throw new RuntimeException(
			"xDeltaLogical is " + graph.getXDeltaLogical());
		
		for (;;)  // each notch
		{
			if (xLogical > graph.getXEndLogical()) break;

			//try { xNotchDisplay = translateXToDisplay(xLogical); }
			//catch (OverflowException oe) { throw new RuntimeException(oe); }

			//if (xNotchDisplay > xEndDisplay) break;
			
			String stringValue = numberFormat.format(xLogical / DateAndTimeUtils.MsInADay);

			maxHeight = Math.max(maxHeight, stringValue.length() * dy);
			xLogical += graph.getXDeltaLogical();
		}
		
		bottomMargin = maxHeight;
		
		bottomMargin += XAxisLabelHeight;
		
		
		double yLogical = graph.getYStartLogical();
		//xNotchDisplay = ...
		int maxWidth = DefaultLeftMargin;
		for (;;)  // each notch
		{
			if (yLogical > getYEndLogical()) break;
			
			//try { yNotchDisplay = translateYToDisplay(yLogical); }
			//catch (OverflowException oe) { throw new RuntimeException(oe); }

			//if (yNotchDisplay < yEndDisplay) break;
			
			String stringValue = numberFormat.format(yLogical);
			
			int stringWidth = fontMetrics.charsWidth(stringValue.toCharArray(),
				0, stringValue.length());
			
			maxWidth = Math.max(maxWidth, stringWidth);

			yLogical += graph.getYDeltaLogical();
		}
		
		leftMargin = maxWidth;
		
		leftMargin += YAxisLabelWidth;
		
		
		graph.setXStartDisplay(leftMargin);
		
		graph.setYStartDisplay(this.getHeight() - bottomMargin);

		graph.setMaxXAxisLength(this.getWidth() - leftMargin - rightMargin);
		graph.setMaxYAxisLength(this.getHeight() - topMargin - bottomMargin);
		
		/*
		if (graph.getMaxXAxisLength() < MinGraphWidth) { throw new InvalidGraph(
			"Graph X axis length is " + graph.getMaxXAxisLength() +
			": must be longer than " + MinGraphWidth); }
		
		if (graph.getMaxYAxisLength() < MinGraphHeight)
		{
			System.out.println("GraphPanel height=" + this.getHeight());
			System.out.println("topMargin=" + topMargin);
			System.out.println("bottomMargin=" + bottomMargin);
			
			throw new InvalidGraph(
			"Graph Y axis length is " + graph.getMaxYAxisLength() +
			": must be longer than " + MinGraphHeight);
		}
		*/
	
		sizeComputed = true;
	}
}

