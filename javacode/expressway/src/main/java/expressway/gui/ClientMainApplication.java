/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ser.NodeSer;
import expressway.common.*;
import java.util.List;
import java.io.File;


public interface ClientMainApplication
{
	String getModelEngineNetworkPath();
	
	void setModelEngineNetworkPath(String path);
	
	int getHTTPPort();
	
	void setHTTPPort(int p);
	
	ModelEngineRMI getModelEngine();
	
	void connectToModelEngine();

	void addXMLDir(File xmlDir);
	
	/** The last element is the most recent. */
	List<File> getXMLDirs();
	
	File getMostRecentXMLDir();
	
	void addJARDir(File jarDir);
	
	/** The last element is the most recent. */
	List<File> getJARDirs();
	
	File getMostRecentJARDir();
}

