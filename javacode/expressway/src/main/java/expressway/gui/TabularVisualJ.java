/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.gui.TabularView.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.help.*;
import expressway.ser.*;
import expressway.generalpurpose.ThrowableUtil;
import expressway.generalpurpose.StringUtils;
import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableModel;
import org.jdesktop.swingx.JXTreeTable;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.Enumeration;
import java.util.HashSet;
import java.io.IOException;
import java.io.Serializable;
import java.io.PrintWriter;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Container;
import java.awt.Component;
import java.awt.event.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.tree.TreePath;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;


/**
 * Base class for Visuals in Views that utilize a JXTreeTable.
 * Derived classes should not define fields to hold Node data. Instead, Node data
 * should be stored as column data, using the TreeTableNode.setValueAt(...) method.
 * Column 0 is always a name field.
 */
 
public abstract class TabularVisualJ extends AbstractMutableTreeTableNode
	implements VisualComponent
{
	private HierarchySer nodeSer;
	private TreeTablePanelViewBase viewPanel;
	private boolean unpropagated = false;
	private boolean inconsistent = false;
	private boolean populated = false;
	private Dimension visualNormalSize;
	private ImageIcon imageIcon;
	
	/** One value per column, except that the first column (Name column) is not
		included: it is stored in the NodeSer for this Visual. */
	private Object[] values;
	
	
	/**
	 * @param noOfValues Number of columns in addition to the name column.
	 */
	 
	protected TabularVisualJ(NodeSer nodeSer, TreeTablePanelViewBase viewPanel,
		int noOfValues)
	throws
		ParameterError
	{
		if (! (nodeSer instanceof HierarchySer)) throw new RuntimeException(
			"nodeSer is not a HierarchySer");
		
		this.nodeSer = (HierarchySer)nodeSer;
		this.viewPanel = viewPanel;
		values = new Object[noOfValues];
		// Note: value should be populated by the specialized class.

		try { setImageIcon(); }
		catch (IOException ex) { throw new ParameterError(ex); }
	}
	
	
	/**
	 * Return the kind of server Node that this Visual depicts. The server method
	 * getInstantiableNodeKinds returns the kinds of Nodes that may be instantiated
	 * inside of an existing Node.
	 */
	 
	public abstract String getNodeKind();
	
	
	/**
	 * Return the kind of Node that the Node depicted by this Visual should assume
	 * if it is 'demoted'. See the server method demoteNode().
	 */
	 
	public abstract String getDemotionNodeKind();
	
	
	/**
	 * Send an update request to the server, based on the data in the specified
	 * column of this tabular Visual.
	 */
	 
	public abstract void updateServer(int column);
	
	
	/**
	 * If this Visual's View depicts Scenarios, return the Id of the current Scenario.
	 * Otherwise return null.
	 */
	 
	public abstract String getScenarioId();
	
	
	/**
	 * Add editing behavior:
	 *	Delete.
	 *	Edit cell.
	 *	Clear.
	 *	Duplicate row.
	 *	Insert a new row before/after this row.
	 *	In/Out-dent this row.
	 *	View/Edit details for row.
	 */
					
	protected void addContextMenuItems(JPopupMenu containerPopup, final int x, final int y)
	{
		addWhatIsThisMenuItem(containerPopup, x, y);
		addCrossRefMenuItem(containerPopup, x, y);
		MenuItemHelper.addMenuOwnerItems(getClientView(), this,
			(MenuOwnerSer)(getNodeSer()), containerPopup, x, y);
		
		JMenuItem menuItem;
		
		
		/* Delete row */
		
		menuItem = new HelpfulMenuItem("Delete row", false,
			"Delete the row under the mouse");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				deleteRow();
			}
		});
		
		containerPopup.add(menuItem);
		
		
		/* Edit cell */
		
		menuItem = new HelpfulMenuItem("Edit cell...", false,
			"Open a visual editing panel for editing the value of the current cell.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int[] columnIndexes = getSelectedColumns();
				if (columnIndexes.length == 0)
				{
					ErrorDialog.showErrorDialog(getViewPanel(),
						"No cells selected");
					return;
				}
				
				if (columnIndexes.length > 1)
				{
					ErrorDialog.showErrorDialog(getViewPanel(),
						"May only select one cell at a time to edit");
					return;
				}
				
				try { for (int colIndex : columnIndexes) editCell(colIndex); }
				catch (NotVisible nv) { throw new RuntimeException(nv); }
			}
		});
		
		containerPopup.add(menuItem);
		
		
		/* Clear row */
		
		menuItem = new HelpfulMenuItem("Clear cell", false,
			"Clear all columns in this row and update server.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int[] columnIndexes = getSelectedColumns();
				for (int colIndex : columnIndexes) clearCell(colIndex);
			}
		});
		
		containerPopup.add(menuItem);
		
		
		/* Duplicate row */
		
		menuItem = new HelpfulMenuItem("Duplicate row", false,
			"Create a new row below the current one.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				duplicateRow();
			}
		});
		
		containerPopup.add(menuItem);
		
		
		/* Insert new row before */
		
		menuItem = new HelpfulMenuItem("Insert new row before", false,
			"Insert a new (empty) row before the current row.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				insertNewBefore();
			}
		});
		
		containerPopup.add(menuItem);
		
		
		/* Insert new row after */
		
		menuItem = new HelpfulMenuItem("Insert new row after", false,
			"Insert a new (empty) row after the current row.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				insertNewAfter();
			}
		});
		
		containerPopup.add(menuItem);
		
		
		/* Indent row */
		
		menuItem = new HelpfulMenuItem("Indent row", false,
			"Make the current row a child of the row preceding it.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				demote();
			}
		});
		
		containerPopup.add(menuItem);
		
		
		/* Outdent row */
		
		menuItem = new HelpfulMenuItem("Outdent row", false,
			"Make this row a child of its parent's parent.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				promote();
			}
		});
		
		containerPopup.add(menuItem);
		
		
		/* Insert column before */
		
		menuItem = new HelpfulMenuItem("Insert column before", false,
			"Add an " +
			HelpWindow.createHref("Attributes", "Attribute") +
			" to the hierarchy, to be displayed prior to the current column.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int[] columnIndexes = getSelectedColumns();
				if (columnIndexes.length == 0)
				{
					ErrorDialog.showErrorDialog(getViewPanel(),
						"No cells selected");
					return;
				}
				
				if (columnIndexes.length > 1)
				{
					ErrorDialog.showErrorDialog(getViewPanel(),
						"May only select one cell at a time to edit");
					return;
				}
				
				for (int colIndex : columnIndexes) insertAttributeBeforeColumn(colIndex);
			}
		});
		
		containerPopup.add(menuItem);
		
		
		/* Insert column after */
		
		menuItem = new HelpfulMenuItem("Insert column after", false,
			"Add an " +
			HelpWindow.createHref("Attributes", "Attribute") +
			" to the hierarchy, to be displayed after the current column.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int[] columnIndexes = getSelectedColumns();
				if (columnIndexes.length == 0)
				{
					ErrorDialog.showErrorDialog(getViewPanel(),
						"No cells selected");
					return;
				}
				
				if (columnIndexes.length > 1)
				{
					ErrorDialog.showErrorDialog(getViewPanel(),
						"May only select one cell at a time to edit");
					return;
				}
				
				for (int colIndex : columnIndexes) insertAttributeAfterColumn(colIndex);
			}
		});
		
		containerPopup.add(menuItem);		
		
		
		/* Remove column */
		
		menuItem = new HelpfulMenuItem("Remove column", false,
			"Delete the " +
			HelpWindow.createHref("Attributes", "Attribute") +
			" from the hierarchy: all values for this Attribute will be deleted.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int[] columnIndexes = getSelectedColumns();
				if (columnIndexes.length == 0)
				{
					ErrorDialog.showErrorDialog(getViewPanel(),
						"No cells selected");
					return;
				}
				
				if (columnIndexes.length > 1)
				{
					ErrorDialog.showErrorDialog(getViewPanel(),
						"May only select one cell at a time to edit");
					return;
				}
				
				for (int colIndex : columnIndexes) deleteAttributeForColumn(colIndex);
			}
		});
		
		containerPopup.add(menuItem);

		
		/* Rename column */
		
		menuItem = new HelpfulMenuItem("Rename column", false,
			"Rename the " +
			HelpWindow.createHref("Attributes", "Attribute") +
			" that is depicted by this column.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int[] columnIndexes = getSelectedColumns();
				if (columnIndexes.length == 0)
				{
					ErrorDialog.showErrorDialog(getViewPanel(),
						"No cells selected");
					return;
				}
				
				if (columnIndexes.length > 1)
				{
					ErrorDialog.showErrorDialog(getViewPanel(),
						"May only select one cell at a time to edit");
					return;
				}
				
				for (final int ci : columnIndexes)
				{
					if (ci == 0)
					{
						ErrorDialog.showErrorDialog(getTabularView().getComponent(),
							"Cannot rename the Name column");
						return;
					}
					
					final StringEntryPanel sep = new StringEntryPanel("Enter new column name", false);
					sep.setValueReturner(new StringEntryPanel.ValueReturner()
					{
						public void returnStringValue(String value)
						{
							if (value.length() == 0)
							{
								ErrorDialog.showErrorDialog(sep,
									"Cannot set column name to a null string");
								return;
							}
							
							renameAttributeForColumn(ci, value);
							sep.dispose();
						}
					});
					sep.show();
				}
			}
		});
		
		containerPopup.add(menuItem);
	}
	
	
  /* From VisualComponent */
	

	public String getNodeId()
	throws
		NoNodeException
	{
		return nodeSer.getNodeId();
	}
	
	
	public String getName()
	{
		return nodeSer.getName();
	}
	
	
	public String getFullName()
	{
		return nodeSer.getFullName();
	}
	

	public ImageIcon getImageIcon() { return imageIcon; }
	
	
	public void setImageIcon(ImageIcon imgIcon) { this.imageIcon = imgIcon; }
		

	public abstract String getDescriptionPageName();


	public int getSeqNo()
	{
		// Return the ordinal position (beginning with 1) of this Visual
		// among its parent's immediate children, regardless of which children
		// are showing.
		int index = getTreeTableModel().getIndexOfChild(getParentObject(), this);
		if (index < 0) return 0;
		return index+1;
	}


	public boolean contains(VisualComponent comp)
	throws
		NoNodeException
	{
		TreeTableNode parent = (TreeTableNode)comp;
		for (;;)
		{
			parent = parent.getParent();
			if (parent == null) return false;
			if (parent == this) return true;
		}
	}
	
	
	public boolean isOwnedByMotifDef()
	{
		return getNodeSer().ownedByMotifDef();
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		return getNodeSer().ownedByActualTemplateInstance();
	}
	

	public String[] getChildIds()
	{
		return nodeSer.getChildNodeIds();
	}
	
	
	public void nameChangedByServer(String newName, String newFullName)
	{
		NodeSer nodeSer = this.getNodeSer();
		if (getTabularView().getRootNodeSer() == nodeSer)
		{
			getTabularView().setName(newName);
			if (nodeSer instanceof NodeDomainSer) nodeSer.setDomainName(newName);
		}
		
		this.nodeSer.setName(newName);
		this.nodeSer.setFullName(newFullName);
		
		
		// Update the name in the tab. Name field will be refreshed on repaint.
		
		if (getTabularView().getRootNodeSer().getNodeId().equals(this.getNodeId()))
		{
			PanelManager panelManager = this.getTabularView().getPanelManager();
			int id = panelManager.idOfPanel((Container)(this.getTabularView()));
			if (id < 0) return;
			
			panelManager.setTitleAt(id, newName);
		}
		
		viewPanel.repaint();
	}
	
	
	public void nodeDeleted()
	{
		try { getTabularView().removeVisual(this); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
	public void notifyCrossReferenceCreated(String toNodeId, String crossRefId)
	{
		NodeSer ser;
		try { ser = getModelEngine().getNode(crossRefId); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this.viewPanel, ex);
			return;
		}
		
		if (! (ser instanceof CrossReferenceSer)) throw new RuntimeException(
			"Node with Id " + crossRefId + " is not a CrossReference");
		CrossReferenceSer crSer = (CrossReferenceSer)ser;
		getClientView().notifyCrossReferenceCreated(crSer);
	}
		
		
	public void notifyCrossReferenceDeleted(String toNodeId, String crossRefId)
	{
		getClientView().notifyCrossReferenceDeleted(this.getNodeId(), toNodeId, crossRefId);
	}
		
		
	public View getClientView()
	{
		return viewPanel;
	}
	
	
	public ModelEngineRMI getModelEngine()
	{
		return viewPanel.getModelEngine();
	}
	
	
	public NodeSer getNodeSer()
	{
		return nodeSer;
	}
	
	
	public void setNodeSer(NodeSer nodeSer)
	throws
		NoNodeException
	{
		this.nodeSer = (HierarchySer)nodeSer;
	}
	
	
	public void update()
	{
		NodeSer nodeSer = null;
		try { nodeSer = getClientView().getModelEngine().getNode(this.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(getViewPanel(), 
				ThrowableUtil.getAllMessages(ex),
				"Error while updating Visual " + this.getName(), ex);
		}
		
		this.update(nodeSer);
		
		depopulateChildren();
		
		if (! getAsIcon())
		{
			try { populateChildren(); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
	}
	
	
	public void update(NodeSer newNodeSer)
	{
		this.nodeSer = (HierarchySer)newNodeSer;

		this.setUnpropagated(false);
		this.setInconsistent(false);
		
		try { this.refreshRedundantState(); }  // Needs to re-construct this Visual.
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(getViewPanel(), 
				ThrowableUtil.getAllMessages(ex),
				"Error while updating Visual " + this.getName(), ex);
					
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	public void redraw() { getTabularView().repaintVisual(this); }
	
	
	public void refresh() { update(); redraw(); }
	

	public java.awt.Point getLocation()
	{
		return new java.awt.Point(0, getTreeTablePanelViewBase().getYPosition(this));
	}
	
	
	public int getX() { return 0; }
	
	
	public int getY() {return getTreeTablePanelViewBase().getYPosition(this); }
	
	
	public void refreshLocation() {}  // NA

	
	public void refreshSize() {}  // NA

	
	public void setLocation() throws Exception {}  // NA
	
	
	public void setSize() throws Exception {}  // NA


	/**
	 * Reconstruct this Visual, in place, without removing it from the table.
	 * Assume that the Visual has the correct number of columns.
	 */
	 
	public void refreshRedundantState()
	throws
		Exception
	{
		JXTreeTable treeTable = viewPanel.getTreeTable();
		String nodeId = getNodeId();
		AttributeSer[] attrSers = getModelEngine().getAttributesForId(getNodeId());
		String scenarioId = getScenarioId();
		if (scenarioId == null)
			values = getModelEngine().getAttributeDefaultValuesForNode(nodeId);
		else
			values = getModelEngine().getAttributeValuesForNode(nodeId, scenarioId);
		
		int colIndex = 1;
		
		for (AttributeSer attrSer : attrSers)
		{
			try
			{
				int index = getColumnIndexForName(attrSer.getName());
				if (index == 0)
				{
					throw new Exception(
						"Did not find column with name '" + attrSer.getName() + "'");
				}
				if (index != colIndex) throw new Exception(
					"Unexpected column name: '" + attrSer.getName() + "' " +
					"at column index " + index + " - expected to be at index " + colIndex);
			}
			catch (IllegalArgumentException ex)  // column not found
			{
				throw new Exception("Incorrect number of columns", ex);
			}
			
			setValueAt(values[colIndex-1], colIndex);
			colIndex++;
		}
		
		// Remove remaining columns.
		int nCols = treeTable.getColumnCount();
		for (; colIndex < nCols; colIndex++)
		{
			treeTable.removeColumn(treeTable.getColumn(colIndex));
		}
	}
	
	
	/**
	 * Search for the column with the specified name and return its index.
	 * Column 0 is the name column, but that column cannot be searched for.
	 * If the column name is not found, return 0.
	 * Case sensitive.
	 */
	 
	protected int getColumnIndexForName(String columnName)
	{
		DefaultTreeTableModel model = getTreeTableModel();
		int nCols = model.getColumnCount();
		for (int index = 1; index < nCols; index++)
		{
			if (model.getColumnName(index).equals(columnName)) return index;
		}
		return 0;
	}
	
	
	protected Serializable[] getAttributeValues()
	throws
		Exception
	{
		return getModelEngine().getAttributeDefaultValuesForNode(getNodeId());
	}

	
	public boolean getAsIcon() {  return false; }  // NA

	
	public void setAsIcon(boolean yes) {}  // NA
	

	public boolean useBorderWhenIconified()
	{
		return false;
	}
	
	
	/**
	 * Implementation is recursive, so that the View model will populate any
	 * nested rows.
	 */
	 
	public void populateChildren()
	throws
		ParameterError,
		Exception
	{
		String[] childNodeIds = getNodeSer().getChildNodeIds();
		for (String childNodeId : childNodeIds)
		{
			// must be added in sequence.
			Set<VisualComponent> newChildren = getClientView().addNode(childNodeId);
			for (VisualComponent newChild : newChildren) newChild.populateChildren();
		}
	}
	
	
	public void depopulateChildren()
	{
		Enumeration childVisuals = this.children();
		List childrenCopy = new Vector();
		for (; childVisuals.hasMoreElements();)
		{
			childrenCopy.add(childVisuals.nextElement());
		}
		
		for (Object child : childrenCopy) try
		{
			getClientView().removeVisual((VisualComponent)child);
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(getViewPanel(),
				ThrowableUtil.getAllMessages(ex), "While removing child visuals", ex);
		}
	}
	
	
	public Set<VisualComponent> getChildVisuals()
	{
		Set<VisualComponent> childVisualSet = new HashSet<VisualComponent>();
		Enumeration childVisuals = this.children();
		for (; childVisuals.hasMoreElements();)
			childVisualSet.add((VisualComponent)(childVisuals.nextElement()));
		return childVisualSet;
	}
	
	
	public Object getParentObject()
	{
		return getParent();
	}
	
	
	public Container getContainer() { return viewPanel; }
	
	
	public Component getComponent() { return viewPanel; }
	
	
	public void highlight(boolean yes)
	{
		int rowNo;
		try { rowNo = getRowNo(); }
		catch (NotVisible nv) { throw new RuntimeException(nv); }

		getTreeTable().addRowSelectionInterval(rowNo, rowNo);
	}


	public int getRowNo()
	throws
		NotVisible
	{
		TreePath path = new TreePath(getPathToRoot());
		int rowNo = getTreeTable().getRowForPath(path);
		if (rowNo < 0) throw new NotVisible(this.getFullName());
		return rowNo;
	}
	
	
	protected TreeTableNode[] getPathToRoot()
	{
		List<TreeTableNode> nodes = new Vector<TreeTableNode>();
		TreeTableNode node = this;
		for (;;)
		{
			nodes.add(0, node);
			node = node.getParent();
			if (node == null) break;
		}
		
		return nodes.toArray(new TreeTableNode[nodes.size()]);
	}
	
	
	public TreeTablePanelViewBase getTreeTablePanelViewBase() { return viewPanel; }
	
	
	public TabularView getTabularView() { return (TabularView)viewPanel; }
	
	
	public ViewPanelBase getViewPanel() { return viewPanel; }
	
	
	protected JXTreeTable getTreeTable()
	{
		return getTabularView().getTreeTable();
	}
	
	
	protected DefaultTreeTableModel getTreeTableModel()
	{
		return getTabularView().getTreeTableModel();
	}
	
	
	public boolean isHighlighted()
	{
		int rowNo;
		try { rowNo = getRowNo(); }
		catch (NotVisible nv) { throw new RuntimeException(nv); }

		int[] selectedRows = getTreeTable().getSelectedRows();
		for (int selectedRow : selectedRows) if (selectedRow == rowNo) return true;
		return false;
	}
	
	
	public Color getNormalBackgroundColor()
	{
		return getTabularView().getBackgroundNonSelectionColor();
	}
	
	
	public Color getHighlightedBackgroundColor()
	{
		return getTabularView().getBackgroundSelectionColor();
	}
	
	
	public int getWidth()
	{
		JXTreeTable treeTable = getTabularView().getTreeTable();
		return treeTable.getWidth();
	}
	

	public int getHeight()
	{
		JXTreeTable treeTable = getTabularView().getTreeTable();
		try { return treeTable.getRowHeight(getRowNo()); }
		catch (NotVisible nv) { throw new RuntimeException(nv); }
	}
	
	
	public void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		writeScenarioDataAsXML(writer, indentation, scenarioVisual);
		Set<VisualComponent> children = getChildVisuals();
		for (VisualComponent child : children)
			child.writeScenarioDataAsXMLRecursive(writer, indentation+1, scenarioVisual);
	}
	
	
	public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		List<String> attrNames = new Vector<String>();
		List<Serializable> values = scenarioVisual.getAttributeValues(this, attrNames);
		int i = 0;
		for (String attrName : attrNames)
		{
			writer.println(StringUtils.tabs[indentation] + "<Attribute name=\"" +
				attrName + "\" value=\"" + values.get(i++) + "\"/>");
		}
	}


  /* ***************************************************************************
	 ***************************************************************************
	 * Protected accessors.
	 */
	 
	
	protected void setView(GraphicView view) { this.viewPanel = (TreeTablePanelViewBase)view; }
	
	
	//protected int getViewXOriginOffset() { return this.getGraphicView().getXOriginOffset(); }
	
	
	//protected int getViewYOriginOffset() { return this.getGraphicView().getYOriginOffset(); }
	
	
	protected void setImageIcon()
	throws
		IOException
	{
		final String handle = getNodeSer().getIconImageHandle();
		final ImageIcon icon = getClientView().getStoredImage(handle);
		setImageIcon(icon);
	}
	
	
	protected void setImageIcon(String imageIconName)
	throws
		IOException
	{
		setImageIcon(getClientView().getStoredImage(imageIconName));
	}
	
	
	protected boolean isPopulated() { return populated; }
	
	
	protected void setPopulated(boolean pop) { this.populated = pop; }
	
	
	protected Dimension getNormalSize() { return visualNormalSize; }
	
	
	protected long getLastUpdated() { return nodeSer.getLastUpdated(); }
	
	
	protected int getVersionNumber() { return nodeSer.getVersionNumber(); }
	
	
	protected boolean isInconsistent() { return inconsistent; }
	
	
	protected void setInconsistent(boolean newValue)
	{
		this.inconsistent = newValue;
	}
	
	
	protected boolean isUnpropagated() { return unpropagated; }
	
	protected void setUnpropagated(boolean newValue)
	{
		this.unpropagated = newValue;
	}
	
	
	public boolean isEditable(int column) { return true; }
	
	
	
  /* ***************************************************************************
	 ***************************************************************************
	 * From AbstractMutableTreeTableNode.
	 */
	
	
	public int getColumnCount() { return values.length + 1; }
	
	
	/**
	 * Merely an accessor. Retrieve the value that is maintained by this Visual
	 * for the specified column. Column 0 is the 'Name' column.
	 */
	 
	public Object getValueAt(int column)
	{
		if (column == 0) return getName();
		return values[column-1];
	}
	
	
	/**
	 * An accessor. Sets the value that is maintained by this Visual for the
	 * specified column. Column 0 is the 'Name' column.
	 */
	 
	public void setValueAt(Object aValue, int column)
	{
		if (column == 0)  // set Node's name.
		{
			setName(aValue.toString());
			return;
		}
		
		values[column-1] = aValue;
		if ((aValue != null) && (! (aValue instanceof Serializable)))
		{
			ErrorDialog.showErrorDialog(getViewPanel(),
				"Column no. " + column + " is not Serializable; value=" + aValue +
				" (" + aValue.getClass().getName() + ")");
			return;
		}
	}
	
	
	
  /* ***************************************************************************
	 ***************************************************************************
	 * Implementation methods.
	 */
	 
	
	/**
	 * Find all of the children of this Visual that match the specified Node Id.
	 * Include this Visual if it matches. Return the result in 'matchingVisuals'.
	 * If 'nodeId' is null then include all child Visuals.
	 * Recursive.
	 */
	 
	Set<VisualComponent> identifyVisualComponents(Set<VisualComponent> matchingVisuals,
		String nodeId)
	{
		if ((nodeId == null) || this.getNodeId().equals(nodeId)) matchingVisuals.add(this);
		Enumeration e = children();
		for (; e.hasMoreElements();)
		{
			MutableTreeTableNode child = (MutableTreeTableNode)(e.nextElement());
			if (! (child instanceof TabularVisualJ)) throw new RuntimeException(
				"TreeTable node is not a TabularVisualJ");
			((TabularVisualJ)child).identifyVisualComponents(matchingVisuals, nodeId);
		}

		return matchingVisuals;
	}
	
	
	VisualComponent identifyVisualComponent(String nodeId)
	{
		if (this.getNodeId().equals(nodeId)) return this;
		Enumeration e = children();
		for (; e.hasMoreElements();)
		{
			MutableTreeTableNode child = (MutableTreeTableNode)(e.nextElement());
			if (! (child instanceof TabularVisualJ)) throw new RuntimeException(
				"TreeTable node is not a TabularVisualJ");
			VisualComponent vis = 
				((TabularVisualJ)child).identifyVisualComponent(nodeId);
			if (vis != null) return vis;
		}
		
		return null;
	}
	
	
	Set<VisualComponent> identifyVisualComponents(Set<VisualComponent> matchingVisuals,
		Class c)
	throws
		ParameterError
	{
		if (c == null) return matchingVisuals;
		if (c.isAssignableFrom(this.getClass())) matchingVisuals.add(this);
		Enumeration e = children();
		for (; e.hasMoreElements();)
		{
			MutableTreeTableNode child = (MutableTreeTableNode)(e.nextElement());
			if (! (child instanceof TabularVisualJ)) throw new RuntimeException(
				"TreeTable node is not a TabularVisualJ");
			((TabularVisualJ)child).identifyVisualComponents(matchingVisuals, c);
		}

		return matchingVisuals;
	}
	
	
	Set<VisualComponent> identifyVisualComponents(Set<VisualComponent> matchingVisuals)
	throws
		ParameterError
	{
		matchingVisuals.add(this);
		Enumeration e = children();
		for (; e.hasMoreElements();)
		{
			MutableTreeTableNode child = (MutableTreeTableNode)(e.nextElement());
			if (! (child instanceof TabularVisualJ)) throw new RuntimeException(
				"TreeTable node is not a TabularVisualJ");
			((TabularVisualJ)child).identifyVisualComponents(matchingVisuals);
		}

		return matchingVisuals;
	}
	
	
	void addWhatIsThisMenuItem(JPopupMenu containerPopup, final int x, final int y)
	{
		VisualHelper.addWhatIsThisMenuItem(viewPanel, containerPopup, this, x, y);
	}
	
	
	void addCrossRefMenuItem(JPopupMenu containerPopup, final int x, final int y)
	{
		VisualHelper.addCrossRefMenuItem(viewPanel, containerPopup, this, x, y);
	}
	
	
	TreePath[] getSelectionPaths()
	{
		return getTabularView().getTreeTable().getTreeSelectionModel().getSelectionPaths();
	}
	
	
	/**
	 * 
	 */
	 
	int[] getSelectedColumns()
	{
		return getTabularView().getTreeTable().getColumnModel().getSelectedColumns();
	}
	
	
	/**
	 * Return the Node Id of the Attribute that is depicted by the specified column.
	 * The 'colIndex' argument is the index of the column (beginning with 0).
	 * Note that the first column (column 0) is always the Node name column, and so
	 * it does not correspond to an Attribute: so if the argument is 0, that refers
	 * to the name column, and this method will throw a ParameterError.
	 */
	 
	protected String getAttributeIdForColumn(int colIndex)
	throws
		ParameterError  // if the column specified is < 1.
	{
		if (colIndex < 1) throw new ParameterError("No Attribute at column " + colIndex);
		String[] attrNodeIds = getNodeSer().getAttributeNodeIds();
		return attrNodeIds[colIndex-1];
	}
	
	
	NodeDomainSer getDomainSer()
	{
		String domainId = getNodeSer().getDomainId();
		VisualComponent domainVisual = identifyVisualComponent(domainId);
		if (domainVisual == null)
		{
			// Get from server.
			NodeSer ns;
			try { ns = getModelEngine().getNode(domainId); }
			catch (Exception ex) { throw new RuntimeException(ex); }
			if (! (ns instanceof NodeDomainSer)) throw new RuntimeException(
				"Node expected to be a DomainSer but is a " + ns.getClass().getName());
			return (NodeDomainSer)ns;
		}
		else
			return (NodeDomainSer)(domainVisual.getNodeSer());
	}

	
	public String toString()
	{
		return getName() + " (" + getClass().getName() + ")";
	}
	
	
	
  /* ***************************************************************************
	 ***************************************************************************
	 * Server action methods. These methods update the server. They do not update
	 * the client Model. The client Model gets updated when the server notifies
	 * the client that a Node has changed. This results in VisualComponent and View
	 * methods being called. Those methods should update the Model by calling
	 * setValueAt(...) and other methods on the TreeTableModel or the TreeTableNode.
	 */
	 
	
	/**
	 * Delete the server Node that is depicted by this TreeTableNode.
	 */
	
	void deleteRow()
	{
		try
		{
			try { getModelEngine().deleteNode(false, getNodeId()); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(getViewPanel(),
					w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
					== JOptionPane.YES_OPTION)
				{
					getModelEngine().deleteNode(true, getNodeId());
				}
			}
		}
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	/**
	 * Display an editor to enable the user to edit the specified field (column)
	 * of this TreeTableNode. The value is not written to the TreeTableNode: rather,
	 * the corresponding server Node is updated. This TreeTableNode's column gets
	 * updated when the server sends a Notice to the client that results in
	 * a refresh of this TreeTableNode.
	 */
	
	void editCell(int colIndex)
	throws
		NotVisible
	{
		String columnName = getTreeTableModel().getColumnName(colIndex);
		FieldEditor editor = getTabularView().getEditor(columnName, getRowNo());
		editor.show();
	}
	
	
	/**
	 * Request server to clear each field of the Node depicted by this Visual.
	 * (Not currently used.)
	 */
	 
	void clearRow()
	{
		int noOfCols = getTreeTableModel().getColumnCount();
		for (int colIndex = 0; colIndex < noOfCols; colIndex++)
		{
			clearCell(colIndex);
		}
	}
	
	
	/**
	 * Employ an editor to clear the specified field. As for editCell(), the
	 * TreeTableNode field is not cleared immediately: it is only cleared when
	 * the server sends a Notice to the client causing this TreeTableNode to
	 * refresh.
	 */
	
	void clearCell(int colIndex)
	{
		String columnName = getTreeTableModel().getColumnName(colIndex);
		FieldEditor editor;
		try { editor = getTabularView().getEditor(columnName, getRowNo()); }
		catch (NotVisible nv) { throw new RuntimeException(nv); }
		
		try { editor.clear(); }
		catch (Warning w)
		{
			JOptionPane.showMessageDialog(getViewPanel(),
				ThrowableUtil.getAllMessages(w), "While clearing cell", JOptionPane.WARNING_MESSAGE);
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(getViewPanel(),
				ThrowableUtil.getAllMessages(ex), "While clearing cell", ex);
		}
	}
	
	
	/**
	 * Create a new row with the same structure and data as this row. Insert the
	 * new row under this one.
	 */
	 
	void duplicateRow()
	{
		try
		{
			try { getModelEngine().cloneListNodeAfter(false, getNodeId()); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(getViewPanel(),
					w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
					== JOptionPane.YES_OPTION)
				{
					getModelEngine().cloneListNodeAfter(true, getNodeId());
				}
			}
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}

	
	/**
	 * Request the server to delete the Node depicted by this TreeTableNode.
	 * In addition, save its fields in the system paste buffer
	 * (clipboard).
	 *
	
	void cutRow()
	{
		copyRow();
		deleteRow();
	}
	
	
	final DataFlavor NodeIdFlavor = new DataFlavor(String.class, "Node Id");
	
	
	
	 * Save this Node's data fields in the system paste buffer (clipboard).
	 *
	
	void copyRow()
	{
		final String copiedNodeId = getNodeId();
		
		// Copy nodeData to paste buffer.
		
		Clipboard clipboard = getViewPanel().getToolkit().getSystemClipboard();
		
		clipboard.setContents(new Transferable()
		{
			public Object getTransferData(DataFlavor flavor)
			throws
				UnsupportedFlavorException,
				IOException
			{
				if (flavor.equals(NodeIdFlavor)) return copiedNodeId;
				
				throw new UnsupportedFlavorException(flavor);
			}
			
			public DataFlavor[] getTransferDataFlavors()
			{
				return new DataFlavor[] { NodeIdFlavor, DataFlavor.getTextPlainUnicodeFlavor() };
			}
			
			public boolean isDataFlavorSupported(DataFlavor flavor)
			{
				return 
					flavor.equals(NodeIdFlavor) ||
					flavor.isFlavorTextType();
			}
		},
		new ClipboardOwner()
		{
			public void lostOwnership(Clipboard clipboard, Transferable contents)
			{
			}
		});
	}
	
	
	
	 * Paste the values from the system paste buffer (clipboard) into a new
	 * row. The row should be added after this row, unless this row is the root, 
	 * in which case the new row should be added as the first child of root.
	 *
	
	void pasteRow()
	{
		Clipboard clipboard = getViewPanel().getToolkit().getSystemClipboard();
		
		try
		{
			String copiedNodeId = (String)(clipboard.getData(NodeIdFlavor));
			if (copiedNodeId == null)
			{
				ErrorDialog.showErrorDialog(getViewPanel(),
					"No Node data in paste buffer");
				return;
			}
			
			String parentId;
			String priorNodeId;
			if (this == getTabularView().getOutermostVisual())
			{
				parentId = this.getNodeId();
				priorNodeId = null;
			}
			else
			{
				parentId = ((TabularVisualJ)(this.getParent())).getNodeId();
				priorNodeId = this.getNodeId();
			}
			
			try { getModelEngine().createListNodeAfter(false, parentId, priorNodeId,
				getScenarioId(), copiedNodeId); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(getViewPanel(),
					w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
					== JOptionPane.YES_OPTION)
				{
					getModelEngine().createListNodeAfter(true, parentId, priorNodeId,
						getScenarioId(), copiedNodeId);
				}
			}
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}*/
	
	
	/**
	 * Request the server to insert a "blank" Node before this Node (actually, the
	 * server Node depected by this TreeTableNode).
	 */
	
	void insertNewBefore()
	{
		/*
		If this Node is the root, return.
		else
			insert a new blank peer before this node.
		*/
		
		if (this == getTabularView().getOutermostVisual()) return;

		//String nodeKind = this.getDemotionNodeKind();
		TreeTableNode parent = getParent();
		String parentId = ((TabularVisualJ)parent).getNodeId();
		String nextNodeId = this.getNodeId();

		NodeSer newNodeSer;
		try
		{
			try { newNodeSer = getModelEngine().createListNodeBefore(false, parentId,
				nextNodeId, getScenarioId(), getNodeId()); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(getViewPanel(),
					w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
					== JOptionPane.YES_OPTION)
				{
					newNodeSer = getModelEngine().createListNodeBefore(true, parentId,
						nextNodeId, getScenarioId(), null);
				}
			}
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
	/**
	 * Request the server to insert a "blank" Node after this Node (actually, the
	 * server Node depected by this TreeTableNode).
	 */
	
	void insertNewAfter()
	{
		/*
		If this Node is the root,
			insert a new blank child as the first child.
		else
			insert a new blank peer after this node.
		*/
		
		String parentId;
		String priorNodeId;
		
		if (this == getTabularView().getOutermostVisual())
		{
			parentId = this.getNodeId();
			priorNodeId = null;
		}
		else
		{
			parentId = ((TabularVisualJ)(this.getParent())).getNodeId();
			priorNodeId = this.getNodeId();
		}
		
		try
		{
			try { getModelEngine().createListNodeAfter(false, parentId, priorNodeId,
				getScenarioId(), null); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(getViewPanel(),
					w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
					== JOptionPane.YES_OPTION)
				{
					getModelEngine().createListNodeAfter(true, parentId, priorNodeId,
						getScenarioId(), getNodeId());
				}
			}
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
	void demote()
	{
		// If this TreeTableNode has no peer above it, return.
		
		if (this == getTabularView().getOutermostVisual()) return;
		int thisIndex = getTreeTableModel().getIndexOfChild(this.getParent(), this);
		if (thisIndex == 0) return;  // no prior peers.
		

		try
		{
			try { getModelEngine().demoteListNode(false, getNodeId()); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(getViewPanel(),
					w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
					== JOptionPane.YES_OPTION)
				{
					getModelEngine().demoteListNode(true, getNodeId());
				}
			}
		}
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	void promote()
	{
		// If this TreeTableNode is the table root or its parent is the table root, return.
		
		TreeTableNode root = (TreeTableNode)(getTabularView().getOutermostVisual());
		if (this == root) return;
		if (this.getParent() == root) return;
		
		try
		{
			try { getModelEngine().promoteListNode(false, getNodeId()); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(getViewPanel(),
					w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
					== JOptionPane.YES_OPTION)
				{
					getModelEngine().promoteListNode(true, getNodeId());
				}
			}
		}
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	void insertAttributeBeforeColumn(int colIndex)
	{
		if (colIndex == 0)
		{
			ErrorDialog.showErrorDialog(getViewPanel(),
				"Cannot insert a column before the Name column");
			return;
		}
		
		String attrNodeId;
		try { attrNodeId = getAttributeIdForColumn(colIndex); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		try
		{
			NodeSer nodeSer = getNodeSer();
			try { getModelEngine().createHierarchyDomainAttributeBefore(false,
				nodeSer.getDomainId(), attrNodeId, null, null); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(getViewPanel(),
					w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
					== JOptionPane.YES_OPTION)
				{
					getModelEngine().createHierarchyDomainAttributeBefore(true,
						nodeSer.getDomainId(), attrNodeId, null, null);
				}
			}
		}
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	void insertAttributeAfterColumn(int colIndex)
	{
		try
		{
			String thisNodeId = getNodeId();
			if (colIndex == 0)
			{
				int noOfAttrs = getModelEngine().getNoOfAttributes(thisNodeId);
				if (noOfAttrs == 0)
				{
					NodeSer nodeSer = getNodeSer();
					try { getModelEngine().createHierarchyDomainAttributeAfter(false,
						nodeSer.getDomainId(), null, null, null); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(getViewPanel(),
							w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
							== JOptionPane.YES_OPTION)
						{
							getModelEngine().createHierarchyDomainAttributeAfter(true,
								nodeSer.getDomainId(), null, null, null);
						}
					}
				}
				else
				{
					String attrNodeId = getAttributeIdForColumn(1);
					try { getModelEngine().createAttributeBefore(false, attrNodeId); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(getViewPanel(),
							w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
							== JOptionPane.YES_OPTION)
						{
							getModelEngine().createAttributeBefore(true, attrNodeId);
						}
					}
				}
			}
			else
			{
				String attrNodeId = getAttributeIdForColumn(colIndex);
				try { getModelEngine().createAttributeAfter(false, attrNodeId); }
				catch (Warning w)
				{
					if (JOptionPane.showConfirmDialog(getViewPanel(),
						w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
						== JOptionPane.YES_OPTION)
					{
						getModelEngine().createAttributeAfter(true, attrNodeId);
					}
				}
			}
		}
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	void deleteAttributeForColumn(int colIndex)
	{
		try
		{
			if (colIndex == 0) throw new RuntimeException("Cannot delete the name column");

			String attrNodeId = getAttributeIdForColumn(colIndex);
			try { getModelEngine().deleteNode(false, attrNodeId); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(getViewPanel(),
					w.getMessage(), "Confirm", JOptionPane.YES_NO_OPTION)
					== JOptionPane.YES_OPTION)
				{
					getModelEngine().deleteNode(true, attrNodeId);
				}
			}
		}
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	void renameAttributeForColumn(int colIndex, String newName)
	{
		try
		{
			if (colIndex == 0) throw new RuntimeException("Cannot rename the Name column");

			String attrNodeId = getAttributeIdForColumn(colIndex);
			getModelEngine().setNodeName(true, attrNodeId, newName);
		}
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	void setName(String name)
	{
		try
		{
			try { getModelEngine().setNodeName(false, getNodeId(), name); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(null, w.getMessage(),
					"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
				{
					getModelEngine().setNodeName(true, getNodeId(), name);
				}
			}
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(getViewPanel(), ex);
			setValueAt(getNodeSer().getName(), 0);
		}
	}
}

