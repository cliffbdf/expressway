/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import java.io.File;
import java.io.IOException;
import java.awt.Component;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;


public class FileChooserUtils
{
	public enum FileType { xml, jar }
	
	
	public static File getFileToOpen(Component comp, FileType... fts)
	throws
		IOException
	{
		JFileChooser chooser = getChooser(fts);
		int returnVal = chooser.showOpenDialog(comp);
		if (returnVal != JFileChooser.APPROVE_OPTION) return null;
		
		return chooser.getSelectedFile();
	}
	
	
	public static File getFileToSave(Component comp, FileType... fts)
	throws
		IOException
	{
		JFileChooser chooser = getChooser(fts);
		int returnVal = chooser.showSaveDialog(comp);
		if (returnVal != JFileChooser.APPROVE_OPTION) return null;
		
		return chooser.getSelectedFile();
	}
	
	
	protected static JFileChooser getChooser(FileType... fts)
	throws
		IOException
	{
		JFileChooser chooser;
		boolean includesXML = false;
		boolean includesJAR = false;
		for (FileType ft : fts)
		{
			switch (ft)
			{
			case xml: includesXML = true; break;
			case jar: includesJAR = true; break;
			default: throw new RuntimeException("Unexpected FileType");
			}
		}
		
		FileNameExtensionFilter filter;
		if (includesJAR)
		{
			chooser = getJARFileChooser();
			if (includesXML) filter = new FileNameExtensionFilter(
				"Expressway XML model or motif JAR files", "xml", "XML", "jar", "JAR");
			else filter = new FileNameExtensionFilter(
				"Expressway JAR files", "jar", "JAR");
		}
		else if (includesXML)
		{
			chooser = getXMLFileChooser();
			filter = new FileNameExtensionFilter(
				"Expressway XML model files", "xml", "XML");
		}
		else
			return new JFileChooser();
		
		chooser.setFileFilter(filter);
		
		return chooser;
	}
	
	
	protected static JFileChooser getXMLFileChooser()
	{
		File xmlDir = ClientGlobalValues.mainApplication.getMostRecentXMLDir();
		if (xmlDir == null) return new JFileChooser();
		else return new JFileChooser(xmlDir);
	}
	
	
	protected static JFileChooser getJARFileChooser()
	{
		File jarDir = ClientGlobalValues.mainApplication.getMostRecentJARDir();
		if (jarDir == null) return new JFileChooser();
		else return new JFileChooser(jarDir);
	}
}

