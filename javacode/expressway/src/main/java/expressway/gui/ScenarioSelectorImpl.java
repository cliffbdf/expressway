/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.ClientModel.*;
import expressway.common.VisualComponent;
import expressway.common.ModelAPITypes.*;
import expressway.gui.InfoStrip.*;
import expressway.ser.*;
import expressway.help.*;
import java.io.Serializable;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.ComponentListener;
import java.util.Iterator;
import java.net.URL;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;


/** ************************************************************************
 * A tabbed panel for allowing the user to select from among the set of
 * available Scenarios for the Domain. The tabs representing Scenarios
 * should be adjacent. The tabs for Show/Hide and New
 * are expected to be the zeroth and first tabs, respectively.
 * Each Scenario tab contains an instance of ScenarioInfoStrip.
 */

public class ScenarioSelectorImpl extends JTabbedPane implements ScenarioSelector
{
	public static final int NoOfFixedTabs = 1;
	
	private static final int ShowHideTabIndex = 0;
	private final NodeScenarioView view;
	private ComponentListener componentListener;  // listens for changes
		// in the size of the Container, and then resizes this strip as needed.
	
	public ScenarioSelectorImpl(NodeScenarioView view)
	{
		if (! (view instanceof NodeView)) throw new RuntimeException(
			"view is not a NodeView");
		
		this.view = view;
		setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);
		putClientProperty("Quaqua.TabbedPane.shortenTabs", Boolean.TRUE);
	}
	
	
	/** Accessor. */
	synchronized void setComponentListener(ComponentListener l) { this.componentListener = l; }
	
	/** Accessor. */
	synchronized ComponentListener getComponentListener() { return this.componentListener; }
	
	/** Accessor. */
	View getClientView() { return this.view; }
	
	
	public Component getComponent() { return this; }
	
	
	public Container getContainer() { return this; }
	
	
	public JComponent getJComponent() { return this; }
	
	
  /* From ScenarioSelector */
	
	
	public void setControlPanelTitle(ControlPanel controlPanel, String newName)
	{
		// Find InfoStrip that owns 'controlPanel'.
		int count = getTabCount();
		for (int i = 0; i < count; i++)
		{
			Component comp = getTabComponentAt(i);
			if (! (comp instanceof ScenarioInfoStrip)) continue;
			ScenarioInfoStrip infoStrip = (ScenarioInfoStrip)comp;
			NodeScenarioPanel cp = infoStrip.getScenarioPanel();
			if (cp == controlPanel)
			{
				setTitleAt(i, newName);
				break;
			}
		}
	}
	
	
	public ControlPanel getControlPanel(String name)
	{
		// Find InfoStrip named 'name'.
		int count = getTabCount();
		for (int i = 0; i < count; i++)
		{
			Component comp = getTabComponentAt(i);
			if (! (comp instanceof ScenarioInfoStrip)) continue;
			ScenarioInfoStrip infoStrip = (ScenarioInfoStrip)comp;
			NodeScenarioPanel cp = infoStrip.getScenarioPanel();
			if (cp.getName().equals(name)) return cp;
		}
		return null;
	}
	
	
	public DomainInfoStrip createDomainInfoStrip()
	{
		DomainInfoStrip infoStrip = getNodeView().constructDomainInfoStrip();
		addTab(getNodeView().getOutermostNodeSer().getName(), infoStrip.getComponent());
		setCurrentScenarioPanel();
		return infoStrip;
	}
	
	
	public ScenarioInfoStrip createScenarioInfoStrip(NodeScenarioPanel panel)
	{
		ScenarioInfoStrip infoStrip = getScenarioViewPanel().constructScenarioInfoStrip(panel);
		addTab(panel.getNodeSer().getName(), infoStrip.getComponent());
		setCurrentScenarioPanel();
		return infoStrip;
	}
	
	
	public void removeScenarioInfoStrip(ScenarioInfoStrip sis)
	{
		if (sis == null) return;
		removeTabAt(indexOfComponent(sis.getComponent()));
		setCurrentScenarioPanel();
	}
	
	
  /* Implementation methods */


	NodeScenarioPanel getScenarioPanelById(String id)
	{
		// Find InfoStrip named 'name'.
		int count = getTabCount();
		for (int i = 0; i < count; i++)
		{
			Component comp = getTabComponentAt(i);
			if (! (comp instanceof ScenarioInfoStrip)) continue;
			ScenarioInfoStrip infoStrip = (ScenarioInfoStrip)comp;
			NodeScenarioPanel sp = infoStrip.getScenarioPanel();
			if (sp.getNodeId().equals(id)) return sp;
		}
		return null;
	}
	
	
	protected PanelManager getPanelManager() { return view.getPanelManager(); }
	
	
	protected ModelEngineRMI getModelEngine() { return view.getModelEngine(); }
	
	
	protected NodeSer getNodeSer() { return ((NodeView)view).getOutermostNodeSer(); }
	
	
	protected String getNodeId() { return ((NodeView)view).getOutermostNodeId(); }
	
	
	protected NodeView getNodeView() { return (NodeView)view; }
	
	
	protected NodeScenarioView getScenarioViewPanel() { return (NodeScenarioView)view; }
	
	
	public synchronized void repaint()
	{
		super.repaint();
		NodeScenarioPanel curPanel = getCurrentScenarioPanel();
		if (curPanel != null) curPanel.repaint();
	}
	
	
	/** ********************************************************************
	 * Return the index in this Tabbed Pane at which the Show/Hide tab is
	 * displayed. It should be the first index (0).
	 */
	 
	synchronized int getShowHideTabIndex() { return ShowHideTabIndex; }
	
	
	/** ********************************************************************
	 * Return the index at which the first Scenario tab exists. If there are
	 * no Scenario tabs, return -1.
	 */
	 
	synchronized int getFirstScenarioTabIndex()
	{
		int count = this.getTabCount();
		return (count > NoOfFixedTabs ? NoOfFixedTabs : -1);
	}
	
	
	/** ********************************************************************
	 * Return the index at which the last Scenario tab exists. If there are
	 * no Scenario tabs, return -2.
	 */
	 
	synchronized int getLastScenarioTabIndex()
	{
		int count = this.getTabCount();
		return (count > NoOfFixedTabs ? (count - 1) : -2);
	}
	
	
	/** ********************************************************************
	 * Return the number of tabs that correspond to Scenarios.
	 */
	 
	synchronized int getNoOfScenarioTabs()
	{
		int count = this.getTabCount();
		return Math.max(count - NoOfFixedTabs, 0);
	}
	
	
	/** ********************************************************************
	 * Return the InfoStrip that is depicted by selecting the specified tab.
	 * If there is no InfoStrip at the specified tab index, return null.
	 */
	 
	synchronized InfoStrip getInfoStripAtIndex(int index)
	{
		if (index < NoOfFixedTabs) return null;
		if (index >= this.getTabCount()) return null;
		
		return (InfoStrip)(this.getComponentAt(index));
	}
	
	
	/** ********************************************************************
	 * Return the Scenario Panel that should be displayed (overlaid on the
	 * Domain View) when the specified tab is selected.
	 */
	 
	synchronized NodeScenarioPanel getScenarioPanelAtIndex(int index)
	{
		if (index == 0) return null;
		
		ScenarioInfoStrip infoStrip = (ScenarioInfoStrip)(this.getInfoStripAtIndex(index));
		if (infoStrip == null) return null;
		
		return infoStrip.getScenarioPanel();
	}
	
	
	/** ********************************************************************
	 * Return the NodeScenarioPanel that precedes the specified one (in the
	 * sequence of tabs), or null if none precedes it.
	 */
	 
	synchronized NodeScenarioPanel getPriorScenarioPanel(NodeScenarioPanel panel)
	{
		int tabIndex = getScenarioTab(panel);
		if (tabIndex < 0) return null;
		return getScenarioPanelAtIndex(tabIndex-1);
	}
	
	
	synchronized NodeScenarioPanel getNextScenarioPanel(NodeScenarioPanel panel)
	{
		int tabIndex = getScenarioTab(panel);
		if (tabIndex < 0) return null;
		return getScenarioPanelAtIndex(tabIndex+1);
	}
	

	synchronized boolean isSelected(InfoStrip infoStrip)
	{
		return this.getSelectedComponent() == infoStrip;
	}
	
	
	synchronized public void setCurrentScenarioPanel()
	{
		setCurrentScenarioPanel(getCurrentScenarioPanel());
	}
	
	
	/** ********************************************************************
	 * Make the specified panel the currently selected Scenario Panel.
	 * 
	 */
	 
	synchronized public void setCurrentScenarioPanel(NodeScenarioPanel panel)
	{
		if (panel == null)
		{
			this.setSelectedIndex(ShowHideTabIndex);
			return;
		}
		
		int tabIndex = this.getScenarioTab(panel);
		this.setSelectedIndex(tabIndex);
		getClientView().getDisplayArea().setComponentZOrder(panel.getComponent(), 0);
		//panel.getComponent().repaint();
		//getClientView().getDisplayArea().repaint();
	}


	/** ********************************************************************
	 * Return the current Scenario. May be null if no Scenario is currently
	 * visible.
	 */
	
	synchronized public NodeScenarioPanel getCurrentScenarioPanel()
	{
		int tabIndex = this.getSelectedIndex();
		
		if (tabIndex < 0) return null;
		if (tabIndex == 0) return null;
		
		Component c = this.getComponentAt(tabIndex);
		if (c == null) return null;
		
		ScenarioInfoStrip infoStrip = (ScenarioInfoStrip)c;
		
		return infoStrip.getScenarioPanel();
	}


	/** ********************************************************************
	 * Find and return the index of the tab that contains a Visual for the
	 * specified Scenario. There should not be more than one, but if there is, 
	 * simply return the first one. If not found, return -1.
	 */
	 
	synchronized public int getScenarioTab(NodeScenarioPanel panel)
	{
		for (int tabNo = 0; tabNo < this.getTabCount(); tabNo++)
		{
			Component c = this.getComponentAt(tabNo);
			if (c == null) continue;
			if (! (c instanceof ScenarioInfoStrip)) continue;
			
			ScenarioInfoStrip infoStrip = (ScenarioInfoStrip)c;
			
			NodeScenarioPanel p = infoStrip.getScenarioPanel();
			
			if (p == panel) return tabNo;
		}
		
		return -1;
	}
	
	
	/** ********************************************************************
	 * Set the text on the tab corresponding to the specified Attribute Value
	 * Panel. If the Panel is not found then simply return.
	 */
	 
	synchronized public void setScenarioName(NodeScenarioPanel panel, String name)
	{
		int tabIndex = this.getScenarioTab(panel);
		if (tabIndex < 0) return;
		this.setTitleAt(tabIndex, name);
	}


	/** ********************************************************************
	 * For iterating over the InfoStrips owned by this Scenario Selection Strip.
	 */
	 
	synchronized Iterator<ScenarioInfoStrip> getScenarioInfoStripIterator()
	{
		return new Iterator<ScenarioInfoStrip>()
		{
			private int nextTab = getFirstScenarioTabIndex();
			private int lastTab = getLastScenarioTabIndex();
			
			public boolean hasNext()
			{
				if (nextTab < 0) return false;
				if (nextTab > lastTab) return false;
				
				return true;
			}
			
			public ScenarioInfoStrip next()
			{
				return (ScenarioInfoStrip)(getComponentAt(nextTab++));
			}
			
			public void remove()
			{
				throw new RuntimeException("Not supported");
			}
		};
	}
	
	
	/** ********************************************************************
	 * For iterating over the Attribute Value Panels that are owned by the
	 * InfoStrips that are owned by this Scenario Selection Strip.
	 */
	 
	synchronized Iterator<NodeScenarioPanel> getScenarioPanelIterator()
	{
		return new Iterator<NodeScenarioPanel>()
		{
			private Iterator<ScenarioInfoStrip> infoStripIterator = getScenarioInfoStripIterator();
			
			public boolean hasNext()
			{
				return infoStripIterator.hasNext();
			}
			
			public NodeScenarioPanel next()
			{
				return infoStripIterator.next().getScenarioPanel();
			}
			
			public void remove()
			{
				throw new RuntimeException("Not supported");
			}				
		};
	}
}

