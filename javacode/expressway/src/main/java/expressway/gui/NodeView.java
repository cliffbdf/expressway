/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.ListenerRegistrar;
import expressway.common.ClientModel.PeerListener;
import expressway.common.VisualComponent.SimulationVisual;
import expressway.common.VisualComponent.ScenarioVisual;
import expressway.gui.ControlPanel;
import expressway.gui.InfoStrip.*;
import expressway.ser.NodeSer;
import expressway.ser.ScenarioSer;
import javax.swing.*;
import java.util.Set;
import java.io.IOException;
import java.awt.Container;
import java.awt.Component;
import java.awt.Image;
import java.awt.Window;


/**
 * A View that has only one outermost Visual.
 */

public interface NodeView
	extends View,
	VisualComponent  // for the Domain
{
	ScenarioVisual showScenario(String scenarioId) throws Exception;
	
	
	/** Return the ScenarioVisual that depicts the specified Scenario of
		this DomainVisual instance. */
	ScenarioVisual getScenarioVisual(String scenarioId);
	
	
	void setOutermostNodeSer(NodeSer nodeSer)
	throws
		Exception;
		
		
	/** ************************************************************************
	 * Return the NodeSer for the top-most or outmost Node
	 * that is depicted in this View.
	 */
	
	NodeSer getOutermostNodeSer();
	
	
	String getOutermostNodeId();
	
	
	/** ************************************************************************
	 * Return the Visual depicting the outermost NodeSer. If the View itself
	 * implements VisualComponent, do not return that: only return the Visual
	 * that is the root of the hierarchy of Visuals within the View.
	 */
	 
	VisualComponent getOutermostVisual();
	
	
	void initializeNodeSer(NodeSer nodeSer);
	
	
	/** ************************************************************************
	 * Identify the client-side Visual Components in this View that
	 * represent the specified server-side Persistent Node. 
	 * If the View is itself a VisualComponent, do not include it. If there is
	 * more than one (besides the View itself), throw a RuntimeException.
	 * If there are none return null.
	 */
	 
	VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError;  // if the node Id is null or invalid.
	
	
	/** ************************************************************************
	 * Return the formats into which this View may be rendered and sent via
	 * HTTP. Formats include 'svg', 'html', and 'applet'.
	 */
	 
	String[] getAvailableHTTPFormats(String[] descriptions)
	throws
		Exception;
	
	
	/** ************************************************************************
	 * Return an HTTP URL that can be sent to Expressway's HTTP server to request
	 * a depiction of this View in the specified format.
	 */
	 
	String getWebURLString(String format)
	throws
		Exception;
	
	
	/** ************************************************************************
	 * Return a URL that can be embedded in Expressway textual elements that may
	 * contain hyperlinks. The URL will cause Expressway to open this View.
	 */
	 
	String getExpresswayURLString()
	throws
		ParameterError;
	
	
	/** ************************************************************************
	 * Return the name of this type of View. The view type is used to construct
	 * URLs that open Views in Expressway.
	 */
	 
	String getViewType();


	/** ************************************************************************
	 * Construct a ScenarioVisual for this NodeView. Do not do anything other than
	 * construct it.
	 */
	 
	ScenarioVisual makeScenarioVisual(ScenarioSer scenSer);
	
	
	/** ************************************************************************
	 * Create an InfoStrip for the Domain and add it to this View Panel.
	 */
	 
	DomainInfoStrip createDomainInfoStrip();
	
	
	DomainInfoStrip constructDomainInfoStrip();

	
	DomainInfoStrip getDomainInfoStrip();
	
	
	void resizeDomainInfoStrip();
}

