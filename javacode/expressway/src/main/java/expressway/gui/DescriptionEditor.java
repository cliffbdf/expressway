/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.generalpurpose.HTMLWindow;
import java.net.URL;
import java.awt.event.*;
import java.lang.reflect.Method;
import javax.swing.JButton;
import javax.swing.event.*;


/**
 * For viewing and editing the HTML description of a Node.
 */

public class DescriptionEditor extends HTMLWindow
{
	private ViewFactory viewFactory;
	private PanelManager panelManager;
	private ModelEngineRMI modelEngine;
	
	
	public DescriptionEditor(String title, final NodeSer nodeSer,
		final ViewFactory viewFactory, final PanelManager panelManager,
		final ModelEngineRMI me)
	{
		super(title, false, false);
		this.viewFactory = viewFactory;
		this.panelManager = panelManager;
		this.modelEngine = me;
		
		
		addHyperlinkListener(new HyperlinkListener()
		{
			public void hyperlinkUpdate(HyperlinkEvent e)
			{
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
				{
					URL url = e.getURL();
					
					try
					{
						if (url.getProtocol().equals("expressway"))
						{
							// Open a View.
							
							String[] nodeIds = new String[2];
							String[] parts = URLHelper.getRequestStringParts(
								url.getPath(), url.getQuery(), nodeIds);
							
							String viewName = parts[0];
							//String formatName = parts[1];  // not needed
							//String queryString = parts[2];  // not needed
							
							Method method;
							try
							{
								if (nodeIds[1] == null)
									method = viewFactory.getClass().getMethod(
										"create" + viewName + "Panel", String.class);
								else
									method = viewFactory.getClass().getMethod(
										"create" + viewName + "Panel", String.class, String.class);
							}
							catch (NoSuchMethodException ex)
							{
								ErrorDialog.showReportableDialog(DescriptionEditor.this,
									"While trying to activate link '" +
									url.toString() + "'", "Link Error", ex);
								return;
							}
							
							ViewPanelBase viewPanel;
							try  // Create the View.
							{
								viewPanel = (ViewPanelBase)(method.invoke(nodeIds));
							}
							catch (Throwable t)
							{
								ErrorDialog.showReportableDialog(DescriptionEditor.this,
									"While trying to create View '" +
									viewName + "'", "View Creation Error", t);
								return;
							}
							
							panelManager.addPanel(viewPanel.getName(), viewPanel, false);
						}
					}
					catch (Throwable t) { GlobalConsole.printStackTrace(t); }
				}
			}
		});
		
		
		JButton updateButton = createControlButton("Save", new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					modelEngine.setNodeHTMLDescription(nodeSer.getNodeId(), getHTMLText());
				}
				catch (Throwable t) { GlobalConsole.printStackTrace(t); }
			}
		});
	}
}

