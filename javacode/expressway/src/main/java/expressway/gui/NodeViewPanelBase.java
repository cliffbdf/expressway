/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ser.*;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.ClientModel.*;
import expressway.common.VisualComponent;
import expressway.common.ModelAPITypes.*;
import expressway.gui.InfoStrip.*;
import expressway.help.*;
import java.util.List;
import java.util.Vector;
import java.awt.Container;
import java.awt.Color;
import java.awt.Insets;
import java.awt.FlowLayout;
import java.awt.event.*;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Clipboard;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;


public abstract class NodeViewPanelBase extends ViewPanelBase implements NodeView
{
	public static final int DomainInfoStripHeight = 40;


	private NodeSer outermostNodeSer;
	private DomainInfoStrip domainInfoStrip = null;
	private ImageIcon imageIcon;
	
	
	public NodeViewPanelBase(ModelEngineRMI modelEngine, PanelManager panelManager,
		NodeSer nodeSer, ViewFactory viewFactory, String name, boolean maximize)
	throws
		Exception
	{
		super(modelEngine, panelManager, viewFactory, name, maximize);
		setOutermostNodeSer(nodeSer);
		domainInfoStrip = createDomainInfoStrip();
	}
	
	
	public NodeViewPanelBase(ModelEngineRMI modelEngine, PanelManager panelManager,
		NodeSer nodeSer, String name, boolean maximize)
	throws
		Exception
	{
		super(modelEngine, panelManager, name, maximize);
		setOutermostNodeSer(nodeSer);
		domainInfoStrip = createDomainInfoStrip();
	}
	
	
	public void setOutermostNodeSer(NodeSer nodeSer)
	throws
		Exception
	{
		ListenerRegistrar lisReg = getListenerRegistrar();
		if (lisReg != null)
		{
			if (outermostNodeSer != null)  // unsubscribe
			{
				lisReg.subscribe(outermostNodeSer.getNodeId(), false);
			}
			
			lisReg.subscribe(nodeSer.getNodeId(), true);
		}
		
		this.outermostNodeSer = nodeSer;
	}
	
	
	public NodeSer getOutermostNodeSer() { return outermostNodeSer; }
	
	
	public String getOutermostNodeId() { return outermostNodeSer.getNodeId(); }
	
	
	public DomainInfoStrip getDomainInfoStrip() { return domainInfoStrip; }
	
	
	public void initializeNodeSer(NodeSer nodeSer)
	{
		this.outermostNodeSer = nodeSer;
	}
	
	
	public DomainInfoStrip createDomainInfoStrip()
	{
		// Construct an InfoStrip and add it directly to this NodeView.
		DomainInfoStrip infoStrip = constructDomainInfoStrip();
		
		// Add Listener to listen for changes in the size of the InfoStrip's
		// Container, so that the Listener can resize the InfoStrip.
		// (The Container is a JPanel with a null Layout Manager.)
		
		this.addComponentListener(new ComponentListener()
		{
			public void componentHidden(ComponentEvent e) {}
			
			public void componentMoved(ComponentEvent e) {}
			
			public void componentResized(ComponentEvent e)
			{
				resizeDomainInfoStrip();
			}
			
			public void componentShown(ComponentEvent e) {}
		});
		
		addDomainInfoStrip(infoStrip);
		
		return infoStrip;
	}
	
	
	protected void addDomainInfoStrip(InfoStrip infoStrip)
	{
		getInfoPanel().add(infoStrip.getComponent());
		validate();
	}


	public void resizeDomainInfoStrip()
	{
		if (domainInfoStrip == null) return;
		
		Insets insets = this.getInsets();
		
		int leftInset = 0;
		int rightInset = 0;
		int topInset = 0;
		int bottomInset = 0;
		
		if (insets != null)
		{
			leftInset = insets.left;
			rightInset = insets.right;
			topInset = insets.top;
			bottomInset = insets.bottom;
		}
		
		this.domainInfoStrip.setSize(this.getWidth() - leftInset - rightInset,
			DomainInfoStripHeight);
			
		this.domainInfoStrip.setLocation(leftInset, topInset);//....but the container is BorderLayout
		
		this.domainInfoStrip.setVisible(true);

		this.validate();
	}
	
	
	public String[] getAvailableHTTPFormats(String[] descriptions)
	throws
		Exception
	{
		String nodeId = getOutermostNodeSer().getNodeId();
		return getModelEngine().getAvailableHTTPFormats(nodeId, descriptions);
	}
	
	
	public String getWebURLString(String format)
	throws
		Exception
	{
		String nodeId = getOutermostNodeSer().getNodeId();
		return getModelEngine().getWebURLString(nodeId, format);
	}


	public String getExpresswayURLString()
	throws
		ParameterError
	{
		String serverName = ClientGlobalValues.mainApplication.getModelEngineNetworkPath();
		String portStr = Integer.toString(ClientGlobalValues.mainApplication.getHTTPPort());
		
		return URLHelper.createExpresswayURLString(getViewType(), serverName, portStr, 
			getOutermostNodeSer().getNodeId());
	}
	
	
	public abstract String getViewType();
	
	
	public ImageIcon getImageIcon() { return imageIcon; }
	
	
	public void setImageIcon(ImageIcon imgIcon) { this.imageIcon = imgIcon; }
	
	
  /* From View */


	public final List<NodeSer> getOutermostNodeSers()
	{
		List<NodeSer> sers = new Vector<NodeSer>();
		if (outermostNodeSer == null) return sers;
		sers.add(outermostNodeSer);
		return sers;
	}
	
	
	public final List<String> getOutermostNodeIds()
	{
		List<String> ids = new Vector<String>();
		if (outermostNodeSer == null) return ids;
		ids.add(outermostNodeSer.getNodeId());
		return ids;
	}
	
	
  /* From NodeView */
	
	
	public ScenarioVisual showScenario(String scenarioId)
	throws
		Exception
	{
		return null;
	}
	
	 
	public void showScenarioCreated(String scenarioId)
	{
		try { showScenario(scenarioId); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, "Unable to show Scenario", ex);
		}
	}
	
	
	public void showScenarioDeleted(String scenarioId)
	{
		// Unsubscribe.
		if (getListenerRegistrar() != null)
			try { getListenerRegistrar().subscribe(scenarioId, false); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(this, ex);
			}
	}
}

