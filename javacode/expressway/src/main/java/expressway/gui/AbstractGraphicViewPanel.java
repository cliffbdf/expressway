/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import javax.swing.JPanel;
import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.gui.activityrisk.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.Constants;
import expressway.common.VisualComponent.*;
import expressway.ser.ActivitySer;
import expressway.ser.ModelComponentSer;
import expressway.ser.PortedContainerSer;
import expressway.ser.PortSer;
import expressway.awt.AWTTools;
import expressway.generalpurpose.ThrowableUtil;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.Vector;
import java.util.List;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.util.Collection;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;


/** ****************************************************************************
 * A common Panel base class for all GUI Graphic Views that may contain Visuals.
 */
 
public abstract class AbstractGraphicViewPanel extends NodeViewPanelBase implements GraphicView
{
 /* ****************************************************************************
	 ***************************************************************************
	 * Initiatlization permanent state.
	 */
	
	
	
  /* ***************************************************************************
	 * Transient state:
	 * Transformation parameters for this view. These describe the view, relative
	 * to the model that this View depicts. Note that we do not have any plans
	 * to handle rotation or anything other than a linear transformation.
	 */
	
	/** The x-dimension size of a display space pixel in the real-valued space
	 of this View. */
	private double pixelXSize = 1.0;
	
	
	/** The y-dimension size of a display space pixel in the real-valued space
	 of this View. */
	private double pixelYSize = 1.0;
	
	
	private int xOriginStaticOffset = 0;
	private int yOriginStaticOffset = 0;
	
	
	private int xOriginDynamicOffset = 0;
	private int yOriginDynamicOffset = 0;
	
	
	/** The Set of all Visuals owned by this View. */
	//private Set<VisualComponent> visuals = new HashSet<VisualComponent>();
	
	
	/** The Set of currently selected Visuals. */
	private Set<VisualComponent> selectedVisuals = new HashSet<VisualComponent>();


	public AbstractGraphicViewPanel(PanelManager container, ViewFactory viewFactory,
		NodeSer nodeSer, ModelEngineRMI modelEngine, String name)
	throws
		Exception
	{
		super(modelEngine, container, nodeSer, viewFactory, name, false);

		//this.displayArea = new VisualComponentDisplayAreaImpl();
		//this.add(displayArea);
		
		this.setLayout(null);
	}
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Static methods.
	 */
	 
	 
 	/** ************************************************************************
	 * Recursively identfiy child elements that represent the specified node ID.
	 * This component is included if it also represents the node id.
	 * Note: This implementation assumes that all Visual Components are awt Containers.
	 */
	 
	public static void identifyVisualComponents(Container parent, String nodeId, 
		Set<VisualComponent> matchingComps)
	throws
		ParameterError  // if the Node Id is null or invalid.
	{
		if (nodeId == null) throw new ParameterError("Node Id is null");
		
		if (parent instanceof VisualComponent)
		{
			if (parent instanceof VisualComponent)
			{
				String parentNodeId = null;
				try { parentNodeId = ((VisualComponent)parent).getNodeId(); }
				catch (VisualComponent.NoNodeException ex) {}  // leave null
				
				if (parentNodeId != null)  // could be null if not saved yet.
				{
					if (parentNodeId.equals(nodeId))
					{
						matchingComps.add((VisualComponent)parent);
					}
				}
			}
		}
			
		Component[] comps = parent.getComponents();
		
		for (Component comp : comps)
		{
			identifyVisualComponents((Container)comp, nodeId, matchingComps);
		}
	}
	
	
	/** ************************************************************************
	 * Recursively identfiy child elements. This component is included also if
	 * it is a VisualComponent.
	 * Note: It is assumed that all Visual Components are awt Containers.
	 */
	 
	static void identifyVisualComponents(Container parent, 
		Set<VisualComponent> matchingComps)
	{
		if (parent instanceof VisualComponent)
			matchingComps.add((VisualComponent)parent);
		
		Component[] comps = parent.getComponents();
		
		for (Component comp : comps)
		{
			identifyVisualComponents((Container)comp, matchingComps);
		}
	}
	
	
	
  /* ***************************************************************************
	 ***************************************************************************
	 * Defined in Container.
	 */
	 
	public synchronized Component add(Component c)
	{
		super.add(c);
		//if (this.getWidth() == 0 || this.getHeight() == 0) throw new RuntimeException(
		//	"Adding a Component when the size of the View Container is not defined.");
		
		return c;
	}
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Methods defined in View interface.
	 */
	 
	 
	public abstract Class getVisualClass(NodeSer node) throws CannotBeInstantiated;
	
	
	public synchronized Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError
	{
		Set<VisualComponent> visualComponents = new HashSet<VisualComponent>();
		
		identifyVisualComponents(this, nodeId, visualComponents);
		
		return visualComponents;
	}
	
	
	public VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError
	{
		Set<VisualComponent> viss = 
			identifyVisualComponents((Component)(getOutermostVisual()), null, 
				nodeId, new HashSet<VisualComponent>());
		if (viss.size() == 0) return null;
		VisualComponent viewVisual = null;
		for (VisualComponent vis : viss) if (vis instanceof View) viewVisual = vis;
		if (viewVisual != null) viss.remove(viewVisual);
		if (viss.size() > 1) throw new RuntimeException("More than one Visual");
		VisualComponent vis = null;
		for (VisualComponent v : viss) vis = v;
		return vis;
	}

	
	public synchronized Set<VisualComponent> identifyVisualComponents(Class c)
	throws
		ParameterError
	{
		//if (! (c.isAssignableFrom(VisualComponent.class))) throw new ParameterError(
		if (! (VisualComponent.class.isAssignableFrom(c))) throw new ParameterError(
			"The class parameter must be a VisualComponent type");
		
		return identifyVisualComponents(this, c, null, new HashSet<VisualComponent>());
	}
	
	
	protected Set<VisualComponent> identifyVisualComponents(Component comp, Class c, 
		String nodeId, Set<VisualComponent> visuals)
	{
		boolean matchesClass = false;
		if (c == null) matchesClass = true;
		else if (c.isAssignableFrom(comp.getClass())) matchesClass = true;
		
		boolean matchesId = false;
		if (comp instanceof VisualComponent)
		{
			if (nodeId == null) matchesId = true;
			if (nodeId.equals(((VisualComponent)comp).getNodeId())) matchesId = true;
		}
		
		if (matchesClass && matchesId) visuals.add((VisualComponent)comp);
		
		if (! (comp instanceof Container)) return visuals;
		
		Component[] components = ((Container)comp).getComponents();
		for (Component component : components)
		{
			identifyVisualComponents(component, c, nodeId, visuals);  // recursive
		}
		
		return visuals;
	}
	
	
	public synchronized Set<VisualComponent> identifyVisualComponents()
	throws
		ParameterError
	{
		Set<VisualComponent> visualComponents = new HashSet<VisualComponent>();
		identifyVisualComponents(this, visualComponents);
		return visualComponents;
	}
	
	
	public abstract VisualComponent makeVisual(NodeSer node, Container container,
		boolean asIcon)
	throws
		ParameterError,
		Exception;
	
	
	public synchronized Set<VisualComponent> refreshVisuals(String nodeId)
	throws
		Exception
	{
		// Retrieve Node data from the server.
		NodeSer node = getModelEngine().getNode(nodeId);
		
		// Update each Visual that depicts the Node.
		
		return refreshVisuals(node);
	}
	
	
	public synchronized void select(VisualComponent visual, boolean yes)
	{
		if (yes) selectedVisuals.add(visual);
		else selectedVisuals.remove(visual);
		
		visual.highlight(yes);
	}
	
	
	public synchronized void selectNodesWithId(String nodeId, boolean yes)
	throws
		ParameterError
	{
		Set<VisualComponent> visualComponents = new HashSet<VisualComponent>();
		
		identifyVisualComponents(this, nodeId, visualComponents);
		
		for (VisualComponent visual : visualComponents)
		{
			select(visual, yes);
		}
	}


	public synchronized Set<VisualComponent> getSelected()
	{
		return new HashSet(selectedVisuals);
	}
	
	
	public synchronized boolean isSelected(VisualComponent visual)
	{
		return selectedVisuals.contains(visual);
	}


	public synchronized void selectAll(boolean yes)
	{
		if (yes)
		{
			Component[] comps = this.getComponents();
			for (Component comp : comps)
			{
				if (comp instanceof VisualComponent) select((VisualComponent)comp, true);
			}
		}
		else
		{
			for (VisualComponent visual : selectedVisuals)
			{
				visual.highlight(false);
			}
			
			this.selectedVisuals.clear();
		}
	}
	

	
  /* ***************************************************************************
	 * *************************************************************************
	 * Methods defined in GraphicView interface.
	 */
	
		
	public synchronized void computeDisplayAreaLocationAndSize(boolean repositionComponents)
	{
		super.computeDisplayAreaLocationAndSize();
		if (repositionComponents) this.positionOutermostVisual();
	}
	

	public synchronized void positionOutermostVisual()
	{
		VisualComponentDisplayArea da = getDisplayArea();
		VisualComponent ov = getOutermostVisual();
		if (ov != null)
		{
			java.awt.Insets insets = ((Container)ov).getInsets();
			int insetLeft = (insets == null? 0 : insets.left);
			int insetBottom = (insets == null? 0 : insets.bottom);
			this.setXOriginStaticOffset(0);
			this.setYOriginStaticOffset(da.getHeight());
			this.setXOriginDynamicOffset(insetLeft);
			this.setYOriginDynamicOffset(-insetBottom);
		}
	}
	 
	 
	public synchronized int transformNodeXCoordToView(double modelXCoord, int xOriginOffset)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeXCoordToView(modelXCoord,
			pixelXSize, xOriginOffset);
	}
	
	
	public synchronized int transformNodeYCoordToView(double modelYCoord, int yOriginOffset)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeYCoordToView(modelYCoord,
			pixelYSize, yOriginOffset);
	}
	
	
	public synchronized int transformNodeDXToView(double nodeWidth)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeDXToView(nodeWidth,
			pixelXSize);
	}
		
		
	public synchronized int transformNodeDYToView(double nodeHeight)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeDYToView(nodeHeight,
			pixelYSize);
	}


	public synchronized double transformViewXCoordToNode(int viewXCoord, int xOriginOffset)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewXCoordToNode(viewXCoord,
			pixelXSize, xOriginOffset);
	}
	
	
	public synchronized double transformViewYCoordToNode(int viewYCoord, int yOriginOffset)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewYCoordToNode(viewYCoord,
			pixelYSize, yOriginOffset);
	}
		
		
	public synchronized double transformViewDXToNode(int viewWidth)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewDXToNode(viewWidth,
			pixelXSize);
	}
		
		
	public synchronized double transformViewDYToNode(int viewHeight)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewDYToNode(viewHeight,
			pixelYSize);
	}
	

	/** Should be called whenever a Visual moves relative to the DisplayArea.
		This provides notification of display Components whose position or appearance
		are dependent on the visual position of the Visual. */
	abstract public void notifyVisualMoved(VisualComponent visual);
	
	
	abstract public void notifyVisualResized(VisualComponent visual);
	
	
	abstract public void notifyVisualRemoved(VisualComponent visual);
	

	public synchronized void notifyThatViewAreaChanged(int x, int y, int width, int height) {}

	
	public synchronized double getPixelXSize() { return pixelXSize; }
	
	
	public synchronized double getPixelYSize() { return pixelYSize; }
	
	
	public synchronized int getXOriginOffset() { return xOriginStaticOffset + xOriginDynamicOffset; }
	
	
	public synchronized int getYOriginOffset()
	{
		return yOriginStaticOffset + yOriginDynamicOffset;
	}


	public synchronized int getXOriginStaticOffset() { return xOriginStaticOffset; }


	public synchronized int getYOriginStaticOffset() { return yOriginStaticOffset; }
	
	
	public synchronized int getXOriginDynamicOffset() { return xOriginDynamicOffset; }


	public synchronized int getYOriginDynamicOffset() { return yOriginDynamicOffset; }
	
	
	public synchronized void setPixelXSize(double s) { this.pixelXSize = s; }
	
	
	public synchronized void setPixelYSize(double s) { this.pixelYSize = s; }
	
	
	protected synchronized void setXOriginStaticOffset(int xo)
	{
		xOriginStaticOffset = xo;
	}


	protected synchronized void setYOriginStaticOffset(int yo)
	{
		yOriginStaticOffset = yo;
	}
	
	
	protected synchronized void setXOriginDynamicOffset(int xo) { xOriginDynamicOffset = xo; }


	protected synchronized void setYOriginDynamicOffset(int yo)
	{
		yOriginDynamicOffset = yo;
	}


	
  /* ***************************************************************************
	 * *************************************************************************
	 * Protected methods.
	 */
	 
	
	/** ************************************************************************
	 * Refresh all of the Visuals in this View that depict the specified Node,
	 * based on the data contained in the argument. Return a Set of the Visuals
	 * that were actually refreshed.
	 */
	 
	protected Set<VisualComponent> refreshVisuals(NodeSer node)
	throws
		Exception
	{
		// Update each Visual that depicts the Node.
		
		Set<VisualComponent> visualsCopy = new HashSet<VisualComponent>();
		identifyVisualComponents(this, visualsCopy);
		
		Set<VisualComponent> nodeVisuals = new HashSet<VisualComponent>();
		
		for (VisualComponent visual : visualsCopy)
		{
			if (! visual.getNodeId().equals(node.getNodeId())) continue;
			
			removeVisual(visual);
			
			Set<VisualComponent> newVisuals = addNode(visual.getNodeSer().getNodeId());
			if (newVisuals != null) nodeVisuals.addAll(newVisuals);
		}
		
		for (VisualComponent visual : nodeVisuals)
			if (visual instanceof Component) ((Component)visual).repaint();
			
			
		return nodeVisuals;
	}
	
	
	/*public synchronized void computeDisplayAreaLocationAndSize(boolean repositionComponents)
	{
		super.computeDisplayAreaLocationAndSize(repositionComponents);
		
		// Resposition Visuals: maintain distance from baseline.
		
		VisualComponentDisplayAreaImpl displayArea = 
			(VisualComponentDisplayAreaImpl)(getDisplayArea());
		
		if (repositionComponents)
		{
			Component[] components = displayArea.getComponents();
			for (Component comp : components)
			{
				if (comp instanceof VisualComponent) try
				{
					VisualComponent visual = (VisualComponent)comp;
					
					NodeSer ser = visual.getNodeSer();
					//Container visualParent = visual.getParent();
					
					int parentBottomInset = 0;
					int parentLeftInset = 0;
					int visBottomInset = 0;
					
					if (displayArea instanceof JComponent)
					{
						JComponent jda = (JComponent)displayArea;
						parentBottomInset =
							(jda.getInsets() == null? 0 : jda.getInsets().bottom);
					
						parentLeftInset =
							(jda.getInsets() == null? 0 : jda.getInsets().left);
					}
					
					if (comp instanceof JComponent)
					{
						JComponent jcomp = (JComponent)comp;
						visBottomInset =
							(jcomp.getInsets() == null? 0 : jcomp.getInsets().bottom);
					}
					
					int newVisualX = this.transformNodeDXToView(ser.getX()) + parentLeftInset;
					
					int nodeYPix = this.transformNodeDYToView(ser.getY());
					int nodeHPix = this.transformNodeDYToView(ser.getHeight());
					int newVisualY = 
						displayArea.getHeight() - parentBottomInset
						- nodeYPix
						- nodeHPix
						- visBottomInset;
					
					comp.setLocation(newVisualX, newVisualY);
					
					this.notifyVisualMoved(visual);
				}
				catch (ParameterError ex) { ex.printStackTrace(); }
			}
		}
	}*/
}

