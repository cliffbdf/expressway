/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.util.Set;
import java.util.HashSet;
import java.io.Serializable;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;


/**
 * AttributeVisual base implementation for extension by the various specialized
 * TreeTablePanel Views.
 */
 
public abstract class AttributeVisualBase extends JPanel implements AttributeVisual
{
	public static final Color BackgroundColor = new Color(100, 100, 100);
	public static final Color BackgroundHLColor = new Color(150, 100, 100);


	public abstract void setLocation();
	
	
	public void setSize()
	{
		super.setSize(DefaultAttributeVisualWidth, DefaultAttributeVisualHeight);
	}


	protected static AttributeValueParser DefaultValueParser = new AttributeValueParser()
	{
		public Serializable parseValue(String text)
		throws
			ParameterError
		{
			return ObjectValueWriterReader.parseObjectFromString(text);
		}
	};
	
	
	public static final int DefaultAttributeVisualWidth = 300;
	public static final int DefaultAttributeVisualHeight = 25;
	
	public static final Color normalBorderColor = new Color(0, 0, 128);
	public static final Color highlightedBorderColor = new Color(255, 100, 0);


	/** Contains the data that identifies and describes the Visual as a Node
	 on the server. */
	private AttributeSer attrSer;
	
	/** The View that contains this Visual. */
	private ViewPanelBase viewPanel;

	/** If true, this Visual has been populated with its child Visuals, by
	 requesting the corresponding child NodeSers from the server. */
	private boolean populated = true;
	
	/** If true, this Visual should be displayed as an icon, with no internal structure. */
	private boolean asIcon = true;
	
	/** If true, the Visual has been selected for highlighting. */
	boolean highlighted = false;
	
	/** Inner border (inside the title region) for when this Visual is displayed
	 full size (not an icon) and not highlighted. */
	private Border outerNormalBorder = null;
	
	/** Inner border (inside the title region) for when this Visual is displayed
	 full size (not an icon) and highlighted. */
	private Border outerNormalBorderHighlighted = null;
	
	/** Inner border (inside the title region) for when this Visual is displayed
	 as an icon. */
	private Border outerIconBorder = null;

	/** Inner border (inside the title region) for when this Visual is displayed
	 as an icon and highlighted. */
	private Border outerIconBorderHighlighted = null;

	/** Border used when this Visual is full-size (i.e., not an icon). */
	private TitledBorder normalBorder;
	
	/** Border used when this Visual is displayed as an icon. */
	private TitledBorder iconBorder;
	
	/** Image used to depict the icon. */
	private ImageIcon imageIcon;
	
	private boolean inconsistent;
	private boolean unpropagated;
	
	private AttributeValueParser valueParser = DefaultValueParser;
	
	
	public String getDescriptionPageName() { return "Attributes"; }
	
	
	public AttributeVisualBase(AttributeSer attrSer, ViewPanelBase viewPanel)
	{
		super();
		this.attrSer = attrSer;
		this.viewPanel = viewPanel;
		this.setName(attrSer.getFullName());

		this.setLayout(null);

		setBackground(getNormalBackgroundColor());
		try { setImageIcon(); }
		catch (IOException ex) { throw new RuntimeException(ex); }
		
		int borderThickness = getNormalBorderThickness();
		
		outerNormalBorder = BorderFactory.createMatteBorder(
			borderThickness, borderThickness, borderThickness, borderThickness,
			normalBorderColor);
			
		outerNormalBorderHighlighted = BorderFactory.createMatteBorder(
			borderThickness, borderThickness, borderThickness, borderThickness,
			highlightedBorderColor);
			
		normalBorder = BorderFactory.createTitledBorder(outerNormalBorder, attrSer.getName());
		normalBorder.setTitleJustification(TitledBorder.CENTER);
		normalBorder.setTitleFont(getClientView().getDisplayConstants().getComponentTitleFont());
		
		outerIconBorder = BorderFactory.createLineBorder(
			normalBorderColor, getNormalBorderThickness());
			
		outerIconBorderHighlighted = BorderFactory.createLineBorder(
			highlightedBorderColor, getNormalBorderThickness());
			
		iconBorder = BorderFactory.createTitledBorder(outerIconBorder, attrSer.getName());
		
		setBorder(this.normalBorder);
	}
	
	
  /* From AttributeVisual */
	
	
	public AttributeValueParser getValueParser() { return valueParser; }
	
	public Serializable parseValue(String text)
	throws
		ParameterError
	{
		return valueParser.parseValue(text);
	}
	
	
  /* From VisualComponent */


	public String getNodeId()
	throws NoNodeException { return attrSer.getNodeId(); }
	
	
	public String getName() { return attrSer.getName(); }
	
	
	public String getFullName() { return attrSer.getFullName(); }
	

	public abstract int getSeqNo();


	public boolean contains(VisualComponent comp)
	throws NoNodeException { return false; }
	
	
	public boolean isOwnedByMotifDef()
	{
		return getNodeSer().ownedByMotifDef();
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		return getNodeSer().ownedByActualTemplateInstance();
	}
	

	public String[] getChildIds()
	{
		return getNodeSer().getChildNodeIds();
	}
	
	
	public void nameChangedByServer(String newName, String newFullName)
	{
		this.attrSer.setName(newName);
		this.attrSer.setFullName(newFullName);
		this.setName(newName);
		Border border = this.getBorder();
		if (border != null)
			if (border instanceof TitledBorder)
				((TitledBorder)border).setTitle(newName);
		this.repaint();
	}
	
	
	public void nodeDeleted() {}
	
	
	public void notifyCrossReferenceCreated(String toNodeId, String crossRefId)
	{
		NodeSer ser;
		try { ser = getModelEngine().getNode(crossRefId); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
			return;
		}
		
		if (! (ser instanceof CrossReferenceSer)) throw new RuntimeException(
			"Node with Id " + crossRefId + " is not a CrossReference");
		CrossReferenceSer crSer = (CrossReferenceSer)ser;
		getClientView().notifyCrossReferenceCreated(crSer);
	}
		
		
	public void notifyCrossReferenceDeleted(String toNodeId, String crossRefId)
	{
		getClientView().notifyCrossReferenceDeleted(this.getNodeId(), toNodeId, crossRefId);
	}
		
		
	public View getClientView() { return viewPanel; }
	
	
	public ModelEngineRMI getModelEngine() { return viewPanel.getModelEngine(); }
	
	
	public NodeSer getNodeSer() { return attrSer; }
	
	
	public void setNodeSer(NodeSer attrSer)
	throws NoNodeException { this.attrSer = (AttributeSer)attrSer; }
	
	
	public void update()
	{
		NodeSer attrSer = null;
		try { attrSer = getClientView().getModelEngine().getNode(this.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, 
				"Error while updating Visual " + this.getName(), ex);
		}
		
		this.update(attrSer);
		
		depopulateChildren();
		
		if (! getAsIcon())
		{
			try { populateChildren(); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
	}
	

	public void update(NodeSer newNodeSer)
	{
		this.attrSer = (AttributeSer)newNodeSer;

		this.setUnpropagated(false);
		this.setInconsistent(false);
		
		try { this.refreshRedundantState(); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, 
				"Error while updating Visual " + this.getName(), ex);
					
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	public void redraw() { repaint(); }
	

	public void refresh() { update(); redraw(); }
	
	
	public void refreshLocation()
	{
		NodeSer nSer = null;
		try { nSer = getClientView().getModelEngine().getNode(this.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, 
				"Error while updating Visual " + this.getName(), ex);
		}
		
		
		//this.attrSer.setX(nSer.getX());
		//this.attrSer.setY(nSer.getY());
		
		this.attrSer = (AttributeSer)nSer;
		
		try { this.setLocation(); }
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		this.getClientView().repaint();
	}

	
	public void refreshSize()
	{
		NodeSer nSer = null;
		try { nSer = getClientView().getModelEngine().getNode(this.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, 
				"Error while updating Visual " + this.getName(), ex);
		}
		
		this.attrSer.setWidth(nSer.getWidth());
		this.attrSer.setHeight(nSer.getHeight());
		this.attrSer.setIconWidth(nSer.getIconWidth());
		this.attrSer.setIconHeight(nSer.getIconHeight());
		this.attrSer.setX(nSer.getX());
		this.attrSer.setY(nSer.getY());
		
		try
		{
			this.setSize();
			this.setLocation();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		this.repaint();
	}

	
	public void refreshRedundantState()
	throws
		Exception
	{
	}

	
	protected boolean isPopulated() { return populated; }
	
	
	protected void setPopulated(boolean pop) { this.populated = pop; }
	
	
	protected boolean isInconsistent() { return inconsistent; }
	
	
	protected void setInconsistent(boolean newValue) { this.inconsistent = newValue; }
	
	
	protected boolean isUnpropagated() { return unpropagated; }
	
	
	protected void setUnpropagated(boolean newValue) { this.unpropagated = newValue; }
	
	
	public boolean getAsIcon() { return this.asIcon; }
	
	
	public void setAsIcon(boolean yes) { this.asIcon = yes; }
	
	
	protected void setImageIcon()
	throws
		IOException
	{
		final String handle = getNodeSer().getIconImageHandle();
		final ImageIcon icon = getClientView().getStoredImage(handle);
		setImageIcon(icon);
	}
	
	
	public ImageIcon getImageIcon() { return imageIcon; }
	
	
	public void setImageIcon(ImageIcon imgIcon) { this.imageIcon = imgIcon; }
	
	
	public boolean useBorderWhenIconified() { return attrSer.useBorderWhenIconified(); }
	
	
	public void populateChildren()
	throws
		ParameterError,
		Exception
	{
		setPopulated(true);
		
		String[] ids = getChildIds();
		for (String id : ids) getClientView().addNode(id);
		
		validate();
		repaint();
	}
	
	
	/*public void refreshChildren()
	throws
		ParameterError,
		Exception
	{
	}*/
	
	
	void depopulateChildren()
	{
		setPopulated(false);
		
		Component[] childComponents = this.getComponents();
		for (Component child : childComponents)
		{
			if (! (child instanceof VisualComponent)) continue;
			VisualComponent childVisual = (VisualComponent)child;
			try { this.getClientView().removeVisual(childVisual); }
			catch (Exception ex)
			{
				this.setInconsistent(true);
				ErrorDialog.showReportableDialog(this,
					"Error while removing Visual " + childVisual.getName(),
					ex);
			}
		}
		
		repaint();
	}
	
	
	public Set<VisualComponent> getChildVisuals()
	{
		String[] childIds = getNodeSer().getChildNodeIds();
		Set<VisualComponent> childVisuals = new HashSet<VisualComponent>();
		for (String childId : childIds) try
		{
			Set<VisualComponent> childViss = getClientView().identifyVisualComponents(childId);
			if (childViss != null) childVisuals.addAll(childViss);
		}
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		return childVisuals;
	}
	
	
	public Object getParentObject() { return this.getParent(); }
	
	
	public Container getContainer() { return this; }
	
	
	public Component getComponent() { return this; }
	
	
	public void highlight(boolean yes)
	{
		if (yes)
		{
			if (normalBorder != null) normalBorder.setBorder(outerNormalBorderHighlighted);
			if (iconBorder != null) iconBorder.setBorder(outerIconBorderHighlighted);
		}
		else
		{
			if (normalBorder != null) normalBorder.setBorder(outerNormalBorder);
			if (iconBorder != null) iconBorder.setBorder(outerIconBorder);
		}
			
		highlightBackground(yes);
		highlighted = yes;
	}
	
	
	public boolean isHighlighted() { return highlighted; }
	
	
	protected void setHighlighted(boolean yes) { this.highlighted = yes; }
	
	
	public Color getNormalBackgroundColor() { return BackgroundColor; }
	
	
	public Color getHighlightedBackgroundColor() { return BackgroundHLColor; }
	
	
	void highlightBackground(boolean yes)
	{
		if (yes) setBackground(getHighlightedBackgroundColor());
		else setBackground(getNormalBackgroundColor());
	}
	
	
	int getNormalBorderThickness()
	{
		if (getClientView() == null) return 0;
		if (getAsIcon())
			if (useBorderWhenIconified())
				return getClientView().getDisplayConstants().getNormalBorderThickness();
			else
				return 0;
		else
			return getClientView().getDisplayConstants().getNormalBorderThickness();
	}
}

