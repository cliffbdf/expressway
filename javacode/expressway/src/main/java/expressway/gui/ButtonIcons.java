/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.awt.AWTTools;
import javax.swing.Icon;
import javax.swing.ImageIcon;


public class ButtonIcons
{
	public static final String ConnectButtonIconImagePath = "/images/Button-Connect.png";
	public static final String NewModelDomainButtonIconImagePath = "/images/Button-NewModelDomain.png";
	public static final String NewMotifButtonIconImagePath = "/images/Button-NewMotif.png";
	public static final String NewModelScenarioButtonIconImagePath = "/images/Button-NewModelScenario.png";
	public static final String NewScenarioButtonIconImagePath = "/images/Button-NewScenario.png";
	public static final String NewViewButtonIconImagePath = "/images/Button-NewView.png";
	public static final String CloseButtonIconImagePath = "/images/Button-Close.png";
	public static final String StartButtonIconImagePath = "/images/Button-Start.png";
	public static final String ImportXMLButtonIconImagePath = "/images/Button-ImportXML.png";
	public static final String HistoryButtonIconImagePath = "/images/Button-History.png";
	public static final String NoHistoryButtonIconImagePath = "/images/Button-NoHistory.png";
	public static final String SaveButtonIconImagePath = "/images/Button-Save.png";
	public static final String SyncButtonIconImagePath = "/images/Button-Sync.png";
	public static final String TimeButtonIconImagePath = "/images/Button-Time.png";
	public static final String ShowValueControlsButtonIconImagePath = "/images/Button-ShowValueControls.png";
	public static final String URLButtonIconImagePath = "/images/Button-URL.png";
	public static final String ShowHideNamedReferenceIconImagePath = "/images/Button-ShowHideNamedRefs.png";

	public static Icon ConnectButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, ConnectButtonIconImagePath));

	public static Icon NewModelDomainButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, NewModelDomainButtonIconImagePath));
	
	public static Icon NewMotifButtonIcon =
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, NewMotifButtonIconImagePath));

	public static Icon NewModelScenarioButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, NewModelScenarioButtonIconImagePath));
		
	public static Icon NewScenarioButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, NewScenarioButtonIconImagePath));
		
	public static Icon NewViewButtonIcon =
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, NewViewButtonIconImagePath));

	public static Icon CloseButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, CloseButtonIconImagePath));

	public static Icon StartButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, StartButtonIconImagePath));

	public static Icon ImportXMLButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, ImportXMLButtonIconImagePath));

	public static Icon HistoryButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, HistoryButtonIconImagePath));

	public static Icon NoHistoryButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, NoHistoryButtonIconImagePath));

	public static Icon SaveButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, SaveButtonIconImagePath));

	public static Icon SyncButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, SyncButtonIconImagePath));

	public static Icon TimeButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, TimeButtonIconImagePath));
	
	public static Icon ShowValueControlsButtonIcon = 
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, ShowValueControlsButtonIconImagePath));
	
	public static Icon URLButtonIcon =
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, URLButtonIconImagePath));
	
	public static Icon ShowHideNamedReferenceButtonIcon =
		new ImageIcon(AWTTools.getImage(ButtonIcons.class, ShowHideNamedReferenceIconImagePath));
}

