/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.gui.InfoStrip.*;
import java.awt.Container;
import java.util.Set;


/**
 * A View that can depicts Scenarios for its Domain.
 */
 
public interface NodeScenarioView extends NodeView
{
	void setShowScenarios(boolean show);
	
	boolean getShowScenarios();
	
	void addScenarioVisual(NodeScenarioPanel panel);
	
	void removeScenarioVisual(NodeScenarioPanel panel);
	
	Set<NodeScenarioPanel> getScenarioPanels(); 
	
	void setCurrentScenarioPanel(String scenarioId);
	
	void setCurrentScenarioPanel(ScenarioSer scenarioSer);
	
	NodeScenarioPanel getCurrentScenarioPanel();
	
	NodeScenarioPanel getScenarioPanel(String scenarioId);
	

	/** ************************************************************************
	 * Verify that the Scenario name is legitimate, including from a security
	 * perspective. Intended for screening user input Scenario names.
	 */
	 
	void validateScenarioName(String name)
	throws
		ParameterError;
	
	
	/** ************************************************************************
	 * Construct and add a ScenarioInfoStrip to this View.
	 */
	 
	ScenarioInfoStrip createScenarioInfoStrip(NodeScenarioPanel panel);
	
	
	ScenarioInfoStrip constructScenarioInfoStrip(NodeScenarioPanel panel);
	
	
	/** ************************************************************************
	 * Should be called immediately after the dimensions of this Panel are
	 * set or are changed, so that the dimensions of the Strip can be adjusted.
	 */
	 
	void resizeScenarioSelectorImpl();


	/**
	 * Delete any Controls associated with the specified Node, in all Scenarios.
	 * The intention is that this is performed when a Visual is removed from a
	 * View.
	 */
	 
	void deleteControls(String nodeId);
}

