/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.gui.View.*;
import expressway.gui.GraphicView;
import expressway.common.VisualComponent.*;
import expressway.help.*;


public interface MenuItemContext
{
	View getClientView();
	VisualComponentDisplayArea getDisplayArea();
	ScenarioVisual getCurrentScenarioVisual();
	VisualComponent getVisualComponent();
	HelpfulMenuItem getMenuItem();
	ModelEngineRMI getModelEngine();
}

