/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.value;


public interface ValueElementVisual extends VisualComponent
{
	interface FundingSourceVisual extends 
	{
	}
	
	
	interface AccountVisual
	{
	}
	
	
	interface OpportunityVisual
	{
	}
	
	
	interface MarketVisual
	{
	}
	
	
	interface FulfillmentVisual
	{
	}
	
	
	interface SummationVisual
	{
	}
	
	
	interface MetricVisual
	{
	}
	
	
	/**
	 * Depict a Ramp, and provide the ability to fit the curves
	 * to data or estimates.
	 */
	 
	interface RampVisual
	{
	}
}


