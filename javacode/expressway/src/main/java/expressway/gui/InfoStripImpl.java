/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.help.*;
import expressway.gui.ClientGlobalValues;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.datatransfer.*;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


/** ************************************************************************
 * Base class for an information strip about a Domain or a Scenario.
 */
 
public abstract class InfoStripImpl extends JPanel
	implements InfoStrip
{
	public static final Color InfoStripColor = new Color(0, 0, 100);
	public static final Color InfoStripTextColor = Color.white;
	public static final int NameFieldLength = 20;
	
	/** Dispalys and allows editing of a Scenario name. */
	private JTextField nameField;
	
	/** Depicts date/time of the most recent modification to the current Scenario. */
	private JLabel modifiedLabel;
	
	/** Enables user to obtain a URL for publishing or linking to the current view. */
	private JButton urlButton;
	
	private NodeView view;
	
	//private ControlPanel controlPanel;
	
	
	public InfoStripImpl(NodeView nodeView//, ControlPanel controlPanel
		)
	{
		this.view = nodeView;
		//this.controlPanel = controlPanel;
		this.setBackground(InfoStripColor);
		this.setLayout(new FlowLayout());
		
		// Add Labels and Buttons.
		
		JButton bugButton;
		
		if (ClientGlobalValues.IncludeBugReportFeatures)
		{
			this.add(bugButton = new JButton("Report Bug"));
			bugButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					view.reportBug();
				}
			});
		}
	
		this.nameField = new JTextField();
		this.nameField.setColumns(NameFieldLength);
		this.add(this.nameField);
		
		this.urlButton = new ViewURLButton(nodeView);
		this.add(this.urlButton);
		
		/*this.modifiedLabel = new JLabel("Version ");
		this.modifiedLabel.setForeground(InfoStripTextColor);
		this.add(this.modifiedLabel);*/
	}
	
	
	public Component getComponent() { return this; }
	
	
	public Container getContainer() { return this; }
	
	
	public JComponent getJComponent() { return this; }
	
	
	public NodeView getNodeView() { return view; }
	
	
	protected ModelEngineRMI getModelEngine() { return view.getModelEngine(); }
	
	
	public JTextField setNameField(JTextField field) { return this.nameField = field; }
	public JTextField getNameField() { return this.nameField; }
	
	
	public JLabel setModifiedLabel(JLabel label) { return this.modifiedLabel = label; }
	public JLabel getModifiedLabel() { return this.modifiedLabel; }
	
	
	public void update()
	{
	}
}

