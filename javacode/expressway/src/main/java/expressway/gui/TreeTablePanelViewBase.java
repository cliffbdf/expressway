/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.gui.TabularView.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;
import expressway.help.*;
import expressway.swing.MultiHierarchyPanel.*;
import expressway.swing.*;
import expressway.generalpurpose.ThrowableUtil;
import expressway.generalpurpose.StringUtils;
import expressway.awt.AWTTools;
import java.lang.reflect.Constructor;
import java.util.Vector;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Enumeration;
import java.util.EventObject;
import java.awt.Container;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Window;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.io.Serializable;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.DefaultCellEditor;
import javax.swing.tree.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.tree.DefaultXTreeCellRenderer;
import org.jdesktop.swingx.JXTree.DelegatingRenderer;
import org.jdesktop.swingx.treetable.*;


/**
 * Extend this to define each of the hierarchy Panels. This View is designed to
 * be able to be placed in a DualHierarchyPanel.
 */
 
public abstract class TreeTablePanelViewBase extends NodeViewPanelBase
	implements
		TreeTableComponent,
		TabularView,
		PanelManager.Single_NodeView_Container
{
	public static final Color ScenarioColor = new Color(240, 240, 240);
	public static final Color ScenarioHLColor = new Color(240, 200, 200);


	/**
	 * For handling modal selections via the selectNode method.
	 */
	 
	public interface TabularSelectionHandler
	{
		void tableVisualSelected(TabularVisualJ visual);
	}
	
	
  /* Methods and classes to be extended */

	
	/**
	 * List the names of the Node fields that should be retrieved and displayed
	 * as columns. Does not include the 'Name' column, which is column 0.
	 */
	 
	public List<String> getFieldNames()
	{
		String[] fnames = ((TabularSer)(getOutermostVisual().getNodeSer())).getFieldNames();
		ArrayList<String> alist = new ArrayList<String>(fnames.length);
		for (String fname : fnames) alist.add(fname);
		return alist;
	}
	
	
	/**
	 * Define a filter for determining if a particular type of Node is allowed in
	 * this View.
	 */
	 
	protected boolean nodeIsPermitted(NodeSer nodeSer) throws Exception
	{
		return (nodeSer instanceof HierarchySer);
	}


	/**
	 * Add context Menu Items that apply to this entire View, rather than being
	 * specific to a particular Visual type. The MenuItem actions update the server,
	 * which then notifies selected Visuals in this View, causing those Visuals
	 * to refresh themselves.
	 */
	
	protected void addViewContextMenuItems(JPopupMenu containerPopup,
		final TabularVisualJ visual, int x, int y)
	{
		if // this View is the right-most (last) View in its MultiHierarchyPanel
			((getMultiHierarchyPanel() != null) && 
				getMultiHierarchyPanel().isLastTreeTableComponent(this))
		{
			// Add menu items for adding and removing Views.
			
			JMenuItem mi = new HelpfulMenuItem("Add View...", false,
				"Add a side-by-side View, linked to this View by a relationship " +
				"that you specify.");
			
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					// Display a popup that contains a Tree of the Domains that can
					// be selected, and when the user selects a Domain, also display
					// a list of the Named References that can be selected for the
					// CrossMapPanel.
					
					try { (new RelatedDomainSelector(TreeTablePanelViewBase.this)).show(); }
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(TreeTablePanelViewBase.this, ex);
					}
				}
			});

			containerPopup.add(mi);
			
			mi = new HelpfulMenuItem("Remove View", false,
				"Remove this side-by-side View from the display.");
			
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try { getMultiHierarchyPanel().removeTreeTableComponent(
						TreeTablePanelViewBase.this); }
					catch (Exception ex)  // should not happen
					{
						throw new RuntimeException("Should not happen", ex);
					}
				}
			});

			containerPopup.add(mi);
		}
		
		if // this View is in a MultiHierarchyPanel and there is another View
			// to the left
			(getPriorTreeTableComponent() != null)
		{
			final TreeTableComponent leftTreeTableComponent = getPriorTreeTableComponent();
			final View rightView = TreeTablePanelViewBase.this;
			int pos = getMultiHierarchyPanel().getTreeTableComponentNo(leftTreeTableComponent);
			CenterPanel centerPanel = (CenterPanel)(getMultiHierarchyPanel().getCrossMapPanel(pos));
			String[] namedRefNames = centerPanel.getNamedRefNames();
			for (final String namedRefName : namedRefNames)
			{
				if (namedRefName == null) continue;
				// To do: Check for compatibility.
				addNamedRefLinkMenuItem(
					containerPopup, visual, x, y, leftTreeTableComponent, namedRefName);
			}
		}
		
		if // this View is in a MultiHierarchyPanel and there is another View
			// to the right
			(getNextTreeTableComponent() != null)
		{
			final TreeTableComponent rightTreeTableComponent = getNextTreeTableComponent();
			final View leftView = TreeTablePanelViewBase.this;
			
			int pos = getMultiHierarchyPanel().getTreeTableComponentNo((TreeTableComponent)leftView);
			CenterPanel centerPanel = (CenterPanel)(getMultiHierarchyPanel().getCrossMapPanel(pos));
			String[] namedRefNames = centerPanel.getNamedRefNames();
			for (final String namedRefName : namedRefNames)
			{
				if (namedRefName == null) continue;
				// To do: Check for compatibility.
				addNamedRefLinkMenuItem(
					containerPopup, visual, x, y, rightTreeTableComponent, namedRefName);
			}
		}
	}
	
	
	protected void addNamedRefLinkMenuItem(final JPopupMenu containerPopup,
		final TabularVisualJ fromVisual, final int x, final int y,
		final TreeTableComponent toTreeTableComponent, final String namedRefName)
	{
		if (namedRefName == null) throw new RuntimeException("NamedReference name is null");
		final NodeView fromView = TreeTablePanelViewBase.this;
		final JMenuItem mi = new HelpfulMenuItem("Create " + namedRefName + " link...",
			false, "Create a Cross Reference to a Node in the " +
			((View)toTreeTableComponent).getName() + " view...");
		
		mi.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// Disable the 'from' View until after the next user action.
				fromView.addTemporaryMouseListener(new MouseAdapter()
				{
					public void mousePressed(MouseEvent e) { } // ok
				});
				
				// Display floating message that instructs the user to
				// select the 'to' Node.
				Object eventSource = e.getSource();
				int x = 0, y = 0;
				if (eventSource instanceof Component)
				{
					x = ((Component)eventSource).getX();
					y = ((Component)eventSource).getY();
				}
				
				final FloatingMessage floater = new FloatingMessage(
					"Select a 'to' Node in the " +
					((View)toTreeTableComponent).getName() + " view",
					x, y, 400, 100);
				
				// Add handler to the View to the left to respond to a Node
				// selection.
				final String fromNodeId = fromVisual.getNodeId();
				toTreeTableComponent.addTemporaryRowSelectionListener(
					new RowSelectionListener()
				{
					public void visualSelected(TreeTableNode toTableNode)
					{
						TabularVisualJ toVisual = (TabularVisualJ)toTableNode;
						//String domainId = fromView.getOutermostNodeSer().getDomainId();
						String toNodeId = toVisual.getNodeId();
						try
						{
							try { getModelEngine().createCrossReference(false, //domainId,
								namedRefName, fromNodeId, toNodeId); }
							catch (Warning w)
							{
								if (JOptionPane.showConfirmDialog(null, w.getMessage(),
									"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
								{
									getModelEngine().createCrossReference(true, //domainId,
										namedRefName, fromNodeId, toNodeId);
								}
							}
						}
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(TreeTablePanelViewBase.this, ex);
						}
						finally
						{
							toTreeTableComponent.removeTemporaryRowSelectionListener();
							fromView.removeTemporaryMouseListener();
							floater.dispose();
						}
					}
				});
			}
		});
		
		containerPopup.add(mi);
	}
	
	
	public abstract void notifyScenarioNameChanged(ScenarioVisual scenVis,
		String newName);
	
	
	public void notifyCrossReferenceCreated(CrossReferenceSer crSer)
	{
		notifyCrossReferenceCreatedOrDeleted(crSer.fromNodeId);
	}
		
		
	public void notifyCrossReferenceDeleted(String fromNodeId, String toNodeId, String crId)
	{
		notifyCrossReferenceCreatedOrDeleted(fromNodeId);
	}
		
		
	protected void notifyCrossReferenceCreatedOrDeleted(String fromNodeId)
	{
		// Determine if this View is nested in a MultiHierarchyPanel. If so,
		// noitify the MultiHierarchyPanel by calling the notifyMappingChanged
		// method on the MultiHierarchyPanel.
		
		Component comp = this;
		for (;;)
		{
			Container parent = comp.getParent();
			if (parent instanceof MultiHierarchyPanel)
			{
				VisualComponent fromVisual;
				try { fromVisual = identifyVisualComponent(fromNodeId); }
				catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); return; }
				
				if ((fromVisual != null) && (fromVisual instanceof TreeTableNode))
				{
					((MultiHierarchyPanel)parent).notifyMappingChanged((TreeTableNode)fromVisual);
				}
				
				return;
			}
			else if (parent instanceof PanelManager) return;
			else if (parent instanceof Window) return;
			
			comp = parent;
		}
	}
		
		
	public abstract Class getVisualClass(NodeSer node);
	
	
	public abstract ScenarioVisual showScenario(String scenarioId)
	throws
		Exception;
	
	
	/**
	 * Create a Visual to depict the specified Node in this View. If 'container'
	 * is not equal to null, add the new Visual to this View, under that Component.
	 */
	 
	public VisualComponent makeTabularVisual(NodeSer nodeSer)
	throws
		ParameterError,
		Exception
	{
		Serializable[] values = getModelEngine().getAttributeDefaultValuesForNode(nodeSer.getNodeId());
		Class visualClass = getVisualClass(nodeSer);
		Constructor constructor = visualClass.getConstructor(NodeSer.class, View.class,
			Serializable[].class);
		
		VisualComponent visual = (TabularVisualJ)(constructor.newInstance(nodeSer, this, values));
		return visual;
	}


	public FieldEditor getEditor(String fieldName, int row)
	{
		if (row < 0) throw new RuntimeException("Attempt to get Editor for non-visible row");
		TreePath rowPath = getTreeTable().getPathForRow(row);
		return getEditor(fieldName, rowPath);
	}
	
	
	public FieldEditor getEditor(String fieldName, TreePath rowPath)
	{
		List<String> fnames = getFieldNames();
		int column = -1;
		for (String fname : fnames)
		{
			column++;
			if (fname.equals(fieldName)) break;
		}
		
		if (column < 0) throw new RuntimeException(
			"Column with name " + fieldName + " not found");
		
		return getEditor(column, rowPath);
	}
	
	
	public FieldEditor getEditor(int column, TreePath rowPath)
	{
		if (column < 0) throw new RuntimeException("Column is less than 0");
		return new FieldEditorImpl(rowPath, column, new JTextField());
	}
	

	public class FieldEditorImpl extends DefaultCellEditor implements FieldEditor
	{
		TabularVisualJ visual;
		int column;
		
		FieldEditorImpl(TreePath row, final int column, JTextField textField)
		{
			super(textField);
			this.visual = (TabularVisualJ)(row.getLastPathComponent());
			this.column = column;
			addCellEditorListener(new CellEditorListener()
			{
				public void editingCanceled(ChangeEvent e) {}
				
				public void editingStopped(ChangeEvent e)
				{
					// Note that BasicTreeTableModel.setValueAt gets called, but it is just an accessor.
					try { visual.updateServer(column); }
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(TreeTablePanelViewBase.this, ex);
					}
				}
			});
		}

		public void clear() throws Warning, Exception
		{
			treeTableModel.setValueAt(null, visual, column);
			try { visual.updateServer(column); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(TreeTablePanelViewBase.this, ex);
			}
		}
		
		public void show()
		throws
			NotVisible
		{
			TreeTableNode[] nodes = treeTableModel.getPathToRoot(visual);
			TreePath path = new TreePath(nodes);
			int rowNo = treeTable.getRowForPath(path); 
			if (rowNo < 0) throw new NotVisible(path.toString());;
			EventObject e = new EventObject(visual);
			treeTable.editCellAt(rowNo, column, e);
		}
	}
	

  /* Methods that can be extended, but do not usually need to be. These are for
  	the purpose of propagating changes from one part of the View to other
  	indirectly related parts (e.g., overlays). */

	
	protected void treeNodesChanged(TreeModelEvent e) {}
	
	protected void treeNodesInserted(TreeModelEvent e) {}
	
	protected void treeNodesRemoved(TreeModelEvent e) {}
	
	protected void treeStructureChanged(TreeModelEvent e) {}
	
	public void notifyVisualComponentPopulated(VisualComponent visual,
		boolean populated) {}
	
	public void notifyVisualRemoved(VisualComponent visual) {}
	
	
	protected void keyPressed(EditActionType keyAction, TreePath[] selectedPaths,
		int[] columns)
	{
		keyPressedDefault(keyAction, selectedPaths, columns);
	}
	
	
	public void saveFocusFieldChanges() {}
	
	
	public void domainCreated(String domainId) {}
	
	
	public void motifCreated(String motifId) {}
	

  /* ***************************************************************************/
  /* ***************************************************************************/
  /* Standard parts - the parts below should not be extended */
	
	
  /* Fields */

	
	private JXTreeTable treeTable;
	
	private BasicTreeTableModel treeTableModel;
	
	private List<TreeTableComponentChangeListener> listeners = 
		new Vector<TreeTableComponentChangeListener>();
	
	//private ValueControlFactory valueControlFactory;//should belong to ControlPanel
	//private VisibilityControlFactory visibilityControlFactory;//should belong to ControlPanel
	
	private TabularSelectionHandler tabularSelectionHandler = null;
	
	private RowSelectionListener temporaryVisSelListener = null;
	
	private boolean popup;
			
	
  /* Constructors */


	protected TreeTablePanelViewBase(final PanelManager panelManager, ViewFactory viewFactory,
		NodeSer rootNodeSer, ModelEngineRMI modelEngine)
	throws
		Exception
	{
		super(modelEngine, panelManager, rootNodeSer, viewFactory,
			rootNodeSer.getFullName(), true);
		
		/*
		// Add a Button to add another View.
		JButton newViewButton = new JButton(ButtonIcons.NewViewButtonIcon);
		newViewButton.setToolTipText("Add a new View after this one");
		newViewButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				(new RelatedDomainSelector(TreeTablePanelViewBase.this)).show();
			}
		});
		getInfoPanel().add(newViewButton);
		//the infoPanel will no longer be there if this uses ScenarioViewPanelHelper
		

		// Add a URL button to enable the user to obtain a URL for this View.
		getInfoPanel().add(new ViewURLButton(this));
		
		
		// Add button for closing this View.
		JButton closeButton = new JButton(ButtonIcons.CloseButtonIcon);
		closeButton.setToolTipText("Close this View");
		closeButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				panelManager.removePanel(TreeTablePanelViewBase.this);
			}
		});
		getInfoPanel().add(closeButton);
		*/
		
		buildTreeTable();
	}
	
	
	protected void buildTreeTable()
	{
		// Create root Node Visual.
		Set<VisualComponent> roots;
		VisualComponent root = null;
		NodeSer nodeSer;
		try
		{
			nodeSer = getModelEngine().getNode(getOutermostNodeSer().getNodeId());
			setOutermostNodeSer(nodeSer);
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
			return;
		}

		try { root = makeTabularVisual(nodeSer); }
		catch (Exception ex) { throw new RuntimeException(ex); }

		// Create TreeTableModel.

		this.treeTableModel = new BasicTreeTableModel((TreeTableNode)root);
		
		List<String> columnNames = new Vector<String>(getFieldNames());
		
		columnNames.add(0, "Name");
				// See also PersistentNodeImpl.renderAsHTML().
		
		this.treeTableModel.setColumnIdentifiers(columnNames);
		
		// Create the Tree Table.
		
		this.treeTable = new JXTreeTable(this.treeTableModel)
		{
			{
				List<TableColumn> cols = getColumns();
				for (TableColumn col : cols) col.setResizable(true);
			}
			
			public final TableCellEditor getCellEditor(int rowNo, int column)
			{
				if (rowNo < 0) throw new RuntimeException(
					"Attempt to edit a non-visible row");
				if (column < 0) throw new RuntimeException(
					"Attempt to get Editor for column " + column);
				
				TreePath path = getPathForRow(rowNo); 
				return getEditor(column, path);
			}
		};
		
		
		// Configure the tree.
		
		treeTable.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		treeTable.setShowsRootHandles(true);
		treeTable.setRootVisible(true);
		treeTable.setEditable(true);
		
		
		// Set cell editor?
		
		//TreeTableCellEditor cellEditor = new ...
		//treeTable.setCellEditor(cellEditor);
		
		
		// Set selection listeners. Needed?
		
		treeTable.addTreeSelectionListener(new TreeSelectionListener()
		{
			public void valueChanged(TreeSelectionEvent e) {}
		});
		
		
		treeTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			public void valueChanged(ListSelectionEvent e) {}
		});
				
		
		// Add mouse listener.
		
		treeTable.addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent e)
			{
				if ((e.getButton() != MouseEvent.BUTTON1) || e.isControlDown()) return;
				
				synchronized (TreeTablePanelViewBase.this)
				{
					if ((tabularSelectionHandler != null) || (temporaryVisSelListener != null)) try
					{
						TreePath selPath = treeTable.getPathForLocation(e.getX(), e.getY());
						int selRow = treeTable.getRowForPath(selPath);
						if(selRow == -1) return;
						TabularVisualJ visual = (TabularVisualJ)(selPath.getLastPathComponent());
						
						if (tabularSelectionHandler != null)
							tabularSelectionHandler.tableVisualSelected(visual);
						
						if (temporaryVisSelListener != null)
							temporaryVisSelListener.visualSelected(visual);
					}
					finally  // remove handler
					{
						tabularSelectionHandler = null;
						removeTemporaryRowSelectionListener();
					}
				}
			}
			
			
			public void mouseDrgged(MouseEvent e)
			{
				// Rows can be dragged up and down to change their position.
				// TBD.
			}
			
			
			public void mouseReleased(MouseEvent e)
			{
				if (! ((e.getButton() == MouseEvent.BUTTON2)
					|| (e.getButton() == MouseEvent.BUTTON3)
					|| ((e.getButton() == MouseEvent.BUTTON1) && e.isControlDown())
				)) return;
				
				TreePath selPath = treeTable.getPathForLocation(e.getX(), e.getY());
				int selRow = treeTable.getRowForPath(selPath);
				if(selRow == -1) return;
				TabularVisualJ visual = (TabularVisualJ)(selPath.getLastPathComponent());
				
				try  // Create a popup menu to contain the menu item.
				{
					JPopupMenu containerPopup = new JPopupMenu();
					visual.addContextMenuItems(containerPopup, e.getX(), e.getY());
					addViewContextMenuItems(containerPopup, visual, e.getX(), e.getY());
					containerPopup.show(TreeTablePanelViewBase.this, e.getX(), e.getY());
				}
				catch (Exception ex)
				{
					visual.setInconsistent(true);
					
					ErrorDialog.showReportableDialog(TreeTablePanelViewBase.this,
						ThrowableUtil.getAllMessages(ex), "Error", ex); return;
				}
			}
		});
		
		
		// Add keyboard listener to handle delete, etc.
		
		treeTable.addKeyListener(new KeyListener()
		{
			public void keyPressed(KeyEvent e)
			{
				if (! e.isActionKey()) return;
				
				// Determine which row or cell is selected.
				
				TreePath[] selectedPaths = treeTable.getTreeSelectionModel().getSelectionPaths();
				int[] columns = treeTable.getColumnModel().getSelectedColumns();
				
				if ((selectedPaths.length == 0) || (columns.length == 0)) return;
				
				EditActionType keyAction;
				
				switch (e.getKeyCode())
				{
				case KeyEvent.VK_ENTER: keyAction = EditActionType.modify; break;
				case KeyEvent.VK_DELETE: keyAction = EditActionType.delete; break;
				case KeyEvent.VK_CLEAR: keyAction = EditActionType.delete; break;
				case KeyEvent.VK_INSERT: keyAction = EditActionType.insertBefore; break;
				case KeyEvent.VK_CUT: keyAction = EditActionType.cut; break;
				case KeyEvent.VK_COPY: keyAction = EditActionType.copy; break;
				case KeyEvent.VK_PASTE: keyAction = EditActionType.paste; break;
				case KeyEvent.VK_FIND: keyAction = EditActionType.find; break;
				case KeyEvent.VK_HELP: keyAction = EditActionType.help; break; 
				case KeyEvent.VK_UNDO: keyAction = EditActionType.undo; break;
				default: return;
				}
				
				TreeTablePanelViewBase.this.keyPressed(keyAction, selectedPaths, columns);
			}
			
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
		});
		
		
		// Add change listener for updating server. It merely delegates to
		// View instance methods, allowing View subclasses to implement them.
		
		/*treeTableModel.addTreeModelListener(new TreeModelListener()
		{
			public void treeNodesChanged(TreeModelEvent e) { treeNodesChanged(e); }
			
			public void treeNodesInserted(TreeModelEvent e) { treeNodesInserted(e); }
			
			public void treeNodesRemoved(TreeModelEvent e) { treeNodesRemoved(e); }
			
			public void treeStructureChanged(TreeModelEvent e) { treeStructureChanged(e); }
		});*/
		
		
		// Add listener to the JXTreeTable to notify the TreeTableComponentChangeListeners.
		// Enables this View to notify other TreeTable Views that this one has changed.
		
		treeTableModel.addTreeModelListener(new TreeModelListener()
		{
			public void treeNodesChanged(TreeModelEvent e)
			{
				Object obj = e.getTreePath().getLastPathComponent();
				TabularVisualJ visualInThisView = (TabularVisualJ)obj;
				for (TreeTableComponentChangeListener listener : listeners)
				{
					listener.notifyNodeChanged(visualInThisView);
				}
			}
			
			public void treeNodesInserted(TreeModelEvent e)
			{
				Object obj = e.getTreePath().getLastPathComponent();
				TabularVisualJ visualInThisView = (TabularVisualJ)obj;
				for (TreeTableComponentChangeListener listener : listeners)
				{
					listener.notifyNodeInserted(visualInThisView);
				}
			}
			
			public void treeNodesRemoved(TreeModelEvent e)
			{
				Object obj = e.getTreePath().getLastPathComponent();
				TreeTableNode visualInThisView = (TreeTableNode)obj;
				for (TreeTableComponentChangeListener listener : listeners)
				{
					listener.notifyNodeRemoved(visualInThisView);
				}
			}
			
			public void treeStructureChanged(TreeModelEvent e)
			{
				Object obj = e.getTreePath().getLastPathComponent();
				TabularVisualJ visualInThisView = (TabularVisualJ)obj;
				for (TreeTableComponentChangeListener listener : listeners)
				{
					listener.notifyTreeStructureChanged(visualInThisView);
				}
			}
		});
		
		getDisplayArea().removeAll();
		getDisplayArea().add(new JScrollPane(treeTable), "Tree Table");
		
		try { root.refreshRedundantState(); }
		catch (Exception ex) { throw new RuntimeException(ex); }

		try { root.populateChildren(); }
		catch (Exception pe) { throw new RuntimeException(pe); }
	}
	
	
  /* Final methods */
	
	
	protected final void setLeafIcon(ImageIcon ii)
	{
		((DelegatingRenderer)(treeTable.getTreeCellRenderer())).setLeafIcon(ii);
	}
	
	
	protected final void setOpenIcon(ImageIcon ii)
	{
		((DelegatingRenderer)(treeTable.getTreeCellRenderer())).setOpenIcon(ii);
	}
	
	
	protected final void setClosedIcon(ImageIcon ii)
	{
		((DelegatingRenderer)(treeTable.getTreeCellRenderer())).setClosedIcon(ii);
	}
		
		
	/**
	 * Modal operation.
	 * 
	 * Require the user to either (1) click on a Node that is of the specified type,
	 * (2) click elsewhere within this View, or press the Escape key. If the user
	 * clicks on a Node of the specified type, the Node is returned. If the user
	 * clicks elsewhere, return null. If the user presses the Escape key, return null.
	 */
	 
	public final void selectNode(TabularSelectionHandler handler)
	{
		synchronized (this)
		{
			this.tabularSelectionHandler = handler;
		}
	}
	
	
	/**
	 * If this TreeTablePanelView exists in a MultiHierarchyPanel, return 
	 * the TreeTablePanelViews that are adjacent to this TreeTablePanelView.
	 * Otherwise return an empty Set.
	 */
	 
	public final Set<TreeTablePanelViewBase> getAdjacentTreeTablePanelViews()
	{
		List<TreeTableComponent> treePanels = 
			getMultiHierarchyPanel().getAdjacentTreeTableComponents(this);
		
		Set<TreeTablePanelViewBase> views = new HashSet<TreeTablePanelViewBase>();
		for (TreeTableComponent ttc : treePanels)
		{
			if ((ttc != this) && (ttc instanceof TreeTablePanelViewBase))
				views.add((TreeTablePanelViewBase)ttc);
		}
		
		return views;
	}
	
	
	public final MultiHierarchyPanel getMultiHierarchyPanel()
	{
		Container parent = this.getParent();
		if (parent instanceof MultiHierarchyPanel) return (MultiHierarchyPanel)parent;
		return null;
	}
	
	
	public final VisualComponent getOutermostVisual()
	{
		if (treeTableModel == null) return null;
		return (VisualComponent)(treeTableModel.getRoot());
	}
	
	
	public List<VisualComponent> getOutermostVisuals()
	{
		List<VisualComponent> visuals = new Vector<VisualComponent>();
		VisualComponent vis = getOutermostVisual();
		if (vis == null) return visuals;
		visuals.add(vis);
		return visuals;
	}
	
	
	public final String getRootNodeId()
	{
		VisualComponent vc = getOutermostVisual();
		if (vc == null) return null;
		return vc.getNodeId();
	}
	
	
	public NodeSer getRootNodeSer() { return getOutermostNodeSer(); }
	
	
	public final JXTreeTable getTreeTable() { return treeTable; }
	
	
	public Color getBackgroundSelectionColor()
	{
		DefaultTreeCellRenderer renderer = 
			(DefaultTreeCellRenderer)(treeTable.getTreeCellRenderer());
		return renderer.getBackgroundSelectionColor();
	}
	
	
	public Color getBackgroundNonSelectionColor()
	{
		DefaultTreeCellRenderer renderer = 
			(DefaultTreeCellRenderer)(treeTable.getTreeCellRenderer());
		return renderer.getBackgroundNonSelectionColor();
	}
	
	
	public DefaultTreeTableModel getTreeTableModel()
	{
		return treeTableModel;
	}
	
	
	protected BasicTreeTableModel getBasicTreeTableModel()
	{
		return treeTableModel;
	}
	
	
	public void repaintVisual(VisualComponent visual)
	{
		TreePath rowPath = new TreePath(treeTableModel.getPathToRoot((TreeTableNode)visual));
		try { treeTableModel.valueForPathChanged(rowPath, visual); }
		catch (RuntimeException ex)
		{
			GlobalConsole.println("Path='" + rowPath.toString() + "'");
			GlobalConsole.println(ex.getMessage());
			//throw ex;  // allow it.
		}
	}
	
	
	/**
	 * Define a key action handler. The keyboard actions update the server,
	 * which then notifies selected Visuals in this View, causing those Visuals
	 * to refresh themselves.
	 */
	
	final protected void keyPressedDefault(EditActionType keyAction, TreePath[] selectedPaths,
		int[] columns)
	{
		if (selectedPaths.length == 0) return;
		if (selectedPaths.length > 1)
		{
			ErrorDialog.showErrorDialog(this, "May only select one row for this action");
			return;
		}
		
		TabularVisualJ visual = (TabularVisualJ)(selectedPaths[0].getLastPathComponent());
		
		switch (keyAction)
		{
		case create: insertNewAfterRow(visual); break;
		case delete: deleteRow(visual); break;
		case paste: duplicateRow(visual); break;
		case insertBefore: insertNewBeforeRow(visual); break;
		case insertAfter: insertNewAfterRow(visual); break;
		case modify:
			{
				if (columns.length != 1) break;
				modifyCell(visual, columns[0]);
				break;
			}
		//case cut: cutRow(visual); break;
		//case copy: copyRow(visual); break;
		//case paste: pasteRow(visual); break;
		case find: break;
		case help: break;
		case undo: break;
		default: break;
		}
	}

	
	/* TabularVisualJ methods:
	void deleteRow()
	void editCell(int colNo)
	void clearRow()
	void clearCell(int colNo)
	void duplicateRow()
	void cutRow()
	void copyRow()
	void pasteRow()
	void insertNewBefore()
	void insertNewAfter()
	void demote()
	void promote()
	*/

	final void deleteRow(TabularVisualJ visual) { visual.deleteRow(); }
	final void clearRow(TabularVisualJ visual) { visual.clearRow(); } // not used.
	final void clearCell(TabularVisualJ visual, int col) { visual.clearCell(col); }
	final void duplicateRow(TabularVisualJ visual) { visual.duplicateRow(); }
	//final void cutRow(TabularVisualJ visual) { visual.cutRow(); }
	//final void copyRow(TabularVisualJ visual) { visual.copyRow(); }
	//final void pasteRow(TabularVisualJ visual) { visual.pasteRow(); }
	final void insertNewBeforeRow(TabularVisualJ visual) { visual.insertNewBefore(); }
	final void insertNewAfterRow(TabularVisualJ visual) { visual.insertNewAfter(); }
	
	final void modifyCell(TabularVisualJ visual, int column)
	{
		TreePath rowPath = new TreePath(treeTableModel.getPathToRoot(visual));
		FieldEditor editor = getEditor(column, rowPath);
		visual.updateServer(column);
	}
	
	

  /* From TreeTableComponent */
	
	
	public JXTreeTable getJXTreeTable() { return treeTable; }
	
	
	public TreeTableNode getNodeById(String nodeId)
	{
		Set<VisualComponent> visuals;
		try { visuals = identifyVisualComponents(nodeId); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		if (visuals.size() > 1) throw new RuntimeException(
			"More than one Visual with Node Id " + nodeId);
		if (visuals.size() == 0) return null;
		
		VisualComponent visual = null;
		for (VisualComponent v : visuals) visual = v;
		return (TreeTableNode)visual;
	}
	
	
	public void addChangeListener(TreeTableComponentChangeListener listener)
	{
		listeners.add(listener);
	}
	
	
	public void addTemporaryRowSelectionListener(RowSelectionListener listener)
	{
		/* Called when a Visual is selected. After being called, the listener
			is removed. */

		this.temporaryVisSelListener = listener;
	}
	
	
	public void removeTemporaryRowSelectionListener()
	{
		this.temporaryVisSelListener = null;
	}
	
	
	public int getYPosition(TreeTableNode node)
	{
		try
		{
			return MultiHierarchyPanel.getYPosition(treeTable, node) + 
				((JPanel)(getDisplayArea())).getY();
		}
		catch (Exception ex) { throw new RuntimeException(ex); }
	}
	
	
	public TreeTableComponent getPriorTreeTableComponent()
	{
		MultiHierarchyPanel mhPanel = getMultiHierarchyPanel();
		if (mhPanel == null) return null;
		int panelNo = mhPanel.getTreeTableComponentNo(this);
		if (panelNo == 0) return null;
		return mhPanel.getTreeTableComponent(panelNo-1);
	}
	
	
	public TreeTableComponent getNextTreeTableComponent()
	{
		MultiHierarchyPanel mhPanel = getMultiHierarchyPanel();
		if (mhPanel == null) return null;
		int panelNo = mhPanel.getTreeTableComponentNo(this);
		if (mhPanel.isLastTreeTableComponent(this)) return null;
		return mhPanel.getTreeTableComponent(panelNo+1);
	}
	
	

  /* From PanelManager nested types. */


	public NodeView getNodeView()
	{
		return this;
	}


	public Set<NodeView> getNodeViews(String nodeId)
	{
		Set<NodeView> nodeViews = new HashSet<NodeView>();
		nodeViews.add(this);
		return nodeViews;
	}


	public Set<View> getViews(String nodeId)
	{
		Set<View> views = new HashSet<View>();
		views.add(this);
		return views;
	}
	
	
	public Set<View> getViews()
	{
		Set<View> views = new HashSet<View>();
		views.add(this);
		return views;
	}
	
	
	public int getNoOfViews() { return 1; }



  /* From View. These are called in response to server Notices. */
	
	
	public Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError
	{
		Set<VisualComponent> visuals = new HashSet<VisualComponent>();
		VisualComponent ov = getOutermostVisual();
		if (ov == null) return visuals;
		((TabularVisualJ)ov).identifyVisualComponents(visuals, nodeId);
		return visuals;
	}
	

	public VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError
	{
		return ((TabularVisualJ)(getOutermostVisual())).identifyVisualComponent(nodeId);
	}
	
	
	public Set<VisualComponent> identifyVisualComponents()
	throws
		ParameterError
	{
		Set<VisualComponent> visuals = new HashSet<VisualComponent>();
		((TabularVisualJ)(getOutermostVisual())).identifyVisualComponents(
			new HashSet<VisualComponent>());
		return visuals;
	}
	
	
	public Set<VisualComponent> identifyVisualComponents(Class c)
	throws
		ParameterError
	{
		return ((TabularVisualJ)(getOutermostVisual())).identifyVisualComponents(
			new HashSet<VisualComponent>(), c);
	}
	
	
	public Set<VisualComponent> addNode(String nodeId)
	throws
		Exception
	{
		Set<VisualComponent> newVisuals = new HashSet<VisualComponent>();
		Set<VisualComponent> visuals = identifyVisualComponents(nodeId);
		
		if (visuals.size() > 0) return newVisuals;
		
		NodeSer nodeSer = getModelEngine().getNode(nodeId);

		if (! nodeIsPermitted(nodeSer)) return newVisuals;
		HierarchySer tabNodeSer = (HierarchySer)nodeSer;
		
		String parentId = nodeSer.getParentNodeId();
		VisualComponent newVisual = null;
		if (parentId == null) // a Domain Node.
		{
			throw new RuntimeException("Cannot create the root in this way");
		}
		else
		{
			Set<VisualComponent> parents = identifyVisualComponents(parentId);
			if (parents.size() == 0) return newVisuals;
			if (parents.size() == 1)
			{
				for (VisualComponent parent : parents)  // will only iterate once.
				{
					newVisual = makeTabularVisual(nodeSer);
					
					// Determine the preceding TreeTableNode, or if
					// the new TreeTableNode should be the only child of its parent.
					
					MutableTreeTableNode priorNode;
					String[] childNodeIds = parent.getNodeSer().getChildNodeIds();
					int seqNo = ((HierarchySer)nodeSer).getSeqNo();
					
					int priorSeqNo = Math.min(childNodeIds.length-1, seqNo-1);
					for (;;) // until the prior Node is permitted.
					{
						if (priorSeqNo < 0)
						{
							priorNode = null;  // Make new new node the first one.
							break;
						}
						
						String priorId = childNodeIds[priorSeqNo];
						NodeSer priorNodeSer = getModelEngine().getNode(priorId);
						if (nodeIsPermitted(priorNodeSer))
						{
							priorNode = (MutableTreeTableNode)(identifyVisualComponent(priorId));
							if (priorNode == null) throw new RuntimeException(
								"Visual with Id " + priorId + " not found in View.");
							break;
						}
						
						priorSeqNo--;
					}
					
					if (priorNode == null)
						treeTableModel.insertNodeInto((MutableTreeTableNode)newVisual,
							(MutableTreeTableNode)parent, 0);
					else
						treeTableModel.insertNodeAfter((MutableTreeTableNode)newVisual, 
							(MutableTreeTableNode)parent, priorNode);
					
					treeTable.repaint();
				}
			}
			else
				throw new RuntimeException("More than one Visuals for Node Id " + parentId);
		}
		
		newVisuals.add(newVisual);


		// Subscribe to receive update notifications for the child.
		// Note that this does NOT result in additional server connections.

		if (this.getListenerRegistrar() != null)
			this.getListenerRegistrar().subscribe(nodeId, true);
		
		newVisual.refreshRedundantState();
		
		// Call this methoid recursively on the children of the Node.
		
		newVisual.populateChildren();
		
		
		return newVisuals;
	}
	
	
	public void removeVisual(VisualComponent visual)
	throws
		Exception
	{
		// Remove child Visuals.
		
		if (! (visual instanceof TabularVisualJ)) throw new RuntimeException(
			"Visual is not a TabularVisualJ");
		
		TabularVisualJ tabVis = (TabularVisualJ)visual;
		Enumeration e = tabVis.children();
		Set<VisualComponent> children = new HashSet<VisualComponent>();
		for (; e.hasMoreElements();)
		{
			TabularVisualJ child = (TabularVisualJ)(e.nextElement());
			children.add(child);
		}
		
		for (VisualComponent child : children) removeVisual(child);
		

		// Remove from GUI.
		
		TabularVisualJ parent = (TabularVisualJ)(tabVis.getParent());
		
		if (parent == null)
			throw new RuntimeException("Cannot remove the root in this way");
		
		getBasicTreeTableModel().removeChild((MutableTreeTableNode)visual);
		
		
		// Unsubscribe.
		
		if (this.getListenerRegistrar() != null) try
		{
			this.getListenerRegistrar().subscribe(visual.getNodeId(), false);
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		
		
		// Notify alias owners.
		
		this.notifyVisualRemoved(visual);
	}


  /* From ViewPanelBase */


	public boolean isPopup() { return this.popup; }
	
	
	public void setIsPopup(boolean yes) { this.popup = yes; }
	
	
	public Set<VisualComponent> refreshVisuals(String nodeId)
	throws
		Exception
	{
		Set<VisualComponent> visuals = new HashSet<VisualComponent>();
		TreeTableNode node = getNodeById(nodeId);
		if (node == null) return visuals;
		if (! (node instanceof VisualComponent)) return visuals;
		VisualComponent visual = (VisualComponent)node;
		visuals.add(visual);
		visual.refresh();
		return visuals;
	}
		
		
	public VisualComponent makeVisual(NodeSer node, Container container,
		boolean asIcon)
	throws
		ParameterError,
		Exception
	{
		return makeTabularVisual(node);
	}
	
	
	public void select(VisualComponent visual, boolean yes)
	{
		// Add visual to the selection set.
		if (! (visual instanceof TabularVisualJ)) return;
		TabularVisualJ tvisual = (TabularVisualJ)visual;
		TreeTableNode[] nodes = getBasicTreeTableModel().getPathToRoot(tvisual);
		TreePath path = new TreePath(nodes);
		TreeSelectionModel selectionModel = getTreeTable().getTreeSelectionModel();
		
		if (yes) selectionModel.addSelectionPath(path);
		else selectionModel.removeSelectionPath(path);
	}
	
	
	public void selectNodesWithId(String nodeId, boolean yes)
	throws
		ParameterError
	{
		VisualComponent visual = identifyVisualComponent(nodeId);
		if (visual == null) return;
		
		select(visual, yes);
	}
	

	public Set<VisualComponent> getSelected()
	{
		// Return the Set of Visuals that are currently selected.
		
		TreeSelectionModel selectionModel = getTreeTable().getTreeSelectionModel();
		TreePath[] paths = selectionModel.getSelectionPaths();
		
		Set<VisualComponent> visuals = new HashSet<VisualComponent>();
		
		for (TreePath path : paths)
		{
			Object[] objects = path.getPath();
			Object lastObject = objects[objects.length-1];
			if (! (lastObject instanceof TabularVisualJ)) continue;
			VisualComponent vis = (VisualComponent)lastObject;
			visuals.add(vis);
		}
		
		return visuals;
	}
	

	public boolean isSelected(VisualComponent visual)
	{
		// Return true if the specified Visual is currently selected.
		
		TreeSelectionModel selectionModel = getTreeTable().getTreeSelectionModel();
		if (! (visual instanceof TreeTableNode)) return false;
		TreeTableNode tvisual = (TreeTableNode)visual;
		TreeTableNode[] nodes = getBasicTreeTableModel().getPathToRoot(tvisual);
		TreePath path = new TreePath(nodes);
		return selectionModel.isPathSelected(path);
	}


	public void selectAll(boolean yes)
	{
		// Select all Visuals in this View.
		Set<VisualComponent> visuals;
		try { visuals = identifyVisualComponents(); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
			return;
		}
		
		for (VisualComponent vis : visuals) select(vis, yes);
	}
	
	
  /* Provide implementation of CrossMapPanel that can be used as a bridge between
  	two TreeTablePanelViews. */


	public static class CenterPanel extends CrossMapPanel
	{
		String[] namedRefNames;
		
		
		public CenterPanel(TreeTablePanelViewBase leftPanel, TreeTablePanelViewBase rightPanel,
			String... namedRefNames)
		{
			super(leftPanel, rightPanel);
			this.namedRefNames = namedRefNames;
			this.setLeftToRightLineColor(Color.green);
			this.setRightToLeftLineColor(Color.blue);
		}
		
		
		public String[] getNamedRefNames() { return namedRefNames; }
		
		
		protected void showFatalServerError(Exception ex)
		{
			ErrorDialog.showReportableDialog(CenterPanel.this,
				"While contacting server", "Error", ex);
		}
		
		
		protected TreeTablePanelViewBase getLeftPanelView()
		{
			return (TreeTablePanelViewBase)(getLeftPanel());
		}
		
		
		protected TreeTablePanelViewBase getRightPanelView()
		{
			return (TreeTablePanelViewBase)(getRightPanel());
		}
		
		
	  /* From CrossMapPanel. */
		
		
		public void refresh()  // TBD: This implementation is inefficient.
		{
			removeAllConnections();
			
			// Rebuild all connections.
			
			Set<VisualComponent> visuals;
			try { visuals = ((View)(getLeftPanel())).identifyVisualComponents(); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(this, ex);
				return;
			}
			
			for (VisualComponent vis : visuals)
			{
				leftTreeTableNodeChanged((TreeTableNode)vis);
			}
			
			try { visuals = ((View)(getRightPanel())).identifyVisualComponents(); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(this, ex);
				return;
			}
			
			for (VisualComponent vis : visuals)
			{
				rightTreeTableNodeChanged((TreeTableNode)vis);
			}
			
			repaint();
		}
		
		
		public void leftTreeTableNodeChanged(TreeTableNode node)
		{
			// Remove all connections from and to the Node.
			
			leftNodeRemoved(node);
			
			// Rebuild all connections from and to the Node.
			
			connectNode((TreeTablePanelViewBase)(getRightPanel()), node);
			repaint();
		}
		
		
		protected void connectNode(TreeTablePanelViewBase otherPanel, TreeTableNode node)
		{
			for (String namedRefName : namedRefNames)
			{
				NodeSer[] fromNodeSers;
				NodeSer[] toNodeSers;
				try
				{
					toNodeSers = otherPanel.getModelEngine().getNamedRefToNodes(
						((TabularVisualJ)node).getNodeId(), namedRefName);
					fromNodeSers = otherPanel.getModelEngine().getNamedRefFromNodes(
						((TabularVisualJ)node).getNodeId(), namedRefName);
				}
				catch (Exception ex)
				{
					showFatalServerError(ex);
					return;
				}
				
				for (NodeSer toNodeSer : toNodeSers)
				{
					// Determine if the toNode is in the other Panel.
					TreeTableNode otherNode = otherPanel.getNodeById(toNodeSer.getNodeId());
					if (otherNode != null)
					{
						if (otherPanel == getRightPanel())
							connectLeftToRightNode(true, node, otherNode, false);
						else
							connectRightToLeftNode(true, node, otherNode, false);
					}
				}
				
				for (NodeSer fromNodeSer : fromNodeSers)
				{
					// Determine if the fromNode is in the other Panel.
					TreeTableNode otherNode = otherPanel.getNodeById(fromNodeSer.getNodeId());
					if (otherNode != null)
					{
						if (otherPanel == getRightPanel())
							connectRightToLeftNode(true, otherNode, node, false);
						else
							connectLeftToRightNode(true, otherNode, node, false);
					}
				}
			}
		}
		
		
		public void leftTreeTableNodeInserted(TreeTableNode node)
		{
			connectNode((TreeTablePanelViewBase)(getRightPanel()), node);
		}
		
		
		public void leftTreeTableNodeRemoved(TreeTableNode node)
		{
			leftNodeRemoved(node);
		}
		
		
		public void leftTreeStructureChanged(TreeTableNode node)
		{
			// Call leftTreeStructureChanged on the Node and each of its
			// children, recursively.
			
			leftTreeTableNodeChanged(node);
			TreeTableModel model = getLeftPanel().getJXTreeTable().getTreeTableModel();
			int noOfChildren = model.getChildCount(node);
			for (int i = 0; i < noOfChildren; i++)
			{
				TreeTableNode child = (TreeTableNode)(model.getChild(node, i));
				leftTreeStructureChanged(child);
			}
		}
		
		
		public void rightTreeTableNodeChanged(TreeTableNode node)
		{
			// Remove all connections from and to the Node.
			
			rightNodeRemoved(node);
			
			// Rebuild all connections from and to the Node.
			
			connectNode((TreeTablePanelViewBase)(getLeftPanel()), node);
			repaint();
		}
		
		
		public void rightTreeTableNodeInserted(TreeTableNode node)
		{
			connectNode((TreeTablePanelViewBase)(getLeftPanel()), node);
		}
		
		
		public void rightTreeTableNodeRemoved(TreeTableNode node)
		{
			rightNodeRemoved(node);
		}
		
		
		public void rightTreeStructureChanged(TreeTableNode node)
		{
			// Call rightTreeStructureChanged on the Node and each of its
			// children, recursively.
			
			rightTreeTableNodeChanged(node);
			TreeTableModel model = getRightPanel().getJXTreeTable().getTreeTableModel();
			int noOfChildren = model.getChildCount(node);
			for (int i = 0; i < noOfChildren; i++)
			{
				TreeTableNode child = (TreeTableNode)(model.getChild(node, i));
				rightTreeStructureChanged(child);
			}
		}


		public void connectionSelected(final TreeTableNode leftNode,
			final TreeTableNode rightNode, final int x, final int y)
		{
		}
		
		
		public void addContextMenuItemsForConnections(final JPopupMenu containerPopup,
			final TreeTableNode fromNode, final TreeTableNode toNode, final int x, final int y)
		{
			final TabularVisualJ fromNodeVisual = (TabularVisualJ)fromNode;
			final TabularVisualJ toNodeVisual = (TabularVisualJ)toNode;
			
			JMenuItem mi = new HelpfulMenuItem("Delete Cross Reference", false,
				"Delete the selected Cross Reference");
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					JPopupMenu popup1 = new JPopupMenu();
					CrossReferenceSer[] crSers;
					try { crSers = getLeftPanelView().getModelEngine()
						.getCrossReferences(fromNodeVisual.getNodeId(), toNodeVisual.getNodeId()); }
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(CenterPanel.this, ex);
						return;
					}
					
					for (CrossReferenceSer crSer : crSers)
					{
						final String crId = crSer.getNodeId();
						JMenuItem mi = new HelpfulMenuItem("Delete " + crSer.namedRefName +
							" from " + fromNodeVisual.getName() + " to " +
							toNodeVisual.getName(), false,
							"The '" + crSer.namedRefName + "' " +
							HelpWindow.createHref("Cross References", "Cross Reference") +
							" from Node '" + fromNodeVisual.getFullName() +
							"' to Node '" +
							toNodeVisual.getFullName() + "' will be removed.");
						mi.addActionListener(new ActionListener()
						{
							public void actionPerformed(ActionEvent e2)
							{
								try
								{
									try { getLeftPanelView().getModelEngine()
										.deleteCrossReference(false, crId); }
									catch (Exception ex)
									{
										if (JOptionPane.showConfirmDialog(null, ex.getMessage(),
											"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
										{
											getLeftPanelView().getModelEngine()
												.deleteCrossReference(true, crId);
										}
									}
								}
								catch (Exception ex)
								{
									ErrorDialog.showReportableDialog(CenterPanel.this, ex);
								}
							}
						});
						
						popup1.add(mi);
					}
					
					int x = 0, y = 0;
					if (e.getSource() instanceof Component)
					{
						Component c = (Component)(e.getSource());
						x = c.getX(); y = c.getY();
					}
					
					popup1.show(CenterPanel.this, x, y);
				}
			});
			containerPopup.add(mi);
		}
		
		
		public CenterPanel(TreeTableComponent leftPanel, TreeTableComponent rightPanel)
		{
			super(leftPanel, rightPanel);
		}
	}
	
	
	public class BasicTreeTableModel extends DefaultTreeTableModel
	//public class BasicTreeTableModel extends AbstractTreeTableModel
	{
		//private List<String> names;
		
		public BasicTreeTableModel(TreeTableNode root) { super(root); }
		
		public BasicTreeTableModel(TreeTableNode root, List<String> names)
		{
			super(root, names);
			//super(root);
			//this.names = names;
		}
		
		public TreeTableNode[] getPathToRoot(TreeTableNode node)
		{
			List<TreeTableNode> nodes = new Vector<TreeTableNode>();
			for (;;)
			{
				nodes.add(0, node);
				node = node.getParent();
				if (node == null) break;
			}
			
			return nodes.toArray(new TreeTableNode[nodes.size()]);
		}
		
		public void insertNodeInto(MutableTreeTableNode newChild, MutableTreeTableNode parent,
			int index)
		{
			parent.insert(newChild, index);
			
			TreePath path = new TreePath(getPathToRoot(newChild));
			treeTable.expandPath(path);
			
			treeTable.repaint();
		}
		
		void insertNodeBefore(MutableTreeTableNode newChild, MutableTreeTableNode parent,
			MutableTreeTableNode nextNode)
		{
			if (parent == null) throw new RuntimeException("parent is null");
			if (nextNode == null) throw new RuntimeException("nextNode is null");
			int index = getIndexOfChild(parent, nextNode);
			if (index < 0) throw new RuntimeException("nextNode not found");
			insertNodeInto(newChild, parent, index);
			
			TreePath parentPath = new TreePath(getPathToRoot(parent));
			modelSupport.fireChildAdded(parentPath, index, newChild);

			TreePath path = new TreePath(getPathToRoot(newChild));
			treeTable.expandPath(path);
		}
		
		void insertNodeAfter(MutableTreeTableNode newChild, MutableTreeTableNode parent,
			MutableTreeTableNode priorNode)
		{
			if (parent == null) throw new RuntimeException("parent is null");
			if (priorNode == null) throw new RuntimeException("priorNode is null");
			int index = getIndexOfChild(parent, priorNode);
			if (index < 0) throw new RuntimeException("priorNode not found");
			insertNodeInto(newChild, parent, index+1);
			
			TreePath parentPath = new TreePath(getPathToRoot(parent));
			modelSupport.fireTreeStructureChanged(parentPath);
			//modelSupport.fireChildAdded(parentPath, index+1, newChild);

			TreePath path = new TreePath(getPathToRoot(newChild));
			treeTable.expandPath(path);
		}
		
		void removeChild(MutableTreeTableNode child)
		{
			MutableTreeTableNode parent = (MutableTreeTableNode)(child.getParent());
			TreePath parentPath = new TreePath(getPathToRoot(parent));
			int index = getIndexOfChild(parent, child);
			parent.remove(child);
			modelSupport.fireChildRemoved(parentPath, index, child);
		}
		
		public void setColumnIdentifiers(List<?> columnIdentifiers)
		{
			//this.names = columnIdentifiers;
			super.setColumnIdentifiers(columnIdentifiers);
			modelSupport.fireNewRoot();
			//setTableHeader(createDefaultTableHeader());
		}
		
		//public int getColumnCount() { return names.size(); }
		public void setValueAt(Object value, Object node, int column)
		{
			((TreeTableNode)node).setValueAt(value, column);
			//MutableTreeTableNode parent = (MutableTreeTableNode)(child.getParent());
			//TreePath parentPath = new TreePath(getPathToRoot(parent));
			//int index = getIndexOfChild(parent, child);
			//modelSupport.fireChildChanged(parentPath, index, node);
		}
		
		public Object getValueAt(Object node, int column) { return ((TreeTableNode)node).getValueAt(column); }
		public Object getChild(Object parent, int index) { return ((TreeTableNode)parent).getChildAt(index); }
		public int getChildCount(Object parent) { return ((TreeTableNode)parent).getChildCount(); }
		public int getIndexOfChild(Object parent, Object child) { return ((TreeTableNode)parent).getIndex((TreeTableNode)child); }
		public boolean isLeaf(Object node) { return ((TreeTableNode)node).isLeaf(); }
		public boolean isCellEditable(Object node, int column) { return ((TreeTableNode)node).isEditable(column); }
		//public String getColumnName(int column) {return names.get(column); }
	};
	
	
  /* From VisualComponent - the Visual is the Domain */
	
	
	public String getNodeId()
	throws
		NoNodeException
	{
		return getOutermostNodeSer().getDomainId();
	}
	
	
	public String getFullName()
	{
		return getOutermostNodeSer().getDomainName();
	}
	

	public int getSeqNo() { return 0; }
	
	
	public boolean contains(VisualComponent comp)
	throws
		NoNodeException
	{
		return false;  // because a View itself has no child Visuals: only
			// the Visuals _within_ the View have child Visuals.
	}
	
	
	public boolean isOwnedByMotifDef()
	{
		return getOutermostNodeSer().ownedByMotifDef();
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		return false;  // because a Domain cannot belong to an instance of TemplateInstance.
	}
	

	public String[] getChildIds()
	{
		return new String[0];  // because a View itself has no child Visuals: only
			// the Visuals _within_ the View have child Visuals.
	}
	
	
	public void nameChangedByServer(String newName, String newFullName)
	{
		// The name of the Domain has changed.
		
		NodeSer outermostSer = this.getOutermostNodeSer();  // not necessarily the Domain.
		outermostSer.setDomainName(newName);
		
		if (outermostSer instanceof NodeDomainSer)
		{
			outermostSer.setName(newName);
			outermostSer.setFullName(newFullName);
			
			// Update the name in the tab and the name field.
			PanelManager panelManager = this.getPanelManager();
			int id = panelManager.idOfPanel(this);
			if (id < 0) return;
			panelManager.setTitleAt(id, newName);
		}
	}
	
	
	public void nodeDeleted()  // the Domain has been deleted.
	{
		this.getPanelManager().removePanel(this);
	}
	
	
	public void notifyCrossReferenceCreated(String toNodeId, String crossRefId)
	{
		NodeSer ser;
		try { ser = getModelEngine().getNode(crossRefId); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
			return;
		}
		
		if (! (ser instanceof CrossReferenceSer)) throw new RuntimeException(
			"Node with Id " + crossRefId + " is not a CrossReference");
		CrossReferenceSer crSer = (CrossReferenceSer)ser;
		notifyCrossReferenceCreated(crSer);
	}
		
		
	public void notifyCrossReferenceDeleted(String toNodeId, String crossRefId)
	{
		notifyCrossReferenceDeleted(this.getNodeId(), toNodeId, crossRefId);
	}
		
		
	public ClientView getClientView()
	{
		return this;
	}
	
	
	public NodeSer getNodeSer()
	{
		NodeSer ser = getOutermostNodeSer();
		if (ser instanceof NodeDomainSer) return ser;
		try { return getModelEngine().getNode(ser.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this,  ex);
			throw new RuntimeException(ex);
		}
	}
	
	
	public void setNodeSer(NodeSer nodeSer)
	throws
		NoNodeException
	{
		NodeSer ser = getOutermostNodeSer();
		try { if (ser.getNodeId().equals(nodeSer.getNodeId())) setOutermostNodeSer(ser); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
			return;
		}
	}
	
	
	public void update()
	{
		try
		{
			NodeSer ser = getModelEngine().getNode(getNodeId());
			update(ser);
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this,  ex);
			return;
		}
	}
	

	public void update(NodeSer nodeSer)
	{
		setNodeSer(nodeSer);
		try { refreshRedundantState(); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this,  ex);
			return;
		}
		
		buildTreeTable();
	}
	
	
	public void redraw()
	{
		repaint();
	}
	

	public void refresh()
	{
		update();
		redraw();
	}
	
	
	public java.awt.Point getLocation()
	{
		return new java.awt.Point(0, 0);
	}
	
	
	public int getX() { return 0; }
	
	
	public int getY() { return 0; }

	
	public void refreshLocation() {}

	
	public void refreshSize() {}
	
	
	public void setLocation() throws Exception {}
	
	
	public void setSize() throws Exception {}
	
	
	public void refreshRedundantState()
	throws
		Exception
	{
		setName(getOutermostNodeSer().getDomainName());
	}

	
	public boolean getAsIcon() { return false; }  // never display as an icon.
	
	
	public void setAsIcon(boolean yes) {}
	
	
	public boolean useBorderWhenIconified() { return false; }
	
	
	public void populateChildren()
	{
	}
	
	
	public Set<VisualComponent> getChildVisuals()
	{
		Set<VisualComponent> visuals = new HashSet<VisualComponent>();
		return visuals;
	}
	
	
	public Object getParentObject() { return this.getParent(); }
	
	
	public Container getContainer() { return this; }
	
	
	public Component getComponent() { return this; }
	
	
	public void highlight(boolean yes) {}
	
	
	public boolean isHighlighted() { return false; }
	

	public Color getNormalBackgroundColor() { return ScenarioColor; }


	public Color getHighlightedBackgroundColor() { return ScenarioHLColor; }
	
	
	public int getWidth() { return super.getWidth(); }

	
	public int getHeight() { return super.getHeight(); }
	
	
	public void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		writeScenarioDataAsXML(writer, indentation, scenarioVisual);
	}
	
	
	public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		writer.print(StringUtils.tabs[indentation]);
		writer.println("<" + getViewType() + " full_name=\"" + this.getFullName() + "\">");
		writer.println("</" + getViewType() + ">");
	}
}

