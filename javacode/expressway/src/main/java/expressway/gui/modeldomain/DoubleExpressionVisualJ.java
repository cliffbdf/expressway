/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;
import java.util.Set;
import java.io.Serializable;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.NodeIconImageNames;
import expressway.gui.*;
import expressway.ser.*;


public class DoubleExpressionVisualJ extends ActivityVisualJ
{
	private static AttributeValueParser ExpressionValueParser = new AttributeValueParser()
	{
		public Serializable parseValue(String text)
		{
			return text;  // do not parse expression values: they are parsed by the server.
		}
	};
	

	public DoubleExpressionVisualJ(DoubleExpressionSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		setImageIcon(NodeIconImageNames.DoubleExpressionIconImageName);
		setBackground(getNormalBackgroundColor());
		//repaintColor = getNormalColor();
	}
	
	
	public String getDescriptionPageName() { return "Expressions"; }


	void populateAttributes()
	throws
		ParameterError,
		Exception
	{
		NodeSer nodeSer = getNodeSer();
		
		String[] attrNodeIds = ((ModelElementSer)nodeSer).getAttributeNodeIds();
	
		for (String attrNodeId : attrNodeIds)
		{
			Set<VisualComponent> vs = this.getClientView().addNode(attrNodeId);
			
			for (VisualComponent v : vs)
			{
				((AttributeVisualJ)v).setValueParser(ExpressionValueParser);
			}
		}
		
		
		//setVisible(true);
		validate();
		repaint();
	}

	
	//int getNormalBorderThickness() { return 0; }
	

	/*
	void highlightBackground(boolean yes)
	{
		if (yes) this.repaintColor = getHighlightedColor();
		else this.repaintColor = getNormalColor();
		repaint();
	}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
	}
	
	
	private static final java.awt.Stroke normalStroke = new java.awt.BasicStroke((float)2.0);
	*/


	public Color getNormalBackgroundColor() { return DoubleExpressionColor; }
	
	
	public Color getHighlightedBackgroundColor() { return DoubleExpressionHLColor; }
}

