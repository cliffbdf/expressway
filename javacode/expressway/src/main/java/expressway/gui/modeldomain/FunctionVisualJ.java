/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import java.awt.Image;
import expressway.ser.*;
import expressway.gui.*;
import expressway.common.VisualComponent.*;
import expressway.common.*;
import expressway.common.NodeIconImageNames;
import expressway.common.ModelAPITypes.*;
import java.awt.Container;
import java.awt.Color;
import javax.swing.JPopupMenu;


class FunctionVisualJ extends PortedContainerVisualJ implements FunctionVisual
{
	FunctionVisualJ(FunctionSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		setImageIcon(NodeIconImageNames.FunctionIconImageName);
		setBackground(getNormalBackgroundColor());
	}


	public String getDescriptionPageName() { return "Functions"; }


	public void showStart()
	{
		GlobalConsole.println("FunctionVisualJ: " + getName() + " started.");
	}
	
	
	public void showStop()
	{
		GlobalConsole.println("FunctionVisualJ: " + getName() + " stopped.");
	}


	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
	}
	
	
	public Color getNormalBackgroundColor() { return FunctionColor; }
	
	
	public Color getHighlightedBackgroundColor() { return FunctionHLColor; }
	
	
	void addContextMenuItems(JPopupMenu containerPopup, int x, int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
		MenuItemHelper.addMenuOwnerItems(getGraphicView(), this,
			(MenuOwnerSer)(getNodeSer()), containerPopup, x, y);
	}
}

