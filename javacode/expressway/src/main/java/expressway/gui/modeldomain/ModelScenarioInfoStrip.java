/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.modeldomain;

import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.gui.*;
import expressway.ser.*;
import expressway.help.HelpWindow;
import expressway.help.HelpButton;
import expressway.awt.AWTTools;
import expressway.generalpurpose.DateAndTimeUtils;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JOptionPane;


public class ModelScenarioInfoStrip extends ScenarioInfoStripImpl
{
	/** Button for setting the simulation time range. */
	private JButton timeButton;
	
	/** Pops up a SimulatePanel. */
	private JButton simButton;
	
	/** Button for putting the Scenario View into ShowEventsMode. See
		the method setShowEventsMode. */
	private JButton showEventsButton;
	
	private URL timeImgURL;
	private URL simImgURL;
	private URL showEventsImgURL;
	
	/** Display the name of the SimulationRun for which Event history is being
		displayed. */
	private JLabel simRunNameLabel;
	
	private Class thisClass = this.getClass();
	
	
	ModelScenarioInfoStrip(ModelScenarioPanel panel)
	{
		super(panel);
		
		this.timeButton = new JButton(ButtonIcons.TimeButtonIcon);
		this.timeButton.setToolTipText("Simulation Time Range");
		this.add(this.timeButton);
		
		this.simButton = new JButton(ButtonIcons.StartButtonIcon);
		this.simButton.setToolTipText("Simulate...");
		this.add(this.simButton);

		this.showEventsButton = new JButton(ButtonIcons.HistoryButtonIcon);
		this.showEventsButton.setToolTipText("Show Events");
		this.add(this.showEventsButton);
		
		timeImgURL = thisClass.getResource(ButtonIcons.TimeButtonIconImagePath);
		simImgURL = thisClass.getResource(ButtonIcons.StartButtonIconImagePath);
		showEventsImgURL = thisClass.getResource(ButtonIcons.HistoryButtonIconImagePath);
		
		this.timeButton.addActionListener(new SetTimeRangeActionListener());
		this.simButton.addActionListener(new SimActionListener());
		this.showEventsButton.addActionListener(new ShowEventsActionListener(panel.getNodeId()));
	}


	protected String getHelpText()
	{
		return super.getHelpText() +
		"<p><img src=\"" + timeImgURL.toString() + "\" /> &nbsp;<b></b> " +
			"Set the simulation date and/or time at which simulations of this " +
			"Scenario should being and end. Use standard date and time syntax, " +
			"e.g., Jun 10, 2011 10:00:00 PM EDT. " +
			"A blank field leaves a date unspecified. These values may be overridden " +
			"when one starts a simulation." +
			"</p>" +
		"<p><img src=\"" + simImgURL.toString() + "\" /> &nbsp;<b></b> " +
			"Open a control panel for configuring and starting a simulation." +
			"</p>" +
		"<p><img src=\"" + showEventsImgURL.toString() + "\" /> &nbsp;<b></b> " +
			"Display the event history of a simulation. If this is selected, " +
			"you must the right-click on the desired simulation in the " +
			HelpWindow.createHref("Domain List View") +
			" and select 'View Event History'. The selected simulation's history " +
			"is then shown in a popup window, and you can step through it and " +
			"see it visually in the Scenario view." +
			"</p>";
	}
	
	
	/** ********************************************************************
	 * Handle request to view and change the simulation time range for a Scenario.
	 */
	 
	class SetTimeRangeActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			// Display a popup containing a field for the start time and the end time.
			// Populate the fields with the start time and end time of this Scenario.
			// Add an Ok and Cancel button.
			// Add an ActionListener to the Ok Button, to update the start and end
			// times for the Scenario. The server method should send a
			// ModelScenarioSimTimeChangedNotice.
			
			final Frame frame = AWTTools.getContainingFrame(ModelScenarioInfoStrip.this);
			
			JDialog dialog = new JDialog(frame, "Default Simulation Time Parameters", true)
			{
				final JDialog dialog = this;
				JPanel topPanel = new JPanel();
				JPanel bottomPanel = new JPanel();
				
				JTextField fromField;
				JTextField toField;
				
				JTextField iterLimitField;
				JTextField durationField;
				
				{
					topPanel.setLayout(new GridLayout(2, 4));
					
					topPanel.add(new JLabel("From", SwingConstants.RIGHT));
					topPanel.add(fromField = new JTextField());
					topPanel.add(new JLabel("to", SwingConstants.RIGHT));
					topPanel.add(toField = new JTextField());
					
					topPanel.add(new JLabel("Duration (days)", SwingConstants.RIGHT));
					topPanel.add(durationField = new JTextField());
					topPanel.add(new JLabel("Iteration Limit", SwingConstants.RIGHT));
					topPanel.add(iterLimitField = new JTextField());
					
					final ModelScenarioSer scenarioSer = 
						(ModelScenarioSer)(getModelScenarioPanel().getNodeSer());
					final Date currentFromDate = scenarioSer.startingDate;
					final Date currentToDate = scenarioSer.finalDate;
					final long currentDuration = scenarioSer.duration;
					final int currentIterLimit = scenarioSer.iterationLimit;
					
					fromField.setText(currentFromDate == null? "" : currentFromDate.toString());
					toField.setText(currentToDate == null? "" : currentToDate.toString());
					durationField.setText(String.valueOf(currentIterLimit));
					iterLimitField.setText(String.valueOf(currentDuration));
					
					fromField.setColumns(20);
					toField.setColumns(20);
					durationField.setColumns(8);
					iterLimitField.setColumns(8);
					
					JButton okButton;
					JButton cancelButton;
					
					bottomPanel.add(okButton = new JButton("Ok"));
					bottomPanel.add(cancelButton = new JButton("Cancel"));
					
					bottomPanel.add(new HelpButton("Default Simulation Time Parameters",
						HelpButton.Size.Medium,
						"Set the simulation date and/or time at which simulations of this " +
						"Scenario should being and end, and also the duration and " +
						"iteration limit. Use standard date and time syntax. " +
						"A blank field leaves a date unspecified. These values may be overridden " +
						"when one starts a simulation."
						));
					
					okButton.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							DateFormat df = DateAndTimeUtils.DateTimeFormat;
							
							Date newFromDate = currentFromDate;
							String newFromDateStr = fromField.getText().trim();
							if (newFromDateStr.equals(""))
								newFromDate = null;
							else
								try { newFromDate = df.parse(newFromDateStr); }
								catch (ParseException pe)
								{
									ErrorDialog.showReportableDialog(dialog, pe);
									return;
								}
							
							Date newToDate = currentToDate;
							String newToDateStr = toField.getText().trim();
							if (newToDateStr.equals(""))
								newToDate = null;
							else
								try { newToDate = df.parse(newToDateStr); }
								catch (ParseException pe)
								{
									ErrorDialog.showReportableDialog(dialog, pe);
									return;
								}
							
							long newDuration = currentDuration;
							String newDurStr = durationField.getText().trim();
							if (newDurStr.equals(""))
								newDuration = 0;
							else
								try { newDuration = Long.parseLong(newDurStr); }
								catch (NumberFormatException ex)
								{
									ErrorDialog.showReportableDialog(dialog, ex);
									return;
								}
							
							int newIterLimit = currentIterLimit;
							String newIterLimitStr = iterLimitField.getText().trim();
							if (newIterLimitStr.equals(""))
								newIterLimit = 0;
							else
								try { newIterLimit = Integer.parseInt(newIterLimitStr); }
								catch (NumberFormatException ex)
								{
									ErrorDialog.showReportableDialog(dialog, ex);
									return;
								}
							
							
							// Attempt to update server.
							
							try
							{
								try { getModelEngine().setModelScenarioTimeParams(false,
									scenarioSer.getNodeId(), newFromDate, newToDate,
									newDuration, newIterLimit); }
								catch (Warning w)
								{
									if (JOptionPane.showConfirmDialog(null, w.getMessage(),
										"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
									{
										getModelEngine().setModelScenarioTimeParams(true,
											scenarioSer.getNodeId(), newFromDate, newToDate,
											newDuration, newIterLimit);
									}
								}
							}
							catch (Exception ex)
							{
								ErrorDialog.showReportableDialog(dialog, ex);
								return;
							}
							
							dialog.dispose();
						}
					});
					
					cancelButton.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							dialog.dispose();
						}
					});
					
					this.add(topPanel, BorderLayout.NORTH);
					this.add(bottomPanel, BorderLayout.SOUTH);
					
					this.setSize(700, 150);
					this.setLocation(frame.getWidth()/2, 0);
					
					this.show();
				}
			};
		}
	}


	/** ********************************************************************
	 * Handle "Simulate" button selection actions.
	 */
	 
	class SimActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			SimulatePanel simPanel = new SimulatePanel(getNodeView(),
				getModelScenarioPanel(), 
				(VisualComponent.SimulationVisualMonitor)(getModelScenarioPanel()),
				(ModelScenarioSer)(getScenarioPanel().getNodeSer()), 
				(ModelDomainSer)(getNodeView().getOutermostNodeSer()),
				getNodeView().getViewFactory(), getModelEngine());
				
			simPanel.show();
		}
	}
	
	
	/** ********************************************************************
	 * Handle "Show Events" button selection actions.
	 */
	 
	class ShowEventsActionListener implements ActionListener
	{
		private String distOwnerId;
		
		
		ShowEventsActionListener(String distOwnerId)
		{
			this.distOwnerId = distOwnerId;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			java.util.List<SimulationRunSer> simRunSers;
			try { simRunSers =
				getModelEngine().getSimulationRuns(distOwnerId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelScenarioInfoStrip.this, ex);
				return;
			}
			
			if (simRunSers.size() == 0)
			{
				ErrorDialog.showErrorDialog(ModelScenarioInfoStrip.this,
					"There are no Simulation Runs for this Scenario.");
				return;
			}
			
			// Display a list of SimulationRuns for this Scenario.
			
			Component originator = (Component)(e.getSource());
			JPopupMenu menu = new JPopupMenu();
			
			for (final SimulationRunSer srSer : simRunSers)
			{
				JMenuItem mi = new JMenuItem(srSer.getName());
				mi.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent me)
					{
						ModelScenarioInfoStrip.this.getModelScenarioPanel().setShowEventsMode(true);

						// Create an Event history Window.
						try { ModelScenarioInfoStrip.this.getModelScenarioPanel().setEventHistoryFrame(
							ModelScenarioInfoStrip.this.getModelScenarioPanel().createEventHistoryWindow(
								srSer.getNodeId())); }
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(ModelScenarioInfoStrip.this, ex);
							return;
						}
						
						// Display name of sim run in toolbar area.
						simRunNameLabel = new JLabel("Scenario: " + srSer.getName());
						simRunNameLabel.setForeground(Color.white);
						ModelScenarioInfoStrip.this.add(simRunNameLabel);
						ModelScenarioInfoStrip.this.validate();
						ModelScenarioInfoStrip.this.repaint();
					}
				});
				
				menu.add(mi);
			}
			
			JMenuItem mi = new JMenuItem("Revert to showing Attribute values");
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent me)
				{
					// Toggle the mode.
					ModelScenarioInfoStrip.this.getModelScenarioPanel().setShowEventsMode(false);
					
					// Remove scenario name from toolbar area.
					if (simRunNameLabel != null) ModelScenarioInfoStrip.this.remove(simRunNameLabel);
					simRunNameLabel = null;
					ModelScenarioInfoStrip.this.validate();
					ModelScenarioInfoStrip.this.repaint();
					ModelScenarioInfoStrip.this.getModelScenarioPanel().clearEventHistoryFrame();
				}
			});
			
			menu.add(mi);
			menu.show(originator, originator.getWidth(), 0);
		}
	}
	
	
	protected ModelScenarioPanel getModelScenarioPanel()
	{
		return (ModelScenarioPanel)(getScenarioPanel());
	}
}

