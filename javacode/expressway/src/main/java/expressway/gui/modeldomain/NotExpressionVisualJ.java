/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import java.awt.Graphics;
import java.awt.Color;
import expressway.ser.*;
import expressway.gui.*;
import java.awt.Image;
import expressway.common.NodeIconImageNames;


public class NotExpressionVisualJ extends ActivityVisualJ
{
	private Color repaintColor;
	

	public NotExpressionVisualJ(NotExpressionSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		setImageIcon(NodeIconImageNames.NotExpressionIconImageName);
		setBackground(TransparentWhite);
		repaintColor = getNormalColor();
	}
	

	public String getDescriptionPageName() { return "Not Expressions"; }


	//int getNormalBorderThickness() { return 0; }
	

	void highlightBackground(boolean yes)
	{
		if (yes) this.repaintColor = getHighlightedColor();
		else this.repaintColor = getNormalColor();
		repaint();
	}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int width = this.getWidth();
		int height = this.getHeight();
		
		// Draw a "NOT" symbol.
		/*
		int x = this.getWidth()/4;
		int y = this.getHeight()/2;
		
		int symbolWidth = this.getWidth()/2;
		
		int x1 = x + this.getWidth()/2;
		int y1 = y;
		
		int x2 = x1;
		int y2 = y + symbolWidth/2;
		
		if (g instanceof java.awt.Graphics2D)
			((java.awt.Graphics2D)g).setStroke(normalStroke);
	
		g.setColor(repaintColor);		
		g.drawLine(x, y, x1, y1);
		g.drawLine(x1, y1, x2, y2);
		*/
	}
	
	
	private static final java.awt.Stroke normalStroke = new java.awt.BasicStroke((float)2.0);


	public Color getNormalColor() { return NotExpressionColor; }
	
	
	public Color getHighlightedColor() { return NotExpressionHLColor; }
}

