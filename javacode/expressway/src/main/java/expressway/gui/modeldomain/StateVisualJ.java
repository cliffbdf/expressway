/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import java.awt.Image;
import java.util.Date;
import java.util.Set;
import expressway.gui.*;
import expressway.ser.*;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.NodeIconImageNames;
import expressway.common.ModelAPITypes.*;
import expressway.help.*;
import expressway.generalpurpose.ThrowableUtil;
import java.awt.Container;
import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;
import java.awt.FlowLayout;
import java.awt.event.*;
import javax.swing.*;
//import javax.swing.JOptionPane; // needed only for diagnostic.


class StateVisualJ extends VisualJComponent implements StateVisual
{
	StateVisualJ(StateSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		//setImageIcon(NodeIconImageNames.StateIconImageName);
		//setBackground(getNormalBackgroundColor());
	}


	public String getDescriptionPageName() { return "States"; }


	/** Set to zero to minimize the border. The result will be that only the minimum
		border will be used, sufficient ot display the title. */
	//int getNormalBorderThickness() { return 0; }
	

	public void eventScheduled(Serializable futureValue, Date date) {}
	public void valueUpdatedInEpoch(int epochNo, Date date) {}


	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
		
		//this.nameLabel.setText(this.getName());
	}
	
	
	public void populateChildren()
	throws
		ParameterError,
		Exception
	{
		super.populateChildren();
		this.populatePredefinedEvents();
	}
	
	
	/*public void refreshChildren()
	throws
		ParameterError,
		Exception
	{
	}*/
	
	
	protected void populatePredefinedEvents()
	throws
		ParameterError,
		Exception
	{
		StateSer node = (StateSer)(getNodeSer());
		
		String[] preEventNodeIds = node.preEventNodeIds;
		for (String preEventNodeId : preEventNodeIds)
		{
			Set<VisualComponent> visuals = this.getClientView().addNode(preEventNodeId);
		}
		
		validate();
		repaint();
	}
	
	
	public int getSeqNo() { return ((StateSer)(this.getNodeSer())).stateNo; }


	public Color getNormalBackgroundColor() { return StateColor; }
	
	
	public Color getHighlightedBackgroundColor() { return StateHLColor; }
	
	
	public boolean hasAttributeStateBindings()
	{
		return ((StateSer)(this.getNodeSer())).attributeBindingNodeIds.length > 0;
	}
	
	
	void addContextMenuItems(final JPopupMenu containerPopup, int x, int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
		
		if (this.hasAttributeStateBindings())
		{
			final JMenuItem menuItem1 = new HelpfulMenuItem("Show Attributes bound to this State", false,
				"Show the Attributes (in other Model Domains) that are bound to this State. " +
				"See " + HelpWindow.createHref("Binding an Attribute to a State") + ".");
			
			menuItem1.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					JPopupMenu attrPopup = new JPopupMenu();
					
					String[] attrIds = 
						((StateSer)(StateVisualJ.this.getNodeSer())).boundAttrNodeIds;
					
					attrPopup.add(new HelpfulMenuItem(
						"These " + attrIds.length + " Attributes (in other Domains) are bound to State '" +
						StateVisualJ.this.getFullName() + ":", false,
						"Select an Attribute: its Domain will be loaded and the Attribute " +
						"will be highlighted."));
					
					attrPopup.addSeparator();
						
					for (String attrId : attrIds)
					{
						NodeSer nodeSer;
						try { nodeSer = getModelEngine().getNode(attrId); }
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(StateVisualJ.this, ex);
							return;
						}
						
						if (! (nodeSer instanceof AttributeSer))
						{
							ErrorDialog.showReportableDialog(StateVisualJ.this,
								new Exception("Node with id " + attrId + 
									" is not an Attribute: it is a " + nodeSer.getClass().getName()));
							return;
						}
						
						final ModelAttributeSer attrSer = (ModelAttributeSer)nodeSer;
						
						JMenuItem menuItem = new HelpfulMenuItem(
							"'" + attrSer.getFullName() + "' (in Domain '" +
								attrSer.getDomainName() + "')",
							false, "This Attribute is bound to State '" +
							StateVisualJ.this.getFullName() + "'. See " +
							HelpWindow.createHref("Binding an Attribute to a State") + ".");
						
						menuItem.addActionListener(new ActionListener()
						{
							public void actionPerformed(ActionEvent e)
							{
								// Load the Attribute Domain, and highlight the Attribute.
								
								ViewFactory viewFactory = StateVisualJ.this.getClientView().getViewFactory();
								ModelDomainViewPanel domainView;
								try { domainView = (ModelDomainViewPanel)
									(viewFactory.createModelDomainViewPanel(
										attrSer.domainId, false, true)); }
								catch (Exception ex)
								{
									ErrorDialog.showReportableDialog(StateVisualJ.this, ex);
									return;
								}
								
								try { domainView.selectNodesWithId(attrSer.getNodeId(), true); }
								catch (ParameterError pe) {}
							}
						});
						
						attrPopup.add(menuItem);
					}
					
					// Show the Attribute popup next to where menuItem1 was.

					Object evtSrc = e.getSource();
					JMenuItem menuItemSrc = (JMenuItem)evtSrc;
					
					int x = menuItemSrc.getX();
					int y = menuItemSrc.getY();
					int w = menuItemSrc.getWidth();
			
					attrPopup.show(StateVisualJ.this, x + w, y);
				}
			});
			
			containerPopup.add(menuItem1);
		}
	}


	public static final int AttrStateBindingChevronX = 0;
	public static final int AttrStateBindingChevronY = 0;
	public static final int AttrStateBindingChevronWidth = 4;
	public static final int AttrStateBindingChevronHeight = 20;
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		if (hasAttributeStateBindings())  // Draw indicator of the existence of bindings.
		{
			ChevronSymbol.drawChevronSymbol(g, AttrStateBindingChevronX,
				AttrStateBindingChevronY, AttrStateBindingChevronWidth,
				AttrStateBindingChevronHeight);
		}
	}
}

