/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.Constants;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.*;
import expressway.gui.InfoStrip.*;
import expressway.generalpurpose.ThrowableUtil;
import expressway.awt.AWTTools;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Color;
import java.awt.Insets;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.Vector;
import java.util.List;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;



/** ****************************************************************************
 * A common Panel base class for all GUI Model Domain Views.
 * Implements a Graphic View of an Element in a Model Domain.
 */

public abstract class ModelDomainViewPanelBase extends AbstractGraphicViewPanel
	implements
		GraphicDomainViewPanel,
		ModelDomainVisual,
		PanelManager.Single_NodeView_Container,
		SimulatableView
{
	/** The SimulationControlPanels owned by this View. */
	private Map<Integer, Set<SimulationVisual>> simVisuals = 
		new HashMap<Integer, Set<SimulationVisual>>();
	
	
	/** Each Visual in this Set depicts the outermost Node in this View.
		Normally there will only be one element in this Set. */
	//private Set<VisualJComponent> outermostVisuals = new HashSet<VisualJComponent>();
	VisualComponent outermostVisual;
	
	
	/** Font to use for labeling Components. */
	private Font componentTitleFont = null;
	
	
	/** If true, this Visual should be displayed as an icon. Otherwise it should
		be displayed full-size. */
	private boolean asIcon;
	
	
	private int displayAreaHeight = 0;
	
	private VisualComponentFactory visualFactory;
	
	
	/** Indicates whether this View Panel exists in its own popup View Window, or
		is part of the main Window. */
	private boolean isInPopupWindow = false;
	

	
	/** ************************************************************************
     * Constructor.
     */
	 
	public ModelDomainViewPanelBase(PanelManager container, NodeSer nodeSer,
		//String nodeId,
		String name, ViewFactory viewFactory, ModelEngineRMI modelEngine)
	throws
		Exception
	{
		super(container, viewFactory, nodeSer, modelEngine, name);
		
		if (name == null) throw new RuntimeException("name is null");
		if (name.equals("")) throw new RuntimeException("name is blank");
		
		this.visualFactory = new VisualJFactory(this);
	}
	
	
	//protected void setDisplayAreaPriorHeight(int h) { this.displayAreaPriorHeight = h; }
	
	//protected int getDisplayAreaPriorHeight() { return this.displayAreaPriorHeight; }
	
	
	public boolean isPopup() { return this.isInPopupWindow; }
	
	
	public void setIsPopup(boolean yes) { this.isInPopupWindow = yes; }
	
	
	public List<VisualComponent> getOutermostVisuals()
	{
		List<VisualComponent> visuals = new Vector<VisualComponent>();
		if (outermostVisual == null) return visuals;
		visuals.add(outermostVisual);
		return visuals;
	}


	public synchronized void postConstructionSetup()
	throws
		Exception
	{
		if (getOutermostNodeSer() == null) return;
		setNodeSer(getModelEngine().getNode(this.getOutermostNodeId()));
		super.postConstructionSetup();
		
		
		
		// Adjust size and position of Display Area, and install handler that
		// will re-adjust it when this Panel is resized.
		
		this.computeDisplayAreaLocationAndSize(false);
		
		
		// (Re-)instantiate the top level VisualJComponent, and its immediate children.
		
		populateChildren();
		
		
		// Position the outermost Visual at the bottom left of the Display Area.
		
		positionOutermostVisual();  // does not refresh the View.
		
		
		// Refresh the View.
				
		getOutermostVisual().refresh();


		// Add handler for when the Display Area changes size.
		
		this.addComponentListener(new ComponentAdapter()
		{
			public void componentResized(ComponentEvent e)
			{
				int priorHeight = displayAreaHeight;
				ModelDomainViewPanelBase.this.computeDisplayAreaLocationAndSize(
					false /* don't re-position components */ );
				
				int dh = displayAreaHeight - priorHeight;
				setYOriginDynamicOffset(getYOriginDynamicOffset() - dh);
			}
		});
		

		// Subscribe this View, because it is also a Visual.
		
		if (this.getListenerRegistrar() != null)
		{
			this.getListenerRegistrar().subscribe(
				((ModelElementSer)(this.getNodeSer())).domainId, true);
		}
		
		validate();
		repaint();
		// did not cause a repaint.
	}
	
	
	public VisualComponentFactory getVisualFactory() { return visualFactory; }
	
	
	protected void setVisualFactory(VisualComponentFactory factory)
	{
		this.visualFactory = factory;
	}
	
	
	public void saveFocusFieldChanges() {}
	
	
	public synchronized void repaint()
	{
		super.repaint();
	}
	
	
	
  /* From PanelManager nested types. */


	public NodeView getNodeView()
	{
		return this;
	}


	public Set<NodeView> getNodeViews(String nodeId)
	{
		Set<NodeView> nodeViews = new HashSet<NodeView>();
		nodeViews.add(this);
		return nodeViews;
	}


	public Set<View> getViews(String nodeId)
	{
		Set<View> views = new HashSet<View>();
		views.add(this);
		return views;
	}
	
	
	public Set<View> getViews()
	{
		Set<View> views = new HashSet<View>();
		views.add(this);
		return views;
	}


	public int getNoOfViews() { return 1; }




  /* ***************************************************************************
	 * *************************************************************************
	 * Methods defined in SimulatableView interface.
	 */
	 
	 
	public Set<SimulationVisual> getSimulationVisuals(Integer callbackId)
	{
		return simVisuals.get(callbackId);
	}
	
	
	public Set<SimulationVisual> getSimulationVisuals()
	{
		Set<SimulationVisual> simViss = new HashSet<SimulationVisual>();
		Collection<Set<SimulationVisual>> simVisSets = this.simVisuals.values();
		for (Set<SimulationVisual> simVisSet : simVisSets)
			simViss.addAll(simVisSet);
		
		return simViss;
	}


	/**
	 * Replace the mapping for the id with the new Set of SimulationVisuals.
	 */
	 
	protected void setSimVisuals(Integer id, Set<SimulationVisual> simVisualsForId)
	{
		this.simVisuals.put(id, simVisualsForId);
	}
	
	
	/**
	 * Add the specified SimulationVisual to the mapping for the specified
	 * id, or create the mapping if it does not exist.
	 */
	 
	public void addSimVisual(Integer id, SimulationVisual simVisualForId)
	{
		Set<SimulationVisual> simVisualsForId = this.simVisuals.get(id);
		
		if (simVisualsForId == null)
		{
			simVisualsForId = new TreeSetNullDisallowed<SimulationVisual>();
			this.simVisuals.put(id, simVisualsForId);
		}
		
		simVisualsForId.add(simVisualForId);
	}
	
	
	/**
	 * Add the specified SimulationVisuals to the mapping for the specified
	 * id, or create the mapping if it does not exist.
	 */
	 
	public void addSimVisuals(Integer id, Set<SimulationVisual> newSimVisualsForId)
	{
		Set<SimulationVisual> simVisualsForId = this.simVisuals.get(id);
		
		if (simVisualsForId == null)
		{
			simVisualsForId = new TreeSetNullDisallowed<SimulationVisual>();
			this.simVisuals.put(id, simVisualsForId);
		}
		
		simVisualsForId.addAll(newSimVisualsForId);
	}
	
	
	protected void removeSimVisual(Integer id)
	{
		this.simVisuals.remove(id);
	}
	
	
	protected Set<SimulationVisual> getSimVisuals()
	{
		Set<SimulationVisual> visuals = new HashSet<SimulationVisual>();
		Collection<Set<SimulationVisual>> valueSets = this.simVisuals.values();
		for (Set<SimulationVisual> valueSet : valueSets)
			visuals.addAll(valueSet);
		return visuals;
	}



  /* ***************************************************************************
	 * *************************************************************************
	 * Methods defined in View interface.
	 */
	 
	 
	public VisualComponent getOutermostVisual() { return outermostVisual; }
	
	
	public synchronized Class getVisualClass(NodeSer node)
	throws
		CannotBeInstantiated
	{
		return getVisualFactory().getVisualClass(node);
	}
	
	
	public synchronized VisualComponent makeVisual(NodeSer node, Container container,
		boolean asIcon)
	throws
		CannotBeInstantiated,
		ParameterError,
		Exception
	{
		return ((GraphicVisualComponentFactory)(getVisualFactory()))
			.makeGraphicVisual(node, container, asIcon);
	}
	
	
	public synchronized Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError
	{
		Set<VisualComponent> visualComponents = super.identifyVisualComponents(nodeId);
		
		if (this.getNodeSer().getDomainId().equals(nodeId))
		{
			visualComponents.add(this); // this View is also a ModelDomainVisual.
		}
		
		return visualComponents;
	}
	
	
	public synchronized VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError
	{
		Set<VisualComponent> visuals = identifyVisualComponents(nodeId);
		if (visuals == null) return null;
		if (visuals.size() == 0) return null;
		VisualComponent viewVisual = null;
		for (VisualComponent vis : visuals) if (vis instanceof View) viewVisual = vis;
		if (viewVisual != null) visuals.remove(viewVisual);
		if (visuals.size() > 1) throw new RuntimeException(
			"Expected only one Visual with Id " + nodeId + " but there are " + visuals.size());
		VisualComponent visual = null;
		for (VisualComponent v : visuals) visual = v;
		return visual;
	}
	
	
	public synchronized Set<VisualComponent> identifyVisualComponents()
	throws
		ParameterError
	{
		Set<VisualComponent> visualComponents = super.identifyVisualComponents();
		visualComponents.add(this);
		return visualComponents;
	}
	
	
	public synchronized Set<VisualComponent> addNode(String nodeId)
	throws
		Exception
	{
		// Determine if the Node is already represented. If it is a Domain,
		// it should be represented twice in this View: once as this 
		// Panel (which implements ModelDomainVisual) and once as a top-level
		// Visual in this Panel. Otherwise, it should be represented
		// at most once.
		
		Set<VisualComponent> visuals = identifyVisualComponents(nodeId);
		
		if ((this.getOutermostNodeSer() instanceof ModelDomainSer)
			&& nodeId.equals(this.getNodeId()))
		{
			if (visuals.size() > 1) return null;
		}
		else
		{
			if (visuals.size() == 1)
			{
				for (VisualComponent v : visuals)
				{
					if (! (v instanceof View)) return null;
				}
			}
		}
		

		// Retrieve the Node from the server.
		
		NodeSer node = getModelEngine().getNode(nodeId);
		
		if (node == null) throw new RuntimeException("Node is null");
		
		if (! node.isVisible()) return null;
		
		
		// Identify the parent Node.
		
		String parentNodeId = node.getParentNodeId();
		
		boolean asIcon = ! node.getNodeId().equals(getOutermostNodeId());
		

		// Identify the Visuals in this View that depict the parent of the Node.
		
		Set<VisualComponent> parentVisuals = null;
		if (parentNodeId != null) parentVisuals = identifyVisualComponents(parentNodeId);
		
		
		// Need to consider case when the only parent visual is the ViewPanel,
		// and it is the same type of Visual as the one that is to be created. In that
		// case we still need to create a VisualJComponent, which will be the top-level
		// Visual of this View.
		
		boolean creatingTopLevelVisual = false;
		if ((parentVisuals != null) && (parentVisuals.size() == 1))
		{
			VisualComponent firstParentVisual = null;
			for (VisualComponent v : parentVisuals)
			{
				firstParentVisual = v;
				break;
			}
			
			if (firstParentVisual instanceof ModelDomainViewPanelBase)
				creatingTopLevelVisual = true;
		}
		
		
		// In each parent Visual, create a client-side Visual to represent the
		// server-side Node 'node', adding the Visual to this Panel and View.
		
		Set<VisualComponent> newVisuals = new HashSet<VisualComponent>();
		
		if ((parentVisuals == null) || (parentVisuals.size() == 0)
			|| creatingTopLevelVisual)  // No parent Visual.
		{
			if (! nodeId.equals(this.getNodeId())) return null;
				// The Component's Container has not yet been created.
			
			VisualJComponent visual;
			try
			{
				visual = 
				(VisualJComponent)(this.makeVisual(node, (Container)(this.getDisplayArea()),
					asIcon));
			}
			catch (CannotBeInstantiated ex) { return null; }
					
			boolean shouldBeVisible = true;
			
			newVisuals.add(visual);
			visual.refreshRedundantState();
			visual.setVisible(shouldBeVisible);
			
			
			// Populate the new Visual.
			
			if (asIcon)
			{
				if (visual instanceof PortedContainerVisualJ)
					((PortedContainerVisualJ)visual).populatePorts();
			}
			else
			{
				visual.populateChildren();
				this.notifyVisualComponentPopulated(visual, true);
			}
			

			// Register a mouse handler with the Activity.
			// If the Node is a Transformation Activity, the mouse handler
			// shows the "View Evidence" popup.
				
			visual.addMouseListener(this.getPopupListener());
			visual.addMouseListener(this.getMouseInputListener());
			visual.addMouseMotionListener(this.getMouseMotionListener());

			this.outermostVisual = visual;
		}
		else
		{
			for (VisualComponent parentVisual : parentVisuals)
			{
				if (! (parentVisual instanceof Container)) throw new RuntimeException(
					"Visual 'parentVisual' is not an AWT Container.");
				
				if (! (parentVisual instanceof VisualJComponent)) continue; // skip over Panel.
				
				VisualJComponent visual;
				try
				{
					visual = (VisualJComponent)(this.makeVisual(
						node, (Container)parentVisual, asIcon));
				}
				catch (CannotBeInstantiated ex) { continue; }

			
				// Update the parent's NodeSer.
				
				NodeSer newParentNodeSer = getModelEngine().getNode(parentNodeId);
				parentVisual.setNodeSer(newParentNodeSer);
				
				boolean shouldBeVisible = ((Component)parentVisual).isVisible();
				newVisuals.add(visual);
				visual.refreshRedundantState();
				visual.setVisible(shouldBeVisible);

			
				// Populate the new Visual.
				
				if (asIcon)
				{
					if (visual instanceof PortedContainerVisualJ)
						((PortedContainerVisualJ)visual).populatePorts();
				}
				else
				{
					visual.populateChildren();
					this.notifyVisualComponentPopulated(visual, true);
				}
				

				// Register a mouse handler with the Activity.
				// If the Node is a Transformation Activity, the mouse handler
				// shows the "View Evidence" popup.
					
				visual.addMouseListener(this.getPopupListener());
				visual.addMouseListener(this.getMouseInputListener());
				visual.addMouseMotionListener(this.getMouseMotionListener());
			}
		}


		// Subscribe to receive update notifications for the child.
		// Note that this does NOT result in additional server connections.

		if (this.getListenerRegistrar() != null)
			this.getListenerRegistrar().subscribe(nodeId, true);

		
		repaint();
		return newVisuals;
	}
	

	public synchronized void removeVisual(VisualComponent visual)
	throws
		Exception
	{
		/*
		 * Remove the specified Visual from this View.
		 * Unsubscribe it from server events if necessary.
		 * Remove child Visuals owned by this ViewPanel.
		 */
		 
		if (visual instanceof ModelScenarioVisual) return;
		
		
		// Do this recursively for each sub-Component as well

		if (visual instanceof Container)
		{
			Component[] children = ((Container)visual).getComponents();
			for (Component child : children)
			{
				if (child instanceof VisualComponent)
				{
					VisualComponent childVisual = (VisualComponent)child;
					removeVisual(childVisual);
				}
			}
		}


		// Remove from GUI.
		
		if (! (visual instanceof Component)) throw new RuntimeException(
			"Visual is not an AWT Component: is a " + visual.getClass().getName());
		
		VisualJComponent visualJ = (VisualJComponent)visual;
		
		Container parent = visualJ.getParent();
		
		visualJ.setVisible(false);
		if (parent != null)
		{
			parent.remove(visualJ);
			if (outermostVisual == visual) outermostVisual = null;
		}
		
		
		// Unsubscribe.
		
		if (this.getListenerRegistrar() != null)
			this.getListenerRegistrar().subscribe(visual.getNodeId(), false);
		
		
		// Notify alias owners.
		
		this.notifyVisualRemoved(visual);
	}


	/** ************************************************************************
	 * Create a popup View for the specified Node. The popup View should display
	 * the same Scenarios as this View.
	 */
	
	abstract ModelDomainViewPanelBase createPopupModelDomainViewPanel(String title,
		ModelElementSer nodeSer)
	throws
		Exception;
	

	public void notifyVisualComponentPopulated(VisualComponent visual,
		boolean populated)
	{
		// Just a hook: no action here.
	}
	
	
  /* *******************************************************************
	 * *************************************************************************
	 * Methods defined in NodeView.
	 */
	
	
	public DomainInfoStrip constructDomainInfoStrip()
	{
		return new GraphicDomainInfoStripImpl(this);
	}
	
	
  /* *******************************************************************
	 * *************************************************************************
	 * Methods defined in GraphicView.
	 */
	

	public synchronized void computeDisplayAreaLocationAndSize(boolean repositionComponents)
	{
		if (getScenarioSelector() == null)
		{
			super.computeDisplayAreaLocationAndSize(repositionComponents);
			return;
		}
		
		super.computeDisplayAreaLocationAndSize(false);
		
		resizeScenarioSelector();
		
		Insets insets = this.getInsets();
		
		int left = insets.left;
		int top = insets.top;
		int right = insets.right;
		int bottom = insets.bottom;
		
		int w = this.getWidth();
		int h = this.getHeight();
		int sssHeight = getScenarioSelector().getComponent().getHeight();	

		int daW = w - left - right;
		int daH = h - top - bottom - sssHeight;

		VisualComponentDisplayAreaImpl displayArea = 
			(VisualComponentDisplayAreaImpl)(getDisplayArea());

		displayArea.setSize(daW, daH);
		
		displayArea.setLocation(left, top + sssHeight);
		
		
		// Recompute offset, because Display Area size has changed.
		
		setYOriginStaticOffset(daH);
		
		
		// Resposition Visuals: maintain distance from baseline.
		
		if (repositionComponents)
		{
			Component[] components = displayArea.getComponents();
			for (Component c : components)
			{
				if (c instanceof VisualComponent) try
				{
					VisualComponent visual = (VisualComponent)c;
					visual.setLocation();
				}
				catch (Exception ex) { ex.printStackTrace(); }
			}
		}
		
		try
		{
			String id = getOutermostNodeId();
			VisualComponent outermostVisual = identifyVisualComponent(id);
			if (outermostVisual != null) outermostVisual.setLocation();
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
	
	public VisualComponent makeGraphicVisual(NodeSer node, Container container, boolean asIcon)
	throws
		CannotBeInstantiated,
		ParameterError,
		Exception
	{
		return makeVisual(node, container, asIcon);
	}
	
	
	/** Apply recursively. */
	
	public synchronized void notifyVisualMoved(VisualComponent visual)
	{
		if (visual instanceof Component)
		{
			VisualJComponent vj = (VisualJComponent)visual;
			
			Component[] subComps = vj.getComponents();
			for (Component c : subComps)
			{
				if (c instanceof VisualComponent)
				{
					notifyVisualMoved((VisualComponent)c);
				}
			}
		}
	}
	
	
	public synchronized void notifyVisualResized(VisualComponent visual) {}
	
	
	public synchronized void notifyVisualRemoved(VisualComponent visual) {}
	
	
	public synchronized void notifyThatDisplayAreaChanged(int x, int y, int width, int height)
	{
	}

	
	/** ************************************************************************
	 * Shift the View's display offsets so that the specified point is in the middle
	 * of the View's DisplayArea. Then repaint the DisplayArea.
	 */
	
	public void centerOnViewLocation(int xView, int yView)
	throws
		Exception
	{
		// Determine the offsets that will position the point int the center of
		// the Display Area..
		
		VisualComponentDisplayAreaImpl displayArea = 
			(VisualComponentDisplayAreaImpl)(getDisplayArea());
		
		int centerX = displayArea.getWidth()/2;
		int centerY = displayArea.getHeight()/2;
		int deltaX = xView - centerX;
		int deltaY = yView - centerY;
		
		
		// Set the offsets.
		this.setXOriginDynamicOffset(this.getXOriginDynamicOffset() - deltaX);
		this.setYOriginDynamicOffset(this.getYOriginDynamicOffset() - deltaY);
		
		// Redraw.
		
		populateChildren();
	}
	
	
	
  /* *******************************************************************
	 * *************************************************************************
	 * Methods defined in DomainSpaceVisual.
	 */
	

	public void domainCreated(String domainId) {}
	 
	 
	public void motifCreated(String motifId) {}
	
	

  /* *******************************************************************
	 * *************************************************************************
	 * Methods defined in VisualComponent.
	 */
	

	public String getDescriptionPageName() { return "Model Domains"; }


	public String getNodeId() { return getOutermostNodeSer().getNodeId(); }
	
	
	public String getName() { return super.getName(); }
	
	
	public String getFullName() { return getOutermostNodeSer().getFullName(); }
	
	
	public int getSeqNo() { return 0; }
	
		 
	public boolean contains(VisualComponent comp)
	{
		return false;
	}


	public boolean isOwnedByMotifDef()
	{
		return ((ModelElementSer)(getNodeSer())).isOwnedByMotifDef;
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		return getNodeSer().ownedByActualTemplateInstance();
	}
	

	public String[] getChildIds()
	{
		return new String[0];
	}
	
	
	public void nameChangedByServer(String newName, String newFullName)
	{
		NodeSer outermostSer = this.getOutermostNodeSer();
		outermostSer.setName(newName);
		outermostSer.setFullName(newFullName);
		
		if (outermostSer instanceof ModelDomainSer)
			outermostSer.setDomainName(newName);
		
		
		// Update the name in the tab and the name field.
		
		PanelManager panelManager = this.getPanelManager();
		int id = panelManager.idOfPanel(this);
		if (id < 0) return;
		
		panelManager.setTitleAt(id, newName);
	}


	public View getClientView() { return this; }
	
	
	public NodeSer getNodeSer() { return getOutermostNodeSer(); }
	
	
	public void setNodeSer(NodeSer nodeSer)
	{
		initializeNodeSer(nodeSer);
	}
	
	
	public void update()
	{
		try
		{
			NodeSer nodeSer = 
				this.getModelEngine().getNode(this.getNodeSer().getNodeId());
				
			this.update(nodeSer);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this, ThrowableUtil.getAllMessages(ex),
				"Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	

	public void update(NodeSer nodeSer)
	{
		this.setNodeSer(nodeSer);
		
		try
		{
			this.refreshRedundantState();
			this.populateChildren();
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
		}
	}
	
	
	public void redraw() { repaint(); }		
	

	public void refresh()
	{
		this.update();
		this.redraw();
	}
	
	
	public void refreshLocation()
	{
	}

	
	public void refreshSize()
	{
	}
	
	
	public void setLocation()
	throws
		Exception
	{
	}
	
	
	public void setSize()
	throws
		Exception
	{
	}
	
	
	public void refreshRedundantState()
	throws
		Exception
	{
		this.setName(this.getOutermostNodeSer().getName());
	}
	 
	 
	public boolean getAsIcon() { return asIcon; }
	
	
	public void setAsIcon(boolean yes) { this.asIcon = yes; }
	
	
	public boolean useBorderWhenIconified() { return true; }
	

	/** ************************************************************************
	 * Instantiate the outermost Visual that depicts this View's nodeId.
	 * Return a Set of the Visuals that are instantiated (should only be one).
	 */

	public void populateChildren()
	throws
		ParameterError,
		Exception
	{
		VisualComponent ov = getOutermostVisual();
		if (ov != null) this.removeVisual(ov);

		Set<VisualComponent> topLevelVisuals = addNode(getOutermostNodeId());
		
		topLevelVisuals = identifyVisualComponents(this.getOutermostNodeId());
		
		if (topLevelVisuals == null) return;
		
		// Populate the outermost Visual.
		
		for (VisualComponent topLevelVisual : topLevelVisuals)
		{
			if (! (topLevelVisual instanceof VisualJComponent)) continue;
			((VisualJComponent)topLevelVisual).populateChildren();
		}
	}
	
	
	public Set<VisualComponent> getChildVisuals()
	{
		Set<VisualComponent> viss = new HashSet<VisualComponent>();
		viss.add(getOutermostVisual());
		return viss;
	}


	public Object getParentObject() { return this.getParent(); }
	
	
	public Container getContainer() { return this; }
	
	
	public Component getComponent() { return this; }
	
	
	public void nodeDeleted()  // this Model Domain has been deleted.
	{
		this.getPanelManager().removePanel(this);
	}
	
	
	public void notifyCrossReferenceCreated(String toNodeId, String crossRefId)
	{
		NodeSer ser;
		try { ser = getModelEngine().getNode(crossRefId); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
			return;
		}
		
		if (! (ser instanceof CrossReferenceSer)) throw new RuntimeException(
			"Node with Id " + crossRefId + " is not a CrossReference");
		CrossReferenceSer crSer = (CrossReferenceSer)ser;
		notifyCrossReferenceCreated(crSer);
	}
		
		
	public void notifyCrossReferenceDeleted(String toNodeId, String crossRefId)
	{
		notifyCrossReferenceDeleted(this.getNodeId(), toNodeId, crossRefId);
	}
		
		
	public void highlight(boolean yes) {}
	
	
	public boolean isHighlighted() { return false; }
	

	public Color getNormalBackgroundColor() { return VisualJComponent.DomainColor; }
	
	
	public Color getHighlightedBackgroundColor() { return VisualJComponent.DomainHLColor; }
	
	
	public void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		writeScenarioDataAsXML(writer, indentation, scenarioVisual);
	}


  /* *******************************************************************
	 * *************************************************************************
	 * Methods defined in ModelDomainVisual.
	 */
	 
	
	public abstract List<ModelScenarioVisual> getModelScenarioVisuals();
	
	
	public void simulationRunsDeleted()
	{
	}

	
	public void showScenarioSetCreated(String newScenSetId)
	{
	}


	public void attributeStateBindingChanged(String attrId, String stateId)
	{
	}


	public void motifAdded(String motifId)
	{
	}
	
	
	public void motifRemoved(String motifId)
	{
	}
	
	

  /* *******************************************************************
	 * *************************************************************************
	 * Methods defined in ModelContainerVisual.
	 */
	 
	
	public synchronized void resetConduitColors() {}
	public synchronized void incrementConduitColor() {}
	public synchronized Color getConduitColor() { return null; }



  /* *******************************************************************
	 * *************************************************************************
	 * Methods defined in ContainerVisual.
	 */
	
	
	public void componentResized(VisualComponent child)
	throws
		Exception
	{
		child.refresh();
	}
	
	
			
  /* ***************************************************************************
	 ***************************************************************************
	 * Protected methods.
	 */


	/** ************************************************************************
	 * Returns a Mouse Listener for handling right-clicks on a Visual.
	 */
	 
	protected abstract MouseListener getPopupListener();
	
	
	/** ************************************************************************
	 * Return a handler for mouse clicks on a Visual.
	 */
	 
	protected abstract MouseListener getMouseInputListener();
	
	
	/** ************************************************************************
	 * Return a handler for mouse drag events. (Normally the same as the handler
	 * for click events.)
	 */
	 
	protected abstract MouseMotionListener getMouseMotionListener();
}

