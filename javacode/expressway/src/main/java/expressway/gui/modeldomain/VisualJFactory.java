/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import expressway.common.VisualComponent;
import expressway.common.VisualComponent.*;
import expressway.common.ModelEngineRMI;
import expressway.common.InflectionPoint;
import expressway.geometry.Point;
import expressway.geometry.PointImpl;
import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.VisualComponent.*;
import expressway.common.ModelAPITypes.*;
import expressway.gui.*;
import expressway.ser.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Date;
import java.io.IOException;
import java.net.URL;



/** ************************************************************************
 * Factory method for making VisualComponents. If populate is true, then
 * retrieve child component Ids and display them as icons.
 */
public class VisualJFactory implements GraphicVisualComponentFactory
{
	private GraphicView graphicView;
	
	
	VisualJFactory(GraphicView view)
	{
		this.graphicView = view;
	}
	
	
	public Class getVisualClass(NodeSer nodeSer)
	throws
		CannotBeInstantiated
	{
		return VisualJFactory.getVisualClassForNodeSer(nodeSer);
	}
	
	
	public VisualComponent makeTabularVisual(NodeSer nodeSer)
	throws
		CannotBeInstantiated,
		ParameterError,
		Exception
	{
		return (VisualJComponent)(VisualJFactory.makeVisual(nodeSer, this.graphicView,
			null, false));
	}
	
	
	public VisualComponent makeGraphicVisual(NodeSer nodeSer, Container container,
		boolean asIcon)
	throws
		CannotBeInstantiated,
		ParameterError,
		Exception
	{
		return (VisualJComponent)(VisualJFactory.makeVisual(nodeSer, this.graphicView,
			container, asIcon));
	}
	
	
  /* Implementation */
	
	
	private static Class getVisualClassForNodeSer(NodeSer node)
	throws
		CannotBeInstantiated
	{
		// Specialized types.
		if (node instanceof VariablePulseGeneratorSer) return VariablePulseGeneratorVisualJ.class;
		if (node instanceof VariableGeneratorSer) return VariableGeneratorVisualJ.class;
		if (node instanceof PulseGeneratorSer) return PulseGeneratorVisualJ.class;
		if (node instanceof GeneratorSer) return GeneratorVisualJ.class;
		if (node instanceof TerminalSer) return TerminalVisualJ.class;
		if (node instanceof NotExpressionSer) return NotExpressionVisualJ.class;
		if (node instanceof DoubleExpressionSer) return DoubleExpressionVisualJ.class;
		
		// General types.
		if (node instanceof ModelDomainSer) return ModelDomainVisualJ.class;
		//if (node instanceof DecisionDomainSer) return DecisionDomainVisualJ.class;
		//if (node instanceof ModelScenarioSer) return ModelScenarioVisualJ.class;
		//if (node instanceof DecisionScenarioSer) return DecisionScenarioVisualJ.class;
		if (node instanceof ActivitySer) return ActivityVisualJ.class;
		if (node instanceof FunctionSer) return FunctionVisualJ.class;
		if (node instanceof ConduitSer) return ConduitVisualJ.class;
		if (node instanceof PortSer) return PortVisualJ.class;
		if (node instanceof StateSer) return StateVisualJ.class;
		if (node instanceof PredefinedEventSer) return PredefinedEventVisualJ.class;
		if (node instanceof AttributeSer) return AttributeVisualJ.class;
		if (node instanceof AttributeStateBindingSer) return AttributeStateBindingVisualJ.class;
		if (node instanceof VariableSer) return VariableVisualJ.class;
		
		throw new CannotBeInstantiated(node.getClass().getName());
	}
	
	
	private static VisualJComponent makeVisual(NodeSer node, GraphicView view,
		Container container, boolean asIcon)
	throws
		CannotBeInstantiated,
		ParameterError,  // if the component cannot be made due to an erroneous
						// or out-of-range parameter.
		Exception	// any other error.
	{
		/*
		 * Create a specialized instance of this class to Visually represent the
		 * node.
		 */
		
		Class visualClass = view.getVisualClass(node);
		
		VisualJComponent visual = null;
		
		
		// Specialized types first.
		
		if (visualClass.equals(VariablePulseGeneratorVisualJ.class))
			visual = new VariablePulseGeneratorVisualJ((VariablePulseGeneratorSer)node, view);
		
		else if (visualClass.equals(VariableGeneratorVisualJ.class))
			visual = new VariableGeneratorVisualJ((VariableGeneratorSer)node, view);
		
		else if (visualClass.equals(PulseGeneratorVisualJ.class))
			visual = new PulseGeneratorVisualJ((PulseGeneratorSer)node, view);
		
		else if (visualClass.equals(GeneratorVisualJ.class))
			visual = new GeneratorVisualJ((GeneratorSer)node, view);
		
		else if (visualClass.equals(TerminalVisualJ.class))
			visual = new TerminalVisualJ((TerminalSer)node, view);
		
		else if (visualClass.equals(NotExpressionVisualJ.class))
			visual = new NotExpressionVisualJ((NotExpressionSer)node, view);
		
		else if (visualClass.equals(DoubleExpressionVisualJ.class))
			visual = new DoubleExpressionVisualJ((DoubleExpressionSer)node, view);
		
		
		// Now general types.
		
		else if (visualClass.equals(ModelDomainVisualJ.class))
			visual = new ModelDomainVisualJ((ModelDomainSer)node, view);
		
		//else if (visualClass.equals(DecisionDomainVisualJ.class))
		//	visual = new DecisionDomainVisualJ((DecisionDomainSer)node, view);
		
		//else if (visualClass.equals(ModelScenarioVisualJ.class))
		//	visual = new ModelScenarioVisualJ((ModelScenarioSer)node, view);
		
		//else if (visualClass.equals(DecisionScenarioVisualJ.class))
		//	visual = new DecisionScenarioVisualJ((DecisionScenarioSer)node, view);
		
		else if (visualClass.equals(ActivityVisualJ.class))
			visual = new ActivityVisualJ((ActivitySer)node, view);
		
		else if (visualClass.equals(FunctionVisualJ.class))
			visual = new FunctionVisualJ((FunctionSer)node, view);
		
		else if (visualClass.equals(ConduitVisualJ.class))
			visual = new ConduitVisualJ((ConduitSer)node, view);
		
		else if (visualClass.equals(PortVisualJ.class))
			visual = new PortVisualJ((PortSer)node, view);
		
		else if (visualClass.equals(StateVisualJ.class))
			visual = new StateVisualJ((StateSer)node, view);
		
		else if (visualClass.equals(PredefinedEventVisualJ.class))
			visual = new PredefinedEventVisualJ((PredefinedEventSer)node, view);
		
		else if (visualClass.equals(AttributeVisualJ.class))
			visual = new AttributeVisualJ((AttributeSer)node, view);
		
		else if (visualClass.equals(AttributeStateBindingVisualJ.class))
			visual = new AttributeStateBindingVisualJ((AttributeStateBindingSer)node, view);
		
		else if (visualClass.equals(VariableVisualJ.class))
			visual = new VariableVisualJ((VariableSer)node, view);
				
		else throw new RuntimeException(
			"Unrecognized node kind: " + node.getClass().getName());
		
		visual.setView(view);		
		
		if (container != null) container.add(visual);
		visual.refreshRedundantState();
		
		if (container instanceof GraphicView.VisualComponentDisplayArea)
		{
			view.positionOutermostVisual();
		}
		
		visual.setLocation();
		
		
		// Keep Conduits in the foreground.
		
		if (ConduitVisualJ.class.isAssignableFrom(visualClass))
		{
			visual.bringToFront();
		}
		else
		{
			//container.add(visual, 0);
			//visual.updateLocation();
			visual.sendToBack();
		}
			
		//visual.iconify(asIcon);
		visual.setAsIcon(asIcon);
		
		visual.validate();
		
		return visual;
	}
}


