/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import java.awt.*;
import java.awt.event.*;
import java.text.NumberFormat;
import java.text.ParseException;
import javax.swing.*;
import expressway.common.*;
import expressway.gui.*;
import expressway.gui.graph.GraphPanel;
import expressway.help.*;
import expressway.graph.*;
import expressway.generalpurpose.ThrowableUtil;
import expressway.statistics.FitGammaDistribution;
import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.Distribution;
import org.apache.commons.math.distribution.GammaDistribution;
import org.apache.commons.math.distribution.GammaDistributionImpl;


public class DistributionChooserPanel extends JFrame
{
	private PanelManager container;
	private Component component;
	private DistributionChooserCallbackHandler callbackHandler;
	private java.awt.Point location;

	private boolean initialValuesProvided = false;
	
	private double initialAlpha;
	private double initialBeta;
	
	private JPanel radioButtonPanel;
	private JRadioButton determRadioButton;
	private JRadioButton gammaRadioButton;
	private JPanel cardPanel;
	private CardLayout cardLayout;
	private GammaFitPanel gammaPanel;
	private DetermPanel determPanel;
	
	static NumberFormat numberFormat;
	static
	{
		numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(2);
	}
	
	static int graphDisplayWidth = 400;
	static int graphDisplayHeight = 300;
	
	/** The name of the X axis. (Mathematically, the X values are the "domain".) */
	String domainName;
	
	public static final Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
	
	//public static final Color TranlucentWhite = new Color(255, 255, 255, 63);
	
	
	public DistributionChooserPanel(PanelManager container, Component component, 
		DistributionChooserCallbackHandler handler)
	{
		this(container, component, 0.0, 0.0, handler);
	}
	
	
	public DistributionChooserPanel(PanelManager container, Component component, 
		double initialAlpha, double initialBeta,
		DistributionChooserCallbackHandler handler)
	{
		super("Fit Probability Curve for " + component.getName());
		this.setAlwaysOnTop(true);
		this.container = container;
		this.component = component;
		this.callbackHandler = handler;

		//setBackground(TranlucentWhite);
		this.initialAlpha = initialAlpha;
		this.initialBeta = initialBeta;
		this.initialValuesProvided = true;
		
		init();
	}
	
	
	public final String DetermPanelName = "D";
	public final String GammaPanelName = "G";
	
	
	protected void init()
	{	
		this.setSize(700, 450);
		//this.setPreferredSize(new Dimension(400, 300));
		location = new java.awt.Point(0, 0);
		//location = getLocationInFrameContentPane(component);
		location.x += component.getWidth();
		location.y += component.getHeight();
		this.setLocation(location);
		
		domainName = component.getName();
		if (domainName == null) domainName = "Value";
		
		Container mainPanel = getContentPane();
		
		mainPanel.setLayout(new BorderLayout());
		
		mainPanel.add(radioButtonPanel = new JPanel(), BorderLayout.NORTH);
		radioButtonPanel.add(determRadioButton = new JRadioButton("Deterministic"));
		radioButtonPanel.add(gammaRadioButton = new JRadioButton("Gamma"));
		
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(determRadioButton);
		buttonGroup.add(gammaRadioButton);
		
		ActionListener radioButtonActionListener = new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Object source = e.getSource();
				
				if (source == determRadioButton)
				{
					cardLayout.show(cardPanel, DetermPanelName);
				}
				else if (source == gammaRadioButton)
				{
					cardLayout.show(cardPanel, GammaPanelName);
				}
			}
		};
		
		
		mainPanel.add(cardPanel = new JPanel(), BorderLayout.CENTER);
		cardPanel.setLayout(cardLayout = new CardLayout());

		cardPanel.add(gammaPanel = new GammaFitPanel(this, callbackHandler, initialAlpha,
			initialBeta, initialValuesProvided), GammaPanelName);
			
		cardPanel.add(determPanel = new DetermPanel(this, callbackHandler), DetermPanelName);
		
		determRadioButton.addActionListener(radioButtonActionListener);
		gammaRadioButton.addActionListener(radioButtonActionListener);


		if (initialAlpha == 0.0)
		{
			determRadioButton.setSelected(true);
			cardLayout.show(cardPanel, DetermPanelName);
		}
		else
		{
			gammaRadioButton.setSelected(true);
			cardLayout.show(cardPanel, GammaPanelName);
		}
	}
}


interface DistributionChooserCallbackHandler
{
	public void provideDistribution(Distribution dist);
}
	
	
class DetermPanel extends JPanel
{
	private static final Font TitleFont = new Font("Arial", Font.BOLD, 20);
	private static final Color TitleColor = Color.black;
	private static final FontMetrics TitleFontMetrics = 
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(TitleFont);

	private final DistributionChooserPanel chooserPanel;
	private DistributionChooserCallbackHandler callbackHandler;
	private JLabel title;
	private HelpButton titleHelpButton;
	
	private JTextField meanTextField;
	private HelpButton meanTextFieldHelpButton;
	private JLabel meanLabel;
	private JButton okButton;
	private JButton cancelButton;

	private ConstantDistribution dist;


	public DetermPanel(DistributionChooserPanel chooserPanel,
		DistributionChooserCallbackHandler handler)
	{
		this.chooserPanel = chooserPanel;
		this.callbackHandler = handler;
		
		this.setLayout(null);
		
		
		// Add Components.
		
		this.add(title = new JLabel("Choose Constant Value"));
		this.add(titleHelpButton = new HelpButton("Choose Constant Value",
			HelpButton.Size.Medium,
			"Choose a constant value to use, instead of a probability distribution." +
			" That is, the value entered will be used rather than selecting a value at random." +
			" (Statisticians: This is equivalent to using a probability distribution that is" +
			" a delta function at the chosen constant value.)"));
		
		this.add(meanTextField = new JTextField(""));
		this.add(meanTextFieldHelpButton = new HelpButton("Enter Value",
			HelpButton.Size.Medium, 
			"The constant value to be used for the distribution."));
		this.add(meanLabel = new JLabel("Value"));
		
		
		this.add(okButton = new JButton("Ok"));
		this.add(cancelButton = new JButton("Cancel"));
		
		
		// Define sizes.
		
		int titleWidth = TitleFontMetrics.stringWidth(title.getText());
		title.setSize(titleWidth, 60);
		//title.setSize(300, 60);
		title.setFont(TitleFont);

		meanTextField.setColumns(10);
		meanTextField.setSize(70, 25);
		meanTextField.setFont(title.getFont().deriveFont((float)8.0));
		meanLabel.setSize(50, 15);

		okButton.setSize(100, 20);
		cancelButton.setSize(100, 20);
		
		
		// Set locations.
	
		title.setLocation(20, 0);
		titleHelpButton.setLocation(title.getX() + title.getWidth() + 4, title.getY());

		meanTextField.setLocation(120, 210);
		meanTextFieldHelpButton.setLocation(meanTextField.getX() + meanTextField.getWidth() + 4,
			meanTextField.getY());
		meanLabel.setLocation(60, 210);
		
		okButton.setLocation(120, 300);
		cancelButton.setLocation(120, 330);
		
		
		// Add action listeners for Text Fields and Buttons.
		
		okButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				double doubleValue;
				try { doubleValue = Double.parseDouble(meanTextField.getText()); }
				catch (NumberFormatException ex)
				{
					ErrorDialog.showReportableDialog(DetermPanel.this,
						meanTextField.getText() + " is not a valid numeric value",
						"Error", ex); return;
				}
				
				dist = new ConstantDistribution(doubleValue);
				
				callbackHandler.provideDistribution(dist);
				DetermPanel.this.chooserPanel.dispose();
			}
		});
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) { DetermPanel.this.chooserPanel.dispose(); }
		});
	}
	
	
	public static class ConstantDistribution implements Distribution
	{
		private double c;
		
		
		public double getValue() { return c; }
		
		
		public ConstantDistribution(double c)
		{
			this.c = c;
		}
		
		
		public double cumulativeProbability(double x)
		throws MathException
		{
			return (x < c ? 0.0 : 1.0);
		}
		
		
		public double cumulativeProbability(double x0, double x1)
		throws MathException
		{
			if (x0 > x1) throw new MathException(x0 + " is greater than " + x1);
			
			return cumulativeProbability(x1);
		}
	}
}


class GammaFitPanel extends JPanel
{
	private static final Font TitleFont = new Font("Arial", Font.BOLD, 20);
	private static final Color TitleColor = Color.black;
	private static final FontMetrics TitleFontMetrics = 
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(TitleFont);

	private final DistributionChooserPanel chooserPanel;
	private DistributionChooserCallbackHandler callbackHandler;
	private JLabel title;
	private HelpButton titleHelpButton;
	
	private double initialAlpha;
	private double initialBeta;
	boolean initialValuesProvided;
	
	private AdjustableGraph graph;
	private GraphPanel graphPanel;
	private JTextField alphaTextField;
	private HelpButton alphaTextFieldHelpButton;
	private JLabel alphaLabel;
	private JTextField betaTextField;
	private HelpButton betaTextFieldHelpButton;
	private JLabel betaLabel;
	private JTextField minus2sigmaTextField;
	private HelpButton minus2sigmaTextFieldHelpButton;
	private JLabel minus2sigmaLabel;
	private JTextField meanTextField;
	private HelpButton meanTextFieldHelpButton;
	private JLabel meanLabel;
	private JTextField plus2sigmaTextField;
	private HelpButton plus2sigmaTextFieldHelpButton;
	private JLabel plus2sigmaLabel;
	private JButton okButton;
	private JButton cancelButton;
	private MouseDragListener mouseDragListener;
	private TextActionHandler textListener;

	private Cursor priorCursor = null;
	
	private GammaDistributionImpl dist;
	private double range;

	
	public GammaFitPanel(DistributionChooserPanel chooserPanel,
		DistributionChooserCallbackHandler handler,
		double initialAlpha, double initialBeta, boolean initialValuesProvided)
	{
		this.chooserPanel = chooserPanel;
		this.callbackHandler = handler;
		this.initialValuesProvided = initialValuesProvided;
		this.initialAlpha = initialAlpha;
		this.initialBeta = initialBeta;
		
		this.setLayout(null);
		

		// Create a Graph that shows the distribution.
		
		graph = new AdjustableGraph();
		graph.setDrawPoints(false);
		
		
		// Add Components.
		
		this.add(title = new JLabel("Fit Gamma Curve"));
		this.add(titleHelpButton = new HelpButton("Fit Gamma Curve",
			HelpButton.Size.Medium,
			"Choose a <a href=\"http://en.wikipedia.org/wiki/Gamma_distribution\">" +
			"gamma probablity distribution</a> for the events to be generated." +
			" A gamma distribution fits a wide variety of random phenomena." +
			" Choose by dragging the vertical bars until the 'at least'," +
			" 'probably', and 'at most' values are acceptable. These correspond to" +
			" to the values at which the distribution is at the percentiles of" +
			" 2.22, 50, and 97.78 percent, respectively. These correspond to" +
			" the mean minus two standard deviations, the mean, and" +
			" the mean plus two standard deviations."));
		
		this.add(graphPanel = new GraphPanel(null, graph));
		
		this.add(alphaTextField = new JTextField(""));
		this.add(alphaTextFieldHelpButton = new HelpButton("alpha", HelpButton.Size.Small,
			"The computed value of \u03B1 for the " +
			"<a href=\"http://en.wikipedia.org/wiki/Gamma_distribution\">" +
			"gamma distribution</a>. Also referred to as '\u03b3', 'k', or the 'shape'" +
			" because it determines the shape of the distribution curve. See <a href=\"" +
			ReleaseInfo.GammaTutorialURL.toString() + "\">" +
			"Gamma Distributions</a> for more explanation of the " +
			HelpWindow.createHref("Generators", "Generator") +
			" shape and scale parameters in Expressway."));
		this.add(alphaLabel = new JLabel("alpha"));
		
		this.add(betaTextField = new JTextField(""));
		this.add(betaTextFieldHelpButton = new HelpButton("alpha", HelpButton.Size.Small,
			"The computed value of \u03B2 for the " +
			"<a href=\"http://en.wikipedia.org/wiki/Gamma_distribution\">" +
			"gamma distribution</a>. Also referred to as '\u03B8', or the 'scale'" +
			" because it determines how stretched out the distribution curve is" +
			"along the value axis. See <a href=\"" +
			ReleaseInfo.GammaTutorialURL.toString() + "\">" +
			"Gamma Distributions</a> for more explanation of the " +
			HelpWindow.createHref("Generators", "Generator") +
			" shape and scale parameters in Expressway."));
		this.add(betaLabel = new JLabel("beta"));
		
		this.add(minus2sigmaTextField = new JTextField(""));
		this.add(minus2sigmaTextFieldHelpButton = new HelpButton("at least",
			HelpButton.Size.Medium, "The mean minus two standard deviations" +
			": the 2.22th percentile."));
		this.add(minus2sigmaLabel = new JLabel("at least"));
		
		this.add(meanTextField = new JTextField(""));
		this.add(meanTextFieldHelpButton = new HelpButton("probably", HelpButton.Size.Medium,
			"The mean: the expected value."));
		this.add(meanLabel = new JLabel("probably"));
		
		this.add(plus2sigmaTextField = new JTextField(""));
		this.add(plus2sigmaTextFieldHelpButton = new HelpButton("probably", HelpButton.Size.Medium,
			"The mean plus two standard deviations: the 97.78th percentile."));
		this.add(plus2sigmaLabel = new JLabel("at most"));
		
		this.add(okButton = new JButton("Ok"));
		this.add(cancelButton = new JButton("Cancel"));
		
		
		// Define sizes.
		
		title.setSize(TitleFontMetrics.stringWidth(title.getText()), 60);
		//title.setSize(300, 60);
		title.setFont(TitleFont);

		graphPanel.setSize(DistributionChooserPanel.graphDisplayWidth, 
			DistributionChooserPanel.graphDisplayHeight);
		
		alphaTextField.setColumns(10);
		alphaTextField.setFont(title.getFont().deriveFont((float)8.0));		
		alphaTextField.setSize(70, 15);
		alphaLabel.setSize(50, 15);

		betaTextField.setColumns(10);
		betaTextField.setSize(70, 15);
		betaTextField.setFont(title.getFont().deriveFont((float)8.0));
		betaLabel.setSize(50, 15);

		minus2sigmaTextField.setColumns(10);
		minus2sigmaTextField.setSize(100, 25);
		minus2sigmaLabel.setSize(50, 15);
		
		meanTextField.setColumns(10);
		meanTextField.setSize(100, 25);
		meanLabel.setSize(90, 15);
		
		plus2sigmaTextField.setColumns(10);
		plus2sigmaTextField.setSize(100, 25);
		plus2sigmaLabel.setSize(50, 15);
		
		okButton.setSize(100, 20);
		cancelButton.setSize(100, 20);
	
		
		// Specify locations.
		
		title.setLocation(20, 0);
		titleHelpButton.setLocation(title.getX() + title.getWidth() + 4, title.getY());

		graphPanel.setLocation(250, 50);
		//graphPanel.setLayout(null);
		
		alphaTextField.setLocation(150, 100);
		alphaTextFieldHelpButton.setLocation(alphaTextField.getX() + 
			alphaTextField.getWidth() + 4, alphaTextField.getY());
		alphaLabel.setLocation(115, 100);
		
		betaTextField.setLocation(150, 125);
		betaTextFieldHelpButton.setLocation(betaTextField.getX() + 
			betaTextField.getWidth() + 4, betaTextField.getY());
		betaLabel.setLocation(115, 125);

		minus2sigmaTextField.setLocation(120, 180);
		minus2sigmaTextFieldHelpButton.setLocation(minus2sigmaTextField.getX() + 
			minus2sigmaTextField.getWidth() + 4, minus2sigmaTextField.getY());
		minus2sigmaLabel.setLocation(70, 180);
		
		meanTextField.setLocation(120, 210);
		meanTextFieldHelpButton.setLocation(meanTextField.getX() + 
			meanTextField.getWidth() + 4, meanTextField.getY());
		meanLabel.setLocation(60, 210);
		
		plus2sigmaTextField.setLocation(120, 240);
		plus2sigmaTextFieldHelpButton.setLocation(plus2sigmaTextField.getX() + 
			plus2sigmaTextField.getWidth() + 4, plus2sigmaTextField.getY());
		plus2sigmaLabel.setLocation(67, 240);
		
		okButton.setLocation(120, 300);
		cancelButton.setLocation(120, 330);
		//cancelButton.setLocation(240, 300);
		
		
		alphaTextField.setEditable(false);
		betaTextField.setEditable(false);
		
		
		// Compute the graph. This reads the values in some text fields.
		
		double minus2sigma = 1.0;   // typical values.
		double mean = 10.0;
		double plus2sigma = 15.0;
		
		if (initialValuesProvided) try
		{
			GammaDistribution d = new GammaDistributionImpl(initialAlpha, initialBeta);
			
			
			double alpha = d.getAlpha();
			double beta = d.getBeta();
			mean = alpha * beta;
			double sigma = beta * Math.sqrt(alpha);
			double twoSigma = 2.0 * sigma;
			minus2sigma = mean - twoSigma;
			if (minus2sigma <= 0.0) minus2sigma = 0.0;  // use zero.
			
			plus2sigma = mean + twoSigma;
			
			
			
			//minus2sigma = d.inverseCumulativeProbability(0.022);
			//mean = d.inverseCumulativeProbability(0.5);  -- no.
			//plus2sigma = d.inverseCumulativeProbability(0.978);
			
			// The following is only true for a normal distribution, not a gamma:
			// The area between mean and one sd contains 34.1%; the next sd
			// contains 13.6%. The third sd contains 2.1%. See:
			// http://en.wikipedia.org/wiki/Standard_deviation
		}
		catch (Exception ex)
		{
			//JOptionPane.showMessageDialog(GammaFitPanel.this,
			//	"Initial values alpha=" + initialAlpha + ", beta=" + initialBeta +
			//	" cannot be used; substituting typical values",
			//	"Error",
			//	JOptionPane.ERROR_MESSAGE);
			
			minus2sigma = 1.0;
			mean = 10.0;
			plus2sigma = 15.0;
		}
		
		minus2sigmaTextField.setText(DistributionChooserPanel.numberFormat.format(minus2sigma));
		meanTextField.setText(DistributionChooserPanel.numberFormat.format(mean));
		plus2sigmaTextField.setText(DistributionChooserPanel.numberFormat.format(plus2sigma));
		
		setRange();
		formatGraph();
		graphPanel.repaint();
		
		
		// Add behavior to the graph.
	
		graphPanel.addMouseListener(mouseDragListener = new MouseDragListener());
		graphPanel.addMouseMotionListener(mouseDragListener);
		
		
		// Add action listeners for Text Fields and Buttons.
		
		minus2sigmaTextField.addActionListener(
			textListener = new TextActionHandler());
		
		meanTextField.addActionListener(textListener);
		
		plus2sigmaTextField.addActionListener(textListener);
		
		okButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				callbackHandler.provideDistribution(dist);
				GammaFitPanel.this.chooserPanel.dispose();
			}
		});
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) { GammaFitPanel.this.chooserPanel.dispose(); }
		});
	}
	
	
	void formatGraph()
	{
		// Read the 'at leat' value from its TextField.
		
		double minus2sigma;
		try { minus2sigma = getMinus2Sigma(); }
		catch (ParseException ex)
		{
			ErrorDialog.showReportableDialog(GammaFitPanel.this,
				"Value could not be parsed into a number: " 
					+ ThrowableUtil.getAllMessages(ex), "Error", ex); return;
		}
		
		
		// Read the 'probably' value from its TextField.
		
		double mean;
		try { mean = getMean(); }
		catch (ParseException ex)
		{
			ErrorDialog.showReportableDialog(GammaFitPanel.this,
				"Value could not be parsed into a number: " 
					+ ThrowableUtil.getAllMessages(ex), "Error", ex); return;
		}
				
		
		// Fit a gamma distribution to the 'at least' and 'probably' values.
		
		try { dist = FitGammaDistribution.fit(minus2sigma, mean); }
		catch (Exception ex) { ex.printStackTrace(); return; }
				
		
		// Split range into 100 points, and compute dist at each point.
		
		double deltaX = range / 99.0;
		double[] xValues = new double[100];
		double[] yValues = new double[100];
	
		double xValue = 0.0;  // Graph X should start at 0.
		double priorYDistValue = 0.0;
		for (int i = 0;;)
		{
			xValues[i] = xValue;
			
			double cumDist;
			try { cumDist = dist.cumulativeProbability(xValue); }
			catch (org.apache.commons.math.MathException ex)
			{
				ErrorDialog.showReportableDialog(GammaFitPanel.this,
					"Error computing cumulative distribution for " 
						+ Double.toString(xValue), "Error", ex); return;
			}
			
			yValues[i] = (cumDist - priorYDistValue) / deltaX;  // compute density.

			i++;
			if (i < 100) xValue += deltaX;
			else break;
			
			priorYDistValue = cumDist;
		}
		
		
		GraphInput graphInput = new GraphInput();
		graphInput.formatInput(xValues, yValues);
		
		GraphDimensions graphDimensions = new GraphDimensions();
		try { graphDimensions.getOptimalDimensions(graphInput,
			DistributionChooserPanel.graphDisplayWidth, 
			DistributionChooserPanel.graphDisplayHeight); }
		catch (Graph.InvalidGraph ex)
		{
			ErrorDialog.showReportableDialog(GammaFitPanel.this,
				ex.getMessage(),
				"Error",
				ex); return;
		}
		
		//try { graphPanel.computeGraphSize(graph); }
		//catch (Graph.InvalidGraph ex) { ex.printStackTrace(); return; }
		
		graph.setXValues(graphInput.xValues);
		graph.setYValues(graphInput.yValues);
		graph.setXAxisLabel(chooserPanel.domainName);
		graph.setYAxisLabel("p");

		graph.setPixelXSize(graphDimensions.pixelXSize);
		graph.setPixelYSize(graphDimensions.pixelYSize);
		graph.setXStartLogical(graphDimensions.xStartLogical);
		graph.setYStartLogical(graphDimensions.yStartLogical);
		graph.setXEndLogical(graphDimensions.xEndLogical);
		graph.setYEndLogical(graphDimensions.yEndLogical);
		graph.setXDeltaLogical(graphDimensions.xDeltaLogical);
		graph.setYDeltaLogical(graphDimensions.yDeltaLogical);
		
		try { graphPanel.computeGraphSize(graph); }
		catch (Graph.InvalidGraph ex) { ex.printStackTrace(); return; }

		graph.setMinus2sigma(minus2sigma);
		graph.setMean(mean);
		
		
		// Update the alpha and beta text fields.
		
		double alpha = dist.getAlpha();
		double beta = dist.getBeta();

		alphaTextField.setText(DistributionChooserPanel.numberFormat.format(alpha));
		betaTextField.setText(DistributionChooserPanel.numberFormat.format(beta));
	}
	
	
	double getMinus2Sigma()
	throws
		ParseException
	{
		String s = minus2sigmaTextField.getText();
		return (DistributionChooserPanel.numberFormat.parse(s)).doubleValue();
	}
	
	
	double getMean()
	throws
		ParseException
	{
		String s = meanTextField.getText();
		return (DistributionChooserPanel.numberFormat.parse(s)).doubleValue();
	}
	
	
	double getPlus2Sigma()
	throws
		ParseException
	{
		String s = plus2sigmaTextField.getText();
		return (DistributionChooserPanel.numberFormat.parse(s)).doubleValue();
	}
	
	
	void setRange()
	{
		// Read the 'at most' value from its TextField.

		try { this.range = getPlus2Sigma(); }
		catch (ParseException ex)
		{
			ErrorDialog.showReportableDialog(GammaFitPanel.this,
				"Value could not be parsed into a number: " 
					+ ThrowableUtil.getAllMessages(ex), "Error",
				ex); return;
		}
	}
	
	
	class MouseDragListener implements MouseListener, MouseMotionListener
	{
		private ControlLine controlLineSelected = null;
		private java.awt.Point mouseStartingPoint = new java.awt.Point();
		private static final int mouseTolerance = 1;  // +/- one pixel.
		
		
		public void mousePressed(MouseEvent e)
		{
			if (e.getButton() != MouseEvent.BUTTON1) return;
			Component source = e.getComponent();
			if (source != graphPanel) return;

			mouseStartingPoint = e.getPoint();
			
			
			// Determine if the mouse is over one of the control lines.
			
			int x = e.getX();  // loc of e in coord sys of the GraphPanel.
			
			int minus2sigmaX = graph.getMinus2sigmaLine().getXLocationInGraph();
			int meanX = graph.getMeanLine().getXLocationInGraph();
			
			if ( (x >= (minus2sigmaX - mouseTolerance))
				&&
				(x <= (minus2sigmaX + mouseTolerance)) )
				controlLineSelected = graph.getMinus2sigmaLine();
			else if ( (x >= (meanX - mouseTolerance))
				&&
				(x <= (meanX + mouseTolerance)) )
				controlLineSelected = graph.getMeanLine();
			else
				controlLineSelected = null;
		}
		
			
		public void mouseDragged(MouseEvent e)
		{
			if (e.getButton() != MouseEvent.BUTTON1) return;
			Component source = e.getComponent();
			if (source != graphPanel) return;
			if (controlLineSelected == null) return;
			
			int dx = e.getX() - mouseStartingPoint.x;
			int dy = e.getY() - mouseStartingPoint.y;
			
			
			try
			{
				if (controlLineSelected == graph.getMinus2sigmaLine())
					graph.incrementMinus2sigmaInGraph(dx);
				else if (controlLineSelected == graph.getMeanLine())
					graph.incrementMeanInGraph(dx);
			}
			catch (Graph.InvalidGraph ex)
			{
				ErrorDialog.showReportableDialog(GammaFitPanel.this,
					ex.getMessage(),
					"Error",
					ex); return;
			}
						
			mouseStartingPoint = e.getPoint();
			
						
			// Update the graph.
			
			formatGraph();
			graphPanel.repaint();
			
			
			// Update the alpha and beta text fields.
			
			double alpha = dist.getAlpha();
			double beta = dist.getBeta();

			alphaTextField.setText(DistributionChooserPanel.numberFormat.format(alpha));
			betaTextField.setText(DistributionChooserPanel.numberFormat.format(beta));
			
			
			// Compute the value of x such that the cumulative distribution of x
			// is (mean + 2sigma).
			
			try
			{
				double meanPlug2sigma = dist.inverseCumulativeProbability(0.9778);
				plus2sigmaTextField.setText(DistributionChooserPanel.numberFormat.format(meanPlug2sigma));
			}
			catch (Exception ex) { ex.printStackTrace(); }
		}
		
			
		public void mouseReleased(MouseEvent e)
		{
			if (e.getButton() != MouseEvent.BUTTON1) return;
			Component source = e.getComponent();
			if (source != graphPanel) return;
			if (controlLineSelected == null) return;
			
			controlLineSelected = null;

			//graphPanel.setCursor(priorCursor);
		}
		

		public void mouseEntered(MouseEvent e) {}
		
		
		public void mouseExited(MouseEvent e)
		{
			controlLineSelected = null;
			if (priorCursor != null) graphPanel.setCursor(priorCursor);
		}
		
		
		public void mouseClicked(MouseEvent e) {}
		
		
		public void mouseMoved(MouseEvent e)
		{
			Component source = e.getComponent();
			if (source != graphPanel) return;

			
			// Determine if the mouse is over one of the control lines.
			
			int x = e.getX();  // loc of e in coord sys of the GraphPanel.
			
			int minus2sigmaX = graph.getMinus2sigmaLine().getXLocationInGraph();
			int meanX = graph.getMeanLine().getXLocationInGraph();
			boolean overALine = false;
			
			if ( (x >= (minus2sigmaX - mouseTolerance))
				&&
				(x <= (minus2sigmaX + mouseTolerance)) )
				overALine = true;
			else if ( (x >= (meanX - mouseTolerance))
				&&
				(x <= (meanX + mouseTolerance)) )
				overALine = true;
			else
				overALine = false;

			if (overALine)
			{
				if (graphPanel.getCursor() != DistributionChooserPanel.handCursor)
				{
					priorCursor = graphPanel.getCursor();
					graphPanel.setCursor(DistributionChooserPanel.handCursor);
				}
			}
			else
				graphPanel.setCursor(priorCursor);
		}
	}
	
	
	class TextActionHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			formatGraph();
			graphPanel.repaint();
		}
	}
	
	
	class ControlLine
	{
		private int xLocationInGraph = 0;
		

		public int getXLocationInGraph() { return xLocationInGraph; }
		

		/** Be careful to call this once there is a Graphics context and
		 the GraphPanel has been sized. */
		public void setXLocationInGraph(int loc) { this.xLocationInGraph = loc; }
		

		public void incrementLocationInGraph(int dx) { this.xLocationInGraph += dx; }
		
		
		public void draw(Graphics g)
		{
			g.setColor(Color.red);
			int yEndDisplay;
			try { yEndDisplay = graph.getYEndDisplay(); }
			catch (Graph.OverflowException ex)
			{
				ex.printStackTrace();
				return;
			}
			
			g.drawLine(xLocationInGraph, graph.getYStartDisplay(), 
				xLocationInGraph, yEndDisplay);
		}
	}
	
	
	class AdjustableGraph extends SmoothGraph
	{
		private ControlLine minus2sigmaLine = new ControlLine();
		private ControlLine meanLine = new ControlLine();
		
		
		ControlLine getMinus2sigmaLine() { return minus2sigmaLine; }
		
		ControlLine getMeanLine() { return meanLine; }
		
		
		/** Be careful to call this once there is a Graphics context and
		 the GraphPanel has been sized. */
		void setMinus2sigma(double minus2sigma)
		{
			try { minus2sigmaLine.setXLocationInGraph(translateXToDisplay(minus2sigma)); }
			catch (Graph.OverflowException ex)
			{
				ErrorDialog.showReportableDialog(GammaFitPanel.this,
					"Error displaying value " + minus2sigma + " on graph", 
					"Error",
					ex); return;
			}
		}
			
			
		/** Be careful to call this once there is a Graphics context and
		 the GraphPanel has been sized. */
		void setMean(double mean)
		{
			try { meanLine.setXLocationInGraph(translateXToDisplay(mean)); }
			catch (Graph.OverflowException ex)
			{
				ErrorDialog.showReportableDialog(GammaFitPanel.this,
					"Error displaying value " + mean + " on graph", 
					"Error",
					ex); return;
			}
		}
		
		
		public void incrementMinus2sigmaInGraph(int dx)
		throws
			InvalidGraph
		{
			/*
			 * Check if the change results in a valid Gamma distribution.
			 */
			
			int newX = minus2sigmaLine.getXLocationInGraph() + dx;
			double newValue = graph.translateDisplayXToLogical(newX);
			
			double mean;
			try { mean = getMean(); }
			catch (ParseException ex) { throw new InvalidGraph(ex); }
			
			if (newValue >= mean) throw new Graph.InvalidGraph(
				"Mean (" + mean + ") must be greater than mean-2sigma ("
					+ newValue + ")");
			
			this.setMinus2sigma(newValue);
			minus2sigmaTextField.setText(DistributionChooserPanel.numberFormat.format(newValue));
		}
		
		
		public void incrementMeanInGraph(int dx)
		throws
			InvalidGraph
		{
			/*
			 * Check if the change results in a valid Gamma distribution.
			 */
			
			int newX = meanLine.getXLocationInGraph() + dx;
			double newValue = graph.translateDisplayXToLogical(newX);
			
			double minus2sigma;
			try { minus2sigma = getMinus2Sigma(); }
			catch (ParseException ex) { throw new InvalidGraph(ex); }
			
			if (newValue <= minus2sigma) throw new Graph.InvalidGraph(
				"Mean-2sigma (" + minus2sigma + ") must be less than mean ("
					+ newValue + ")");
			
			this.setMean(newValue);
			meanTextField.setText(DistributionChooserPanel.numberFormat.format(newValue));
		}
		
		
		public void draw(Graphics g)
		throws
			InvalidGraph
		{
			super.draw(g);
			
			double newMean;
			double new2sigma;
			try
			{
				newMean = getMean();
				new2sigma = getMinus2Sigma();
			}
			catch (ParseException ex) { throw new InvalidGraph(ex); }
			
			setMean(newMean);
			setMinus2sigma(new2sigma);
			
			minus2sigmaLine.draw(g);
			meanLine.draw(g);
		}
	}
	
	
	/**
	 * Determine the location of the specified Component relative to the content
	 * pane of the Frame that contains the Component.
	 *
	 
	public static java.awt.Point getLocationInFrameContentPane(Component component)
	{
		java.awt.Point point = component.getLocation();
		
		Container container = component.getParent();
		for (;;)
		{
			if (container == null) throw new RuntimeException(
				"Could not identify Frame");
			
			if (container instanceof JFrame) break;
			
			java.awt.Point delta = container.getLocation();
			point.translate(delta.x, delta.y);
			
			container = container.getParent();
		}
		
		//Frame frame = (Frame)container;
		//Container contentPane = frame.getContentPane();
		//java.awt.Point cpLoc = contentPane.getLocation();
		//point.translate(-cpLoc.x, -cpLoc.y);
		
		return 	point;
	}*/
	
	
	/** for testing only. *
	public static void main(String[] args)
	{
		JLabel label = new JLabel("Label");
		//frame.add(label);
		//label.setLocation(20, 20);
		
		DistributionChooserCallbackHandler handler = new DistributionChooserCallbackHandler()
		{
			public void provideDistribution(GammaDistribution dist)
			{
				System.out.println("alpha=" + dist.getAlpha());
				System.out.println("beta=" + dist.getBeta());
			}
		};
			
		GammaFitPanel panel = new GammaFitPanel(label, 2.0, 10.0, handler);
		panel.init();
		panel.setVisible(true);
	}*/
}

