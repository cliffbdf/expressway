/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import java.awt.Graphics;
import java.awt.Color;


public class EventSymbol
{
	public static final int DefaultEventSymbolLength = 24;
	
	
	public static void drawEventSymbol(Graphics g, int x, int y, int len, Color color)
	{
		int x2 = x + len;
		int y2 = y;
		int jag1x = x + len/2;
		int jag1y = y - 2;
		int jag2x = x + len/2 - 2;
		int jag2y = y + 2;
		
		int[] arrowXPoints = { x2, x2-8, x2-5 };
		int[] arrowYPoints = { y2, y2-2, y2+5 };
		
		g.setColor(color);
		g.drawLine(x, y, jag1x, jag1y);
		g.drawLine(jag1x, jag1y, jag2x, jag2y);
		g.drawLine(jag2x, jag2y, x2, y2);
		g.fillPolygon(arrowXPoints, arrowYPoints, 3);
	}
}
	
