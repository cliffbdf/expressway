/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import java.awt.Image;
import expressway.gui.*;
import expressway.ser.*;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.NodeIconImageNames;
import expressway.help.*;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPopupMenu;
import javax.swing.JOptionPane;


public class PortVisualJ extends VisualJComponent implements PortVisual
{
	PortVisualJ(PortSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view, false);
		//setImageIcon(NodeIconImageNames.PortIconImageName);
		//setBackground(getNormalBackgroundColor());
		
		setBorder(null);  // so that title is not displayed.
		
		int portDiameter = view.getDisplayConstants().getPortDiameter();
		this.setSize(portDiameter, portDiameter);
		//this.setToolTipText(this.getName());
	}


	public String getDescriptionPageName() { return "Ports"; }


	void addContextMenuItems(JPopupMenu containerPopup, final int x, final int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
		
		final boolean isBlack = ((PortSer)(this.getNodeSer())).isBlack;
		
		HelpfulMenuItem mi = new HelpfulMenuItem(
			"Make " + (isBlack ? "Reflective" : "Black"), false,
			"This Port is currently " + (isBlack ? "'black'" : "'reflective'") + ". " +
			"Selecting this menu item will change it to be " +
			(isBlack ? "'reflective'" : "'black'") + ". " +
			"A black Port " +
			"does not propagate ('reflect') Events that arrive at it from outside " +
			"its owner, to other Conduits that are attached and are also outside " +
			"the owner; however, a 'reflective' Port does. " +
			"See " + HelpWindow.createHref("Ports", "Ports") + ".");
			
		mi.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					try { getModelEngine().setPortReflectivity(false, getNodeId(), ! isBlack); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(null, w.getMessage(),
							"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
						
							getModelEngine().setPortReflectivity(true, getNodeId(), ! isBlack);
					}
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(PortVisualJ.this, ex);
				}
			}
		});
	}
	
	
	/**
	 * See also PortVisualJ.setLocation() and ConduitVisualJ.setLocation().
	 */

	public void setLocation()
	throws
		Exception
	{
		int radius = getGraphicView().getDisplayConstants().getPortDiameter()/2;


		// Update Component location.
		
		PortedContainerVisualJ parent = (PortedContainerVisualJ)(getParent());
		if (parent == null) return;
		
		double x, y;
		PortSer portSer = (PortSer)(this.getNodeSer());
		
		if (parent.getAsIcon())
		{
			x = portSer.xInIcon;
			y = portSer.yInIcon;
		}
		else
		{
			x = portSer.getX();
			y = portSer.getY();
		}
		
		int newVisualX =
			((GraphicVisualComponent)(this.getParent())).transformNodeXCoordToView(
				x) - radius;
		
		int newVisualY =
			((GraphicVisualComponent)(this.getParent())).transformNodeYCoordToView(
				y) - radius;
		
		this.setLocation(newVisualX, newVisualY);
		
		
		// Notify the View that this Visual has moved, so that it can adjust the
		// position of overlaid elements.
		
		this.getGraphicView().notifyVisualMoved(this);
	}


	public void setSize(int width, int height)
	{
		int diam = getGraphicView().getDisplayConstants().getPortDiameter();
		super.setSize(diam, diam);
	}
	
	
	public void setSize(java.awt.Dimension d)
	{
		int diam = getGraphicView().getDisplayConstants().getPortDiameter();
		this.setSize(diam, diam);
	}
	
	
	public void setBounds(int x, int y, int w, int h)
	{
		int diam = getGraphicView().getDisplayConstants().getPortDiameter();
		super.setBounds(x, y, diam, diam);
	}
	
	
	public Color getNormalBackgroundColor() { return PortColor; }
	
	
	public Color getBlackBackgroundColor() { return PortBlackColor; }
	
	
	public Color getHighlightedBackgroundColor() { return PortHLColor; }
	
	
	void highlightBackground(boolean yes)
	{
		super.highlightBackground(yes);
		
		repaint();
	}
	
	
	protected boolean isBlack() { return ((PortSer)(getNodeSer())).isBlack; }
	
	
	protected synchronized void paintComponent(Graphics g)
	{
		Color parentColor = getParent().getBackground();
		setBackground(parentColor);
		
		super.paintComponent(g);
		
		int w = this.getWidth();
		int h = this.getHeight();
		
		if (isHighlighted())
			g.setColor(getHighlightedBackgroundColor());
		else
			if (isBlack())
				g.setColor(getNormalBackgroundColor());
			else
				g.setColor(getBlackBackgroundColor());
		
		g.fillOval(0, 0, w, h);
		
		PortSer portSer = (PortSer)(this.getNodeSer());
		
		PortDirectionType direction = portSer.direction;
		Position currentSide = portSer.currentSide;
		
		drawArrow(g, direction, currentSide);
	}
	
	
	/**
	 * Draw an arrow, pointing in the direction of the specified side.
	 * The argument 'dir' specifies if the arrow should point outward, inward,
	 * or in both directions (double-headed). The arrow is centered around
	 * this Port's coordinate system (0, 0).
	 */
	 
	protected void drawArrow(Graphics g, PortDirectionType dir, Position side)
	{
		Color color = Color.white;
		
		g.setColor(color);
		
		int hw = this.getWidth()/2;  // half width
		int hh = this.getHeight()/2;  // half height
		
		int length;
		int lineCoordSet;
		int headCoordSet;
		
		boolean drawNormalHead = false;
		boolean drawInvertedHead = false;
		
		switch (side)
		{
			case top:
				length = this.getHeight();
				lineCoordSet = 0;
				headCoordSet = 0;
				drawNormalHead = true;  // initially set as if dir is "out"
				break;
			case bottom:
				length = this.getHeight();
				lineCoordSet = 0;
				headCoordSet = 0;
				drawInvertedHead = true;
				break;
			case left:
				length = this.getWidth();
				lineCoordSet = 1;
				headCoordSet = 1;
				drawNormalHead = true;
				break;
			case right:
				length = this.getWidth();
				lineCoordSet = 1;
				headCoordSet = 1;
				drawInvertedHead = true;
				break;
			default: throw new RuntimeException("Unexpected Position value");
		}
		
		int halfLength = length/2;
		
		int[] a = lineCoords[lineCoordSet][0];
		int[] b = lineCoords[lineCoordSet][1];
		
		g.drawLine(a[0]*halfLength + hw, a[1]*halfLength + hh, 
			b[0]*halfLength + hw, b[1]*halfLength + hh);
		
		switch (dir)
		{
			case bi:
				drawNormalHead = true;
				drawInvertedHead = true;
				break;
			case input:
				drawNormalHead = ! drawNormalHead;
				drawInvertedHead = ! drawInvertedHead;
				break;
			case output:
				break;
			default: throw new RuntimeException("Unexpected PortDirectionType value");
		}
		
		a = headCoords[headCoordSet][0];
		b = headCoords[headCoordSet][1];
		int[] c = headCoords[headCoordSet][2];
		
		if (drawNormalHead)
		{
			g.drawLine(a[0]*halfLength + hw, a[1]*halfLength + hh, 
				b[0]*halfLength + hw, b[1]*halfLength + hh);
			
			g.drawLine(b[0]*halfLength + hw, b[1]*halfLength + hh, 
				c[0]*halfLength + hw, c[1]*halfLength + hh);
		}
		
		if (drawInvertedHead)
		{
			g.drawLine(-a[0]*halfLength + hw, -a[1]*halfLength + hh, 
				-b[0]*halfLength + hw, -b[1]*halfLength + hh);
			
			g.drawLine(-b[0]*halfLength + hw, -b[1]*halfLength + hh, 
				-c[0]*halfLength + hw, -c[1]*halfLength + hh);
		}
	}
	
	private static final int[][][] lineCoords = 
	{
		{
			{ 0, 1}, {0, -1}  // horizontal
		},
		{
			{-1, 0}, {1, 0}  // vertical
		}
	};
	
	private static final int[][][] headCoords =
	{
		{
			{-1, 0}, {0, -1}, {1, 0}  // facing up
		},
		{
			{0, 1}, {-1, 0}, {0, -1}  // facing left
		}
	};
}

