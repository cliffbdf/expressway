/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;
import expressway.common.*;
import expressway.common.NodeIconImageNames;
import expressway.ser.*;
import expressway.gui.*;
import java.awt.image.ImageObserver;


public class VariablePulseGeneratorVisualJ extends GeneratorVisualJ
{
	public static final Image GeneratorIconImage = getImage(NodeIconImageNames.GeneratorIconImageName);
	public static final Image NormalDistImage = getImage(NodeIconImageNames.NormalDistImageName);
	
	
	public VariablePulseGeneratorVisualJ(VariablePulseGeneratorSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
	}
}

