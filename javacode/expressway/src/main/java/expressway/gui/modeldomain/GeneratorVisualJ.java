/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;
import java.awt.Component;
import java.io.IOException;
import java.io.Serializable;
import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.common.NodeIconImageNames;
import expressway.ser.*;
import expressway.gui.*;
import expressway.help.*;
import java.awt.image.ImageObserver;
import java.awt.event.*;
import javax.swing.*;
import expressway.awt.AWTTools;
import expressway.generalpurpose.ThrowableUtil;
import org.apache.commons.math.distribution.Distribution;
import org.apache.commons.math.distribution.GammaDistribution;


public class GeneratorVisualJ extends ActivityVisualJ
{
	public static final Image NormalDistImage = getImage(NodeIconImageNames.NormalDistImageName);

	private Color chevronColor;


	public GeneratorVisualJ(GeneratorSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		//setImageIcon(GeneratorIconImage);
		setBackground(TransparentWhite);
		chevronColor = getNormalBackgroundColor();
		//setBackground(GeneratorColor);
	}


	public String getDescriptionPageName() { return "Generators"; }


	//int getNormalBorderThickness() { return 0; }


	void highlightBackground(boolean yes)
	{
		if (yes) this.chevronColor = getHighlightedBackgroundColor();
		else this.chevronColor = getNormalBackgroundColor();
		repaint();
	}


	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		int width;
		int height;

		if (getAsIcon())
		{
			super.paintComponent(g);
		}
		else
		{
			width = this.getWidth();
			height = this.getHeight();

			// Draw a chevron in the background.

			int[] xPoly = { 0, width - height/2, width, width - height/2, 0, 0 };
			int[] yPoly = { height, height, height/2, 0, 0, height };

			g.setColor(chevronColor);
			g.fillPolygon(xPoly, yPoly, 6);
		}

		AbstractGeneratorSer ser = (AbstractGeneratorSer)(this.getNodeSer());

		//if (g instanceof java.awt.Graphics2D)
		//	((java.awt.Graphics2D)g).setStroke(normalStroke);


		if (ser instanceof PulseGeneratorSer) drawEventSymbol(g);

		if (! (ser.ignoreStartup)) drawStartSymbol(g);

		if (ser.repeating) drawRepeatSymbol(g);


		/*
		int size = Math.min(width, height);  // this will be the bounds on the
			// width and height of the curve that we will draw.

		size = size/2;  // Make it half the size of the Component.

		// Center the curve.
		int xCenter = height/2;
		int yCenter = width/2;

		int x = xCenter - size/2;
		int y = yCenter - size/2;

		g.drawImage(NormalDistImage, x, y, size, size, new ImageObserver()
		{
			public boolean imageUpdate(Image img, int infoflags, int x, int y,
				int width, int height)
			{
				return (infoflags & ImageObserver.ALLBITS) != ImageObserver.ALLBITS;
			}
		});
		*/
	}


	private static final java.awt.Stroke normalStroke = new java.awt.BasicStroke((float)2.0);


	protected void drawEventSymbol(Graphics g)
	{
		int len = EventSymbol.DefaultEventSymbolLength;
		int x = this.getWidth() -
			this.getGraphicView().getDisplayConstants().getPortDiameter() - len;
		int y = this.getHeight() / 2;

		Color color = Color.white;

		EventSymbol.drawEventSymbol(g, x, y, len, color);
	}


	protected void drawStartSymbol(Graphics g)
	{
		final int CircleDiam = 2;
		int portDiameter = this.getGraphicView().getDisplayConstants().getPortDiameter();

		final int x = 4;
		final int y = 4;

		int len = (this.getHeight() - portDiameter - y) / 2;
			// length of line part of arrow.

		int x2 = x;
		int y2 = y + len;
		int[] arrowXPoints = { 4, 1, 7 };
		int[] arrowYPoints = { y2, y2-5, y2-5 };

		Color color;
		if (getAsIcon()) color = Color.black;
		else color = Color.white;

		g.setColor(color);
		g.fillOval(x-3, y-3, 6, 6);
		g.drawLine(x, y, x2, y2);
		g.fillPolygon(arrowXPoints, arrowYPoints, 3);
	}


	protected void drawRepeatSymbol(Graphics g)
	{
		int radius = 7;
		int w = radius * 2;
		int h = w;
		int cx = radius + 2;
		int cy = this.getHeight() - h/2 - 2;
		int startDegrees = 45;
		int deltaDegrees = -270;

		int hypot = (radius * 7) / 10;  // approx. div by sqrt(2)

		int x = cx - radius;
		int y = cy - radius;
		int x2 = cx - hypot;
		int y2 = cy - hypot;

		int[] arrowXPoints = { x2, x2-4, x2 };
		int[] arrowYPoints = { y2, y2, y2+4 };

		Color color;
		if (getAsIcon()) color = Color.black;
		else color = Color.white;

		g.setColor(color);
		g.drawArc(x, y, w, h, startDegrees, deltaDegrees);
		g.fillPolygon(arrowXPoints, arrowYPoints, 3);
	}


	void addScenarioContextMenuItems(JPopupMenu containerPopup, String scenarioNodeId)
	{
		if (scenarioNodeId == null) return;

		GeneratorSer genSer = (GeneratorSer)(this.getNodeSer());

		ChooseDistributionHandler chooseTimeDistributionHandler =
			new ChooseDistributionHandler(genSer.timeShapeAttrNodeId,
				genSer.timeScaleAttrNodeId, scenarioNodeId);

		ChooseDistributionHandler chooseValueDistributionHandler =
			new ChooseDistributionHandler(genSer.valueShapeAttrNodeId,
				genSer.valueScaleAttrNodeId, scenarioNodeId);


		JMenuItem menuItem = new HelpfulMenuItem("Choose Time Distribution...",
			false, "Opens a panel in which you can interactively select" +
			" a probability distribution for the time between the events" +
			" that are generated by the " +
			HelpWindow.createHref("Generators", "Generator") + ".");

		menuItem.addActionListener(chooseTimeDistributionHandler);
		containerPopup.add(menuItem);

		menuItem = new HelpfulMenuItem("Choose Value Distribution...", false,
			"Opens a panel in which you can interactively select" +
			" a probability distribution for the value of each event" +
			" that is genrated by the " +
			HelpWindow.createHref("Generators", "Generator") + ".");

		menuItem.addActionListener(chooseValueDistributionHandler);
		containerPopup.add(menuItem);
	}

	/*
	class PopupListener extends MouseInputAdapter
	{
		public void mouseReleased(MouseEvent e)
		{
			Component source = e.getComponent();

			if (! (source instanceof GeneratorVisualJ)) return;
			GeneratorVisualJ generatorVisual = (GeneratorVisualJ)source;

			int onmask =
				java.awt.event.InputEvent.CTRL_DOWN_MASK |
				java.awt.event.InputEvent.BUTTON1_DOWN_MASK;

			if ((e.getButton() == MouseEvent.BUTTON2) ||
				(e.getButton() == MouseEvent.BUTTON3) ||
				((e.getButton() == MouseEvent.BUTTON1) && e.isControlDown())
				)
			try
			{
				// Create a popup menu to contain the menu item.

				JPopupMenu containerPopup = new JPopupMenu();

				addContextMenuItems(containerPopup, generatorVisual);

				containerPopup.show(generatorVisual, e.getX(), e.getY());
			}
			catch (Exception ex)
			{
				generatorVisual.setInconsistent(true);
				ex.printStackTrace();

				JOptionPane.showMessageDialog(GeneratorVisualJ.this,
					ThrowableUtil.getAllMessages(ex), "Error",
					JOptionPane.ERROR_MESSAGE); return;
			}
		}
	}*/


	class ChooseDistributionHandler implements ActionListener
	{
		private String shapeAttrNodeId;
		private String scaleAttrNodeId;
		private String scenarioNodeId;


		ChooseDistributionHandler(String shapeAttrNodeId, String scaleAttrNodeId,
			String scenarioNodeId)
		{
			this.shapeAttrNodeId = shapeAttrNodeId;
			this.scaleAttrNodeId = scaleAttrNodeId;
			this.scenarioNodeId = scenarioNodeId;
		}


		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			GeneratorVisualJ generatorVisual = (GeneratorVisualJ)originator;

			double initialAlpha;
			double initialBeta;

			final ModelEngineRMI me = getClientView().getModelEngine();
			GeneratorSer genSer = (GeneratorSer)(getNodeSer());

			try
			{
				Serializable o = me.getAttributeValueById(shapeAttrNodeId,
					scenarioNodeId);

				if (! (o instanceof Number))
				{
					JOptionPane.showMessageDialog(GeneratorVisualJ.this,
					"Value of shape Attribute is not numeric", "Error",
					JOptionPane.ERROR_MESSAGE); return;
				}

				initialAlpha = ((Number)o).doubleValue();

				o = me.getAttributeValueById(scaleAttrNodeId,
					scenarioNodeId);

				if (! (o instanceof Number))
				{
					JOptionPane.showMessageDialog(GeneratorVisualJ.this,
						"Value of scale Attribute is not numeric", "Error",
						JOptionPane.ERROR_MESSAGE); return;
				}

				initialBeta = ((Number)o).doubleValue();
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(GeneratorVisualJ.this, ex); return;
			}

			DistributionChooserPanel distributionChooserPanel = new DistributionChooserPanel(
				getClientView().getPanelManager(), GeneratorVisualJ.this,
				initialAlpha, initialBeta,

				new DistributionChooserCallbackHandler()
				{
					public void provideDistribution(Distribution dist)
					{
						double alpha = 0.0;
						double beta = 0.0;


						if (dist instanceof DetermPanel.ConstantDistribution)
						{
							beta = ((DetermPanel.ConstantDistribution)dist).getValue();
						}
						else if (dist instanceof GammaDistribution)
						{
							alpha = ((GammaDistribution)dist).getAlpha();
							beta = ((GammaDistribution)dist).getBeta();
						}


						// Set the Attributes pertaining to the time distribution.

						try
						{
							try { me.setAttributeValue(false, shapeAttrNodeId,
								scenarioNodeId, alpha); }
							catch (Warning w)
							{
								if (JOptionPane.showConfirmDialog(null, w.getMessage(),
									"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
									try
									{
										me.setAttributeValue(true, shapeAttrNodeId,
											scenarioNodeId, alpha);
									}
									catch (CannotObtainLock col)
									{
										ErrorDialog.showReportableDialog(GeneratorVisualJ.this,
											"Cannot set value of shape Attribute ", "Error", col);
									}
							}
							catch (CannotObtainLock col)
							{
								ErrorDialog.showReportableDialog(GeneratorVisualJ.this,
									"Cannot set value of shape Attribute ", "Error", col);
							}

							try { me.setAttributeValue(false, scaleAttrNodeId,
								scenarioNodeId, beta); }
							catch (Warning w)
							{
								if (JOptionPane.showConfirmDialog(null, w.getMessage(),
									"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)

									try
									{
										me.setAttributeValue(true, scaleAttrNodeId,
											scenarioNodeId, beta);
									}
									catch (CannotObtainLock col)
									{
										ErrorDialog.showReportableDialog(GeneratorVisualJ.this,
											"Cannot set value of scale Attribute ", "Error", col);
									}
							}
							catch (CannotObtainLock col)
							{
								ErrorDialog.showReportableDialog(GeneratorVisualJ.this,
									"Cannot set value of scale Attribute ", "Error", col);
							}
						}
						catch (Warning w) { throw new RuntimeException("Should not happen"); }
						catch (ParameterError pe)
						{
							ErrorDialog.showReportableDialog(GeneratorVisualJ.this,
								"Value of shape Attribute cannot be set", "Error",
								pe); return;
						}
						catch (IOException ioe)
						{
							ErrorDialog.showReportableDialog(GeneratorVisualJ.this, ioe); return;
						}
					}
				}
				);

			//gammaFitPanel.init();
			distributionChooserPanel.setVisible(true);
		}
	}


	public Color getNormalBackgroundColor() { return GeneratorColor; }


	public Color getHighlightedBackgroundColor() { return GeneratorHLColor; }
}
