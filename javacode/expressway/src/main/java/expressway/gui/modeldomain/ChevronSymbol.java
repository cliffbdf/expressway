/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import java.awt.Graphics;
import java.awt.Color;


public class ChevronSymbol
{
	public static void drawChevronSymbol(Graphics g, int x, int y, int width, int height)
	{
		int[] arrowXPoints = { x, width, x };
		int[] arrowYPoints = { y, y + height/2, y + height };
		
		g.setColor(Color.white);
		g.fillPolygon(arrowXPoints, arrowYPoints, 3);
	}
}

