/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import java.awt.Image;
import expressway.ser.*;
import expressway.gui.*;
import expressway.common.NodeIconImageNames;
import expressway.common.VisualComponent.*;
import expressway.common.ModelAPITypes.*;
import java.awt.Container;
import javax.swing.JLabel;
import java.awt.Color;


class ConstraintVisualJ extends VisualJComponent implements ConstraintVisual
{
	ConstraintVisualJ(ConstraintSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		//setImageIcon(NodeIconImageNames.ConstraintIconImageName);
		//setBackground(getNormalBackgroundColor());
		
		this.setSize(1, 1);
	}


	public String getDescriptionPageName() { return "Constraints"; }


	//int getNormalBorderThickness() { return 0; }
	

	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
	}
	
	
	public Color getNormalBackgroundColor() { return ConstraintColor; }
	
	
	public Color getHighlightedBackgroundColor() { return ConstraintHLColor; }
}

