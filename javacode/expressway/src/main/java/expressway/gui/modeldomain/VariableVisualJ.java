/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import java.awt.Image;
import expressway.ser.*;
import expressway.gui.*;
import expressway.common.VisualComponent.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.NodeIconImageNames;
import java.awt.Container;
import java.awt.Color;


class VariableVisualJ extends VisualJComponent implements VariableVisual
{
	VariableVisualJ(VariableSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		//setImageIcon(NodeIconImageNames.VariableIconImageName);
		//setBackground(getNormalBackgroundColor());
	}


	public String getDescriptionPageName() { return "Variables"; }
	
	
	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
	}
	
	
	public Color getNormalBackgroundColor() { return VariableColor; }
	
	
	public Color getHighlightedBackgroundColor() { return VariableHLColor; }
}

