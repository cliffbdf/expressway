/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import expressway.geometry.Point;
import expressway.geometry.PointImpl;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.ClientModel.*;
import expressway.common.VisualComponent.*;
import expressway.common.ModelAPITypes.*;
import expressway.help.*;
import expressway.ser.*;
import expressway.gui.View;
import expressway.gui.GraphicView;
import expressway.gui.GraphicVisualComponent;
import expressway.gui.VisualHelper;
import expressway.gui.AbstractGraphicViewPanel;
import expressway.generalpurpose.ThrowableUtil;
import expressway.generalpurpose.StringUtils;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.Border;
import java.awt.Insets;
import java.awt.Color;
import java.awt.Stroke;
import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.awt.event.*;
import java.awt.datatransfer.*;
import java.awt.AWTEvent;
import java.util.Set;
import java.util.List;
import java.util.Vector;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.util.Date;
import java.io.IOException;
import java.io.Serializable;
import java.io.PrintWriter;
import java.net.URL;
import java.net.MalformedURLException;


/** ****************************************************************************
 * Base class for all VisualComponents for Model Views.
 * VisualJComponent is also a factory for VisualComponents.
 */

public abstract class VisualJComponent extends JPanel implements GraphicVisualComponent,
	ModelElementVisual
{
  /* ***************************************************************************
	 ***************************************************************************
	 * Colors used by this class.
	 */
	 
	public static final Color Transparent = new Color(255, 0, 0, 0);
	public static final Color TransparentWhite = new Color(255, 255, 255, 0);
	
	public static final Color DomainColor = new Color(255, 255, 255);
	public static final Color DomainHLColor = new Color(255, 200, 200);

	public static final Color TerminalColor = new Color(50, 50, 255);
	public static final Color TerminalHLColor = new Color(200, 200, 255);

	public static final Color NotExpressionColor = new Color(0, 0, 0);
	public static final Color NotExpressionHLColor = new Color(150, 150, 150);

	public static final Color DoubleExpressionColor = new Color(150, 150, 150);
	public static final Color DoubleExpressionHLColor = new Color(200, 200, 200);

	public static final Color ActivityColor = new Color(200, 200, 255);
	public static final Color ActivityHLColor = new Color(255, 200, 255);

	public static final Color FunctionColor = new Color(255, 200, 200);
	public static final Color FunctionHLColor = new Color(255, 150, 150);

	public static final Color ConduitColor = new Color(255, 100, 155);
	public static final Color ConduitHLColor = new Color(255, 50, 100);

	public static final Color PortColor = new Color(0, 255, 255);
	public static final Color PortBlackColor = new Color(0, 0, 0);
	public static final Color PortHLColor = new Color(255, 0, 0);

	public static final Color StateColor = new Color(0, 255, 5);
	public static final Color StateHLColor = new Color(255, 255, 5);

	public static final Color PredefinedEventColor = new Color(200, 150, 150);
	public static final Color PredefinedEventHLColor = new Color(255, 200, 200);

	public static final Color VariableColor = new Color(255, 200, 255);
	public static final Color VariableHLColor = new Color(255, 150, 200);

	public static final Color AttributeColor = new Color(200, 194, 58);
	public static final Color AttributeHLColor = new Color(255, 194, 58);

	public static final Color AttributeStateBindingColor = new Color(100, 194, 58);
	public static final Color AttributeStateBindingHLColor = new Color(155, 194, 58);

	public static final Color ContainerColor = new Color(100, 100, 100);
	public static final Color ContainerHLColor = new Color(150, 100, 100);

	public static final Color ConstraintColor = new Color(70, 70, 100);
	public static final Color ConstraintHLColor = new Color(120, 70, 100);

	public static final Color GeneratorColor = new Color(228, 100, 100);
	public static final Color GeneratorHLColor = new Color(255, 130, 130);
	
	public static final Color normalBorderColor = new Color(0, 0, 128);
	public static final Color highlightedBorderColor = new Color(255, 100, 0);

	
  /* ***************************************************************************
	 ***************************************************************************
	 * Display constants.
	 */
	
	/** Image displayed as a Visual's icon, unless over-ridden by a specialized
		Visual type. The image size must match the size of the IconWidth and
		IconHeight constants. */
	public static final Image DefaultIconImage = getImage(NodeIconImageNames.DefaultImageName);


	/** The width and height of the tab on the lower right corner area for resizing
  		a Visual. */
	public static final int ResizingTabDiam = 20;
	

  /* ***************************************************************************
	 ***************************************************************************
	 * Instance state.
	 */

	/** The View that owns this Visual. */
	private GraphicView view;
	
	/** Contains the data that identifies and describes the Visual as a Node
	 on the server. */
	private NodeSer nodeSer;
	
	/** If true, this Visual has been populated with its child Visuals, by
	 requesting the corresponding child NodeSers from the server. */
	private boolean populated = true;
	
	/** If true, this Visual should be displayed as an icon, with no internal structure. */
	private boolean asIcon = true;
	
	/** If true, the Visual has been selected for highlighting. */
	boolean highlighted = false;
	
	/** Inner border (inside the title region) for when this Visual is displayed
	 full size (not an icon) and not highlighted. */
	private Border outerNormalBorder = null;
	
	/** Inner border (inside the title region) for when this Visual is displayed
	 full size (not an icon) and highlighted. */
	private Border outerNormalBorderHighlighted = null;
	
	/** Inner border (inside the title region) for when this Visual is displayed
	 as an icon. */
	private Border outerIconBorder = null;

	/** Inner border (inside the title region) for when this Visual is displayed
	 as an icon and highlighted. */
	private Border outerIconBorderHighlighted = null;

	/** Border used when this Visual is full-size (i.e., not an icon). */
	private TitledBorder normalBorder;
	
	/** Border used when this Visual is displayed as an icon. */
	private TitledBorder iconBorder;
	
	/** Image used to depict the icon. */
	private ImageIcon imageIcon;
	
	/** Size of this Visual when displayed full-size (i.e., when not an icon).
	 This size may be out of sync with the embedded NodeSer's size. The intended
	 protocol is that the user can change a Visual's size, and assume that when
	 this change is propagated to the server, the server will accept it; however,
	 if the server does not accept it, the server pushes a new resize operation
	 to the client to set the size back to its prior (or some other) value. */
	private Dimension visualNormalSize;

	/** Indicates whether this Visual has been determined (by the server) to
	 be inconsistent (with respect to the server). */
	private boolean inconsistent;
	
	/** Indicates whether this Visual contains state that has not been
	 propagated to the server. */
	private boolean unpropagated;

	
  /* ***************************************************************************
	 ***************************************************************************
	 * Static methods.
	 */

	/** ************************************************************************
	 * Utility method for retrieving an image specified by a resource name.
	 */
	static Image getImage(String imageResourceName)
	{
		URL resource = VisualJComponent.class.getResource(imageResourceName);
		if (resource == null) throw new RuntimeException("Resource for " +
			imageResourceName + " is null.");
		
		return Toolkit.getDefaultToolkit().getImage(resource);
	}
	


  /* ***************************************************************************
	 ***************************************************************************
	 * Constructors.
	 */

	/** ************************************************************************
	 * Internal constructor. This is intended to be used only by the specialized
	 * classes. If useBorder is false, no Border
	 * is created for the Visual.
	 */
	 
	protected VisualJComponent(NodeSer node, GraphicView view, boolean useBorder)
	throws
		ParameterError
	{
		super();
		this.nodeSer = node;
		this.view = view;

		
		// Allow components to be positioned based on their coordinate location.
		this.setLayout(null);

		setBackground(getNormalBackgroundColor());
		try { setImageIcon(); }
		catch (IOException ex) { throw new ParameterError(ex); }
		
		int borderThickness = getNormalBorderThickness();
		
		outerNormalBorder = BorderFactory.createMatteBorder(
			borderThickness, borderThickness, borderThickness, borderThickness,
			normalBorderColor);
			
		outerNormalBorderHighlighted = BorderFactory.createMatteBorder(
			borderThickness, borderThickness, borderThickness, borderThickness,
			highlightedBorderColor);
			
		//outerNormalBorder = BorderFactory.createLineBorder(
		//	normalBorderColor,
		//	getNormalBorderThickness());
			
		//outerNormalBorderHighlighted = BorderFactory.createLineBorder(
		//	highlightedBorderColor,
		//	getNormalBorderThickness());
			
		normalBorder = BorderFactory.createTitledBorder(outerNormalBorder, node.getName());
		normalBorder.setTitleJustification(TitledBorder.CENTER);
		normalBorder.setTitleFont(view.getDisplayConstants().getComponentTitleFont());
		
		outerIconBorder = BorderFactory.createLineBorder(
			normalBorderColor, getNormalBorderThickness());
			
		outerIconBorderHighlighted = BorderFactory.createLineBorder(
			highlightedBorderColor, getNormalBorderThickness());
			
		iconBorder = BorderFactory.createTitledBorder(outerIconBorder, node.getName());
		
		
		//iconBorder = BorderFactory.createTitledBorder(iconBorder, node.getName(),
		//	TitledBorder.CENTER, TitledBorder.TOP, iconBorderFont);

		setBorder(this.normalBorder);
		
		//Insets insets = this.normalBorder.getBorderInsets(this);
		
			
		//try { setImageIcon(NodeIconImageNames.DefaultImageName); }
		//catch (IOException ex) { throw new ParameterError(ex); }
		
		enableEvents(java.awt.event.MouseEvent.MOUSE_DRAGGED);
		//enableEvents(java.awt.event.MouseEvent.MOUSE_PRESSED);
		//enableEvents(java.awt.AWTEvent.MOUSE_MOTION_EVENT_MASK);
	}
	
	
	protected VisualJComponent(NodeSer node, GraphicView view)
	throws
		ParameterError
	{
		this(node, view, node.useBorderWhenIconified());
	}
	
	
	/**
	 * To be called only by this class.
	 */
	 
	private final void setVisualJComponentRedundantState()
	throws
		Exception
	{
		// Update the dimensions.
		
		setSize();
		
		
		// Update the location.
		
		setLocation();

		
		// Set name on borders.
		
		if (normalBorder != null) normalBorder.setTitle(nodeSer.getName());
		if (iconBorder != null) iconBorder.setTitle(nodeSer.getName());
	}
	
	

  /* ***************************************************************************
	 ***************************************************************************
	 * Defined in Container.
	 */
	 
	public Component add(Component c)
	{
		super.add(c);
		//if (this.getWidth() == 0 || this.getHeight() == 0)
		//{
			//RuntimeException ex = new RuntimeException(
			//	"Adding a Component when the size of the Container is not defined.");
			//System.out.println(ex.getMessage());
			//ex.printStackTrace();
			//throw ex;
		//}
		
		try { this.setLocation(); }
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
		
		return c;
	}
	
	
	
  /* ***************************************************************************
	 ***************************************************************************
	 * Defined in VisualComponent interface.
	 */
	 
	public String getNodeId()
	throws NoNodeException { return nodeSer.getNodeId(); }
	
	
	public String getName() { return nodeSer.getName(); }
	
	
	public String getFullName() { return nodeSer.getFullName(); }


	public ImageIcon getImageIcon() { return imageIcon; }
	
	
	public void setImageIcon(ImageIcon imgIcon) { this.imageIcon = imgIcon; }
		
	
	public int getSeqNo() { return 0; }
	
	
	public boolean contains(VisualComponent comp)
	{
		if (! (this instanceof Container)) return false;
		
		if (! (comp instanceof VisualJComponent)) return false;
		
		Container container = (Container)this;
		Component[] children = container.getComponents();
		for (Component child : children)
		{
			if (child == comp) return true;
			if (child instanceof VisualComponent)
			{
				VisualComponent childVisual = (VisualComponent)child;
				try { if (childVisual.contains(comp)) return true; }
				catch (VisualComponent.NoNodeException ex) { continue; }
			}
		}
		
		return false;
	}


	public boolean isOwnedByMotifDef()
	{
		return getNodeSer().ownedByMotifDef();
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		return getNodeSer().ownedByActualTemplateInstance();
	}


	public String[] getChildIds()
	{
		return getNodeSer().getChildNodeIds();
	}


	public void nameChangedByServer(String newName, String newFullName)
	{
		this.nodeSer.setName(newName);
		this.nodeSer.setFullName(newFullName);
		this.setName(newName);
		Border border = this.getBorder();
		if (border != null)
			if (border instanceof TitledBorder) ((TitledBorder)border).setTitle(newName);
		this.repaint();
	}
	
	
	public void nodeDeleted()
	{
		view.notifyVisualRemoved(this);
		
		Set<VisualComponent> visuals;
		try { visuals = identifyVisualComponents(this.getNodeId()); }
		catch (ParameterError pe) { pe.printStackTrace(); return; }
		
		Set<Component> toRepaint = new HashSet<Component>();
		for (VisualComponent visual : visuals)
		{
			Component parent = ((Component)visual).getParent();
			if (parent != null) toRepaint.add(parent);
			
			try { view.removeVisual(visual); }
			catch (Exception ex) { ex.printStackTrace(); }
		}
		
		for (Component c : toRepaint) c.repaint();
			
		// ...Does not seem to be a way to force a repaint...
		// Same problem exists for any change, e.g., moving a Component.
		// Name changes work though.
	}
	
	
	public void notifyCrossReferenceCreated(String toNodeId, String crossRefId)
	{
		NodeSer ser;
		try { ser = getModelEngine().getNode(crossRefId); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
			return;
		}
		
		if (! (ser instanceof CrossReferenceSer)) throw new RuntimeException(
			"Node with Id " + crossRefId + " is not a CrossReference");
		CrossReferenceSer crSer = (CrossReferenceSer)ser;
		getClientView().notifyCrossReferenceCreated(crSer);
	}
		
		
	public void notifyCrossReferenceDeleted(String toNodeId, String crossRefId)
	{
		getClientView().notifyCrossReferenceDeleted(this.getNodeId(), toNodeId, crossRefId);
	}
		
		
	public View getClientView() { return view; }
	
	
	public ModelEngineRMI getModelEngine() { return view.getModelEngine(); }
	
	
	public NodeSer getNodeSer() { return nodeSer; }
	
	
	public void setNodeSer(NodeSer nodeSer)
	throws NoNodeException { this.nodeSer = nodeSer; }
	

	public void update()
	{
		NodeSer nodeSer = null;
		try { nodeSer = getClientView().getModelEngine().getNode(this.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, 
				ThrowableUtil.getAllMessages(ex),
				"Error while updating Visual " + this.getName(), ex);
		}
		
		this.update(nodeSer);
		
		depopulateChildren();
		// Note: The above call will destroy all of the Visibility Controls.,
		//To do: What I really need to do is have a method called refreshChildren():
		//As it removes a child, it preserves the child's visibility state and then
		//re-creates the child with that state.
		
		if (! getAsIcon())
		{
			try { populateChildren(); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
	}
	
	
	public void update(NodeSer newNodeSer)
	{
		this.nodeSer = newNodeSer;

		this.setUnpropagated(false);
		this.setInconsistent(false);
		
		try { this.refreshRedundantState(); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, 
				ThrowableUtil.getAllMessages(ex),
				"Error while updating Visual " + this.getName(), ex);
					
			GlobalConsole.printStackTrace(ex);
		}
	}


	public void redraw() { repaint(); }
	
	
	public void refresh() { update(); redraw(); }
	
	
	public void refreshLocation()
	{
		NodeSer nSer = null;
		try { nSer = getClientView().getModelEngine().getNode(this.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, 
				ThrowableUtil.getAllMessages(ex),
				"Error while updating Visual " + this.getName(), ex);
		}
		
		
		//this.nodeSer.setX(nSer.getX());
		//this.nodeSer.setY(nSer.getY());
		
		this.nodeSer = nSer;
		
		try { this.setLocation(); }
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		this.getClientView().repaint();
	}

	
	public void refreshSize()
	{
		NodeSer nSer = null;
		try { nSer = getClientView().getModelEngine().getNode(this.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, 
				ThrowableUtil.getAllMessages(ex),
				"Error while updating Visual " + this.getName(), ex);
		}
		
		this.nodeSer.setWidth(nSer.getWidth());
		this.nodeSer.setHeight(nSer.getHeight());
		this.nodeSer.setIconWidth(nSer.getIconWidth());
		this.nodeSer.setIconHeight(nSer.getIconHeight());
		this.nodeSer.setX(nSer.getX());
		this.nodeSer.setY(nSer.getY());
		
		try
		{
			this.setSize();
			this.setLocation();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		this.repaint();
	}


	public void setVisible(boolean vis)
	{
		super.setVisible(vis && nodeSer.isVisible());
	}
	

	public void refreshRedundantState()
	throws
		Exception
	{
		setVisualJComponentRedundantState();
	}
	
	
	public boolean getAsIcon() { return this.asIcon; }
	
	
	public void setAsIcon(boolean yes) { this.asIcon = yes; }
	
	
	public boolean useBorderWhenIconified() { return nodeSer.useBorderWhenIconified(); }
	
	
	public void populateChildren()
	throws
		ParameterError,
		Exception
	{
		setPopulated(true);
		View view = getClientView();
		
		String[] ids = getChildIds();
		for (String id : ids) view.addNode(id);  // will not add a Visual if it
			// cannot be instantiated, e.g., if the Visual is a ScenarioVisual.
		
		validate();
		repaint();
	}
	
	
	/*public void refreshChildren()
	throws
		ParameterError,
		Exception
	{
	}*/
	
	
	public Set<VisualComponent> getChildVisuals()
	{
		Set<VisualComponent> subVisuals = new HashSet<VisualComponent>();
		Component[] comps = this.getComponents();
		for (Component c : comps)
		{
			if (c instanceof VisualComponent) subVisuals.add((VisualComponent)c);
		}
		
		return subVisuals;
	}
	
	
	public Object getParentObject() { return this.getParent(); }
	
	
	public Container getContainer() { return this; }
	
	
	public Component getComponent() { return this; }
	
	
	public void highlight(boolean yes)
	{
		if (yes)
		{
			if (normalBorder != null) normalBorder.setBorder(outerNormalBorderHighlighted);
			if (iconBorder != null) iconBorder.setBorder(outerIconBorderHighlighted);
		}
		else
		{
			if (normalBorder != null) normalBorder.setBorder(outerNormalBorder);
			if (iconBorder != null) iconBorder.setBorder(outerIconBorder);
		}
			
		highlightBackground(yes);
		highlighted = yes;
	}
	
	
	protected void setHighlighted(boolean yes) { this.highlighted = yes; }
	
	
	public boolean isHighlighted() { return highlighted; }


	public ModelElementSer getModelElementSer()	{ return (ModelElementSer)(getNodeSer()); }
	
	
	public void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		writeScenarioDataAsXML(writer, indentation, scenarioVisual);
		Set<VisualComponent> children = getChildVisuals();
		for (VisualComponent child : children)
			child.writeScenarioDataAsXMLRecursive(writer, indentation+1, scenarioVisual);
	}
	
	
	public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		List<String> attrNames = new Vector<String>();
		List<Serializable> values = scenarioVisual.getAttributeValues(this, attrNames);
		int i = 0;
		for (String attrName : attrNames)
		{
			writer.println(StringUtils.tabs[indentation] + "<Attribute name=\"" +
				attrName + "\" value=\"" + values.get(i++) + "\"/>");
		}
	}

	
	
  /* ***************************************************************************
	 ***************************************************************************
	 * Defined in GraphicVisualComponent interface.
	 */
	 
	 
	public GraphicView getGraphicView() { return view; }
	
	
	public int transformNodeXCoordToView(double modelXCoord)
	throws
		ParameterError
	{
		/*
		<X-coord in this Visual> =
			transformNodeDXToView(<X-coord in Node>) + <Visual left inset>
		 */
		
		int leftInset = (this.getInsets() == null? 0 : this.getInsets().left);
		
		return transformNodeDXToView(modelXCoord) + leftInset;
	}
	
	
	public int transformNodeYCoordToView(double modelYCoord)
	throws
		ParameterError
	{
		/*
		<Y-coord in this Visual> =
			transformNodeDYToView(<Node height> - <Y-coord in Node>) + <Visual top inset>
		 */
		
		int topInset = (this.getInsets() == null? 0 : this.getInsets().top);
		
		double nodeHeight;
		if (this.getAsIcon()) nodeHeight = this.getNodeSer().getIconHeight();
		else nodeHeight = this.getNodeSer().getHeight();
		
		return transformNodeDYToView(nodeHeight - modelYCoord) + topInset;
	}
		
		
	public int transformNodeDXToView(double nodeWidth)
	throws
		ParameterError
	{
		return this.view.transformNodeDXToView(nodeWidth);
	}
		
		
	public int transformNodeDYToView(double nodeHeight)
	throws
		ParameterError
	{
		return this.view.transformNodeDYToView(nodeHeight);
	}
	
	
	public double transformViewXCoordToNode(int visualXCoord)
	throws
		ParameterError
	{
		/*
		<X-coord in Node> =
			transformViewDXToNode(<X-coord in Visual> - <Visual left inset>)
		 */
		
		int leftInset = (this.getInsets() == null? 0 : this.getInsets().left);
		
		return transformViewDXToNode(visualXCoord - leftInset);
	}
	
	
	public double transformViewYCoordToNode(int visualYCoord)
	throws
		ParameterError
	{
		/*
		<Y-coord in Node> =
			<Node height> -
				transformViewDYToNode(<Y-coord in Visual> - <Visual top inset>)
		 */
		
		int topInset = (this.getInsets() == null? 0 : this.getInsets().top);
		
		double nodeHeight;
		if (this.getAsIcon()) nodeHeight = this.getNodeSer().getIconHeight();
		else nodeHeight = this.getNodeSer().getHeight();
		
		return nodeHeight - transformViewDYToNode(visualYCoord - topInset);
	}
		
		
	public double transformViewDXToNode(int viewWidth)
	throws
		ParameterError
	{
		return this.view.transformViewDXToNode(viewWidth);
	}
		
		
	public double transformViewDYToNode(int viewHeight)
	throws
		ParameterError
	{
		return this.view.transformViewDYToNode(viewHeight);
	}


	public void resizeNode(double dw, double dh, double childShiftX,
		double childShiftY, boolean notify)
	throws
		Exception
	{
		// In Node space, the Node's lower boundary has been moved
		// downward by nodeDy, away from its children.
		incrementSizeOnServer(dw, dh, childShiftX, childShiftY, notify);
		
		// In Node space, the Node's lower boundary has been moved
		// downward.
		incrementLocationOnServer(0.0, -childShiftY, false);

		((ContainerVisual)(getParent())).componentResized(this);
	
		//...Problem: The componentChanged method does not properly
		//	notify a Conduit Container.
	
		// this ends up notifying the server a second time,
		// causing a callback to the Conduit Visual.
	}
		
		
	public void moveNode(double dx, double dy, boolean notify)
	throws
		Exception
	{
		incrementLocationOnServer(dx, dy, notify);
		
		((ContainerVisualJ)(getParent())).componentMoved(this);
		
		//...Problem: The componentChanged method does not properly
		//	notify a Conduit Container.
		
		// this ends up notifying the server a second time,
		// causing a callback to the Conduit Visual.
	}
	
	
	public Color getNormalBackgroundColor() { return ContainerColor; }
	
	
	public Color getHighlightedBackgroundColor() { return ContainerHLColor; }

	
	
  /* ***************************************************************************
	 ***************************************************************************
	 * Protected accessors.
	 */
	 
	protected void setView(GraphicView view) { this.view = view; }
	
	
	protected int getViewXOriginOffset() { return this.getGraphicView().getXOriginOffset(); }
	
	
	protected int getViewYOriginOffset() { return this.getGraphicView().getYOriginOffset(); }
	
	
	protected void setImageIcon()
	throws
		IOException
	{
		final String handle = getNodeSer().getIconImageHandle();
		final ImageIcon icon = getClientView().getStoredImage(handle);
		setImageIcon(icon);
	}
	

	protected void setImageIcon(String imageIconName)
	throws
		IOException
	{
		setImageIcon(getClientView().getStoredImage(imageIconName));
	}
	
	
	protected boolean isPopulated() { return populated; }
	
	protected void setPopulated(boolean pop) { this.populated = pop; }
	
	
	protected Dimension getNormalSize() { return visualNormalSize; }
	
	
	protected long getLastUpdated() { return nodeSer.getLastUpdated(); }
	
	
	protected int getVersionNumber() { return nodeSer.getVersionNumber(); }
	
	
	protected boolean isInconsistent() { return inconsistent; }
	
	protected void setInconsistent(boolean newValue)
	{
		//System.out.println("INCONSISTENT: for " + this.getName() +
		//	": changing from " + this.inconsistent + " to " +
		//	newValue);
			
		this.inconsistent = newValue;
	}
	
	
	protected boolean isUnpropagated() { return unpropagated; }
	
	protected void setUnpropagated(boolean newValue)
	{
		//System.out.println("UNPROPAGATED: for " + this.getName() +
		//	": changing from " + this.unpropagated + " to " +
		//	newValue);
			
		this.unpropagated = newValue;
	}
	
	

  /* ***************************************************************************
	 ***************************************************************************
	 * Public methods.
	 */

	/** ************************************************************************
	 * Dispose of all resources owned by the Visual.
	 */
	 
	public synchronized void close()
	{
	}
	

	/** ************************************************************************
	 * Change the size of this Visual. The server is notified of the change.
	 * The server may reject the change immediately, in which case this Visual
	 * should not change its size. If the server later rejects the change, the
	 * server is expected to push a resize command to the client to cause this
	 * Visual to be returned to its prior (or some other) size.
	 */
	 
	public synchronized void changeSize(int newWidth, int newHeight,
		int childShiftX, int childShiftY)
	throws
		Exception
	{
		// Notify server.
		NodeSer nodeSer = 
			view.getModelEngine().changeNodeSize(
				getVersionNumber(), getNodeId(),
				this.transformViewDXToNode(
					newWidth - this.getInsets().left - this.getInsets().right),
				this.transformViewDYToNode(
					newHeight - this.getInsets().top - this.getInsets().bottom),
				this.transformViewDXToNode(childShiftX),
				this.transformViewDYToNode(childShiftY)
					);
			
		// Save the new version number of the Node.
		this.setNodeSer(nodeSer);
		

		// Change size of this Visual.
		this.visualNormalSize = new Dimension(newWidth, newHeight);
		
		// Re-display this Visual.
		repaint();
	}
	
	
	/** ************************************************************************
	 * Delegates to View.select(boolean).
	 */
	 
	public synchronized void select(boolean yes)
	{
		getClientView().select(this, yes);  // calls select_internal
	}
	
	

  /* ***************************************************************************
	 ***************************************************************************
	 * Override painting methods inheritied from JComponent.
	 */

	protected void paintComponent(final Graphics g)
	{
		//if (clearBeforeDraw()) g.clearRect(0, 0, this.getWidth(), this.getHeight());
		
		// Display background image, if any; otherwise, background color.
		if (getAsIcon())
		{
			int iconX = (this.getWidth() - getImageIcon().getIconWidth()) / 2;
			int iconY = (this.getHeight() - getImageIcon().getIconHeight()) / 2;
			
			//if ((iconX >= 0) && (iconY >= 0))
			{
				getImageIcon().paintIcon(this, g, iconX, iconY);
			}
		}
		else
		{
			super.paintComponent(g);
			
			if (this.getNodeSer().isResizable())
			{
				// Draw the grip on the bottom right corner, which the user drags
				// to resize the Visual.
				
				int w = this.getWidth();
				int h = this.getHeight();
				int d = 2;
				
				g.setColor(Color.black);
				
				while (d < ResizingTabDiam)
				{
					g.drawLine(w-d, h, w, h-d);
					d += 2;
				}
			}
		}
	}
	
	
	//protected boolean clearBeforeDraw() { return true; }
	
	

  /* ***************************************************************************
	 ***************************************************************************
	 * Package-scope methods.
	 */
	
	
	int getNormalBorderThickness()
	{
		if (view == null) return 0;
		if (getAsIcon())
			if (useBorderWhenIconified())
				return view.getDisplayConstants().getNormalBorderThickness();
			else
				return 0;
		else
			return view.getDisplayConstants().getNormalBorderThickness();
	}
	
	
	int getIconWidth()
	{
		if (view == null) return 0;
		return view.getDisplayConstants().getIconWidth();
	}
	
	
	int getIconHeight()
	{
		if (view == null) return 0;
		return view.getDisplayConstants().getIconHeight();
	}
	
	
	int getBorderFontPointSize()
	{
		if (view == null) return 0;
		return view.getDisplayConstants().getBorderFontPointSize();
	}
	
	
	int getConduitThickness()
	{
		if (view == null) return 0;
		return view.getDisplayConstants().getConduitThickness();
	}
	
	
	int getPortDiameter()
	{
		if (view == null) return 0;
		return view.getDisplayConstants().getPortDiameter();
	}
	
	
	Font getComponentTitleFont()
	{
		if (view == null) return null;
		return view.getDisplayConstants().getComponentTitleFont();
	}
	
	
	final void incrementLocationOnServer(double dx, double dy, boolean notify)
	throws
		Exception
	{
		this.inconsistent = true;
		
		NodeSer nodeSer = getClientView().getModelEngine().incrementNodeLocation(
			this.nodeSer.getVersionNumber(), this.nodeSer.getNodeId(), dx, dy, notify);

		this.setNodeSer(nodeSer);
	}
	
	
	final void incrementSizeOnServer(double dw, double dh, double childShiftX,
		double childShiftY, boolean notify)
	throws
		Exception
	{
		this.inconsistent = true;
		
		NodeSer nodeSer = getClientView().getModelEngine().incrementNodeSize(
			this.nodeSer.getVersionNumber(), this.nodeSer.getNodeId(), dw, dh,
			childShiftX, childShiftY, notify);
			
		this.setNodeSer(nodeSer);
	}
	

	final void setLocationOnServer(double x, double y)
	throws
		Exception
	{
		this.inconsistent = true;
		
		NodeSer nodeSer = getClientView().getModelEngine().changeNodeLocation(
			this.nodeSer.getVersionNumber(), this.nodeSer.getNodeId(), x, y);

		this.setNodeSer(nodeSer);
	}
	
	
	final void setSizeOnServer(double w, double h, double childShiftX,
		double childShiftY)
	throws
		Exception
	{
		this.inconsistent = true;
		
		NodeSer nodeSer = getClientView().getModelEngine().changeNodeSize(
			this.nodeSer.getVersionNumber(), this.nodeSer.getNodeId(), w, h,
			childShiftX, childShiftY);
			
		this.setNodeSer(nodeSer);
	}
	

	final void updateLocationOnServer()
	throws
		Exception
	{
		this.setLocationOnServer(nodeSer.getX(), nodeSer.getY());
	}
	
	
	/**
	 * Return true if this Visual is an outermost Visual in its View.
	 */
	 
	boolean isOutermostInView()
	{
		return this.getParent() instanceof GraphicView.VisualComponentDisplayArea;
	}
	
	
	/* debug
	public void setLocation(int x, int y)
	{
		GlobalConsole.printStackTrace(new Exception("Setting location of '" + this.getFullName() +
			"' to " + x + "," + y));
		super.setLocation(x, y);
	}
	
	public void setLocation(java.awt.Point p)
	{
		GlobalConsole.printStackTrace(new Exception("Setting location of '" + this.getFullName() +
			"' to " + p));
		super.setLocation(p);
	}
	
	public void setBounds(int x, int y, int width, int height)
	{
		GlobalConsole.printStackTrace(new Exception("Setting bounds of '" + this.getFullName() +
			"' to " + x + "," + y + "," + width + "," + height));
		super.setBounds(x, y, width, height);
	}
	*/ //end debug
	
	
	/**
	 * This Visual's owner may be a DisplayArea or a ModelScenarioPanel: this method
	 * must work for either case.
	 * 
	 * See also PortVisualJ.setLocation() and ConduitVisualJ.setLocation().
	 */
	 
	public void setLocation()
	throws
		Exception
	{
		Container container = this.getParent();
		if (container == null) return;


		// If the Container is a ModelScenarioPanel, position the Visual based on the
		// location of the VisualJComponent that depicts the Node's parent.
		
		if (container instanceof ModelScenarioVisual)
		{
			((ModelScenarioVisual)container).setLocation(this);
			// Note that notifyVisualMoved is not called.
			return;
		}
		
		
		// Update Component location.
		
		int newVisualX;
		int newVisualY;

		Insets insets = this.getInsets();
		int insetLeft = (insets == null? 0 : insets.left);
		int insetTop = (insets == null? 0 : insets.top);
		int insetBottom = (insets == null? 0 : insets.bottom);
			
		if (this.isOutermostInView())
		{
			GraphicView view = this.getGraphicView();
			
			newVisualX = 
				view.transformNodeXCoordToView(0, view.getXOriginOffset()) - insetLeft;
				
			newVisualY = 
				view.transformNodeYCoordToView(this.getNodeSer().getHeight(),
					view.getYOriginOffset()) - insetTop;
		}
		else
		{
			// The Visual is NOT the outermost VisualJComponent in the View.
			// This is the "normal" case.
			
			if (container instanceof VisualJComponent)
			{
				VisualJComponent containerVisual = (VisualJComponent)container;
				
				double nodeLeftEdge = this.getNodeSer().getX();
				
				double nodeTopEdge = this.getNodeSer().getY() 
					+ (this.getAsIcon() ?
						this.getNodeSer().getIconHeight() : this.getNodeSer().getHeight());
				
				newVisualX = 
					containerVisual.transformNodeXCoordToView(nodeLeftEdge) - insetLeft;
			
				newVisualY = 
					containerVisual.transformNodeYCoordToView(nodeTopEdge) - insetTop;
			}
			else
				throw new RuntimeException(
					"Should not happen: container is a " + container.getClass().getName());
		}

		this.setLocation(newVisualX, newVisualY);
		
		
		// Notify the View that this Visual has moved, so that it can adjust the
		// position of overlaid elements.
		
		this.getGraphicView().notifyVisualMoved(this);
		// Note that notifyVisualMoved is only called if the Visual is in the Domain
		// - i.e., not the ControlPanel.
	}
	
	
	public void setSize()
	throws
		Exception
	{
		// Adjust for inset.
		
		Insets insets = this.getInsets();
		
		int leftBorderWidth = (insets == null? 0 : insets.left);
		int topBorderHeight = (insets == null? 0 : insets.top);
		int rightBorderWidth = (insets == null? 0 : insets.right);
		int bottomBorderHeight = (insets == null? 0 : insets.bottom);
		
		double nodeW = getAsIcon() ? nodeSer.getIconWidth() : nodeSer.getWidth();
		double nodeH = getAsIcon() ? nodeSer.getIconHeight() : nodeSer.getHeight();
		
		int newVisualWidth = this.transformNodeDXToView(nodeW)
			+ leftBorderWidth + rightBorderWidth;
		int newVisualHeight = this.transformNodeDYToView(nodeH)
			+ topBorderHeight + bottomBorderHeight;
		
		setSize(newVisualWidth, newVisualHeight);
		setPreferredSize(getSize());
	}


	/** ************************************************************************
	 * Identify all VisualComponents displayed within this VisualJComponent
	 * that represent the specified node Id. This component is included (if it
	 * represents the specified node id).
	 */
	 
	Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError
	{
		Set<VisualComponent> comps = new TreeSetNullDisallowed<VisualComponent>();
		
		AbstractGraphicViewPanel.identifyVisualComponents(this, nodeId, comps);
		
		return comps;
	}
	
	
	VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError
	{
		if (this.getNodeId().equals(nodeId)) return this;
		Component[] components = getComponents();
		for (Component comp : components)
		{
			if (! (comp instanceof VisualComponent)) continue;
			VisualComponent vis = ((VisualJComponent)comp).identifyVisualComponent(nodeId);
			if (vis != null) return vis;
		}
		
		return null;
	}
	
	
	/** ************************************************************************
	 * Query the server to determine if the Node that is depicted by this Visual
	 * has an Attribute with the specified name.
	 */
	 
	boolean hasAttribute(String attrName)
	throws
		Exception
	{
		AttributeSer attr = null;
		attr = view.getModelEngine().getAttribute(nodeSer.getNodeId(), attrName);
		return (attr != null);
	}
	
	
	/** ************************************************************************
	 * Query the server to determine if the Node that is depicted by this Visual
	 * has an Attribute of the specified class, in the context of the specified
	 * Model Scenario.
	 */
	 
	boolean hasAttributeType(Class c, String scenarioId)
	throws
		Exception
	{
		AttributeSer[] attrs = 
			view.getModelEngine().getAttributes(nodeSer.getNodeId(), c.getName(), scenarioId);
		return (attrs != null) && (attrs.length > 0);
	}


	/** ************************************************************************
	 * Return the Set of Visuals that are currently owned by this Visual.
	 */
	 
	Set<VisualJComponent> getChildren()
	{
		Set<VisualJComponent> componentSet = new HashSet<VisualJComponent>();
		Component[] componentAr = this.getComponents();
		
		for (Component component : componentAr)
			if (component instanceof VisualJComponent)
				componentSet.add((VisualJComponent)component);
		
		
		return componentSet;
	}
	
	
	void highlightBackground(boolean yes)
	{
		if (yes) setBackground(getHighlightedBackgroundColor());
		else setBackground(getNormalBackgroundColor());
	}
	
	
	/** ************************************************************************
	 * Flush the AWT Event Queue for the calling Thread and disable future Events
	 * for Mouse Movement actions.
	 */
	 	
	void disableEvents()
	{
		this.disableEvents(AWTEvent.MOUSE_MOTION_EVENT_MASK);
	}
	

	/** ************************************************************************
	 * Re-enable the enqueing of move movement Events in the Event Queue for
	 * the calling Thread.
	 */
	 	
	void enableEvents()
	{
		this.enableEvents(AWTEvent.MOUSE_MOTION_EVENT_MASK);
	}


	/** ************************************************************************
	 * Set the z-order of this Visual such that it is at the background of its
	 * parent Container. If this Visual has no parent Container, then do nothing.
	 */
	 	
	void sendToBack()
	{
		Container container = getParent();
		if (container == null) return;
		
		int index = container.getComponentZOrder(this);

		container.setComponentZOrder(this, container.getComponentCount()-1);
	}


	/** ************************************************************************
	 * Set the z-order of this Visual such that it is at the foreground of its
	 * parent Container. If this Visual has no parent Container, then do nothing.
	 */
	 	
	void bringToFront()
	{
		Container container = getParent();
		if (container == null) return;
		
		int index = container.getComponentZOrder(this);

		container.setComponentZOrder(this, 0);
	}
	
	
	//private static Class[] ChildVisualClasses = { ActivityVisual.class, FunctionVisual.class, 
	//	ConduitVisual.class, ConstraintVisual.class };
	
	
	/** ************************************************************************
	 * Remove all of the child Visuals of this Visual, except for child Visuals
	 * that are of any of the specified types of subclasses of those types.
	 * (Do not remove mandatory elements.) Also, remove any Controls that are
	 * associated with this Visual.
	 */
	 
	void depopulateChildren(Class ...exclusions)
	{
		setPopulated(false);
		
		Component[] childComponents = this.getComponents();
		for (Component child : childComponents)
		{
			if (! (child instanceof VisualJComponent)) continue;
			VisualJComponent childVisual = (VisualJComponent)child;
			
			Class childVisualClass = childVisual.getClass();
			
			
			// Check if the child is of a type that should be excluded based on
			// the exclusions argument.
			
			boolean exclude = false;
			
			for (Class exclusion : exclusions) if (exclusion.isAssignableFrom(childVisualClass))
			{
				exclude = true;
				break;
			}
				
			if (exclude) continue;
			
			
			// Check that the child is a Visual that depicts a child Node, as defined
			// by the Node Container hierarchy. These are the "non-mandatory"
			// Visual types.
			
			boolean isAChildClass = false;
			if (child instanceof VisualJComponent)
			//for (Class c : ChildVisualClasses) if (c.isAssignableFrom(childVisualClass))
			{
				isAChildClass = true;
				//break;
			}
			
			if (! isAChildClass) continue;  // don't remove mandatory types.
				
			
			// Remove it from the GUI.

			try { this.getClientView().removeVisual(childVisual); }
			catch (Exception ex)
			{
				this.setInconsistent(true);
				
				ErrorDialog.showReportableDialog(this,
					"Error while removing Visual " + childVisual.getName(),
					ex);
			}
		}
		
		repaint();
	}
	
	
	void depopulateChildren()
	{
		Class[] exclusions = new Class[0];
		depopulateChildren(exclusions);
	}
	
	
	/** ************************************************************************
	 * Populate the Attributes that are owned by the Model Element represented
	 * by this Visual..
	 *
	 
	void populateAttributes()
	throws
		ParameterError,
		Exception
	{
		NodeSer node = getNodeSer();
		
		if (node instanceof ModelElementSer)
		{
			Set<String> attrNodeIds = ((ModelElementSer)node).getAttributeNodeIds();
		
			for (String attrNodeId : attrNodeIds)
			{
				this.getClientView().addNode(attrNodeId);
				//Set<VisualComponent> attrVisuals = this.getClientView().addNode(attrNodeId);
			}
		}
		
		
		//setVisible(true);
		validate();
		repaint();
	}*/
	
	
	/**
	 * Add a "What is this?" MenuItem to the specified Popup Menu.
	 * This method is overridden by the PortedContainerVisualJ class so that it
	 * can provide a "What is this?" MenuItem for a Port-State binding, which is
	 * not an actual VisualComponent but rather is merely rendered within its
	 * PortedContainerVisualJ.
	 */
	 
	void addWhatIsThisMenuItem(JPopupMenu containerPopup, final int x, final int y)
	{
		VisualHelper.addWhatIsThisMenuItem(this, containerPopup, this, x, y);
	}
	
	
	void addCrossRefMenuItem(JPopupMenu containerPopup, final int x, final int y)
	{
		VisualHelper.addCrossRefMenuItem(this, containerPopup, this, x, y);
	}
	
	
	/**
	 * Append items to the context menu that is provided as a argument. When this
	 * is called, the View has already added items to the popup menu. The x and y
	 * values are the mouse location in the Component that is the original source
	 * of the Event.
	 */
	 
	void addContextMenuItems(JPopupMenu containerPopup, final int x, final int y)
	{
		addWhatIsThisMenuItem(containerPopup, x, y);
		addCrossRefMenuItem(containerPopup, x, y);
		
		JMenuItem menuItem = new HelpfulMenuItem("Get Web URL", false,
			"Place in the system clipboard a Web URL that can be used in a Web page " +
			"to display the selected object. A choice of formats will be presented.");
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// Display choice of format.
				
				String[] descriptions = new String[4];
				String[] formats;
				try
				{
					formats = getModelEngine().getAvailableHTTPFormats(getNodeId(),
						descriptions);
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(VisualJComponent.this, ex);
					return;
				}
				
				JPopupMenu formatPopup = new JPopupMenu();
				int i = 0;
				for (final String format : formats)
				{
					HelpfulMenuItem mi = new HelpfulMenuItem(format, false,
						descriptions[i++]);
					formatPopup.add(mi);
					mi.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e2)
						{
							// Get URL from server.
							
							String urlString;
							try { urlString = getModelEngine().getWebURLString(getNodeId(), format); }
							catch (Exception ex)
							{
								ErrorDialog.showReportableDialog(VisualJComponent.this, ex);
								return;
							}
							
							// Place the URL in the system clipboard.
							
							StringSelection stringSelection = new StringSelection(urlString);
							
							Clipboard clipboard = 
								VisualJComponent.this.getToolkit().getSystemClipboard();
							clipboard.setContents(stringSelection, stringSelection);
						}
					});
				}
				
				formatPopup.show(VisualJComponent.this, x, y);
			}
		});
		
		containerPopup.add(menuItem);
	}
	
	
	/** ************************************************************************
	 * Return the shortest distance between this Visual's resizable border (the
	 * edge that one clicks and drags) and the specified Point.
	 * The Point is in the space of this Visual. The closest side is
	 * returned as the first element in 'side', which must have at least one element.
	 * The values 0 through 3 represent the sides, bottom, right, top, left,
	 * respectively (i.e., counter-clockwise from bottom).
	 */
	 
	protected double getDistanceFromEdge(java.awt.Point p, int[] side)
	{
		double shortestDistance = 0.0;
		int closestSide = 0;
		
		int s = 0;
		double distance = (double)(Math.abs(p.y));
		if (distance < shortestDistance)
		{
			shortestDistance = distance;
			closestSide = s;
		}
		
		s = 1;
		distance = (double)(Math.abs(this.getWidth() - p.x));
		if (distance < shortestDistance)
		{
			shortestDistance = distance;
			closestSide = s;
		}
		
		s = 2;
		distance = (double)(Math.abs(this.getHeight() - p.y));
		if (distance < shortestDistance)
		{
			shortestDistance = distance;
			closestSide = s;
		}
		
		s = 3;
		distance = (double)(Math.abs(p.x));
		if (distance < shortestDistance)
		{
			shortestDistance = distance;
			closestSide = s;
		}
		
		
		side[0] = closestSide;
		return shortestDistance;
	}
	

	/**
	 * Diagnostic method. Write the names and Ids of the child Visuals to stdout.
	 */
	 
	static void dumpVisuals(Container parent)
	{
		Component[] components = parent.getComponents();
		for (Component component : components)
			// each top-level visual component of this panel
		{
			if (! (component instanceof VisualJComponent)) continue;
			
			VisualJComponent visual = (VisualJComponent)component;
			
			System.out.println(
				"\t" + visual.getName() + " (Id=" + visual.getNodeId() + ")");
			
			dumpVisuals(visual);
		}
	}
	
	
	public String toString()
	{
		return "[ " + this.getClass().getName() + ": at " +
			this.getX() + ", " + this.getY() + "]";
	}
}



/* *****************************************************************************
 * *****************************************************************************
 * The classes below are abstract implementations to be used by the various
 * VisualComponent concrete classes.
 */


abstract class ContainerVisualJ extends VisualJComponent implements ContainerVisual,
	GraphicVisualComponent
{
	//public static final Image ContainerIconImage = getImage(ContainerIconImageName);
	
	
	/** ************************************************************************
	 * Constructor.
	 */
	
	ContainerVisualJ(NodeSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		//setBackground(ContainerColor);
		//setImageIcon(ContainerIconImage);
	}
	
	
	/** ************************************************************************
	 * Unregister for updates to this VisualComponent's child PersistentNodes,
	 * and dispose of the associated VisualComponents.
	 */
	 
	//public void releaseChildren();
	
	
	/** ************************************************************************
	 * If bShow is true, display the child VisualComponents of this VisualComponent,
	 * if there are any. Do not automatically fetch them from the server: only display the
	 * VisualComponents representing PersistentNodes that have already been fetched.
	 * Calling this method may alter the visual appearance of this VisualComponent;
	 * e.g., it may be expanded in size to accommodate the size and positions of
	 * the child components.
	 */
	 
	//public void showChildren(boolean show);
	
	
	/** ************************************************************************
	 * Allows this ContainerVisualJ to be notified when one of its children has
	 * been resized in a manner that might impact this ContainerVisualJ's visual
	 * depiction.
	 */
	 
	public void componentResized(VisualComponent child)
	throws
		Exception
	{
		componentChanged((VisualJComponent)child);
	}
	
	
	/** ************************************************************************
	 * Allows this ContainerVisualJ to be notified when one of its children has
	 * been moved in a manner that might impact this ContainerVisualJ's visual
	 * depiction.
	 */
	 
	void componentMoved(VisualJComponent child)
	throws
		Exception
	{
		componentChanged(child);
	}
	
	
	/** ************************************************************************
	 * Allows this ContainerVisualJ to be notified when one of its children has
	 * changed in a manner that might impact this ContainerVisualJ's visual
	 * depiction.
	 */
	 
	void componentChanged(VisualJComponent child)
	throws
		Exception
	{
		if (child.getParent() != this) throw new ParameterError(
			this.getName() + " does not contain " + child.getName());
		
		child.refresh();
		
		if (! (child instanceof PortedContainerVisualJ)) return;  // child has no Ports
		
		
		// 'child' has Ports.
		// 
		// Create a Set of the Conduits that are connected to this Container's
		// Ports, but that are external to this Container. Those are the Conduits
		// that need to be rerouted.
		
		Set<ConduitVisual> conduitsAffected = new HashSet<ConduitVisual>();
		
	
		PortedContainerSer pcSer = (PortedContainerSer)(child.getNodeSer());
		String[] portNodeIds = pcSer.portNodeIds;
		
		for (String portNodeId : portNodeIds)  // each Port of child
		{
			String[] conduitNodeIds = ((ModelContainerSer)(this.getNodeSer())).conduitNodeIds;
			
			for (String conduitNodeId : conduitNodeIds)
				// each Conduit owned by this ContainerVisualJ that is connected
				// to the Port
			{
				// Find Visual(s) identified by conduitNodeId.
				Set<VisualComponent> conduitVisuals =
					getClientView().identifyVisualComponents(conduitNodeId);
					
				for (VisualComponent v : conduitVisuals)
				{
					if (! (v instanceof ConduitVisual)) throw new RuntimeException(
						"Set of ConduitVisuals contains a Visual that is not a " +
						"ConduitVisual: is a " + v.getClass().getName());
						
					ConduitVisual conduitVisual = (ConduitVisual)v;
					
					ConduitSer conduitSer = (ConduitSer)(conduitVisual.getNodeSer());
						
					if (conduitSer.p1NodeId.equals(portNodeId) ||
						conduitSer.p2NodeId.equals(portNodeId))
					{
						// Conduit is connected to the Port
						conduitsAffected.add(conduitVisual);
					}
						
					break;  // only need to do this once to identify the Conduit.
				}
			}
		}
		
		
		// Notify server that Conduits must be re-positioned.
		// The server will send only one refresh Notice to the client as a
		// result, causing it to refresh this Container only once.
		
		//String[] conduitIdAr = new String[conduitsAffected.size()];
		//int i = 0;
		for (ConduitVisual cVis : conduitsAffected) cVis.refresh();
			//conduitIdAr[i++] = cVis.getNodeId();
		
		//this.getClientView().getModelEngine().rerouteConduits(this.getNodeId(), conduitIdAr);
	}
}


abstract class ModelContainerVisualJ extends ContainerVisualJ implements ModelContainerVisual
{
	ModelContainerVisualJ(ModelContainerSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
	}
	
	
	protected synchronized void paintComponent(Graphics g)
	{
		resetConduitColors();
		super.paintComponent(g);
		
		
		// Draw Conduits. We must do this because each ConduitVisualJ is set to
		// not be visible. We do that because Java does not appear to render them
		// properly, since they rely on transparency, so we have to render them
		// manually.
		
		Insets insets = this.getInsets();
		Graphics condiutGraphics = g.create(insets.left, insets.top,
			this.getWidth() - insets.left - insets.right,
			this.getHeight() - insets.top - insets.bottom);
		
		Component[] comps = getComponents();
		for (Component c : comps)
		{
			if (c instanceof ConduitVisualJ) ((ConduitVisualJ)c).paintComponent(condiutGraphics);
		}
	}
	
	
	public void resetConduitColors() { this.conduitColorNumber = 0; }
	
	
	public void incrementConduitColor()
	{
		conduitColorNumber = (conduitColorNumber + 1) % ConduitColors.length;
	}
	
	
	public Color getConduitColor() { return ConduitColors[conduitColorNumber]; }
	
	
	static int conduitColorNumber = 0;
	static Color[] ConduitColors = { 
		Color.blue,
		Color.cyan,
		Color.green,
		Color.lightGray,
		Color.magenta,
		Color.orange,
		Color.pink,
		Color.red
	};
}


/*abstract class ScenarioVisualJ extends VisualJComponent implements ScenarioVisual
{
	public static final Image ScenarioIconImage = getImage(NodeIconImageNames.ScenarioIconImageName);
	
	
	ScenarioVisualJ(ScenarioSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		setImageIcon(NodeIconImageNames.ScenarioIconImageName);
	}
}*/


abstract class PortedContainerVisualJ extends ModelContainerVisualJ implements PortedContainerVisual
{
	/** The mouse must click within this number of pixels to select a Port-State binding. */
	public static final double PortStateBindingSelectionTolerance = 3.0;
	
	//public static final Image PortedContainerIconImage = getImage(PortedContainerIconImageName);
	
	
	PortedContainerVisualJ(PortedContainerSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		//setImageIcon(PortedContainerIconImage);
	}
	
	
	void addWhatIsThisMenuItem(JPopupMenu containerPopup, final int x, final int y)
	{
		StateVisual[] stateVisualRef = new StateVisual[1];
		PortVisual[] portVisualRef = new PortVisual[1];
		if (! isOverPortBinding(x, y, stateVisualRef, portVisualRef))
		{
			super.addWhatIsThisMenuItem(containerPopup, x, y);
			return;
		}
		
		JMenuItem menuItem = new JMenuItem("What is this?");
		menuItem.setBackground(Color.yellow);
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					HelpWindow w = new HelpWindow("What is this?");
					URL url = new URL(
						HelpWindow.createURL("States", "Binding_a_Port_to_a_State"));
					w.setURL(url);
					w.setLocation(x, y);
					w.show();
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(PortedContainerVisualJ.this, ex);
				}
			}
		});
		
		containerPopup.add(menuItem);
	}
	
	
	void addContextMenuItems(JPopupMenu containerPopup, final int x, final int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
		
		// Check if x,y is over a Port-State Binding depiction.
		
		StateVisual[] stateVisualRef = new StateVisual[1];
		PortVisual[] portVisualRef = new PortVisual[1];
		if (isOverPortBinding(x, y, stateVisualRef, portVisualRef))  // Add MenuItems.
		{
			final String stateName = stateVisualRef[0].getFullName();
			final String portName = portVisualRef[0].getFullName();
			final String portId = portVisualRef[0].getNodeId();
			
			
			// Add MenuItem for deleting the binding.
			
			HelpfulMenuItem unbindMenuItem = new HelpfulMenuItem(
				"Remove binding", false,
				"Delete the binding between State '" + stateName + "' and Port '" +
				portName + ". " +
				"When a " + HelpWindow.createHref("Ports", "Port") +
				" is bound to a " + HelpWindow.createHref("States", "State") +
				", changes in the State's value propagate to the Port.");
				
			unbindMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try
					{
						try { getModelEngine().unbindPort(false, portId); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(null, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
							
								getModelEngine().unbindPort(true, portId);
						}
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(PortedContainerVisualJ.this, ex);
					}
				}
			});
			
			containerPopup.add(unbindMenuItem);
		}
	}
	
	
	/**
	 * If the specified coordinates (in this Visual's space) are over a Port-State
	 * binding line, within a reasonable tolerance, then return true and return
	 * in stateVisual[0] and portVisual[0] the Visuals depicting the bound State
	 * and Port, respectively.
	 */
	 
	boolean isOverPortBinding(int x, int y, StateVisual[] stateVisualRef,
		PortVisual[] portVisualRef)
	{
		stateVisualRef[0] = null;
		portVisualRef[0] = null;

		if (getAsIcon()) return false;
		
		StateVisualJ stateOfClosestBinding = null;
		PortVisualJ portOfClosestBinding = null;
		double distanceToClosestPortBinding = 0.0;
		
		StateVisualJ stateVisualJ = null;
		PortVisualJ portVisualJ = null;
				
		String[] stateIds = ((PortedContainerSer)(this.getNodeSer())).stateNodeIds;
		for (String stateId : stateIds)
		{
			Set<VisualComponent> stateVisuals = null;
			try { stateVisuals = getClientView().identifyVisualComponents(stateId); }
			catch (ParameterError pe) {pe.printStackTrace(); continue; }
			
			StateVisual stateVisual = null;
			for (VisualComponent visual : stateVisuals)
			{
				if (((VisualJComponent)visual).getParent() == this)
				{
					stateVisual = (StateVisual)visual;
					break;
				}
			}
			
			if (stateVisual == null) continue;  // no Visual for the State. This will
				// be the case for States for which isVisible is false.
			
			StateSer stateSer = null;
			try { stateSer = (StateSer)(getClientView().getModelEngine().getNode(stateId)); }
			catch (Exception ex) { ex.printStackTrace(); return false; }
			
			stateVisualJ = (StateVisualJ)stateVisual;
			
			String[] boundPortIds = stateSer.portBindingNodeIds;
			for (String boundPortId : boundPortIds)
			{
				Set<VisualComponent> portVisuals = null;
				try { portVisuals = getClientView().identifyVisualComponents(boundPortId); }
				catch (ParameterError pe) { pe.printStackTrace(); continue; }
				
				PortVisual portVisual = null;
				for (VisualComponent visual : portVisuals)
				{
					if (((VisualJComponent)visual).getParent() == this)
					{
						portVisual = (PortVisual)visual;
						break;
					}
				}
				
				if (portVisual == null)
				{
					GlobalConsole.println("Internal error: Port Visual not found for bound Port");
					continue;
				}
				
				// Determine if the x,y location is near the line connecting
				// the State and Port Visual.
				
				portVisualJ = (PortVisualJ)portVisual;
				
				int stateX = stateVisualJ.getX() + (stateVisualJ.getWidth() / 2);
				int stateY = stateVisualJ.getY() + (stateVisualJ.getHeight() / 2);
				int portX = portVisualJ.getX() + (portVisualJ.getWidth() / 2);
				int portY = portVisualJ.getY() + (portVisualJ.getHeight() / 2);
				// Line is from stateX, stateY to portX, portY.
				
				// Compute distance between (x,y) and the line.
				
				geometry.PointImpl p = new geometry.PointImpl(x, y);
				geometry.Point statePoint = new geometry.PointImpl(stateX, stateY);
				geometry.Point portPoint = new geometry.PointImpl(portX, portY);
				geometry.LineSegment lSeg = new geometry.LineSegment(statePoint, portPoint);
				double distance = lSeg.distanceTo(p);
				
				if (distance > PortStateBindingSelectionTolerance) continue;
				
				if (portOfClosestBinding == null)
				{
					stateOfClosestBinding = stateVisualJ;
					portOfClosestBinding = portVisualJ;
					distanceToClosestPortBinding = distance;
				}
				else if (distance < distanceToClosestPortBinding)
				{
					stateOfClosestBinding = stateVisualJ;
					portOfClosestBinding = portVisualJ;
					distanceToClosestPortBinding = distance;
				}
			}
		}
		
		if ((stateOfClosestBinding != null) && (portOfClosestBinding != null))
		{
			stateVisualRef[0] = stateVisualJ;
			portVisualRef[0] = portVisualJ;
			return true;
		}
		
		return false;
	}
	

	protected void paintComponent(Graphics g)
	{
		//System.out.println("Paining " + this.getName());
		super.paintComponent(g);
		
		if (getAsIcon()) return;
		
		
		// Draw Port bindings.
		
		String[] stateIds = ((PortedContainerSer)(this.getNodeSer())).stateNodeIds;
		for (String stateId : stateIds)
		{
			Set<VisualComponent> stateVisuals = null;
			try { stateVisuals = getClientView().identifyVisualComponents(stateId); }
			catch (ParameterError pe) {pe.printStackTrace(); continue; }
			
			StateVisual stateVisual = null;
			for (VisualComponent visual : stateVisuals)
			{
				if (((VisualJComponent)visual).getParent() == this)
				{
					stateVisual = (StateVisual)visual;
					break;
				}
			}
			
			if (stateVisual == null) continue;  // no Visual for the State. This will
				// be the case for States for which isVisible is false.
			
			StateSer stateSer = null;
			try { stateSer = (StateSer)(getClientView().getModelEngine().getNode(stateId)); }
			catch (Exception ex) {ex.printStackTrace(); return; }
			
			String[] boundPortIds = stateSer.portBindingNodeIds;
			for (String boundPortId : boundPortIds)
			{
				Set<VisualComponent> portVisuals = null;
				try { portVisuals = getClientView().identifyVisualComponents(boundPortId); }
				catch (ParameterError pe) { pe.printStackTrace(); continue; }
				
				PortVisual portVisual = null;
				for (VisualComponent visual : portVisuals)
				{
					if (((VisualJComponent)visual).getParent() == this)
					{
						portVisual = (PortVisual)visual;
						break;
					}
				}
				
				if (portVisual == null)
				{
					GlobalConsole.println("Internal error: Port Visual not found for bound Port");
					continue;
				}
				
				drawPortBinding(g, stateVisual, portVisual);
			}
		}
	}
		
		
	void drawPortBinding(Graphics g, StateVisual stateVisual, PortVisual portVisual)
	{
		Stroke originalStroke = null;
		Color originalColor;
		
		if (g instanceof Graphics2D)
		{
			Graphics2D g2 = (Graphics2D)g;
			originalStroke = g2.getStroke();
		}
		
		originalColor = g.getColor();
		
		try
		{
			if (g instanceof Graphics2D)
			{
				Graphics2D g2 = (Graphics2D)g;
				g2.setStroke(PortBindingStroke);
			}
			
			
			// Draw a dashed line from the center of the State to the center of
			// the Port.
			
			StateVisualJ stateVisualJ = (StateVisualJ)stateVisual;
			PortVisualJ portVisualJ = (PortVisualJ)portVisual;
			
			int stateX = stateVisualJ.getX() + (stateVisualJ.getWidth() / 2);
			int stateY = stateVisualJ.getY() + (stateVisualJ.getHeight() / 2);
			int portX = portVisualJ.getX() + (portVisualJ.getWidth() / 2);
			int portY = portVisualJ.getY() + (portVisualJ.getHeight() / 2);
			
			g.setColor(PortBindingColor);
			g.drawLine(stateX, stateY, portX, portY);
		}
		finally   // restore Graphics
		{
			if (g instanceof Graphics2D)
			{
				Graphics2D g2 = (Graphics2D)g;
				
				g2.setStroke(originalStroke);
			}
			
			g.setColor(originalColor);
		}
	}
	
	
	static final Color PortBindingColor = Color.gray;
	
	static final float[] PortBindingDashPattern = { (float)(3.0) };
	
	static final Stroke PortBindingStroke = 
		new BasicStroke(
			(float)3.0, // width
			BasicStroke.CAP_SQUARE, // cap
			BasicStroke.JOIN_ROUND, // join
			(float)10.0, // miterlimit
			PortBindingDashPattern, // dash
			(float)3.0  // dash_phase
			);


	public void update()
	{
		super.update();
		
		if (getAsIcon()) try { populatePorts(); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }

		
		// Repopulate Port Visuals.
		
		//String[] portNodeIds = ((PortedContainerSer)(this.getNodeSer())).portNodeIds;
		//for (String portNodeId : portNodeIds)
		//{
		//	try { this.getClientView().addNode(portNodeId); }
		//	catch (Exception ex)
		//	{
		//		ex.printStackTrace();
		//		continue;
		//	}
		//}
		
		
		// Update Conduits connected to this Visual's Ports.
		
		Container container = this.getParent();
		if (! (container instanceof ModelContainerVisualJ)) return;
		
		ModelContainerVisualJ parentVisual = (ModelContainerVisualJ)container;
		
		ModelContainerSer parentSer = (ModelContainerSer)(parentVisual.getNodeSer());
		
		String[] externalConduitNodeIds = parentSer.conduitNodeIds;
		
		PortedContainerSer thisPortedContainerSer = (PortedContainerSer)(this.getNodeSer());
		String[] portNodeIds = thisPortedContainerSer.portNodeIds;
		
		
		// Update this Visual's Port Visuals.
		/*
		for (String portNodeId : portNodeIds)
		{
			//NodeSer nodeSer = getClientView().getModelEngine().getNode(portNodeId);
			
			Set<VisualComponent> portVisuals = null;
			try { portVisuals = this.getClientView().identifyVisualComponents(portNodeId); }
			catch (Exception ex)
			{
				ex.printStackTrace();
				continue;
			}
			
			for (VisualComponent vis : portVisuals)
			{
				vis.update();
			}
		}*/
		
		
		// Update Conduits that are externally connected to this Visual's Port Visuals.
		
		for (String conduitId : externalConduitNodeIds)
			// each Conduit owned by this PortedContainer's parent,
			
		{
			Set<VisualComponent> conduitVisuals = null;
			try { conduitVisuals = this.getClientView().identifyVisualComponents(conduitId); }
			catch (Exception ex)
			{
				ex.printStackTrace();
				continue;
			}
			
			for (VisualComponent cv : conduitVisuals)
			{
				ConduitVisualJ conduitVisual = (ConduitVisualJ)cv;
				ConduitSer conduitSer = (ConduitSer)(conduitVisual.getNodeSer());
				
				String p1NodeId = conduitSer.p1NodeId;
				String p2NodeId = conduitSer.p2NodeId;
	
				for (String portNodeId : portNodeIds)  // each of this Visual's Ports.
				{
					if (portNodeId.equals(p1NodeId) || portNodeId.equals(p2NodeId))
						// p1NodeId or p2NodeId is owned by this PortedContainer
						// the Conduit is connected to a Port owned by this PortedContainer,
					{
						// Update the Conduit Visual.
						
						Set<VisualComponent> visuals = null;
						try { visuals = this.getClientView().identifyVisualComponents(conduitId); }
						catch (Exception ex)
						{
							ex.printStackTrace();
							break;
						}
						
						for (VisualComponent vis : visuals)
						{
							((ConduitVisual)vis).refresh();
						}
						
						break;  // go on to next Conduit.
					}
				}
			}
		}
		
		getClientView().notifyVisualComponentPopulated(this, true);
	}

	
	/** for debug only.
	protected synchronized void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		System.out.println("PortedContainer " + this.getName() + " has these Ports:");
		Component[] comps = this.getComponents();
		for (Component comp : comps)
		{
			if (comp instanceof PortVisualJ)
			{
				PortVisualJ portVis = (PortVisualJ)comp;
				System.out.println("\t" + portVis.getName() + " at " + portVis.getX() 
					+ "," + portVis.getY() + " with dimensions " + portVis.getWidth()
					+ "," + portVis.getHeight());
				//portVis.repaint();
			}
		}
	}*/
	
	/*
	void populateSubcomponents()
	throws
		ParameterError,
		Exception
	{
		super.populateSubcomponents();
		populateStates();
		populatePorts();
	}*/
	
	
	/**
	 * Populate this PortedContainerVisualJ with Visuals for any States that the
	 * NodeSer (which must be a PortedContainerSer) owns.
	 *
	 
	void populateStates()
	throws
		Exception
	{
		PortedContainerSer ser = (PortedContainerSer)(this.getNodeSer());
		String[] stateIds = ser.stateNodeIds;
		
		for (String stateId : stateIds) getClientView().addNode(stateId);
	}*/
	
	
	/**
	 * Populate this PortedContainerVisualJ with Visuals for any Ports that the
	 * NodeSer (which must be a PortedContainerSer) owns.
	 */
	 
	void populatePorts()
	throws
		Exception
	{
		PortedContainerSer ser = (PortedContainerSer)(this.getNodeSer());
		String[] portIds = ser.portNodeIds;
		for (String portId : portIds)
		{
			getClientView().addNode(portId);
		}
	}
	
	
	/** ************************************************************************
	 * Populate this PortedContainer with child Visuals to depict the Node's
	 * children. Do this recursively, up to noOfLevels levels, where 1 means one
	 * level of children (i.e., only immediate children) within this PortedContainer.
	 * If noOfLevels is zero (0), do nothing.
	 *
	 
	void populate(int noOfLevels)
	throws
		Exception
	{
		...
	}
	*/
}

