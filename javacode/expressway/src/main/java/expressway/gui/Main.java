/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.Constants;
import expressway.gui.domainlist.*;
import expressway.awt.AWTTools;
import expressway.generalpurpose.MainCallback;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Frame;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.Vector;
import java.util.List;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;
import java.util.Collection;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;


/**
 * Main class for the GUI. Instantiates the top-level GUI components
 * and exits.
 */

public class Main
{
	/**
	 * Main method for the Expressway GUI. Instantiate the top-level GUI components
	 * and exit.
	 */

	public static void main(String[] args)
	{
		// Set the properties needed by RMI.
		
		String rmiCodebase = "file://" + System.getProperty("user.dir") + 
			System.getProperty("file.separator") + "visual.jar";
			
		rmiCodebase = rmiCodebase.replace(" ", "%20");
			
		System.setProperty("java.rmi.server.codebase", rmiCodebase);
		
		System.out.println("java.rmi.server.codebase=" + System.getProperty(
			"java.rmi.server.codebase"));
			
		
		// Mac-specific UI properties.
		
		System.setProperty("Quaqua.tabLayoutPolicy", "wrap");
			
					
		// Instantiate the Main Frame.
		
		new MainFrame();
	}
}


/**
 * The main Frame of the Expressway GUI application.
 */
 
class MainFrame extends JFrame implements MainCallback, ClientMainApplication
{
	public static final String SmallIconImagePath = "/images/SmallIconImage.png";
	public static final String MediumIconImagePath = "/images/MediumIconImage.png";
	public static final String LargeIconImagePath = "/images/LargeIconImage.png";
	
	public static final int DefaultFrameX = 50;
	public static final int DefaultFrameY = 100;
	public static final int DefaultFrameWidth = 1100;
	public static final int DefaultFrameHeight = 600;
	public static final int DefaultFrameMinWidth = 800;
	public static final int DefaultFrameMinHeight = 100;

	private MainTabbedPanel panelManager = null;
	private boolean exitCleanupPhaseCompleted = false;
	
	private Image smallIconImage;
	private Image mediumIconImage;
	private Image largeIconImage;
	
	private DomainListPanel domainListPanel;
	private List<File> jarDirs = new Vector<File>();
	private List<File> xmlDirs = new Vector<File>();
	private ModelEngineRMI modelEngine = null;
	private int httpPort = 8080;
	
	
	MainFrame()
	{
		super("Expressway\u2122 Version " + ReleaseInfo.VersionNumber);
		setVisible(false);
		
		
		// *********************************************************************
		// Create menu bar.
		
		JMenuBar menuBar = new JMenuBar();
		final JMenu menu = new JMenu("Windows");
		menu.setMnemonic(KeyEvent.VK_W);
		menu.getAccessibleContext().setAccessibleDescription(
			"Display a menu of Windows; selecting one brings that Window to the front.");
		menuBar.add(menu);
		
		final ActionListener frameActionListener = new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Object source = e.getSource();
				if (source instanceof FrameMenuItem)
				{
					FrameMenuItem fmi = (FrameMenuItem)source;
					Frame f = fmi.getFrame();
					f.toFront();
				}
			}
		};
		
		menu.addMenuListener(new MenuListener()
		{
			public void menuSelected(MenuEvent e)
			{
				menu.removeAll();
				
				Frame[] frames = Frame.getFrames();
				
				for (Frame frame : frames)
				{
					if (! frame.isVisible()) continue;
					FrameMenuItem menuItem = new FrameMenuItem(frame);
					menuItem.addActionListener(frameActionListener);
					menu.add(menuItem);
				}
			}
			
			public void menuCanceled(MenuEvent e) {}

			public void menuDeselected(MenuEvent e) {}
		});
		
		menu.setMnemonic(KeyEvent.VK_W);
		this.setJMenuBar(menuBar);
		

		// *********************************************************************
		// Define behavior for when user closes the main window.
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowListener()
		{
			public void windowActivated(WindowEvent e) {}
			public void windowClosed(WindowEvent e) {}
			
			public void windowClosing(WindowEvent e)
			{
				closeMainWindowAndApplication();
			}
			
			public void windowDeactivated(WindowEvent e) {}
			public void windowDeiconified(WindowEvent e) {}
			public void windowIconified(WindowEvent e) {}
			public void windowOpened(WindowEvent e) {}
		});
		

		// *********************************************************************
		// Provide icon images for the OS to use to depict this application.
		
		List<Image> iconImages = new Vector<Image>();
		iconImages.add(smallIconImage = AWTTools.getImage(MainFrame.class, SmallIconImagePath));
		iconImages.add(mediumIconImage = AWTTools.getImage(MainFrame.class, MediumIconImagePath));
		iconImages.add(largeIconImage = AWTTools.getImage(MainFrame.class, LargeIconImagePath));
		setIconImages(iconImages);
		
		try
		{
			//Application app = Application.getApplication();

			Class appClass = Class.forName("com.apple.eawt.Application");
			java.lang.reflect.Method methodGetApp = appClass.getMethod("getApplication");
			Object app = methodGetApp.invoke(null);
			

			//app.setDockIconImage(mediumIconImage);
			
			java.lang.reflect.Method methodSetDocIconImage = appClass.getMethod(
				"setDockIconImage", Image.class);
				
			methodSetDocIconImage.invoke(app, mediumIconImage);

			Class appListenerClass = Class.forName("generalpurpose.AppleApplicationListener");
			Object appListener = appListenerClass.newInstance();
			
			java.lang.reflect.Method methodSetMainFrame = appListenerClass.getMethod(
				"setMainFrame", MainCallback.class);
			methodSetMainFrame.invoke(appListener, this);
	
			java.lang.reflect.Method methodSetAboutTitle = appListenerClass.getMethod(
				"setAboutTitle", String.class);
			methodSetAboutTitle.invoke(appListener, "About Expressway\u2122");
			
			java.lang.reflect.Method methodSetAboutText = appListenerClass.getMethod(
				"setAboutText", String.class);
			methodSetAboutText.invoke(appListener, "Expressway\u2122 v. " + 
				ReleaseInfo.VersionNumber + "\n" + ReleaseInfo.CopyrightStatement);
			
			java.lang.reflect.Method methodSetAboutImageIcon = appListenerClass.getMethod(
				"setAboutImageIcon", javax.swing.ImageIcon.class);
			methodSetAboutImageIcon.invoke(appListener, new ImageIcon(
				AWTTools.getImage(MainFrame.class, MainFrame.MediumIconImagePath)));
			
			Class appleApplAdapterClass = Class.forName("com.apple.eawt.ApplicationListener");
			java.lang.reflect.Method methodAddAppListener = appClass.getMethod(
				"addApplicationListener", appleApplAdapterClass);
			
			methodAddAppListener.invoke(app, appListener);
		}
		catch (Throwable ex)
		{
			System.out.println("Warning: " + ex.getMessage());
			System.out.println("\t(Apple Mac-specific features not loaded)");
		}

		
		// Set initial position and size.
		
		setLocation(DefaultFrameX, DefaultFrameY);
		setSize(DefaultFrameWidth, DefaultFrameHeight);
		setMinimumSize(new Dimension(DefaultFrameMinWidth, DefaultFrameMinHeight));
		
		
		// Instantiate sub-components. (They are actually added to the JFrame's
		// conent panel. See documentation for JFrame.)
		
		this.panelManager = new MainTabbedPanel(this, this);
		getContentPane().add(this.panelManager, BorderLayout.CENTER);
		
		
		// Instantiate sub-components.
		
		this.domainListPanel = new DomainListPanel(this, this.panelManager);
		ClientGlobalValues.mainApplication = this;
		this.panelManager.addPanel("Domains", this.domainListPanel, false);
		
		
		// Add copyright notice.
		
		JPanel bottomPanel;
		getContentPane().add(bottomPanel = new JPanel(), BorderLayout.SOUTH);
		JLabel copyrightLabel = new JLabel(ReleaseInfo.CopyrightStatement);
		bottomPanel.add(copyrightLabel);
		copyrightLabel.setFont(copyrightLabel.getFont().deriveFont((float)10.0));
		
		
		// Re-position sub-components: The main tabbed panel.
		
		validate();
		
		
		// Restore configuration, as specified in configuration file.
		
		try { ClientConfigurationParser.readConfiguration(this, 
			this.panelManager, this.domainListPanel); }
		catch (Exception ex)
		{
			String home = System.getProperty("user.home");
			File configFile = new File(home, Constants.ConfigFileName);
		
			ErrorDialog.showReportableDialog(this,
				"Configuration file appears to be corrupt. You may delete it.\n" +
				"It is located at " + configFile.getAbsolutePath(),
				"Error Reading Configuration File", ex);
		}
		
		this.domainListPanel.recreateDomainSelector();  // now connected to ModelEngine.
		validate();
		

		// Make visible.
		
		setVisible(true);
	}
	
	
  /* From MainCallback.
   * To be called by this class or any other to close the application gracefully. */
	 
	public synchronized void closeMainWindowAndApplication()
	{
		ClientConfigurationWriter.writeConfiguration(this, 
			this.panelManager, this.domainListPanel);
		
		domainListPanel.saveFocusFieldChanges();
		
		if (! exitAll()) return;
		setVisible(false);
		dispose();
		System.exit(0);
	}
	
	
	/**
	 * Close open files and stop any activities in which the application is
	 * involved. This might result in asking the user for permission to abort
	 * some activities. If permission is not given, then the exit is aborted.
	 * Return true if successfully completed.
	 */
	 
	protected synchronized boolean exitAll()
	{
		if (exitCleanupPhaseCompleted) return true;
		exitCleanupPhaseCompleted = true;
		return true;
	}
	
		
	class FrameMenuItem extends JMenuItem
	{
		private Frame frame;
		
		FrameMenuItem(Frame f) { super(f.getTitle()); this.frame = f; }
		
		Frame getFrame() { return frame; }
	}


  /* From ClientMainApplication: */
	
	private String hostName = "localhost";
	private int portNumber = 1099;
	
	
	public String getModelEngineNetworkPath() { return this.hostName; }
	
	public void setModelEngineNetworkPath(String path) { this.hostName = path; }

	public int getHTTPPort() { return httpPort; }
	
	public void setHTTPPort(int p) { this.httpPort = p; }
	
	public ModelEngineRMI getModelEngine() { return this.modelEngine; }
	
	public void connectToModelEngine()
	{
		GlobalConsole.println("Connecting to Registry on " + hostName + "...");
			
		try // to Connect to ModelEngine
		{
			Registry registry = LocateRegistry.getRegistry(hostName, portNumber);
			
			GlobalConsole.println("...Registry found; connecting to Model Engine...");
			Remote remote = registry.lookup(Constants.RMIServiceName);
			
			modelEngine = (ModelEngineRMI)remote;
			if (domainListPanel != null) domainListPanel.connectedToModelEngine(true);

			GlobalConsole.println("Connected to Model Engine.");
		}
		catch (Throwable t)
		{
			ErrorDialog.showReportableDialog(this,
				"Could not connect to " + Constants.RMIServiceName +
					" on host " + hostName, t);
			return;
		}
		
		try { setHTTPPort(modelEngine.getHTTPPort()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, "While connecting to server", ex);
		}
	}

	public void addXMLDir(File xmlDir) { xmlDirs.add(xmlDir); }
	
	public List<File> getXMLDirs() { return xmlDirs; }
	
	public File getMostRecentXMLDir()
	{
		if (xmlDirs.size() == 0) return null;
		return xmlDirs.get(xmlDirs.size()-1);
	}
	
	public void addJARDir(File jarDir) { jarDirs.add(jarDir); }
	
	public List<File> getJARDirs() { return jarDirs; }
	
	public File getMostRecentJARDir()
	{
		if (jarDirs.size() == 0) return null;
		return jarDirs.get(jarDirs.size()-1);
	}
}

