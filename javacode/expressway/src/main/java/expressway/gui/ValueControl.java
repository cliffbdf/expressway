/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.Constants;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import java.io.Serializable;
import java.text.NumberFormat;
import java.awt.Font;
import java.awt.Color;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComponent;
import javax.swing.JOptionPane;



/** ********************************************************************
 * A Component that displays the value associated with some VisualComponent.
 * May (or may not) enable the user to modify the value.
 * A ValueControl owns the value field that displays the value that is managed
 * by the ValueControl, but a ValueControl does not own the Visual that the value
 * applies to.
 */
 
public abstract class ValueControl extends JPanel
{
	public static final int DefaultValueControlWidth = 100;
	public static final int DefaultValueControlHeight = 30;
	public static final Font ValueControlTextFont = new Font(null, Font.PLAIN, 10);
	public static final NumberFormat ValueControlNumberFormat = NumberFormat.getInstance();
	public static final Color NormalColor = Color.white;
	public static final Color HighlightColor = Color.yellow;


	static
	{
		ValueControlNumberFormat.setMaximumFractionDigits(2);
	}
	
	
	public interface ValueControlFactory
	{
		/** Return true if Visuals for the specified Node are of a type that
			should have ValueControls in the Visual's View. */
		boolean shouldHaveValueControl(NodeSer nodeSer);
		
		Class getValueControlClass();
		
		/** Create a ValueControl for the specified Visual.
			//and add it to the
			//ControlPanel and any Lists for managing it.
			'vis' is the Visual 
			(e.g., Attribute) to which the Control pertains. 'visControl' may
			be null. */
		ValueControl makeValueControl(VisualComponent vis, VisibilityControl visControl);
		
		ValueControl makeValueControl(NodeSer nodeSer, VisibilityControl visControl);
		
		/** Undoes the operation of makeValueControl. */
		void unmakeValueControl(ValueControl vc);
		
		ViewPanelBase getViewPanel();
		
		ControlPanel getControlPanel();
	}
	
	
	public ValueControl(ValueControlFactory factory,
		VisibilityControl visControl, VisualComponent visual, Serializable initValue,
		boolean editable)
	{
		this.factory = factory;
		this.visibilityControl = visControl;
		this.visual = visual;
		this.valueFont = ValueControlTextFont;
		this.numberFormat = ValueControlNumberFormat;
		//if (visControl != null) visControl.addValueControl(this);
		
		BorderLayout layout = new BorderLayout();
		setLayout(layout);
		layout.setHgap(0);
		layout.setVgap(0);
		
		valueField = new JTextField();
		//valueField.setBorder(null);
		valueField.setFont(valueFont);
		if (! editable) valueField.setEditable(false);
		
		add(valueField, BorderLayout.CENTER);
		
		valueField.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				updateServer();
			}
		});
		
		this.setSize(getValueControlWidth(), getValueControlHeight());
		
		setValue(initValue);
		
		highlight(false);
		
		validate();
		
		addFocusListener(new FocusListener()
		{
			public void focusGained(FocusEvent e) {}
			
			public void focusLost(FocusEvent e)
			{
				// Force the value to be cached when focus leaves the ValueControl.
				rememberValue();
			}
		});
	}
	
	
	public ValueControl(ValueControlFactory factory,
		VisualComponent visual, Serializable initValue, boolean editable)
	{
		this(factory, null, visual, initValue, editable);
	}
	
	
	public Font getValueFont() { return valueFont; }
	
	
	public void setValueFont(Font f)
	{
		this.valueFont = f;
		valueField.setFont(f);
	}
	
	
	public NumberFormat getValueNumberFormat() { return numberFormat; }
	
	
	public void setValueNumberFormat(NumberFormat nf)
	{
		this.numberFormat = nf;
	}
	
	
	public int getValueControlWidth() { return DefaultValueControlWidth; }
	
	
	public int getValueControlHeight() { return DefaultValueControlHeight; }
	
	
	public ValueControlFactory getFactory() { return factory; }
	
	
	public final VisibilityControl getVisibilityControl() { return visibilityControl; }
	
	
	public final ViewPanelBase getViewPanel() { return (ViewPanelBase)(visual.getClientView()); }
	
	
	public final ControlPanel getControlPanel() { return (ControlPanel)(this.getParent()); }
	
	
	public final VisualComponent getVisual() { return visual; }
	
	
	/** Highlight or make this ValueControl more prominent. May cause this to
		to be added to the selection set. */
		
	public final void highlight(boolean yes)
	{
		highlighted = yes;
		if (yes) valueField.setBackground(HighlightColor);
		else valueField.setBackground(NormalColor);
	}
	
	
	public boolean isHighlighted() { return highlighted; }
	
	
	public void setValue(Serializable value)
	{
		if (value == null)
		{
			String s;
			try { s = ObjectValueWriterReader.writeObjectAsString(value); }
			catch (ParameterError pe)
			{
				GlobalConsole.printStackTrace(pe);
				s = "???";
			}
			
			valueField.setText(s);
		}
		else
		{
			if (value instanceof Number)
			{
				double dval = ((Number)value).doubleValue();
				valueField.setText(numberFormat.format(dval));
			}
			else
			{
				String s;
				try { s = ObjectValueWriterReader.writeObjectAsString(value); }
				catch (ParameterError pe)
				{
					GlobalConsole.printStackTrace(pe);
					s = "???";
				}
				
				valueField.setText(s);
			}
		}
	}
	
	
	public Serializable getValue()
	throws
		ParameterError
	{
		String textValue = valueField.getText();
		
		Class fieldType =
			getControlPanel().retrieveRememberedTypeForVisual(visual, this.getClass());
			
		try
		{
			Serializable value = ObjectValueWriterReader.parseObjectFromString(textValue);
			getControlPanel().clearValueControlTypeForVisual(visual, this.getClass());
			return value;
		}
		catch (ParameterError pe)
		{
			if ((fieldType != null) && (String.class.isAssignableFrom(fieldType)))
				return textValue.trim();
			else
			{
				if (JOptionPane.showConfirmDialog(null, 
					"Could not recognize type of '" + textValue + "'; treat as a String?",
					"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
				{
					getControlPanel().rememberValueControlTypeForVisual(visual, this.getClass(), String.class);
					return textValue;
				}
				else
				{
					getControlPanel().clearValueControlTypeForVisual(visual, this.getClass());
					throw pe;
				}
			}
		}
	}
	
	
	public String getValueAsString()
	{
		return valueField.getText();
	}
	
	
	public JTextField getValueComponent() { return valueField; }
	
	
	/**
	 * Ask the ControlPanel to remember the value of this ValueControl.
	 */
	 
	public void rememberValue()
	{
		try
		{
			// Only do this if the value differs from the current db value.
			Serializable fieldValue = getValue();
			Serializable dbValue = getVisibilityControl().getScenario().
				getAttributeValue(visual.getNodeId());
			if (fieldValue == dbValue) return;
			if ((fieldValue != null) && fieldValue.equals(dbValue))
				getControlPanel().rememberValueControlStateForVisual(visual, this.getClass(),
					fieldValue);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}


	public AttributeSer getAttributeSer() { return (AttributeSer)(getVisual().getNodeSer()); }
	
	
	public AttributeVisual getAttributeVisual() { return (AttributeVisual)(getVisual()); }
	
	
	/**
	 * Update the server value that is depicted by this ValueControl. The value
	 * may be in the context of a Scenario, or not, depending on the context
	 * of the concrete class that extends this ValueControl.
	 */
	 
	public abstract void updateServer();
	
	
	/**
	 * Position this Value Control immediately to the right of the
	 * Visual that it depicts the value of. The Visual may be in the DisplayArea
	 * or it may be in the ControlPanel. Stagger nested Controls.
	 * Assumes that the Visual has already been positioned.
	 */
	 
	public void setLocation()
	{
		int x;
		int y;
		
		if (visual.getParentObject() instanceof ControlPanel)
		{
			x = visual.getLocation().x;
			y = visual.getLocation().y;
		}
		else // Visual owned by DisplayArea: translate to ControlPanel coordinates.
		{
			java.awt.Point visualLocationInDisplayArea = 
				((View)(visual.getClientView())).getDisplayArea().getLocationOfVisual(visual);

			x = visualLocationInDisplayArea.x;
			y = visualLocationInDisplayArea.y;
		}

		x += visual.getWidth() + 5;
		
		setLocation(x, y);
	}
	
	
 /* Internal implementation */
	
	
	private ValueControlFactory factory;
	private VisibilityControl visibilityControl;  // may be null
	private VisualComponent visual; // in a Scenario Panel, if in context of Scenario
	private JTextField valueField;
	private Font valueFont = ValueControlTextFont;
	private NumberFormat numberFormat = ValueControlNumberFormat;
	private boolean highlighted = false;
}

