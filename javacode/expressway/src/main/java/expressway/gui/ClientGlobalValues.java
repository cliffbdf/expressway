/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;


/**
 * Constants for operation, including diagnostic flags.
 * For the server application, see expressway.server.OperationMode.
 */
 
public class ClientGlobalValues
{
	public static ClientMainApplication mainApplication = null;

	/** If true, print all incoming PeerNotices to the console. */
	public static boolean PrintIncomingPeerNotices = true;
	
	/** If true, print PeerNotice to the console just before the Notice is
		executed. */
	public static boolean PrintExecutingPeerNotices = true;
	
	/** If true, print PeerNotice to the console when its target Visuals
		have been identified. */
	public static boolean PrintTargetIdentifiedPeerNotices = true;
	
	/** If true, convenient controls for bug reporting are displayed. */
	public static final boolean IncludeBugReportFeatures = true;
}

