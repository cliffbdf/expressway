/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;


public class AttributeVisibilityControlFactory implements VisibilityControlFactory
{
	private AttributeValueControlFactory attrValueControlFactory;
	
	
	public AttributeVisibilityControlFactory(AttributeValueControlFactory controlFactory)
	{
		this.attrValueControlFactory = controlFactory;
	}
	
	
	public Class getVisibilityControlClass() { return AttributeVisibilityControl.class; }
	
	
	public VisibilityControl makeVisibilityControl(VisualComponent visual,
		boolean showInitially)
	{
		return new AttributeVisibilityControl(this, 
			getViewPanel(),
			getControlPanel(),
			visual, 
			attrValueControlFactory,
			showInitially);
	}
	
	
	public void unmakeVisibilityControl(VisibilityControl vc)
	{
		vc.showValueControls(false, false);
	}
	
	
	public ValueControlFactory getValueControlFactory() { return attrValueControlFactory; }
		
	
	public ViewPanelBase getViewPanel() { return attrValueControlFactory.getViewPanel(); }
	
	
	public ControlPanel getControlPanel() { return attrValueControlFactory.getControlPanel(); }
}

