/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.hier;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.common.ClientModel.*;
import expressway.ser.*;
import expressway.gui.*;
import expressway.gui.InfoStrip.*;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;
import expressway.generalpurpose.StringUtils;
import java.awt.Container;
import java.io.Serializable;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.PopupMenu;
import java.awt.LayoutManager;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Window;
import java.lang.reflect.*;
import java.awt.event.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JPopupMenu;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.tree.*;
import javax.swing.event.*;



/**
 * Add Scenarios to the HierarchyViewPanel.
 * The Scenarios are implemented as ScenarioControlJPanels. Each is displayed in
 * a separate tab of the ScenarioSelectorImpl. Each ScenarioControlJPanel is
 * opaque and contains a complete table, displaying the Attribute values of each
 * table row in the cells of the table.
 */
 
public class HierarchyScenarioViewPanel extends HierarchyViewPanel
	implements
		NodeScenarioView
{
  /* ***************************************************************************/
  /* Static values. */
	
	public static final String ViewTypeName = "HierarchyScenarioView";
	
	
	
  /* ***************************************************************************/
  /* Instance variables. */

	
	/** Implements the standard functionality of a NodeScenarioView. This class
		delegates most of its functions to the Helper class. */
	//private ScenarioViewPanelHelper helper; - don't need this: uses the BaseHelper.
	
	/** Indicates whether this View Panel exists in its own popup View Window, or
		is part of the main Window. */
	private boolean isInPopupWindow = false;

		

  /* ***************************************************************************/
  /* Constructor. */

	
	public HierarchyScenarioViewPanel(PanelManager container, 
		NodeSer nodeSer, ViewFactory viewFactory, ModelEngineRMI modelEngine)
	throws
		Exception
	{
		super(container, nodeSer, viewFactory, modelEngine);
		if (! (nodeSer instanceof HierarchySer)) throw new RuntimeException(
			"nodeSer is not a ModelElementSer");
		createScenarioSelector();
	}
	
	
	
  /* ***************************************************************************
   * Methods from TabularVisualComponentFactory.
   */
	
	
	public VisualComponent makeTabularVisual(NodeSer nodeSer)
	throws
		ParameterError,
		Exception
	{
		if (getCurrentScenario() == null) return (VisualComponent)
			super.makeTabularVisual(nodeSer);
		
		Serializable[] values =
			getModelEngine().getAttributeDefaultValuesForNode(nodeSer.getNodeId());
		Class visualClass = getVisualClass(nodeSer);
		Constructor constructor = visualClass.getConstructor(NodeSer.class, View.class,
			Serializable[].class);
		return (TabularVisualJ)(constructor.newInstance(nodeSer, this, values));
	}
	
	
	
  /* ***************************************************************************
   * Methods from ViewPanelBase.
   */
	
	
	protected View.ViewHelper createHelper()
	{
		return new ScenarioViewPanelHelper(this, new SuperNodeScenarioView());
	}
	
	
	protected ScenarioViewPanelHelper getHelper()
	{
		return (ScenarioViewPanelHelper)(getBaseHelper());
	}
	
	
	public void postConstructionSetup()
	throws
		Exception
	{
		getHelper().postConstructionSetup();
	}
	
	
	public ScenarioSelector getScenarioSelector()
	{
		return getHelper().getScenarioSelector();
	}
	
	
	public void resizeScenarioSelector()
	{
		getHelper().resizeScenarioSelectorImpl();
	}
	
	
	public String getViewType() { return ViewTypeName; }
	
	
	
  /* ***************************************************************************
   * Methods from NodeView.
   */
	
	
	public ScenarioVisual getScenarioVisual(String scenarioId)
	{
		return getHelper().getScenarioVisual(scenarioId);
	}

	
	public ScenarioVisual showScenario(String scenarioId)
	throws
		Exception
	{
		return getHelper().showScenario(scenarioId);
	}
	
	 
	public void showScenarioCreated(String scenarioId)
	{
		// debug
		GlobalConsole.println("entered HierarchyScenarioViewPanel.showScenarioCreated");
		// end debug
		
		
		getHelper().showScenarioCreated(scenarioId);
	}
	
	
	public void showScenarioDeleted(String scenarioId)
	{
		getHelper().showScenarioDeleted(scenarioId);
	}

	
	
  /* ***************************************************************************
   * Methods from DomainViewPanelBase.
   */
	
	
	public Set<VisualComponent> addNode(String nodeId)
	throws
		Exception
	{
		return getHelper().addNode(nodeId);
	}
	
	
	public void removeVisual(VisualComponent visual)
	throws
		Exception
	{
		getHelper().removeVisual(visual);
	}
	
	
	public void notifyScenarioNameChanged(ScenarioVisual scenVis,
		String newName)
	{
		getHelper().notifyScenarioNameChanged(scenVis, newName);
	}

	
	public void notifyCrossReferenceCreated(CrossReferenceSer crSer)
	{
	}
		
		
	public void notifyCrossReferenceDeleted(String fromNodeId, String toNodeId, String crossRefId)
	{
	}
		
		
	public VisualComponent makeVisual(NodeSer nodeSer, Container container,
		boolean asIcon)
	throws
		ParameterError,
		Exception
	{
		return super.makeVisual(nodeSer, container, asIcon);
		// calls newVisual.setLocation().
		
		// The new Visual's setLocation() method should observe that the Container is
		// this ModelScenarioPanel, and position the new Visual based on the location
		// of the VisualComponent that depicts the Node's parent.
	}
	

	public Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError
	{
		return getHelper().identifyVisualComponents(nodeId);
		
		// The implementation in AbstractGraphicViewPanel will
		// automatically include the Scenario Panels and their Visuals.
	}
	
	
	public VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError
	{
		return getHelper().identifyVisualComponent(nodeId);
	}


	public Set<VisualComponent> identifyVisualComponents()
	throws
		ParameterError
	{
		return getHelper().identifyVisualComponents();
	}
	
	
	public void refreshRedundantState()
	throws
		Exception
	{
	}


	public ScenarioVisual getCurrentScenario()
	{
		return getHelper().getCurrentScenarioPanel();
	}
	
	
	HierarchyViewPanel createPopupHierarchyViewPanel(String title, NodeSer meSer)
	throws
		Exception
	{
		return (HierarchyViewPanel)(getViewFactory().createViewPanel(true, meSer.getNodeId()));
	}


	public ScenarioVisual makeScenarioVisual(ScenarioSer scenarioSer)
	{
		return new HierarchyScenarioPanel(this, scenarioSer, getScenarioSelector());
	}
	

	public ScenarioVisual getCurrentScenarioVisual()
	{
		return getHelper().getCurrentScenario();
	}
	
	
	public ControlPanel getCurrentControlPanel()
	{
		return getHelper().getScenarioSelector().getCurrentScenarioPanel();
	}
	
	
	public void notifyVisualMoved(VisualComponent visual)
	{
		super.notifyVisualMoved(visual);
		Set<NodeScenarioPanel> panels = getScenarioPanels();
		for (NodeScenarioPanel panel : panels)
		{
			panel.notifyVisualMoved(visual);
		}
	}
	
	
	public void notifyVisualRemoved(VisualComponent visual)
	{
		super.notifyVisualRemoved(visual);
		Set<NodeScenarioPanel> panels = getScenarioPanels();
		for (NodeScenarioPanel panel : panels)
		{
			panel.notifyVisualRemoved(visual);
		}
	}
	
	
	public void notifyThatDisplayAreaChanged(int x, int y, int width, int height)
	{
		getHelper().notifyThatDisplayAreaChanged(x, y, width, height);
	}
	
	

  /* ***************************************************************************
   * Methods from ScenarioView.
   */

	
	public void deleteControls(String nodeId)
	{
		getHelper().deleteControls(nodeId);
	}
	
	

  /* ***************************************************************************
   * Methods from NodeScenarioView.
   */

	
	/** ************************************************************************
	 * Toggle whether or not to display the current Scenario. If false, only
	 * the Domain is displayed. If true, the current Scenario is overlayed
	 * on the Domain. It is intended that this method be called by a menu
	 * selection or other visual control.
	 */
	 
	public void setShowScenarios(boolean show)
	{
		getHelper().setShowScenarios(show);
	}
	
	
	/** ************************************************************************
	 * Return the current state of the toggle set by the setShowScenarios method.
	 */
	 
	public boolean getShowScenarios()
	{
		return getHelper().getShowScenarios();
	}
	
	
	/** ************************************************************************
	 * Add the specified NodeScenarioPanel as an InfoStrip in the Scenario selection
	 * strip tabbed pane; also add the NodeScenarioPanel to this Scenario
	 * View Panel and make it not visible.
	 * Do not change the current visible tab unless the new tab is the only tab.
	 */
	 
	public void addScenarioVisual(final NodeScenarioPanel panel)
	{
		getHelper().addScenarioVisual(panel);
	}
	
	
	/** ************************************************************************
	 * Remove the specified Scenario as a tab in the Scenario selection strip. Do not
	 * change the current visible tab unless the tab is the current tab.
	 */
	 
	public void removeScenarioVisual(NodeScenarioPanel panel)
	{
		getHelper().removeScenarioVisual(panel);
	}
	
	
	public Set<NodeScenarioPanel> getScenarioPanels()
	{
		return getHelper().getScenarioPanels();
	}
	
	
	public void setCurrentScenarioPanel(String scenarioId)
	{
		getHelper().setCurrentScenarioPanel(scenarioId);
	}
	
	
	public void setCurrentScenarioPanel(ScenarioSer scenarioSer)
	{
		setCurrentScenarioPanel(scenarioSer.getNodeId());
	}
	
	
	/** ************************************************************************
	 * Make the specified panel the currently selected Scenario Panel, and
	 * make it visible if this.showScenarios is true. If panel is null, then
	 * make no panel the current one.
	 */
	 
	public void setCurrentScenarioPanel(NodeScenarioPanel panel)
	{
		getHelper().setCurrentScenarioPanel(panel);
	}
	
	
	public NodeScenarioPanel getCurrentScenarioPanel()
	{
		return getHelper().getCurrentScenarioPanel();
	}
	
	
	/** ************************************************************************
	 * Return the Attribute Value Panel that depicts the specified Scenario, or
	 * null if none depicts it.
	 */
	 
	public NodeScenarioPanel getScenarioPanel(String scenarioNodeId)
	{
		return getHelper().getScenarioPanel(scenarioNodeId);
	}
	

	public Container getContainer() { return this; }
	
	
	/** ************************************************************************
	 * Verify that the Scenario name is legitimate, including from a security
	 * perspective. Intended for screening user input Scenario names.
	 */
	 
	public void validateScenarioName(String name)
	throws
		ParameterError
	{
		getHelper().validateScenarioName(name);
	}
	
	
	public ScenarioInfoStrip createScenarioInfoStrip(NodeScenarioPanel panel)
	{
		return getScenarioSelector().createScenarioInfoStrip(panel);
	}
	
	
	public ScenarioInfoStrip constructScenarioInfoStrip(NodeScenarioPanel panel)
	{
		return new ScenarioInfoStripImpl(panel);
	}
	
	
	/** ************************************************************************
	 * Should be called immediately after the dimensions of this Panel are
	 * set or are changed, so that the dimensions of the Strip can be adjusted.
	 */
	 
	public void resizeScenarioSelectorImpl()
	{
		getHelper().resizeScenarioSelectorImpl();
	}
	
	
	
  /* ***************************************************************************
   * Methods from java.awt.Component.
   */
	
	public void repaint()
	{
		if (getHelper() == null) super.repaint();
		else getHelper().repaint();
	}
	
	
	
  /* ***************************************************************************
   * Methods from VisualComponent.
   */
	
	
	public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		writer.print(StringUtils.tabs[indentation]);
		writer.println("<HierarchyScenario full_name=\"" + scenarioVisual.getFullName() + "\">");
		writer.println("</HierarchyScenario>");
	}
	

  /* *******************************************************************
   * Methods defined in DomainSpaceVisual.
   */
	 
	
	public void showScenarioSetCreated(String scenarioSetId)
	{
		getHelper().showScenarioSetCreated(scenarioSetId);
	}

	
  /* *******************************************************************
   * Methods defined in ModelDomainVisual.
   *
	 
	
	public void loadAllScenarios()
	{
		getHelper().loadAllScenarios();
	}


	public java.util.List<ScenarioVisual> getScenarioVisuals()
	{
		return getHelper().getScenarioVisuals();
	}*/
	
	

  /* ***************************************************************************
   * Methods from AbstractGraphicViewPanel.
   */
	
	
	protected Set<VisualComponent> refreshVisuals(NodeSer node)
	throws
		Exception
	{
		return getHelper().refreshVisuals(node);
	}
	
	
	
  /* ***************************************************************************
   * Protected methods.
   */
	
   
	protected void addOrRemoveAttributeControls(VisualComponent visual)
	{
		getHelper().addOrRemoveAttributeControls(visual);
	}
	
	
	protected void notifyComponentRepainted(Component comp)
	{
		getHelper().notifyComponentRepainted(comp);
	}
	
	
	/**
	 * If there is a current Scenario, return its Node Id. Otherwise return
	 * null.
	 */
	
	protected String getCurrentScenarioId()
	{
		return getHelper().getCurrentScenarioId();
	}
	
	
	void dumpAttrPanels(String msg)  // for debug only
	{
		getHelper().dumpAttrPanels(msg);
	}
	
	
	/** ************************************************************************
	 * Create and instantiate a Scenario Selection Strip within this Panel.
	 * This method is intended to be called only once, or when the contents of
	 * this Panel need to be completely refreshed. This method does not
	 * set the size of the Strip: for that, call resizeScenarioSelectorImpl.
	 *
	 * Should be called after Panel construction because this method makes calls
	 * to the server to obtain information about each Scenario.
	 */
	
	protected void createScenarioSelector()
	{
		getHelper().createScenarioSelector();
	}
	
	
	/** ************************************************************************
	 * Find and return the index of the tab that contains a Visual for the
	 * specified Scenario. There should not be more than one, but if there is, 
	 * simply return the first one. If not found, return -1.
	 */
	 
	protected int getScenarioTab(NodeScenarioPanel panel)
	{
		return getHelper().getScenarioSelector().getScenarioTab(panel);
	}
	
	
	protected void addContextMenuItems(JPopupMenu containerPopup, 
		final VisualComponent visual, int x, int y)
	{
		//super.addContextMenuItems(containerPopup, visual, x, y);
		
		// Check if the Scenarios are hidden or not.
		
		if (! getShowScenarios()) return;

		
		/*
			Add menu items that pertain to Scenario-specific function:
			<Scenario><>.
			<Scenario><>.
			<Scenario><>.
		*/
		
	}

	
	public void notifyVisualComponentPopulated(VisualComponent visual,
		boolean populated)
	{
		super.notifyVisualComponentPopulated(visual, populated);
	}
	
	
  /* Proxy classes needed by ScenarioViewPanelHelper */
	
	
	private class SuperNodeScenarioView extends SuperNodeView
	{
	}
	
	
	private class SuperNodeView extends SuperView implements NodeView
	{
		public ScenarioVisual showScenario(String scenarioId) throws Exception
		{ throw new RuntimeException("Should not be called"); }
		
		public ScenarioVisual getScenarioVisual(String scenarioId)
		{ throw new RuntimeException("Should not be called"); }
		
		public void setOutermostNodeSer(NodeSer nodeSer) throws Exception
		{ HierarchyScenarioViewPanel.super.setOutermostNodeSer(nodeSer); }
		
		public NodeSer getOutermostNodeSer() { return HierarchyScenarioViewPanel.super.getOutermostNodeSer(); }
		
		public String getOutermostNodeId() { return HierarchyScenarioViewPanel.super.getOutermostNodeId(); }
		
		public VisualComponent getOutermostVisual() { return HierarchyScenarioViewPanel.super.getOutermostVisual(); }
		
		public void initializeNodeSer(NodeSer nodeSer) { HierarchyScenarioViewPanel.super.initializeNodeSer(nodeSer); }
	
		public void postConstructionSetup()
		throws
			Exception { HierarchyScenarioViewPanel.super.postConstructionSetup(); }
			
		public VisualComponent identifyVisualComponent(String nodeId)
		throws
			ParameterError { return HierarchyScenarioViewPanel.super.identifyVisualComponent(nodeId); }
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		throws
			Exception { return HierarchyScenarioViewPanel.super.getAvailableHTTPFormats(descriptions); }
		
		public String getWebURLString(String format)
		throws
			Exception { return HierarchyScenarioViewPanel.super.getWebURLString(format); }
		
		public String getExpresswayURLString()
		throws
			ParameterError { return HierarchyScenarioViewPanel.super.getExpresswayURLString(); }
		
		public String getViewType() { throw new RuntimeException("Should not be called"); }
	
		public ScenarioVisual makeScenarioVisual(ScenarioSer scenSer) { throw new RuntimeException("Should not be called"); }
		
		public DomainInfoStrip createDomainInfoStrip() { return HierarchyScenarioViewPanel.super.createDomainInfoStrip(); }
		
		public DomainInfoStrip getDomainInfoStrip() { return HierarchyScenarioViewPanel.super.getDomainInfoStrip(); }
		
		public DomainInfoStrip constructDomainInfoStrip() { return HierarchyScenarioViewPanel.super.constructDomainInfoStrip(); }
		
		public void resizeDomainInfoStrip() { HierarchyScenarioViewPanel.super.resizeDomainInfoStrip(); }
		
		
		/* From VisualComponent */
		
		public String getNodeId() throws NoNodeException
		{ return HierarchyScenarioViewPanel.super.getNodeId(); }
		
		public String getName()
		{ return HierarchyScenarioViewPanel.super.getName(); }
		
		public String getFullName()
		{ return HierarchyScenarioViewPanel.super.getFullName(); }
	
		public ImageIcon getImageIcon()
		{ return HierarchyScenarioViewPanel.super.getImageIcon(); }
		
		public void setImageIcon(ImageIcon imgIcon)
		{ HierarchyScenarioViewPanel.super.setImageIcon(imgIcon); }
		
		public String getDescriptionPageName()
		{ return HierarchyScenarioViewPanel.super.getDescriptionPageName(); }
			
		public int getSeqNo()
		{ return HierarchyScenarioViewPanel.super.getSeqNo(); }
		
		public boolean contains(VisualComponent comp) throws NoNodeException
		{ return HierarchyScenarioViewPanel.super.contains(comp); }
		
		public boolean isOwnedByMotifDef()
		{ return HierarchyScenarioViewPanel.super.isOwnedByMotifDef(); }
		
		public boolean isOwnedByActualTemplateInstance()
		{ return HierarchyScenarioViewPanel.super.isOwnedByActualTemplateInstance(); }
		
		public String[] getChildIds()
		{ return HierarchyScenarioViewPanel.super.getChildIds(); }
		
		public void nameChangedByServer(String newName, String newFullName)
		{ HierarchyScenarioViewPanel.super.nameChangedByServer(newName, newFullName); }
		
		public void nodeDeleted()
		{ HierarchyScenarioViewPanel.super.nodeDeleted(); }
		
		public void notifyCrossReferenceCreated(String toNodeId, String crossRefId)
		{ HierarchyScenarioViewPanel.super.notifyCrossReferenceCreated(toNodeId, crossRefId); }
			
		public void notifyCrossReferenceDeleted(String toNodeId, String crossRefId)
		{ HierarchyScenarioViewPanel.super.notifyCrossReferenceDeleted(toNodeId, crossRefId); }
			
		public ClientView getClientView()
		{ return HierarchyScenarioViewPanel.super.getClientView(); }
		
		public ModelEngineRMI getModelEngine()
		{ return HierarchyScenarioViewPanel.super.getModelEngine(); }
		
		public NodeSer getNodeSer()
		{ return HierarchyScenarioViewPanel.super.getNodeSer(); }
		
		public void setNodeSer(NodeSer nodeSer) throws NoNodeException
		{ HierarchyScenarioViewPanel.super.setNodeSer(nodeSer); }
		
		public void update()
		{ HierarchyScenarioViewPanel.super.update(); }
		
		public void update(NodeSer nodeSer)
		{ HierarchyScenarioViewPanel.super.update(nodeSer); }
		
		public void redraw()
		{ HierarchyScenarioViewPanel.super.redraw(); }
		
		public void refresh()
		{ HierarchyScenarioViewPanel.super.refresh(); }
		
		public java.awt.Point getLocation()
		{ return HierarchyScenarioViewPanel.super.getLocation(); }
		
		public int getX()
		{ return HierarchyScenarioViewPanel.super.getX(); }
		
		public int getY()
		{ return HierarchyScenarioViewPanel.super.getY(); }
	
		public void refreshLocation()
		{ HierarchyScenarioViewPanel.super.refreshLocation(); }
	
		public void refreshSize()
		{ HierarchyScenarioViewPanel.super.refreshSize(); }
		
		public void setLocation() throws Exception
		{ HierarchyScenarioViewPanel.super.setLocation(); }
		
		public void setSize() throws Exception
		{ HierarchyScenarioViewPanel.super.setSize(); }
	
		public void refreshRedundantState() throws Exception
		{ HierarchyScenarioViewPanel.super.refreshRedundantState(); }
	
		public boolean getAsIcon()
		{ return HierarchyScenarioViewPanel.super.getAsIcon(); }
		
		public void setAsIcon(boolean yes)
		{ HierarchyScenarioViewPanel.super.setAsIcon(yes); }
		
		public boolean useBorderWhenIconified()
		{ return HierarchyScenarioViewPanel.super.useBorderWhenIconified(); }
		
		public void populateChildren() throws ParameterError, Exception
		{ HierarchyScenarioViewPanel.super.populateChildren(); }
			
		public Set<VisualComponent> getChildVisuals()
		{ return HierarchyScenarioViewPanel.super.getChildVisuals(); }
		
		public Object getParentObject()
		{ return HierarchyScenarioViewPanel.super.getParentObject(); }
		
		public Container getContainer()
		{ return HierarchyScenarioViewPanel.super.getContainer(); }
		
		public void highlight(boolean yes)
		{ HierarchyScenarioViewPanel.super.highlight(yes); }
		
		public boolean isHighlighted()
		{ return HierarchyScenarioViewPanel.super.isHighlighted(); }
		
		public Color getNormalBackgroundColor()
		{ return HierarchyScenarioViewPanel.super.getNormalBackgroundColor(); }
		
		public Color getHighlightedBackgroundColor()
		{ return HierarchyScenarioViewPanel.super.getHighlightedBackgroundColor(); }
		
		public int getWidth()
		{ return HierarchyScenarioViewPanel.super.getWidth(); }
		
		public int getHeight()
		{ return HierarchyScenarioViewPanel.super.getHeight(); }
		
		public void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
			ScenarioVisual scenarioVisual) throws IOException
		{ HierarchyScenarioViewPanel.super.writeScenarioDataAsXMLRecursive(writer,
			indentation, scenarioVisual); }
		
		public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
			ScenarioVisual scenarioVisual) throws IOException
		{ HierarchyScenarioViewPanel.super.writeScenarioDataAsXML(writer, indentation,
			scenarioVisual); }
	}
	
	
	private class SuperView extends SuperClientView implements View
	{
		public void postConstructionSetup() throws Exception
		{ HierarchyScenarioViewPanel.super.postConstructionSetup(); }

		public VisualComponentDisplayArea getDisplayArea() { return HierarchyScenarioViewPanel.super.getDisplayArea(); }
		
		public JPanel getInfoPanel() { return HierarchyScenarioViewPanel.super.getInfoPanel(); }

		public void computeDisplayAreaLocationAndSize() { HierarchyScenarioViewPanel.super.computeDisplayAreaLocationAndSize(); }
		
		public ClientMainApplication getClientMainApplication() { return HierarchyScenarioViewPanel.super.getClientMainApplication(); }
		
		public void registerPeerListener() throws Exception { HierarchyScenarioViewPanel.super.registerPeerListener(); }
		
		public void unregisterPeerListener() throws Exception { HierarchyScenarioViewPanel.super.unregisterPeerListener(); }
	
		public boolean isPopup() { return HierarchyScenarioViewPanel.super.isPopup(); }
		
		public ControlPanel getCurrentControlPanel() { throw new RuntimeException("Should not be called"); }
		
		public ViewConstants getDisplayConstants() { return HierarchyScenarioViewPanel.super.getDisplayConstants(); }
	
		public ImageIcon getStoredImage(String handle)
		throws
			IOException { return HierarchyScenarioViewPanel.super.getStoredImage(handle); }
		
		public ViewFactory getViewFactory() { return HierarchyScenarioViewPanel.super.getViewFactory(); }
		
		public VisualComponent.VisualComponentFactory getVisualFactory() { return HierarchyScenarioViewPanel.super.getVisualFactory(); }
		
		public void saveFocusFieldChanges() { HierarchyScenarioViewPanel.super.saveFocusFieldChanges(); }
		
		public Class getVisualClass(NodeSer node) { return HierarchyScenarioViewPanel.super.getVisualClass(node); }
		
		public void select(VisualComponent visual, boolean yes) { HierarchyScenarioViewPanel.super.select(visual, yes); }
		
		public void selectNodesWithId(String nodeId, boolean yes)
		throws
			ParameterError { HierarchyScenarioViewPanel.super.selectNodesWithId(nodeId, yes); }
	
		public Set<VisualComponent> getSelected() { return HierarchyScenarioViewPanel.super.getSelected(); }
		
		public boolean isSelected(VisualComponent visual) { return HierarchyScenarioViewPanel.super.isSelected(visual); }
		
		public void selectAll(boolean yes) { HierarchyScenarioViewPanel.super.selectAll(yes); }
	
		public PeerListener getPeerListener() { return HierarchyScenarioViewPanel.super.getPeerListener(); }
		
		public ListenerRegistrar getListenerRegistrar() { return HierarchyScenarioViewPanel.super.getListenerRegistrar(); }
		
		public PanelManager getPanelManager() { return HierarchyScenarioViewPanel.super.getPanelManager(); }
		
		public Window getWindow() { return HierarchyScenarioViewPanel.super.getWindow(); }
		
		public void addTemporaryMouseListener(MouseListener listener) { HierarchyScenarioViewPanel.super.addTemporaryMouseListener(listener); }
		
		public void removeTemporaryMouseListener() { HierarchyScenarioViewPanel.super.removeTemporaryMouseListener(); }
		
		public void reportBug() { HierarchyScenarioViewPanel.super.reportBug(); }
	}
	
	
	private class SuperClientView extends SuperSwingComponent implements ClientView
	{
		public ModelEngineRMI getModelEngine()
		{ return HierarchyScenarioViewPanel.super.getModelEngine(); }
	
		public List<NodeSer> getOutermostNodeSers()
		{ return HierarchyScenarioViewPanel.super.getOutermostNodeSers(); }
		
		public List<String> getOutermostNodeIds()
		{ return HierarchyScenarioViewPanel.super.getOutermostNodeIds(); }
		
		public List<VisualComponent> getOutermostVisuals()
		{ return HierarchyScenarioViewPanel.super.getOutermostVisuals(); }
		
		public String getName() { return HierarchyScenarioViewPanel.super.getName(); }
	
		public void showScenarioCreated(String scenarioId)
		{ HierarchyScenarioViewPanel.super.showScenarioCreated(scenarioId); }
		
		public void showScenarioDeleted(String scenarioId)
		{ HierarchyScenarioViewPanel.super.showScenarioDeleted(scenarioId); }
		
		public ScenarioVisual showScenario(String scenarioId)
		throws
			Exception { throw new RuntimeException("Should not be called"); }
			
		public ScenarioVisual getCurrentScenarioVisual()
		{ throw new RuntimeException("Should not be called"); }
	
		public Set<VisualComponent> identifyVisualComponents(String nodeId)
		throws
			ParameterError { return HierarchyScenarioViewPanel.super.identifyVisualComponents(nodeId); }
	
		public Set<VisualComponent> identifyVisualComponents(Class c)
		throws
			ParameterError { return HierarchyScenarioViewPanel.super.identifyVisualComponents(c); }
		
		public Set<VisualComponent> identifyVisualComponents()	throws
			ParameterError { return HierarchyScenarioViewPanel.super.identifyVisualComponents(); }
		
		public Set<VisualComponent> refreshVisuals(String nodeId)
		throws
			Exception { return HierarchyScenarioViewPanel.super.refreshVisuals(nodeId); }
	
		public void refreshAttributeStateBinding(String domainId, String attrId, String stateId)
		throws
			Exception { HierarchyScenarioViewPanel.super.
				refreshAttributeStateBinding(domainId, attrId, stateId); }
		
		public Set<VisualComponent> addNode(String nodeId)
		throws
			Exception { return HierarchyScenarioViewPanel.super.addNode(nodeId); }

		public void removeVisual(VisualComponent visual)
		throws
			Exception { HierarchyScenarioViewPanel.super.removeVisual(visual); }
	
		public void notifyVisualComponentPopulated(VisualComponent visual, boolean populated)
		{ HierarchyScenarioViewPanel.super.notifyVisualComponentPopulated(visual, populated); }
		
		public void notifyScenarioNameChanged(ScenarioVisual scenVis, String newName)
		{ throw new RuntimeException("Should not be called"); }
		
		public void notifyCrossReferenceCreated(CrossReferenceSer crSer)
		{ throw new RuntimeException("Should not be called"); }
			
		public void notifyCrossReferenceDeleted(String fromNodeId, String toNodeId,
			String crossRefId)
		{ HierarchyScenarioViewPanel.super.notifyCrossReferenceDeleted(
			fromNodeId, toNodeId, crossRefId); }
	}
	
	
	private class SuperSwingComponent extends SuperAWTContainer implements SwingComponent
	{
		public JComponent getJComponent() { return HierarchyScenarioViewPanel.this; }
	}
	
	
	private class SuperAWTContainer extends SuperAWTComponent implements AWTContainer
	{
		public Container getContainer() { return HierarchyScenarioViewPanel.this; }
		
		public Component add(Component comp) { return HierarchyScenarioViewPanel.super.add(comp); }
		
		public Component add(Component comp, int index) { return HierarchyScenarioViewPanel.super.add(comp, index); }
		
		public Component add(String name, Component comp) { return HierarchyScenarioViewPanel.super.add(name, comp); }
		
		public Component getComponent(int n) { return HierarchyScenarioViewPanel.super.getComponent(n); }
		
		public int getComponentCount() { return HierarchyScenarioViewPanel.super.getComponentCount(); }
		
		public Component[] getComponents() { return HierarchyScenarioViewPanel.super.getComponents(); }
		
		public int getComponentZOrder(Component comp) { return HierarchyScenarioViewPanel.super.getComponentZOrder(comp); }
		
		public Insets getInsets() { return HierarchyScenarioViewPanel.super.getInsets(); }
		
		public boolean isAncestorOf(Component c) { return HierarchyScenarioViewPanel.super.isAncestorOf(c); }
		
		public void remove(Component comp) { HierarchyScenarioViewPanel.super.remove(comp); }
		
		public void remove(int index) { HierarchyScenarioViewPanel.super.remove(index); }
		
		public void removeAll() { HierarchyScenarioViewPanel.super.removeAll(); }
		
		public void setComponentZOrder(Component comp, int index) { HierarchyScenarioViewPanel.super.setComponentZOrder(comp, index); }
		
		public void setFont(Font f) { HierarchyScenarioViewPanel.super.setFont(f); }
		
		public void setLayout(LayoutManager mgr) { HierarchyScenarioViewPanel.super.setLayout(mgr); }
	}
	
	
	private class SuperAWTComponent implements AWTComponent
	{
		public Component getComponent() { return HierarchyScenarioViewPanel.this; }
		
		public void add(PopupMenu popup) { HierarchyScenarioViewPanel.super.add(popup); }
		
		public void addComponentListener(ComponentListener l) { HierarchyScenarioViewPanel.super.addComponentListener(l); }
		
		public void removeComponentListener(ComponentListener l) { HierarchyScenarioViewPanel.super.removeComponentListener(l); }
		
		public void addMouseListener(MouseListener l) { HierarchyScenarioViewPanel.super.addMouseListener(l); }
		
		public boolean contains(Point p) { return HierarchyScenarioViewPanel.super.contains(p); }
		
		public Rectangle getBounds() { return HierarchyScenarioViewPanel.super.getBounds(); }
		
		public Component getComponentAt(int x, int y) { return HierarchyScenarioViewPanel.super.getComponentAt(x, y); }
		
		public Graphics getGraphics() { return HierarchyScenarioViewPanel.super.getGraphics(); }
		
		public String getName() { return HierarchyScenarioViewPanel.super.getName(); }
		
		public Container getParent() { return HierarchyScenarioViewPanel.super.getParent(); }
		
		public Dimension getSize() { return HierarchyScenarioViewPanel.super.getSize(); }
		
		public Toolkit getToolkit() { return HierarchyScenarioViewPanel.super.getToolkit(); }
		
		public int getWidth() { return HierarchyScenarioViewPanel.super.getWidth(); }
		
		public int getHeight() { return HierarchyScenarioViewPanel.super.getHeight(); }
		
		public int getX() { return HierarchyScenarioViewPanel.super.getX(); }
		
		public int getY() { return HierarchyScenarioViewPanel.super.getY(); }
		
		public Point getLocation() { return HierarchyScenarioViewPanel.super.getLocation(); }
		
		public Point getLocation(Point rv) { return HierarchyScenarioViewPanel.super.getLocation(rv); }
		
		public Point getLocationOnScreen() { return HierarchyScenarioViewPanel.super.getLocationOnScreen(); }
		
		public boolean isEnabled() { return HierarchyScenarioViewPanel.super.isEnabled(); }
		
		public boolean isOpaque() { return HierarchyScenarioViewPanel.super.isOpaque(); }
		
		public boolean isShowing() { return HierarchyScenarioViewPanel.super.isShowing(); }
		
		public boolean isVisible() { return HierarchyScenarioViewPanel.super.isVisible(); }
		
		public void repaint() { HierarchyScenarioViewPanel.super.repaint(); }
		
		public void setBounds(int x, int y, int width, int height) { HierarchyScenarioViewPanel.super.setBounds(x, y, width, height); }
		
		public void setBounds(Rectangle r) { HierarchyScenarioViewPanel.super.setBounds(r); }
		
		public void setForeground(Color c) { HierarchyScenarioViewPanel.super.setForeground(c); }
		
		public void setLocation(int x, int y) { HierarchyScenarioViewPanel.super.setLocation(x, y); }
		
		public void setLocation(Point p) { HierarchyScenarioViewPanel.super.setLocation(p); }
		
		public void setName(String name) { HierarchyScenarioViewPanel.super.setName(name); }
		
		public void setPreferredSize(Dimension preferredSize) { HierarchyScenarioViewPanel.super.setPreferredSize(preferredSize); }
		
		public void setSize(Dimension d) { HierarchyScenarioViewPanel.super.setSize(d); }
		
		public void setSize(int width, int height) { HierarchyScenarioViewPanel.super.setSize(width, height); }
		
		public void setVisible(boolean b) { HierarchyScenarioViewPanel.super.setVisible(b); }
		
		public void validate() { HierarchyScenarioViewPanel.super.validate(); }
	}
}

