/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.hier;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.gui.*;
import expressway.ser.*;
import java.awt.Color;


/**
 * An opaque Panel that displays a Heirarchy table, in which the cell values are
 * the Attribute values for the Scenario depicted by this ScenarioPanel.
 * A HierarchyView overlays a set of HierarchyScenarioPanels: one for each Scenario.
 * Only one HierarchyScenarioPanel may be set to be visible at a time. In order to
 * show the Hierarchy's Attribute's default values, all HierarchyScenarioPanels are
 * set to not be visible.
 */
 
public class HierarchyScenarioPanel
	extends ScenarioControlJPanel
	implements NodeScenarioPanel
{
	private boolean showAttrControls = false;
	
	
	HierarchyScenarioPanel(HierarchyViewPanel viewPanel, ScenarioSer scenarioSer,
		ScenarioSelector controlPanelSelector)
	{
		super(viewPanel, scenarioSer, controlPanelSelector);
		this.setBackground(Color.white);
		this.setOpaque(true);
	}
	
	
	public NodeView getNodeView() { return (NodeView)(getViewPanel()); }
}

