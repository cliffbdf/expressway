/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.Constants;
import expressway.gui.VisibilityControl.*;
import expressway.gui.ValueControl.*;
import expressway.ser.*;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ComponentListener;
import java.util.Set;
import java.util.List;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import javax.swing.JPanel;
import javax.swing.JComponent;


/**
 * An implementation of ControlPanel that is based on JPanel.
 */
 
public abstract class ControlJPanel extends JPanel implements ControlPanel
{
	public static final int SpacingBetweenControls = 2;
	public static final int NestIndentation = 20;  // indentation for each level.

	private NodeViewPanelBase viewPanel;
	//private ScenarioVisual scenarioVisual;
	private ScenarioSelector controlPanelSelector;  // may be null


	/** The InfoStrip that owns this ScenarioPanel. Null until this
		Panel is added to an InfoStrip. */
	private InfoStrip infoStrip;
	
	/** Map (by ValueControl class and Visual Node ID) of the visibility of the
		ValueControls for that Visual. */
	private Map<String, Boolean> showValueControls = new HashMap<String, Boolean>();
	
	/** Map (by ValueControl class and Visual Node ID) of the value in the
		ValueControl field. */
	private Map<String, Serializable> valueControlFieldValues =
		new HashMap<String, Serializable>();
		
	/** If an Attribute ID has an entry, its type must conform to the specified type. */
	private Map<String, Class> valueFieldTypes = new HashMap<String, Class>();
	
	/** Set to true when Attribute value controls are to be shown. */
	private boolean showAttrControls = true;
	
	/** Listens for changes in the size of the Container, so that this Panel
		can be resized. */
	private ComponentListener componentListener;
	
	
	public ControlJPanel(NodeViewPanelBase viewPanel, //ScenarioVisual scenarioVisual,
		ScenarioSelector controlPanelSelector)
	{
		super(null);  // nullify LayoutManager
		if (! (viewPanel instanceof NodeScenarioView)) throw new RuntimeException(
			"Expected viewPanel to implement NodeScenarioView");
		
		this.viewPanel = viewPanel;
		//this.scenarioVisual = scenarioVisual;
		this.controlPanelSelector = controlPanelSelector;

		this.setBackground(ModelAPITypes.TransparentWhite);
		//this.setDoubleBuffered(false);
		this.setOpaque(false);

		// Add Listener to listen for changes in the size of the ControlPanel's
		// Container, so that the Listener can resize the ControlPanel.
		// (The Container is asssumed to be a JPanel with a null Layout Manager.)
		/*
		setComponentListener(new ComponentListener()
		{
			public void componentHidden(ComponentEvent e) {}
			
			public void componentMoved(ComponentEvent e) {}
			
			public void componentResized(ComponentEvent e)
			{
				layoutControlPanel();
			}
			
			public void componentShown(ComponentEvent e) {}
		});*/
	}
	
	
	public Component getComponent() { return this; }
	
	
	public Container getContainer() { return this; }
	
	
	public JComponent getJComponent() { return this; }
	
	
	public ViewPanelBase getViewPanel() { return viewPanel; }
	
	
	//public boolean isOptimizedDrawingEnabled() { return false; }
	
	
  /* Implemented methods from ControlPanel */
	
	
	public ControlPanel postConstructionSetup()
	{
		return this;
	}


	public VisualComponent addNode(NodeSer nodeSer, VisibilityControl visControl)
	throws
		Exception
	{
		VisualComponent visual = ((GraphicVisualComponentFactory)
			(getViewPanel().getVisualFactory())).makeGraphicVisual(nodeSer, this, true);
		visual.setSize();
		
		// Add Controls.
		
		if (nodeSer instanceof AttributeSer)
		{
			ValueControl vc = createValueControl(getAttributeValueControlFactory(), visControl, visual);
			if (visControl != null) visControl.addValueControl(vc);
		}
		
		return visual;
	}
	
	
	public void removeVisual(VisualComponent visual)
	{
		if (visual == null) return;
		
		if (visual instanceof Component)
		{
			Component comp = (Component)visual;
			if (comp.getParent() == this) this.remove(comp);
		}
		
		// Remove its ValueControl.
		
		AttributeValueControl ac = (AttributeValueControl)
			(getValueControl(visual, AttributeValueControl.class));
		uncreateValueControl(ac);
	}
	
	
	public ValueControlFactory getValueControlFactory(Class controlType)
	{
		if (AttributeValueControl.class.isAssignableFrom(controlType)) return getAttributeValueControlFactory();
		return null;
	}


	public VisibilityControlFactory getVisibilityControlFactory(Class controlType)
	{
		if (AttributeVisibilityControl.class.isAssignableFrom(controlType))
			return getAttrVisControlFactory();
		return null;
	}
	
	
	public ValueControl createValueControl(ValueControlFactory controlFactory,
		VisibilityControl visControl /* may be null */, VisualComponent visual)
	{
		ValueControl vc = controlFactory.makeValueControl(visual, visControl);
		if (visControl != null) visControl.addValueControl(vc);
		add(vc);
		vc.setLocation();
		
		// If the state of the ValueControl was previously saved in this ControlPanel,
		// restore that state.
		Serializable priorValue = retrieveRememberedValueFor(visual, vc.getClass());
		if (priorValue != null) vc.setValue(priorValue);
		return vc;
	}
	
	
	public void uncreateValueControl(ValueControl vc)
	{
		VisibilityControl visControl = vc.getVisibilityControl();
		if (visControl != null) visControl.removeValueControl(vc);
		remove(vc);
		vc.getFactory().unmakeValueControl(vc);
	}
	
	
	public void unmakeVisibilityControl(VisibilityControl visc)
	{
		visc.getFactory().unmakeVisibilityControl(visc);
	}
	
	
	public void setComponentListener(ComponentListener listener) { this.componentListener = listener; }
	public ComponentListener getComponentListener() { return this.componentListener; }
	
	
	public Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError
	{
		Set<VisualComponent> matchingComps = new HashSet<VisualComponent>();
		AbstractGraphicViewPanel.identifyVisualComponents(this, nodeId, matchingComps);
		return matchingComps;
	}
	
	
	public VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError
	{
		Component[] comps = getComponents();
		for (Component comp : comps)
		{
			if (comp instanceof VisualComponent)
				if (((VisualComponent)comp).getNodeId().equals(nodeId))
					return (VisualComponent)comp;
		}
		
		return null;
	}


	public VisualComponent addNode(String nodeId, VisibilityControl vc)
	throws
		Exception
	{
		return addNode(viewPanel.getModelEngine().getNode(nodeId), vc);
	}
	
	
	public void rememberVisibilityControlStateForVisual(VisualComponent visual,
		Class valueControlClass, boolean show)
	{
		
		showValueControls.put(
			valueControlClass.getName() + ":" + visual.getNodeId(), new Boolean(show));
	}
	
	
	public void rememberValueControlStateForVisual(VisualComponent visual,
		Class valueControlClass,
		Serializable value)
	{
		valueControlFieldValues.put(
			valueControlClass.getName() + ":" + visual.getNodeId(), value);
	}
	
	
	public Serializable retrieveRememberedValueFor(VisualComponent visual,
		Class valueControlClass)
	{
		return valueControlFieldValues.get(valueControlClass.getName() + 
			":" + visual.getNodeId());
	}
	
	
	public void rememberValueControlTypeForVisual(VisualComponent visual,
		Class valueControlClass, Class valueType)
	{
		valueFieldTypes.put(valueControlClass.getName() + ":" + visual.getNodeId(),
			valueType);
	}
	
	
	public Class retrieveRememberedTypeForVisual(VisualComponent visual,
		Class valueControlClass)
	{
		return valueFieldTypes.get(valueControlClass.getName() + ":" + visual.getNodeId());
	}
	
	
	public void clearValueControlTypeForVisual(VisualComponent visual,
		Class valueControlClass)
	{
		valueFieldTypes.remove(valueControlClass.getName() + ":" + visual.getNodeId());
	}
	
	
	public boolean containsUnsavedFieldValues() { return valueControlFieldValues.size() > 0; }
	
	
	public void clearFieldValues()
	{
		valueControlFieldValues.clear();
	}
	
	
	public void clearValueFieldTypes()
	{
		valueFieldTypes.clear();
	}


	public boolean controlsForVisualAreVisible(VisualComponent visual,
		Class valueControlClass)
	{
		Boolean b = showValueControls.get(valueControlClass.getName() + ":" + visual.getNodeId());
		if (b == null) return true;
		return b;
	}


	/** ********************************************************************
	 * Add (or remove) the AttributeValueControl for the specified Visual. If the Visual
	 * depicts a Node that has child Attributes, add a VisibilityControl and
	 * reuest it to show its ValueControls. The VisibilityControl will
	 * call addNode for each child Attribute.
	 */
	 
	public void addOrRemoveAttributeControls(boolean add, VisualComponent visual) // Done
	throws
		Exception
	{
		addOrRemoveControls(add, visual, getAttrVisControlFactory());
	}


	public void deleteControls(String nodeId) { removeControls(nodeId); }
	

	public boolean controlsForAttrAreVisible(VisualComponent visual)
	{
		return controlsForVisualAreVisible(visual, AttributeValueControl.class);
	}
		
		
	public VisibilityControl getVisibilityControl(VisualComponent visual, Class visControlType)
	{
		Component[] comps = getComponents();
		for (Component comp : comps)
		{
			if (visControlType.isAssignableFrom(comp.getClass()))
			{
				VisibilityControl vc = (VisibilityControl)comp;
				if (vc.getVisual() == visual) return vc;
			}
		}
		
		return null;
	}
	
	
	public VisibilityControl getVisibilityControl(String visualId, Class visControlType)
	{
		Component[] comps = getComponents();
		for (Component comp : comps)
		{
			if (visControlType.isAssignableFrom(comp.getClass()))
			{
				VisibilityControl vc = (VisibilityControl)comp;
				if (vc.getVisual().getNodeId().equals(visualId)) return vc;
			}
		}
		
		return null;
	}
	
	
	public void showControls(ValueControlFactory valueControlFactory, Class controlledVisualType,
		VisibilityControlFactory visControlFactory)
	{
		// Iterate over each Visual in the View.
		List<VisualComponent> ovs = getViewPanel().getOutermostVisuals();
		for (VisualComponent ov : ovs ) try
		{
			//addOrRemoveControls(true, ov, valueControlFactory, controlledVisualType);
			addOrRemoveControls(true, ov, visControlFactory);
		
			if (! (ov instanceof Container)) continue;
			Component[] children = ((Container)ov).getComponents();
			
			for (Component child : children)
			{
				if (! (child instanceof VisualComponent)) continue;
				
				//addOrRemoveControls(true, (VisualComponent)child,
				//	valueControlFactory, controlledVisualType);
				addOrRemoveControls(true, (VisualComponent)child,
					visControlFactory);
			}
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		
		((JComponent)(getViewPanel().getDisplayArea())).repaint();
	}
		
		
	public void showControls(Class controlType, boolean show)
	{
		removeAllControls();
		
		if (AttributeValueControl.class.isAssignableFrom(controlType))
		{
			this.showAttrControls = show;
			
			if (! show) return;
			
			showControls(getValueControlFactory(AttributeValueControl.class), AttributeVisual.class,
				getVisibilityControlFactory(AttributeVisibilityControl.class));
		}
		else throw new RuntimeException("Unrecognized Control type");
	}
	

	public Set<ValueControl> getValueControls(Class valueControlType)
	{
		Set<ValueControl> controls = new HashSet<ValueControl>();
		Component[] comps = getComponents();
		for (Component comp : comps)
		{
			if (valueControlType.isAssignableFrom(comp.getClass()))
			{
				ValueControl vc = (ValueControl)comp;
				controls.add(vc);
			}
		}
		
		return controls;
	}
	
	
	public void removeControls(VisualComponent visual)
	{
		Set<VisibilityControl> viscs = getVisibilityControls(visual);
		for (VisibilityControl visc : viscs)
		{
			visc.getFactory().unmakeVisibilityControl(visc);
			remove(visc);
		}
		
		Set<ValueControl> valueControls = getValueControls(visual);
		for (ValueControl vc : valueControls)
		{
			uncreateValueControl(vc);
			remove(vc);
		}
	}
	
	
	public void removeControls(String nodeId)
	{
		Set<VisualComponent> visuals;
		try { visuals = identifyVisualComponents(nodeId); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		for (VisualComponent visual : visuals) removeControls(visual);
		
		// Delete the Controls that do not have Visuals in this ControlPanel.
		Component[] comps = getComponents();
		for (Component comp : comps)
		{
			if (comp instanceof VisibilityControl)
			{
				VisibilityControl visc = (VisibilityControl)comp;
				if (visc.getVisual().getNodeId().equals(nodeId))
				{
					unmakeVisibilityControl(visc);
					remove(visc);
				}
			}
			else if (comp instanceof ValueControl)
			{
				ValueControl valc = (ValueControl)comp;
				if ((valc.getVisual().getNodeId().equals(nodeId)) && (valc.getParent() != null))
				//if (valc.getVisibilityControl() == null)
				{
					uncreateValueControl(valc);
					remove(valc);
				}
			}
		}
	}


	public void removeAllControls()
	{
		showValueControls.clear();
		removeAll();
	}
	
	
	/**
	 * From interface: Refresh the location of the specified Visual, as located
	 * within this ModelScenario Visual. The specified Visual must be owned by
	 * this ModelScenarioVisual.
	 * 
	 * Note that <VisualFactory>.makeVisual calls VisualComponent.setLocation(),
	 * which delegates to this method in order to set the location if the Visual
	 * is owned by this ControlPanel.
	 *
	 * The Visual, which is owned by the ControlPanel, should be placed over the
	 * Visual that depicts its Node's parent. (The Visual for the parent is owned
	 * by another Visual.) The Visual should be offset so that it does not fully
	 * overlap the parent's Visual. (See the slides "ValueControl Location" and
	 * "Location of Child ValueControls".)
	 * 
	 */
	
	public void setLocation(VisualComponent visual)
	{
		// Integrity checks.
		
		if (! (visual instanceof Component)) throw new RuntimeException(
			"Visual " + visual.getFullName() + " is not an AWT Component");
		
		if (((Component)visual).getParent() != this) throw new RuntimeException(
			"Visual " + visual.getFullName() + " is not owned by this ControlPanel");
		
		try
		{
			// Set Visual's location.
			
			Dimension offset = computeNestOffset(visual);
			offset.width += 20;
			offset.height += 20;
			
			java.awt.Point p = getViewPanel().getDisplayArea().getLocationOfVisual(visual);
			((Component)visual).setLocation(p);
			// The Node might be a child of a Node in this DisplayArea, so
			// its location will be relative to its parent.
			
			
			if (visual instanceof AWTComponent)
				((AWTComponent)visual).setLocation(p.x + offset.width, p.y + offset.height);
			
			
			// Set Visual's Control location.
			
			Set<ValueControl> controls = getValueControls(visual);
			for (ValueControl control : controls) control.setLocation();
			
			
			// Recursively call setSocation of the Visual's child Visuals.
			
			String[] childIds = visual.getNodeSer().getChildNodeIds();
			for (String childId : childIds)
			{
				Set<VisualComponent> childVisuals = identifyVisualComponents(childId);
				for (VisualComponent childVisual : childVisuals) setLocation(childVisual);
			}
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
		}
	}


	/**
	 * From interface:
	 * Perform any required updates to Components in this ControlPanel in
	 * response to the movement of a Visual in the Domain View underneath.
	 * The Visual is not owned by this ControlPanel.
	 * 
	 * Thus, we must move the Visual's Controls as well as all nested Visuals
	 * and all nested Controls. Do this by delegating it to setLocation(VisualComponent).
	 */
	 
	public void notifyVisualMoved(VisualComponent visual)
	{
		// Set location of the Visual's VisibilityControl.
		
		Set<VisibilityControl> viscons = getVisibilityControls(visual);
		for (VisibilityControl viscon : viscons) viscon.setLocation();
		
		
		// If the Visual has a ValueControl (case 1 from slide "Location of 
		// Child ValueControls"), set its location.
		
		Set<ValueControl> valcons = getValueControls(visual);
		for (ValueControl valcon : valcons) valcon.setLocation();
		
		
		// Call setLocation(VisualComponent) on each immediate child Visual that
		// is in this ControlPanel.
		
		String[] childIds = visual.getNodeSer().getChildNodeIds();
		for (String childId : childIds) try
		{
			if (childId == null) throw new RuntimeException(
				"childId is null, for Visual '" + visual.getFullName() + "'");
			
			Set<VisualComponent> childVisuals = identifyVisualComponents(childId);
			for (VisualComponent childVisual : childVisuals)
			{
				if ((childVisual instanceof ScenarioVisual) || 
					(childVisual instanceof ScenarioSetVisual)) continue;
				setLocation(childVisual);
			}
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
		}
	}
	
	
	public void notifyVisualRemoved(VisualComponent visual)
	{
		Set<VisibilityControl> viscons = getVisibilityControls(visual);
		Set<VisibilityControl> visconsCopy = new HashSet<VisibilityControl>(viscons);
		for (VisibilityControl viscon : visconsCopy) remove(viscon);
	}
	

	public void setInfoStrip(InfoStrip infoStrip) { this.infoStrip = infoStrip; }
	
	
	public InfoStrip getInfoStrip() { return this.infoStrip; }
	
	
	public ScenarioSelector getScenarioSelector() { return controlPanelSelector; }
	
	
	public boolean attributeControlsAreVisible() { return showAttrControls; }
	
	
  /* ***********************************************************************
   * From java.awt.Component and JComponent.
   */
	
	public Dimension getPreferredSize()
	{
		if (this.getParent() == null) throw new RuntimeException(
			this.getName() + " (" + this.getClass().getName() + ") has no parent.");
		
		return this.getParent().getSize();
	}


  /* ***********************************************************************
   * Protected methods.
   */
	
	
	protected abstract AttributeValueControlFactory getAttributeValueControlFactory();
	
	
	protected abstract AttributeVisibilityControlFactory getAttrVisControlFactory();
	
	
	/**
	 * Compute the x,y offset of this ValueControl relative to the first ValueControl.
	 * (See slide "Location of Child ValueControls".)
	 */
	
	public java.awt.Dimension computeNestOffset(VisualComponent visual)
	throws
		ParameterError
	{
		int[] seqNoRef = new int[1];
		int nestLevel = getNestLevel(visual, seqNoRef);
		int xOffset = nestLevel * NestIndentation;
		
		int h = visual.getHeight();
		int yOffset = (h + SpacingBetweenControls) * seqNoRef[0];
		return new java.awt.Dimension(xOffset, yOffset);
	}
	
	
	/**
	 * Compute the number of levels of indentation that the specified Visual should
	 * have. Also, return in seqNoRef[0] the ordinal number of the Visual
	 * (beginning with 0) relative to all of the Visuals that have ValueControls,
	 * including nested ones. (See the slide "Location of Child ValueControls".)
	 */
	 
	protected int getNestLevel(VisualComponent visual, int[] seqNoRef)
	throws
		ParameterError
	{
		VisualComponent vis = visual;
		int level = 0;
		seqNoRef[0] = 0;
		for (;;)  // each nest level, working outward.
		{
			// If vis is not owned by the ControlPanel, then we are at the
			// outermost level.
			if (vis.getParentObject() != this) return level;
			
			String parentId = vis.getNodeSer().getParentNodeId();
			if (parentId == null) throw new RuntimeException("No parent Node Id");
			
			VisualComponent parentVisual = identifyVisualComponent(parentId);

			if (parentVisual == null)
				parentVisual = viewPanel.identifyVisualComponent(parentId);
			
			if (parentVisual == null) throw new RuntimeException(
				"Could not find parent Visual");
			
			level++;
			
			vis = parentVisual;
			seqNoRef[0] += (vis.getSeqNo() - 1);
		}
	}


	/** ********************************************************************
	 * Return the AttributeVisuals of the View whose values are depicted in
	 * this ControlPanel.
	 */
	 
	protected Set<AttributeVisual> getAttributeVisualsFor(VisualComponent vis)
	{
		Set<AttributeVisual> attrViss = new HashSet<AttributeVisual>();
		Set<ValueControl> vcs = getValueControls(AttributeValueControl.class);
		for (ValueControl vc : vcs)
		{
			VisualComponent visual = vc.getVisual();
			if (visual instanceof AttributeVisual)
				if (visual.getNodeSer().getParentNodeId().equals(vis.getNodeId()))
					attrViss.add((AttributeVisual)visual);
		}
		
		return attrViss;
	}


	protected ValueControl getValueControl(VisualComponent visual, Class valueControlType)
	{
		Component[] comps = getComponents();
		for (Component comp : comps)
		{
			if (valueControlType.isAssignableFrom(comp.getClass()))
			{
				ValueControl vc = (ValueControl)comp;
				if (vc.getVisual() == visual) return vc;
			}
		}
		
		return null;
	}
	
	
	protected ValueControl getValueControl(String nodeId, Class valueControlType)
	{
		Component[] comps = getComponents();
		for (Component comp : comps)
		{
			if (valueControlType.isAssignableFrom(comp.getClass()))
			{
				ValueControl vc = (ValueControl)comp;
				if (vc.getVisual().getNodeId().equals(nodeId)) return vc;
			}
		}
		
		return null;
	}
	
	
	protected Set<ValueControl> getValueControls(VisualComponent visual)
	{
		Set<ValueControl> controls = new HashSet<ValueControl>();
		Component[] comps = getComponents();
		for (Component comp : comps)
		{
			if (comp instanceof ValueControl)
			{
				ValueControl vc = (ValueControl)comp;
				if (vc.getVisual() == visual) controls.add(vc);
			}
		}
		
		return controls;
	}
	
	
	protected AttributeValueControl getAttributeControl(AttributeVisual visual)
	{
		return (AttributeValueControl)(getValueControl(visual, AttributeValueControl.class));
	}
	
	
	protected AttributeValueControl getAttributeControl(String nodeId)
	{
		return (AttributeValueControl)(getValueControl(nodeId, AttributeValueControl.class));
	}
	
	
	protected Set<VisibilityControl> getVisibilityControls(VisualComponent visual)
	{
		Set<VisibilityControl> controls = new HashSet<VisibilityControl>();
		Component[] comps = getComponents();
		for (Component comp : comps)
		{
			if (comp instanceof VisibilityControl)
			{
				VisibilityControl vc = (VisibilityControl)comp;
				if (vc.getVisual() == visual) controls.add(vc);
			}
		}
		
		return controls;
	}
	
	
	/** ********************************************************************
	 * Create an AttributeVisibilityControl for the specified Visual. The
	 * 'showInitially' argument indicates whether the ValueControls managed
	 * by the VisibilityControl should be created in a visible state.
	 * Instantiate the Attribute Visuals if necessary.
	 */
	 
	protected VisibilityControl createVisibilityControl(VisualComponent vj,
		boolean showInitially, VisibilityControlFactory visControlFactory)
	{
		VisibilityControl vc =
			visControlFactory.makeVisibilityControl(vj, showInitially);
		vc.setLocation();
		add(vc);
		return vc;
	}
	
	
	/**
	 * Also removes each ValueControl, and each corresponding Visual
	 * owned by this ControlPanel.
	 */
	 
	protected void uncreateVisibilityControl(VisibilityControl vc)
	{
		vc.showValueControls(false);
		remove(vc);
	}
	

	/**
	 * Add or remove all Controls for the Visual and its children.
	 */
	 
	protected void addOrRemoveControls(boolean add, VisualComponent visual,
		VisibilityControlFactory visControlFactory)
	throws
		Exception
	{
		if (visControlFactory == null) throw new RuntimeException("visControlFactory is null");
		
		VisibilityControl visControl = 
			getVisibilityControl(visual, visControlFactory.getVisibilityControlClass());
		
		if (visControl != null) uncreateVisibilityControl(visControl);
			// Also removes each ValueControl, and each corresponding Visual
			// owned by this ControlPanel.
		
		if (visual.getAsIcon() && add)
		{
			this.createVisibilityControl(visual, 
				controlsForVisualAreVisible(visual, 
					visControlFactory.getValueControlFactory().getValueControlClass()),
				visControlFactory);
		}
	}
}

