/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.help.*;
import expressway.gui.ClientGlobalValues;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.datatransfer.*;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class ViewURLButton extends JButton
{
	public ViewURLButton(final NodeView view)
	{
		super((ButtonIcons.URLButtonIcon));
		setToolTipText("Copy a URL for this view to the system clipboard");
		
		addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// Display popup, giving user a choice of an Expressway URL
				// or a Web URL.
				
				JPopupMenu popup = new JPopupMenu();
				HelpfulMenuItem mi = new HelpfulMenuItem("Get Web URL", false,
					"Copy a URL to your system clipboard. The URL can be embedded " +
					"in Web pages as hyperlinks, or as the content URL of a frame. " +
					"This enables you to embed Expressway views in Web pages.");
				mi.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e2)
					{
						JPopupMenu popup2 = new JPopupMenu("Choose Format");
						
						String[] descriptions = new String[4];
						String[] formats;
						try { formats = view.getAvailableHTTPFormats(descriptions); }
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(ViewURLButton.this,
								"While getting available HTTP formats",
								"While getting available HTTP formats", ex);
							return;
						}
						
						int i = 0;
						for (final String format : formats)
						{
							HelpfulMenuItem mi2 = new HelpfulMenuItem(format, false,
								descriptions[i++]);
							
							mi2.addActionListener(new ActionListener()
							{
								public void actionPerformed(ActionEvent e3)
								{
									String serverName = 
										view.getClientMainApplication().getModelEngineNetworkPath();
									int port = view.getClientMainApplication().getHTTPPort();

									String urlString = URLHelper.createWebURLString(
										serverName, 
										Integer.toString(port),
										format, 
										new String[] { view.getOutermostNodeId() }
										);
									
									// Place the URL in the system clipboard.
									StringSelection stringSelection = new StringSelection(urlString);
									Clipboard clipboard = 
										ViewURLButton.this.getToolkit().getSystemClipboard();
									clipboard.setContents(stringSelection, stringSelection);
								}
							});
							
							popup2.add(mi2);
						}
						
						popup2.show(ViewURLButton.this, getX() + getWidth(), getY());
					}
				});
				
				popup.add(mi);
				
				mi = new HelpfulMenuItem("Get Expressway URL", false,
					"Copy a URL to your system clipboard. The URL can be embedded " +
					"as hyperlinks in Expressway text elements that permit " +
					"HTML content, such as an element description. Clicking " +
					"on such a hyperlink will open that view in the Expressway " +
					"client program.");
				mi.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e2)
					{
						String serverName = 
							view.getClientMainApplication().getModelEngineNetworkPath();
						int port = view.getClientMainApplication().getHTTPPort();
						
						String urlString = URLHelper.createExpresswayURLString(
							view.getViewType(), serverName, Integer.toString(port),
							view.getOutermostNodeId());
						
						// Place the URL in the system clipboard.
						StringSelection stringSelection = new StringSelection(urlString);
						Clipboard clipboard = 
							ViewURLButton.this.getToolkit().getSystemClipboard();
						clipboard.setContents(stringSelection, stringSelection);
					}
				});
				
				popup.add(mi);
				
				popup.show(ViewURLButton.this, getX() + getWidth(), getY());
			}
		});
	}
}

