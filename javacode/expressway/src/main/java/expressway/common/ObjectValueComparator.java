/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.MathException;
import expressway.common.ClientModel.*;
import expressway.common.VisualComponent.*;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.util.Set;
import expressway.generalpurpose.TreeSetNullDisallowed;


/**
 * Compare two values that might be of different types but that are semantically
 * equivalent, or that might be null.
 */
 
public class ObjectValueComparator
{
	public static boolean compare(Object a, Object b)
	{
		// Check if one of the values is null.
		if ((a == null) || (b == null)) return (a == b);
		
		// Check if both are Number.
		if ((a instanceof Number) && (b instanceof Number))
		{
			//if (! (((Number)a).doubleValue() == ((Number)b).doubleValue())) 
			//	{ (new Exception("NOT EQUAL: " + a + ", " + b)).printStackTrace(); }
			
			return ((Number)a).doubleValue() == ((Number)b).doubleValue();
		}
		
		// Not numeric or null.
		return a.equals(b);
	}
}

