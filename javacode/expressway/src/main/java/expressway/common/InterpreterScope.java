/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


/**
 * Base class for any symbol table used by an ExpressionInterpreter.
 */
 
public interface InterpreterScope
{
	public interface ClassScope extends InterpreterScope
	{
		/**
		 * Search the ClassLoader for the specified Java package.
		 */
		 
		Package findJavaPackage(String name);
	}
}

