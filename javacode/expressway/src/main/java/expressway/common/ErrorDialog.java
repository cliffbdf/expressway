/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import expressway.generalpurpose.*;


public class ErrorDialog extends JOptionPane
{
	private static final boolean Debug = true;
	
	private static final Font smallFont = new Font(Font.SANS_SERIF, Font.PLAIN, 8);
	
	private final JButton submitButton;
	
	private JDialog dialog;
	
	
	public ErrorDialog(final Object message, final Throwable t)
	{
		super("<html>" + HTMLEncoder.stringToHTMLString(message.toString()) + "</html>",
			JOptionPane.ERROR_MESSAGE);
		
		JLabel submitButtonLabel = new JLabel(
			"If you think that this error is an error in Expressway,\n" +
			"please click 'Send to Expressway'");
			
		submitButtonLabel.setFont(smallFont);
			
		add(submitButtonLabel);
		add(submitButton = new JButton("Send to Expressway"));
		
		submitButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// Construct a String representation of the stack trace.
				
				String nestedTraces = "";
				
				if (t != null)
				{
					Throwable t2 = t;
					for (;;)
					{
						nestedTraces += t.getClass().getName() + ": " +
							t.getMessage() + "\n";
							
						StackTraceElement[] traces = t2.getStackTrace();
						
						for (StackTraceElement trace : traces)
						{
							nestedTraces += trace.toString() + "\n";
						}
						
						t2 = t2.getCause();
						if (t2 == null) break;
						
						nestedTraces += "Caused by:\n";
					}
				}
				
				
				// POST the information to the bug reporting website.
				
				HTTPPoster.postAsync(
					ReleaseInfo.BugReportURL,
					"FirstName", "Expressway",
					"LastName", "",
					"formmail_mail_email", "bugs@expresswaysolutions.com",
					"Country", "",
					"BugInfo", nestedTraces
					);
				
				submitButton.setEnabled(false);
			}
		});
	}
	
	
	public JDialog createDialog(Component parentComponent, String title)
	{
		return (this.dialog = super.createDialog(parentComponent, title));
	}
		
		
	JDialog getDialog() { return this.dialog; }
	
	
	/**
	 * Create and display a modal error dialog.
	 */
	 
	public static void showErrorDialog(Component parentComponent, Object message)
	{
		ErrorDialog pane = new ErrorDialog(message, null);
		JDialog dialog = pane.createDialog(parentComponent, "Error");
		dialog.show();
	}
	
	
	/**
	 * Create and display a modal error dialog that has a button for reporting the
	 * error to Expressway Solutions.
	 */
	 
	public static void showReportableDialog(Component parentComponent, Object message, 
		String title, Throwable t)
	{
		if (Debug) GlobalConsole.printStackTrace(t);
		
		ErrorDialog pane = new ErrorDialog(message, t);
		JDialog dialog = pane.createDialog(parentComponent, title);
		dialog.show();
	}
	
	
	public static void showReportableDialog(Component parentComponent, String title,
		Throwable t)
	{
		showReportableDialog(parentComponent, ThrowableUtil.getAllMessages(t), title, t);
	}
	
	
	public static void showReportableDialog(Component parentComponent, Throwable t)
	{
		showReportableDialog(parentComponent, ThrowableUtil.getAllMessages(t), "Error", t);
	}
	
	
	/**
	 * Create and display a modal error dialog that (in Debug mode) also prints
	 * the stack trace to the console.
	 */
	 
	public static void showThrowableDialog(Component parentComponent, Object message, 
		String title, Throwable t)
	{
		if (Debug) GlobalConsole.printStackTrace(t);
		
		JOptionPane pane = new JOptionPane(
			"<html>" + HTMLEncoder.stringToHTMLString(message.toString()) +
			"</html>", JOptionPane.ERROR_MESSAGE);
		JDialog dialog = pane.createDialog(parentComponent, title);
		dialog.show();
	}
	
	
	public static void showThrowableDialog(Component parentComponent, String title,
		Throwable t)
	{
		showThrowableDialog(parentComponent, ThrowableUtil.getAllMessages(t), title, t);
	}
	
	
	public static void showThrowableDialog(Component parentComponent, Throwable t)
	{
		showThrowableDialog(parentComponent, ThrowableUtil.getAllMessages(t), "Error", t);
	}
}

