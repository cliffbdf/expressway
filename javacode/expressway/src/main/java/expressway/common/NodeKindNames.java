/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

public class NodeKindNames
{
	public static String Action = "Action";
	public static String Activity = "Activity";
	public static String AttributeStateBinding = "AttributeStateBinding";
	public static String Choice = "Choice";
	public static String ClassificationAttribute = "ClassificationAttribute";
	public static String ClassificationDomainMotifDef = "ClassificationDomainMotifDef";
	public static String ClassificationDomain = "ClassificationDomain";
	public static String Classification = "Classification";
	public static String Compensator = "Compensator";
	public static String Conduit = "Conduit";
	public static String ConstantValue = "ConstantValue";
	public static String Constraint = "Constraint";
	public static String DecisionAttribute = "DecisionAttribute";
	public static String DecisionDomainMotifDef = "DecisionDomainMotifDef";
	public static String DecisionDomain = "DecisionDomain";
	public static String DecisionFunction = "DecisionFunction";
	public static String DecisionPoint = "DecisionPoint";
	public static String DecisionScenario = "DecisionScenario";
	public static String DecisionScenarioSet = "DecisionScenarioSet";
	public static String Decision = "Decision";
	public static String Delay = "Delay";
	public static String Dependency = "Dependency";
	public static String Derives = "Derives";
	public static String DirectedRelation = "DirectedRelation";
	public static String Discriminator = "Discriminator";
	public static String DomainType = "DomainType";
	public static String DomainTypeAttribute = "DomainTypeAttribute";
	public static String DoubleExpression = "Numeric Expression";
	public static String Function = "Function";
	public static String Generator = "Value Generator";
	public static String HierarchyAttribute = "HierarchyAttribute";
	public static String HierarchyDomain = "HierarchyDomain";
	public static String HierarchyDomainMotifDef = "HierarchyDomainMotifDef";
	public static String HierarchyScenario = "HierarchyScenario";
	public static String HierarchyScenarioSet = "HierarchyScenarioSet";
	public static String Max = "Max";
	public static String MenuItem = "MenuItem";
	public static String MenuTree = "MenuTree";
	public static String MenuTreeAttribute = "MenuTreeAttribute";
	public static String ModelAttribute = "ModelAttribute";
	public static String ModelDomainMotifDef = "ModelDomainMotifDef";
	public static String ModelDomain = "ModelDomain";
	public static String ModelScenario = "ModelScenario";
	public static String ModelScenarioSet = "ModelScenarioSet";
	public static String Modulator = "Modulator";
	public static String NamedReference = "NamedReference";
	public static String NamedReferenceAttribute = "NamedReferenceAttribute";
	public static String CrossReference = "NamedReference.CrossReference";
	public static String NotExpression = "Not-Expression";
	public static String OneWay = "OneWay";
	public static String Parameter = "Parameter";
	public static String Port = "Port";
	public static String PrecursorRelationship = "PrecursorRelationship";
	public static String PredefinedEvent = "PredefinedEvent";
	public static String PulseGenerator = "Event Generator";
	public static String RequirementAttribute = "RequirementAttribute";
	public static String RequirementDomainMotifDef = "RequirementDomainMotifDef";
	public static String RequirementDomain = "RequirementDomain";
	public static String Requirement = "Requirement";
	public static String SimRunSet = "SimRunSet";
	public static String SimulationRun = "SimulationRun";
	public static String State = "State";
	public static String StrategyAttribute = "StrategyAttribute";
	public static String StrategyDomainMotifDef = "StrategyDomainMotifDef";
	public static String StrategyDomain = "StrategyDomain";
	public static String Strategy = "Strategy";
	public static String Summation = "Summation";
	public static String Sustain = "Sustain";
	public static String Switch = "Switch";
	public static String Tally = "Tally";
	public static String Terminal = "Terminal";
	public static String VariableAttributeBinding = "VariableAttributeBinding";
	public static String VariableGenerator = "VariableGenerator";
	public static String VariablePulseGenerator = "VariablePulseGenerator";
	public static String Variable = "Variable";
}

