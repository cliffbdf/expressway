/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import java.util.List;
import expressway.common.ModelAPITypes.*;


public class PathUtilities
{
	public static String assemblePath(List<String> pathParts)
	{
		String path = "";
		boolean firstTime = true;
		for (String part : pathParts)
		{
			if (firstTime) firstTime = false;
			else path += ".";
			
			path += part;
		}
		
		return path;
	}
	
	
	public static String assemblePath(String[] pathParts)
	{
		String path = "";
		boolean firstTime = true;
		for (String part : pathParts)
		{
			if (firstTime) firstTime = false;
			else path += ".";
			
			path += part;
		}
		
		return path;
	}
	
	
	/**
	 * Convert a full Node path into a relative path that is relative to the
	 * specified 'relativeTo' path, which must be fully specified. The resulting
	 * relative path will begin below the 'relativeTo' path.
	 */
	 
	public static String getRelativePath(String fullPath, String relativeTo)
	throws
		ParameterError
	{
		String[] fullPathParts = fullPath.split("\\.");
		
		String[] relativeToParts = relativeTo.split("\\.");
		
		return getRelativePath(fullPathParts, relativeToParts);
	}

	 
	public static String getRelativePath(String[] fullPathParts, String[] relativeToParts)
	throws
		ParameterError
	{
		String remainingParts = "";
		boolean pastEndOfRelativeTo = false;
		int partNo = 0;  // the first is 1
		for (String part : fullPathParts)
		{
			partNo++;
			if (! pastEndOfRelativeTo)
				if (partNo > relativeToParts.length) pastEndOfRelativeTo = true;
			
			if (pastEndOfRelativeTo)
				remainingParts += part;
			else
			{
				if (! part.equals(relativeToParts[partNo-1])) throw new ParameterError(
					"Mismatch: did not find '" + assemblePath(relativeToParts) +
					"' within '" + assemblePath(fullPathParts) + "'");
			}
		}
		
		return remainingParts;
	}
}

