/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.generalpurpose.StringUtils;
import expressway.common.ModelAPITypes.*;
import expressway.common.InterpreterScope.ClassScope;
import expressway.tools.analysis.DepthFirstAdapter;
import expressway.tools.node.*;
import java.util.*;
import java.lang.reflect.*;



/**
 * Override generated analyzer methods in the generated analyzer, to implement
 * expression interpreter functionality.
 *
 * Customizations of this class should extend the methods getPredefinedPathValue,
 * callBuiltinFunction, and callFunction(...).
 *
 * The Sable abstract syntax tree is defined in the "Abstract Syntax Tree" section
 * of the file "expr_grammar.sablecc".
 */
 
public abstract class ExpressionInterpreter extends DepthFirstAdapter
{
	private InterpreterScope.ClassScope classScope;
	private ExprStack exprStack = new ExprStack();
	private ScopeStack scopeStack = new ScopeStack(this);
	private Map<AFunccallExpr, Object> keeps = new HashMap<AFunccallExpr, Object>();
	
	
	public ExpressionInterpreter(InterpreterScope.ClassScope classScope)
	{
		this.classScope = classScope;
		scopeStack.pushClassScope(classScope);
	}
	
	
	public InterpreterScope.ClassScope getClassScope() { return classScope; }
	
	
	public double getFinalDoubleResult()
	{
		if (exprStack.getSize() != 1) throw new RuntimeException(
			"final expr stack size is not equal to 1");
		
		return exprStack.popDouble();
	}
	
	
	public Object getFinalObjectResult()
	{
		if (exprStack.getSize() != 1) throw new RuntimeException(
			"final expr stack size is not equal to 1");
		
		return exprStack.popObject();
	}
	
	
	/**
	 * Extend this method to recognize and return the value of pre-defined names
	 * that are specified by an identifier or a period-separated sequence of identifiers
	 * (i.e., a path).
	 */
	 
	protected double getPredefinedPathValue(List<String> pathParts)
	throws
		ModelContainsError,
		ElementNotFound
	{
		throw new ElementNotFound();
	}
	
	
	/**
	 * Override this method to perform function calls and return the result.
	 * Call the built-in function of the specified name, providing it with the
	 * arguments in 'argObjects'. If the function produces an error, throw
	 * a InterpretationError.
	 */
	 
	protected double callBuiltinFunction(String name, Object... argObjects)
	throws
		ModelContainsError,
		ElementNotFound
	{
		throw new ElementNotFound();
	}
	
	
	/**
	 * Determine the type of function call to perform, and perform it. Delegates
	 * calling built-in functions to callBuiltinFunction. Pushes the result onto
	 * the expression stack.
	 */
	 
	protected void callFunction(Object target, AFunccallExpr funccall)
	{
		LinkedList<PExpr> args = funccall.getFargs();
		Object[] argAr = new Object[args.size()];
		int i = args.size()-1;
		for (PExpr expr : args)
		{
			Object obj = exprStack.popObject();
			argAr[i--] = obj;
		}
		
		PKp kp = funccall.getKp();

		Object result;
		LinkedList<TId> funcPath = funccall.getIds();
		Class[] argTypes = new Class[argAr.length];
		int j = 0;
		for (Object arg : argAr)
		{
			Class argType = arg.getClass();
			if (Number.class.isAssignableFrom(argType)) argType = Double.TYPE;
			argTypes[j++] = argType;
		}
		
		Class targetType = (target == null ? null : target.getClass());
		Member member = scopeStack.findJavaMember(targetType, funcPath, argTypes);
		
		if (member == null)
		{
			if (target != null) throw new RuntimeException(
				"Function call may not have a target object");
			
			if (kp instanceof AYesKp) throw new RuntimeException(
				"Can only specify 'keep' for a constructor call");
			
			FuncDef funcDef = scopeStack.findFunctionDef(funcPath);
			if (funcDef == null)
			{
				// Did not find it: must be a built-in function.
				String funcName = ExprUtils.getPathString(funcPath);
				try { result = new Double(callBuiltinFunction(funcName, argAr)); }
				catch (RuntimeException rex) { throw rex; }
				catch (Exception ex) { throw new RuntimeException(
					"While calling '" + funcName + "'", ex); }
			}
			else
			{
				result = funcDef.call(this, scopeStack, exprStack, argAr);
			}
		}
		else if (member instanceof Method)
		{
			if (kp instanceof AYesKp) throw new RuntimeException(
				"Can only specify 'keep' for a constructor call");
			
			Method method = (Method)member;
			try { result = method.invoke(target, argAr); }
			catch (Exception ex) { throw new RuntimeException(
				"While calling function " + method.getName(), ex); }
		}
		else if (member instanceof Constructor)
		{
			if (target != null) throw new RuntimeException(
				"Constructor may not have a target object");
			
			result = keeps.get(funccall);
			if (result == null)
			{
				Constructor constructor = (Constructor)member;
				try { result = constructor.newInstance(argAr); }
				catch (Exception ex) { throw new RuntimeException(
					"While calling constructor " + constructor.getName(), ex); }
				if (kp instanceof AYesKp) keeps.put(funccall, result);
			}
		}
		else
			throw new RuntimeException(ExprUtils.getPathString(funcPath) + 
				" is not a Java method or constructor");
		
		//if (! (result instanceof Number)) throw new RuntimeException(
		//	"Non-numeric result for function call sequence");
		
		exprStack.push(result);
	}
	
	
	/* *************************************************************************
	 * The methods below should not need to change when specilizing this
	 * ExpressionInterpreter.
	 */
	
	
	public void outAAddExpr(AAddExpr node)
	{
		// Add the left and right values, and store the result on the expression
		// stack.
		
		double right = exprStack.popDouble();
		double left = exprStack.popDouble();
		exprStack.push(left + right);
	}
	
	
	public void outASubExpr(ASubExpr node)
	{
		double right = exprStack.popDouble();
		double left = exprStack.popDouble();
		exprStack.push(left - right);
	}
	
	
	public void outAMulExpr(AMulExpr node)
	{
		double right = exprStack.popDouble();
		double left = exprStack.popDouble();
		exprStack.push(left * right);
	}
	
	
	public void outADivExpr(ADivExpr node)
	{
		double right = exprStack.popDouble();
		double left = exprStack.popDouble();
		exprStack.push(left / right);
	}
	
	
	public void outAGtExpr(AGtExpr node)
	{
		double right = exprStack.popDouble();
		double left = exprStack.popDouble();
		exprStack.push(left > right);
	}
	
	
	public void outALtExpr(ALtExpr node)
	{
		double right = exprStack.popDouble();
		double left = exprStack.popDouble();
		exprStack.push(left < right);
	}
	
	
	public void outAGeExpr(AGeExpr node)
	{
		double right = exprStack.popDouble();
		double left = exprStack.popDouble();
		exprStack.push(left >= right);
	}
	
	
	public void outALeExpr(ALeExpr node)
	{
		double right = exprStack.popDouble();
		double left = exprStack.popDouble();
		exprStack.push(left <= right);
	}
	
	
	public void outAEqExpr(AEqExpr node)
	{
		double right = exprStack.popDouble();
		double left = exprStack.popDouble();
		exprStack.push(left == right);
	}
	
	
	public void outANeExpr(ANeExpr node)
	{
		double right = exprStack.popDouble();
		double left = exprStack.popDouble();
		exprStack.push(left != right);
	}
	
	
	public void outAIntegerExpr(AIntegerExpr node)
	{
		TNumber number = node.getNumber();
		String numberText = number.getText();
		double d = 0.0;
		try { d = Double.parseDouble(numberText); }
		catch (NumberFormatException nfe) { throw new RuntimeException(
			"Illegal number format: " + numberText); }
		
		exprStack.push(d);
	}
	
	
	public void outAFloatExpr(AFloatExpr node)
	{
		TNumber left = node.getLeft();
		String leftText = left.getText();
		
		TNumber fraction = node.getFraction();
		String fractionText = fraction.getText();
		
		double d = 0.0;
		try { d = Double.parseDouble(leftText + "." + fractionText); }
		catch (NumberFormatException nfe) { throw new RuntimeException(
			"Illegal number format: ." + leftText + "." + fractionText); }
		
		exprStack.push(d);
	}


	/**
	 * Override this method to identify identifiers in the context of the expression's
	 * evaluation.
	 */
	 
	public void outAIdentExpr(AIdentExpr node)
	{
		LinkedList<TId> ids = node.getIds();
		List<String> pathParts = new Vector<String>();
		for (TId id : ids) pathParts.add(id.getText());
		
		double pathValue = 0.0;
		try { pathValue = getPredefinedPathValue(ExprUtils.decodeEscapes(pathParts)); }
		catch (RuntimeException rex) { throw rex; }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		exprStack.push(pathValue);
	}
	
	
	public void outAFunccallseqExpr(AFunccallseqExpr node)
	{
		LinkedList<PExpr> exprs = node.getCalls();
		Object result = null;
		for (PExpr expr : exprs)
		{
			if (! (expr instanceof AFunccallExpr)) throw new RuntimeException(
				"Unexpected Node type: " + expr.getClass().getName());
			
			callFunction(result, (AFunccallExpr)expr);
			result = exprStack.popObject();
		}
		
		//if (! (result instanceof Number)) throw new RuntimeException(
		//	"Non-numeric result for function call sequence");
		
		exprStack.push(result);
	}
	
	
	public void outAIfexprExpr(AIfexprExpr node)
	{
		double exprRight = exprStack.popDouble();
		double exprLeft = exprStack.popDouble();
		boolean cond = exprStack.popBoolean();
		
		if (cond) exprStack.push(exprLeft);
		else exprStack.push(exprRight);
	}
	
	
	public void inACommaexprExpr(ACommaexprExpr node)
	{
		scopeStack.pushDeclScope();
	}
	
		
	public void outACommaexprExpr(ACommaexprExpr node)
	{
		scopeStack.popScope();
	}
	
	
	public void outANegExpr(ANegExpr node)
	{
		double expr = exprStack.popDouble();
		exprStack.push(-expr);
	}
	
	
	public void outAArexExpr(AArexExpr node)
	{
		List<PExpr> exprs = node.getExprs();
		List<Object> values = new LinkedList<Object>();
		for (PExpr expr : exprs)
		{
			Object value = exprStack.popObject();
			values.add(0, value);
		}
		
		Class commonClass = determineCommonClass(values);

		Object[] valueAr = null;
		if (Number.class.isAssignableFrom(commonClass))
		{
			try
			{
				double[] dAr = convertToPrimitiveDoubleArray(values);
				exprStack.push(dAr);
				return;
			}
			catch (ClassCastException ex) {} // ok
		}
		
		Object[] ar = (Object[])(java.lang.reflect.Array.newInstance(commonClass, values.size()));
		valueAr = values.toArray(ar);
		
		exprStack.push(valueAr);
	}
	
	
	/**
	 * Determine the most specific class that is common to all of the elements
	 * of the List. Returns one of { Integer.TYPE, Integer, Float.TYPE, Float, Double.TYPE, Double, Object }.
	 */
	 
	protected static Class determineCommonClass(List<Object> values)
	{
		for (Class c : ClassesToCompare)
		{
			boolean matchedAll = true;
			for (Object v : values)
			{
				if (! c.isAssignableFrom(v.getClass()))
				{
					matchedAll = false;
					break;
				}
			}
			
			if (matchedAll) return c;
		}
		
		return Object.class;
	}
	
	
	protected static double[] convertToPrimitiveDoubleArray(List<Object> values)
	throws
		ClassCastException
	{
		double[] doubleValues = new double[values.size()];
		int i = 0;
		for (Object v : values)
		{
			if (! (v instanceof Number)) throw new ClassCastException(
				"Cannot compute double value of a " + v.getClass().getName());
			
			double dv = ((Number)v).doubleValue();
			doubleValues[i++] = ((Number)v).doubleValue();
		}
		
		return doubleValues;
	}
	
	
	private static final Class[] ClassesToCompare =
	{
		Integer.TYPE, Integer.class, Float.TYPE, Float.class, Double.TYPE, Double.class
	};
	
	
	public void outAUseDecl(AUseDecl node)
	{
		LinkedList<TId> ids = node.getIds();
		try { scopeStack.addJavaUse(ids); }
		catch (ElementNotFound ex) { throw new RuntimeException(
			"While adding use clause for " + ExprUtils.getPathString(ids), ex); }
	}
	
	
	public void outAFuncdefDecl(AFuncdefDecl node)
	{
		scopeStack.addFuncDef(node);
	}
	
	
	public void outAArgnameArgdef(AArgnameArgdef node)
	{
	}
	
	
	public void outAYesKp(AYesKp node)
	{
	}
	
	
	public void outANoKp(ANoKp node)
	{
	}


	/**
	 * Override to prevent evaluation of the function's expression during
	 * traversal of the FuncdefDecl. Instead, it will be evaluated during
	 * traversal of each Funccall, in the context of the call's actual arguments.
	 *
	decl = {use} id*
		 | {funcdef} id argdef* expr;
	 */
	 
    public void caseAFuncdefDecl(AFuncdefDecl node)
    {
    	inAFuncdefDecl(node);
    	
        if(node.getId() != null)
        {
            node.getId().apply(this);
        }
        
        {
        	List<PArgdef> copy = new ArrayList<PArgdef>(node.getArgdefs());
        	for (PArgdef ad : copy)
        	{
        		ad.apply(this);
        	}
        }
        
        //if (node.getExpr() != null)
        //{
        //	node.getExpr().apply(this);
        //}
        
    	outAFuncdefDecl(node);
	}
}


class ExprUtils
{
	/**
	 * Translate character strings containing escapes (e.g., "%20") into their
	 * intended actual values.
	 */
	 
	public static List<String> decodeEscapes(List<String> pathParts)
	{
		List<String> newPathParts = new Vector<String>();
		
		for (String part : pathParts)
		{
			String decodedString = StringUtils.decodeEscapes(part);
			newPathParts.add(decodedString);
		}
		
		return newPathParts;
	}
	
	
	public static String getPathString(LinkedList<TId> path)
	{
		String string = "";
		boolean first = true;
		for (TId tid : path)
		{
			if (first) first = false;
			else string += ".";
			
			string += tid.getText();
		}
		
		return string;
	}


	/** Decompose the path into a class and a method name. The class is
		returned as the only element in the array 'classNameRef'. */
		
	public static String getMethodName(Class targetType, LinkedList<TId> path,
		Class[] classRef)
	throws
		ClassNotFoundException
	{
		TId last = path.getLast();
		if (last == null) throw new RuntimeException("Zero length path");
		
		String className = "";
		if (targetType == null) // Method must be static. The class name is the
			// part of the path up to but not including the last element in the List.
		{
			boolean first = true;
			for (TId id : path)
			{
				if (id == last)
				{
					try
					{
						targetType = Class.forName(className);
						break;
					}
					catch (ClassNotFoundException ex) {} // ok
				}
				
				if (first) first = false;
				else className += ".";
				className += id.getText();
			}
			
			if (targetType == null) targetType = Class.forName(className);
		}
		
		classRef[0] = targetType;
		return last.getText();
	}
}


class ScopeStack
{
	private List<InterpreterScope> scopes = new Vector<InterpreterScope>();
	private ExpressionInterpreter interpreter;
	
	
	ScopeStack(ExpressionInterpreter interpreter)
	{
		this.interpreter = interpreter;
	}
	
	
	public ClassScope pushClassScope(ClassScope scope)
	{
		scopes.add(0, scope);
		return scope;
	}
	
	
	public DeclInterpreterScope pushDeclScope()
	{
		DeclInterpreterScope scope = new DeclInterpreterScope();
		scopes.add(0, scope);  // most recent go at the beginning.
		return scope;
	}
	
	
	public FunccallInterpreterScope pushFunccallScope()
	{
		FunccallInterpreterScope scope = new FunccallInterpreterScope();
		scopes.add(0, scope);  // most recent go at the beginning.
		return scope;
	}
	
	
	public void popScope()
	{
		scopes.remove(0);
	}
	
	
	/** Add the specified path to the current scope. */
	public void addJavaUse(LinkedList<TId> path)
	throws
		ElementNotFound  // if the specified Java package is not found.
	{
		if (findJavaPackage(path) == null) throw new ElementNotFound(ExprUtils.getPathString(path));
		
		DeclInterpreterScope scope = getCurrentDeclScope();
		if (scope == null) throw new RuntimeException("No Decl scope to add use clause to");
		scope.addJavaPath(path);
	}
	
	
	/**
	 * Find the specified Java Method or Constructor among the packages defined
	 * in the nested InterpreterScopes, beginning with the most recent InterpreterScope.
	 * If not found, return null.
	 */
	 
	public Member findJavaMember(Class targetType, LinkedList<TId> path, Class... argTypes)
	{
		String memberFullName = ExprUtils.getPathString(path);
		
		Class[] classRef = new Class[1];
		String memberName;
		
		try { memberName = ExprUtils.getMethodName(targetType, path, classRef); }
		catch (ClassNotFoundException ex) { return null; }
		
		targetType = classRef[0];

		try
		{
			Method method = targetType.getMethod(memberName, argTypes);
			return method;
		}
		catch (NoSuchMethodException ex2) {} // ok
		
		try
		{
			Constructor constructor = targetType.getConstructor(argTypes);
			return constructor;
		}
		catch (NoSuchMethodException ex2) {} // ok
		
		for (InterpreterScope scope : this.scopes)  // from most recent first.
		{
			if (! (scope instanceof DeclInterpreterScope)) continue;
			DeclInterpreterScope dscope = (DeclInterpreterScope)scope;
			Member member = dscope.getJavaMember(targetType, path, argTypes);
			if (member != null) return member;
		}
		
		return null;
	}
	
	
	public void addFuncDef(AFuncdefDecl node)
	{
		getCurrentDeclScope().addFuncDef(node);
	}
	
	
	public FuncDef findFunctionDef(LinkedList<TId> path)
	{
		for (InterpreterScope scope : this.scopes)  // from most recent first.
		{
			if (! (scope instanceof DeclInterpreterScope)) continue;
			FuncDef funcDef = ((DeclInterpreterScope)scope).getFuncDef(path);
			if (funcDef != null) return funcDef;
		}
		
		return null;
	}
	
	
	public Object findSymbolValue(String name)
	{
		for (InterpreterScope scope : this.scopes)
		{
			if (! (scope instanceof FunccallInterpreterScope)) continue;
			FunccallInterpreterScope fscope = (FunccallInterpreterScope)scope;
			if (fscope.definesSymbol(name)) return fscope.getSymbolValue(name);
		}
		
		return null;
	}
	
	
	public InterpreterScope getCurrentScope() { return scopes.get(0); }
	
	
	public DeclInterpreterScope getCurrentDeclScope()
	{
		for (int i = 0;;)
		{
			if (i+1 > scopes.size()) break;
			InterpreterScope scope = scopes.get(i++);
			if (scope == null) break;
			if (scope instanceof DeclInterpreterScope) return (DeclInterpreterScope)scope;
		}
		
		return null;
	}
	
	
	public FunccallInterpreterScope getCurrentFunccallScope()
	{
		for (int i = 0;;)
		{
			if (i+1 > scopes.size()) break;
			InterpreterScope scope = scopes.get(i++);
			if (scope == null) break;
			if (scope instanceof FunccallInterpreterScope) return (FunccallInterpreterScope)scope;
		}
		
		return null;
	}
	
	
	/**
	 * Search the ClassLoader for the specified Java package.
	 */
	 
	protected Package findJavaPackage(LinkedList<TId> path)
	{
		return interpreter.getClassScope().findJavaPackage(ExprUtils.getPathString(path));
	}
}


class FuncDef
{
	private AFuncdefDecl funcdefDecl;
	
	FuncDef(AFuncdefDecl funcdefDecl)
	{
		this.funcdefDecl = funcdefDecl;
	}
	
	
	public String getFuncName() { return funcdefDecl.getId().getText(); }
	
	
	/**
	 * Call the user-defined function. A FunccallInterpreterScope is created for the call,
	 * containing a symbol entry for each formal argument, and the actual
	 * argument expression values are mapped to the corresponding formal argument
	 * symbols and stored in the InterpreterScope. The function declaration expression is
	 * then evaluated in the context of the FunccallInterpreterScope.
	 */
	 
	public Object call(ExpressionInterpreter exprInterpreter, ScopeStack scopeStack,
		ExprStack exprStack, Object[] argAr)
	{
		// Create and push a scope for the actual arguments, and add to the scope
		// a symbol for each formal argument.
		
		FunccallInterpreterScope fscope = scopeStack.pushFunccallScope();
		
		List<PArgdef> argdefs = funcdefDecl.getArgdefs();
		List<TId> ids = new ArrayList<TId>();
		for (PArgdef argdef : argdefs) ids.add(((AArgnameArgdef)argdef).getId());
		for (TId id : ids) // each formal
			fscope.addSymbol(id.getText()); // formal name
		

		// Evauate each actual argument.
    		
		int i = 0;
		for (Object arg : argAr)
		{
			// Pop the expression and assign the value to the corresponding
			// formal argument in the scope.
			
			fscope.setSymbolValue(ids.get(i++).getText() /* formal name */, arg);
		}
    		
    		
		// Evaluate the expression in the Funcdef, using the current scope (call
		// frame) for formal parameters.
		
		PExpr funcdefExpr = funcdefDecl.getExpr();
		funcdefExpr.apply(exprInterpreter);
		Object result = exprStack.popObject();
		
		
		// Pop the function call scope.
		
		scopeStack.popScope();
		
		return result;
	}
}


/**
 * A symbol table for a set of Java package and user-defined function declarations.
 */
 
class DeclInterpreterScope implements InterpreterScope
{
	private List<LinkedList<TId>> javaPaths = new Vector<LinkedList<TId>>();
	private List<FuncDef> funcdefs = new Vector<FuncDef>();
	
	
	public void addJavaPath(LinkedList<TId> path)
	{
		javaPaths.add(path);
	}
	
	
	/**
	 * Search each of the javaPaths in this scope, from the most recent first,
	 * until the specified Java Member is found. Return null if not found.
	 */
	 
	public Member getJavaMember(Class targetType, LinkedList<TId> path, Class... argTypes)
	{
		Class[] classRef = new Class[1];
		String methodName;
		try { methodName = ExprUtils.getMethodName(targetType, path, classRef); }
		catch (ClassNotFoundException ex) { return null; }
		
		targetType = classRef[0];
		
		
		// Search the target class.
		
		//Class[] argTypes2 = new Class[argTypes.length+1];
		//argTypes2[0] = targetType;
		//for (int i = 1; i < argTypes2.length; i++) argTypes2[i] = argTypes[i-1];		

		try { return targetType.getMethod(methodName, argTypes); }
		catch (NoSuchMethodException ex) {}
		
		try { return targetType.getConstructor(argTypes); }
		catch (NoSuchMethodException ex) {}
			
		return null;
	}
	
	
	/** Add a user-defined function definition. */
	
	public void addFuncDef(AFuncdefDecl node)
	{
		funcdefs.add(new FuncDef(node));
	}
	

	/** Find a user-defined function definition. */
	
	public FuncDef getFuncDef(LinkedList<TId> path)
	{
		String funcName = ExprUtils.getPathString(path);
		for (FuncDef def : funcdefs)
		{
			if (funcName.equals(def.getFuncName())) return def;
		}
		
		return null;
	}
}


/**
 * A call frame for the mapping between actual function call arguments and the
 * corresponding formal arguments.
 */
 
class FunccallInterpreterScope implements InterpreterScope
{
	private Map<String, Object> symbolValues = new HashMap<String, Object>();
	
	
	public void addSymbol(String name)
	{
		symbolValues.put(name, null);
	}
	
	
	public void setSymbolValue(String name, Object value)
	{
		symbolValues.put(name, value);
	}
	
	
	public boolean definesSymbol(String name)
	{
		return symbolValues.containsKey(name);
	}
	
	
	public Object getSymbolValue(String name)
	{
		return symbolValues.get(name);
	}
}


class ExprStack
{
	private List<Object> elements = new Vector<Object>();
	
	public int getSize() { return elements.size(); }
	
	
	public void push(int i)
	{
		push(new Integer(i));
	}
	
	
	public void push(boolean b)
	{
		elements.add(new Boolean(b));
	}
	
	
	public void push(double d)
	{
		push(new Double(d));
	}
	
	
	public void push(Object obj)
	{
		elements.add(obj);
	}
	
	
	public boolean popBoolean()
	{
		if (elements.size() == 0) throw new RuntimeException(
			"Stack is empty: canot pop");
		
		Object o = elements.remove(elements.size()-1);
		if (! (o instanceof Boolean)) throw new RuntimeException(
			"Expected a boolean value; found a " + o.getClass().getName() +
				" of value " + o);
		
		return ((Boolean)o).booleanValue();
	}
	
	
	public double popDouble()
	{
		if (elements.size() == 0) throw new RuntimeException(
			"Stack is empty: canot pop");
		
		Object o = elements.remove(elements.size()-1);
		if (! (o instanceof Number)) throw new RuntimeException(
			"Expected a numeric value; found a " + o.getClass().getName() +
				" of value " + o);
		
		return ((Number)o).doubleValue();
	}
	
	
	public Object popObject()
	{
		if (elements.size() == 0) throw new RuntimeException(
			"Stack is empty: canot pop");
		
		Object o = elements.remove(elements.size()-1);
		
		return o;
	}
}

