/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.ser.*;
import expressway.statistics.HistogramDomainParameters;
import expressway.statistics.TimeSeriesParameters;
import java.util.List;
import java.util.Vector;
import java.util.Date;
import java.util.Set;
import java.io.IOException;
import java.io.Serializable;
import javax.swing.ImageIcon;


/**
 * The remote interface that is made avaiable to all clients of all kinds.
 * Only general-purpose types (IDL-compatible) are used.
 *
 * These methods are assumed to be transactional, for implementations
 * that support multiple concurrent requests. That is, each method is assumed
 * to create and complete a transaction.
 */

public interface ModelEngineRemote
{


  /* ***************************************************************************
   * Lifecycle methods.
   */


	/** ************************************************************************
	 * Return the current date and time, indicating that the server is responding.
	 */
	
	String ping(String clientId)
	throws
		IOException;

		
	
	/** ************************************************************************
	 * If there might be changes that have not been written to persistent storage,
	 * then return true.
	 */
	 
	boolean hasUnflushedChanges();
	
	
	
	/** ************************************************************************
	 * Ensure that the entire database contents has been written to disk.
	 * A client should call this if the underlying database does not do this
	 * automatically, e.g., for the file-based implementation.
	 */
	
	void flush(String clientId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		


  /* ***************************************************************************
   * Methods that pertain to or affect the structure of a Domain or a
   * collection of Domains.
   */


	/** ************************************************************************
	 * Start the asynchronous processing of an Expressway XML command stream.
	 * Returns the callback ID of the parsing thread that is created.
	 */
	
	Integer parseXMLCharAr(String clientId, char[] charAr, PeerListener peerListener)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		BaseException,
		IOException;
		
	
	
	/** ************************************************************************
	 * Start the asynchronous processing of a JAR containing an Expressway XML
	 * command stream. Returns the callback ID of the parsing thread that is created.
	 */
	
	Integer processJARByteAr(String clientId, byte[] byteAr,
		PeerListener peerListener)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		BaseException,
		IOException;
		
	
	
	/** ************************************************************************
	 * Return an Expressway XML representation of the specified Node.
	 */

	String exportNodeAsXML(String clientId, String nodeId)
	throws
		ParameterError,
		IOException;
	
		
		
	DomainTypeSer[] getDomainTypes(String clientId)
	throws
		ParameterError,
		IOException;
		
		
	
	void createDomainForDomainType(String clientId, String id)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
		
		
	
	DomainTypeSer[] getBaseDomainTypes(String clientId)
	throws
		ParameterError,
		IOException;
		
		
	
	void createDomainType(String clientId, String baseDomainTypeId, String... motifDefIds)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
		
		
		
	void deleteDomainType(String clientId, String domainTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
		
		
	
	/**
	 * Return the MotifDefs that are compatible with the specified Domain type.
	 */
	 
	MotifDefSer[] getMotifsForBaseDomainType(String clientId, String baseDomainTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
		
		
		
	MotifDefSer[] getMotifsCompatibleWithDomainId(String clientId, String domainId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;

		
	
	DomainTypeSer[] getUserDefinedDomainTypes(String clientId)
	throws
		ParameterError,
		IOException;
		
		
		
	/**
	 * 
	 */
	 
	MotifDefSer createMotifForDomainType(String clientId, String domainBaseTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
	
	
	
	/** ************************************************************************
	 * Return the name of the client-side View that should be created by the
	 * client's ViewFactory to depict the specified Node in the context of its
	 * Domain. See ViewFactory. Seel also the method NodeView.getViewType.
	 */
	 
	String getViewType(String clientId, String nodeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
	
		
		
	/** ************************************************************************
	 * Return the name of the View class that should be used by the client to
	 * display a View of the specified Node. If returns null, then the client
	 * should call getViewType and decide which View class to use based on the
	 * View type that is returned. Generally, a Node will specify a View type
	 * only if a Motif has been created that defines its own View class for
	 * its Nodes.
	 */
	 
	String getViewClassName(String clientId, String nodeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
	
		
		
	/** ************************************************************************
	 * Retrieve the specified ModelDomain. If not found, return null.
	 */
	
	ModelDomainSer getModelDomain(String clientId, String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
	
	/** ************************************************************************
	 * Retrieve the specified DecisionDomain. If not found, return null.
	 */
	
	DecisionDomainSer getDecisionDomain(String clientId, String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
	
	/** ************************************************************************
	 * Retrieve a List of the names of all of the Domains known to this ModelEngine.
	 */
	
	List<String> getDomainNames(String clientId)
	throws
		IOException;
	
		
		
	/** ************************************************************************
	 * Retrieve a List of all of the Domains known to this ModelEngine.
	 */
	
	List<NodeDomainSer> getDomains(String clientId)
	throws
		ParameterError,
		IOException;
	
		
		
	/** ************************************************************************
	 * Retrieve a List of all of the Model Domains known to this ModelEngine.
	 */
	
	List<ModelDomainSer> getModelDomains(String clientId)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	/** ************************************************************************
	 * Retrieve a List of all of the Motifs known to this ModelEngine.
	 */
	 
	List<ModelDomainMotifDefSer> getModelDomainMotifs(String clientId)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
		
	/** ************************************************************************
	 * Retrieve a List of the Motifs used by the specified Domain. Must be a
	 * ModelDomain.
	 */
	 
	List<MotifDefSer> getMotifsUsedByDomain(String clientId, String domainId)
	throws
		ParameterError,
		IOException;
	

		
	/** ************************************************************************
	 * Return true if the specified Domain imports the specified Motif. Domain
	 * ust be a ModelDomain.
	 */
	 
	boolean domainUsesMotif(String clientId, String domainId, String motifId)
	throws
		ParameterError,
		IOException;
	

		
	/** ************************************************************************
	 * Import the specified Motif into the specified Domain. The Domain must be a
	 * ModelDomain.
	 */
	 
	void useMotif(String clientId, String domainId, String motifId, boolean yes)
	throws
		ParameterError,
		Warning,
		IOException;
	

		
	/** ************************************************************************
	 * Search all motif JARs for the specified resource.
	 */
	
	byte[] getMotifResource(String clientId, String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException;
	


	/** ************************************************************************
	 * Search all motif JARs for the specified Java class.
	 */
	
	byte[] getMotifClass(String clientId, String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException;
	


	/** ************************************************************************
	 * Retrieve a List of all of the Decision Domains known to this ModelEngine.
	 */
	
	List<DecisionDomainSer> getDecisionDomains(String clientId)
	throws
		ParameterError,
		IOException;
		
		
	
	/** ************************************************************************
	 * Create a new ModelDomain for this ModelEngine.
	 */

	ModelDomainSer createModelDomain(String clientId, String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException;

		

	/** ************************************************************************
	 * Create a new ModelDomain for this ModelEngine, and provide a unique
	 * name.
	 */

	ModelDomainSer createModelDomain(String clientId)
	throws
		ParameterError,
		IOException;
	
	
	
	/** ************************************************************************
	 * Create a new Motif for this ModelEngine.
	 */
	 
	ModelDomainMotifDefSer createModelDomainMotif(String clientId, String name,
		boolean useNameExactly)
	throws
		ParameterError,
		IOException;

		

	/** ************************************************************************
	 * Create a new Motif for this ModelEngine, and provide a unique name.
	 */
	 
	ModelDomainMotifDefSer createModelDomainMotif(String clientId)
	throws
		ParameterError,
		IOException;
	

		
	/** ************************************************************************
	 * Create a new ModelDomain that is a complete copy of the specified one, except
	 * for SimulationRuns. A unique name is created for the new Domain.
	 */

	ModelDomainSer createIndependentCopy(String clientId, String modelDomainId)
	throws
		ParameterError,
		ModelContainsError,
		IOException;
		
		
		
	/** ************************************************************************
	 * Create a new DecisionDomain for this ModelEngine.
	 */

	DecisionDomainSer createDecisionDomain(String clientId, String name, 
		boolean useNameExactly)
	throws
		ParameterError,
		IOException;

		
	
	/** ************************************************************************
	 * Create an AttributeStateBinding as specified. The State must be in a
	 * different ModelDomain than the Attribute.
	 */
	 
	AttributeStateBindingSer bindAttributeToState(String clientId, boolean confirm, 
		String attrId, String stateFullName)
	throws
		Warning,
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;
	
	
	/** ************************************************************************
	 * 
	 */
	 
	void deleteAttributeStateBinding(String clientId, boolean confirm, String bindingNodeId)
	throws
		Warning,
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;



	/** ************************************************************************
	 * 
	 */
	 
	void setStateBindingParameters(String clientId, boolean confirm, String domainOrScenarioId,
		Boolean type, TimePeriodType period)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;



	/** ************************************************************************
	 * Note: The bound Scenario Id may be null. This clears any existing binding.
	 * However, the boundDomainId may not be null.
	 */
	 
	void bindScenarioToScenarioForDomain(String clientId, boolean confirm, 
		String bindingScenarioId, String boundScenarioId, String boundDomainId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;


	
	/** ************************************************************************
	 * If the specified Attribute has no AttributeStateBinding, return null.
	 */
	 
	AttributeStateBindingSer getAttributeStateBinding(String clientId, String attrId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;
	
	
	/** ************************************************************************
	 * Return the Domains that own States to which Attributes of the specified
	 * Domain are bound.
	 */
	 
	ModelDomainSer[] getBoundDomains(String clientId, String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;



	/** ************************************************************************
	 * Return the Domains that own Attributes that are bound to States of 
	 * the specified Domain.
	 */
	 
	ModelDomainSer[] getBindingDomains(String clientId, String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;



	/** ************************************************************************
	 * 
	 */
	 
	ModelScenarioSer getBoundScenarioForBoundDomain(String clientId, 
		String bindingScenarioId, String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;



	/** ************************************************************************
	 * 
	 */
	 
	StateSer[] getStatesForModelDomain(String clientId, String modelDomainName)
	throws
		ParameterError,
		CannotObtainLock,
		IOException;


	
	/** ************************************************************************
	 * 
	 */
	 
	StateSer[] getTallyStatesForModelDomain(String clientId, String modelDomainName)
	throws
		ParameterError,
		CannotObtainLock,
		IOException;
		


	/** ************************************************************************
	 * The PersistentNode identified by parentId must implement PersistentListNode.
	 * If the argument 'nextNodeId' is null, the new Node is created as the last
	 * child of the ListNode. The new Node is initialized by cloning the data
	 * from the specified 'copiedNode', which may be null. If scenarioId is not null,
	 * then it is assumed that any trailing data values in 'nodeData' should be
	 * used to create Attributes with those values. This method should also
	 * ensure that the new Node has reasonable values for x,y location.
	 */
	 
	NodeSer createListNodeBefore(String clientId, boolean confirm, String parentId,
		String nextNodeId, String sceanrioId, String copiedNodeId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;


	
	/** ************************************************************************
	 * The PersistentNode identified by parentId must implement PersistentListNode.
	 * If the argument 'priorNodeId' is null, the new Node is created as the first
	 * child of the ListNode. Othersise, same as createListNodeBefore.
	 */
	 
	NodeSer createListNodeAfter(String clientId, boolean confirm, String parentId,
		String priorNodeId, String sceanrioId, String copiedNodeId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;
		


	/** ************************************************************************
	 * 
	 */
	 
	NodeSer cloneListNodeAfter(String clientId, boolean confirm, String nodeId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		InternalEngineError,
		IOException;
		
	
	
	/** ************************************************************************
	 * Return the class names of all of the Node interface types.
	 */

	String[] getInterfaceNodeKinds(String clientId)
	throws
		ParameterError,
		IOException;
	
	
		
	/** ************************************************************************
	 * An array of the class names of ModelElements that may be instantiated
	 * into the specified parent Node is returned. Entries for Templates are at
	 * the end.
	 */

	String[] getInstantiableNodeKinds(String clientId, String parentId)
	throws
		ParameterError,
		IOException;
	
	
	
	/** ************************************************************************
	 * An array of the HTML descriptions of the ModelElements that may be
	 * instantiated into the specified parent Node is returned. If a
	 * description begins with an @ symbol, the string following the @ is
	 * considered to be the name of a Web page (see class expressway.help.HelpfulMenuItem).
	 */

	String[] getInstantiableNodeDescriptions(String clientId, String parentId)
	throws
		ParameterError,
		IOException;
	
	
	
	/** ************************************************************************
	 * Create and instantiate a Model Element of the specified kind within the
	 * Model Element identified by the parent Id. The kind must be one of the
	 * kinds returned by getInstantiableNodeKinds(). The Element is
	 * instantiated at the specified x,y location within its parent.
	 * Note: Warn user if the Domain has SimulationRuns.
	 */

	ModelElementSer createModelElement(String clientId, boolean confirm, 
		String parentId, String kind,
		double x, double y)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;


	
	/** ************************************************************************
	 * Delete the specified Model Element.
	 * Note: Warn user if the Domain has SimulationRuns.
	 */
	 
	void deleteNode(String clientId, boolean confirm, String elementId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;

	
	
	/** ************************************************************************
	 * If this Node has a sibling above it, make this Node the last child of
	 * that sibling.
	 */

	void demoteListNode(String clientId, boolean confirm, String nodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;

	
	
	/** ************************************************************************
	 * If this Node has a parent, and if the parent is not a Domain, make this
	 * Node the parent's next sibling.
	 */

	void promoteListNode(String clientId, boolean confirm, String nodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
		

		
	/** ************************************************************************
	 * 
	 */
	 
	void createNamedReference(String clientId, boolean confirm, String domainId,
		String name, String desc, String fromTypeClassName, String toTypeClassName)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
		
		
		
	/** ************************************************************************
	 * 
	 */
	 
	void deleteNamedReference(String clientId, boolean confirm, String namedRefId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
		
		

	/** ************************************************************************
	 * Create a NamedReference.CrossReference between the two specified Nodes.
	 * The 'domainId' is the Domain that owns the specified NamedReference.
	 */
	 
	void createCrossReference(String clientId, boolean confirm, //String domainId,
		String namedRefName, String fromNodeId, String toNodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
	
		
		
	/** ************************************************************************
	 * 
	 */
	 
	void deleteCrossReference(String clientId, boolean confirm, String crossRefId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;

	
	
	/** ************************************************************************
	 * Get the "from" Nodes that are linked to the specified Node by the specified
	 * NamedReference. Do not include the Node itself.
	 */
	 
	NodeSer[] getNamedRefFromNodes(String clientId, String nodeId, String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	

	/** ************************************************************************
	 * Get the "to" Nodes that are linked to the specified Node by the specified
	 * NamedReference. Do not include the Node itself.
	 */
	 
	NodeSer[] getNamedRefToNodes(String clientId, String nodeId, String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	
	
	/** ************************************************************************
	 * Return the names of the NamedReferences that are owned by the specified
	 * Domain.
	 */
	 
	String[] getNamedReferences(String clientId, String domainId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
		
		
		
	/** ************************************************************************
	 * 
	 */
	 
	NamedReferenceSer getNamedReferenceForDomain(String clientId, String domainId,
		String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	

	/** ************************************************************************
	 * 
	 */
	 
	CrossReferenceSer[] getCrossReferences(String clientId, String namedRefId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	

	/** ************************************************************************
	 * If one of the Id arguments is null, treat that Id as a '*': i.e., return
	 * all matches. If both Ids are null, throw ParameterError.
	 */
	 
	CrossReferenceSer[] getCrossReferences(String clientId, String fromId, String toId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	

	/** ************************************************************************
	 * Bind the specified State to the specified Port.
	 */
	 
	void bindStateAndPort(String clientId, boolean confirm, String stateId, String portId)
	throws
		Warning,
		ParameterError,  // if the Port and State do not share a common parent.
		CannotObtainLock,
		ElementNotFound,
		IOException;



	/** ************************************************************************
	 * Unbind the specified Port from the State to which it is bound.
	 */
	 
	void unbindPort(String clientId, boolean confirm, String portId)
	throws
		Warning,
		ParameterError,  // if the Port is not bound to a State.
		CannotObtainLock,
		ElementNotFound,
		IOException;



	/** ************************************************************************
	 * Return the State to which the specified Port is bound, or null if it is
	 * not bound to any State.
	 */
	 
	StateSer getPortBinding(String clientId, String portId)
	throws
		ParameterError,
		ElementNotFound,
		IOException;



	/** ************************************************************************
	 * Set a new default time for the specified Predefined Event.
	 */
	 
	void setPredefEventDefaultTime(String clientId, boolean confirm, 
		String predEventNodeId, String timeExpr)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	
	
	/** ************************************************************************
	 * Set a new default value for the specified Predefined Event.
	 */
	 
	void setPredefEventDefaultValue(String clientId, boolean confirm, 
		String predEventNodeId, String valueExpr)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	


	/** ************************************************************************
	 * Set a new default value for the specified Predefined Event.
	 */
	 
	void setPredefEventIsPulse(String clientId, boolean confirm,
		String predEventNodeId, boolean pulse)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	
	
	/** ************************************************************************
	 * Rename the specified Node.
	 * Note: Warn user if the Domain has SimulationRuns.
	 */
	 
	void setNodeName(String clientId, boolean confirm, String nodeId, String newName)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;
	
	
	
	/** ************************************************************************
	 * 
	 */
	 
	void setNodeHTMLDescription(String clientId, String nodeId, String newHTMLDesc)
	throws
		ParameterError,
		CannotObtainLock,
		IOException;
		
	
	
	/** ************************************************************************
	 * Sets whether the Port is "black". That is, whether it propagates incoming
	 * Events on external Conduits to other attached external Conduits.
	 */
	 
	void setPortReflectivity(String clientId, boolean confirm, String portId, boolean black)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;



	/** ************************************************************************
	 * Set the direction of the specified Port.
	 * Note: Warn user if the Domain has SimulationRuns.
	 */
	 
	void setPortDirection(String clientId, boolean confirm, String portId, 
		PortDirectionType dir)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ModelContainsError,
		IOException;
	
	

	/** ************************************************************************
	 * Note: Warn user if the Domain has SimulationRuns.
	 */
	 
	ConduitSer createConduit(String clientId, boolean confirm, String parentId, 
		String portAId, String portBId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;
	
	
	/** ************************************************************************
	 * Same as createConduit(String, boolean, String, String, String), but
	 * the server chooses the owner of the Conduit based on the topology.
	 * Note: Warn user if the Domain has SimulationRuns.
	 */
	 
	ConduitSer createConduit(String clientId, boolean confirm, 
		String portAId, String portBId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;
	
	
	/** ************************************************************************
	 * Have the server reroute the specified Conduit. The server will then send
	 * a PeerNotice indicating that the Conduit has changed.
	 */
	 
	void rerouteConduit(String clientId, String conduitId)
	throws
		CannotObtainLock,
		ParameterError,
		IOException;


	
	/** ************************************************************************
	 * Have the server reroute the specified Conduits that belong to the
	 * specified Model Container. If conduitIds is null, then reroute
	 * all Conduits belonging to the Model Container. The server will then send
	 * a PeerNotice indicating that the Model Container has changed.
	 */
	 
	void rerouteConduits(String clientId, String containerId, String[] conduitIds)
	throws
		CannotObtainLock,
		ParameterError,
		IOException;


	
	/** ************************************************************************
	 * Return an externalized version of the specified PersistentNode.
	 */
	
	NodeSer getNode(String clientId, String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;


		
	/** ************************************************************************
	 * Same as getNode(String, String), but check that the returned NodeSer is
	 * identical to or a subclass of the specified Class.
	 */
	 
	NodeSer getNode(String clientId, String nodeId, Class nodeSerKind)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	/** ************************************************************************
	 * Return the named immediate child of the specified parent Node.
	 */
	 
	NodeSer getChild(String clientId, String parentId, String childName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	/** ************************************************************************
	 * Attempt to update the Node identified by the specified NodeSer.
	 * Return a new NodeSer that represents the updated information.
	 * If any aspect of the update is impossible, then do not apply any
	 * of the update, and throw an exception.
	 * Note: Warn user if the Domain has SimulationRuns.
	 *
	
	NodeSer updateNode(String clientId, boolean confirm, NodeSer nodeSer)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;*/


	
	/** ************************************************************************
	 * Return the names of the Web formats in which the specified Node can be
	 * displayed. Return in descriptionsRef[0] a description of each format.
	 */
	 
	String[] getAvailableHTTPFormats(String clientId, String nodeId, String[] descriptions)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
	
	
	
	/** ************************************************************************
	 * Return an HTTP URL that can be used to make requests to the Expressway's
	 * HTTP service to render a depiction of the specified Node, using the
	 * specified format.
	 */
	 
	String getWebURLString(String clientId, String nodeId, String format)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;	
	
	
	/** ************************************************************************
	 * Inform the client what TCP port to use for Expressway HTTP requests.
	 */
	 
	int getHTTPPort(String clientId)
	throws
		IOException;
		
	

	/** ************************************************************************
	 * 
	 */
	 
	ScenarioSer updateScenario(String clientId, boolean confirm, 
		ScenarioSer scenarioSer)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
		
		
	
	/** ************************************************************************
	 * 
	 */
	 
	void setModelScenarioTimeParams(String clientId, boolean confirm, String scenarioId,
		Date newFromDate, Date newToDate, long newDuration, int newIterLimit)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
	
	
		
	/** ************************************************************************
	 * Add the delta size of the specified Node, and return the NodeSer
	 * for the Node. Shift the locations of the Node's children by the
	 * specified amount. Only notify clients if 'notify' is true.
	 */
	 
	NodeSer incrementNodeSize(String clientId, int lastRefreshed, String nodeId, 
		double dw, double dh,
		double childShiftX, double childShiftY, boolean notify)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
		


	/** ************************************************************************
	 * Add the delta position of the specified Node, within its parent, or within
	 * its absolute coordinate space if it has no parent. Return the new
	 * NodeSer for the Node. Only notify clients if 'notify' is true.
	 */
	 
	NodeSer incrementNodeLocation(String clientId, int lastRefreshed, String nodeId, 
		double dx, double dy, boolean notify)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
		

		
	/** ************************************************************************
	 * Change the size of the specified Node, and return the new NodeSer
	 * for the Node. Shift the locations of the Node's children by the
	 * specified amount.
	 */
	 
	NodeSer changeNodeSize(String clientId, int lastRefreshed, String nodeId, 
		double newWidth, double newHeight, double childShiftX, double childShiftY)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
		


	/** ************************************************************************
	 * Change the position of the specified Node, within its parent, or within
	 * its absolute coordinate space if it has no parent. Return the new
	 * NodeSer for the Node.
	 */
	 
	NodeSer changeNodeLocation(String clientId, int lastRefreshed, String nodeId, 
		double newX, double newY)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
		

		
	/** ************************************************************************
	 * Return the node ID of the PersistentNode identified by the specified path.
	 */
	
	String resolveNodePath(String clientId, String path)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
	
	/** ************************************************************************
	 * Retrieve the node IDs of the child nodes of the specified PersistentNode.
	 */
	 
	String[] getChildNodeIds(String clientId, String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	/** ************************************************************************
	 * Retrieve the node IDs of the sub-Component ModelElements of the specified
	 * ModelContainer.
	 */
	 
	String[] getSubcomponentNodeIds(String clientId, String modelContainerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	/** ************************************************************************
	 * Retrieve the specified Attribute for the specified Node. If the Node
	 * exists but it does not have the specified Attribute, return null.
	 */
	 
	AttributeSer getAttribute(String clientId, String nodeId, String attrName)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
	
	/** ************************************************************************
	 * Return all Attributes nested within the specified Node or any of
	 * its sub-Nodes.
	 */
	 
	AttributeSer[] getAttributesDeep(String clientId, String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;	
	
	
	
	/** ************************************************************************
	 * Retrieve all of the Attributes associated with the specified Node.
	 * If there are none, return an empty array.
	 */
	 
	AttributeSer[] getAttributes(String clientId, String nodePath)
	throws
		ParameterError,  // if the Node does not exist.
		IOException;

	
		
	/** ************************************************************************
	 * Retrieve all of the Attributes associated with the specified ModelElement.
	 * If there are none, return an empty array.
	 */
	 
	AttributeSer[] getAttributesForId(String clientId, String nodeId)
	throws
		ParameterError,  // if the ModelElement does not exist.
		IOException;

	
		
	/** ************************************************************************
	 * 
	 */
	 
	boolean isGeneratorRepeating(String clientId, String genNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
	
	
	/** ************************************************************************
	 * 
	 */
	 
	void makeGeneratorRepeating(String clientId, boolean confirm, String genNodeId, 
		boolean repeating)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	/** ************************************************************************
	 * 
	 */
	 
	void setGeneratorIgnoreStartup(String clientId, boolean confirm, String genNodeId, 
		boolean ignore)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	/** ************************************************************************
	 * Retrieve all of the Scenarios associated with the specified Domain.
	 * If there are none, return an empty array.
	 */
	 
	ScenarioSer[] getScenarios(String clientId, String domainName)
	throws
		ParameterError,  // if the Domain does not exist.
		IOException;

	
		
	/** ************************************************************************
	 * Retrieve all of the Scenarios associated with the specified Domain.
	 * If there are none, return an empty array.
	 */
	 
	ScenarioSer[] getScenariosForDomainId(String clientId, String domainId)
	throws
		ParameterError,  // if the Domain does not exist.
		IOException;
		

	
	/** ************************************************************************
	 * Retrieve the specified Model Scenario.
	 */
	 
	ModelScenarioSer getModelScenario(String clientId, String modelDomainName, 
		String modelScenarioName)
	throws
		ParameterError,  // if the ModelDomain or ModelScenario does not exist.
		ElementNotFound,
		IOException;

	
	
	/** ************************************************************************
	 * 
	 */
	 
	ModelScenarioSer[] getScenariosForScenarioSetId(String clientId, String scenSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	/** ************************************************************************
	 * 
	 */
	 
	ModelScenarioSetSer[] getScenarioSetsForDomainId(String clientId, String domainId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		

		
	/** ************************************************************************
	 * Retrieve all of the Decisions associated with the specified DecisionPoint
	 * for the specified DecisionScenario. If the DecisionPoint has no Decisions
	 * in that DecisionScenarion, return an empty array.
	 */
	 
	DecisionSer[] getDecisions(String clientId, String decisionPointPath, String scenarioName)
	throws
		ParameterError,  // if the DecisionPoint does not exist or the
						// DecisionScenario does not exist.
		IOException;



	/** ************************************************************************
	 * 
	 */
	 
	ImageIcon getStoredImageIcon(String clientId, String handle)
	throws
		ParameterError,
		IOException;
	
	

  /* ***************************************************************************
   * Methods that pertain to Scenarios.
   */


	/** ************************************************************************
	 * Retrieve the Attributes of the specified type for the specified Node.
	 */
	 
	AttributeSer[] getAttributes(String clientId, String nodeId, String className, 
		String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
	
	/** ************************************************************************
	 * Retrieve all Nodes that have Attributes with values of the specified type,
	 * for the specified Node or any of its children, in the context of the specified 
	 * Scenario.
	 */
	 
	NodeSer[] getNodesWithAttributeDeep(String clientId, String nodeId, 
		String className, String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	/** ************************************************************************
	 * Create a new Scenario for the specified Domain. If scenarioName
	 * is null, create an arbitrary unique name.
	 */

	ScenarioSer createScenario(String clientId, String domainName, String scenarioName)
	throws
		ParameterError,
		IOException;
	
		
	
	/** ************************************************************************
	 * Create an independent copy of the specified Scenario. The copy will
	 * belong to the same Domain and have the same values for all Attributes. A
	 * unique name will be created.
	 */

	ScenarioSer copyScenario(String clientId, String scenarioId)
	throws
		ModelContainsError,
		ParameterError,
		IOException;
	
		
	
	/** ************************************************************************
	 * Create a new Scenario for the specified Domain. If scenarioName
	 * is null, create an arbitrary unique name.
	 */

	ScenarioSer createScenario(String clientId, String domainName)
	throws
		ParameterError,
		IOException;
	
		
	
	/** ************************************************************************
	 * 
	 */
	 
	ModelScenarioSetSer createScenariosOverRange(String clientId, String baseScenId,
		String attrId, Serializable[] values)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		

	int getNoOfAttributes(String clientId, String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		IOException;

		
	
	/** ************************************************************************
	 * Note: Warn user if the Domain has SimulationRuns.
	 */
	 
	AttributeSer createAttribute(String clientId, boolean confirm, String parentId, 
		double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;


	
	/** ************************************************************************
	 * If 'nextAttrId' is null, make the new Attribute the last one in the list.
	 */
	 
	AttributeSer createAttributeBefore(String clientId, boolean confirm, String nextAttrId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;

		
		
	/** ************************************************************************
	 * If 'priotAttrId' is null, make the new Attribute the first one in the list.
	 */
	 
	AttributeSer createAttributeAfter(String clientId, boolean confirm, String priorAttrId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;


	
	/** ************************************************************************
	 * Create a new Attribute for the specified Node (with parentId).
	 * If 'scenarioId' is null, set the value for all Scenarios. Also, set the
	 * default value of the new Attribute to equal 'value'.
	 */
	 
	AttributeSer createAttribute(String clientId, boolean confirm, String parentId, 
		Serializable value, String scenarioId, double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;
		
		
	
	/** ************************************************************************
	 * Create a HierarchyAttribute and insert it before the specified Attribute.
	 * If 'attrId' is not null, the Attribute specified must belong to the Domain.
	 * The Domain must be a HierarchyDomain.
	 * Call applyToHierarchy() on the HierarchyDomain.
	 * If 'scenarioId' is null, treat 'value' as a default value and set the value
	 * in all Scenarios for the Domain.
	 * If 'attrId' is null, make the new HierarchyAttribute the first Attribute
	 * of the Domain.
	 */
	 
	AttributeSer createHierarchyDomainAttributeBefore(String clientId, boolean confirm,
		String domainId, String attrId, Serializable value, String scenarioId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
		
		
	
	/** ************************************************************************
	 * Create a HierarchyAttribute and insert it before the specified Attribute.
	 * If 'attrId' is not null, the Attribute specified must belong to the Domain.
	 * The Domain must be a HierarchyDomain.
	 * Call applyToHierarchy() on the HierarchyDomain.
	 * If 'scenarioId' is null, treat 'value' as a default value and set the value
	 * in all Scenarios for the Domain.
	 * If 'attrId' is null, make the new HierarchyAttribute the last Attribute
	 * of the Domain.
	 */
	 
	AttributeSer createHierarchyDomainAttributeAfter(String clientId, boolean confirm,
		String domainId, String attrId, Serializable value, String scenarioId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;



	/** ************************************************************************
	 * Retrieve the value of the specified Attribute, as recorded in the
	 * specified ModelScenario. If the Attribute has no value, then return
	 * null.
	 */
	 
	Serializable getAttributeValue(String clientId, String attrPath, String scenarioName)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException;
	
		
	
	/** ************************************************************************
	 * Retrieve the value of the specified Attribute, as recorded in the
	 * specified ModelScenario. If the Attribute has no value, then return
	 * null.
	 */
	 
	Serializable getAttributeValueForId(String clientId, String attrId, String scenarioName)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException;
	
	
		
	/** ************************************************************************
	 * Retrieve the value of the specified Attribute, as recorded in the
	 * specified ModelScenario. If the Attribute has no value, then return
	 * null.
	 */
	 
	Serializable getAttributeValueById(String clientId, String attrId, String scenarioId)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException;
		
	
		
	/** ************************************************************************
	 * Values are returned in the same sequence as the AttributeSers are returned
	 * from the getAttributes and getAttributesForId methods. If the Attribute
	 * has no value in the Scenario, then return null.
	 */
	 
	Serializable[] getAttributeValuesForNode(String clientId, String nodeId, String scenarioId)
	throws
		ElementNotFound,  // if the Node does not exist.
		ParameterError,  // if the Scenario does not exist
		IOException;
	
	
		
	/** ************************************************************************
	 * Retrieve the default value of the specified Attribute. If the Attribute
	 * has no default value, then return null.
	 */
	 
	Serializable getAttributeDefaultValueById(String clientId, String attrId)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException;
	
	
	
	/** ************************************************************************
	 * Values are returned in the same
	 * sequence as the AttributeSers are returned from the getAttributes and
	 * getAttributesForId methods.
	 */
	 
	Serializable[] getAttributeDefaultValuesForNode(String clientId, String nodeId)
	throws
		ElementNotFound,  // if the Node does not exist.
		ParameterError,  // if the Scenario does not exist
		IOException;
	
	
		
	/** ************************************************************************
	 * 
	 */
	 
	void setAttributeDefaultValue(String clientId, boolean confirm, String attrId,
		Serializable value)
	throws
		Warning,
		CannotObtainLock,
		ParameterError,
		IOException;
	
		
	
	/** ************************************************************************
	 * 
	 */
	 
	Serializable getAttributeDefaultValue(String clientId, String attrPath)
	throws
		ParameterError,
		IOException;
	
		
	
	/** ************************************************************************
	 * Set the value of the specified Attribute in the context of the specified
	 * Scenario. If the value cannot be set, thrown an Exception.
	 * If scenarioId is null set the Attribute value for all Scenarios.
	 * Note: Warn user if the Scenario has SimulationRuns.
	 */
	 
	void setAttributeValue(String clientId, boolean confirm, String attrNodeId, 
		String scenarioId, Serializable value)
	throws
		Warning,
		CannotObtainLock,
		ParameterError,  // if the value cannot be set, e.g., if the Attribute
						// is bound to a state.
		IOException;
		
		
	
	/** ************************************************************************
	 * Set the value of the specified Attribute for all Scenarios in the ModelDomain.
	 * If the value cannot be set, thrown an Exception.
	 * Note: Warn user if the Domain has SimulationRuns.
	 *
	 
	void setAttributeValue(String clientId, boolean confirm, String attrNodeId, 
		Serializable value)
	throws
		Warning,
		CannotObtainLock,
		ParameterError,  // if the value cannot be set, e.g., if the Attribute
						// is bound to a state.
		IOException;*/
		
		
	
  /* ***************************************************************************
   * Methods that pertain to simulation initiation, control, or monitoring.
   */


	/** ************************************************************************
	 * Retrieve the latest progress status for a previously initiated
	 * service request. Once this method is called, progress records are purged.
	 */

	List<String> getProgress(String clientId, int callbackId)
	throws
		ParameterError,
		IOException;

		

	/** ************************************************************************
	 * Retrieve the latest set of asynchronous messages for a previously initiated
	 * service request. Once this method is called, message records are purged.
	 */

	List<String> getMessages(String clientId, int callbackId)
	throws
		ParameterError,
		IOException;

		

	/** ************************************************************************
	 * Retrieve the latest completion status for a previously initiated
	 * service request.
	 */

	boolean isComplete(String clientId, int callbackId)
	throws
		ParameterError,
		IOException;


		
	/** ************************************************************************
	 * Retrieve the latest abort status for a previously initiated
	 * service request.
	 */

	boolean isAborted(String clientId, int callbackId)
	throws
		ParameterError,
		IOException;

		

	/** ************************************************************************
	 * Terminate the specified service request, gracefully if possible. This call
	 * does not block.
	 */

	void abort(String clientId, int callbackId)
	throws
		ParameterError,
		IOException;

		

	/** ************************************************************************
	 * Get a new Decision Set for a previously initiated
	 * service request.
	 */

	//public Set<Decision> getNewDecisions(int callbackId);


	/** ************************************************************************
	 * Simulate the specified ModelScenario. Clone the "current" Scenario for 
	 * the Domain if no Scenario is specified,
	 * or create a blank one if the current Scenario for the Domain is null.
	 * If step is True, then create a breakpoint at the start of simulation.
	 * Each simulation run will create a SimulationRun to contain its results.
	 * If 'repeatable' is true, then the simulations should generate the same
	 * results each time they are run (assuming the models and scenarios are
	 * not changed).
	 */
	
	Integer simulate(String clientId, String modelDomainName, String scenarioName, 
		Date initialEpoch, Date finalEpoch,
		int iterations, boolean step, int runs, long duration /* ms */,
		PeerListener peerListener,  // may be null
		boolean repeatable
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	/** ************************************************************************
	 * Simulate, varying the value of the selected Attribute, over the range
	 * of the Attribute's allowed values.
	 * See the slide "Iteration Over Variables".
	 */
	 
	Integer simulateScenarioSet(String clientId,
		String scenSetId, 
		Date initialEpoch, Date finalEpoch,
		int iterations, boolean step, int noOfRuns, long duration /* ms */, 
		PeerListener peerListener, boolean repeatable
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
	/** ************************************************************************
	 * Locate the specified Decision Variable in the database, and update
	 * it with the specified new value. Clone the specified DecisionScenario,
	 * or clone the "current" one for the Domain if no Scenario is specified,
	 * or create a blank one if the current Scenario for the Domain is null.
	 *
	 * The specified value is of type Vector, because OpenLaszlo requires
	 * that. The Vector value is expected to contain a single element.
	 *
	 * If propagate is true, a Variable update may result in the re-evaluation
	 * of the state of Model elements, and eventually propagation of these state
	 * changes to Decision models that are sensitive to those states. The method
	 * therefore returns a logical handle to a MainCallback that recomputes
	 * the ModelDomain's state and updates Decisions, or it may return null
	 * if no changes propagated. The handle may be used by clients to
	 * identify the MainCallback object.
	 *
	 * If step is True, then create a breakpoint at the start of the Decision-
	 * making process, and at the start of any simulation that is performed.
	 */

	Integer[] updateVariable(String clientId, int lastRefreshed, 
		String decisionDomainName, String scenarioName,
		String decisionPointName, String variableName, Vector<Serializable> valueVector)
	throws
		ClientIsStale,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;



  /* ***************************************************************************
   * Methods that return information pertaining to a collection of SimulationRuns.
   */


	/** ************************************************************************
	 * Return the results of all simulation runs for the specified nodes and the
	 *			specified ModelScenario.
	 *
	 * @param	nodePaths A List of nodePaths (each specified as a String). A node must be
	 *			a State.
	 * @param	modelDomainName The Model Domain that owns the specified nodes.
	 * @param	modelScenarioName The ModelScenario for which to compute the statistics, based on
	 *			the existing runs for that ModelScenario..
	 * @param	options - Any or all of "mean", "geomean", "max", "min", "sd",
	 *			or "gamma".
	 * @return	A List of arrays of doubles. Each array is for a node, and each array
	 *			element is a statistic corresponding with the options.
	 */

	List<double[]> getResultStatisticsForScenario(String clientId, List<String> nodePaths, 
		String modelDomainName,
		String modelScenarioName, String[] options)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	/** ************************************************************************
	 * 
	 */
	 
	List<double[]> getResultStatisticsById(String clientId, List<String> nodeIds,
		String distOwnerId, String[] options)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	/** ************************************************************************
	 * 
	 */
	 
	double[] getResultStatisticsById(String clientId, String nodeId,
		String distOwnerId, String[] options)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;


		
	/** ************************************************************************
	 * Compute the Assurance of each solution, based on the simulations over 
	 * the ScenarioSet.
	 */
	 
	double[] computeAssurances(String clientId, String scenSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		

	/** ************************************************************************
	 * Return a List of histograms of the final values of the specified States.
	 * This method aligns all of the histograms so that they start at the same
	 * range value and have the same number of buckets.
	 *
	 * @param	statePaths A List of paths of model States.
	 * @param	bucketSize The width of each range bucket in each histogram.
	 * @param	bucketStart The lowest range value of the first bucket.
	 * @param	noOfBuckets The number of buckets in the histrograms to be generated.
	 * @param	allValues If true, then all values must fall into a histrogram
	 *			bucket. If false, values may fall outside the buckets, and hence
	 *			are not counted.
	 * @return	A List of histograms.
	 */

	List<int[]> getHistograms(String clientId, List<String> statePaths, String modelDomainName,
		String modelScenarioName, double bucketSize, double bucketStart, int noOfBuckets,
		boolean allValues)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException;

	
		
	/** ************************************************************************
	 * 
	 */
	 
	int[] getHistogramById(String clientId, String stateId,
		String distOwnerId, double bucketSize, double bucketStart, int noOfBuckets,
		boolean allValues)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException;

		
	
	/** ************************************************************************
	 * 
	 */
	 
	HistogramDomainParameters defineOptimalHistogramById(String clientId, String stateId, 
		String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException;
		

		
	/** ************************************************************************
	 * Determine the size of the time domain, based on the Simulation Runs that
	 * exist for the specified DistributionOwner. If the Simulation Runs do not all
	 * span the same simulated time, then choose the time period that overlaps
	 * all runs.
	 */
	 
	TimeSeriesParameters defineOptimalValueHistoryById(String clientId, 
		String stateId, String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	/** ************************************************************************
	 * Return the history of the expected value of the specified State, computed
	 * by averaging across all simulations for the specified DistributionOwner. Also
	 * return the upper and lower standard deviation for each point. Thus, the
	 * object returned is an array of arrays, where each sub-array contains these
	 * double values in this sequence:
			stats[0] = 15.9th percentile (for normal, this is one sd below mean)
			stats[1] = mean
			stats[2] = 84.1th percentile (for normal, this is one sd above mean)
			stats[3] = lowestValue
			stats[4] = highestValue
			stats[5] = sigma
			stats[6] = median
	 */
	 
	double[][] getExpectedValueHistory(String clientId, String stateId, String distOwnerId, 
		Date startingEpoch, int noOfPeriods, long periodInMs)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
		
	/** ************************************************************************
	 * Return the statistical correlation between the pairs of States specified by
	 * the statePaths parameter.
	 *
	 * @param	statePaths A List of arrays, each containing two elements. Each
	 *			array element must specify the path of a model State.
	 * @param	option Must be null, an empty String, or one of "cc" or "cv", for
	 *			correlation coefficient or co-variance, respectively. A null or
	 *			empty option results in the default choice of cc.
	 * @return	An array of doubles. Each double is the correlation for the corresponding
	 *			pair of States in the statePaths List.
	 */

	double[] getCorrelations(String clientId, List<String[]> statePaths, String modelDomainName,
		String modelScenarioName, String option)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;



	/** ************************************************************************
	 * Return a List of the names of the simulation runs that exist for the specified
	 * Model Domain and Model Scenario.
	 */
	 
	List<String> getSimulationRuns(String clientId, String modelDomainName, String modelScenarioName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	/** ************************************************************************
	 * Return a List of the names of the simulation runs that exist for the specified
	 * Model Scenario.
	 */
	 
	List<SimulationRunSer> getSimulationRuns(String clientId, ModelScenarioSer s)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	/** ************************************************************************
	 * Return a List of the simulation runs that exist for the specified
	 * DistributionOwner.
	 */
	 
	List<SimulationRunSer> getSimulationRuns(String clientId, String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		
		
	/** ************************************************************************
	 * Retrieve the values of the specified State for the specified ModelScenario,
	 * for each simulation run that exists for that scenario.
	 */
	 
	Serializable[] getFinalStateValues(String clientId, String statePath, String scearioName)
	throws
		CannotObtainLock,
		ParameterError,  // if the State does not exist, or the
						// Scenario does not exist.
		IOException;

		
		
	/** ************************************************************************
	 * Retrieve the values of the specified State for the specified ModelScenario,
	 * for each simulation run that exists for that scenario.
	 */
	 
	Serializable[] getFinalStateValuesForId(String clientId, String stateId, String scearioId)
	throws
		CannotObtainLock,
		ParameterError,  // if the State does not exist, or the
						// Scenario does not exist.
		IOException;

		
		
	/** ************************************************************************
	 * 
	 */
	 
	SimulationRunSer[] getSimulationRunsForSimRunSetId(String clientId, String simRunSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
			

  /* ***************************************************************************
   * Methods that return information pertaining to a specific SimulationRun
   * or SimRunSet.
   */


	/** ************************************************************************
	 * 
	 */
	 
	SimRunSetSer[] getSimRunSetsForScenarioId(String clientId, String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

	
	
	/** ************************************************************************
	 * Note: First iteration is iteration 0.
	 */
	 
	List<GeneratedEventSer> getEventsForSimNodeForIteration(String clientId, String simRunId,
		int iteration)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;



	/** ************************************************************************
	 * Return an array of Event Lists for the specified element, where each array
	 * represents a Simulation Run of the specified Model Scenario.
	 * The element paths must be canonical and must all specify the same Model Domain.
	 * The elements specified must be State, Activity, Port, or Conduits.
	 * The first (0th) element of the List is iteration 1.
	 */
	 
	List<GeneratedEventSer>[] getEventsByEpoch(String clientId, String modelDomainName, 
		String modelScenarioName,
		String simRunName, List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;



	/** ************************************************************************
	 * Same as getEventsByEpoch(String, String, String, String, List<String>),
	 * but the caller only specifies the Node Id of the SimulationRun.
	 */
	 
	List<GeneratedEventSer>[] getEventsForSimNodeByEpoch(String clientId, String simNodeId, 
		List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;



	/** ************************************************************************
	 * Same as getEventsForSimNodeByEpoch(String, String, List<String>),
	 * but all States are assumed..
	 */
	 
	List<GeneratedEventSer>[] getEventsForSimNodeByEpoch(String clientId, String simNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;



	/** ************************************************************************
	 * Same as getEventsForSimNodeByEpoch(String, String),
	 * but only reports States that are tagged with an Attribute that has a
	 * value with a type of at least one of the classes in 'stateTags'.
	 */
	 
	List<GeneratedEventSer>[] getEventsForSimNodeByEpoch(String clientId, String simNodeId,
		List<String> statePaths, Class[] stateTags, List<String>[] filteredPathsHolder,
		List<String>[] filteredIdsHolder)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;



	/** ************************************************************************
	 * 
	 */
	 
	StateSer[] getStatesRecursive(String clientId, String modelContainerNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	/** ************************************************************************
	 * 
	 */
	 
	String getEventsByEpochAsHTML(String clientId, String simRunNodeId, 
		List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;



	/** ************************************************************************
	 * Same as getEventsByEpochAsHTML(String, String), but for all output States.
	 * (Some States may be omitted, such as a Generator's Count State.)
	 */
	 
	String getEventsByEpochAsHTML(String clientId, String simRunNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;



	/** ************************************************************************
	 * Returns:
	 *	Object[2], in which the two elements are as follows:
	 *		[0]: A Serializable[], containing the State values.
	 *		[1]: a List<String>, containing the Ids of the States, in the same
	 *			order as the values that are returned in element 0.
	 *	If 'iteration' is 0 (zero), then the array of Serializable values is
	 *	an array of nulls.
	 */
	 
	Object[] getStateValuesForIteration(String clientId, String simRunId, int iteration)
	throws
		CannotObtainLock,
		ParameterError,
		IOException;
	
	
	
	/** ************************************************************************
	 * Set the mode of the specified PersistentNode such that it provides full
	 * diagnostic information whenever it performs any action or changes
	 * state.
	 */
	 
	void watchNode(String clientId, boolean enable, String nodePath)
	throws
		ParameterError,  // if the PersistentNode does not exist.
		IOException;
}
