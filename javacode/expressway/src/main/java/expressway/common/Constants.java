/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

public class Constants
{
	public final static String RMIServiceName = "Expressway";
	public static final String ConfigFileName = "expressway_config.xml";
	
}

