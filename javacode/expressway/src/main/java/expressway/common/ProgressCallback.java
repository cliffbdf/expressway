/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.common.ModelAPITypes.*;
import java.util.List;


/**
 * Callback interface for signaling a client on the progress of an
 * asynchronously activity. Implemented by the client, and called by
 * the simulator.
 */

public interface ProgressCallback
{
	/**
	 * To be called by the simulator to check if the client has requested to
	 * gracefully abort the simulation.
	 */
	 
	boolean checkAbort();
	
	
	/**
	 * To be called by the simulator to update progress status.
	 */

	void showProgress(String msg);


	/**
	 * To be called by the simulator to asynchronously pass a message
	 * to the client.
	 */

	void showMessage(String msg);
	
	
	/**
	 * To be called by the simulator to asynchronously pass an error message
	 * to the client.
	 */
	 
	void showErrorMessage(String msg);


	/**
	 * To be called by the simulator to update completion status.
	 */

	void completed();


	/**
	 * To be called by the simulator to signal to the client that simulation
	 * was aborted.
	 */

	void aborted();


	/**
	 * To be called by the simulator to signal to the client that simulation
	 * was aborted. The msg String is provided to the client.
	 */

	void aborted(String msg);


	/**
	 * To be called by the simulator to signal to the client that simulation
	 * was aborted due to the specified Exception.
	 */

	void aborted(Exception ex);


	/**
	 * To be called by the simulator to signal to the client that simulation
	 * was aborted due to the specified cause and Exception.
	 */

	void aborted(String msg, Exception ex);
}

