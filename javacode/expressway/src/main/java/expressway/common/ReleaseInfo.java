/**
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


import java.net.URL;


/**
 * Constants pertaining to the version number and descriptive information about
 * the current release.
 */
 
public class ReleaseInfo
{
	public static final String VersionNumber = "0.4";
	
	public static final String CopyrightStatement =
		"Copyright (c) 2007-2011 by Expressway Solutions LLC. All Rights Reserved.";
	
	public static final String ExpresswayURLString = "http://ExpresswaySolutions.com";

	public static final String HelpBaseURLString = "http://expresswaysolutions.com/help/";
	
	public static final String DownloadBaseURLString = "http://expresswaysolutions.com/Downloads/";
	
	public static final String APIBaseURLString = "http://expresswaysolutions.com/APIs/";
	
	public static final String ValueDrivenITURLString = "http://ValueDrivenIT.com";
	
	public static final String ValueDrivenITWikiBaseURLString = 
		"http://expresswaysolutions.com/valuedrivenit_com/wiki1/tiki-index.php?page=";

	public static final String ValueDrivenITWikiMainPageName = "Edition2";
	
	

	public static URL BugReportURL;
	
	public static URL QuickStartTutorialURL;
	
	public static URL QuickReferenceURL;
	
	public static URL OnlineHelpDirectoryURL;
	
	public static URL GammaTutorialURL;
	
	public static URL ExpresswayUserManualURL;
	
	public static URL ValueDrivenITURL;
	
	
	
	static
	{
		try
		{
			BugReportURL = new URL("http://expresswaysolutions.com/cgi-bin/FormMail.bugreport.pl");
			
			QuickStartTutorialURL = new URL(
				"http://ExpresswaySolutions.com/help/QuickStart/html/Expressway_Quick-Start_Tutorial.html");
			
			QuickReferenceURL = new URL("http://expresswaysolutions.com/help/QuickReference/img0.html");
			
			OnlineHelpDirectoryURL = new URL("http://expresswaysolutions.com/help/index.html");
			
			GammaTutorialURL = new URL("http://expresswaysolutions.com/help/Gamma/img0.html");
			
			ExpresswayUserManualURL = new URL("http://ExpresswaySolutions.com/Downloads/Expressway_User_Manual.pdf");
			
			ValueDrivenITURL = new URL(ValueDrivenITURLString);
		}
		catch (java.net.MalformedURLException ex)
		{
			GlobalConsole.printStackTrace(ex);  // should not happen
			throw new Error(ex);
		}
	}
}

