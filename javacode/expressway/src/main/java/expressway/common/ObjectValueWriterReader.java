/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


import expressway.common.ModelAPITypes.*;
import java.io.Serializable;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.*;
import java.lang.reflect.*;


/** ****************************************************************************
 * For externalizing Object values, for the values of Attributes and Events.
 * Objects must be Serializable. Object types supported at this time include:
 *	Null
 *	Integer
 *	Float
 *	Double
 *	Boolean
 *	String
 *	A class instance, using syntax defined for the method recognizeInstance(String).
 *	A class, using the syntax defined for the method recognizeType(String).
 *
 * Note: See also EventListArchiver.
 */
 
public class ObjectValueWriterReader
{
	/** ************************************************************************
	 * Attempt to parse the specified String, to translate it into an Object value.
	 * If the type cannot be recognized, treat it as a String.
	 */
	 
	public static Serializable parseObjectFromString(String s)
	throws
		ParameterError  // if not recognized
	{
		if (s == null) return null;
		
		if (s.trim().equals("")) return null;
		
		if (s.equalsIgnoreCase("Null")) return null;
		
		try { return Integer.parseInt(s); }
		catch (NumberFormatException ex) {}
			
		try { return Float.parseFloat(s); }
		catch (NumberFormatException ex) {}
			
		try {return Double.parseDouble(s); }
		catch (NumberFormatException ex) {}
		
		if (s.equalsIgnoreCase("true")) return new Boolean(true);
		else if (s.equalsIgnoreCase("false")) return new Boolean(false);
		
		Serializable instance = recognizeInstance(s);
		if (instance != null) return instance;
		
		Class c = recognizeType(s);
		if (c != null) return c;
		
		throw new ParameterError("Could not recognize value");
	}
	
	
	/** ************************************************************************
	 * Write the specified Object out in a manner that can later be parsed by
	 * the above parseObjectFromString method..
	 */
	 
	public static String writeObjectAsString(Serializable o)
	throws
		ParameterError
	{
		if (o == null) return "Null";
		
		if (o instanceof Number) return o.toString();
		
		if (o instanceof String) return (String)o;
		
		if (o instanceof Boolean) return o.toString();
		
		/*if (o instanceof Types)
		{
			Class c = o.getClass();
			
			String sVal = "new " + c.getName() + "(";
			
			Constructor[] cons = c.getConstructors();
			if (cons.length > 2) throw new RuntimeException(
				"Found more than two constructors for Class " + c.getName());
			
			Constructor constructor0 = null;
			try { constructor0 = c.getConstructor(); }
			catch (NoSuchMethodException ex) { throw new RuntimeException(ex); }
			
			Constructor constructor1 = null;
			if (cons.length == 2)
				if (constructor0 == cons[0]) constructor1 = cons[1];
				else constructor1 = cons[0];
			
			if (constructor1 != null)  // use the non-zero argument constructor.
			{
				Class[] argTypes = constructor1.getParameterTypes();
				
				Field[] fields = c.getFields();
	
				int i = 0;
				for (Field field : fields)
				{
					if (i != 0) sVal += ", ";
					
					if (field.getType() != argTypes[i]) throw new RuntimeException(
						"Field type of a Types object does not match constructor type");
					
					Object fieldValue = null;
					try { fieldValue = field.get(o); }
					catch (IllegalAccessException ex) { throw new RuntimeException(ex); }
					
					if (! (fieldValue instanceof Serializable)) throw new ParameterError(
						"Field " + field.getName() + " of type " + argTypes[i] + " is not Serializable");
					
					sVal += writeObjectAsString((Serializable)fieldValue);
					
					i++;
				}
			}
			
			sVal += ")";
			
			return sVal;
		}*/
		
		return o.toString();  // last ditch attempt
	}
	
	
	/**
	 * Recognize the instantiation of a new instance of a class, with constructor
	 * arguments. The syntax is as follows:
	 
		'new' <type_name> '(' arg* ')'
		
	 * Each arg must be either a number, an identifier, or a string value delimited
	 * by double quotes. "slash-star" comments may be embedded in s: they are skipped
	 * over.
	 *
	 * The only constraint on the recognized class is that it must be Serializable,
	 * and each of the constructor arguments must be Serializable.
	 */
	 
	public static Serializable recognizeInstance(String s)
	throws
		ParameterError
	{
		// Attempt to parse the String.
		
		if (s == null) return null;
		
		if (s.equals("")) return null;
		
		StreamTokenizer tokenizer = new StreamTokenizer(new StringReader(s));
		tokenizer.wordChars('a', 'z');
		tokenizer.wordChars('A', 'Z');
		tokenizer.wordChars('0', '9');
		tokenizer.wordChars('$', '$');
		tokenizer.whitespaceChars(' ', ' ');
		tokenizer.whitespaceChars('\t', '\t');
		tokenizer.parseNumbers();
		tokenizer.quoteChar('"');
		tokenizer.slashStarComments(true);
		
		
		if (getNextToken(tokenizer) != StreamTokenizer.TT_WORD) return null;
		
		if (! (tokenizer.sval.equalsIgnoreCase("new"))) return null;
		
		if (getNextToken(tokenizer) != StreamTokenizer.TT_WORD) throw new ParameterError(
			"Syntax: expected a class name");
		
		String className = tokenizer.sval;
		
		if (getNextToken(tokenizer) != '(') throw new ParameterError(
			"Syntax: expected '('; sval='" + tokenizer.sval + "', ttype=" + tokenizer.ttype +
			" ('" + (char)(tokenizer.ttype) + "')");
		
		
		// Get args.
		
		List<Serializable> args = new Vector<Serializable>();
		boolean lookingForFirstArg = true;
		while (true)
		{
			int status = getNextToken(tokenizer);
			
			if (status == ')')
			{
				tokenizer.pushBack();
				break;
			}
			
			if (lookingForFirstArg) lookingForFirstArg = false;
			else
			{
				if (status == ',') status = getNextToken(tokenizer);
				else throw new RuntimeException("Expecting ','; found '" +
					(char)status + "' (" + status + ")");
			}
			
			if (status == StreamTokenizer.TT_WORD)
				args.add(tokenizer.sval);
			else if (status == StreamTokenizer.TT_NUMBER)
				args.add(new Double(tokenizer.nval));
			else if (status == '"')
				args.add(tokenizer.sval);
			else
				throw new RuntimeException("Unexpected status: '" + (char)status +
					"' (" + status + ")");
		}
		
		if (getNextToken(tokenizer) != ')') throw new ParameterError(
			"Syntax: expected ')'");
		
		
		// Identify the Constructor to use to create an instance of the recognized
		// type.
		
		Class cl = null;
		try { cl = Class.forName(className); }
		catch (ClassNotFoundException ex) { throw new ParameterError(
			"Could not identify the class '" + className + "'"); }
		
		if (! (cl instanceof Serializable)) throw new ParameterError(
			"Class " + cl.getName() + " is not Serializable");
			
		Class[] argTypes = new Class[args.size()];
		int i = 0;
		for (Serializable arg : args) argTypes[i++] = arg.getClass();
		
		Constructor constructor = null;
		try { constructor = cl.getConstructor(argTypes); }
		catch (NoSuchMethodException ex) { throw new ParameterError(ex); }
		
		
		// Create an instance of the recognized Type, with the recognized constructor
		// arguments.
		
		Serializable[] argsAr = args.toArray(new Serializable[args.size()]);
		Serializable instance = null;
		try { instance = (Serializable)(constructor.newInstance((Object[])argsAr)); }
		catch (Exception ex) { throw new ParameterError(ex); }
		
		return instance;
	}
	
	
	protected static int getNextToken(StreamTokenizer tokenizer)
	throws
		ParameterError
	{
		try { return tokenizer.nextToken(); }
		catch (java.io.IOException ex) { throw new ParameterError(ex); }
	}
	
	
	/**
	 * Parse the String. If it begins with "class " then attempt to recognized the
	 * name of a Types class following the "class ". If one is found, return it. 
	 * Otherwise return null. Example:
	 *   class expressway.common.Types$State$Financial$NetValue
	 */
	 
	public static Class recognizeType(String s)
	{
		if (! s.startsWith("class ")) return null;
		//if (! s.startsWith("tag:")) return null;
		
		s = s.substring(5).trim();
		//s = s.replace('$', '.');
		
		Class c;
		try { c = Class.forName(s); }
		catch (ClassNotFoundException ex)
		{
			GlobalConsole.println("Did not find class " + s);
			return null;
		}
		
		return c;
	}
}

