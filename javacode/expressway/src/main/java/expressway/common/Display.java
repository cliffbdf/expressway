/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.geometry.Point;

public interface Display extends Point
{
	double getX();
	double getY();
	double getScale();
	double getOrientation();  // in radians.

	void setX(double x);
	void setY(double y);
	void setScale(double scale);
	void setOrientation(double angle);  // in radians.
	
	//Object clone() throws CloneNotSupportedException;
	
	//Object deepCopy();
}



