/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import java.util.Set;
import java.util.List;
import java.io.Serializable;
import java.io.IOException;
import java.rmi.Remote;
import javax.swing.ImageIcon;


/**
 * A callback interface for the ModelEngine to use to inform the client of
 * the status and activities of particular model elements and decision
 * elements. This interface definition also defines the 
 */

public interface ClientModel
{
	/**
	 * Server-side abstraction of client view.
	 */
	 
	interface ClientView
	{
		/** ************************************************************************
		 * Return the ModelEngine that contains the PersistentNodes depicted by
		 * this View.
		 */
		 
		ModelEngineRMI getModelEngine();
	
	
		List<NodeSer> getOutermostNodeSers();
		
		
		List<String> getOutermostNodeIds();
		
		
		List<VisualComponent> getOutermostVisuals();
		
		
		String getName();
	
	
		void showScenarioCreated(String scenarioId);
		
		
		void showScenarioDeleted(String scenarioId);
	
	
		/** ************************************************************************
		 * If the specified Scenario is not currently depicted, depict it. 
		 * Make sure that the Scenario is visible. Note that the ScenarioVisual
		 * returned might be in a different View than this View. If this View cannot
		 * have Scenarios, or if the specified Scenario cannot be associated with
		 * this View for any other reason, throw a RuntimeException.
		 */
		 
		ScenarioVisual showScenario(String scenarioId)
		throws
			Exception;
			
			
		/** ************************************************************************
		 * Return the current ScenarioVisual in this View. Return null if this View
		 * does not depict Scenarios or if a Sceanario is not currently depicted.
		 */
		 
		ScenarioVisual getCurrentScenarioVisual();
	
	
		/** ************************************************************************
		 * Identify all of the client-side Visual Components in this View that
		 * represent the specified server-side Persistent Node. Includes Visuals in
		 * ControlPanels: calls <ControlPanel>.identifyVisualComponent(String) on any
		 * ControlPanels that the View owns.
		 */
		 
		Set<VisualComponent> identifyVisualComponents(String nodeId)
		throws
			ParameterError;  // if the node Id is null or invalid.
	
	
		/** ************************************************************************
		 * Identify all of the client-side Visual Components in this View that
		 * are of the specified class (or are derived from that class).
		 */
		 
		Set<VisualComponent> identifyVisualComponents(Class c)
		throws
			ParameterError;
		
		
		/** ************************************************************************
		 * Return a Set of all of the VisualComponents represented by this View.
		 * The returned Set should not be modified, as it might be the actual Set
		 * used by the View.
		 */
		 
		Set<VisualComponent> identifyVisualComponents()	throws
			ParameterError;
		
		
		/** ************************************************************************
		 * Obtain information about the specified Node from the server, and update
		 * the corresponding Visuals to accurately reflect the Node's state on the
		 * server. Note that in some circumstances it might be necessary to re-constitute
		 * one or more Visuals. Any lists of Visuals maintained by this View are automatically
		 * updated.
		 */
	
		Set<VisualComponent> refreshVisuals(String nodeId)
		throws
			Exception;
	
	
		/** ************************************************************************
		 * Notify clients that the binding has been added, modified, or removed.
		 * This must cause a refresh of the Attribute's Visuals.
		 * Note that the "References" part of the Domain's status panel
		 * needs to be updated as well.
		 *
		 * The Domain to be refreshed is specified by the domainId argument.
		 *
		 * Notify clients that the State is now referenced by another
		 * Domain. This affects the "Referenced By" part of the State's
		 * Domain status panel.
		 *
		 * If the attrId or stateId is null, then refresh all bindings for the Domain.
		 */
		 
		void refreshAttributeStateBinding(String domainId, String attrId, String stateId)
		throws
			Exception;

		
		/** ************************************************************************
		 * Add the specified Node to this View. The Model Engine will be contacted
		 * to obtain the Node's information. Return the Visuals in this View that
		 * are added to depict the specified Node. If the Node is already in the View,
		 * then do nothing and return null. Does not create any Nodes in the server.
		 * Does not add child Nodes of the Node that is added. Also calls showControls
		 * on each ControlPanel, for the type of Control that should be shown, if any.
		 */
	
		Set<VisualComponent> addNode(String nodeId)
		throws
			Exception;


		/** ************************************************************************
		 * Remove the specified Visual from this View. Unsubscribe it from server
		 * events if necessary. Remove child Visuals as well. Remove any Controls
		 * that are associated with the Visual. Remove Visuals for any Nodes that
		 * are child Nodes of visual's Node, and their Controls as well. Does not
		 * remove any Nodes in the server.
		 */
	
		void removeVisual(VisualComponent visual)
		throws
			Exception;
	
	
		/** ************************************************************************
		 * Perform actions that need to occur after a Visual has been populated or
		 * up-populated.
		 */
		 
		void notifyVisualComponentPopulated(VisualComponent visual, boolean populated);
		
		
		/** ************************************************************************
		 * Called after the Visual has been updated with its new name, to notify
		 * this View so that it can change features that depend on the Scenario's
		 * name, such as the Scenario's tab title, etc.
		 */
		 
		void notifyScenarioNameChanged(ScenarioVisual scenVis, String newName);
		
		
		/** ************************************************************************
		 * Call to inform this View that a CrossReference has been created, and so
		 * the View should determine if the CrossReference should be depicted, either
		 * in this View or in an enclosing MultiHierarchyPanel.
		 */
		 
		void notifyCrossReferenceCreated(CrossReferenceSer crSer);
			
			
		/** ************************************************************************
		 * Call to inform this View that a CrossReference that the View or an enclosing
		 * MultiHierarchyPanel might depict has been deleted.
		 */
		 
		void notifyCrossReferenceDeleted(String fromNodeId, String toNodeId, String crossRefId);
	}
	
	
	interface PeerNotice extends Serializable
	{
		/** If true, notification is always for a Visual and therefore notify(Visual)
			will be called. Otherwise, the client may respond in a way that is 
			not specific to a particular Visual. */
		boolean isAlwaysForAVisual();
			
			
		/** To determine the order of this message relative to other messages.
			Each server defines its own message sequence. Message sequences
			start again from zero when a server is restarted, or when the
			maximum sequence number is reached. */
		int getMessageSequenceNo();
		
		String getPeerId();
		
		void notify(VisualComponent visComp)
		throws
			InconsistencyError;  // if the event no longer can be applied to
				// the component, perhaps because server notification was
				// received after the component was changed again.
	}
	
	
	/*interface DomainCreatedNotice extends PeerNotice
	{
		String getNewDomainId();
		
		void notify(Visual.DomainSpaceVisual ds)
		throws
			InconsistencyError;
	}*/
	
	
	/**
	 * Dispatches Peer Notices from the server to the Peer Listener (proxies)
	 * so that the notices are sent to the clients that are interested in the Node
	 * identified in the Peer Notice. An implementation can achieve this by
	 * creating a separate Peer for each Node for which there is interest.
	 */
	 
	interface Peer
	{
		/**
		 * Send the specified Peer Notice to each registered Peer Listener that
		 * is interested in the Node referenced by the Peer Notice.
		 */
		 
		void notifyAllListeners(PeerNotice peerNotice) throws Exception;
	}
	
	
	/**
	 * Callback provided by client to server, for sending notices. The client
	 * sends a Peer Listener (proxy) with the server by calling 
	 * ModelEngine.registerPeerListener,  and the server returns a Listener Registrar 
	 * that the client then uses to subscribe to particular Nodes.
	 */
	 
	interface PeerListener extends Remote
	{
		/** Unique ID (in the space of the client) for the PeerListener. */
		String getId() throws IOException;
		
		//Scenario getScenario();
		
		//void setScenario(Scenario s);
		
		void notify(PeerNotice notice)
		throws
			NotInterested,
			InconsistencyError,
			IOException;  // If this method is thrown on several successive calls
				// then it should be assumed that the PeerListener is no longer
				// accessible. In that case, the client should be required to
				// re-register the PeerListener.
	}
	
	
	/**
	 * For clients to call (remotely) to subscribe to notices about Nodes.
	 */
	 
	interface ListenerRegistrar extends Remote
	{
		/**
		 * Register to be notified of changes pertaining to the specified PersistentNode.
		 * If subscr is false, then unsubscribe.
		 */
		 
		void subscribe(String nodeId, boolean subscr)
		throws
			CannotObtainLock,
			ElementNotFound,
			ModelContainsError,
			ParameterError,
			InternalEngineError,
			IOException;
		
		
		/**
		 * Register to be notified of changes pertaining to the specified PersistentNode,
		 * but only those changes that manifest in the context of the specified Scenario,
		 * such as changes to final State values. If subscr is false, then unsubscribe
		 * rather than subscribe.
		 */
		 
		void subscribe(String nodeId, String scenarioName, boolean subscr)
		throws
			CannotObtainLock,
			ElementNotFound,
			ModelContainsError,
			ParameterError,
			InternalEngineError,
			IOException;
			
			
		/**
		 * Unsubscribe the client-side PeerListener from any and all PersistentNodes.
		 */
		 
		void unsubscribeAll()
		throws
			IOException;
	}


	/**
	 * An event can no longer can be applied to a client-side Component, perhaps
	 * because server notification was received after the component was changed 
	 * again.
	 */
	 
	public class InconsistencyError extends BaseException
	{
		public InconsistencyError() { super("Inconsistency between server and user view"); }
		public InconsistencyError(Throwable t) { super("Inconsistency: " + t.getMessage()); }
		public InconsistencyError(String msg) { super("Inconsistency: " + msg); }
		public InconsistencyError(String msg, Throwable t) { super("Inconsistency: " + msg + ": " + t.getMessage(), t); }
	}
	
	
	public class NotInterested extends BaseException
	{
		public NotInterested() { super("Client not interested in this Node"); }
		public NotInterested(Throwable t) { super("Not interested: " + t.getMessage()); }
		public NotInterested(String msg) { super("Not interested: " + msg); }
		public NotInterested(String msg, Throwable t) { super("Not interested: " + msg + ": " + t.getMessage(), t); }
	}
}

