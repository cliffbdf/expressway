/**
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


import expressway.common.ModelAPITypes.*;
import javax.servlet.ServletException;


public class URLHelper
{
	/**
	 * Parse the specified HTTP request string (the part following the host and port)
	 * and return its parts in a String array with these three elements:
	 *	viewName
	 *	formatName
	 *	queryString
	 * Return the Node Id arguments in the String array argument 'nodeIds'. The 'nodeIds'
	 * argument must be a String array with a length of at least 2.
	 */
	 
	public static String[] getRequestStringParts(String reqStr, String queryString,
		String[] nodeIds)
	throws
		ParameterError  // If 'nodeIds' is not large enough to hold the Node Id arguments.
	{
		String[] parts = reqStr.split("\\:");
		if (parts.length < 1) throw new ParameterError("Empty request");

		
		// Parse format.
		
		String format = null;
		if (parts.length > 2) throw new ParameterError("More than one ':' in request");
		if (parts.length > 1) format = parts[1];
		
		
		// Parse view type.
		
		String viewSpec = parts[0];  //  [ <view-type> ] ( <object-id>+ )
		int openParenPos = viewSpec.indexOf('(');
		if (openParenPos < 0) throw new ParameterError("Expected '('");
		String viewName = "";
		String formatName = viewSpec.substring(0, openParenPos);
		
		
		// Parse object Id arguments.
		
		int closeParenPos = viewSpec.indexOf(')', openParenPos);
		if (closeParenPos < 0) throw new ParameterError("Expected ')'");
		String argsStr = viewSpec.substring(openParenPos+1, closeParenPos);
		String[] args = argsStr.split(",");
		if (nodeIds.length < args.length) throw new ParameterError(
			"nodeIds array is not long enough to hold all of the Node Id arguments");
		
		int i = 0;
		for (String nodeId : nodeIds)
		{
			if (i < args.length) nodeIds[i++] = nodeId.trim();
			else nodeIds[i++] = null;
		}
		
		return new String[] { viewName, formatName, queryString };
	}
	
	
	public static String createExpresswayURLString(String viewType, String serverName, String portStr,
		String... nodeIds)
	{
		String urlStr = "expressway://" + serverName + ":" + portStr + "/" + viewType + "(";
		boolean first = true;
		for (String nodeId : nodeIds)
		{
			if (first) first = false;
			else urlStr += ",";
			urlStr += nodeId;
		}
		
		urlStr += ")";
		
		return urlStr;
	}
	
	
	public static String createWebURLString(String serverName, String portStr,
		String format, String[] nodeIds, String... queryStrings)
	{
		String urlStr = "http://" + serverName + ":" + portStr + "/(";
		
		boolean first = true;
		for (String nodeId : nodeIds)
		{
			if (first) first = false;
			else urlStr += ",";
			urlStr += nodeId;
		}
		
		urlStr += ("):" + format);
		
		first = true;
		for (String query : queryStrings)
		{
			if (first)
			{
				first = false;
				urlStr += "?";
			}
			else
				urlStr += ";";
			
			urlStr += query;
		}
		
		return urlStr;
	}
	
	
	public static String getListURL(String nodeId)
	{
		return "list(" + nodeId + ")";
	}
	
	
	public static String getXMLURL(String nodeId)
	{
		return "xml(" + nodeId + ")";
	}
}



