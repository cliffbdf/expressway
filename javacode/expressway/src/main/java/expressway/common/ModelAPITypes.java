/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.ser.NodeSer;
import expressway.generalpurpose.DateAndTimeUtils;
import expressway.generalpurpose.ThrowableUtil;
import java.io.Serializable;
import java.io.IOException;
import java.awt.Color;
import org.w3c.dom.Element;


public class ModelAPITypes
{
  /* ***************************************************************************
	 * *************************************************************************
	 * Constants.
	 */

	public static final String StandaloneClientId = "StandaloneClient";
	
	public static final String RMIServerClientId = "RMIServerClient";
	
	public static final String LocalServerClientId = "LocalServer";
	
	public static final String ServletClientId = "Servlet";
	
 	//public static final Color TranslucentGray = new Color(0, 0, 0, 127);
	public static final Color TransparentWhite = new Color(255, 255, 255, 0);
	public static final Color TranslucentWhite = new Color(255, 255, 255, 127);
	public static final Color TranslucentGreen = new Color(0, 255, 0, 127); // for test only.
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Enumeration types.
	 */

	public enum StatOptionsType { mean, median, geomean, mode, max, min, sd, gamma }
	
	
	public enum TimePeriodType 
	{ unspecified, daily, weekly, monthly, quarterly, semiannually, annually };
	
	
	public static long getMsForTimePeriod(TimePeriodType perType)
	throws
		UndefinedValue
	{
		final long MsInADay = DateAndTimeUtils.MsInADay;
		final long MsInAYear = MsInADay * 365;
		
		switch (perType)
		{
			case unspecified: throw new UndefinedValue();
			
			case daily: return MsInADay;
			
			case weekly: return MsInADay * 7;
			
			case monthly: return MsInAYear / 12;
			
			case quarterly: return MsInAYear / 4;
			
			case semiannually: return MsInAYear / 2;
			
			case annually: return MsInAYear;
			
			default: throw new RuntimeException("Unexpected value for TimePeriodType");
		}
	}
	
	
	public enum StatCorrelationType { cc, cv }
	
	
	public enum PortDirectionType { bi, input, output }
	
	
	/** A side of a Node. */
	public enum Position { top, bottom, left, right }
	
	
	public enum EditActionType { create, delete, insertBefore, insertAfter, modify,
		cut, copy, paste, find, help, undo };
	

	//public enum AttrStateBindingType
	//{
	//	average,	// apply the average value of the State to this Attribute.
	//	most_recent,// apply the most recent value of the State to this Attribute.
	//	expression  // use an expression that is stored in the Binding.
	//}

	
	/** ************************************************************************
	 * General purpose externalizable table for mapping IDs to values.
	 */
	
	public static class TableEntry implements Serializable, Cloneable
	{
		public String nodeId;
		public Object mappedValue;


		public TableEntry(String nodeId, Object mappedValue)
		{
			this.nodeId = nodeId;
			this.mappedValue = mappedValue;
		}
		
		
		public Object deepCopy()
		{
			TableEntry entry = null;
			try { entry = (TableEntry)(this.clone()); }
			catch (CloneNotSupportedException ex) { throw new RuntimeException(ex); }
		
			entry.mappedValue = null;
			return entry;
		}
	}


	public static class InterpretationError extends RuntimeException
	{
		public InterpretationError(String msg) { super(msg); }
	}
	
	
	public static class NullError extends RuntimeException
	{
		public NullError(String msg) { super(msg); }
	}
	
	
	/** ************************************************************************
	 * Base exception type for all application-level server-side exceptions that
	 * are propagated to the client.
	 */
	
	public abstract static class BaseException extends Exception implements Serializable
	{
		public BaseException() { super(); }
		public BaseException(String msg) { super(msg); }
		public BaseException(String msg, Throwable t) { super(msg, t); }
		public BaseException(Throwable t) { super(t); }
	}


	/** ************************************************************************
	 * Notify the client that the operation has risks. The client can choose to
	 * override the warning by passing true for a 'confirm' argument.
	 */
	
	public static class Warning extends BaseException
	{
		public Warning() { super(); }
		public Warning(String msg) { super(msg); }
		public Warning(String msg, Throwable t) { super(msg, t); }
		public Warning(Throwable t) { super(t); }
	}
	
	
	/** ************************************************************************
	 * Indicates that a field of a Node has not been set, with the implication
	 * that an operation that requries that field to be set cannot be completed.
	 * Fields are allowed to remain unset to allow the incremental assembly of
	 * models, especially by instantiating Templates: a Template cannot always
	 * anticipate how all fields of an intantiated TemplateInstance should be
	 * set, and so the user will have to set those fields as a separate action,
	 * perhaps at a much later point in time. When an attempt is made to access
	 * a field that has not been set, a ValueNotSet warning should be thrown by
	 * the accessor method.
	 */
	 
	public static class ValueNotSet extends Warning
	{
		public ValueNotSet() { super(); }
		public ValueNotSet(String msg) { super(msg); }
		public ValueNotSet(String msg, Throwable t) { super(msg, t); }
		public ValueNotSet(Throwable t) { super(t); }
	}
	
	
	/** ************************************************************************
	 * When performing semantic processing on Expressway XML.
	 */
	 
	public static class GenException extends BaseException
	{
		public GenException() { super("Parse Exception"); }
		
		public GenException(String msg) { super(msg); }
		
		public GenException(String msg, Throwable t) { super(msg, t); }
		
		public GenException(Throwable t) { super(t); }
		
		public GenException(Element element, String msg)
		{
			super(
				"At element <" + element.getTagName() + 
				(element.getAttribute("name").equals("") ? "" :
					(" name=\"" + element.getAttribute("name") + "\"")) + 
				">, " +
				msg);
		}
		
		public GenException(Element element, String msg, Throwable t)
		{
			super(
				"At element <" + element.getTagName() + 
				(element.getAttribute("name").equals("") ? "" :
					(" name=\"" + element.getAttribute("name") + "\"")) + 
				">, " +
				msg, t);
		}
		
		public GenException(Element element, Throwable t)
		{
			super(
				"At element <" + element.getTagName() + 
				(element.getAttribute("name").equals("") ? "" :
					(" name=\"" + element.getAttribute("name") + "\"")) + 
				">, ", t);
		}
		
		public GenException(Element element, String nodeFullName, String msg,
			Throwable t)
		{
			super(
				"At element <" + element.getTagName() +
				(element.getAttribute("name").equals("") ? "" :
					(" name=\"" + element.getAttribute("name") + "\"")) + 
				">, " +
				(nodeFullName == null? "" : ("for " + nodeFullName + ", ")) +
				(msg == null? "" : msg), 
				t);
		}
		
		public GenException(Element element, String nodeFullName, String msg)
		{
			super(
				"At element <" + element.getTagName() +
				(element.getAttribute("name").equals("") ? "" :
					(" name=\"" + element.getAttribute("name") + "\"")) + 
				">, " +
				(nodeFullName == null? "" : ("for " + nodeFullName + ", ")) +
				(msg == null? "" : msg)
				);
		}
	}
	
	
	/** ************************************************************************
	 * 
	 */
	 
	public static class CannotBeInstantiated extends BaseException
	{
		final static String stdMsg = "Node kind cannot be instanated in this way.";
		public CannotBeInstantiated() { super(stdMsg); }
		public CannotBeInstantiated(String msg) { super(stdMsg + ";\n" + msg); }
		public CannotBeInstantiated(Throwable t) { super(stdMsg, t); }
		public CannotBeInstantiated(String msg, Throwable t) { super(stdMsg + ";\n" + msg, t); }
	}
	
	
	
	/** ************************************************************************
	 * A VisualComponent is not currently visible and so it cannot perform an
	 * action that requires it to be visible.
	 */
	 
	public static class NotVisible extends BaseException implements Serializable
	{
		final static String stdMsg = "Component or row is not currently visible.";
		public NotVisible() { super(stdMsg); }
		public NotVisible(String msg) { super(stdMsg + ";\n" + msg); }
		public NotVisible(Throwable t) { super(stdMsg, t); }
		public NotVisible(String msg, Throwable t) { super(stdMsg + ";\n" + msg, t); }
	}

	
	/** ************************************************************************
	 * Notify client that an input stream is empty.
	 */
	 
	public static class Empty extends IOException
	{
		public Empty() { super("Input is empty"); }
	}


	/** ************************************************************************
	 * Indicates that there is contention for an object that must be accessed, and
	 * the contention caused a timeout or could not be resolved. The client may
	 * re-try the request.
	 */
	
	public static class CannotObtainLock extends BaseException implements Serializable
	{
		final static String stdMsg = "Cannot obtain database lock.";
		public CannotObtainLock() { super(stdMsg); }
		public CannotObtainLock(String msg) { super(stdMsg + ";\n" + msg); }
		public CannotObtainLock(Throwable t) { super(stdMsg, t); }
		public CannotObtainLock(String msg, Throwable t) { super(stdMsg + ";\n" + msg, t); }
	}


	/** ************************************************************************
	 * Indicates that a Model Element that has been requested could not be found.
	 */
	
	public static class ElementNotFound extends BaseException implements java.io.Serializable
	{
		final static String stdMsg = "Element not found.";
		public ElementNotFound() { super(stdMsg); }
		public ElementNotFound(String msg) { super(stdMsg + ";\n" + msg); }
		public ElementNotFound(Throwable t) { super(stdMsg, t); }
		public ElementNotFound(String msg, Throwable t) { super(stdMsg + ";\n" + msg, t); }
	}


	/** ************************************************************************
	 * Indicates that an object that has been requested could not be found.
	 */
	
	public static class ResourceNotFound extends BaseException implements java.io.Serializable
	{
		final static String stdMsg = "Resource not found.";
		public ResourceNotFound() { super(stdMsg); }
		public ResourceNotFound(String msg) { super(stdMsg + ";\n" + msg); }
		public ResourceNotFound(Throwable t) { super(stdMsg, t); }
		public ResourceNotFound(String msg, Throwable t) { super(stdMsg + ";\n" + msg, t); }
	}


	/** ************************************************************************
	 * Indicates that a parameter supplied to a method call is invalid.
	 */
	
	public static class ParameterError extends BaseException implements java.io.Serializable
	{
		final static String stdMsg = "Invalid parameter in call to engine.";
		public ParameterError() { super("Invalid Parameter"); }
		public ParameterError(Throwable t) { super(stdMsg, t); }
		public ParameterError(String msg, Throwable t) { super(stdMsg + ";\n" + msg, t); }
		public ParameterError(String msg) { super(stdMsg + ";\n" + msg); }
	}


	/** ************************************************************************
	 * 
	 */
	
	public static class DisallowedOperation extends BaseException implements java.io.Serializable
	{
		final static String stdMsg = "Operation is not permitted.";
		public DisallowedOperation() { super(stdMsg); }
		public DisallowedOperation(Throwable t) { super(stdMsg, t); }
		public DisallowedOperation(String msg, Throwable t) { super(stdMsg + ";\n" + msg, t); }
		public DisallowedOperation(String msg) { super(stdMsg + ";\n" + msg); }
	}


	/** ************************************************************************
	 * Indicates that the contents of an input stream (such as a file) are corrupt.
	 */
	
	public static class CorruptInput extends BaseException
	{
		public CorruptInput() { super(); }
		public CorruptInput(String msg) { super(msg); }
		public CorruptInput(String msg, Throwable t) { super(msg, t); }
		public CorruptInput(Throwable t) { super(t); }
	}


	/** ************************************************************************
	 * Indicates that the state of a server-side object has changed since the client
	 * last obtained the object. This is usually thrown by remote methods that
	 * attempt to update the server-side state. It indicates that there may be
	 * a conflict between updates from multiple clients.
	 */
	
	public static class ClientIsStale extends BaseException implements java.io.Serializable
	{
		final static String stdMsg = "There have been server-side changes since the client-side view was last refreshed.";
		public ClientIsStale() { super(stdMsg); }
		public ClientIsStale(Throwable t) { super(stdMsg, t); }
		public ClientIsStale(String msg) { super(stdMsg + ";\n" + msg); }
	}


	/** ************************************************************************
	 * Indicates an internal inconsistency or invalid data within a model defined
	 * by a user. Such an error may be detected statically or dynamically (during
	 * simulation).
	 */
	
	public static class ModelContainsError extends BaseException implements java.io.Serializable
	{
		final static String stdMsg = "Model contains error";
		public ModelContainsError() { super(stdMsg); }
		public ModelContainsError(Throwable t) { super(stdMsg, t); }
		public ModelContainsError(String msg) { super(stdMsg + ";\n" + msg); }
		public ModelContainsError(String msg, Throwable t) { super(stdMsg + ";\n" + msg, t); }
	}
	
	
	/** ************************************************************************
	 * The message may contain embedded Node Ids: each must be represented as
	 * "$(id)", where "id" is the Node Id. For example,
	 *		$(ABC)
	 * represents the Node Id "ABC".
	 */
	 
	public static class PatternViolation extends BaseException implements java.io.Serializable
	{
		final static String stdMsg = "Pattern violation";
		public String[] nodeIds;
		
		public PatternViolation(String nodeId, String msg)
		{
			super(msg);
			this.nodeIds = new String[] { nodeId };
		}
		
		public PatternViolation(String[] nodeIds, String msg)
		{
			super(msg);
			this.nodeIds = nodeIds;
		}
		
		
		/** Replace each embedded Node Id in the message with the full name of
			the Node. */
		public String getMessage(ModelEngineRMI modelEngine)
		{
			String s = getMessage();
			int seqNo = 0;
			for (String nodeId : nodeIds)
			{
				seqNo++;
				String keyStr = "$(" + seqNo + ")";
				if (! (s.contains(keyStr))) break;
				
				try
				{
					NodeSer nodeSer = modelEngine.getNode(nodeId);
					s = s.replace(keyStr, nodeSer.getFullName());
				}
				catch (Exception ex)
				{
					s += ex.getMessage();
					break;
				}
			}
			
			return s;
		}
	}
	
	
	/** ************************************************************************
	 * A value is undefined.
	 */
	 
	public static class UndefinedValue extends BaseException
	{
		final static String stdMsg = "Value is undefined.";
		public UndefinedValue() { super(); }
		public UndefinedValue(String msg) { super(stdMsg + "; " + msg); }
		public UndefinedValue(Throwable t) { super(stdMsg, t); }
		public UndefinedValue(String msg, Throwable t) { super(stdMsg + "; " + msg, t); }
	}
	
			
	/** ************************************************************************
	 * To indicate that a simulation or other computed result does not match what
	 * is expected.
	 */
	
	public static class ResultMismatch extends ModelContainsError
	{
		final static String stdMsg = "New results do not match old results.";
		public ResultMismatch() { super(); }
		public ResultMismatch(String msg) { super(stdMsg + "; " + msg); }
		public ResultMismatch(Throwable t) { super(stdMsg, t); }
		public ResultMismatch(String msg, Throwable t) { super(stdMsg + "; " + msg, t); }
	}
	
	
	/** ************************************************************************
	 * 
	 */
	
	public static class ComponentBehaviorError extends ModelContainsError
	{
		final static String stdMsg = "Error in component";
		
		public NodeSer nodeSer;
		public Epoch epoch;
		
		public ComponentBehaviorError(NodeSer nodeSer, Epoch epoch)
		{
			this(stdMsg, nodeSer, epoch);
		}
		
		public ComponentBehaviorError(NodeSer nodeSer, Epoch epoch, Throwable t)
		{
			this(stdMsg, nodeSer, epoch, t);
		}
		
		public ComponentBehaviorError(String msg, NodeSer nodeSer, Epoch epoch)
		{
			super(stdMsg + ";\n" + msg);
			this.nodeSer = nodeSer;
			this.epoch = epoch;
		}
		
		public ComponentBehaviorError(String msg, NodeSer nodeSer, Epoch epoch, Throwable t)
		{
			super(stdMsg + ";\n" + msg, t);
			this.nodeSer = nodeSer;
			this.epoch = epoch;
		}
		
		public String toString()
		{
			return this.getClass().getName() + ", for '" + nodeSer.getFullName() +
				"', in iteration " + epoch.getIteration() + ", " +
				ThrowableUtil.getAllMessages(this);
		}
	}


	/** ************************************************************************
	 * Represents an exception condition that occurs during simulation, such as
	 * the abort of simulation, but that is not an error in a model itself. 
	 * This should not be used to represent errors in the simulator engine itself.
	 */

	public static class SimulationError extends Exception
	{
		public SimulationError(String msg) { super(msg); }
		
		public String toString()
		{
			return this.getClass().getName() + ": " + ThrowableUtil.getAllMessages(this);
		}
		
		public static String getMessagePrefix() { return messagePrefix; }
		
		private static final String messagePrefix = "Simulation Error: ";
		
		public static SimulationError parseSimulationErrorString(String simErrorAsString)
		{
			if (! (simErrorAsString.startsWith(messagePrefix))) 
				throw new IllegalArgumentException("Invalid SimulationError String");
			
			String msg = simErrorAsString.substring(messagePrefix.length());
			
			return new SimulationError(msg);
		}
	}


	/** ************************************************************************
	 * 
	 */
	
	public static class CallbackNotFound extends ParameterError
	{
		public CallbackNotFound() { this(null); }
		public CallbackNotFound(Throwable t) { super("Simulation ID not found.", t); }
	}
	
	
	/** ************************************************************************
	 * 
	 */
	
	public static class CannotModifyDuringSimulation extends CannotObtainLock
	{
		final static String stdMsg = "Cannot modify object while it is being used in a simulation.";
		public CannotModifyDuringSimulation() { super(stdMsg); }
		public CannotModifyDuringSimulation(String msg) { super(stdMsg + ";\n" + msg); }
		public CannotModifyDuringSimulation(Throwable t) { super(stdMsg, t); }
		public CannotModifyDuringSimulation(String msg, Throwable t) { super(stdMsg + ";\n" + msg, t); }
	}
	
	
	/** ************************************************************************
	 * Indicates an internal error occurred within the server, indicating a bug.
	 * The server can also throw a RuntimeException instead of an InternalEngineError.
	 */
	
	public static class InternalEngineError extends RuntimeException implements java.io.Serializable
	{
		final static String stdMsg = "Internal error within Expressway.";
		public InternalEngineError() { super(stdMsg); }
		public InternalEngineError(Throwable t) { super(stdMsg, t); }
		public InternalEngineError(String msg) { super(stdMsg + ";\n" + msg); }
	}
}
