/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.common.ModelAPITypes.*;


/**
 * Defines hints for the scaling of the model space to the typical View space.
 * This is used to determine, for example, if two Ports are so close that they
 * will appear to overlap when viewed. Each Domain may have a VisualGuidance
 * associated with it, or it may use the default DefaultVisualGuidance.
 */
 
public class DefaultVisualGuidance implements VisualGuidance, java.io.Serializable, Cloneable
{
	public double getProbablePixelSize() { return 1.0; }
	public double getProbablePortDiameter() { return 16.0 * getProbablePixelSize(); }
	public VisualGuidance makeCopy()
	{
		try { return (VisualGuidance)(clone()); }
		catch (CloneNotSupportedException ex) { throw new RuntimeException(ex); }
	}

	
	public static VisualGuidance guidance = new DefaultVisualGuidance();
}

