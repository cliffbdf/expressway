/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.common.ClientModel.*;
import expressway.common.VisualComponent.*;
import java.util.*;


/**
 * Message types for the server to notify clients that Nodes have changed.
 * Each message contains a notify(VisualComponent) method. A PeerNotice
 * subclass should implement that method. The method will be called by the
 * client when the client receives the message. Clients instantiate PeerListeners
 * that receive PeerNotices.
 */
 
public abstract class PeerNoticeBase implements PeerNotice, Comparable
{
	public static final int LowestPeerNoticeNumber = 0;
	private static int mostRecentAssignedSequenceNumber = LowestPeerNoticeNumber;
	public final int sequenceNumber;
	
	/** The Id of the outermost Node that is affected (in the model, not in Views
		of the model) by the change. */
	public final String peerId;
	
	
	public PeerNoticeBase(String peerId)
	{
		synchronized (PeerNoticeBase.class)
		{
			mostRecentAssignedSequenceNumber = 
				(mostRecentAssignedSequenceNumber + 1) % Integer.MAX_VALUE;
				
			this.sequenceNumber = mostRecentAssignedSequenceNumber;
		}
		
		this.peerId = peerId;
	}

	
	public int compareTo(Object obj)
	{
		if (! (obj instanceof PeerNotice)) throw new RuntimeException(
			"Can only compare a PeerNotice to another PeerNotice");
		
		PeerNotice otherNotice = (PeerNotice)obj;
		
		int otherSeqNo = otherNotice.getMessageSequenceNo();
		
		if (this.sequenceNumber < otherSeqNo) return -1;
		if (this.sequenceNumber == otherSeqNo) return 0;
		if (this.sequenceNumber > otherSeqNo) return 1;
		throw new RuntimeException("Should not happen");
	}
	
	
	/** Each subclass may override this to indicate that the constructor argument
		may be null. */
	public boolean isAlwaysForAVisual() { return true; }
	
	
	public abstract void notify(VisualComponent visComp) throws InconsistencyError;


	public int getMessageSequenceNo() { return sequenceNumber; }
	
	
	public String getPeerId() { return peerId; }
	
	
	public String toString()
	{
		return this.getClass().getName() + ": peerId=" + peerId;
	}
	
	
	//public void notify(VisualComponent visComp) 
	//throws InconsistencyError 
	//{ GlobalConsole.println("Notifying " + visComp.getNodeId()); }
	
	
	/*public static class ModelElementCreatedNotice extends PeerNoticeBase
	{
		private String nodeId;
		
		public ModelElementCreatedNotice(String parentNodeId, String nodeId)
		{
			super(parentNodeId);
			this.nodeId = nodeId;
		}
		
		public void notify(VisualComponent parentVisComp) throws InconsistencyError
		{
			if (! (parentVisComp instanceof ModelContainerVisual))
				throw new RuntimeException(
					"ModelElementCreatedNotice sent to a Visual that is not a ModelContainerVisual");
					
			((ModelContainerVisual)parentVisComp).modelElementAdded(nodeId);
		}
	}*/


	public static class DomainCreatedNotice extends PeerNoticeBase// implements DomainCreatedNotice
	{
		private String newDomainId;
		
		
		public DomainCreatedNotice(String newDomainId)
		{
			super(null);
			this.newDomainId = newDomainId;
		}
		
		
		public boolean isAlwaysForAVisual() { return false; }
		
		
		public String getNewDomainId() { return newDomainId; }
						
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			throw new RuntimeException("Should not be called");
		}
	
	
		public void notify(VisualComponent.DomainSpaceVisual ds)
		throws
			InconsistencyError
		{
			if (! (ds instanceof DomainSpaceVisual)) return;
			ds.domainCreated(newDomainId);
		}
		
		
		public String toString()
		{
			return super.toString() + "; newDomainId=" + newDomainId;
		}
	}
	
	
	public static class MotifCreatedNotice extends PeerNoticeBase
	{
		public String motifId;
		
		public MotifCreatedNotice(String motifId)
		{
			super(null);
			this.motifId = motifId;
		}
		
		
		public boolean isAlwaysForAVisual() { return false; }
		
		
		public String getNewMotifId() { return motifId; }
						
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			throw new RuntimeException("Should not be called");
		}
	
	
		public void notify(VisualComponent.DomainSpaceVisual ds)
		throws
			InconsistencyError
		{
			if (! (ds instanceof DomainSpaceVisual)) return;
			ds.motifCreated(motifId);
		}


		public String toString()
		{
			return super.toString() + "; motifId=" + motifId;
		}
	}
	
	
	public static class MotifUseChangedNotice extends PeerNoticeBase
	{
		public String motifDefId;
		public boolean uses;
		
		public MotifUseChangedNotice(String domainId, String motifDefId, boolean uses)
		{
			super(domainId);
			if (domainId == null) throw new RuntimeException("Domain Id is null");
			this.motifDefId = motifDefId;
			this.uses = uses;
		}
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			if (visComp instanceof DomainVisual)
			{
				if (uses) ((DomainVisual)visComp).motifAdded(motifDefId);
				else ((DomainVisual)visComp).motifRemoved(motifDefId);
			}
		}
	}
	
	
	public static class NodeChangedNotice extends PeerNoticeBase
	{
		public NodeChangedNotice(String nodeId)
		{
			super(nodeId);
		}
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			visComp.refresh();
		}
	}
	
	
	public static class ModelScenarioSimTimeChangedNotice extends PeerNoticeBase
	{
		public Date newFromDate;
		public Date newToDate;
		public long newDuration;
		public int newIterLimit;
		
		
		public ModelScenarioSimTimeChangedNotice(String modelScenarioNodeId,
			Date newFromDate, Date newToDate, long newDuration, int newIterLimit)
		{
			super(modelScenarioNodeId);
			this.newFromDate = newFromDate;
			this.newToDate = newToDate;
			this.newDuration = newDuration;
			this.newIterLimit = newIterLimit;
		}
		
		public void notify(VisualComponent modelScenarioVisual) throws InconsistencyError
		{
			((ModelScenarioVisual)modelScenarioVisual).simTimeParamsChanged(
				newFromDate, newToDate, newDuration, newIterLimit);
		}
	}
	
	
	/**
	 * An Attribute has been bound to (or unbound from) the specified State.
	 * (The Attribute and State are in different Domains.)
	 */
	 
	public static class AttributeStateBindingChanged extends PeerNoticeBase
	{
		public String attrId = null;
		public String stateId = null;
		
		
		/** Refresh the specified binding. */
		
		public AttributeStateBindingChanged(String domainId, String attrId, String stateId)
		{
			super(domainId);  // The Domain to be notified. Note that both the
				// Attribute's Domain and the State's Domain must be notified.
			
			if (attrId == null) throw new RuntimeException("Attribute Id is null");
			if (stateId == null) throw new RuntimeException("State Id is null");
			
			this.attrId = attrId;
			this.stateId = stateId;
		}
		
		
		/** Refresh all bindings. */
		
		public AttributeStateBindingChanged(String domainId)
		{
			super(domainId);
		}
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			// Notify clients that the binding has been added.
			// This must cause a refresh of the Attribute's Visuals.
			// Note that the "References" part of the Domain's status panel
			// needs to be updated as well.
			
			// Notify clients that the State is now referenced by another
			// Domain. This affects the "Referenced By" part of the State's
			// Domain status panel.
	
			if (this.attrId == null)  // Notify both the Attribute's Domain and
				// the State's Domain that all bindings must be refreshed, because
				// the way that the specified Domain treats bindings has changed.
			{
				Set<VisualComponent> domainVisuals = null;
				try { domainVisuals =
					visComp.getClientView().identifyVisualComponents(this.peerId); }
				catch (Exception ex)
				{
					GlobalConsole.printStackTrace(ex);
					return;
				}
				
				for (VisualComponent domainVisual : domainVisuals)
				{
					try
					{
						domainVisual.refresh();
					}
					catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
				}
			}
			else
			{
				try { visComp.getClientView().refreshAttributeStateBinding(this.peerId, this.attrId,
					this.stateId); }
				catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
			}
		}
	}


	/**
	 * Notice contains Id of both the new Node and its parent Node. The View 
	 * identifies each Visual that depicts the parent Node, and calls addNode, 
	 * supplying the new Node's Id as the argument. Do not use for CrossReferences.
	 */
	 
	public static class NodeAddedNotice extends PeerNoticeBase
	{
		private String newNodeId;
		
		
		public NodeAddedNotice(String parentNodeId, String newNodeId)
		{
			super(parentNodeId);
			this.newNodeId = newNodeId;
		}
		
		public void notify(VisualComponent parentVisComp) throws InconsistencyError
		{
			try
			{
				parentVisComp.getClientView().addNode(newNodeId);
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		}
		
		public String getNewNodeId() { return newNodeId; }
	}


	/**
	 * Do not use for CrossReferences.
	 */
	 
	public static class NodeDeletedNotice extends PeerNoticeBase
	{
		public NodeDeletedNotice(String peerId)
		{
			super(peerId);
		}


		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			visComp.nodeDeleted();
		}
	}
	
	
	public static class CrossReferenceAddedNotice extends PeerNoticeBase
	{
		private String toNodeId;
		private String crossRefId;
		
		public CrossReferenceAddedNotice(String fromNodeId, String toNodeId,
			String crossRefId)
		{
			super(fromNodeId);
			this.toNodeId = toNodeId;
			this.crossRefId = crossRefId;
		}
		
		
		public void notify(VisualComponent fromVisual) throws InconsistencyError
		{
			fromVisual.notifyCrossReferenceCreated(toNodeId, crossRefId);
		}
	}


	public static class CrossReferenceDeletedNotice extends PeerNoticeBase
	{
		private String toNodeId;
		private String crossRefId;
		
		public CrossReferenceDeletedNotice(String fromNodeId, String toNodeId,
			String crossRefId)
		{
			super(fromNodeId);
			this.toNodeId = toNodeId;
			this.crossRefId = crossRefId;
		}
		
		
		public void notify(VisualComponent fromVisual) throws InconsistencyError
		{
			fromVisual.notifyCrossReferenceDeleted(toNodeId, crossRefId);
		}
	}


	public static class SimulationRunsDeletedNotice extends PeerNoticeBase
	{
		public SimulationRunsDeletedNotice(String modelDomainOrScenarioId)
		{
			super(modelDomainOrScenarioId);
		}
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			if (visComp instanceof ModelDomainVisual)
				((ModelDomainVisual)visComp).simulationRunsDeleted();
			else if (visComp instanceof ModelScenarioVisual)
				((ModelScenarioVisual)visComp).simulationRunsDeleted();
			else
				(new Exception("A SimulationRunsDeletedNotice was received for a " +
					"Node that is not a ModelDomain or a ModelScenario.")).
					printStackTrace();
		}
	}
	

	public static class NameChangedNotice extends PeerNoticeBase
	{
		public String newName;
		public String newFullName;
		
		
		public NameChangedNotice(String peerId, String newName, String newFullName)
		{
			super(peerId);
			this.newName = newName;
			this.newFullName = newFullName;
		}


		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			visComp.nameChangedByServer(this.newName, this.newFullName);
		}
	}
	

	/**
	 * The View identifies each Visual that depicts the Node, and calls setLocation 
	 * for each.
	 */
	 
	public static class NodeMovedNotice extends PeerNoticeBase
	{
		public NodeMovedNotice(String peerId)
		{
			super(peerId);
		}
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			visComp.refreshLocation();
		}
	}
	

	/**
	 * The View identifies each Visual that depicts the Node, and calls refresh 
	 * for each.
	 */
	 
	public static class NodeResizedNotice extends PeerNoticeBase
	{
		public NodeResizedNotice(String peerId)
		{
			super(peerId);
		}
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			visComp.refresh();
		}
	}
	
	
	/**
	 * When a Node changes in some way that might affect its appearance or
	 * behavior, but its children are not changed in any way.
	 */
	 
	public static class NonStructuralChangeNotice extends PeerNoticeBase
	{
		public NonStructuralChangeNotice(String nodeId)
		{
			super(nodeId);
		}
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			visComp.refresh();  // can be made more efficient.
		}
	}
	

	public static class AttributeValueUpdated extends PeerNoticeBase
	{
		public String attrId;
		
		
		public AttributeValueUpdated(String modelScenarioId, String attrId)
		{
			super(modelScenarioId);
			this.attrId = attrId;
		}
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			ModelScenarioVisual modelScenarioVisual = null;
			try { modelScenarioVisual = (ModelScenarioVisual)visComp; }
			catch (ClassCastException ex) { GlobalConsole.printStackTrace(ex); return; }
			
			try { modelScenarioVisual.refreshAttributeValue(attrId); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		
		
		public String toString()
		{
			return super.toString() + "; attrId=" + attrId;
		}
	}


	public static class ScenarioCreatedNotice extends PeerNoticeBase
	{
		public String newScenarioId;
		//public String currentScenarioId;
		
		
		public ScenarioCreatedNotice(String domainId, String newScenarioId
			//,String currentSceanrioId
			)
		{
			super(domainId);
			this.newScenarioId = newScenarioId;
			//this.currentScenarioId = currentScenarioId;
		}


		public boolean isAlwaysForAVisual() { return false; }
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			// debug
			GlobalConsole.println("in notify; visComp is a " + visComp.getClass().getName() + "...");
			// end debug
			
			
			if (visComp instanceof DomainVisual)
			{
				// debug
				GlobalConsole.println("...about to call showScenarioCreated...");
				// end debug
				
				
				DomainVisual domainVisual = (DomainVisual)visComp;
				domainVisual.showScenarioCreated(this.newScenarioId);


				// debug
				GlobalConsole.println("...called showScenarioCreated on " +
					domainVisual.getName() + "(" + domainVisual.getClass().getName() + ").");
				// end debug
			}
			else  // An outermost Node. Nevertheless, notify the View.
			{
				visComp.getClientView().showScenarioCreated(this.newScenarioId);
			}
		}
		
		
		public void notify(VisualComponent.DomainSpaceVisual ds)
		throws
			InconsistencyError
		{
			ds.showScenarioCreated(this.newScenarioId);
		}
		
		
		public String toString()
		{
			return super.toString() + ";new ScenarioId=" + newScenarioId;
		}
	}
	
	
	public static class ScenarioSetCreatedNotice extends PeerNoticeBase
	{
		public String newScenarioSetId;
		
		
		public ScenarioSetCreatedNotice(String scenarioId, String newScenSetId)
		{
			super(scenarioId);
			this.newScenarioSetId = newScenSetId;
		}
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			if (visComp instanceof ModelDomainVisual)
			{
				ModelDomainVisual domVis = (ModelDomainVisual)visComp;
				
				domVis.showScenarioSetCreated(this.newScenarioSetId);
			}
		}
		
		
		public void notify(VisualComponent.DomainSpaceVisual ds)
		throws
			InconsistencyError
		{
			ds.showScenarioSetCreated(this.newScenarioSetId);
		}
		
		
		public String toString()
		{
			return super.toString() + ";new ModelScenarioSetId=" + newScenarioSetId;
		}
	}
	
	
	public static class ScenarioBindingRemoved extends PeerNoticeBase
	{
		public String unboundDomainId;
		
		
		public ScenarioBindingRemoved(String bindingDomainId, String unboundDomainId)
		{
			super(bindingDomainId);
			this.unboundDomainId = unboundDomainId;
		}
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			if (! (visComp instanceof ScenarioVisual)) return;
			ScenarioVisual scenVis = (ScenarioVisual)visComp;
			
			Set<VisualComponent> bindingDomainVisuals = null;
			try { bindingDomainVisuals =
				visComp.getClientView().identifyVisualComponents(this.peerId); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
				return;
			}
			
			for (VisualComponent domainVisual : bindingDomainVisuals)
			{
				try { domainVisual.refresh(); }
				catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
			}
			
			Set<VisualComponent> unboundDomainVisuals = null;
			try { unboundDomainVisuals =
				visComp.getClientView().identifyVisualComponents(this.unboundDomainId); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
				return;
			}
			
			for (VisualComponent domainVisual : unboundDomainVisuals)
			{
				try { domainVisual.refresh(); }
				catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
			}
		}
	}
	
	
	public static class ScenarioBindingCreated extends PeerNoticeBase
	{
		public String boundDomainId;
		
		public ScenarioBindingCreated(String bindingDomainId, String boundDomainId)
		{
			super(bindingDomainId);
			this.boundDomainId = boundDomainId;
		}
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			if (! (visComp instanceof ScenarioVisual)) return;
			ScenarioVisual scenVis = (ScenarioVisual)visComp;
			
			Set<VisualComponent> bindingDomainVisuals = null;
			try { bindingDomainVisuals =
				visComp.getClientView().identifyVisualComponents(this.peerId); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
				return;
			}
			
			for (VisualComponent domainVisual : bindingDomainVisuals)
			{
				try { domainVisual.refresh(); }
				catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
			}
			
			Set<VisualComponent> boundDomainVisuals = null;
			try { boundDomainVisuals =
				visComp.getClientView().identifyVisualComponents(this.boundDomainId); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
				return;
			}
			
			for (VisualComponent domainVisual : boundDomainVisuals)
			{
				try { domainVisual.refresh(); }
				catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
			}
		}
	}


	/**
	 * Notification to clients that the Conduit has changed and needs to be
	 * redrawn.
	 *
	 
	public static class ConduitChangePeerNotice extends PeerNoticeBase
	{
		public ConduitChangePeerNotice(ConduitSer conduitSer)
		{
			super(conduitSer.getNodeId());
		}
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			ConduitVisual conduitVisual = (ConduitVisual)visComp;
			conduitVisual.refresh();
		}
	}
	*/
	
	
	
	public static class ImportErrorNotice extends PeerNoticeBase
	{
		public String errorMessage;
		
		
		public ImportErrorNotice(String errorMessage)
		{
			super(null);
			this.errorMessage = errorMessage;
		}


		public boolean isAlwaysForAVisual() { return false; }
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			throw new RuntimeException("Should not be called");
		}
	
	
		public void notify(VisualComponent.DomainSpaceVisual ds)
		throws
			InconsistencyError
		{
			GlobalConsole.println("Import error: " + errorMessage);
		}
		
		
		public String getImportError() { return errorMessage; }
	}
	
	
	public static class ImportWarningNotice extends PeerNoticeBase
	{
		public String warningMessage;
		
		
		public ImportWarningNotice(String warningMessage)
		{
			super(null);
			this.warningMessage = warningMessage;
		}


		public boolean isAlwaysForAVisual() { return false; }
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			throw new RuntimeException("Should not be called");
		}
	
	
		public void notify(VisualComponent.DomainSpaceVisual ds)
		throws
			InconsistencyError
		{
			GlobalConsole.println("Import error: " + warningMessage);
		}
		
		
		public String getImportWarning() { return warningMessage; }
	}
	
	
	
   /* **************************************************************************
  	* Notices pertaining to simulation.
	*/
	
	
	/**
	 * Notify clients that (1) a SimulationRun exists for the specified Scenario,
	 * and (2) that SimulationRun has the specified absolute progress.
	 */
	 
	public static class ScenarioSimProgressNotice extends PeerNoticeBase
	{
		public Integer requestId;
		public String simNodeId;
		public int totalEpochsCompleted;
		public long totalTimeElapsed;
		public boolean completed = false;
		
		
		public ScenarioSimProgressNotice(Integer requestId,
			String visualNodeId,  // a ModelScenario Id or a ModelScenarioSet Id.
			String simNodeId, int totalEpochsCompleted,
			long totalTimeElapsed)
		{
			super(visualNodeId);
			
			this.requestId = requestId;
			this.simNodeId = simNodeId;
			this.totalEpochsCompleted = totalEpochsCompleted;
			this.totalTimeElapsed = totalTimeElapsed;
		}
		
		
		public ScenarioSimProgressNotice(Integer requestId,
			String visualNodeId,  // a ModelScenario Id or a ModelScenarioSet Id.
			String simNodeId, boolean completed)
		{
			super(visualNodeId);
			
			this.requestId = requestId;
			this.simNodeId = simNodeId;
			this.completed = completed;
		}
		
		
		/** Call this to specify that the simulation has completed. */
		public void setCompleted(boolean completed)
		{
			this.completed = completed;
		}
		
		
		public void notify(VisualComponent visual) throws InconsistencyError
		{
			Set<SimulationVisual> simVisuals = null;
			
			if (visual instanceof ModelScenarioVisual)
			{
				ModelScenarioVisual modelScenarioVisual = (ModelScenarioVisual)visual;

				simVisuals = 
					modelScenarioVisual.simulationRunCreated(requestId, simNodeId);  // if already
					// added, merely return the Visuals that depict the SimulationRun.
				
				if (this.completed)  // Referesh stats for the Scenario.
				{
					modelScenarioVisual.refreshStatistics();
				}
			}
			else if (visual instanceof ModelScenarioSetVisual)
			{
				ModelScenarioSetVisual scenSetVisual = (ModelScenarioSetVisual)visual;

				simVisuals = 
					scenSetVisual.simulationRunCreated(requestId, simNodeId);  // if already
					// added, merely return the Visuals that depict the SimulationRun.
				
				if (this.completed)  // Referesh stats for the Scenario Set and each Scenario.
				{
					scenSetVisual.refreshStatistics();
				}
			}
			else if (visual instanceof SimRunSetVisual)
			{
				SimRunSetVisual simRunSetVisual = (SimRunSetVisual)visual;
				if (this.completed)
					simRunSetVisual.refreshStatistics();
			}
			else if (visual instanceof SimulationVisual)
			{
				simVisuals = new HashSet<SimulationVisual>();
				simVisuals.add((SimulationVisual)visual);
			}
			else
				throw new RuntimeException(
					"Visual is not a ModelScenarioVisual or ModelScenarioSetVisual");
			
			
			if (simVisuals != null)
			{
				for (SimulationVisual simVis : simVisuals)
				{
					if (completed)
					{
						simVis.simulationCompleted(simNodeId);
					}
					else
					{
						simVis.totalEpochsCompletedForSim(simNodeId, totalEpochsCompleted);
						simVis.totalTimeElapsedForSim(simNodeId, totalTimeElapsed);
					}
				}
			}
		}
	}

	
	/**
	 * 
	 */
	 
	public static class ScenarioSimErrorMessageNotice extends PeerNoticeBase
	{
		public Integer requestId;
		public String simNodeId = null;
		public String msg;
		
		
		public ScenarioSimErrorMessageNotice(Integer requestId, String msg)
		{
			super(null);  // Should really specify the SimRunSet
			
			this.requestId = requestId;
			this.msg = msg;
		}
		
		
		public ScenarioSimErrorMessageNotice(Integer requestId,
			String visualNodeId,  // a ModelScenario Id or a ModelScenarioSet Id.
			String simNodeId, String msg)
		{
			super(visualNodeId);
			
			this.requestId = requestId;
			this.simNodeId = simNodeId;
			this.msg = msg;
		}
		
		
		public boolean isAlwaysForAVisual() { return false; }
		
		
		public String getMessage() { return this.msg; }
		
		
		public void notify(VisualComponent visual) throws InconsistencyError
		{
			Set<SimulationVisual> simVisuals = null;
			
			if (visual == null)
			{
			}
			if (visual instanceof ModelScenarioVisual)
			{
				ModelScenarioVisual modelScenarioVisual = (ModelScenarioVisual)visual;

				simVisuals = 
					modelScenarioVisual.simulationRunCreated(requestId, simNodeId);  // if already
					// added, merely return the Visuals that depict the SimulationRun.
			}
			else if (visual instanceof ModelScenarioSetVisual)
			{
				ModelScenarioSetVisual scenSetVisual = (ModelScenarioSetVisual)visual;

				simVisuals = 
					scenSetVisual.simulationRunCreated(requestId, simNodeId);  // if already
					// added, merely return the Visuals that depict the SimulationRun.
			}
			else if (visual instanceof SimulationVisual)
			{
				simVisuals = new HashSet<SimulationVisual>();
				simVisuals.add((SimulationVisual)visual);
			}
			else
				throw new RuntimeException(
					"Visual is not a ModelScenarioVisual or ModelScenarioSetVisual");
				
				
			if (simVisuals != null) for (SimulationVisual simVis : simVisuals)
			{
				simVis.errorReportedForSim(simNodeId, msg);
			}
		}

	}
	
	
	public static class SimActivityStartNotice extends PeerNoticeBase
	{
		public SimActivityStartNotice(String nodeId) { super(nodeId); }
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			((ActivityVisual)visComp).showStart();
		}
	}
	
	
	public static class SimActivityStopNotice extends PeerNoticeBase
	{
		public SimActivityStopNotice(String nodeId) { super(nodeId); }
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			((ActivityVisual)visComp).showStop();
		}
	}
	
	
	public static class SimActivityRespondingNotice extends PeerNoticeBase
	{
		public SimActivityRespondingNotice(String nodeId) { super(nodeId); }
		
		
		public void notify(VisualComponent visComp) throws InconsistencyError
		{
			((ActivityVisual)visComp).showEnteredRespond();
		}
	}
}

