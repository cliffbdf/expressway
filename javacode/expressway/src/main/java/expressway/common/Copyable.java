
/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


import java.io.Serializable;


/**
 * Intended for values of Attributes and Events that are not immutable.
 *
 * Not intended for Persistent Node types.
 */

public interface Copyable extends Cloneable, Serializable
{
	public Object clone() throws CloneNotSupportedException;
}

