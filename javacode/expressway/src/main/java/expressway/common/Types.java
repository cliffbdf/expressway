package expressway.common;

import java.io.Serializable;


/**
 * Pre-defined types for Model Domain Attributes.
 */
 
public class Types implements Serializable, Cloneable, Copyable
{
	/*
	 * Note: Each class must have a zero-argument public constructor.
	 * Note: Any class _may_ have at most one additional public constructor, that has
	 * one argument for each field (including all inherited fields).
	 */
	
	
	public String toString() { return "new " + this.getClass().getName() + "()"; }
	
	
	public Object clone() throws CloneNotSupportedException { return super.clone(); }
	
	
	/**
	 * Attributes of this type allow the user to tag Nodes to indicate that they
	 * are of interest for certain testing or reporting purposes.
	 */
	
	public static class Inspect extends Types implements Serializable  // for Testing
	{
		/**
		 * Nodes tagged with an Attribute of this type will be considered to be
		 * of interest when reporting simulation Event history.
		 */
		 
		public static class ShowHistory extends Inspect
		{
		}
	}


	public static class State extends Types implements Serializable  // for Attributes of States.
	{
		public static String DefaultAssuranceExpression = "PFromMeanByAtMost(10,10)";
		
		/** An expression String, to be used for computing "Assurance". */
		public String assuranceExpression = DefaultAssuranceExpression;
		
		
		public String toString()
		{
			return "new " + this.getClass().getName() + "(" + 
				(assuranceExpression == null? "null" : ("\"" + assuranceExpression + "\"")) +
				")";
		}
	
	
		public State() {}
		
		
		public State(String asEx)
		{
			this.assuranceExpression = asEx;
		}
		
		
		/**
		 * Financial State Attribute Types, describing States that represent  flows
		 * or outcomes pertaining to business value or cost.
		 */
		
		public static class Financial extends State
		{
			public Financial() {}
			public Financial(String s) { super(s); }
			
			
			public static class Income extends Financial
			{
				public Income() {}
				public Income(String s) { super(s); }
			}
			
			public static class Cost extends Financial
			{
				public Cost() {}
				public Cost(String s) { super(s); }

				public static class Build extends Cost {}
				public static class Mitigation extends Cost {}  // reduces risk		
				public static class Casualty extends Cost {}  // due to a failure of something
				public static class Total extends Cost {}
			}
			
			public static class Value extends Financial
			{
				public Value() {}
				public Value(String s) { super(s); }

				public static class MissionValue extends Value {}
				public static class MarketValue extends Value {}
				public static class CostAvoidance extends Value {}
				public static class RiskReduction extends Value {}
				public static class Total extends Value {}
			}
			
			public static class NetValue extends Financial
			{
				public NetValue() {}
				public NetValue(String s) { super(s); }
			}
			
			public static class ROI extends Financial
			{
				public ROI() {}
				public ROI(String s) { super(s); }
			}
		}
	}
	
	
	public static class Activity extends Types implements Serializable  // for Attributes of Activities.
	{
		/**
		 * Transformation Activity Types, describing business processes that serve
		 * to define or create new business processes - i.e., that are transformational
		 * to an enterprise.
		 */
		
		public static class Transformation extends Activity  // changes a business process.
		{
			public String stdEvidence;
			public String criteria;
			public String actEvidence;
			public String comment;
			

			public String toString()
			{
				return "new " + this.getClass().getName() + "(" +
					(stdEvidence == null? "null" : ("\"" + stdEvidence + "\"")) + ", " +
					(criteria == null? "null" : ("\"" + criteria + "\"")) + ", " +
					(actEvidence == null? "null" : ("\"" + actEvidence + "\"")) + ", " +
					(comment == null? "null" : ("\"" + comment + "\"")) +
					")";
			}
		
		
			public Transformation() {}
			
			
			public Transformation(String stdEvidence, String criteria,
				String actEvidence, String comment)
			{
				this.stdEvidence = stdEvidence;
				this.criteria = criteria;
				this.actEvidence = actEvidence;
				this.comment = comment;
			}


			public void setStdEvidence(String stdEvidence) { this.stdEvidence = stdEvidence; }
			public void setCriteria(String criteria) { this.criteria = criteria; }
			public void setActEvidence(String actEvidence) { this.actEvidence = actEvidence; }
			public void setComment(String comment) { this.comment = comment; }
			
			public String getStdEvidence() { return stdEvidence; }
			public String getCriteria() { return criteria; }
			public String getActEvidence() { return actEvidence; }
			public String getComment() { return comment; }


			public static class Build extends Transformation {} // creates a business process (or component of).
			public static class Acquire extends Transformation {} // obtains a business process (or component of).
			public static class Integrate extends Transformation {} // Deployment into existing processes.
			public static class Enhance extends Transformation {} // alters a business process (or component of).
			public static class Decommission extends Transformation {} // ends a business process (or component of).
			public static class Mitigate extends Transformation {} // reduces risk of transformation.
		}
		
		
		/**
		 * Operation Activity Types, describing a business capability that has been
		 * deployed.
		 */
		
		public static class Operation extends Activity
		{
			public static class Use extends Operation {}
			public static class Operate extends Operation {}
			public static class Maintain extends Operation {}
			public static class Respond extends Operation {}
			public static class Troubleshoot extends Operation {}
			public static class Income extends Operation {} // increases value (as a result of use).
			public static class Mitigate extends Operation {} // a business process that reduces risk.
			public static class Threat extends Operation {} // a source of Casualties.
		}
	}
}

