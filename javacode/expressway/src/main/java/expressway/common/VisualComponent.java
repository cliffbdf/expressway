/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.NodeSer;
import java.awt.Color;
import java.awt.Container;
import java.awt.Component;
import java.util.Date;
import java.util.Set;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;



/** ****************************************************************************
 * Server side abstraction of the visual objects used by a client to depict
 * server-side Persistent Nodes.
 */
 
public interface VisualComponent
{
	interface VisualComponentFactory
	{
		Class getVisualClass(NodeSer nodeSer)
		throws
			CannotBeInstantiated;
	}
	
	
	interface TabularVisualComponentFactory extends VisualComponentFactory
	{
		/**
		 * Create a new instance of a Visual to depict the specified NodeSer.
		 * The new Visual is NOT also added to its parent in the View: that
		 * should be done by the View's addNode method.
		 */
		 
		VisualComponent makeTabularVisual(NodeSer nodeSer)
		throws
			ParameterError,  // if the component cannot be made due to an erroneous
							// or out-of-range parameter.
			Exception;	// any other error.
	}
	
	
	interface GraphicVisualComponentFactory extends VisualComponentFactory
	{
		/**
		 * Create a new instance of a Visual to depict the specified NodeSer.
		 * If 'container' is not null, then the new Visual is added to 'container'.
		 */
		 
		VisualComponent makeGraphicVisual(NodeSer nodeSer, Container container,
			boolean asIcon)
		throws
			CannotBeInstantiated,
			ParameterError,
			Exception;
	}


	public class NoNodeException extends RuntimeException
	{
		final static String stdMsg = 
			"Method call disallowed: target Visual does not depect a " +
				"specific database Node.";
		public NoNodeException() { super(stdMsg); }
		public NoNodeException(String msg) { super(stdMsg + ";\n" + msg); }
		public NoNodeException(Throwable t) { super(stdMsg, t); }
		public NoNodeException(String msg, Throwable t) { super(stdMsg + ";\n" + msg, t); }
	}
	
	
	/** ************************************************************************
	 * Return the ID of the PersistentNode that this VisualComponent visually
	 * represents.
	 */
	 
	String getNodeId()
	throws
		NoNodeException;
	
	
	/** ************************************************************************
	 * Return the name of the PersistentNode that this VisualComponent visually
	 * represents.
	 */
	 
	String getName();
	
	
	/** ************************************************************************
	 * Return the fully qualified name of the PersistentNode that this VisualComponent
	 * visually represents.
	 */
	 
	String getFullName();
	

	ImageIcon getImageIcon();
	
	
	void setImageIcon(ImageIcon imgIcon);
	
	
	/**
	 * Return the name of the HTML page, relative to ReleaseInfo.HelpBaseURLString,
	 * that describes this component type. See the HelpWindow class for a definition
	 * of what a page name is.
	 */
	
	String getDescriptionPageName();
		

	/** ************************************************************************
	 * If the parent Node p of the Node n depicted by this Visual has one or more
	 * Node of the type of Node n, then return the sequence of n within its parent's
	 * set of that type of Node. (The first element in the sequence is 1.) Otherwise,
	 * return 0. Currently only implemented for StateVisualJ and AttributeVisualJ.
	 */
	 
	int getSeqNo();
	
	
	/** ************************************************************************
	 * Return true if this Visual contains the specified Visual. The containership
	 * may be several levels deep. 'Contains' means to visuall enclose: it
	 * does not mean 'own'.
	 */
	 
	boolean contains(VisualComponent comp)
	throws
		NoNodeException;
	
	
	/** ************************************************************************
	 * Return true if the corresponding Node belongs to a Domain that is a MotifDef.
	 * 
	 */
	 
	boolean isOwnedByMotifDef();
	
	
	/** ************************************************************************
	 * Return true if the corresponding Node belongs to an instance of TemplateInstance
	 * that references a Template.
	 */
	 
	boolean isOwnedByActualTemplateInstance();
	

	/** ************************************************************************
	 * Return the Ids of all of the sub-Nodes of the Node that this Visual depicts.
	 * A sub-Node is any directly owned Node.
	 */
	 
	String[] getChildIds();
	
	
	/** ************************************************************************
	 * Callback to inform the Visual that the Node's name has changed.
	 */
	 
	void nameChangedByServer(String newName, String newFullName);
	
	
	/** ************************************************************************
	 * Callback to inform the Visual that the Node has been deleted from the model.
	 */
	 
	void nodeDeleted();
	
	
	/** ************************************************************************
	 * Call to inform this Visual that a CrossReference has been created in which
	 * the Node depicted by this Visual is the 'from' Node, and so
	 * the View should determine if the CrossReference should be depicted, either
	 * in this Visual's View or in an enclosing MultiHierarchyPanel.
	 */
	 
	void notifyCrossReferenceCreated(String toNodeId, String crossRefId);
		
		
	/** ************************************************************************
	 * Call to inform this Visual that a CrossReference that the View or an enclosing
	 * MultiHierarchyPanel might depict has been deleted. The Node depicted by
	 * this Visual is the 'from' Node in that CrossReference.
	 */
	 
	void notifyCrossReferenceDeleted(String toNodeId, String crossRefId);
		
		
	/** ************************************************************************
	 * Return the View that owns this VisualComponent.
	 */
	 
	ClientView getClientView();
	
	
	/** ************************************************************************
	 * Return the ModelEngine interface that is associated with this Visual's View.
	 */
	 
	ModelEngineRMI getModelEngine();
	
	
	/** ************************************************************************
	 * Return the serializable state of the Persistent Node depicted by this Visual.
	 */
	 
	NodeSer getNodeSer();
	
	
	/** ************************************************************************
	 * Set the serailizable state for this Visual. This method is only an accessor.
	 */
	 
	void setNodeSer(NodeSer nodeSer)
	throws
		NoNodeException;
	
	
	/** ************************************************************************
	 * Obtain the latest state of this Visual's Node from the server.
	 */
	 
	void update();
	

	/** ************************************************************************
	 * Set (usually re-set) the Node Ser of this Visual. This is intended to be
	 * used when a Visual is refreshed by obtaining new data from the server.
	 */
	 
	void update(NodeSer nodeSer);
	
	
	/** ************************************************************************
	 * Redraw this VisualComponent from scratch, based on the current state of
	 * the Visual.
	 */
	 
	void redraw();
	

	/** ************************************************************************
	 * Update the state of this Visual, and then redraw it. Equivalent to calling
	 * update() and then redraw().
	 */
	 
	void refresh();
	
	
	/** ************************************************************************
	 * Return the location of this Visual within the Graphic of the AWT Component
	 * that owns it.
	 */
	 
	java.awt.Point getLocation();
	
	
	int getX();
	
	
	int getY();

	
	/** ************************************************************************
	 * Contact to serer to update the location of this Visual, and then redraw it.
	 */
	 
	void refreshLocation();

	
	/** ************************************************************************
	 * Contact the server to update the size of this Visual, and then redraw it.
	 */
	 
	void refreshSize();
	
	
	/** ************************************************************************
	 * Update the location of this Visual based on the View's current information,
	 * and then redraw it.
	 */
	 
	void setLocation()
	throws
		Exception;
	
	
	/** ************************************************************************
	 * Update the size of this Visual based on the View's current information,
	 * and then redraw it.
	 */
	 
	void setSize()
	throws
		Exception;

	
	/** ************************************************************************
	 * Update any redundant internal state that depends on the internal NodeSer
	 * state. This includes any visualization-related state. After this method
	 * is called this Visual should be internally consistent and its graphical
	 * depiction should accurately reflect its internal state.
	 *
	 * Specialized classes should override this method to add any required updates
	 * to fields that are unique to the specialized class.
	 */
	 
	void refreshRedundantState()
	throws
		Exception;

	
	/** ************************************************************************
	 * Return true if this Visual should currently be displayed as an Icon,
	 * rather than its normal depiction.
	 */
	 
	boolean getAsIcon();
	
	void setAsIcon(boolean yes);
	
	boolean useBorderWhenIconified();
	
	
	/** ************************************************************************
	 * Populate each immediate sub-Element component of this Visual. Only populate
	 * Visuals that are appropriate for the View (i.e., do not blindly add
	 * a Visual for every child Node): this decision-making may be delegated to
	 * the View's addNode method.
	 */
	 
	void populateChildren()
	throws
		ParameterError,
		Exception;
		
		
	/** ************************************************************************
	 * Recreate each immediate sub-Component of this Visual. If a sub-Component
	 * is not appropriate for the View (e.g., this Visual is iconified), then
	 * do not display the sub-Component. Retain visibility state for any controls
	 * that are displayed in any overlays (e.g., in a ControlPanel), even if
	 * the controls pertain to an immediate or deeper sub-Component.
	 *
	 
	void refreshChildren()
	throws
		ParameterError,
		Exception;*/
	
	
	/** ************************************************************************
	 * Return the Visuals that are immediately nested within this Visual.
	 * These are not necessarily Visuals that depicts Nodes owned by the Node
	 * that this Visual depicts.
	 */
	 
	Set<VisualComponent> getChildVisuals();
	
	
	Object getParentObject();
	
	
	/** ************************************************************************
	 * Return the AWT Container that contains this Visual. If this Visual is
	 * a Container, return this.
	 */
	 
	Container getContainer();
	
	
	void highlight(boolean yes);
	
	
	boolean isHighlighted();
	
	
	Color getNormalBackgroundColor();
	
	
	Color getHighlightedBackgroundColor();
	
	
	int getWidth();
	
	
	int getHeight();
	
	
	/**
	 * Write Attribute value data for this Visual and its child hierarchy to the
	 * specified Writer. Should be implemented by each Visual base class. 
	 * Should delegate to writeScenarioDataAsXML to actually write the Visual's data.
	 */
	 
	void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException;
	
	
	/**
	 * Should not contact the
	 * server: data should only be obtained from the Visual's View.
	 */
	 
	void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException;
	
	
	
  /* ***************************************************************************
  	 ***************************************************************************
	 * Specialized interfaces, representing each type of Persistent Node.
	 */

	 
	/**
	 * To be implemented by a container of Domain Visuals, such as a DomainListPanel.
	 */
	 
	interface DomainSpaceVisual
	{
		void domainCreated(String domainId);
		
		void motifCreated(String motifId);
		
		void showScenarioCreated(String scenarioId);
		
		void showScenarioSetCreated(String scenarioSetId);
	}
	
	
	/**
	 * Depicts any hierarchical Node such as a ModelConatiner.
	 */
	 
	interface ContainerVisual extends VisualComponent
	{
		void componentResized(VisualComponent child)
		throws
			Exception;
	}
	
	
	interface DomainVisual extends VisualComponent
	{
		void motifAdded(String motifId);
		void motifRemoved(String motifId);

		/** If the specified Scenario is not currently depicted, depict it. 
			Make sure that the Scenario is visible. Note that the ScenarioVisual
			returned might be in a different View than the View that owns the
			DomainVisual. */
		ScenarioVisual showScenario(String scenarioId) throws Exception;
		
		void showScenarioCreated(String scenarioId);
		void showScenarioDeleted(String scenarioId);
		
		/** Return the ScenarioVisual that depicts the specified Scenario of
			this DomainVisual instance. */
		ScenarioVisual getScenarioVisual(String scenarioId);
	}
	
	
	interface ModelElementVisual extends VisualComponent
	{
	}
	
	
	interface ModelContainerVisual extends ContainerVisual, ModelElementVisual
	{
		/** ************************************************************************
		 * Callback to inform the Container that the specified Node has been added.
		 */
		 
		//void modelElementAdded(String nodeId);
	
	
		void resetConduitColors();
		void incrementConduitColor();
		Color getConduitColor();
	}
	
	
	interface ModelDomainVisual extends ModelContainerVisual, DomainVisual
	{
		List<ModelScenarioVisual> getModelScenarioVisuals();
		void simulationRunsDeleted();
		void showScenarioSetCreated(String newScenSetId);
		void attributeStateBindingChanged(String attrId, String stateId);
		void loadAllScenarios();
	}
	
	
	interface MotifDefVisual extends DomainVisual
	{
	}
	
	
	interface DecisionDomainVisual extends ContainerVisual, DomainVisual
	{
	}
	
	
	interface TemplateInstanceVisual extends ModelElementVisual
	{
	}
	

	interface ScenarioVisual extends VisualComponent
	{
		/** Return the value of each Attribute of the specified Visual in the
			context of this Scenario. Return the name of each corresponding
			AttributeVisual in 'attrNames'. Only return those that are actually
			displayed by this ScenarioVisual. */
		List<Serializable> getAttributeValues(VisualComponent vis, List<String> attrNames);
		
		Serializable getAttributeValue(String attrId);
		
		void refreshAttributeValue(String attrNodeId)
		throws
			Exception;
		
		/** Add or remove (depending on value of 'add') the AttributeVisibilityControls, and
			any associated ValueControls, for the specified Visual, in this ScenarioVisual.
			When a VisibilityControl is removed, the ModelScenarioVisual remembers
			if the VisibilityControl for the Visual was set to display its ValueControls: thus,
			when a VisibilityControl is re-added, its ValueControls are not added if
			the VisibilityControl was previously set to hide its ValueControls.
			Has no effect if the ScenarioVisual mode is set to hide its AttributeControls. */
		void addOrRemoveAttributeControls(boolean add, VisualComponent visual)
		throws
			Exception;

		/** Delete any Controls in this Scenario associated with the specified Node. */
		void deleteControls(String nodeId);
		
		/** True if the AttributeValueControls for the Visual were visible when
			AttributeValueControls for the Scenario were most recently showing.
			It is anticipated that the above addOrRemoveAttributeControls method
			will call this method to determine whether to actually display a
			ValueControl. */
		boolean controlsForAttrAreVisible(VisualComponent visual);
		
		/** Refresh the location of the specified Visual, as located within
			this ModelScenario Visual. The specified Visual must be owned by
			this ModelScenarioVisual. */
		void setLocation(VisualComponent visual);
	}
	
	
	interface ScenarioSetVisual extends VisualComponent
	{
	}
	
	
	interface ModelScenarioVisual extends ScenarioVisual, ModelElementVisual
	{
		/** Allows a client to immediately add a Visual for a SimulationRun to
			ensure that notices pertaining to the simulation are received. The
			Visual for the SimulationRun is logically owned by the View. */
		void addSimulationVisual(Integer id, SimulationVisual simPanel);
		
		Set<SimulationVisual> simulationRunCreated(Integer requestId,
			String simNodeId); // may return null.
		
		void simulationRunsDeleted();
		
		/** If showEventsMode is true, the Scenario enables the display of Events.
			This is the mode used when replaying a simulation. */
		void setShowEventsMode(boolean showEventsMode);
		
		boolean getShowEventsMode();
		
		/** Show the state of the Scenario for the specified iteration. Iteration 1
			is the first iteration. */
		void showEventsForIteration(String simRunId, int iteration);
		
		void refreshStatistics();
		
		/** The simulation time range of the Model Scenario has been changed. */
		void simTimeParamsChanged(Date newFromDate, Date newToDate, long newDuration,
			int newIterLimit);

		
		/*
		 * For display of ValueControls.
		 */
		
		/** Add or remove (depending on value of 'add') the StateVisibilityControls, and
			any associated ValueControls, for the specified Visual, in this ScenarioVisual.
			When a VisibilityControl is removed, the ModelScenarioVisual remembers
			if the VisibilityControl for the Visual was set to display its ValueControls: thus,
			when a VisibilityControl is added, its ValueControls are not added if
			the VisibilityControl was previously set to hide its ValueControls. 
			Has no effect if the ScenarioVisual mode is set to hide its StateControls. */
		void addOrRemoveStateControls(boolean add, VisualComponent visual)
		throws
			Exception;
		
		/** True if the StateValueControls for the Visual were visible when
			StateValueControls for the Scenario were most recently showing.
			It is anticipated that the above addOrRemoveStateControls method
			will call this method to determine whether to actually display a
			ValueControl. */
		boolean controlsForStateAreVisible(PortedContainerVisual visual);
		
		
		JFrame createEventHistoryWindow(String simRunId, Class... stateTags)
		throws
			Exception;
	}
	
	
	interface ModelScenarioSetVisual extends ScenarioSetVisual, ModelElementVisual
	{
		/** Allows a client to immediately add a Visual for a SimulationRun to
			ensure that notices pertaining to the simulation are received. The
			Visual for the SimulationRun is logically owned by the ModelScenarioVisual
			that is owned by the ModelScenarioSetVisual. */
		void addSimulationVisual(Integer id, SimulationVisual simPanel);
		
		Set<SimulationVisual> simulationRunCreated(Integer requestId,
			String simNodeId); // may return null.
		
		void simulationRunsDeleted();
		
		void refreshStatistics();
	}
	
	
	interface SimulationVisualMonitor
	{
		/** For a SimulationVisual to call to notify when it is about to close. */
		void simulationVisualClosing(SimulationVisual simVis);
	}


	/**
	 * For reporting to a View on the progress of a specific SimulationRun.
	 * Note that using this interface when many simulations are performed might
	 * result in a great deal of network traffic.
	 */
	 
	interface SimulationRunVisual extends ModelElementVisual
	{
		void attributesInitialized();
		void attributesUpdated();
		void stateInitEventsCreated();
		void predefinedEventsRetrieved();
		void epochStarted(int epochNo, Date date);
		void epochFinished(int epochNo, Date date);
		void allFunctionsEvaluatedForEpoch(int epochNo, Date date);
		void allActivitiesActivatedForEpoch(int epochNo, Date date);
		void reportProgress(String msg);
		void epochCompleted(Date epoch, int iteration);
		JFrame createEventHistoryWindow(Class... stateTags)
		throws
			Exception;
	}
	
	
	interface SimRunSetVisual extends ModelElementVisual
	{
		void simRunSetCreated(String simRunSetId, String modelScenarioId);
		
		void refreshStatistics();
	}
	
	
	/**
	 * For reporting to a View on the progress of a set of SimRunSets. Note that
	 * this Visual does not depict a single Node: it depicts a set of Nodes, each
	 * of which is a SimRunSet. Each SimRunSet has a ModelScenario associated
	 * with it.
	 */
	
	interface SimulationVisual extends SimRunSetVisual
	{
		Integer getCallbackId();
		void simulationRunCreatedForSimulation(String simNodeId, String simRunName);
		void totalEpochsCompletedForSim(String simNodeId, int noOfEpochs);
		void totalTimeElapsedForSim(String simNodeId, long ms);

		/** Either successful or unsuccessful completion. */
		void simulationCompleted(String simNodeId);
		
		void errorReportedForSim(String simNodeId, String msg);
	}
	
	
	interface DecisionScenarioVisual
	{
		void choiceCreated(String choiceId);
		void decisionsUpdated();
	}
	
	
	interface PortedContainerVisual extends ModelContainerVisual
	{
	}
	
	
	interface ActivityVisual extends PortedContainerVisual
	{
		void showStart();
		void showStop();
		void showEnteredRespond();
	}
	
	
	interface FunctionVisual extends PortedContainerVisual
	{
		void showStart();
		void showStop();
	}
	
	
	interface ConduitVisual extends ModelElementVisual
	{
	}
	
	
	interface PortVisual extends ModelElementVisual
	{
	}
	
	
	interface StateVisual extends ModelElementVisual
	{
		void eventScheduled(Serializable futureValue, Date date);
		void valueUpdatedInEpoch(int epochNo, Date date);
		boolean hasAttributeStateBindings();
	}
	
	
	interface PredefinedEventVisual extends ModelElementVisual
	{
	}
	
	
	interface DecisionPointVisual extends VisualComponent
	{
		void showStart();
		void showStop();
	}
	
	
	interface VariableVisual extends VisualComponent
	{
	}


	interface AttributeValueParser
	{
		/** Attempt to recognize the input String value. If its type is not
			recognized, return its String value (unchanged). */
		Serializable parseValue(String text)
		throws
			ParameterError;
	}
	
	
	interface AttributeVisual extends VisualComponent
	{
		AttributeValueParser getValueParser();
		
		/** Uses this AttributeVisual's ValueParser to parse the specified text. */
		Serializable parseValue(String text)
		throws
			ParameterError;
	}
	
	
	interface AttributeStateBindingVisual extends ModelElementVisual
	{
	}
	
	
	interface ConstraintVisual extends ModelElementVisual
	{
	}
	
	
	interface HierarchyAttributeVisual extends AttributeVisual
	{
	}
	
	
	interface HierarchyElementVisual extends VisualComponent
	{
	}
	
	
	interface HierarchyVisual extends HierarchyElementVisual
	{
	}
	
	
	interface HierarchyDomainVisual extends DomainVisual, HierarchyElementVisual
	{
	}


	interface HierarchyScenarioVisual extends ScenarioVisual, HierarchyElementVisual
	{
	}
	
	
	interface HierarchyScenarioSetVisual extends ScenarioSetVisual, HierarchyElementVisual
	{
	}
}

