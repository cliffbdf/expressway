/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.geometry.Point;

/**
 * For any curve, an InflectionPoint is a two-dimensional location at which
 * a parameter that governs the line's shape changes.
 *
 * Current usage:
 * A Conduit links two Ports, and contains a series of inflection points.
 * The inflection points are ordered such that they define the intended path
 * from the first Port to the first inflection point, to the second
 * inflection point, and so on, ending with the second Port.
 * Lines have thickness, and so an InflectionPoint's location is defined to be
 * the point at which the center of the line has an inflection point.
 * 
 * The Points are expressed in the coordinate space of the Container that contains
 * the Conduit.
 */

public interface InflectionPoint extends Point
{
}
