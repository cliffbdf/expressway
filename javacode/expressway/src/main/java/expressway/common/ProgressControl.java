/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.common.ModelAPITypes.*;
import java.util.List;


/**
 * An interface provided by an asynchronous thread to allow a client to manage
 * the thread.
 */
	 
public interface ProgressControl
{
	/**
	 * To be called by the client to pre-emptively abort the thread.
	 */
	 
	void forceAbort();
}

