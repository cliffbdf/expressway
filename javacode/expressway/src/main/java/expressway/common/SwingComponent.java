/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.common.ModelAPITypes.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JComponent;


/**
 * Define all of the Swing Component methods that Expressway uses.
 */
 
public interface SwingComponent extends AWTContainer
{
	/** Return this SwingComponent, cast to a Swing JComponent. */
	JComponent getJComponent();
}

