/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.help;


import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.awt.Dimension;
import java.awt.event.*;
import expressway.generalpurpose.HTMLWindow;
import expressway.generalpurpose.ThrowableUtil;



/**
 * Example:

	new HelpButton("The Stuff", Medium,
		"<h1>title</h1>" +
		"this is the stuff" +
		"and also check out " +
		HelpWindow.createHref("About Generators")
		);

 */

public class HelpButton extends JButton
{
	public enum Size { Small, Medium, Large };
	
	private Size size;
	
	private static final String HTMLFontName = "Helvetica";
	private static final String HTMLFontSize = "4";
	
	public static final java.awt.Dimension SmallSize = new java.awt.Dimension(12, 12);
	public static final java.awt.Dimension MediumSize = new java.awt.Dimension(14, 14);
	public static final java.awt.Dimension LargeSize = new java.awt.Dimension(16, 16);
	
	
	//private final String itemName;
	//private final Size size;
	//private final String htmlText;
	
	
	public Dimension getPreferredSize()
	{
		switch (this.size)
		{
			case Small:	return SmallSize;
			case Medium: return MediumSize;
			case Large: return LargeSize;
		}
		
		throw new RuntimeException("Indeterminate Help Button size");
	}
	
	
	public HelpButton(final String itemName, final Size size, final String htmlText)
	{
		this(itemName, false, size, htmlText);
		
		this.setFocusable(false);
	}
	
	
	public HelpButton(final String itemName, boolean useNameForButtonText,
		final Size size, final String htmlText)
	{
		super(useNameForButtonText ? itemName : "?");
		
		//this.itemName = itemName;
		//this.size = size;
		//this.htmlText = htmlText;
		
		switch (size)
		{
			case Small:	this.setSize(SmallSize); break;
			case Medium: this.setSize(MediumSize); break;
			case Large: this.setSize(LargeSize); break;
		}
		
		this.size = size;
		
		
		this.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// Pop up a scrollable window containing the html text.
				
				HTMLWindow helpWindow = null;
				try { helpWindow = new HelpWindow(itemName); }
				catch (Exception ex)
				{
					JOptionPane.showMessageDialog(HelpButton.this, 
						ThrowableUtil.getAllMessages(ex), "Error",
							JOptionPane.ERROR_MESSAGE);  return;
				}
				
				java.awt.Point loc = HelpButton.this.getLocationOnScreen();
				helpWindow.setLocation(loc.x + HelpButton.this.getWidth(), loc.y);
				
				helpWindow.setText("<html><font size=\"" + HTMLFontSize + "\" " +
					"face=\"" + HTMLFontName + "\">" +
					htmlText + "</font></html>");
				
				//helpWindow.setText("<html style=\"font-size:" + HTMLFontSize + "pt; " +
				//	"font-family:'" + HTMLFontName + "'\">" +
				//	htmlText + "</html>");
				
				helpWindow.show();
			}
		});
	}
}

