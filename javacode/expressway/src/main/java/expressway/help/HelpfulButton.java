/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.help;

import java.net.URL;
import java.net.MalformedURLException;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 * A Button that has a Help Button next to it.
 */
 
public class HelpfulButton extends JPanel
{
	final static Font NormalFont = new Font(Font.SANS_SERIF, Font.PLAIN, 12);
	
	final static FontMetrics NormalFontMetrics =
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(NormalFont);

	private JButton button;
	private FontMetrics fontMetrics;
	private HelpButton helpButton = null;
		

	public HelpfulButton(final String text, final String htmlHelpText)
	{
		this.setBackground(Color.white);
		this.fontMetrics = NormalFontMetrics;
		this.button = new JButton(text);
		this.button.setFont(NormalFont);
		this.add(button);
		
		if (htmlHelpText != null)
		{
			this.add(this.helpButton = 
				new HelpButton(text, HelpButton.Size.Medium, htmlHelpText));
		}

		setLayout(new LayoutManager()
		{
			public void layoutContainer(Container parent)
			{
				button.setLocation(2, 2);

				button.setSize(Math.max(100, button.getX() + getTextWidth() + 10),
					HelpButton.LargeSize.height + 2);
				if (helpButton != null)
					helpButton.setLocation(button.getX() + button.getWidth(), 2);
			}
			
			public Dimension minimumLayoutSize(Container parent)
			{
				return new Dimension(Math.max(100, getTextWidth() + button.getX() + 
					(helpButton == null? 0 : helpButton.getWidth())) + 20, 20);
			}
			
			public Dimension preferredLayoutSize(Container parent) 
			{
				return new Dimension(Math.max(100, getTextWidth() + button.getX() + 
					(helpButton == null? 0 : helpButton.getWidth()) + 20), 20);
			}
			
			public void addLayoutComponent(String name, Component comp) { layoutContainer(HelpfulButton.this); }
			public void removeLayoutComponent(Component comp)  { layoutContainer(HelpfulButton.this); }
			
			int getTextWidth()
			{
				return fontMetrics.stringWidth(getText());
			}
		});
		
		validate();
	}
		
		
	public String getText()
	{
		return button.getText();
	}
	
	
	public void setText(String text)
	{
		String t = text == null? "" : text;
		button.setText(t);
	}
	
	
	public void addActionListener(ActionListener listener)
	{
		button.addActionListener(listener);
	}
	
	
	protected void validateTree() { this.getLayout().layoutContainer(this); }
}

