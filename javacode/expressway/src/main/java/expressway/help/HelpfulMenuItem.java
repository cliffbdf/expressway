/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.help;

import java.net.URL;
import java.net.MalformedURLException;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 * A Menu Item that displays a question mark at the end that acts as a hyperlink
 * to help information about the menu item.
 */
 
public class HelpfulMenuItem extends JMenuItem
{
	private static final int QuestionAreaWidth = 15;
	
	private static Font QuestionFont = new Font("Helvetica", Font.BOLD, 12);
	private static final FontMetrics QuestionFontMetrics = 
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(QuestionFont);
	private static int QuestionWidth = QuestionFontMetrics.stringWidth("?");
	
	HelpWindow helpWindow = null;

	
	public HelpfulMenuItem(String title, boolean isPage, String string)
	{
		super(title);
		
		Dimension minSize = this.getMinimumSize();
		
		minSize.setSize(minSize.width + QuestionAreaWidth, minSize.height);
		
		this.setMinimumSize(minSize);
		
		if (isPage)
		{
			String urlString = HelpWindow.createURL(string);
			
			URL helpHTMLUrl = null;
			try { helpHTMLUrl = new URL(urlString); }
			catch (MalformedURLException ex) { throw new RuntimeException(ex); }
			
			this.addMouseListener(new MouseAdapter(this));
			
			this.addMouseMotionListener(new MouseMotionAdapter(this, helpHTMLUrl));
		}
		else
		{
			this.addMouseListener(new MouseAdapter(this));
			
			this.addMouseMotionListener(new MouseMotionAdapter(this, string));
		}
	}

	
	public HelpfulMenuItem(String title, URL helpHTMLUrl)
	{
		super(title);
		
		Dimension minSize = this.getMinimumSize();
		
		minSize.setSize(minSize.width + QuestionAreaWidth, minSize.height);
		
		this.setMinimumSize(minSize);
		
		this.addMouseListener(new MouseAdapter(this));
			
		this.addMouseMotionListener(new MouseMotionAdapter(this, helpHTMLUrl));
		
		/*this.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.DESELECTED)
				{
					if (helpWindow != null) helpWindow.dispose();
					helpWindow = null;
				}
			}
		});*/
	}
	
	
	boolean mouseEventIsInQuestionArea(MouseEvent e)
	{
		return e.getX() > (getWidth() - QuestionAreaWidth);
	}
	
	
	boolean mouseIsAlongQuestionMarkRightEdge(MouseEvent e)
	{
		return ! (
			((Math.abs(e.getY()) < 5) || (Math.abs(e.getY() - getHeight()) < 5)) &&
			((e.getX() > (getWidth()-QuestionAreaWidth)) && (e.getX() < getWidth()))
		);
		
		//return Math.abs(this.getWidth() - e.getX()) <= 1;  // 1 is the tolerance
	}
	
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		// Render the question mark
		int x = this.getWidth() - QuestionAreaWidth/2 - QuestionWidth/2;
		g.setFont(QuestionFont);
		g.drawString("?", x, 14);
	}
}


class MouseAdapter implements MouseListener
{
	private HelpfulMenuItem menuItem;
	
	
	public MouseAdapter(HelpfulMenuItem menuItem)
	{
		this.menuItem = menuItem;
	}
	
	
	public void mouseExited(MouseEvent e)
	{
		if (! menuItem.mouseIsAlongQuestionMarkRightEdge(e))
		{
			if (menuItem.helpWindow != null) menuItem.helpWindow.dispose();
			menuItem.helpWindow = null;
			return;
		}
	}

	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
}


class MouseMotionAdapter implements MouseMotionListener
{
	private static final String HTMLFontName = "Helvetica";
	private static final String HTMLFontSize = "4";
	
	private HelpfulMenuItem menuItem;
	private String htmlText;
	private URL helpHTMLUrl;
	
	
	public MouseMotionAdapter(HelpfulMenuItem menuItem, String htmlText)
	{
		this.menuItem = menuItem;
		this.htmlText = htmlText;
	}
	
	
	public MouseMotionAdapter(HelpfulMenuItem menuItem, URL helpHTMLUrl)
	{
		this.menuItem = menuItem;
		this.helpHTMLUrl = helpHTMLUrl;
	}
	
	
	public void mouseDragged(MouseEvent e)
	{
	}
	
	
	public void mouseMoved(MouseEvent e)
	{
		if (! menuItem.isArmed()) return;
		
		try
		{
			if (menuItem.mouseEventIsInQuestionArea(e))
				// mouse is over the question mark
			{
				// Display Help
				if (menuItem.helpWindow != null) return;
				//if (helpWindow != null) helpWindow.dispose();
				menuItem.helpWindow = new HelpWindow(menuItem.getText());
				
				if (htmlText == null)
					menuItem.helpWindow.setURL(helpHTMLUrl);
				else
					//menuItem.helpWindow.setText(htmlText);
					menuItem.helpWindow.setText("<html><font size=\"" + 
						HTMLFontSize + "\" " + "face=\"" + HTMLFontName + "\">" +
						htmlText + "</font></html>");
				
					//menuItem.helpWindow.setText("<html style=\"font-size:" + 
					// HTMLFontSize + "pt; " + "font-family:'" + HTMLFontName + "'\">" +
					//	htmlText + "</html>");

				java.awt.Point loc = menuItem.getLocationOnScreen();

				int x = loc.x + menuItem.getWidth();

				menuItem.helpWindow.setLocation(x, loc.y);
				menuItem.helpWindow.show();
			}
			else // if help is displayed, remove it
			{
				if (menuItem.helpWindow != null) menuItem.helpWindow.dispose();
				menuItem.helpWindow = null;
			}
		}
		catch (Exception ex) {
		ex.printStackTrace();
		}  // ignore
	}
}


// http://www.javaworld.com/javaworld/javaqa/1999-10/01-qa-menuhelp.html

