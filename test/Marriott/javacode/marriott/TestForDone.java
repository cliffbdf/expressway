package marriott;


import expressway.ModelElement.*;
import expressway.ModelAPITypes.*;
import expressway.FunctionBase;
import java.util.Set;


/**

			<port name="input"/>
			<port name="output/>

 */

public class TestForDone extends FunctionBase
{
	private State again = null;
	
	
	public void start(FunctionContext context) throws Exception
	{
		super.start(context);
		again = getFunctionContext().getState("again");
		
	}
	
	
	public void respond(Set<Event> events)
	throws
		ModelContainsError
	{
		super.respond(events);
		
		if (events.size() == 0) return;
		
		if (events.size() > 1) throw new ModelContainsError(
			"Multiple events input to TestForDone");
		
		Event[] eventsArray = events.toArray(new Event[1]);
		Event event = eventsArray[0];
		if (event instanceof StartupEvent) return;
		
		Object obj = event.getNewValue();
		Number number = null;
		try { number = (Number)obj; }
		catch (ClassCastException cce) { throw new ModelContainsError(
			"Input event to TestForDone is not numeric"); }
			
		long count = number.longValue();
		
		// Signal the generator to create another ComponentCreated event.
			
		getFunctionContext().setState(again, count);  // just propagate it.
	}
}

