package marriott;


import expressway.ModelElement.*;
import expressway.ModelAPITypes.*;
import expressway.FunctionBase;
import java.util.Set;


/**

		<!-- Input signal -->
		<port name="Component Change"/>
			
		<!-- Output values -->
		<port name="System Security - Period"/>
		<port name="System Security - Severity"/>

 */

public class Security extends FunctionBase
{
	private int n = 0;
	
	
	public void start(FunctionContext context) throws Exception
	{
		super.start(context);
		this.n = 0;
	}
	

	public void respond(Set<Event> events)
	throws
		ModelContainsError
	{
		super.respond(events);
		
		if (events.size() == 0) return;
		if (events.size() > 1)
		{
			System.out.println(">>>***Multiple input events in Security:");
			for (Event e : events)
			{
				System.out.println("\t" + e.toString());
			}
			
			throw new ModelContainsError(
			"More than one component change event in an epoch.");
		}
		
		
		// Compute fraction of components that are old.
		
		n += 1;
		if (n > 10) //throw new ModelContainsError("More than ten components");
			return; // do not change current state.
		
		double f = (double)(10-n) / 10.0;
		
		
		// Compute new security breach generator parameters.

		double mean_period = 1.0 * (2.0 - f);  // months
		double mean_loss = 1000.0 * (2.0 - f);  // dollars
		
		
		// Locate the State that is to be set.
		
		State mean_period_state = getFunctionContext().getState("Mean Period");
		if (mean_period_state == null) throw new ModelContainsError(
			"Cannot locate State 'Mean Period'");
		
		State mean_loss_state = getFunctionContext().getState("Mean Loss");
		if (mean_loss_state == null) throw new ModelContainsError(
			"Cannot locate State 'Mean Loss'");
		
		
		// Send output event.
		
		getFunctionContext().setState(mean_period_state, new Double(mean_period));
		getFunctionContext().setState(mean_loss_state, new Double(mean_loss));
	}
}

