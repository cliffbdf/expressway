package test.validation;

import expressway.server.ModelElement.*;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ActivityBase;
import java.util.Set;


public class val_C2_3_native extends ActivityBase
{
	public void respond(SortedEventSet events)
	throws
		ModelContainsError
	{
		super.respond(events);
		
		// Check if events are received in the input and bi-directional ports.
		Port inputPort = getActivityContext().getPort("input_port");
		Port biPort = getActivityContext().getPort("bi_port");
		
		if (portHasEvent(events, inputPort)) System.out.println("Event on input port");
		if (portHasEvent(events, biPort)) System.out.println("Event on bi port");
		
		
		// Check if events are received on the output ports.
		Port outPort = getActivityContext().getPort("output_port");
		if (portHasEvent(events, outPort)) throw new ModelContainsError("Event on output port");
	}
}

