package test.motif;

import expressway.gui.MenuItemContext;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.gui.modeldomain.VisualJComponent;
import expressway.ser.*;
import java.io.Serializable;
import javax.swing.JOptionPane;


public class TestMotif
{
	/** Define behavior of menu item. */
	
	public static void setAccountDefaultValue(final MenuItemContext context)
	{
		VisualJComponent visual = (VisualJComponent)(context.getVisualComponent());
		
		String acctId = visual.getNodeId();
		String tallyName = "Balance";
		TallySer tallySer;
		
		try { tallySer = (TallySer)(context.getModelEngine().getChild(acctId, tallyName)); }
		catch (Exception ex)
		{
			ErrorDialog.showThrowableDialog(visual, ex);
			return;
		}
		
		String tallyId = tallySer.getNodeId();
		
		String attrName = "init_value";
		AttributeSer attrSer;
		try { attrSer = context.getModelEngine().getAttribute(tallyId, attrName); }
		catch (Exception ex)
		{
			ErrorDialog.showThrowableDialog(visual, ex);
			return;
		}
		
		String attrId = attrSer.getNodeId();
		
		
		// Determine the current Scenario.
		
		ScenarioVisual scenarioVisual = context.getCurrentScenarioVisual();
		if (scenarioVisual == null)
		{
			ErrorDialog.showErrorDialog(visual, "Must select within a Scenario");
			return;
		}
		
		
		// Obtain the current Attribute value from the server.
		
		Serializable curAttrVal;
		try { curAttrVal = 
			context.getModelEngine().getAttributeValueById(attrId, scenarioVisual.getNodeId());
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(visual, ex);
			return;
		}
		

		// Pop up a Panel to ask for the Attribute value.
		
		String sVal = JOptionPane.showInputDialog(visual,
			"Choose Account default balance", curAttrVal.toString());
			
		if ((sVal == null) || sVal.length() == 0) return;
		
		
		// Call the server to set the Attribute value.
		
		String scenarioId = scenarioVisual.getNodeId();
		
		try
		{
			try { context.getModelEngine().setAttributeValue(false, attrId, 
				scenarioId, sVal); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(visual, w.getMessage()) == JOptionPane.YES_OPTION)
				{
					context.getModelEngine().setAttributeValue(true, attrId, 
						scenarioId, sVal);
				}
			}
		}
		catch (Exception ex)
		{
			ErrorDialog.showThrowableDialog(visual, ex);
		}
		
		JOptionPane.showMessageDialog(null, SupportClass.getMessage());
	}
}


class SupportClass
{
	public static String getMessage() { return "From SupportClass"; }
}
