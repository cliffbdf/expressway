# Set up development environment.

export maxerrs := 30
export java_version := 16

# jvisualvm

export java_api_url := http://docs.oracle.com/javase/6/docs/api/
export proj_dir := $(shell pwd)
cd ${proj_dir}scripts/
export Userdir := $(HOME)
export Library := $(Userdir)/Library
J2EE=$Library/J2EE/Release_6.3

APACHE_Batik=$Library/batik-1.7

APACHE_MATH=$Library/commons-math-1.2
xerces=$Library/xerces-2_11_0
Jetty=$Library/jetty-8.0.4/lib

sable=$Library/sablecc-3.2
# See http://sablecc.org/

swingx=$Library/swingx-1.6
# See http://java.net/projects/swingx/, and http://java.net/downloads/swingx/

orobjects=$Library/ORObjects
# See http://1997.opsresearch.com/

src_root=../javacode
src_dir=$src_root/expressway
build_dir=/Transient/build/all
visualonly_build_dir=/Transient/build/visualonly
serveronly_build_dir=/Transient/build/serveronly
commandonly_build_dir=/Transient/build/commandonly
test_dir=../test
val_dir=../test/validation
spath=../javacode:../test/javacode

extjar=$APACHE_MATH/commons-math-1.2.jar
extjar=$extjar:$xerces/xercesImpl.jar:$xerces/xml-apis.jar:$xerces/serializer.jar:$xerces/resolver.jar
#extjar=$extjar:$swingx/swingx-1.6.jar:$swingx/swingx-beaninfo-1.6.jar
extjar=$extjar:$swingx/swingx-all-1.6.3.jar
extjar=$extjar:$J2EE/javax.servlet.jar
extjar=$extjar:$APACHE_Batik/batik.jar
extjar=$extjar:$Jetty/jetty-util-8.0.4.v20111024.jar
extjar=$extjar:$Jetty/jetty-io-8.0.4.v20111024.jar
extjar=$extjar:$Jetty/jetty-http-8.0.4.v20111024.jar
extjar=$extjar:$Jetty/jetty-server-8.0.4.v20111024.jar
extjar=$extjar:$Jetty/jetty-continuation-8.0.4.v20111024.jar
extjar=$extjar:$Jetty/jetty-security-8.0.4.v20111024.jar
extjar=$extjar:$Jetty/jetty-servlet-8.0.4.v20111024.jar

cpath=$build_dir/:$extjar

sable_out_dir=../SableCCOutput

rcp=$cpath
javac="javac -Xmaxerrs $maxerrs -g -sourcepath $spath -classpath $cpath -d $build_dir"
parser_cp=$build_dir/expressway/tools/
dbdir=$Userdir/Documents/Expressway/build/test
HighRishExampleDir=/Users/cliffordberg/Documents/Expressway/Website/site/Whitepapers


# -----------------------------------------------------------------------------
# Used by some scripts:

FINRA=/Users/cliffordberg/Documents/Expressway/Clients/FINRA/
ANT_HOME=/Applications/apache-ant-1.7.0
PATH=$PATH:$ANT_HOME/bin
junit=/Applications/junit-4.4.jar
open_laszlo=/Applications/"OpenLaszlo Server 4.0.6"


# -----------------------------------------------------------------------------
# Extract external packages into the build directory, so that they can be combined
# into the Expressway JAR files.

cd $visualonly_build_dir
jar xvf $APACHE_MATH/commons-math-1.2.jar
jar xvf $swingx/swingx-1.6.jar
jar xvf $swingx/swingx-beaninfo-1.6.jar
jar xvf $xerces/xercesImpl.jar
jar xvf $xerces/xml-apis.jar
jar xvf $xerces/serializer.jar
jar xvf $xerces/resolver.jar

cd $serveronly_build_dir
jar xvf $APACHE_MATH/commons-math-1.2.jar
jar xvf $APACHE_Batik/batik.jar
jar xvf $orobjects/or124.jar
jar xvf $xerces/xercesImpl.jar
jar xvf $xerces/xml-apis.jar
jar xvf $xerces/serializer.jar
jar xvf $xerces/resolver.jar
jar xvf $APACHE_Batik/batik.jar
jar xvf $APACHE_Batik/lib/batik-svggen.jar
jar xvf $APACHE_Batik/lib/batik-svg-dom.jar
jar xvf $APACHE_Batik/lib/batik-gvt.jar
jar xvf $APACHE_Batik/lib/batik-parser.jar
jar xvf $APACHE_Batik/lib/batik-dom.jar
jar xvf $APACHE_Batik/lib/batik-awt-util.jar
jar xvf $APACHE_Batik/lib/batik-xml.jar
jar xvf $APACHE_Batik/lib/batik-css.jar
jar xvf $APACHE_Batik/lib/batik-util.jar
jar xvf $APACHE_Batik/lib/batik-gui-util.jar
jar xvf $APACHE_Batik/lib/batik-ext.jar
jar xvf $J2EE/javax.servlet.jar
jar xvf $Jetty/jetty-util-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-io-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-http-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-server-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-continuation-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-security-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-servlet-8.0.4.v20111024.jar

cd $commandonly_build_dir
jar xvf $APACHE_MATH/commons-math-1.2.jar
jar xvf $APACHE_Batik/batik.jar
jar xvf $orobjects/or124.jar
jar xvf $xerces/xercesImpl.jar
jar xvf $xerces/xml-apis.jar
jar xvf $xerces/serializer.jar
jar xvf $xerces/resolver.jar
jar xvf $APACHE_Batik/batik.jar
jar xvf $APACHE_Batik/lib/batik-svggen.jar
jar xvf $APACHE_Batik/lib/batik-svg-dom.jar
jar xvf $APACHE_Batik/lib/batik-gvt.jar
jar xvf $APACHE_Batik/lib/batik-parser.jar
jar xvf $APACHE_Batik/lib/batik-dom.jar
jar xvf $APACHE_Batik/lib/batik-awt-util.jar
jar xvf $APACHE_Batik/lib/batik-xml.jar
jar xvf $APACHE_Batik/lib/batik-css.jar
jar xvf $APACHE_Batik/lib/batik-util.jar
jar xvf $APACHE_Batik/lib/batik-gui-util.jar
jar xvf $APACHE_Batik/lib/batik-ext.jar
jar xvf $J2EE/javax.servlet.jar
jar xvf $Jetty/jetty-util-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-io-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-http-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-server-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-continuation-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-security-8.0.4.v20111024.jar
jar xvf $Jetty/jetty-servlet-8.0.4.v20111024.jar

cd ${proj_dir}scripts/


# -----------------------------------------------------------------------------
# Compile the independent generalpurpose classes.

$javac $src_root/generalpurpose/*.java
$javac $src_root/swing/*.java
$javac $src_root/debug/*.java


# -----------------------------------------------------------------------------
# Compile and test the independent geometry classes.

$javac $src_root/geometry/*.java
javac -classpath $cpath -d $build_dir/../test/classes ../test/javacode/test/geometry/TestGeometry.java
java -classpath $rcp:$build_dir/../test/classes test.geometry.TestGeometry


# -----------------------------------------------------------------------------
# Compile and test the independent graph classes.

$javac $src_root/graph/*.java
javac -classpath $cpath -d $build_dir/../test/classes ../test/javacode/test/graph/TestGraph.java
java -classpath $rcp:$build_dir/../test/classes test.graph.TestGraph


# -----------------------------------------------------------------------------
# Compile the independent statistics package.


javac -g -sourcepath "$spath" -classpath $cpath:$orobjects/or124.jar:$APACHE_MATH/commons-math-2.2.jar -d "$build_dir" $src_root/statistics/*.java
java -classpath $rcp statistics.FitGammaDistribution


# -----------------------------------------------------------------------------
# Generate interpreter base classes that are needed by the ExpressionInterpreter.

java -jar $sable/lib/sablecc.jar -d $sable_out_dir --no-inline expr_grammar.sablecc
javac -classpath $cpath -d $build_dir $sable_out_dir/expressway/tools/node/*.java $sable_out_dir/expressway/tools/lexer/*.java $sable_out_dir/expressway/tools/analysis/*.java $sable_out_dir/expressway/tools/parser/*.java
cp $sable_out_dir/expressway/tools/lexer/lexer.dat $build_dir/expressway/tools/lexer
cp $sable_out_dir/expressway/tools/parser/parser.dat $build_dir/expressway/tools/parser


# -----------------------------------------------------------------------------
# Compile and run the Expression test suite.

javac -classpath $build_dir -d $build_dir/../test/classes/ ../test/javacode/test/expression/TestExpression.java
java -Xmx256m -classpath $cpath:$build_dir/../test/classes/ test.expression.TestExpression


# -----------------------------------------------------------------------------
# Compile the server interface.

$javac $src_dir/common/*.java
$javac $src_dir/ser/*.java


# -----------------------------------------------------------------------------
# Compile the help libraries.

$javac $src_dir/help/*.java


# -----------------------------------------------------------------------------
# Compile Model Engine server implementation classes.

$javac $src_dir/server/*.java


# -----------------------------------------------------------------------------
# Generate server stubs and skeletons.

rmic -g -keep -d $build_dir -classpath $cpath expressway.server.ModelEngineRMIPojoImpl
rmic -g -keep -d $build_dir -classpath $cpath expressway.server.ListenerRegistrarImpl


# -----------------------------------------------------------------------------
# Compile GUI classes.

$javac $src_dir/../awt/*.java
$javac $src_dir/gui/graph/*.java
$javac $src_dir/gui/*.java
$javac $src_dir/gui/modeldomain/*.java
$javac $src_dir/gui/activityrisk/*.java
$javac $src_dir/gui/domainlist/*.java
$javac $src_dir/gui/hier/*.java
$javac $src_dir/gui/decision/*.java


# -----------------------------------------------------------------------------
# Generate GUI stubs and skeletons.

rmic -g -keep -d $build_dir -classpath $cpath expressway.gui.PeerListenerImpl


# -----------------------------------------------------------------------------
# Generate documentation.

javadoc -link $java_api_url -protected -splitindex -d ../javadoc -classpath $cpath -sourcepath $spath expressway.common expressway.gui expressway.gui.graph expressway.gui.modeldomain expressway.gui.domainlist expressway.gui.activityrisk expressway.help expressway.ser generalpurpose geometry graph statistics awt


# -----------------------------------------------------------------------------
# Create JAR files that require external packages.

#jar cmf manifest_command command.jar -C $build_dir expressway -C $build_dir generalpurpose -C $build_dir geometry -C $build_dir statistics
#jar cmf manifest_gui visual.jar -C $build_dir awt -C $build_dir images -C $build_dir expressway/common -C $build_dir expressway/gui -C $build_dir expressway/ser -C $build_dir generalpurpose -C $build_dir geometry -C $build_dir statistics -C $build_dir graph -C $build_dir treetable -C $build_dir expressway/server/ModelEngineRMIPojoImpl_Stub.class -C $build_dir expressway/server/ListenerRegistrarImpl_Stub.class
#jar cmf manifest_server server.jar -C $build_dir images -C $build_dir expressway/common -C $build_dir expressway/ser -C $build_dir expressway/server -C $build_dir expressway/tools -C $build_dir generalpurpose -C $build_dir geometry -C $build_dir statistics -C $build_dir expressway/gui/ViewPanelBase\$PeerListenerImpl_Stub.class


# -----------------------------------------------------------------------------
# Create standalone JAR files.

cp ../icons/*.png $build_dir/images/

jar cmf manifest_gui visual.jar \
	-C $build_dir expressway/common \
	-C $build_dir expressway/help \
	-C $build_dir expressway/gui \
	-C $build_dir expressway/ser \
	-C $build_dir expressway/server/ModelEngineRMIPojoImpl_Stub.class \
	-C $build_dir expressway/server/ListenerRegistrarImpl_Stub.class \
	-C $build_dir awt \
	-C $build_dir images \
	-C $build_dir generalpurpose \
	-C $build_dir debug \
	-C $build_dir swing \
	-C $build_dir geometry \
	-C $build_dir statistics \
	-C $build_dir graph \
	-C $visualonly_build_dir org


jar cmf manifest_server server.jar \
	-C $build_dir expressway/common \
	-C $build_dir expressway/ser \
	-C $build_dir expressway/server \
	-C $build_dir expressway/tools \
	-C $build_dir expressway/gui/PeerListenerImpl_Stub.class \
	-C $build_dir awt \
	-C $build_dir images \
	-C $build_dir generalpurpose \
	-C $build_dir geometry \
	-C $build_dir statistics \
	-C $serveronly_build_dir org \
	-C $serveronly_build_dir javax


jar cmf manifest_command command.jar \
	-C $build_dir expressway/common \
	-C $build_dir awt \
	-C $build_dir images \
	-C $build_dir expressway/ser \
	-C $build_dir expressway/server \
	-C $build_dir expressway/tools \
	-C $build_dir generalpurpose \
	-C $build_dir geometry \
	-C $build_dir statistics \
	-C $build_dir expressway/gui/PeerListenerImpl_Stub.class \
	-C $commandonly_build_dir org \
	-C $serveronly_build_dir javax


# -----------------------------------------------------------------------------
# Run the server using JAR files that have external dependencies.

rmiregistry&
java -Xmx256m -Djava.rmi.server.codebase="file://${build_dir}/ file://${APACHE_MATH}" -classpath $rcp expressway.server.ModelEngineRMIPojoImpl -dbdir=$dbdir -random_seed=1 -import="/Users/cliffordberg/Documents/AAAA/Course - IASA/course.xml"
java -Xmx256m -D$server_rmi_codebase -classpath $extjar:server.jar expressway.server.ModelEngineRMIPojoImpl -dbdir=$dbdir -random_seed=1 -import="/Users/cliffordberg/Documents/AAAA/Course - IASA/course.xml"

#java -Xmx256m -Djava.rmi.server.codebase="file://${build_dir}/ file://${APACHE_MATH}" -classpath $rcp expressway.server.ModelEngineRMIPojoImpl -dbdir=$dbdir -import=$test_dir/GUI_tests/GUI_test.xml
#java -Xmx256m -Djava.rmi.server.codebase="file://${build_dir}/ file://${APACHE_MATH}" -classpath $rcp expressway.server.ModelEngineRMIPojoImpl -dbdir=$dbdir -random_seed=1 -import=$test_dir/simple_demo/simple_demo.xml
#java -Xmx256m -Djava.rmi.server.codebase="file://${build_dir}/ file://${APACHE_MATH}" -classpath $rcp expressway.server.ModelEngineRMIPojoImpl -dbdir=$dbdir -random_seed=1 -load="/Users/cliffordberg/Documents/Expressway/Prototype/scripts/save.ser"


# -----------------------------------------------------------------------------
# Run the GUI.

java -Xmx256m -classpath $rcp expressway.gui.Main
java -Xmx256m -D$client_rmi_codebase -classpath ${proj_dir}scripts/visual.jar:$extjar expressway.gui.Main

#java -Xmx256m -classpath $rcp:$tinylaf -Dswing.defaultlaf=de.muntjak.tinylookandfeel.TinyLookAndFeel expressway.gui.Main
#java -Xmx256m -classpath $rcp:$oyoaha -Dswing.defaultlaf=com.oyoaha.swing.plaf.oyoaha.OyoahaLookAndFeel expressway.gui.Main
#java -Xmx256m -classpath $rcp:$ -Dswing.defaultlaf= expressway.gui.Main
#java -Xmx256m -classpath $rcp:$ -Dswing.defaultlaf= expressway.gui.Main


# -----------------------------------------------------------------------------
# Run the server and client using standalone JAR files.

java -Xmx256m -Djava.rmi.server.codebase="file://${proj_dir}scripts/server.jar" -jar server.jar -dbdir=$dbdir -random_seed=1 -import="/Users/cliffordberg/Documents/AAAA/Course - IASA/course.xml"
java -Xmx256m -Djava.rmi.server.codebase="file://${proj_dir}scripts/visual.jar" -jar visual.jar

java -Xms512m -Xmx512m -jar server.jar
java -Xms512m -Xmx512m -jar visual.jar

java -jar command.jar -random_seed=1 /Users/cliffordberg/Documents/Expressway/Website/site/Whitepapers/HighRiskFeatures.xml


# -----------------------------------------------------------------------------
# When running a security manager:

java -Djava.security.manager -Djava.security.policy="file:/C:/Documents and Settings/Owner/.java.policy" -Djava.rmi.server.codebase="file:/C:/Expressway/server.jar" -classpath server.jar expressway.server.Main
java -Djava.security.manager -Djava.security.policy="file:/C:/Documents and Settings/Owner/.java.policy" -Djava.rmi.server.codebase="file:/C:/Expressway/server.jar" -jar server.jar



# ---------------------------------TESTS---------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Compile and run the Conduit tests.

javac -classpath $cpath -d $build_dir/../test/classes ../test/javacode/test/conduit/TestConduit.java
java -Xmx256m -classpath $build_dir/../test/classes:$rcp test.conduit.TestConduit


# -----------------------------------------------------------------------------
# Compile source for the Full Model test suite.

#javac -classpath $cpath -d $build_dir/../test/classes ../test/javacode/test/Test2.java
javac -classpath $cpath -d $build_dir/../test/classes ../test/javacode/test/ReplacementCosts.java
javac -classpath $cpath -d $build_dir/../test/classes ../test/javacode/test/ReplaceSystems.java

javac -classpath $cpath -d $build_dir/../test/classes ../test/Marriott/javacode/marriott/TestForDone.java
javac -classpath $cpath -d $build_dir/../test/classes ../test/Marriott/javacode/marriott/Security.java
javac -classpath $cpath -d $build_dir/../test/classes ../test/Marriott/javacode/marriott/CumLoss.java


# -----------------------------------------------------------------------------
# Execute the semantic test suite.

javac -classpath $cpath -d $build_dir/../test/classes ../test/javacode/test/validation/val_C1_1_A_native.java
javac -classpath $cpath -d $build_dir/../test/classes ../test/javacode/test/validation/val_C2_3_native.java

#java -classpath $build_dir/../test/classes:$rcp org.junit.runner.JUnitCore test.Test2 > Test2.output.txt
#java -jar command.jar model.xml > model.output.txt
#java -Xmx256m -jar command.jar model2.xml > model2.output.txt
#java -Xmx256m -jar command.jar GenTest.xml > GenTest.output.txt

java -jar command.jar test_define.xml

# Run without verification:
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_1_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_1_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_1_3e.xml  # an error test
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_1_4.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_2_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_3_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_3_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_3_3e.xml  # an error test
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_4_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_4_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_5_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_5_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_7_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_7_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_7_3.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_7_4.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_8_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_8_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_8_3.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_9_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_9_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_10_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_10_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_11_1.xml
#java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_11_2.xml # test is incomplete
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_12_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_12_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_14_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_14_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_C1_1e.xml  # an error test
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_C1_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_C1_3.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_C1_4.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_C1_5.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_C1_6e.xml  # an error test
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_C2_1.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_C2_2.xml
java -Xmx256m -jar command.jar -random_seed=1 $val_dir/val_C2_3.xml

#Run with verification:
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_1_1.events.archive -domain=val_1_1 -scenario=default $val_dir/val_1_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_1_2.events.archive -domain=val_1_2 -scenario=default $val_dir/val_1_2.xml
#java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_1_3.events.archive -domain=val_1_3 -scenario=default $val_dir/val_1_3e.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_1_4.events.archive -domain=val_1_4 -scenario=default $val_dir/val_1_4.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_2_1.events.archive -domain=val_2_1 -scenario=default $val_dir/val_2_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_3_1.events.archive -domain=val_3_1 -scenario=default $val_dir/val_3_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_3_2.events.archive -domain=val_3_2 -scenario=default $val_dir/val_3_2.xml
#java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_3_3.events.archive -domain=val_3_3 -scenario=default $val_dir/val_3_3e.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_4_1.events.archive -domain=val_4_1 -scenario=default $val_dir/val_4_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_4_2.events.archive -domain=val_4_2 -scenario=default $val_dir/val_4_2.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_5_1.events.archive -domain=val_5_1 -scenario=default $val_dir/val_5_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_5_2.events.archive -domain=val_5_2 -scenario=default $val_dir/val_5_2.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_7_1.events.archive -domain=val_7_1 -scenario=default $val_dir/val_7_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_7_2.events.archive -domain=val_7_2 -scenario=default $val_dir/val_7_2.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_7_3.events.archive -domain=val_7_3 -scenario=default $val_dir/val_7_3.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_7_4.events.archive -domain=val_7_4 -scenario=default $val_dir/val_7_4.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_8_1.events.archive -domain=val_8_1 -scenario=default $val_dir/val_8_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_8_2.events.archive -domain=val_8_2 -scenario=default $val_dir/val_8_2.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_8_3.events.archive -domain=val_8_3 -scenario=default $val_dir/val_8_3.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_9_1.events.archive -domain=val_9_1 -scenario=default $val_dir/val_9_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_9_2.events.archive -domain=val_9_2 -scenario=default $val_dir/val_9_2.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_10_1.events.archive -domain=val_10_1 -scenario=default $val_dir/val_10_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_10_2.events.archive -domain=val_10_2 -scenario=default $val_dir/val_10_2.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_11_1.events.archive -domain=val_11_1 -scenario=default $val_dir/val_11_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_11_2.events.archive -domain=val_11_2 -scenario=default $val_dir/val_11_2.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_12_1.events.archive -domain=val_12_1 -scenario=default $val_dir/val_12_1.xml
#java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_12_2.events.archive -domain=val_12_2 -scenario=default $val_dir/val_12_2.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_14_1.events.archive -domain=val_14_1 -scenario=default $val_dir/val_14_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_14_2.events.archive -domain=val_14_2 -scenario=default $val_dir/val_14_2.xml
#java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_C1_1.events.archive -domain=val_C1_1 -scenario=default $val_dir/val_C1_1e.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_C1_2.events.archive -domain=val_C1_2 -scenario=default $val_dir/val_C1_2.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_C1_3.events.archive -domain=val_C1_3 -scenario=default $val_dir/val_C1_3.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_C1_4.events.archive -domain=val_C1_4 -scenario=default $val_dir/val_C1_4.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_C1_5.events.archive -domain=val_C1_5 -scenario=default $val_dir/val_C1_5.xml
#java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_C1_6.events.archive -domain=val_C1_6 -scenario=default $val_dir/val_C1_6e.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_C2_1.events.archive -domain=val_C2_1 -scenario=default $val_dir/val_C2_1.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_C2_2.events.archive -domain=val_C2_2 -scenario=default $val_dir/val_C2_2.xml
java -Xmx256m -jar command.jar -random_seed=1 -val_archive_file=$val_dir/GoodResults/val_C2_3.events.archive -domain=val_C2_3 -scenario=default $val_dir/val_C2_3.xml



# -----------------------------------------------------------------------------
# Run the course example using Expressway.

java -Xmx256m -jar command.jar -random_seed=1 "/Users/cliffordberg/Documents/AAAA/Course - IASA/course.xml"


# -----------------------------------------------------------------------------
# Compile and run the Complex Statistical tests (independent verification of Course Example).

javac -classpath $orobjects:$APACHE_MATH:$build_dir -d $build_dir/../test/classes $test_dir/javacode/test/statistical/val_S1.java
java -classpath $build_dir/../test/classes:$orobjects:$APACHE_MATH:$build_dir test.statistical.val_S1



javac -classpath $orobjects:$APACHE_MATH:$build_dir -d $build_dir/../test/classes $test_dir/javacode/test/statistical/val_Stats.java
java -classpath $build_dir/../test/classes:$orobjects:$APACHE_MATH:$build_dir test.statistical.val_Stats




# -----------------------------------------------------------------------------
# Run the VIPR model.

java -Xmx256m -jar command.jar -random_seed=1 vipr.xml
java -Xmx256m -jar command.jar -random_seed=1 vipr_cots.xml


# -----------------------------------------------------------------------------
# Run the FINRA model.

javac -classpath $cpath -d $build_dir/../test/classes ${FINRA}FINRA_Model/java/finra/Respond_to_Vendor_native.java
java -Xmx256m -jar command.jar -random_seed=1 finra.xml


# -----------------------------------------------------------------------------
# Run the simple demo.

java -Xmx256m -jar command.jar -random_seed=1 ../test/simple_demo/simple_demo.xml


# -----------------------------------------------------------------------------
# Run the "High-Risk" Example.

java -Xms512m -Xmx512m -jar command.jar -random_seed=1 $HighRishExampleDir/SimScen1.xml
java -Xms512m -Xmx512m -jar command.jar -random_seed=1 $HighRishExampleDir/SimScen1a+.xml
java -Xms512m -Xmx512m -jar command.jar -random_seed=1 $HighRishExampleDir/SimScen1a-.xml
java -Xms512m -Xmx512m -jar command.jar -random_seed=1 $HighRishExampleDir/SimScen2.xml
java -Xms512m -Xmx512m -jar command.jar -random_seed=1 $HighRishExampleDir/SimScen2a+.xml
java -Xms512m -Xmx512m -jar command.jar -random_seed=1 $HighRishExampleDir/SimScen2a-.xml
java -Xms512m -Xmx512m -jar command.jar -random_seed=1 $HighRishExampleDir/SimScen2b+.xml
java -Xms512m -Xmx512m -jar command.jar -random_seed=1 $HighRishExampleDir/SimScen2b-.xml

java -Xms512m -Xmx512m -jar command.jar -random_seed=1 $HighRishExampleDir/HighRiskFeatures.xml




# =============================================================================
# AWT behavior exploration:
$javac $src_dir/../trythis/TestAWTBehavior.java
java -Xmx256m -classpath $build_dir/../test/classes:$rcp trythis.TestAWTBehavior


# For profiling using jrat: (see http://jrat.sourceforge.net/quickstart.html)
#java -javaagent:shiftone-jrat.jar [your java ops] [main class]
#java -Xmx256M -jar shiftone-jrat.jar


# =============================================================================

# -----------------------------------------------------------------------------
# Experimental: behavioral grammar.

java -jar $sable/lib/sablecc.jar -d $sable_out_dir --no-inline grammar.sablecc
javac -sourcepath $sable_out_dir -classpath $cpath -d $build_dir $src_dir/tools/Main.java
cp $sable_out_dir/expressway/tools/lexer/lexer.dat $build_dir/expressway/tools/lexer
cp $sable_out_dir/expressway/tools/parser/parser.dat $build_dir/expressway/tools/parser
jar cvf parser.jar -C $build_dir expressway

# -----------------------------------------------------------------------------
# Test treetable.

javac -classpath $cpath -d $build_dir $src_root/treetable/AbstractCellEditor.java
javac -classpath $cpath -d $build_dir $src_root/treetable/AbstractTreeTableModel.java
javac -classpath $cpath -d $build_dir $src_root/treetable/BookmarkDirectory.java
javac -classpath $cpath -d $build_dir $src_root/treetable/BookmarkEntry.java
javac -classpath $cpath -d $build_dir $src_root/treetable/Bookmarks.java
javac -classpath $cpath -d $build_dir $src_root/treetable/DynamicTreeTableModel.java
javac -classpath $cpath -d $build_dir $src_root/treetable/BookmarksModel.java
javac -classpath $cpath -d $build_dir $src_root/treetable/TreeTableModelAdapter.java
javac -classpath $cpath -d $build_dir $src_root/treetable/JTreeTable.java
javac -classpath $cpath -d $build_dir $src_root/treetable/MergeSort.java
javac -classpath $cpath -d $build_dir $src_root/treetable/Test.java

java -classpath $build_dir treetable.Test


# =============================================================================
# Test DualHierarchyPanel:
$javac ../javacode/swing/DualHierarchyPanel.java
javac -classpath $build_dir -d $build_dir/../test/classes/ ../test/javacode/test/swing/TestDualHierarchyPanel.java
java -Xmx256m -classpath $cpath:$build_dir/../test/classes/ test.swing.TestDualHierarchyPanel


#javac -classpath $build_dir -d $build_dir/../test/classes/ ../test/javacode/test/swing/TestTreeTableHeader.java
#java -Xmx256m -classpath $cpath:$build_dir/../test/classes/ test.swing.TestTreeTableHeader



# =============================================================================
# Test the -library option.

java -jar command.jar -library ...... TestLibraryOption.xml



# =============================================================================
# Compile and run the Motif test.

javac -classpath $cpath -d $build_dir/../test/classes ../test/javacode/test/motif/TestMotif.java
jar cvf $build_dir/../test/jars/ThisMotif.jar -C ./../test/GUI_tests BasicMotifTest.xml -C $build_dir/../test/classes test -C $build_dir/../test/classes AccountIconImage.png
rm ./motifs/ThisMotif.jar
rm /Users/cliffordberg/expressway/motifs/ThisMotif.jar
java -jar command.jar -motif=$build_dir/../test/jars/ThisMotif.jar ../test/GUI_tests/MyOtherDomain.xml



# =============================================================================
# Java bugs:

# The JPanel setName problem:

$javac $src_dir/../trythis/TestThis5.java
java -Xmx256m -classpath $build_dir/../test/classes:$rcp trythis.TestThis5
