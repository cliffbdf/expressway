package expressway.ser;

import expressway.common.*;
import java.io.Serializable;
import expressway.generalpurpose.StringUtils;


public class StrategySer extends StrategyElementSer
	implements HierarchySer, TabularNodeSer
{
	public static String[] fieldNames =
	{
		"Name",
		"HTML Summary",
		"HTML Full Description"
	};
	
	public String htmlSummary;
	public String[] subStrategyNodeIds;
	public int seqNo;
	
	public String getNodeKind() { return NodeKindNames.Strategy; }
	
	
  /* From HierarchySer */

	
	public void setSubHierarchyNodeIds(String[] ids) { this.subStrategyNodeIds = ids; }
	
	
	public String[] getSubHierarchyNodeIds() { return subStrategyNodeIds; }
	
	
  /* From TabularNodeSer */

	
	public String[] getFieldNames() { return fieldNames; }
	
	
	public void setValueAt(Serializable value, int column)
	{
		switch (column)
		{
		case 0: setName((String)value); break;
		case 1: htmlSummary = StringUtils.safeToStringOrNull(value); break;
		case 2: setHtmlDescription(StringUtils.safeToStringOrNull(value)); break;
		}
	}
	
	
	public Serializable getValueAt(int column)
	{
		switch (column)
		{
		case 0: return getName();
		case 1: return htmlSummary;
		case 2: return getHtmlDescription();
		}
		
		throw new RuntimeException("Unexpected column number: " + column);
	}


	public int getSeqNo() { return seqNo; }
}

