/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package archive;

import expressway.ModelAPITypes.*;
import expressway.DecisionElement.*;

class DecisionDomainVersionImpl extends DecisionElementImpl implements DecisionDomainVersion
{
	public DecisionDomain dd = null;
	public Set<DecisionScenario> scenarios = new Vector<DecisionScenario>();


	public DecisionDomainVersionImpl(DecisionDomain dd)
	{
		this.dd = dd;
	}


	/*
	 * Construct a DecisionScenario to represent the change.
	 */

	public DecisionScenario createScenario()
	{
		DecisionScenario scenario = new DecisionScenario(this);
		scenarios.add(scenario);
	}
}
