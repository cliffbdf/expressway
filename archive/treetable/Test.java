package treetable;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.text.DateFormat;

public class Test
{
	/** Used to represent the model. */
	private JTreeTable treeTable;
	
	/** Frame containing everything. */
	private JFrame frame;
	
	/** Number of instances of TreeTableExample3. */
	private static int         ttCount;
	
	public static void main(String[] args)
	{
		Test test = new Test();
	}
	
	
	public Test()
	{
		ttCount++;
		
		// Create a Frame to house the Tree Table.
		
		frame = new JFrame("TreeTable");
	
		frame.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent we)
			{
				frame.dispose();
				if (--ttCount == 0) {
					System.exit(0);
				}
			}
		});
		
		
		// Create the Tree Table Model.
		
		Bookmarks bookmarks = new Bookmarks();
		TreeTableModel model = new BookmarksModel(bookmarks.getRoot());

		
		// Create the Tree Table.
		
		treeTable = new JTreeTable(model);

		treeTable.setDefaultRenderer(Date.class, new BookmarksDateRenderer());
		treeTable.setDefaultRenderer(Object.class, new BookmarksStringRenderer());
		
		
		// Create a scroll pane.
		
		JScrollPane sp = new JScrollPane(treeTable);
		sp.getViewport().setBackground(Color.white);
		frame.getContentPane().add(sp);
		
		
		// Make everything visible.
		
		frame.pack();
		frame.show();
	}
	
	
   /**
     * The renderer used for Dates in the TreeTable. The only thing it does,
     * is to format a null date as '---'.
     */
    private static class BookmarksDateRenderer extends DefaultTableCellRenderer
	{
		DateFormat formatter;
		public BookmarksDateRenderer() { super(); }
	
		public void setValue(Object value)
		{ 
			if (formatter==null) {
				formatter = DateFormat.getDateInstance(); 
			}
			
			setText((value == null) ? "---" : formatter.format(value)); 
		}
    }


    /**
     * The renderer used for String in the TreeTable. The only thing it does,
     * is to format a null String as '---'.
     */
    static class BookmarksStringRenderer extends DefaultTableCellRenderer
	{
		public BookmarksStringRenderer() { super(); }
	
		public void setValue(Object value)
		{ 
			setText((value == null) ? "---" : value.toString());
		}
    }
}

