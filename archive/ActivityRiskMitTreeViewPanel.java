/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.activityrisk;

import expressway.common.*;
import expressway.gui.modeldomain.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.common.VisualComponent.*;
import expressway.gui.*;
import expressway.ser.*;
import expressway.generalpurpose.ThrowableUtil;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.TreeTableModel;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.decorator.CompoundHighlighter;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.StringValue;
import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.Container;
import java.awt.Color;
import java.awt.BorderLayout;
import java.rmi.server.UnicastRemoteObject;
import java.lang.reflect.Field;
import java.awt.Component;
import java.rmi.RemoteException;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.table.TableCellRenderer;


/** ****************************************************************************
 * Implements a tabular View of an Activity, depicting the nested Activities as
 * a "tree table": the first column of the table contains a tree (one row per
 * tree node) that breaks down the hierarchy of Activities within this View's
 * root Activity; the other table columns identify "evidence" and "sufficiency
 * criteria" for the row's Activity. This type of View is used for planning
 * project risk mitigation activities.
 */
 
public class ActivityRiskMitTreeViewPanel extends ViewPanelBase implements TabularView
{
  /* ***************************************************************************
	 * *************************************************************************
	 * Constants.
	 */
	
	public static final String ViewTypeName = "RiskMitigationActivityView";
	
	public static final Color BackgroundColor = new Color(200, 200, 255);
	
	public static final Color BackgroundSelectionColor = new Color(250, 200, 255);
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Initiatlization permanent state.
	 */
	
	/** The root Activity Node representing a project that is composed of a Set
	 of Activities. It is expected that this Activity is a Transformation. */
	private final ActivitySer activity;
	
	
	/** The Model Scenario that provides context for the Activity, so that its
	 Attribute values can be determined. */
	private final String modelScenarioNodeId;
	
	
	/** The Model Engine that owns the Nodes that are represented by this View. */
	private final ModelEngineRMI modelEngine;
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Transient state:
	 * The root node of the tree that is depicted.
	 */
	
	/** The Peer Listener that listens for server messages pertaining to the
	 Visual Components of this View. */
	private PeerListener peerListener;
	

	/** Listens to the server for changes to the server-side components that are
	 represented in this View. */
	private ListenerRegistrar listenerRegistrar = null;  // may be null.
	
	
	/** The TreeTable is placed in this. */
	VisualComponentDisplayAreaImpl displayArea;


	/** The root node of the JTree that is depicted in this View, that represents
	 the server-side Nodes. */
    private DefaultMutableTreeTableNode root = null;
	
	
	/** Parent node that new entries are added to. */
	private DefaultMutableTreeTableNode curParent;
	

	/** Contains the client-side data that is represented by the View. */
	private TreeTableModel treeTableModel = null;

	
	/** The Tree Table displayed within this JPanel. */
	private JXTreeTable treeTable = null;
	
	
	/** Scroll pane to contain the tree table. */
	private JScrollPane scrollPane;
	

	/** The Set of all Visuals owned by this View. */
	private Set<ActivityTreeData> visuals = new HashSet<ActivityTreeData>();
	

	/** The Set of currently selected Visuals. */
	private Set<VisualComponent> selectedVisuals = new HashSet<VisualComponent>();
	

	/** The x-dimension size of a display space pixel in the real-valued space
	 of this View. */
	private double pixelXSize = 1.0;
	
	
	/** The y-dimension size of a display space pixel in the real-valued space
	 of this View. */
	private double pixelYSize = 1.0;
	
	
	/** Display space x coordinate of this JPanel's origin. */
	private int xOriginOffset = 0;
	
	
	/** Display space y coordinate of this JPanel's origin. Note that in the
	 display space, the origin is at the top left of the screen, and the positive
	 y direction is downward. */
	private int yOriginOffset = 0;
	
	/** Not currently used. */
	private boolean isAPopup = false;
	
	 
	 
  /* ***************************************************************************
	 * *************************************************************************
	 * Constructors.
	 */
	 
	 
	/** ************************************************************************
     * Constructor.
     */
	 
    public ActivityRiskMitTreeViewPanel(PanelManager panelManager, ActivitySer activitySer,
		String modelScenarioNodeId, ModelEngineRMI modelEngine)
	{
		super(panelManager, activitySer.getName());
		this.activity = activitySer;
		this.modelScenarioNodeId = modelScenarioNodeId;
		this.modelEngine = modelEngine;
		
		setBackground(BackgroundColor);
		//setBackground(Color.GREEN);
		setLayout(new BorderLayout());
		
		show();
	}
	
	
	public ViewPanelBase postConstructionSetup()
	throws
		Exception
	{
		// Create a TreeNode as the root.
		
		//ActivityTreeData rootData = new ActivityTreeData(this, activitySer);
		//this.root = new DefaultMutableTreeTableNode(rootData);
		this.root = addTreeNode(null, this.activity, this.modelScenarioNodeId);
		this.curParent = root;
		//this.visuals.add(rootData);
		//rootData.setTreeNode(root);
		//listenerRegistrar.subscribe(activitySer.getNodeId(), true);

		// Create a Tree Table Model.
		
		this.treeTableModel = new .....ActivityRiskMitModel(root);

		
		// Create a Tree Table.
		
		this.treeTable = new JXTreeTable(treeTableModel);
		//this.treeTable.setPreferredSize(new java.awt.Dimension(800, 400));
		//this.treeTable.setSize(new java.awt.Dimension(800, 400));
		//this.treeTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		Highlighter highligher = HighlighterFactory.createSimpleStriping(
			HighlighterFactory.BEIGE);
		this.treeTable.setHighlighters(highligher);
		this.treeTable.setShowGrid(false);
		this.treeTable.setShowsRootHandles(false);
		this.treeTable.setColumnControlVisible(true);
		this.treeTable.setTreeCellRenderer(new TreeTableCellRenderer());

		this.treeTable.setDefaultRenderer(String.class, new DefaultTableRenderer(
		new StringValue()
		{
			public String getString(Object value) { return 
				value == null ? "null" : value.toString(); }            
		}));
		
		this.displayArea = new VisualComponentDisplayAreaImpl();
		this.displayArea.add(this.treeTable);
		this.scrollPane = new JScrollPane(this.displayArea);
		add(this.scrollPane, BorderLayout.CENTER);
		//this.scrollPane.setSize(800, 400);
		//this.scrollPane.setPreferredSize(new java.awt.Dimension(800, 400));
		
		this.treeTable.setRootVisible(true);
		
		this.treeTable.doLayout();


		// A View owns a PeerListener for all of the Persistent Nodes that it depicts.
		
		if (modelEngine instanceof ModelEngineRMI)
			if (this.peerListener == null)
			{
				this.peerListener = new PeerListenerImpl(activity.getName());
				this.listenerRegistrar = 
					((ModelEngineRMI)modelEngine).registerPeerListener(peerListener);
			}
		
		this.shallowPopulate();
		
		return this;
	}



  /* ***************************************************************************
	 * *************************************************************************
	 * Public methods..
	 */
	 
	 
	/** ************************************************************************
	 * Accessor: return the root Tree Node.
	 */
	 
	public DefaultMutableTreeTableNode getRoot() { return root; }


	
  /* ***************************************************************************
	 * *************************************************************************
	 * Methods defined in View interface.
	 */
	 
	 
	public VisualComponent getOutermostVisual()
	{
		return (VisualComponent)(root.getUserObject());
	}
	
	
	public VisualComponentDisplayArea getDisplayArea() { return this.displayArea; }
	
	
	public ModelEngineRMI getModelEngine()
	{
		return modelEngine;
	}
	
	
	public boolean isPopup() { return isAPopup; }
	
	
	public void setIsPopup(boolean yes) { this.isAPopup = yes; }
	
	
	public ScenarioVisual showScenario(String scenarioId)
	{
		getViewFactory().createViewPanel(true, activity.getNodeId(), modelScenarioNodeId);
	}
	
	
	public VisualComponentFactory getVisualFactory() { return this; }
	
	
	public void saveFocusFieldChanges() {}
	
	
	public Class getVisualClass(NodeSer node) { return ActivityTreeData.class; }
	
	
	public void notifyScenarioNameChanged(ScenarioVisual scenVis, String newName)
	{
	}


	public VisualComponent makeTabularVisual(NodeSer node)
	throws
		ParameterError,
		Exception
	{
		return new ActivityTreeData(this, getPanelManager(), (ActivitySer)node);
	}


	public VisualComponent makeVisual(NodeSer node, Container container,
		boolean asIcon)
	throws
		ParameterError,
		Exception
	{
		return makeTabularVisual(node);
	}


	public Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError
	{	
		Set<VisualComponent> matchingVisuals = new HashSet<VisualComponent>();
		
		for (ActivityTreeData visual : visuals)
		{
			if (visual.getNodeId().equals(nodeId)) matchingVisuals.add(visual);
		}
		
		return matchingVisuals;
	}
	
	
	public VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError
	{
		Set<VisualComponent> visuals = identifyVisualComponents(nodeId);
		if (visuals == null) return null;
		if (visuals.size() == 0) return null;
		if (visuals.size() > 1) throw new RuntimeException(
			"Expected only one Visual with Id " + nodeId + " but there are " + visuals.size());
		for (VisualComponent visual : visuals) return visual;
	}
	
	
	public Set<VisualComponent> identifyVisualComponents(Class c)
	throws
		ParameterError
	{
		Set<VisualComponent> matchingVisuals = new HashSet<VisualComponent>();
		
		for (ActivityTreeData visual : visuals)
		{
			if (c.isAssignableFrom(visual.getClass())) matchingVisuals.add(visual);
		}
		
		return matchingVisuals;
	}


	public Set<VisualComponent> identifyVisualComponents()
	{
		Set<VisualComponent> matchingVisuals = new HashSet<VisualComponent>(visuals);
		
		return matchingVisuals;
	}
	
	
	public Set<VisualComponent> addNode(String nodeId)
	throws
		Exception
	{
		// Obtain the specified Node from the server.
		
		NodeSer node = modelEngine.getNode(nodeId);
		
		
		// Validate the Node returned by the server.
		
		if (! (node instanceof ActivitySer)) throw new Exception(
			"Server returned a " + node.getClass().getName() +
			", expected an ActivitySer.");
			
		ActivitySer activitySer = (ActivitySer)node;
			
			
		// Make sure this View does not already depict this Node.
		
		Set<VisualComponent> nodeVisuals = identifyVisualComponents(nodeId);
		if (nodeVisuals.size() > 0) return null;
		
		
		// Identify the Visuals that depict the parent of the Node. In this view
		// there should be one and only one, but I have coded it for the general
		// case that allows more than one, in case I extend the view later.
		
		String parentNodeId = activitySer.getParentNodeId();
		Set<VisualComponent> parentVisuals = identifyVisualComponents(parentNodeId);
		
		if (parentVisuals.size() == 0) throw new ParameterError(
			"The parent of Node " + nodeId + " does not have any Visuals in this View.");

		for (VisualComponent parentVisual : parentVisuals)
		{
			// Identify the Tree Node that references the parent Visual.
			// In the present implementation, this loop will only iterate once.
			
			DefaultMutableTreeTableNode parentTreeNode = 
				((ActivityTreeData)parentVisual).getTreeNode();
				
				
			// Create a new Visual for the Activity and attach it to a new Tree Node
			// under the parent Tree Node.
			
			this.curParent = addTreeNode(parentTreeNode, activitySer, this.modelScenarioNodeId);
			
			
			// Add the new Visual to the list of new Visuals that depict the Node.
		
			nodeVisuals.add((ActivityTreeData)(curParent.getUserObject()));
		}
		

		// Return the Visuals that were created: in this case, only one.
		
		return nodeVisuals;
	}
	
	
	public void notifyVisualComponentPopulated(VisualComponent visual, boolean populated)
	{
	}
	

	public void removeVisual(VisualComponent visual)
	{
		// Recursively remove each child.
		if (! (visual instanceof ActivityTreeData)) throw new RuntimeException(
			"Visual is not an ActivityTreeData: is a " + visual.getClass().getName());
		
		ActivityTreeData activityTreeVisual = (ActivityTreeData)visual;
		Set<ActivityTreeData> children = activityTreeVisual.getChildren();
		for (VisualComponent child : children) removeVisual(child);

		// Remove from GUI.
		if (! (visual instanceof ActivityTreeData)) throw new RuntimeException(
			"Visual is not an ActivityTreeData: is a " + visual.getClass().getName());
		
		TreeNode treeNode = ((ActivityTreeData)visual).getTreeNode();
		if (! (treeNode instanceof MutableTreeNode)) throw new RuntimeException(
			"Not a MutableTreeNode: is a " + treeNode.getClass().getName());
		
		((MutableTreeNode)treeNode).removeFromParent();
				
		// Unsubscribe.
		if (listenerRegistrar != null)
			listenerRegistrar.subscribe(visual.getNodeId(), false);
		
		// Remove from list of Visuals
		visuals.remove(visual);
	}
	
	
	public Set<VisualComponent> refreshVisuals(String nodeId)
	throws
		Exception
	{
		// Retrieve Node data from the server.
		NodeSer node = getModelEngine().getNode(nodeId);
		
		// Update each Visual that depicts the Node.
		
		return refreshVisuals(node);
	}


	public void refreshAttributeStateBinding(String attrId, String stateId)
	throws
		Exception
	{
	}
	
	
	public void select(VisualComponent visual, boolean yes)
	{
		if (yes) selectedVisuals.add(visual);
		else selectedVisuals.remove(visual);
	}


	public void selectNodesWithId(String nodeId, boolean yes)
	throws
		ParameterError
	{
		Set<VisualComponent> visualComponents = identifyVisualComponents(nodeId);
		
		for (VisualComponent visual : visualComponents)
		{
			select(visual, yes);
		}
	}
	

	public Set<VisualComponent> getSelected()
	{
		return new HashSet(selectedVisuals);
	}
	
	
	public boolean isSelected(VisualComponent visual)
	{
		return selectedVisuals.contains(visual);
	}
	
	
	public void selectAll(boolean yes)
	{
		if (yes) selectedVisuals = new HashSet(visuals);
		else selectedVisuals.clear();
	}
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Defined in TabularView.
	 */
	
	
	public FieldEditor getEditor(String fieldName, TreePath rowPath)
	{
		int column = -1;
		for (String fname : ActivityRiskMitModel.columnNames)
		{
			column++;
			if (fname.equals(fieldName)) break;
		}
		
		if (column < 0) throw new RuntimeException(
			"Column with name " + fieldName + " not found");
		
		return getEditor(column, rowPath);
	}
	
	
	public FieldEditor getEditor(int column, TreePath rowPath)
	{
		if (column < 0) throw new RuntimeException("Column is less than 0");
		return new FieldEditorImpl(rowPath, column, new JTextField());
	}
	
	
	public class FieldEditorImpl extends DefaultCellEditor implements FieldEditor
	{
		TabularVisualJ visual;
		int column;
		
		FieldEditorImpl(TreePath row, final int column, JTextField textField)
		{
			super(textField);
			this.visual = (TabularVisualJ)(row.getLastPathComponent());
			this.column = column;
			addCellEditorListener(new CellEditorListener()
			{
				public void editingCanceled(ChangeEvent e) {}
				
				public void editingStopped(ChangeEvent e)
				{
					try { visual.updateServer(column); }
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(TreeTablePanelViewBase.this, ex);
					}
				}
			});
		}

		public void clear() throws Warning, Exception
		{
			treeTableModel.setValueAt(null, visual, column);
		}
		
		public void show()
		{
			TreeTableNode[] nodes = treeTableModel.getPathToRoot(visual);
			TreePath path = new TreePath(nodes);
			int rowNo = treeTable.getRowForPath(path); 
			EventObject e = new EventObject(visual);
			treeTable.editCellAt(rowNo, column, e);
		}
	}
	
	
	public NodeSer getRootNodeSer() { return activity; }
	
	
	public JXTreeTable getTreeTable() { return treeTable; }
	
	
	public Color getBackgroundSelectionColor() { return BackgroundSelectionColor; }
	
	
	public Color getBackgroundNonSelectionColor() { return BackgroundColor; }
	
	
	public DefaultTreeTableModel getTreeTableModel() { return treeTableModel; }
	
	
	public void repaintVisual(VisualComponent visual)
	{
		TreePath rowPath = new TreePath(treeTableModel.getPathToRoot((TreeTableNode)visual));
		treeTableModel.valueForPathChanged(rowPath, visual);
	}
	
	
	public void setName(String name) { return activity.getName(); }


	
  /* ***************************************************************************
	 * *************************************************************************
	 * Defined in ViewPanelBase..
	 */
	
	
	public void domainCreated(String domainId) {}
	 
	 
	public void motifCreated(String motifId) {}
	 
	 
	public String getViewType() { return ViewTypeName; }
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Protected methods..
	 */
	 
	 
	/** ************************************************************************
	 * Obtain from the server the top-level Set of PersistentNodes that comprise
	 * the Activity, and add each one as a Visual Component.
	 */
	 
	protected void shallowPopulate()
	throws
		Exception
	{
		// Obtain and display the immediate children.
		
		String[] childNodeIds =
			modelEngine.getChildNodeIds(this.activity.getNodeId());
				
		for (String childNodeId : childNodeIds)
		{
			addNode(childNodeId);
			//Set<VisualComponent> visuals = addNode(childNodeId);
		}
		
		int numberOfRows = treeTable.getRowCount();
	}


	/** ************************************************************************
	 * Create a new Visual to depict the specified Activity; add that Visual under
	 * the specified parent Tree Node and return the Tree Node created.
	 */
	 
	protected DefaultMutableTreeTableNode addTreeNode(DefaultMutableTreeTableNode parentTreeNode, 
		ActivitySer activitySer, String modelScenarioNodeId)
	throws
		Exception
	{
		// Create a new Visual Component to represent the Activity.
		
		//ActivityTreeData visualTreeData = new ActivityTreeData(this, this, activitySer);
		ActivityTreeData visualTreeData = 
			(ActivityTreeData)makeTabularVisual(activitySer);
			
		
		// Set fields in the Visual.
		
		ModelAttributeSer[] transAttrs = 
			visualTreeData.getModelAttributes(
				expressway.common.Types.Activity.Transformation.class, modelScenarioNodeId);
		if (transAttrs.length > 1) throw new ParameterError(
			"Node " + activitySer.getName() + " has more than one Transformation Attribute.");
		if (transAttrs.length == 0) throw new ParameterError(
			"Node " + activitySer.getName() + " does not have a Transformation Attribute.");
		
		visualTreeData.setTransAttribute(transAttrs[0]);
		
		try
		{
			visualTreeData.refreshRedundantState();
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this,
				ThrowableUtil.getAllMessages(ex),
				activitySer.getName(),
				ex);
		}
		
		
		// Add the new Visual to this View's Set of Visuals.
		
		this.visuals.add(visualTreeData);
	

		// Create and add a new Tree Node, to insert into the Tree Table. The
		// new Tree Node contains (references) the new Visual's data.

		DefaultMutableTreeTableNode newTreeNode = new DefaultMutableTreeTableNode(visualTreeData);
		visualTreeData.setTreeNode(newTreeNode);
		if (parentTreeNode != null) parentTreeNode.add(newTreeNode);
				
		
		// Subscribe to receive update notifications for the node.
		// Note that this does NOT result in additional server connections.

		if (listenerRegistrar != null)
			listenerRegistrar.subscribe(activitySer.getNodeId(), true);
		
		
		// Here we should add any mouse listeners required to allow the user to
		// manipulate the Visual or initiate actions based on the context of
		// the Visual.
		// ...
		
		
		return newTreeNode;
	}
	
	
	/** ************************************************************************
	 * Refresh all of the Visuals in this View that depict the specified Node,
	 * based on the data contained in the argument.
	 */
	 
	protected Set<VisualComponent> refreshVisuals(NodeSer node)
	throws
		Exception
	{
		ActivitySer actSer = (ActivitySer)node;
		
		
		// Update each Visual that depicts the Node.
		
		Set<ActivityTreeData> visualsCopy = new HashSet<ActivityTreeData>(visuals);
		Set<VisualComponent> nodeVisuals = new HashSet<VisualComponent>();
		
		for (ActivityTreeData visual : visualsCopy)
		{
			nodeVisuals.add(visual);
			
			// Refresh the Visual internal state.
			
			visual.setActivitySer(actSer);
			visual.refreshRedundantState();
		}

		return nodeVisuals;
	}
	
	

  /* ***************************************************************************
	 * *************************************************************************
	 * Inner classes.
	 */
	 
	 
	/** ************************************************************************
	 * The Peer Listener for this View. (See design slide "PeerListener Pattern".)
	 * Listens for notifcation messages from a ModelEngine, and relays those messages
	 * to the visual counterparts of the server components identified in the messages.
	 */
	 
	class PeerListenerImpl extends UnicastRemoteObject implements PeerListener
	{
		private String id;
		
		
		protected PeerListenerImpl(String id) throws RemoteException
		{
			super();
			this.id = id;
		}
		
		
		public String getId() throws IOException { return id; }
		
		
		public void notify(PeerNotice notice)
		throws
			NotInterested,
			InconsistencyError,
			IOException
		{
			String nodeId = notice.getPeerId();
			
			Set<VisualComponent> visuals = null;
			try { visuals = identifyVisualComponents(nodeId); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
			
			if (visuals.size() == 0) throw new NotInterested();
			
			for (VisualComponent visual : visuals) notice.notify(visual);
		}
	}
}

