/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

import org.odmg.*;
import org.ozoneDB.*;
import nextapp.echo2.app.*;

public class Session extends nextapp.echo2.app.ApplicationInstance
{
    private Window window;

	public Window init()
	{
		setStyleSheet(Styles.DEFAULT_STYLE_SHEET);
		Window window = new Window();
		window.setTitle("Expressway");

		ContentPane contentPane = new ContentPane();
		window.setContent(contentPane);


		/*
		 * Instantiate a row of radio buttons, to simlate a slider.
		 * When one of these buttons is clicked, a business parameter change
		 * event is sent to the model, causing the model to update affected
		 * elements and decisions and re-display.
		 */

		Row row = new Row();

		RowLayoutData rowLayoutData = new RowLayoutData();
		rowLayoutData.setWidth(new Extent(4, Extent.IN));	// Set width to 4".
		row.setLayoutData(rowLayoutData);

		ButtonGroup buttonGroup = new ButtonGroup();

		RadioButton b1 = new RadioButton();
		b1.setGroup(buttonGroup);
		row.add(b1);

		RadioButton b2 = new RadioButton();
		b2.setGroup(buttonGroup);
		row.add(b2);

		RadioButton b3 = new RadioButton();
		b3.setGroup(buttonGroup);
		row.add(b3);

		RadioButton b4 = new RadioButton();
		b4.setGroup(buttonGroup);
		row.add(b4);

		RadioButton b5 = new RadioButton();
		b5.setGroup(buttonGroup);
		row.add(b5);

		RadioButton b6 = new RadioButton();
		b6.setGroup(buttonGroup);
		row.add(b6);


		contentPane.add(row);


		/*
		 * Link the button row to its event handler.
		 */

		....

		return window;
	}

	/**
	 * For handling changes to a value of the slider.
	 * When the slider's value changes, notify the model.
	 */

	protected void handleSliderChange()
	{
		....
	}

	/**
	 * For handling changes to the model.
	 * When the model changes, it notifies this method so that the
	 * model display can be re-rendered.
	 */

	protected void handleModelChanges()
	{
		/*
		 * Un-instantiate
		 */
	}
}
