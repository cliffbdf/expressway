/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway;

import expressway.ModelElement.*;
import expressway.ModelAPITypes.*;
import java.io.File;
import java.io.IOException;

/**
 * Implements the ModelEngine interface using a seraized Java object file.
 */

public class ModelEngineSerImpl implements ModelEngine
{
	private String dbDir = null;
	private File dbFile = null;
	private static String DefaultDBFileName = "DB.pojo.ser";

	/**
	 * Construct the database object and open the database, using
	 * the default database path.
	 */

	public ModelEngineOzoneImpl() { this(dbDir); }

	/**
	 * Construct the database object and open the database, using
	 * the supplied database path.
	 */

	public ModelEngineSerImpl(String dbDir)
	{
		this.dbDir = dbDir;

		if (dbDir == null) return;

		dbFile = new File(dbDir, DefaultDBFileName);
		try
		{
			if (! dbFile.createNewFile())
			{
				// it existed: read it in.
		}
		catch (IOException ex)
		{
			throw new RuntimeException(ex);
		}


		....
	}

	/**
	 * Locate the specified model in the database, and update the
	 * specified attribute with the specified new value.
	 */

	public void updateAttribute(String elementName, String attributeName,
		Object value)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		....
	}

	/**
	 * Locate the specified model in the database, and return a serializable
	 * tree (a copy) of its elements.
	 *
	 * Starts and completes transaction.
	 */

	public ModelElement getModelElementTree(String elementName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		....
	}
}
