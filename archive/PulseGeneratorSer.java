package expressway.ser;

import expressway.common.*;
import expressway.common.ModelElement.*;
import expressway.common.DecisionElement.*;

public class PulseGeneratorSer extends GeneratorSer
//public class PulseGeneratorSer extends ActivitySer
{
	public String inputPortNodeId;
	public String outputPortNodeId;
	public String countPortNodeId;
	public String embeddedGeneratorNodeId;
	public String compensatorNodeId;
	
	
	public PulseGeneratorSer(PulseGenerator d)
	{
		super(d);
		
		this.inputPortNodeId = d.getInputPortNodeId();
		this.outputPortNodeId = d.getOutputPortNodeId();
		this.countPortNodeId = d.getCountPortNodeId();
		this.embeddedGeneratorNodeId = d.getEmbeddedGeneratorNodeId();
		this.compensatorNodeId = d.getCompensatorNodeId();
	}                             
}

