/**
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

import org.odmg.*;
import org.ozoneDB.*;

public class Main
{
	static String dbDir = "/usr/local/test/ozoneDB";
	static LocalDatabase db = new LocalDatabase(dbDir);

	public static void main(String[] args)
	{
		/**
		 * Open database.
		 */

		if (!db.exists(dbDir))
		{
			System.out.println("Creating database in " + dbDir);
			db.create(dbDir);
			System.out.println("Database created");
		}

		private static ExternalDatabase	db = null;
		try
		{
			System.out.println("Opening database in " + dbDir);
			db = ExternalDatabase.openDatabase("ozonedb:local://" + dbDir);
		}
		catch (Throwable t)
		{
			System.out.println("Unable to open database in " + dbDir);
			t.printStackTrace();
			return;
		}

		System.out.println("Database opened...");

		db.reloadClasses();


		/**
		 * Start RPC-JSON service and link it to the GetElementTree method.
		 */

		System.out.println("Starting RPC-JSON service...");
		....


		/**
		 * Console.
		 */

		System.out.println("System running; type 'stop' to terminate");

		for (;;)
		{
			// Read input.

			String input = System.in.readln();
			if (input.equals("stop"))
			{
				break;
			}
		}

		....stop RPC-JSON service.
		System.out.println("RPC-JSON service stopped...");

		db.close();
		System.out.println("Database stopped...");
	}

	/**
	 * Locate the specified model in the database, and return a serializable
	 * tree (a copy) of its elements.
	 *
	 * Starts and completes transaction.
	 */
	public ModelElement GetElementTree(modelName: string)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		InternalEngineFailure
	{
		ModelElement element = null;

		/**
		 * Start transaction.
		 */

		org.odmg.Implementation dbImpl = new org.ozoneDB.odmg.ODMG();
		Transaction transaction = dbImpl.newTransaction();


		/**
		 * Wrap transaction in try block to catch any exception so that
		 * rollback can be performed.
		 */
		try
		{
			/**
			 * Locate the element in the database.
			 */

			try
			{
				element = (ModelElement)(db.lookup(modelName));
				if (element == null) throw new ElementNotFound();
			}
			catch (CannotObtainLock ex1)
			{
				throw ex1;
			}
			catch (ElementNotFound ex2)
			{
				throw ex2;
			}

			/**
			 * Recursively traverse the element's sub-elements, and assemble a
			 * serializable copy of the element tree.
			 */

			element = element.getModelElementTree();
		}
		catch (Exception ex)
		{
			transsaction.abort();

			throw ex;		// re-throw.
		}
		catch (Throwable t)		// Top-level exception firewall.
		{
			transsaction.abort();

			InternalEngineFailure ex = new InternalEngineFailure(t);
			er.printStackTrace();
			throw ex;
		}

		transaction.commit();

		return element;
	}
}
