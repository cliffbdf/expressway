/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.server.ClassificationElement.*;
import expressway.ser.ClassificationDomainSer;
import expressway.server.NamedReference.*;
import java.net.URL;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Set;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


public class ClassificationDomainImpl extends HierarchyDomainBase
	implements ClassificationDomain
{
  /* Constructors */
  
  
	protected ClassificationDomainImpl(String baseName)
	throws
		ParameterError
	{
		super(baseName);

		// ClassificationDomain: <Classification> (ImplementsClassification) <>
		createNamedReference(
			StandardNamedReferences.ImplementsClassification, 
			StandardNamedReferences.ImplementsClassificationDesc,
			CrossReferenceable.class, Classification.class);
	}
	
	
  /* From HierarchyElementBase. Extend as needed. */


	protected String getTextToRender()
	{
		return super.getTextToRender();
	}
	
	
	protected String getTagName() { return "classification"; }
	
	
	protected void writeSpecializedXMLAttributes(PrintWriter writer)
	{
	}
	
	
	protected void writeSpecializedXMLElements(PrintWriter writer, int indentation) {} // ok
	
	
  /* From HierarchyBase */


	protected Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		return new ClassificationImpl(name, this);
	}

	
  /* From ClassificationElement */
	
	
	public ClassificationDomain getClassificationDomain() { return this; }	
	
	
	public ClassificationAttribute createClassificationAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (ClassificationAttribute)(createAttribute(name, defaultValue, layoutBound,
			outermostAffectedRef));
	}
	
	
	public ClassificationAttribute createClassificationAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (ClassificationAttribute)(createAttribute(layoutBound,
			outermostAffectedRef));
	}


	public ClassificationAttribute createClassificationAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (ClassificationAttribute)(createAttribute(name, layoutBound,
			outermostAffectedRef));
	}
	

  /* From Classification */
  
  
	public Classification createSpecialization(String name)
	{
		try { return (Classification)(createSubHierarchy(name)); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public Classification getSpecialization(String name)
	throws
		ElementNotFound
	{
		return (Classification)(getSubHierarchy(name));
	}
	
	
	public Classification getNestedSpecialization(String fullName)
	throws
		ElementNotFound
	{
		String[] parts = fullName.split("\\.");
		Classification h = this;
		for (String name : parts)
		{
			h = (Classification)(h.getSubHierarchy(name));
		}
		
		return h;
	}
	
	
	public SortedSet<Classification> getSpecializations()
	{
		List<Hierarchy> subs = getSubHierarchies();
		SortedSet<Classification> classifications = new TreeSet<Classification>();
		for (Hierarchy h : subs) classifications.add((Classification)h);
		return classifications;
	}
	
	
	public Classification getSuperiorClassification()
	{
		return (Classification)(getParentHierarchy());
	}


  /* From PersistentNodeImpl */
	
	
	protected Class getSerClass() { return ClassificationDomainSer.class; }
	
	
	protected NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		synchronized (this.getDomain())
		{
			ClassificationDomainSer ser = (ClassificationDomainSer)nodeSer;

			return super.externalize(ser);
		}
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		ClassificationDomainImpl newInstance =
			(ClassificationDomainImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}

	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	protected void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ClassificationIconImageName);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		 { return new TreeSet<PersistentNode>(); }
	}
	
	
	public double getPreferredWidthInPixels() { return 50; }
	

	public double getPreferredHeightInPixels() { return 0; }
		
		
	public Attribute constructAttribute(String name, Serializable defaultValue)
	{
		return new ClassificationAttributeImpl(name, this, defaultValue);
	}
	
	
	public Attribute constructAttribute(String name)
	{
		return new ClassificationAttributeImpl(name, this);
	}
	
	
  /* From Domain */
  
	
	public String getDefaultViewTypeName() { return "expressway.gui.classification.Classification"; }
}

