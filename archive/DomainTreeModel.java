/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.domainlist;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.Constants;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.*;
import expressway.gui.activityrisk.*;
import expressway.gui.modeldomain.*;
import expressway.help.*;
import expressway.generalpurpose.*;
import java.awt.event.*;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.Vector;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Date;
import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;


/** ****************************************************************************
 * 
 */
 
public class DomainTreeModel
	//extends DefaultTreeSelectionModel
	implements TreeModel
{
	private static final boolean Debug = false;
	
	public static final Color normalColor = new Color(200, 200, 255);
	public static final Color highlightedColor = new Color(255, 200, 255);

	private DomainListPanel viewPanel;
	private DomainListJTree jTree;
	private Vector<TreeModelListener> treeModelListeners =
		new Vector<TreeModelListener>();
		
	private String hostName;
	private Container container;
	private ModelEngineRMI modelEngine;
	

	DomainTreeModel(DomainListPanel viewPanel, DomainListJTree jTree, String hostName, 
		Container container, ModelEngineRMI modelEngine)
	{
		this.viewPanel = viewPanel;
		this.jTree = jTree;
		this.hostName = hostName;
		this.container = container;
		this.modelEngine = modelEngine;
	}
	
	
	public DomainListJTree getJTree() { return this.jTree; }
	
	
	public synchronized Object getRoot()
	{
		return this;
	}
	
	
	public synchronized int getIndexOfChild(Object parent, Object child)
	{
		int seqNo = -1;
		TreeVisual childVisual = null;
				
		try
		{
		if (! (child instanceof TreeVisual)) return seqNo = -1;
		childVisual = (TreeVisual)child;
		
		if (parent == this)
		{
			/*
			if (child instanceof MotifTreeVisual)
			{
				List<ModelDomainMotifDefSer> motifDefSers = null;
				try { motifDefSers = modelEngine.getModelDomainMotifs(); }
				catch (Exception ex)
				{
					if (ex instanceof java.rmi.ConnectException) modelEngine = null;
					
					ErrorDialog.showReportableDialog(this.container, ex);
					return seqNo = -1;
				}
				
				return seqNo = motifDefSers.indexOf(child);
			}*/
			
			if (child instanceof ModelDomainTreeVisual)
			{
				List<ModelDomainSer> mds = null;
				try { mds = modelEngine.getModelDomains(); }
				catch (Exception ex)
				{
					if (ex instanceof java.rmi.ConnectException) modelEngine = null;
					
					ErrorDialog.showReportableDialog(this.container, ex);
					return seqNo = -1;
				}
	
				return seqNo = mds.indexOf(child);
			}
			
			return seqNo = -1;
		}
		else if (parent instanceof MotifTreeVisual)
		{
			return -1;
		}
		else if (parent instanceof ModelDomainTreeVisual)
		{
			// Only ModelScenarios may appear directly under a ModelDomain.
			//
			// Display a ModelScenario if: It is not derived from another Scenario.

			ModelDomainSer modelDomainSer = 
				(ModelDomainSer)(((ModelDomainTreeVisual)parent).getNodeSer());
				
			if (! (child instanceof ModelScenarioTreeVisual)) return seqNo = -1;
			
			try
			{
				ScenarioSer[] scenarioSers = modelEngine.getScenarios(modelDomainSer.getName());
				
				// Do not count derived Scenarios.
				seqNo = -1;
				for (int i = 0; i < scenarioSers.length; i++)
				{
					ModelScenarioSer scenarioSer = (ModelScenarioSer)(scenarioSers[i]);
					if (scenarioSer.scenarioThatWasCopiedNodeId != null)
						continue;
					
					seqNo++;
					
					if (scenarioSer.equals(child)) return seqNo;
				}
				
				return seqNo = -1;
				
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				return seqNo = -1;
			}
		}
		else if (parent instanceof ModelScenarioTreeVisual)
		{
			// ModelScenarios, ModelScenarioSets, SimulationRuns, and SimRunSets
			// may appear directly under a ModelScenario.
			//
			// Display a Scenario if: it is directly derived from visual and does
			//	not belong to a ScenarioSet.
			// Display a SimulationRun if: its Scenario is 'visual' and it does
			//	not belong to a SimRunSet.
			// Display a ScenarioSet if: it is directly derived from visual.
			// Display a SimRunSet if: its Scenario is 'visual'.
			
			ModelScenarioTreeVisual visual = (ModelScenarioTreeVisual)parent;
			
			ModelScenarioSer modelScenarioSer = (ModelScenarioSer)(visual.getNodeSer());
				
			String domainId = ((ModelScenarioSer)(visual.getNodeSer())).domainId;
				
			if (! ((child instanceof SimulationRunSer) ||
					(child instanceof ModelScenarioSer))) return seqNo = -1;
			
			try
			{
				// Simulation Runs.
				
				List<SimulationRunSer> simRunSers = 
					modelEngine.getSimulationRuns(modelScenarioSer.getNodeId());
				
				for (SimulationRunSer simRunSer : simRunSers)
				{
					seqNo++;
					if (simRunSer.getNodeId().equals(childVisual.getNodeId()))
						return seqNo;
				}
				
				
				// SimRunSets.
				
				SimRunSetSer[] simRunSetSers = 
					modelEngine.getSimRunSetsForScenarioId(visual.getNodeId());
					
				for (SimRunSetSer simRunSetSer : simRunSetSers)
				{
					seqNo++;
					if (simRunSetSer.getNodeId().equals(childVisual.getNodeId()))
						return seqNo;
				}

				
				// ModelScenarios.
				
				ScenarioSer[] scenarioSers = modelEngine.getScenariosForDomainId(domainId);
				
				seqNo = -1;
				for (int i = 0; i < scenarioSers.length; i++)
				{
					ModelScenarioSer scenarioSer = (ModelScenarioSer)(scenarioSers[i]);
					
					// Do not count Scenarios that are not derived from this one.
					if (scenarioSer.scenarioThatWasCopiedNodeId == null) continue;
					if (! scenarioSer.scenarioThatWasCopiedNodeId.equals(modelScenarioSer.getNodeId()))
						continue;
					
					seqNo++;
					
					if (scenarioSer.equals(child)) return seqNo;
				}
				

				// ScenarioSets.
				
				ModelScenarioSetSer[] scenarioSetSers = 
					modelEngine.getScenarioSetsForDomainId(domainId);
					
				for (ModelScenarioSetSer scenSetSer : scenarioSetSers)
				{
					seqNo++;
					if (scenSetSer.getNodeId().equals(childVisual.getNodeId()))
						return seqNo;
				}
				
				return seqNo = -1;
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				return seqNo = -1;
			}
		}
		else if (parent instanceof SimulationRunTreeVisual)
		{
			return seqNo = -1;
		}
		else if (parent instanceof ModelScenarioSetTreeVisual)
		{
			ModelScenarioSetTreeVisual visual = (ModelScenarioSetTreeVisual)parent;
			try
			{
				ModelScenarioSer[] scenarioSers = 
					modelEngine.getScenariosForScenarioSetId(visual.getNodeId());
				
				seqNo = -1;
				for (ModelScenarioSer scenSer : scenarioSers)
				{
					seqNo++;
					if (scenSer.getNodeId().equals(childVisual.getNodeId()))
						return seqNo;
				}
				
				return seqNo = -1;
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				return seqNo = -1;
			}
		}
		else if (parent instanceof SimRunSetTreeVisual)
		{
			SimRunSetTreeVisual visual = (SimRunSetTreeVisual)parent;
			try
			{
				SimulationRunSer[] simRunSers = 
					modelEngine.getSimulationRunsForSimRunSetId(visual.getNodeId());
					
				seqNo = -1;
				for (SimulationRunSer simRunSer : simRunSers)
				{
					seqNo++;
					if (simRunSer.getNodeId().equals(childVisual.getNodeId()))
						return seqNo;
				}
				
				return seqNo = -1;
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				return seqNo = -1;
			}
		}
		
		return seqNo = -1;
		}
		finally { if (Debug) {
			System.out.println("getIndexOfChild(" + getObjectName(parent) +
				", " + getObjectName(childVisual) + " = " + seqNo);
		}}
	}
	
	
	public synchronized Object getChild(Object parent, int index)
	{
		VisualComponent childVisual = null;
		int seqNo = -1;
		
		try
		{
		if (modelEngine == null) throw new RuntimeException("Model Engine is null");
		
		if (parent == this)
		{
			/*
			List<ModelDomainMotifDefSer> motifDefSers = null;
			try
			{
				motifDefSers = modelEngine.getModelDomainMotifs();
				
				if (index < motifDefSers.size())
				{
					ModelDomainMotifDefSer motifDefSer = motifDefSers.get(index);
					
					if (viewPanel.getListenerRegistrar() != null)
						viewPanel.getListenerRegistrar().subscribe(
							motifDefSer.getNodeId(), true);
					
					return viewPanel.makeVisual(motifDefSer, null, true);
				}
				else
					seqNo = index-motifDefSers.size();
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				throw new RuntimeException(ex);
			}
			
			if (seqNo < 0) return null;
			*/
				
			List<ModelDomainSer> mds = null;
			try
			{
				mds = modelEngine.getModelDomains();
				
				if (index+1 > mds.size()) return null;
				
				ModelDomainSer modelDomainSer = mds.get(index);
	
				if (viewPanel.getListenerRegistrar() != null)
					viewPanel.getListenerRegistrar().subscribe(
						modelDomainSer.getNodeId(), true);
					
	
				return viewPanel.makeVisual(modelDomainSer, null, true);
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				throw new RuntimeException(ex);
			}
		}
		else if (parent instanceof ModelDomainTreeVisual)
		{
			ModelDomainTreeVisual visual = (ModelDomainTreeVisual)parent;
				
			// Only ModelScenarios may appear directly under a ModelDomain.
			//
			// Display a ModelScenario if: It is not derived from another Scenario.
			
					
			ModelDomainSer modelDomainSer = (ModelDomainSer)(visual.getNodeSer());
				
			ScenarioSer[] scenarioSers = null;
			childVisual = null;
			try
			{
				scenarioSers = modelEngine.getScenarios(modelDomainSer.getName());
				
				if (index >= scenarioSers.length)
				{
					(new Exception("Returning null")).printStackTrace();
					throw new RuntimeException();
				}
				
				seqNo = 0;
				ModelScenarioSer scenarioSer = null;
				for (int i = 0; i < scenarioSers.length; i++)
				{
					scenarioSer = (ModelScenarioSer)(scenarioSers[i]);
					
					
					// Skip Scenarios that are derived from another Scenario.
					if (scenarioSer.scenarioThatWasCopiedNodeId != null)
					{
						scenarioSer = null;
						continue;
					}
					
					// Skip Scenarios that belong to a ModelScenarioSet.
					if (scenarioSer.modelScenarioSetNodeId != null)
					{
						scenarioSer = null;
						continue;
					}
					
					if (seqNo == index) break;

					scenarioSer = null;
					seqNo++;
				}
				
				if (scenarioSer == null)
				{
					(new Exception("Returning null")).printStackTrace();
					throw new RuntimeException();
				}
	
				if (viewPanel.getListenerRegistrar() != null)
					viewPanel.getListenerRegistrar().subscribe(
						scenarioSer.getNodeId(), true);
				
				return childVisual = viewPanel.makeVisual(scenarioSer, null, true);
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				throw new RuntimeException(ex);
			}
			
			//return new ModelScenarioTreeVisual(scenarioSer);
		}
		else if (parent instanceof ModelScenarioTreeVisual)
		{
			ModelScenarioTreeVisual visual = (ModelScenarioTreeVisual)parent;
			
			// ModelScenarios, ModelScenarioSets, SimulationRuns, and SimRunSets
			// may appear directly under a ModelScenario.
			//
			// Display a Scenario if: it is directly derived from visual and does
			//	not belong to a ScenarioSet.
			// Display a SimulationRun if: its Scenario is 'visual' and it does
			//	not belong to a SimRunSet.
			// Display a ScenarioSet if: it is directly derived from visual.
			// Display a SimRunSet if: its Scenario is 'visual'.
					
			
			ModelScenarioSer modelScenarioSer = (ModelScenarioSer)(visual.getNodeSer());
			
			String domainId = ((ModelScenarioSer)(visual.getNodeSer())).domainId;
				
			ScenarioSer[] scenarioSers = null;
			//List<SimulationRunSer> simRuns = null;
			try
			{
				// Perhaps the child is a Simulation Run.
				
				List<SimulationRunSer> simRunSers = 
					modelEngine.getSimulationRuns(modelScenarioSer.getNodeId());
				
				SimulationRunSer simRunSer = null;
				for (SimulationRunSer s : simRunSers)
				{
					simRunSer = s;
					if (simRunSer.simRunSetNodeId != null) continue;
					
					seqNo++;					
					if (seqNo == index) break;
				}
				
				if (seqNo == index)
				{
					if (viewPanel.getListenerRegistrar() != null)
						viewPanel.getListenerRegistrar().subscribe(
							simRunSer.getNodeId(), true);
					
					return childVisual = viewPanel.makeVisual(simRunSer, null, true);
				}
				
				
				// Perhaps the child is a SimRunSet. Is its Scenario 'visual'?
				
				SimRunSetSer[] simRunSetSers = 
					modelEngine.getSimRunSetsForScenarioId(visual.getNodeId());
					
				for (SimRunSetSer simRunSetSer : simRunSetSers)
				{
					if (simRunSetSer.scenarioId.equals(visual.getNodeId()))
					{
						seqNo++;
						if (seqNo == index)
						{
							if (viewPanel.getListenerRegistrar() != null)
								viewPanel.getListenerRegistrar().subscribe(
									simRunSetSer.getNodeId(), true);
							
							return childVisual = viewPanel.makeVisual(simRunSetSer, null, true);
						}
					}
				}
				
				
				// Perhaps a ModelScenario.
				
				scenarioSers = modelEngine.getScenariosForDomainId(domainId);
				
				//seqNo = -1;
				ModelScenarioSer scenarioSer = null;
				for (int i = 0; i < scenarioSers.length; i++)
				{
					// Determine if the index'th child is derived from this Scenario.
					
					scenarioSer = (ModelScenarioSer)(scenarioSers[i]);
					if (scenarioSer.scenarioThatWasCopiedNodeId == null)
					{
						scenarioSer = null;
						continue;
					}
					
					if (! scenarioSer.scenarioThatWasCopiedNodeId.equals(modelScenarioSer.getNodeId()))
					{
						scenarioSer = null;
						continue;  // not derived from this Scenario.
					}
					
					if (scenarioSer.modelScenarioSetNodeId != null)
					{
						scenarioSer = null;
						continue;
					}
					
					
					// Derived from this Scenario and does not belong to a ScenarioSet.
					
					seqNo++;
					if (seqNo == index) break;
					
					scenarioSer = null;
				}
				
				if (scenarioSer != null)  // child is a Model Scenario.
				{
					if (viewPanel.getListenerRegistrar() != null)
						viewPanel.getListenerRegistrar().subscribe(
							scenarioSer.getNodeId(), true);
					
					return childVisual = viewPanel.makeVisual(scenarioSer, null, true);
				}
				
				
				// Perhaps the child is a ScenarioSet. Is it directly derived from visual?
				
				ModelScenarioSetSer[] scenarioSetSers = 
					modelEngine.getScenarioSetsForDomainId(domainId);
					
				for (ModelScenarioSetSer scenSetSer : scenarioSetSers)
					// each ScenarioSet of the Domain
				{
					if (scenSetSer.scenarioThatWasCopiedNodeId.equals(visual.getNodeId()))
					{
						seqNo++;
						if (seqNo == index)
						{
							if (viewPanel.getListenerRegistrar() != null)
								viewPanel.getListenerRegistrar().subscribe(
									scenSetSer.getNodeId(), true);
							
							return childVisual = viewPanel.makeVisual(scenSetSer, null, true);
						}
					}
				}
				
				(new Exception("Returning null")).printStackTrace();
				throw new RuntimeException();
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				throw new RuntimeException(ex);
			}
			
			//return new SimulationRunTreeVisual(simRunSer);
		}
		else if (parent instanceof SimulationRunTreeVisual)
		{
			(new Exception("Returning null")).printStackTrace();
			throw new RuntimeException();
		}
		else if (parent instanceof ModelScenarioSetTreeVisual)
		{
			ModelScenarioSetTreeVisual visual = (ModelScenarioSetTreeVisual)parent;
			
			try
			{
				// ModelScenarios may appear directly under a ModelScenarioSet.
				//
				// Display a Scenario if: it is directly belongs to 'visual'.
				
				seqNo = -1;
				ModelScenarioSer[] scenarioSers = 
					modelEngine.getScenariosForScenarioSetId(visual.getNodeId());
				for (ModelScenarioSer scenarioSer : scenarioSers)
				{
					seqNo++;
					if (seqNo == index) return childVisual = viewPanel.makeVisual(
						scenarioSer, null, true);
				}
				
				return childVisual = null;
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				return childVisual = null;
			}
		}
		else if (parent instanceof SimRunSetTreeVisual)
		{
			SimRunSetTreeVisual visual = (SimRunSetTreeVisual)parent;
			
			// SimulationRuns may appear directly under a SimRunSet.
			//
			// Display a SimulationRun if: it directly belongs to a SimRunSet.
			
			try
			{
				SimulationRunSer[] simRunSers = 
					modelEngine.getSimulationRunsForSimRunSetId(visual.getNodeId());
				
				seqNo = -1;
				for (SimulationRunSer simRunSer: simRunSers)
				{
					seqNo++;
					if (seqNo == index) return childVisual = viewPanel.makeVisual(
						simRunSer, null, true);
				}
				
				return childVisual = null;
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				return childVisual = null;
			}
		}
		else GlobalConsole.println("Unexpected parent type: " + parent.getClass().getName());
		
		(new Exception("Returning null")).printStackTrace();
		
		return childVisual = null;
		}
		finally { if (Debug) {
			System.out.println("For " + getObjectName(parent) +
				": child[" + index + "] is " + getObjectName(childVisual));
		}}
	}
	
	
	public synchronized int getChildCount(Object parent)
	{
		int count = 0;
		
		try
		{
		if (modelEngine == null) return count = 0;
		
		if (parent == this)
		{
			List<ModelDomainSer> mds = null;
			
			//List<ModelDomainMotifDefSer> motifDefSers = null;
			try
			{
				mds = modelEngine.getModelDomains();
				//motifDefSers = modelEngine.getModelDomainMotifs();
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				return count = 0;
			}
			
			return count = mds.size();
				//+ motifDefSers.size();
		}
		else if (parent instanceof ModelDomainTreeVisual)
		{
			// Only ModelScenarios may appear directly under a ModelDomain.
			//
			// Display a ModelScenario if: It is not derived from another Scenario.

			ScenarioSer[] scenarioSers = null;
			try
			{
				scenarioSers = modelEngine.getScenarios(
					((ModelDomainTreeVisual)parent).getNodeSer().getName());
				
				// Do not count derived Scenarios.
				for (int i = 0; i < scenarioSers.length; i++)
				{
					ModelScenarioSer scenarioSer = (ModelScenarioSer)(scenarioSers[i]);
					if (scenarioSer.scenarioThatWasCopiedNodeId != null)
						continue;
					
					count++;
				}
				
				return count;
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				return count = 0;
			}
		}
		else if (parent instanceof ModelScenarioTreeVisual)
		{
			// ModelScenarios, ModelScenarioSets, SimulationRuns, and SimRunSets
			// may appear directly under a ModelScenario.
			//
			// Count a Scenario if: it is directly derived from visual and does
			//	not belong to a ScenarioSet.
			// Count a SimulationRun if: its Scenario is 'visual' and it does
			//	not belong to a SimRunSet.
			// Count a ScenarioSet if: it is directly derived from visual.
			// Count a SimRunSet if: its Scenario is 'visual'.
			
			ModelScenarioTreeVisual visual = (ModelScenarioTreeVisual)parent;
			
			ModelScenarioSer modelScenarioSer = (ModelScenarioSer)(visual.getNodeSer());
			
			String domainId = ((ModelScenarioSer)(visual.getNodeSer())).domainId;
				
			ScenarioSer[] scenarioSers;
			count = 0;
			List<SimulationRunSer> simRuns = null;
			try
			{
				scenarioSers = modelEngine.getScenariosForDomainId(domainId);
				
				for (int i = 0; i < scenarioSers.length; i++)
				{
					// Do not count Scenarios that are not derived from this one.
					ModelScenarioSer scenarioSer = (ModelScenarioSer)(scenarioSers[i]);
					if (scenarioSer.scenarioThatWasCopiedNodeId == null) continue;
					if (! scenarioSer.scenarioThatWasCopiedNodeId.equals(modelScenarioSer.getNodeId()))
						continue;
					
					if (scenarioSer.modelScenarioSetNodeId != null) continue;
					
					count++;
				}
				
				List<SimulationRunSer> simRunSers = 
					modelEngine.getSimulationRuns(modelScenarioSer.getNodeId());
				
				for (SimulationRunSer simRunSer : simRunSers)
				{
					if (simRunSer.simRunSetNodeId != null) continue;
					
					count++;
				}
				
				// Count ScenarioSets that belong immediatley to 'visual'
				ModelScenarioSetSer[] scenSetSers = 
					modelEngine.getScenarioSetsForDomainId(domainId);
				for (ModelScenarioSetSer scenSetSer : scenSetSers)
				{
					if (scenSetSer.scenarioThatWasCopiedNodeId.equals(
						visual.getNodeId())) count++;
				}
				
				// Count SimRunSets that belong immediately to 'visual'
				SimRunSetSer[] simRunSetSers = 
					modelEngine.getSimRunSetsForScenarioId(visual.getNodeId());
				for (SimRunSetSer simRunSetSer : simRunSetSers)
				{
					if (simRunSetSer.parentNodeId.equals(visual.getNodeId())) count++;
				}
				
				return count;
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				return count = 0;
			}
		}
		else if (parent instanceof SimulationRunTreeVisual)
		{
			return count = 0;
		}
		else if (parent instanceof ModelScenarioSetTreeVisual)
		{
			ModelScenarioSetTreeVisual visual = (ModelScenarioSetTreeVisual)parent;
			
			count = 0;
			try
			{
				// ModelScenarios may appear directly under a ModelScenarioSet.
				//
				// Count a Scenario if: it is directly belongs to 'visual'.
				
				ModelScenarioSer[] scenarioSers = 
					modelEngine.getScenariosForScenarioSetId(visual.getNodeId());
				
				count += scenarioSers.length;
				return count;
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				return count = 0;
			}
		}
		else if (parent instanceof SimRunSetTreeVisual)
		{
			SimRunSetTreeVisual visual = (SimRunSetTreeVisual)parent;
			
			count = 0;
			try
			{
				// SimulationRuns may appear directly under a SimRunSet.
				//
				// Count a SimulationRun if: it directly belongs to a SimRunSet.
				
				SimulationRunSer[] simRunSers = 
					modelEngine.getSimulationRunsForSimRunSetId(visual.getNodeId());
					
				count += simRunSers.length;
				return count;
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
				
				ErrorDialog.showReportableDialog(this.container, ex);
				return count = 0;
			}
		}
		
		return count = 0;
		}
		finally { if (Debug) {
			System.out.println("getChildCount(" + 
				getObjectName(parent) + ") returning " + count);
		}}
	}
	
	
	public synchronized boolean isLeaf(Object node)
	{
		boolean result = false;
		
		try
		{
		if (modelEngine == null) return result = true;
		
		if (node == this)
		{
			List<ModelDomainSer> mds = null;
			//List<ModelDomainMotifDefSer> motifDefSers = null;
			
			try
			{
				mds = modelEngine.getModelDomains();
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
					// prevents blizzard of messages if the connection fails.
				
				// Prevent endless loop, in case showMessageDialog tries to
				// re-render the Panel and calls this method (again).
				if (ThreadStackUtils.stackContainsPriorCallTo(
					this.getClass().getName() + ".isLeaf")) return result = true;
				
				ErrorDialog.showReportableDialog(this.container, ex);
					
				return result = true;  // prevents endless loop.
			}

			return result = (mds.size() == 0);
			//return result = ((mds.size() == 0) && (motifDefSers.size() == 0));
		}
		else if (node instanceof MotifTreeVisual)
		{
			return true;
		}
		else if (node instanceof ModelDomainTreeVisual)
		{
			ModelDomainSer modelDomainSer = 
				(ModelDomainSer)(((ModelDomainTreeVisual)node).getNodeSer());
				
			ScenarioSer[] scenarios = null;
			
			try { scenarios = modelEngine.getScenarios(modelDomainSer.getName()); }
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
					// prevents blizzard of messages if the connection fails.
				
				// Prevent endless loop, in case showMessageDialog tries to
				// re-render the Panel and calls this method (again).
				
				if (ThreadStackUtils.stackContainsPriorCallTo(
					this.getClass().getName() + ".isLeaf"))
				{
					//System.out.println("it does");
					return result = true;
				}
				
				ErrorDialog.showReportableDialog(this.container, ex);
					
				return result = true;  // prevents endless loop.
			}
			
			return result = (scenarios.length == 0);  // was commented out
		}
		else if (node instanceof ModelScenarioTreeVisual)
		{
			ModelScenarioTreeVisual visual = (ModelScenarioTreeVisual)node;
			
			ModelScenarioSer modelScenarioSer = (ModelScenarioSer)(visual.getNodeSer());
			
			String domainId = ((ModelScenarioSer)(visual.getNodeSer())).domainId;
			
			result = true;
			try
			{
				// If there is at least one Scenario that is derived from this one,
				// return false.
				
				ScenarioSer[] scenarioSers = modelEngine.getScenariosForDomainId(domainId);
				
				for (int i = 0; i < scenarioSers.length; i++)
				{
					ModelScenarioSer scenarioSer = (ModelScenarioSer)(scenarioSers[i]);
					if (scenarioSer.scenarioThatWasCopiedNodeId == null) continue;
					if (! scenarioSer.scenarioThatWasCopiedNodeId.equals(modelScenarioSer.getNodeId()))
						continue;  // not derived from this one.
					
					return result = false;
				}
				
				
				// SimulationRuns. If there is at least one SimulationRun for this
				// Scenario, return false.
				
				List<SimulationRunSer> simRunSers = 
					modelEngine.getSimulationRuns(visual.getNodeId());
				
				if (simRunSers.size() > 0) return result = false;

				
				// ScenarioSets.
				
				ModelScenarioSetSer[] scenarioSetSers = 
					modelEngine.getScenarioSetsForDomainId(domainId);
					
				for (ModelScenarioSetSer scenSetSer : scenarioSetSers)
					if (scenSetSer.scenarioThatWasCopiedNodeId.equals(visual.getNodeId()))
						return result = false;
				
				
				// SimRunSets.
				
				SimRunSetSer[] simRunSetSers = 
					modelEngine.getSimRunSetsForScenarioId(visual.getNodeId());
				
				if (simRunSetSers.length > 0) return result = false;
				
				
				return result = true;
			}
			catch (Exception ex)
			{
				if (ex instanceof java.rmi.ConnectException) modelEngine = null;
					// prevents blizzard of messages if the connection fails.
				
				// Prevent endless loop, in case showMessageDialog tries to
				// re-render the Panel and calls this method (again).
				if (ThreadStackUtils.stackContainsPriorCallTo(
					this.getClass().getName() + ".isLeaf")) return result = true;
				
				ErrorDialog.showThrowableDialog(this.container, ex);
					
				return result = true;  // prevents endless loop.
			}
		}
		else if (node instanceof SimulationRunTreeVisual)
		{
			return result = true;
		}
		else if (node instanceof ModelScenarioSetTreeVisual)
		{
			ModelScenarioSetTreeVisual visual = (ModelScenarioSetTreeVisual)node;
			try
			{
				ModelScenarioSer[] scenarioSers = 
					modelEngine.getScenariosForScenarioSetId(visual.getNodeId());

				if (scenarioSers.length > 0) return result = false;
				return result = true;
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(this.container, ex);
					
				return result = true;  // prevents endless loop.
			}
		}
		else if (node instanceof SimRunSetTreeVisual)
		{
			SimRunSetTreeVisual visual = (SimRunSetTreeVisual)node;
			try
			{
				// if there are SimulationRuns in the Set return false;
				SimulationRunSer[] simRunSers = 
					modelEngine.getSimulationRunsForSimRunSetId(visual.getNodeId());
				
				if (simRunSers.length > 0) return result = false;
				
				return result = true;
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(this.container, ex);
					
				return result = true;  // prevents endless loop.
			}
		}
		
		return result = false;
		}
		finally
		{
			//GlobalConsole.println("\t\t\tisLeaf(" + getObjectName(node) + ") = " + result);
		}
	}
	
	
	protected String getObjectName(Object obj)
	{
		if (obj == null) return "null";
		if (obj instanceof TreeVisual) return ((TreeVisual)obj).getName();
		return "<" + obj.getClass().getName() + ">";
	}
	
	
	public String toString() { return hostName; }
	
	
	
	/* *************************The methods below are standard.****************/
	
	
	public synchronized void addTreeModelListener(TreeModelListener l)
	{
		treeModelListeners.addElement(l);
	}
	
	
	public synchronized void removeTreeModelListener(TreeModelListener l)
	{
		treeModelListeners.removeElement(l);
	}
	
	
	public synchronized void valueForPathChanged(TreePath path, Object newValue)
	{
		if (! newValue.equals(path.getLastPathComponent()))
		{
			for (TreeModelListener listener : treeModelListeners)
			{
				listener.treeNodesChanged(new TreeModelEvent(this, path));
			}
		}
	}
	
	
	
  /* ***************************************************************************
   * Protected methods.
   */
	
	
	void domainCreated(String domainId)
	{
		Vector<TreeModelListener> listeners = 
			new Vector<TreeModelListener>(treeModelListeners);
		
		Object[] treeObjectPath = jTree.getPathForRow(0).getPath();
			
		for (TreeModelListener listener : listeners)
		{
			TreeModelEvent event = new TreeModelEvent(this, treeObjectPath);
			listener.treeStructureChanged(event);
		}
	}
	/*
		NodeSer domainSer = null;
		try { domainSer = modelEngine.getNode(domainId); }
		catch (Exception ex)
		{
			JOptionPane.showMessageDialog(viewPanel, 
				ThrowableUtil.getAllMessages(ex),
				"Error", JOptionPane.ERROR_MESSAGE); return;
		}
		
		Vector<TreeModelListener> listeners = 
			new Vector<TreeModelListener>(treeModelListeners);
				
		Object[] treeObjectPath = jTree.getPathForRow(0).getPath();
			
		for (TreeModelListener listener : listeners)
		{
			TreeModelEvent event = new TreeModelEvent(this, treeObjectPath);
			listener.treeStructureChanged(event);
		}
	}*/
	
	
	/*
	void notifyThatTreeNodesChanged(NodeSer[] nodeSers)
		// Call when a Node has changed in some way other than the addition or
		//	removal of child Nodes.
	{
		Vector<TreeModelListener> listeners = 
			new Vector<TreeModelListener>(treeModelListeners);
			
		for (TreeModelListener listener : listeners)
		{
			TreeModelEvent event = new TreeModelEvent(this, 
				Object[] path, int[] childIndices, Object[] children);
				
			listener.treeNodesChanged(event);
		}
	}
	
	
	void notifyThatTreeNodesInserted(NodeSer[] nodeSers)
		// Call when child Nodes have been added to a ModelContainer.
	{
		Vector<TreeModelListener> listeners = 
			new Vector<TreeModelListener>(treeModelListeners);
			
		for (TreeModelListener listener : listeners)
		{
			TreeModelEvent event = new TreeModelEvent(this, ????
			
			listener.treeNodesInserted(event);
		}
	}
	
	
	void notifyThatTreeNodesRemoved(NodeSer[] nodeSers)
		// Call when child Nodes have been removed from a ModelContainer.
	{
		Vector<TreeModelListener> listeners = 
			new Vector<TreeModelListener>(treeModelListeners);
			
		for (TreeModelListener listener : listeners)
		{
			TreeModelEvent event = new TreeModelEvent(this, ????
			
			listener.treeNodesRemoved(event);
		}
	}
	*/
	
	
	void notifyThatTreeStructureChanged(String nodeId)
		/* Call when any change has been made to the tree structure. */
	{
		if (nodeId == null)
		{
			NodeSer ns = null;
			notifyThatTreeStructureChanged(ns);
			return;
		}
		
		NodeSer nodeSer = null;
		try { nodeSer = modelEngine.getNode(nodeId); }
		catch (Exception ex)
		{
			ErrorDialog.showThrowableDialog(viewPanel, ex); return;
		}
		
		notifyThatTreeStructureChanged(nodeSer);
	}
	
	
	void notifyThatTreeStructureChanged(NodeSer nodeSer)
		/* Call when any change has been made to the tree structure. */
	{
		Object[] treeObjectPath;
		if (nodeSer == null)
			treeObjectPath = jTree.getPathForRow(0).getPath();
		else
		{
			// Find the tree path to the specified NodeSer.
			int row = findNodeSer(nodeSer);
			if (row < 0) return;  // not displaying the Node.
			
			treeObjectPath = jTree.getPathForRow(row).getPath();
		}
			
		Vector<TreeModelListener> listeners = 
			new Vector<TreeModelListener>(treeModelListeners);
			
		for (TreeModelListener listener : listeners)
		{
			TreeModelEvent event = new TreeModelEvent(this, treeObjectPath);
			
			try { listener.treeStructureChanged(event); }
			catch (Throwable ex)
			{
				System.out.println("caught throwable");
				removeTreeModelListener(listener);
				System.out.println("Removed listener");
			}
		}
	}
	
	
	/**
	 * Find the tree node that contains the specified NodeSer, within the 
	 * JTree. The JTree is assumed to contain nodes of type TreeVisual.
	 * If the NodeSer is not found, return -1.
	 */
	 
	int findNodeSer(NodeSer nodeSer)
	{
		if (jTree == null) throw new RuntimeException("jTree is null");
		Set<TreePath> paths = jTree.getAllPaths();
		
		for (TreePath treePath : paths)
		{
			Object[] objectPath = treePath.getPath();
			
			Object object = treePath.getLastPathComponent();
			
			if (! (object instanceof TreeVisual)) continue;
			
			TreeVisual treeVisual = (TreeVisual)object;
			
			if (treeVisual.getNodeSer().getNodeId().equals(nodeSer.getNodeId())) 
				return jTree.getRowForPath(treePath);
		}
		
		return -1;
	}


	/**
	 * Find the tree node that contains a NodeSer with the specified NodeId, within 
	 * the JTree. The JTree is assumed to contain nodes of type TreeVisual.
	 * If the NodeSer is not found, return null.
	 */
	 
	TreeVisual findTreeVisualByNodeId(String nodeId)
	{
		if (jTree == null) throw new RuntimeException("jTree is null");
		Set<TreePath> paths = jTree.getAllPaths();
		
		for (TreePath treePath : paths)
		{
			if (treePath == null) continue;
			
			Object[] objectPath = treePath.getPath();
			Object object = treePath.getLastPathComponent();
			
			if (! (object instanceof TreeVisual)) continue;
			
			TreeVisual treeVisual = (TreeVisual)object;
			
			if (treeVisual.getNodeSer().getNodeId().equals(nodeId)) 
				return treeVisual;
				//return jTree.getRowForPath(treePath);
		}
		
		return null;
	}



	/*
	 * Concrete implementations of VisualComponent. These are the nodes in the
	 * tree model.
	 */
	
	public abstract class TreeVisual
		extends DefaultMutableTreeNode
		implements VisualComponent, MutableTreeNode
	{
		private NodeSer nodeSer;
		
		
		public TreeVisual(NodeSer nodeSer)
		{
			if (nodeSer == null) throw new RuntimeException("nodeSer is null");
			if (nodeSer.getNodeId() == null) throw new RuntimeException("node Id is null");
			this.nodeSer = nodeSer;
		}
		
		
		public String toString() { return nodeSer.toString(); }
		
		
		public DomainListPanel getDomainListView() { return (DomainListPanel)(this.getView()); }

		
	  /* Analogous to DefaultMutableTreeNode. */
		
		public boolean isNodeChild(TreeNode aNode)
		{
			return super.isNodeChild(aNode);
		}
		
		
	  /* ***************************************************************************
	   * From MutableTreeNode..
	   */
	
		
		public void insert(MutableTreeNode child, int index)
		{
			super.insert(child, index);
		}
		
		
		public void remove(int index)
		{
			super.remove(index);
		}
		
		
		public void remove(MutableTreeNode node)
		{
			super.remove(node);
		}
		
		
		public void removeFromParent()
		{
			super.removeFromParent();
		}
		
		
		public void setParent(MutableTreeNode newParent)
		{
			super.setParent(newParent);
		}
		
		
		public void setUserObject(Object object)
		{
			if (object == null)
			{
				super.setUserObject(this);
			}
			else
			{
				super.setUserObject(object);
			}
		}

		
		
	  /* ***************************************************************************
	   * From TreeNode..
	   */
	
		
		public Enumeration children()
		{
			return super.children();
		}
		

		public boolean getAllowsChildren()
		{
			return super.getAllowsChildren();
		}
		
		
		public TreeNode getChildAt(int childIndex)
		{
			return super.getChildAt(childIndex);
		}
		
		
		public int getChildCount()
		{
			return super.getChildCount();
		}
		
		
		public int getIndex(TreeNode node)
		{
			return super.getIndex(node);
		}
		
		
		public TreeNode getParent()
		{
			return super.getParent();
		}
		
		
		public boolean isLeaf()
		{
			return super.isLeaf();
		}
				
	
	
	  /* ***************************************************************************
	   * From VisualComponent..
	   */
	
		public String getNodeId() { return nodeSer.getNodeId(); }
		
		 
		public String getName() { return nodeSer.getName(); }
		
		
		public String getFullName() { return nodeSer.getFullName(); }
		
		
		public int getSeqNo() { return 0; }
	
		 
		public abstract boolean contains(VisualComponent comp);
		
		
		public boolean isOwnedByMotifDef()
		{
			return ((ModelElementSer)(getNodeSer())).isOwnedByMotifDef;
		}
		
		
		public boolean isOwnedByActualTemplateInstance()
		{
			return ((ModelElementSer)(getNodeSer())).isOwnedByActualTemplateInstance;
		}

	
		public String[] getChildIds() { return new String[0]; }
		
	
		public void nameChangedByServer(String newName, String newFullName)
		{
		}
		
		
		public void nodeDeleted()
		{
			notifyThatTreeStructureChanged(nodeSer.getParentNodeId());
		}
		
		
		public View getView() { return viewPanel; }
		
		
		public ModelEngineRMI getModelEngine() { return viewPanel.getModelEngine(); }
		
		
		public NodeSer getNodeSer() { return nodeSer; }
		
		
		public void setNodeSer(NodeSer nodeSer)
		{
			if (nodeSer == null) throw new RuntimeException(
				"Attempt to set NodeSer to null");
			
			this.nodeSer = nodeSer;
		}
		
		 
		public void update()
		{
			// Get new NodeSer from server.
			try
			{
				this.nodeSer = modelEngine.getNode(nodeSer.getNodeId());
				refreshRedundantState();
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(viewPanel, ex); return;
			}
			
			//notifyThatTreeNodesChanged(????);
			notifyThatTreeStructureChanged(nodeSer);
		}
		
	
		public void update(NodeSer nodeSer)
		{
			this.nodeSer = nodeSer;
		}
		
		
		public void redraw() {}
		
	
		public void refresh()
		{
			update();
			redraw();
		}
		
		
		public void refreshLocation()
		{
		}
		
		
		public void refreshSize()
		{
		}
	
	
		public void refreshRedundantState()
		throws
			Exception
		{
			DomainListPanel.StatusPanel statusPanel = this.getDomainListView().getStatusPanel();
			statusPanel.redrawIfNodeIdIs(this.getNodeId());
		}
		
		
		public boolean getAsIcon() { return false; }
	
	
		public void setAsIcon(boolean yes) {}
		
		
		public boolean useBorderWhenIconified() { return false; }
	
	
		public void populateChildren()
		throws
			ParameterError,
			Exception {}
	
	
		public Set<VisualComponent> getChildVisuals()
		{
			return new HashSet<VisualComponent>();
		}
		
		
		public Object getParentObject() { return this.getParent(); }
	
	
		public void highlight(boolean yes) {}
		
		
		public boolean isHighlighted() { return false; }
		

		public Color getNormalBackgroundColor() { return normalColor; }
	
	
		public Color getHighlightedBackgroundColor() { return highlightedColor; }


		public int getWidth() { return 0; }
		
		public int getHeight() { return 0; }
	}
	
	
	public class ModelDomainTreeVisual extends TreeVisual implements ModelDomainVisual
	{
		private Color conduitColor = null;
		
		
		public ModelDomainTreeVisual(ModelDomainSer ser) { super(ser); }


	
	  /* ***************************************************************************
	   * From VisualComponent..
	   */
	
		public boolean contains(VisualComponent comp)
		{
			// Return true if comp is a direct child of this TreeVisual, or if
			// any of its children return true for <child>.contains(comp).

			if (! (comp instanceof TreeNode)) return false;
			
			if (this.isNodeChild((TreeNode)comp)) return true;
			
			Enumeration children = this.children();
			for (;children.hasMoreElements();)
			{
				Object childObj = children.nextElement();
				if (! (childObj instanceof ModelDomainTreeVisual)) continue;
				if (((ModelDomainTreeVisual)childObj).contains(comp)) return true;
			}
			
			return false;
		}


		public void nameChangedByServer(String newName, String newFullName)
		{
			notifyThatTreeStructureChanged(this.getNodeSer());
		}
		

		
	  /* ***************************************************************************
	   * From DomainVisual..
	   */
	   
		public ModelScenarioVisual showScenario(String scenarioId)
		{
			notifyThatTreeStructureChanged(this.getNodeSer());
			
			Set<VisualComponent> scenarioVisuals = null;
			try { scenarioVisuals = viewPanel.identifyVisualComponents(scenarioId); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
			
			ModelScenarioVisual scenarioVisual = null;
			for (VisualComponent vis : scenarioVisuals)
			{
				scenarioVisual = (ModelScenarioVisual)vis;
				break;
			}
			
			if (scenarioVisual == null) throw new RuntimeException(
				"Could not create a ModelScenarioVisual for Scenario with ID " + scenarioId);
			
			return scenarioVisual;
		}
		
		
		public void showScenarioCreated(String scenarioId)
		{
			notifyThatTreeStructureChanged(this.getNodeSer());
		}
		
		
		public void showScenarioDeleted(String scenarioId)
		{
			notifyThatTreeStructureChanged(this.getNodeSer());
		}
		
		
		public ScenarioVisual getScenarioVisual(String scenarioId)
		{
			....
		}
		

		public void attributeStateBindingChanged(String attrId, String stateId)
		{
			DomainListPanel.StatusPanel statusPanel = this.getDomainListView().getStatusPanel();
			
			//statusPanel.removeCachedPanelFor(this.getNodeId());
			statusPanel.redrawIfNodeIdIs(this.getNodeId());
		}
		

		
	  /* ***************************************************************************
	   * From ModelDomainVisual..
	   */
	   
		public List<ModelScenarioVisual> getModelScenarioVisuals()
		{
			List<ModelScenarioVisual> visuals = new Vector<ModelScenarioVisual>();
			for (Enumeration e = this.children(); e.hasMoreElements();)
			{
				TreeVisual treeVisual = (TreeVisual)(e.nextElement());
				if (treeVisual instanceof ModelScenarioVisual)
					visuals.add((ModelScenarioVisual)treeVisual);
			}
			
			return visuals;
		}
		
		
		public void simulationRunsDeleted()
		{
			notifyThatTreeStructureChanged(this.getNodeSer());
		}
		
		
		public void showScenarioSetCreated(String newScenSetId)
		{
			notifyThatTreeStructureChanged(this.getNodeSer());
		}
		
		
		public void loadAllScenarios()
		{
		}


		public void motifAdded(String motifId)
		{
		}
		
		
		public void motifRemoved(String motifId)
		{
		}
		
		
	  
	  /* *******************************************************************
		 * *************************************************************************
		 * Methods defined in ContainerVisual.
		 */
		
		
		public void componentResized(VisualComponent child)
		throws
			Exception
		{
			child.refresh();
		}
	
	
			
	  /* ***************************************************************************
	   * From ModelContainerVisual..
	   */


		public void resetConduitColors() {}
		
		
		public void incrementConduitColor() {}
		
		
		public Color getConduitColor() { return this.conduitColor; }
	}
	
	
	public class MotifTreeVisual extends ModelDomainTreeVisual implements MotifDefVisual
	{
		public MotifTreeVisual(ModelDomainMotifDefSer ser) { super(ser); }
	}
	
	
	public class ModelScenarioTreeVisual extends TreeVisual implements ModelScenarioVisual
	{
		private boolean showEventsMode = false;  // not actually used.
		
		
		public ModelScenarioTreeVisual(ModelScenarioSer ser) { super(ser); }
		

	
	  /* ***************************************************************************
	   * From VisualComponent..
	   */
	
		public boolean contains(VisualComponent comp)
		{
			// Return true if comp belongs to this TreeVisual.
			
			if (! (comp instanceof SimulationRunTreeVisual)) return false;
			
			if (this.isNodeChild((TreeNode)comp)) return true;
			
			return false;
		}


		public void nameChangedByServer(String newName, String newFullName)
		{
			notifyThatTreeStructureChanged(this.getNodeSer());
		}
		

		
	  /* ***************************************************************************
	   * From ModelScenarioVisual..
	   */
	   
		public void addSimulationVisual(Integer id, SimulationVisual simPanel)
		{
			getDomainListView().addSimVisual(id, simPanel);
		}
		
		
		public Set<SimulationVisual> simulationRunCreated(Integer requestId,
			String simRunNodeId)
		{
			return this.getDomainListView().getSimulationVisuals(requestId);
		}
		
		
		public void simulationRunsDeleted()
		{
		}

		
		public void refreshAttributeValue(String attrNodeId)
		{
			// If the StatusPanel is displaying this Scenario, then refresh
			// the StatusPanel.
			
			if (this.getDomainListView().getStatusPanel().getNodeSer().getNodeId()
					.equals(this.getNodeSer().getNodeId()))
				this.getDomainListView().getStatusPanel().showNodeSer(this.getNodeSer());
		}
		
	   
		public void setShowEventsMode(boolean showEventsMode)
		{
			this.showEventsMode = showEventsMode;
		}
		
		
		public boolean getShowEventsMode()
		{
			return this.showEventsMode;
		}
		
		
		public void showEventsForIteration(String simRunId, int iteration)
		{
		}
		
		
		public void refreshStatistics()
		{
			update();
			
			this.getDomainListView().getStatusPanel().redrawIfNodeIdIs(this.getNodeId());
		}
		
		
		public void simTimeParamsChanged(Date newFrom, Date newTo, long newDuration,
			int newIterLimit)
		{
			this.getDomainListView().getStatusPanel().updateModelScenarioTimeParams(this.getNodeId(),
				newFrom, newTo, newDuration, newIterLimit);
			//this.getDomainListView().getStatusPanel().redrawIfNodeIdIs(this.getNodeId());
		}
		
		
		public void setLocation(VisualComponent visual) {}


		public void addOrRemoveAttributeControls(boolean add, VisualComponent visual)
		throws Exception {}

		public void addOrRemoveStateControls(boolean add, VisualComponent visual)
		throws Exception {}
		
		public void deleteControls(String nodeId) {}
		
		public boolean controlsForAttrAreVisible(ModelElementVisual visual) { return false; }
		
		public boolean controlsForStateAreVisible(PortedContainerVisual visual) { return false; }
		
		public JFrame createEventHistoryWindow(String simRunId, Class... stateTags)
		throws
			Exception
		{
			return EventHistoryPanel.createEventHistoryWindow(getView(), 
				simRunId, stateTags);
		}
	}
	
	
	public class ModelScenarioSetTreeVisual extends TreeVisual implements ModelScenarioSetVisual
	{
		public ModelScenarioSetTreeVisual(ModelScenarioSetSer ser) { super(ser); }
		
		public void addSimulationVisual(Integer id, SimulationVisual simPanel)
		{
			getDomainListView().addSimVisual(id, simPanel);
		}
		
		
		public Set<SimulationVisual> simulationRunCreated(Integer requestId,
			String simNodeId)
		{
			notifyThatTreeStructureChanged(this.getNodeSer());
						
			return viewPanel.getSimulationVisuals(requestId);
		}
		
		
		public void simulationRunsDeleted()
		{
			notifyThatTreeStructureChanged(this.getNodeSer());
		}
		
		
		public void refreshStatistics()
		{
			update();
			
			if (this.getDomainListView().getStatusPanel().getNodeSer().getNodeId()
					.equals(this.getNodeSer().getNodeId()))
				this.getDomainListView().getStatusPanel().showNodeSer(this.getNodeSer());
		}
		

		public boolean contains(VisualComponent comp) { return false; }
	}
	
	
	public class SimulationRunTreeVisual extends TreeVisual implements SimulationRunVisual
	{
		public SimulationRunTreeVisual(SimulationRunSer ser) { super(ser); }
		
		

	  /* ***************************************************************************
	   * From VisualComponent..
	   */
	
		public boolean contains(VisualComponent comp)
		{
			return false;
		}
		

		
	  /* ***************************************************************************
	   * From SimulationRunVisual..
	   */
	   
		public void attributesInitialized() {}
		public void attributesUpdated() {}
		public void stateInitEventsCreated() {}
		public void predefinedEventsRetrieved() {}
		public void epochStarted(int epochNo, Date date) {}
		public void epochFinished(int epochNo, Date date) {}
		public void allFunctionsEvaluatedForEpoch(int epochNo, Date date) {}
		public void allActivitiesActivatedForEpoch(int epochNo, Date date) {}
		public void reportProgress(String msg) {}
		public void epochCompleted(Date epoch, int iteration) {}
		
		public JFrame createEventHistoryWindow(Class... stateTags) throws Exception
		{
			return EventHistoryPanel.createEventHistoryWindow(getView(), 
				getNodeId(), stateTags);
		}
	}


	public class SimRunSetTreeVisual extends TreeVisual implements SimRunSetVisual
	{
		public SimRunSetTreeVisual(SimRunSetSer ser) { super(ser); }

		public void simRunSetCreated(String simRunSetId, String modelScenarioId)
		{
			notifyThatTreeStructureChanged(modelScenarioId);
		}

		public boolean contains(VisualComponent comp) { return false; }
		
		public void refreshStatistics()
		{
			update();
			
			if (this.getDomainListView().getStatusPanel().getNodeSer().getNodeId()
					.equals(this.getNodeSer().getNodeId()))
				this.getDomainListView().getStatusPanel().showNodeSer(this.getNodeSer());
		}
	}
}

