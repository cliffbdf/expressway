package expressway.ser;

import expressway.common.*;
import java.io.Serializable;


public class ClassificationSer extends ClassificationElementSer
	implements HierarchySer, TabularNodeSer
{
	public static String[] fieldNames =
	{
		"Name",
		"HTML Full Description"
	};
	
	public String[] subClassificationNodeIds;
	public int seqNo;

	public String getNodeKind() { return NodeKindNames.Classification; }
	
	
  /* From HierarchySer */

	
	public void setSubHierarchyNodeIds(String[] ids) { subClassificationNodeIds = ids; }
	
	
	public String[] getSubHierarchyNodeIds() { return subClassificationNodeIds; }
	
	
  /* From TabularNodeSer */

	
	public String[] getFieldNames() { return fieldNames; }
	
	
	public void setValueAt(Serializable value, int column)
	{
		switch (column)
		{
		case 0: setName((String)value); break;
		case 1: setHtmlDescription(value.toString()); break;
		}
	}
	
	
	public Serializable getValueAt(int column)
	{
		switch (column)
		{
		case 0: return getName();
		case 1: return getHtmlDescription();
		}
		
		throw new RuntimeException("Unexpected column number: " + column);
	}


	public int getSeqNo() { return seqNo; }
}

