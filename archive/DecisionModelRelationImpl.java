/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class DecisionModelRelationImpl extends DecisionElementImpl implements DecisionModelRelation
{
	DecisionDomain dd = null;
	ModelElement.ModelDomain md = null;
	
	public transient String ddNodeId = null;
	public transient String mdNodeId = null;
	
	public String getDdNodeId() { return ddNodeId; }
	public String getMdNodeId() { return mdNodeId; }


	protected Class getSerClass() { return DecisionModelRelationSer.class; }
	
	
	protected NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		ddNodeId = getNodeIdOrNull(dd);
		mdNodeId = getNodeIdOrNull(md);
		
		
		// Set Ser fields.
		
		((DecisionModelRelationSer)nodeSer).ddNodeId = this.getDdNodeId();
		((DecisionModelRelationSer)nodeSer).mdNodeId = this.getMdNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public Object clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DecisionModelRelationImpl newInstance = (DecisionModelRelationImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.dd = cloneMap.getClonedNode(dd);
		newInstance.md = cloneMap.getClonedNode(md);
		
		return newInstance;
	}


	public void prepareForDeletion()
	{
		super.prepareForDeletion();
		
		// Call <type>Deleted(this) on each DeletionListener.
		// (There are none.)
		
		
		// For each owned Node, call prepareForDeletion() and then remove it.
		// (There are none.)
		
		
		// Remove other non-Node owned Objects.
		// (There are none.)
	}
	
	
	public DecisionModelRelationImpl(DecisionDomain dd, ModelElement.ModelDomain md)
	{
		super(dd, dd);
		setResizable(false);
		this.md = md;
	}


	public int getNoOfHeaderRows() { return 0; }
	
	
	public void setIconImage() { setIconImage(getImageResource(NodeIconImageNames.DecisionModelRelationIconImageName)); }

	
	private static final double PreferredWidthInPixels = 30.0;
	private static final double PreferredHeightInPixels = 30.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public DecisionDomain getDecisionDomain()
	{
		return dd;
	}


	public ModelElement.ModelDomain getModelDomain()
	{
		return md;
	}
}
