/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.ser.*;
import expressway.server.StrategyElement.*;
import expressway.server.MotifElement.*;
import java.util.List;
import java.util.Vector;
import java.util.Set;
import java.util.TreeSet;
import java.util.SortedSet;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.IOException;


public class StrategyImpl
	extends HierarchyBase //StrategyElementImpl
	implements Strategy, StrategyTemplate
{
	private String htmlSummary;
	
	
  /* Constructors */
  
  
	protected StrategyImpl(String baseName, Strategy parent)
	{
		super(baseName, parent);
	}
	
	
  /* From HierarchyBase */
	
	
	protected Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		return new StrategyImpl(name, this);
	}
	
	
  /* From StrategyElement */
	
	
	public StrategyDomain getStrategyDomain() { return (StrategyDomain)(getDomain()); }	
	
	
	public StrategyAttribute createStrategyAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (StrategyAttribute)(createAttribute(name, defaultValue, layoutBound,
			outermostAffectedRef));
	}
	
	
	public StrategyAttribute createStrategyAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (StrategyAttribute)(createAttribute(layoutBound,
			outermostAffectedRef));
	}


	public StrategyAttribute createStrategyAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (StrategyAttribute)(createAttribute(name, layoutBound,
			outermostAffectedRef));
	}
	

  /* From HierarchyElementBase. Extend as needed. */


	protected String getTextToRender()
	{
		return super.getTextToRender();
	}
	
	
	protected String getTagName() { return "strategy"; }
	
	
	protected void writeSpecializedXMLAttributes(PrintWriter writer)
	{
		writer.print(getHTMLSummary() == null ? "" :
			("summary=\"" + getHTMLSummary().replace("\"", "\\\"") + "\" "));
	}
	
	
	protected void writeSpecializedXMLElements(PrintWriter writer, int indentation) {} // ok
	
	
  /* From Strategy */
  
  
	public int getNoOfStrategys()
	{
		return getNoOfHierarchies();
	}
	
	
	public Strategy createSubStrategy(String name, int position, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (Strategy)(createSubHierarchy(name, position, layoutBound, outermostAffectedRef));
	}
		
		
	public Strategy createSubStrategy(String baseName, int position,
		Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return (Strategy)(createSubHierarchy(baseName, position, scenario, copiedNode));
	}


	public Strategy createSubStrategy(String name, int position, 
		Scenario scenario, StrategyTemplate template)
	throws
		ParameterError
	{
		return (Strategy)(createSubHierarchy(name, position, scenario, template));
	}
		
		
	public Strategy createSubStrategy(String baseName, int position)
	throws
		ParameterError
	{
		return (Strategy)(createSubHierarchy(baseName, position));
	}


	public Strategy createSubStrategy(String baseName)
	throws
		ParameterError
	{
		return (Strategy)(createSubHierarchy(baseName));
	}


	public Strategy getParentStrategy()
	{
		return(Strategy)(getParentHierarchy());
	}


	public void setParentStrategy(Strategy parent)
	throws
		ParameterError
	{
		setParentHierarchy(parent);
	}
	
	
	public List<Strategy> getSubStrategys()
	{
		List<Strategy> rlist = new Vector<Strategy>();
		List<Hierarchy> hlist = getSubHierarchies();
		for (Hierarchy h : hlist) rlist.add((Strategy)h);
		return rlist;
	}
	
	
	public Strategy getSubStrategy(String name)
	throws
		ElementNotFound
	{
		return (Strategy)(getSubHierarchy(name));
	}
	
	
	public String getHTMLSummary()
	{
		return htmlSummary;
	}
	
	
	public void setHTMLSummary(String summary)
	{
		this.htmlSummary = summary;
	}


  /* From PersistentNodeImpl */
	
	
	protected Class getSerClass() { return StrategySer.class; }
	
	
	protected NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		synchronized (this.getDomain())
		{
			StrategySer ser = (StrategySer)nodeSer;
			ser.htmlSummary = htmlSummary;
			ser.seqNo = getPosition();

			return super.externalize(ser);
		}
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		StrategyImpl newInstance =
			(StrategyImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}
	
	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		htmlSummary = null;
	}
	
	
	protected void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.StrategyIconImageName);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		 { return new TreeSet<PersistentNode>(); }
	}
	
	
	public double getPreferredWidthInPixels() { return 50; }
	

	public double getPreferredHeightInPixels() { return 0; }
		
		
	public Attribute constructAttribute(String name, Serializable defaultValue)
	{
		return new StrategyAttributeImpl(name, this, defaultValue);
	}
	
	
	public Attribute constructAttribute(String name)
	{
		return new StrategyAttributeImpl(name, this);
	}
	
	
  /* From PersistentListNode */


  /* From MenuOwner */


  /* From Template */


  /* From TemplateInstance */
}

