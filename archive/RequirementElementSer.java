package expressway.ser;

import expressway.common.*;



public abstract class RequirementElementSer
	extends TemplatizableNodeSer implements HierarchySer
	//extends NodeSerBase
	//implements HierarchyElementSer
{
	public String[] subHierarchyNodeIds;
	
	public void setSubHierarchyNodeIds(String[] ids) { subHierarchyNodeIds = ids; }
	
	public String[] getSubHierarchyNodeIds() { return subHierarchyNodeIds; }
}

