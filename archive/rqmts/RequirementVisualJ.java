/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.rqmts;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.*;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;
import expressway.gui.TreeTablePanelViewBase.*;
import expressway.help.*;
import java.util.Set;
import java.util.HashSet;
import java.awt.Container;
import java.awt.event.*;
import java.io.Serializable;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;


public class RequirementVisualJ extends TabularVisualJ
{
	public RequirementVisualJ(NodeSer rqmtSer, View view)
	throws
		ParameterError
	{
		super(rqmtSer, (TreeTablePanelViewBase)view, RequirementSer.fieldNames.length);
		if (! (rqmtSer instanceof RequirementSer)) throw new RuntimeException(
			"rqmtSer is not a RequirementSer");
		if (! (view instanceof RequirementViewPanel)) throw new RuntimeException(
			"view is not a RequirementViewPanel");
		for (int i = 0; i < RequirementSer.fieldNames.length; i++)
			setValueAt(((RequirementSer)rqmtSer).getValueAt(i), i);
	}
	
	
	public String getNodeKind() { return "Requirement"; }
	
	
	public String getDemotionNodeKind() { return "Requirement"; }
	
	
	public String getDescriptionPageName() { return "Requirements"; }


	public String getScenarioId() { return null; }
	
	
	public void updateServer(int column)
	{
		switch (column)
		{
		case 0: // name
			{
				try
				{
					try { getModelEngine().setNodeName(false, getNodeId(), getNodeSer().getName()); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(null, w.getMessage(),
							"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
						{
							getModelEngine().setNodeName(true, getNodeId(), getNodeSer().getName());
						}
					}
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				
				break;
			}
		case 1: // String htmlSummary
			{
				try { getModelEngine().setRequirementSummary(getNodeId(),
					((RequirementSer)(getNodeSer())).htmlSummary); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				
				break;
			}
		case 2: // String htmlDescription
			{
				try { getModelEngine().setNodeHTMLDescription(getNodeId(),
					((RequirementSer)(getNodeSer())).htmlDescription); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				
				break;
			}
		default: throw new RuntimeException("Unexpected column number: " + column);
		}
	}
	
	
	protected void addContextMenuItems(JPopupMenu containerPopup, final int x, final int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
	}
	
	
	public boolean isEditable(int column) { return column >= 0; }
}

