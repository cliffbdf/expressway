/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.rqmts;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.gui.*;
import expressway.ser.*;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;
import java.awt.Container;
import java.io.IOException;
import java.io.PrintWriter;


public class RequirementAttributeVisualJ extends AttributeVisualBase
implements RequirementAttributeVisual
{
	RequirementAttributeVisualJ(AttributeSer attrSer, ViewPanelBase viewPanel)
	{
		super(attrSer, viewPanel);
	}


	public int getSeqNo() { return ((RequirementSer)(getNodeSer())).seqNo; }
	
	
	public void setLocation()
	{
		Container container = this.getParent();
		if (container == null) return;


		// If the Container is a ControlPanel, position the Visual based on the
		// location of the VisualComponent that depicts the Node's parent.
		
		if (! (container instanceof ControlPanel)) throw new RuntimeException(
			"Container is a " + container.getClass().getName() + "; expected a ControlPanel");
		
		((ControlPanel)container).setLocation(this);
	}
	
	
	public void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{ // ok
	}
	
	
	public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{ // ok
	}
}

