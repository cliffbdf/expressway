package expressway.ser;


import expressway.common.*;


public class ClassificationDomainMotifDefSer extends ClassificationDomainSer
	implements MotifDefSer, MenuOwnerSer
{
	public String[] motifClassNames;

	public String[] menuItemNodeIds;
	public String[] menuItemTextAr;
	public String[] menuItemDescAr;
	public String[] javaMethodNames;
	public String[] menuTreeNodeIds;

	public String getNodeKind() { return NodeKindNames.ClassificationDomainMotifDef; }
	
	/* From MotifDefSer. */
	
	public String[] getMotifClassNames() { return motifClassNames; }

	
	/* From MenuOwnerSer. */
	 
	public void setMenuItemNodeIds(String[] ids) { this.menuItemNodeIds = ids; }
	public void setMenuItemTextAr(String[] textAr) { this.menuItemTextAr = textAr; }
	public void setMenuItemDescAr(String[] descAr) { this.menuItemDescAr = descAr; }
	public void setJavaMethodNames(String[] names) { this.javaMethodNames = names; }
	public void setMenuTreeNodeIds(String[] ids) { this.menuTreeNodeIds = ids; }
	
	public String[] getMenuItemNodeIds() { return menuItemNodeIds; }
	public String[] getMenuItemTextAr() { return menuItemTextAr; }
	public String[] getMenuItemDescAr() { return menuItemDescAr; }
	public String[] getJavaMethodNames() { return javaMethodNames; }
	public String[] getMenuTreeNodeIds() { return menuTreeNodeIds; }
}

