/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.strats;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.*;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;
import expressway.gui.TreeTablePanelViewBase.*;
import expressway.help.*;
import java.util.Set;
import java.util.HashSet;
import java.awt.Container;
import java.awt.event.*;
import java.io.Serializable;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;


public class StrategyVisualJ extends TabularVisualJ
{
	public StrategyVisualJ(NodeSer stratSer, View view)
	throws
		ParameterError
	{
		super(stratSer, (TreeTablePanelViewBase)view, StrategySer.fieldNames.length);
		if (! (stratSer instanceof StrategySer)) throw new RuntimeException(
			"stratSer is not a StrategySer");
		if (! (view instanceof StrategyViewPanel)) throw new RuntimeException(
			"view is not a StrategyViewPanel");
		for (int i = 0; i < StrategySer.fieldNames.length; i++)
			setValueAt(((StrategySer)stratSer).getValueAt(i), i);
	}
	
	
	public String getNodeKind() { return "Strategy"; }
	
	
	public String getDemotionNodeKind() { return "Strategy"; }
	
	
	public String getDescriptionPageName() { return "Strategys"; }


	public String getScenarioId() { return null; }
	
	
	public void updateServer(int column)
	{
		switch (column)
		{
		case 0: // name
			{
				try
				{
					try { getModelEngine().setNodeName(false, getNodeId(), getNodeSer().getName()); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(null, w.getMessage(),
							"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
						{
							getModelEngine().setNodeName(true, getNodeId(), getNodeSer().getName());
						}
					}
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				
				break;
			}
		case 1: // String htmlSummary
			{
				try { getModelEngine().setStrategySummary(getNodeId(),
					((StrategySer)(getNodeSer())).htmlSummary); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				
				break;
			}
		case 2: // String htmlDescription
			{
				try { getModelEngine().setNodeHTMLDescription(getNodeId(),
					((StrategySer)(getNodeSer())).htmlDescription); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				
				break;
			}
		default: throw new RuntimeException("Unexpected column number: " + column);
		}
	}
	
	
	protected void addContextMenuItems(JPopupMenu containerPopup, final int x, final int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
		
		/*
		// Find adjacent Views, if any.
		
		Set<TreeTablePanelViewBase> otherTreeTablePanelViews = 
			getTreeTablePanelViewBase().getAdjacentTreeTablePanelViews();
			
		Set<TreeTablePanelViewBase> adjacentStrategyViews = new HashSet<TreeTablePanelViewBase>();
		for (TreeTablePanelViewBase otherView : otherTreeTablePanelViews)
		{
			//if (otherView.getOutermostVisual() instanceof <>)
				adjacentStrategyViews.add(otherView);
		}
		
		for (final TreeTablePanelViewBase otherView : adjacentStrategyViews)
			//if (adjacentStrategyViews.size() > 0)
			// this Visual exists in a DualHierarchyPanel and the other
			// View consists of Strategies
		{
			JMenuItem menuItem = new HelpfulMenuItem("Link to...", false,
				"Link, via a " +
				HelpWindow.createHref("Named References", "Named Reference") +
				", the selected row (" +
				HelpWindow.createHref("Strategies", "Strategy") + ") to a " +
				"node in another View. The name of " +
				"the Named Reference used is 'AddressesStrategy'.");
			
			menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					// The user should now
					// select a Strategy TreeTableNode from the other View.
					// This is a modal operation.
					
					otherView.selectNode(new TabularSelectionHandler()
					{
						public void tableVisualSelected(TabularVisualJ otherVisual)
						{
							try
							{
								try { getModelEngine().createCrossReference(false,
									StrategyVisualJ.this.getNodeSer().getDomainId(),
									StandardNamedReferences.AddressesStrategy,
									otherVisual.getNodeId(), StrategyVisualJ.this.getNodeId()); }
								catch (Warning w)
								{
									if (JOptionPane.showConfirmDialog(null, w.getMessage(),
										"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
									{
										getModelEngine().createCrossReference(true,
											StrategyVisualJ.this.getNodeSer().getDomainId(),
											StandardNamedReferences.AddressesStrategy,
											otherVisual.getNodeId(), StrategyVisualJ.this.getNodeId());
									}
								}
							}
							catch (Exception ex)
							{
								ErrorDialog.showReportableDialog(getViewPanel(),
									"While creating named reference '" + 
									StandardNamedReferences.AddressesStrategy + 
									"' on the server", "Server Error", ex);
							}
						}
					});
					
				}
			});
			
			containerPopup.add(menuItem);
		}*/
	}
	
	
	public boolean isEditable(int column) { return column >= 0; }
}

