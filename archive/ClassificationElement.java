/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import expressway.server.NamedReference.CrossReferenceable;
import expressway.server.HierarchyElement.*;
import java.util.List;
import java.util.SortedSet;
import java.io.Serializable;


/**
 * Nodes that define a hierarchical taxonomy. See the slide "Hierarchy".
 */
 
public interface ClassificationElement extends PersistentNode, HierarchyElement
{
	interface ClassificationDomain extends Classification, HierarchyDomain
	{
	}
	
	
	interface ClassificationAttribute extends ClassificationElement, HierarchyAttribute
	{
	}
	
	
	interface ClassificationDomainMotifDef extends ClassificationDomain, HierarchyDomainMotifDef
	{
	}
	
	
	interface ClassificationTemplate extends HierarchyTemplate
	{
	}


	interface Classification extends ClassificationElement, Hierarchy
	{
		Classification createSpecialization(String name);
		
		
		/**
		 * Return the specified Classification that extends this Classification.
		 * Only consider those that immediately extend it.
		 */
		 
		Classification getSpecialization(String name)
		throws
			ElementNotFound;
		
		
		/**
		 * Return the specified Classification that extends this Classification.
		 * Look beyond those that immediately extend this Classification, based
		 * on a hierarical name, e.g., ABC.DEF would look for the Classification
		 * named "DEF" within the specialization "ABC" of this Classification.
		 */
		 
		Classification getNestedSpecialization(String fullName)
		throws
			ElementNotFound;
		
		
		/**
		 * Return the Classifications in this Classification's Domain that "extend"
		 * this Classification. Only consider those that immediately extend it.
		 */
		 
		SortedSet<Classification> getSpecializations();
		
		
		/**
		 * Return the Classification that this Classification immediately extends.
		 */
		 
		Classification getSuperiorClassification();
	}
	
	
	ClassificationDomain getClassificationDomain();
	
	
	ClassificationAttribute createClassificationAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	
	
	ClassificationAttribute createClassificationAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;


	ClassificationAttribute createClassificationAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
}

