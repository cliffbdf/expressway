/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway;

import org.odmg.Implementation;
import org.odmg.Transaction;
//import org.ozoneDB.LocalDatabase;
//import org.ozoneDB.odmg.OzoneODMG;
import expressway.ModelElement.*;
import expressway.ModelAPITypes.*;

/**
 * Implements the ModelEngine interface using the Ozone database.
 *
 * Persistence is provided by an Ozone object database. Therefore, no
 * relational database or object-relational mapping is required.
 * The public methods provided by this class are transactional.
 */

public class ModelEngineOzoneImpl implements ModelEngine
{
	private static String dbDir =
		"C:\\Documents and Settings\\Cliff\\My Documents\\" +
		"Expressway\\Prototype\\test\\ozoneDB";

	private LocalDatabase db = null;
	private Implementation dbImpl = null;

	/**
	 * Construct the database object and open the database, using
	 * the default database path.
	 */

	public ModelEngineOzoneImpl() { this(dbDir); }

	/**
	 * Construct the database object and open the database, using
	 * the supplied database path.
	 */

	public ModelEngineOzoneImpl(String dbDir)
	{
		/*
		 * Open database.
		 */

		try
		{
			db = new LocalDatabase();

			if (!db.exists(dbDir))
			{
				System.out.println("Creating database in " + dbDir + "...");
				db.create(dbDir);
				System.out.println("...database created.");
			}

			System.out.println("Opening database...");
			db.open(dbDir);
			System.out.println("...database opened...");
			dbImpl = new org.ozoneDB.odmg.OzoneODMG();
			System.out.println("...and DB implementation created.");

			System.out.println("Reloading database classes...");
			db.reloadClasses();
			System.out.println("...database classes reloaded: database ready.");
		}
		catch (Throwable t)
		{
			t.printStackTrace();
			throw new RuntimeException(t);
		}
	}

	/**
	 * Close the database.
	 */

	public void finalize()
	{
		try
		{
			db.close();
			System.out.println("Database stopped...");
			super.finalize();
		}
		catch (Throwable t)
		{
			t.printStackTrace();
			System.out.println("Database stop failed.");
		}
	}


	/**
	 * Add an element instance to the specified parent element.
	 */

	public void addChildElement(ModelElement parent, ModelElement child)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		....
	}


	/**
	 * Locate the specified model in the database, and update the
	 * specified attribute with the specified new value.
	 */

	public void updateAttribute(String elementName, String attributeName,
		Object value)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		System.out.println("Entered updateAttribute");

		/*
		 * Start transaction.
		 */

		Transaction transaction = null;

		try
		{
			transaction = dbImpl.newTransaction();
			if (transaction == null) throw new Exception("Transaction is null");
		}
		catch (Exception ex)
		{
			throw new InternalEngineError(ex);
		}

		/*
		 * Wrap transaction in try block to catch any exception so that
		 * rollback can be performed.
		 */

		try
		{
			////...

			transaction.commit();
		}
		catch (RuntimeException rex)
		{
			transaction.abort();

			throw rex;		// re-throw.
		}
		catch (Throwable t)		// Top-level exception firewall.
		{
			transaction.abort();

			InternalEngineError ier = new InternalEngineError(t);
			throw ier;
		}
	}

	/**
	 * Locate the specified model in the database, and return a serializable
	 * tree (a copy) of its elements.
	 *
	 * Starts and completes transaction.
	 */

	public ModelElement getModelElementTree(String elementName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		System.out.println("Entered getModelElementTree");

		ModelElement element = null;

		/*
		 * Start transaction.
		 */

		Transaction transaction = null;

		try
		{
			transaction = dbImpl.newTransaction();
			if (transaction == null) throw new Exception("Transaction is null");
		}
		catch (Exception ex)
		{
			throw new InternalEngineError(ex);
		}

		/*
		 * Wrap transaction in try block to catch any exception so that
		 * rollback can be performed.
		 */

		try
		{
			/*
			 * Locate the element in the database.
			 */

			try
			{
				element = (ModelElement)(db.objectForName(elementName));
				if (element == null) throw new ElementNotFound();
			}
			catch (CannotObtainLock ex1)
			{
				throw ex1;
			}
			catch (ElementNotFound ex2)
			{
				throw ex2;
			}

			/*
			 * Recursively traverse the element's sub-elements, and assemble a
			 * serializable copy of the element tree.
			 */

			element = element.getModelElementTree();

			ModelElementImpl.dump(element);

			transaction.commit();
		}
		catch (RuntimeException rex)
		{
			transaction.abort();

			throw rex;		// re-throw.
		}
		catch (Throwable t)		// Top-level exception firewall.
		{
			transaction.abort();

			InternalEngineError ex = new InternalEngineError(t);
			ex.printStackTrace();
			throw ex;
		}

		return element;
	}
}
