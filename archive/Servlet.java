/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway;

import org.odmg.*;
import org.ozoneDB.*;
import org.jdom.*;
import org.jdom.output.XMLOutputter;
import javax.servlet.*;
import Element.*;

/**
 * Provides a service-oriented interface for the client.
 * Service requests are HTTP POST queries, and responses are XML
 * documents. The intent is that the requests are issued using
 * an Open-Laszlo DHTML client, and the responses are consumed by
 * the same client.
 *
 * The intent is also that the servlet be instantiated only once,
 * because it serves as the main server application and interface
 * to the database.
 */
public class Servlet extends javax.servlet.Servlet;
{
	static String dbDir = "/usr/local/test/ozoneDB";
	private LocalDatabase db = new LocalDatabase(dbDir);

	public ApplicationInstance newInstance() { return new Session(); }

	/**
	 * Called by servlet container after framework-level initialization
	 * has completed.
	 */

	public void init(ServletConfig servletConfig)
	throws ServletException
	{
		super.init(servletConfig);

		/*
		 * Open database.
		 */

		if (!db.exists(dbDir))
		{
			System.out.println("Creating database in " + dbDir);
			db.create(dbDir);
			System.out.println("Database created");
		}

		private static ExternalDatabase	db = null;
		try
		{
			System.out.println("Opening database in " + dbDir);
			db = ExternalDatabase.openDatabase("ozonedb:local://" + dbDir);
		}
		catch (Throwable t)
		{
			System.out.println("Unable to open database in " + dbDir);
			t.printStackTrace();
			throw new ServletException(t);
		}

		System.out.println("Database opened...");

		db.reloadClasses();
	}

	/**
	 * Called by servlet container when servlet is stopped.
	 */

	public void destroy()
	{
		try
		{
			db.close();
			System.out.println("Database stopped...");
		}
		catch
		{
			System.out.println("Database stop failed.");
		}

		super.destroy();
	}

	/**
	 * Handle service requests.
	 *
	 * All requests have the following parameters:
	 *	METHOD		The service method being requested. Each method corresponds
	 *				to a Java method in this class.
	 *
	 * Each request also has parameters that correspond to the names of the
	 * associated Java method.
	 *
	 * If an exception is thrown by the Java method, the response merely contains
	 * an <exception/> tag with a 'name' attribute that identifies the kind of
	 * error condition, and a 'message' attribute that contains the
	 * error message.
	 *
	 * If the request is successful, the response is returned within a 'response'
	 * XML tag. If the response is null, then the 'response' tag is still present
	 * but contains no content.
	 */

	protected void service(HttpServletRequest req, HttpServletResponse resp)
	throws
		ServletException,
		java.io.IOException
	{
		resp.setContentType("text/xml");

		String methodName = req.getParameter("METHOD");
		if (methodName == null)
		{
			genErrorResp(resp.getOutputStream, "NullMethodName",
				"Null method name");
			return;
		}

		if (methodName.equalsIgnoreCase("GetElementTree"))
		{
			/* Get method parameters. */

			String elementName = req.getParameter("ELEMENTNAME");
			if (elementName == null)
			{
				genErrorResp(resp.getOutputStream, "NullParameter",
					"Null method parameter");
				return;
			}

			/* Search database for the element. */

			expressway.Element element = null;
			try
			{
				element = GetElementTree(elementName);
			}
			catch (CannotObtainLock ex1)
			{
				genErrorResp(resp.getOutputStream(), "DatabaseTimeout",
					"Cannot obtain database lock - timeout");
				return;
			}
			catch (ElementNotFound ex2)
			{
				element = null;
			}
			catch (ModelContainsError ex3)
			{
				genErrorResp(resp.getOutputStream(), "ModelContainsError",
					"Model contains error");
				return;
			}
			catch (InternalEngineFailure ex4)
			{
				genErrorResp(resp.getOutputStream(), "InternalEngineError",
					"Internal error");

				getServletConfig().getServletContext().log("Internal error", ex4);
				return;
			}

			/* Retrieve element tree. */

			org.jdom.Element xmlNode = buildTreeAsXML(element);

			/* Serialize and return the model.*/

			Element xmlResponse = new Element("response");

			if (xmlNode != null) // element was found in the database
				xmlResponse.addContent(xmlNode);

			XMLOutputter outputter = new XMLOutputter();
			outputter.output(xmlResponse, resp.getOutputStream());
		}
		else if (methodName.equalsIgnoreCase("UpdateAttribute"))
		{
			/* Get method parameters: elementName, attributeName and value. */

			String elementName = req.getParameter("ELEMENTNAME");
			if (elementName == null)
			{
				genErrorResp(resp.getOutputStream, "NullParameter",
					"Null method parameter");
				return;
			}

			String attributeName = req.getParameter("ATTRIBUTENAME");
			if (attributeName == null)
			{
				genErrorResp(resp.getOutputStream, "NullParameter",
					"Null method parameter");
				return;
			}

			String value = req.getParameter("VALUE");
			if (value == null)
			{
				genErrorResp(resp.getOutputStream, "NullParameter",
					"Null method parameter");
				return;
			}

			try
			{
				updateAttribute(elementName, attributeName, value);
			}
			catch (CannotObtainLock ex1)
			{
				genErrorResp(resp.getOutputStream(), "DatabaseTimeout",
					"Cannot obtain database lock - timeout");
				return;
			}
			catch (ElementNotFound ex2)
			{
				genErrorResp(resp.getOutputStream(), "ElementNotFound",
					"Element not found");
			}
			catch (ModelContainsError ex3)
			{
				genErrorResp(resp.getOutputStream(), "ModelContainsError",
					"Model contains error");
				return;
			}
			catch (InternalEngineFailure ex4)
			{
				genErrorResp(resp.getOutputStream(), "InternalEngineError",
					"Internal error");

				getServletConfig().getServletContext().log("Internal error", ex4);
				return;
			}
		}
		else
		{
			genErrorResp(resp.getOutputStream(), "UnrecognizedMethod",
				"Unrecognized method name: " + methodName);
			return;
		}
	}


	/* ------------------------Implementation Methods---------------------------- */

	/**
	 * Generate an error response in an XML format.
	 */

	protected void genErrorResp(java.io.OutputStream os, String excName, String msg)
	throws
		java.io.IOException
	{
		XMLOutputter outputter = new XMLOutputter();
		Element element = new Element("exception");
		element.setAttribute("message", msg);
		outputter.output(element, os);
	}

	/**
	 * Locate the specified model in the database, and return a serializable
	 * tree (a copy) of its elements.
	 *
	 * Starts and completes transaction.
	 */

	protected expressway.Element GetElementTree(elementName: string)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		InternalEngineFailure
	{
		Element element = null;

		/*
		 * Start transaction.
		 */

		org.odmg.Implementation dbImpl = new org.ozoneDB.odmg.ODMG();
		Transaction transaction = null;

		try
		{
			transaction = dbImpl.newTransaction();
			if (transaction == null) throw new Exception("Transaction is null");
		}
		catch (Exception ex)
		{
			throw new InternalEngineFailure(ex);
		}


		/*
		 * Wrap transaction in try block to catch any exception so that
		 * rollback can be performed.
		 */
		try
		{
			/*
			 * Locate the element in the database.
			 */

			try
			{
				element = (expressway.Element)(db.lookup(modelName));
				if (element == null) throw new ElementNotFound();
			}
			catch (CannotObtainLock ex1)
			{
				throw ex1;
			}
			catch (ElementNotFound ex2)
			{
				throw ex2;
			}

			/*
			 * Recursively traverse the element's sub-elements, and assemble a
			 * serializable copy of the element tree.
			 */

			element = element.getModelElementTree();

			transaction.commit();
		}
		catch (Exception ex)
		{
			transsaction.abort();

			throw ex;		// re-throw.
		}
		catch (Throwable t)		// Top-level exception firewall.
		{
			transsaction.abort();

			InternalEngineFailure ex = new InternalEngineFailure(t);
			er.printStackTrace();
			throw ex;
		}

		return element;
	}

	/**
	 * Recursively build XML tree representing element.
	 * Return null if elt is null.
	 */

	protected org.jdom.Element buildTreeAsXML(expressway.Element elt)
	{
		if (elt == null) return null;

		org.jdom.Element xmlNode = new org.jdom.Element();
		xmlNode.setAttribute("Name", elt.getName());

		org.jdom.Element x = xmlNode;
		for each child c of elt in database db,
		{
			buildTreeAsXML(c, new org.jdom.Element());
			x.addContent(c);
		}

		return xmlNode;
	}

	/**
	 * Locate the specified model in the database, and update the
	 * specified attribute with the specified new value.
	 */

	protected void updateAttribute(elementName, attributeName, value)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		InternalEngineFailure
	{
		....
	}
}
