

/*

	expr =
					{term} term |
					{plus} expr plus term |
					{minus} expr minus term ;
		
	term =
					{factor} factor |
					{mult} term star factor |
					{div} term slash factor ;
	factor =
					{identifier} id |
					{int} integer_literal |
					{float} floating_point_literal ;


*/


Object interp()
throws
	ModelContainsError
{
	getNextToken();
	Expr expr = interpretExpr();
}



boolean interpretExpr()
throws
	ModelContainsError
{
	if (currentToken().kind is NOT in FIRST(expr))
		return false;
	
	
	// For each kind of expr.
	if (interpretPlusExpr()) return true;
	if (interpretMinusExpr()) return true;
	if (interpretTermExpr()) return true;
			
	return false;
}


boolean interpretPlusExpr()
throws
	ModelContainsError
{
	if (interpretExpr())
}


interpretTerm()
{
}


interpretFactor()
{
}






	/*
	
	a + b * c + d * e
	
	get a: push
	get +:
	get b: push
	get *:
	get c: push
	get +
	pop, push b * c
	
	get opnd, push opnd
	repeat
		if more input,
			get op, get opnd
			
			if op is lower or equal precedence than op on stack,
				pop op, pop opnd, push (popped_opnd popped_op opnd)
			else
				pop opnd, push (popped_opnd op opnd)
		else
			pop opnd
			if (op stack is empty)
				return opnd stack  // done
			else
				pop op
				pop opnd
				push (popped_opnd popped_op opnd)
	
	*/
	
	
	
	
	
