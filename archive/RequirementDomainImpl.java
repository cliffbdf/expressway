/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.server.RequirementElement.*;
import expressway.ser.RequirementDomainSer;
import expressway.server.NamedReference.*;
import java.net.URL;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Set;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


public class RequirementDomainImpl extends HierarchyDomainBase
	implements RequirementDomain
{
	private String htmlSummary;
	
	
  /* Constructors */
  
  
	protected RequirementDomainImpl(String baseName)
	throws
		ParameterError
	{
		super(baseName);
		
		// <Requirement> (AddressesRequirement) <Strategy>
		createNamedReference(
			StandardNamedReferences.AddressesRequirement, 
			StandardNamedReferences.AddressesRequirementDesc,
			CrossReferenceable.class, Requirement.class);
	}
	
	
  /* From HierarchyElementBase. Extend as needed. */


	protected String getTextToRender()
	{
		return super.getTextToRender();
	}
	
	
	protected String getTagName() { return "requirement"; }
	
	
	protected void writeSpecializedXMLAttributes(PrintWriter writer)
	{
		writer.print(getHTMLSummary() == null ? "" :
			("summary=\"" + getHTMLSummary().replace("\"", "\\\"") + "\" "));
	}
	
	
	protected void writeSpecializedXMLElements(PrintWriter writer, int indentation) {} // ok
	
	
  /* From HierarchyBase */


	protected Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		return new RequirementImpl(name, this);
	}

	
  /* From RequirementElement */
	
	
	public RequirementDomain getRequirementDomain() { return this; }	
	
	
	public RequirementAttribute createRequirementAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (RequirementAttribute)(createAttribute(name, defaultValue, layoutBound,
			outermostAffectedRef));
	}
	
	
	public RequirementAttribute createRequirementAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (RequirementAttribute)(createAttribute(layoutBound,
			outermostAffectedRef));
	}


	public RequirementAttribute createRequirementAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (RequirementAttribute)(createAttribute(name, layoutBound,
			outermostAffectedRef));
	}
	

  /* From Requirement */
  
  
	public int getNoOfRequirements()
	{
		return getNoOfHierarchies();
	}
	
	
	public Requirement createSubRequirement(String name, int position, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (Requirement)(createSubHierarchy(name, position, layoutBound, outermostAffectedRef));
	}
		
		
	public Requirement createSubRequirement(String name, int position, 
		Scenario scenario, RequirementTemplate template)
	throws
		ParameterError
	{
		return (Requirement)(createSubHierarchy(name, position, scenario, template));
	}
		
		
	public Requirement createSubRequirement(String baseName, int position,
		Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return (Requirement)(createSubHierarchy(baseName, position, scenario, copiedNode));
	}


	public Requirement createSubRequirement(String baseName, int position)
	throws
		ParameterError
	{
		return (Requirement)(createSubHierarchy(baseName, position));
	}


	public Requirement createSubRequirement(String baseName)
	throws
		ParameterError
	{
		return (Requirement)(createSubHierarchy(baseName));
	}


	/*public Requirement createSubRequirement(String baseName, Template template)
	throws
		ParameterError
	{
		return (Requirement)(createSubHierarchy(baseName, template));
	}*/
	
	
	public Requirement getParentRequirement()
	{
		return(Requirement)(getParentHierarchy());
	}


	public void setParentRequirement(Requirement parent)
	throws
		ParameterError
	{
		setParentHierarchy(parent);
	}
	
	
	public List<Requirement> getSubRequirements()
	{
		List<Requirement> rlist = new Vector<Requirement>();
		List<Hierarchy> hlist = getSubHierarchies();
		for (Hierarchy h : hlist)
			rlist.add((Requirement)h);
		return rlist;
	}
	
	
	public Requirement getSubRequirement(String name)
	throws
		ElementNotFound
	{
		return (Requirement)(getSubHierarchy(name));
	}
	
	
	public String getHTMLSummary()
	{
		return htmlSummary;
	}
	
	
	public void setHTMLSummary(String summary)
	{
		this.htmlSummary = summary;
	}


  /* From PersistentNodeImpl */
	
	
	protected Class getSerClass() { return RequirementDomainSer.class; }
	
	
	protected NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		synchronized (this.getDomain())
		{
			RequirementDomainSer ser = (RequirementDomainSer)nodeSer;
			
			if (ser.parentNodeId != null) throw new RuntimeException("Parent Id is " + ser.parentNodeId);

			return super.externalize(ser);
		}
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		RequirementDomainImpl newInstance =
			(RequirementDomainImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}

	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	protected void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.RequirementIconImageName);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		 { return new TreeSet<PersistentNode>(); }
	}
	
	
	public double getPreferredWidthInPixels() { return 50; }
	

	public double getPreferredHeightInPixels() { return 0; }
		
		
	public Attribute constructAttribute(String name, Serializable defaultValue)
	{
		return new RequirementAttributeImpl(name, this, defaultValue);
	}
	
	
	public Attribute constructAttribute(String name)
	{
		return new RequirementAttributeImpl(name, this);
	}
	
	
  /* From Domain */
  
	
	public String getDefaultViewTypeName() { return "expressway.gui.rqmts.Requirement"; }
}

