package expressway.generalpurpose;

import java.lang.reflect.*;


public class ClassUtils
{
	public static Field[] getAllNonStaticFields(Class c)
	{
		Field[] fields = c.getDeclaredFields();
		
		// For each parent class as well.
		
		Class parentClass = c.getSuperclass();
		
		if (parentClass != null)
		{
			Field[] parentFields = getAllNonStaticFields(parentClass);
			
			Field[] combinedFields = new Field[fields.length + parentFields.length];
			
			for (int i = 0; i < fields.length; i++) combinedFields[i] = fields[i];
			for (int i = 0; i < parentFields.length; i++)
				combinedFields[i+fields.length] = parentFields[i];
			
			fields = combinedFields;
		}
		
		return fields;
	}
	
	
	public static Field[] getAllNonStaticFields(Object obj)
	{
		return getAllNonStaticFields(obj.getClass());
	}
	
	
	public static <T extends S> void copyFields(S source, T target)
	{
		Field[] fields = getAllNonStaticFields(source);
		
		for (Field f : fields)
		{
			int mods = f.getModifiers();
			if (Modifier.isStatic(mods)) continue;
			
			
			// Copy the source value to the target.
			
			Object value = f.get(source);
			f.set(target, value);
		}
		
	}
}

