/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ModelAPITypes.*;



/** ****************************************************************************
 * Provides the coordinate transformation logic that is applicable to most Views.
 * Views call these methods to implement their own transformation methods.
 */
 
public class DefaultViewTransformationHelper
{
	public static int transformNodeXCoordToView(double modelXCoord, double pixelXSize,
		int xOriginOffset)
	throws
		ParameterError
	{
		double d = modelXCoord / pixelXSize + (double)xOriginOffset;
		//double d = pixelXSize * modelXCoord  + (double)xOriginOffset;
		
		if ((d > Integer.MAX_VALUE)
			|| (d < Integer.MIN_VALUE)) throw new ParameterError(
				d + " is out of range of the display space");
				
		return (int)d;
	}
	
	
	public static int transformNodeYCoordToView(double modelYCoord, double pixelYSize,
		int yOriginOffset)
	throws
		ParameterError
	{
		double d = - modelYCoord / pixelYSize + (double)yOriginOffset;
		//double d = pixelYSize * modelYCoord  + (double)yOriginOffset;

		if ((d > Integer.MAX_VALUE)
			|| (d < Integer.MIN_VALUE)) throw new ParameterError(
				d + " is out of range of the display space");
				
		return (int)d;
	}
	
	
	public static int transformNodeWidthToView(double nodeWidth, double pixelXSize)
	throws
		ParameterError
	{
		double d = nodeWidth / pixelXSize;
		//double d = pixelXSize * nodeWidth;
		
		if ((d > Integer.MAX_VALUE)
			|| (d < Integer.MIN_VALUE)) throw new ParameterError(
				d + " is out of range of the display space");
				
		return (int)d;
	}
		
		
	public static int transformNodeHeightToView(double nodeHeight, double pixelYSize)
	throws
		ParameterError
	{
		double d = nodeHeight / pixelYSize;
		//double d = pixelYSize * nodeHeight;
		
		if ((d > Integer.MAX_VALUE)
			|| (d < Integer.MIN_VALUE)) throw new ParameterError(
				d + " is out of range of the display space");
				
		return (int)d;
	}


	public static double transformViewXCoordToNode(int viewXCoord, double pixelXSize,
		int xOriginOffset)
	throws
		ParameterError
	{
		return ((double)viewXCoord - (double)xOriginOffset) * pixelXSize;
	}
	
	
	public static double transformViewYCoordToNode(int viewYCoord, double pixelYSize,
		int yOriginOffset)
	throws
		ParameterError
	{
		return - ((double)viewYCoord - (double)yOriginOffset) * pixelYSize;
	}
		
		
	public static double transformViewWidthToNode(int viewWidth, double pixelXSize)
	throws
		ParameterError
	{
		return (double)viewWidth * pixelXSize;
	}
		
		
	public static double transformViewHeightToNode(int viewHeight, double pixelYSize)
	throws
		ParameterError
	{
		return (double)viewHeight * pixelYSize;
	}
}

