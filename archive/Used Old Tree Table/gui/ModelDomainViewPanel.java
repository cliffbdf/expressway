/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.*;
import expressway.ModelElement.*;
import expressway.ClientModel.*;
import expressway.ModelAPITypes.*;
import expressway.Constants;
import expressway.VisualComponent.*;
import expressway.ser.ActivitySer;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.Vector;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Collection;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;



/** ****************************************************************************
 * Implements a View of a Model Domain. Each visual component within this JPanel
 * is a VisualJComponent representing a Model Element in this JPanel's Model Domain.
 */
 
public class ModelDomainViewPanel extends JPanel implements View
{
  /* ***************************************************************************
	 ***************************************************************************
	 * Initiatlization permanent state.
	 */
	
	private final JTabbedPane container;
	private final String modelDomainName;
	private final ModelEngine modelEngine;
	private final PeerListener peerListener;
	private final ListenerRegistrar listenerRegistrar;
	private final MouseListener popupListener;
	private final ViewEvidenceActionListener viewEvidenceActionListener;
	private final JPopupMenu activityPopup;
	
	
  /* ***************************************************************************
	 * Transient state:
	 * Transformation parameters for this view. These describe the view, relative
	 * to the model that this View depicts. Note that we do not have any plans
	 * to handle rotation or anything other than a linear transformation.
	 */
	
	/** The x-dimension size of a display space pixel in the real-valued space
	 of this View. */
	private double pixelXSize = 1.0;
	
	
	/** The y-dimension size of a display space pixel in the real-valued space
	 of this View. */
	private double pixelYSize = 1.0;
	
	
	/** Display space x coordinate of this JPanel's origin. */
	private int xOriginOffset = 0;
	
	
	/** Display space y coordinate of this JPanel's origin. Note that in the
	 display space, the origin is at the top left of the screen, and the positive
	 y direction is downward. */
	private int yOriginOffset = 0;
	
	
	
  /* ***************************************************************************
	 ***************************************************************************
	 * Constructors.
	 */
	 
	 
	/** ************************************************************************
     * Constructor.
     */
	 
	public ModelDomainViewPanel(JTabbedPane container, String modelDomainName,
		ModelEngine modelEngine)
	throws
		ParameterError,
		IOException
	{
		this.container = container;
		this.modelDomainName = modelDomainName;
		this.modelEngine = modelEngine;
		
		
		// A View owns a PeerListener for all of the Persistent Nodes that it depicts.
		
		peerListener = new ModelDomainPeerListenerImpl();
		listenerRegistrar = modelEngine.registerPeerListener(peerListener);

		
		// Create GUI action handlers.
		
		this.popupListener = new PopupListener();
		this.viewEvidenceActionListener = new ViewEvidenceActionListener();


		// Create a popup menu item and add the action "View Evidence" action
		// handler to it.
		
		JMenuItem menuItem = new JMenuItem("View Evidence");
		menuItem.addActionListener(viewEvidenceActionListener);

		// Create a popup menu to contain the menu item.
		
		this.activityPopup = new JPopupMenu();
		this.activityPopup.add(menuItem);	
	}
	
	
  /* ***************************************************************************
	 ***************************************************************************
	 * Public methods..
	 */
	 
	 
	/** ************************************************************************
	 * Obtain from the server the top-level Set of PersistentNodes that comprise
	 * the Model Domain, and add each one as a Visual Component. Do not populate
	 * the Visual Components themselves.
	 */
	 
	public void shallowPopulate()
	throws
		Exception
	{
		removeAll();
		
		try
		{
			String domainId = modelEngine.resolveNodePath(modelDomainName);
			System.out.println("Id of " + modelDomainName + " is " + domainId);
			
			VisualJComponent visual = (VisualJComponent)(addNode(domainId, null));
			visual.populateChildren();
		}
		catch (Exception ex)
		{
			for (Throwable cause = ex; cause != null; cause = cause.getCause())
			{
				System.out.println("Cause: " + cause.getClass().getName() + ": " +
					cause.getMessage());
			}
			
			throw ex;
		}

		
		// Make visible.
		
		setVisible(true);
		validate();
	}
	
	
  /* ***************************************************************************
	 ***************************************************************************
	 * Package-scope methods..
	 */
	 
	 
	/** ************************************************************************
	 * Retreive the specified Node from the server and create a new Visual for
	 * it, as a child of the specified parent Visual. Subscribe to events
	 * pertaining to the Node.
	 * Add to each Visual a mouse handler to allow the user to select the Visual
	 * and choose to display other kinds of Views of the associated Node.
	 * Populate the Visual with its children, displaying them as icons, but do
	 * not populate the children.
	 */
	 
	public VisualComponent addNode(String nodeId, VisualComponent parent)
	throws
		Exception
	{
		System.out.println("Getting child node " + nodeId);
		NodeSer child = modelEngine.getNode(nodeId);
		System.out.println("Child Node: " + child.getName());

		// Create a client-side Visual to represent the server-side Node, adding
		// the Visual to this Panel and View.
		
		Container p = null;
		if (parent != null)
		{
			if (! (parent instanceof VisualJComponent)) throw new RuntimeException(
				"Expected a VisualJComponent; parent is " + parent.getClass().getName());
			
			p = (Container)parent;
		}
		
		if (p == null) p = this;
		
		VisualJComponent visual = VisualJComponent.makeVisual(child, this,
			p, false);		

		// Subscribe to receive update notifications for the child.
		// Note that this does NOT result in additional server connections.

		listenerRegistrar.subscribe(nodeId);
		
		
		// Register a mouse handler with the Activity.
		// If the Node is a Transformation Activity, the mouse handler
		// shows the "View Evidence" popup.
		
		visual.addMouseListener(popupListener);
		
		
		return visual;
	}
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Methods defined in View interface.
	 */
	 
	 
	public ModelEngine getModelEngine()
	{
		return this.modelEngine;
	}


	public Set<VisualComponent> identifyVisualComponents(String nodeId)
	{
		Set<VisualComponent> visualComponents = new TreeSet<VisualComponent>();
		
		Component[] components = getComponents();
		for (Component component : components)
			// each top-level visual component of this panel
		{
			if (! (component instanceof VisualJComponent)) continue;
			
			((VisualJComponent)component).identifyVisualComponents(nodeId, visualComponents);
		}
		
		return visualComponents;
	}
	
	
	public String identifyNodeId(VisualComponent visual)
	{
		// Look through all of this Panel's Components. Find the child that matches
		// the specified Visual. There should not be more than one.
		
		Component[] components = getComponents();
		for (Component component : components)
			// each top-level visual component of this panel
		{
			if (! (component instanceof VisualJComponent)) continue;
			
			if (component == visual) return ((VisualJComponent)component).getNodeId();
			
			// Recursively check sub-components.
			
			String id = ((VisualJComponent)component).identifyNodeId(visual);
			if (id != null) return id;
		}
		
		return null;  // not found.
	}
	
	
	public int transformNodeXCoordToView(double modelXCoord)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeXCoordToView(modelXCoord,
			pixelXSize, xOriginOffset);
	}
	
	
	public int transformNodeYCoordToView(double modelYCoord)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeYCoordToView(modelYCoord,
			pixelYSize, yOriginOffset);
	}
	
	
	public int transformNodeWidthToView(double nodeWidth)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeWidthToView(nodeWidth,
			pixelXSize);
	}
		
		
	public int transformNodeHeightToView(double nodeHeight)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeWidthToView(nodeHeight,
			pixelYSize);
	}


	public double transformViewXCoordToNode(int viewXCoord)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewXCoordToNode(viewXCoord,
			pixelXSize, xOriginOffset);
	}
	
	
	public double transformViewYCoordToNode(int viewYCoord)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewYCoordToNode(viewYCoord,
			pixelYSize, yOriginOffset);
	}
		
		
	public double transformViewWidthToNode(int viewWidth)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewWidthToNode(viewWidth,
			pixelXSize);
	}
		
		
	public double transformViewHeightToNode(int viewHeight)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewHeightToNode(viewHeight,
			pixelYSize);
	}



  /* ***************************************************************************
	 * *************************************************************************
	 * Inner classes.
	 */
	 
	 
	/** ************************************************************************
	 * Handler for right-click on a Transformation Activity. Displays a
	 * popup menu that allows the user to select "View Evidence", which then
	 * opens an Activity Evidence View.
	 */
	 
	class PopupListener extends MouseInputAdapter
	{
		public void mouseReleased(MouseEvent e)
		{
			System.out.println("Entered mouseReleased");
			
			if ((e.getButton() & (MouseEvent.BUTTON2 | MouseEvent.BUTTON3)) != 0)
				maybeShowPopup(e);
		}
	
		protected void maybeShowPopup(MouseEvent e)
		{
			System.out.println("Entered maybeShowPopup");
			
			Component source = e.getComponent();
			System.out.println("source is a " + source.getClass().getName());
			
			if (! (source instanceof ActivityVisual)) return;

			System.out.println("source is instance of ActivityVisual");
			
			if (! (source instanceof ActivityVisualJ)) throw new RuntimeException(
				"Incompatible implementation of an ActivityVisual: must be an ActivityVisualJ");
			
			System.out.println("source is instance of ActivityVisualJ");
			
			ActivityVisualJ activity = (ActivityVisualJ)source;
			
			try
			{
				if (activity.hasAttribute("Transformation"))
				{
					System.out.println("has Transformation attribute");
					
					activityPopup.show(activity, e.getX(), e.getY());
				}
			}
			catch (Exception ex)
			{
				activity.setInconsistent(true);
				
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ex.getMessage(),
					"Error",
					JOptionPane.ERROR_MESSAGE); return;
			}
		}
	}
	
	
	/** ************************************************************************
	 * Opens a new Activity Evidence View Panel. This handler is invoked after
	 * it a GUI action has been interpreted to mean that the user wants an
	 * Activity Evidence View Panel to open for a user-selected Activity,
	 * and that the selected Activity is a Transformation Activity.
	 */
	 
	class ViewEvidenceActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			System.out.println("responding to " + e.getClass().getName() + "...");
			
			
			// Identify the Activity that the user selected.
			
			Object source = e.getSource();
			System.out.println("...source is a " + source.getClass().getName());
			if (! (source instanceof JMenuItem)) return;
			
			Component component = ((JMenuItem)source).getComponent();
			System.out.println("...component is " + component.getClass().getName());
			
			Component parent = component.getParent();
			System.out.println("...parent is a " + parent.getClass().getName());
			
			Component originator = ((JPopupMenu)parent).getInvoker();
			System.out.println("...originator is a " + originator.getClass().getName());
			
			if (! (originator instanceof ActivityVisualJ)) return;
			ActivityVisualJ activityVisual = (ActivityVisualJ)originator;
			NodeSer nodeSer = activityVisual.getNodeSer();
			ActivitySer activity = (ActivitySer)nodeSer;
			
			
			// Create a new Activity Evidence View Panel for the specified Activity.
			
			System.out.println("...creating Risk panel...");
			
			
			ActivityRiskMitTreeViewPanel panel = null;
			try
			{
				panel =
					new ActivityRiskMitTreeViewPanel(container, activity, modelEngine);

				panel.shallowPopulate();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ex.getMessage(),
					"While trying to create a View for Activity " + activity.getName(),
					JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			
			// Add the new Panel to the Frame's MainTabbedPanel.
			
			System.out.println("...adding risk panel as a tab...");
			
			
			container.addTab("Activity Evidence", panel);
			container.invalidate();
			container.validate();
			System.out.println("...done adding risk panel.");
			
			
		}
	}
	
	
	/** ************************************************************************
	 * The Peer Listener for this View. (See design slide "PeerListener Pattern".)
	 * Listens for notifcation messages from a ModelEngine, and relays those messages
	 * to the visual counterparts of the server components identified in the messages.
	 */
	 
	public class ModelDomainPeerListenerImpl extends UnicastRemoteObject implements PeerListener
	{
		ModelDomainPeerListenerImpl() throws RemoteException { super(); }
		
		
		public void notify(PeerNotice notice)
		throws
			IOException
		{
			Set<VisualComponent> visComps =
				identifyVisualComponents(notice.getPeerId());
			
			// Relay notification to Visual Components.
			for (VisualComponent visComp : visComps) try
			{
				notice.notify(visComp);
			}
			catch (InconsistencyError ie)
			{
				ie.printStackTrace();
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ie.getMessage(),
					"Check " + visComp.getName(),
					JOptionPane.ERROR_MESSAGE);
			}
			catch (ClassCastException cce)
			{
				cce.printStackTrace();
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					"Server sent a represh notice for a visual component and it " +
					"was the wrong type; " + 
					cce.getMessage(),
					"Internal server error",
					JOptionPane.ERROR_MESSAGE);
			}
			catch (Throwable t)
			{
				t.printStackTrace();
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					t.getMessage(),
					"Error while processing message sent from server",
					JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}

