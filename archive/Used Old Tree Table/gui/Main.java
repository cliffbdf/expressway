/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.*;
import expressway.ModelElement.*;
import expressway.ClientModel.*;
import expressway.ModelAPITypes.*;
import expressway.Constants;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.Vector;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Collection;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;


/**
 * Main class for the GUI. Instantiates the top-level GUI components
 * and exits.
 */

public class Main
{
	/**
	 * Main method for the Expressway GUI. Instantiate the top-level GUI components
	 * and exit.
	 */

	public static void main(String[] args)
	{
		// Instantiate the Main Frame.
		
		new MainFrame();
	}
}


/**
 * The main Frame of the Expressway GUI application.
 */
 
class MainFrame extends JFrame
{
	private MainTabbedPanel mainTabbedPanel = null;
	private boolean exitCleanupPhaseCompleted = false;
	
	
	MainFrame()
	{
		super("Expressway");
		
		//setIconImage(....image);
		
		
		// Set initial position and size.
		
		setLocation(0, 0);
		setSize(1000, 600);
		
		
		// Instantiate sub-components. (They are actually added to the JFrame's
		// conent panel. See documentation for JFrame.)
		
		mainTabbedPanel = new MainTabbedPanel(this);
		getContentPane().add(mainTabbedPanel, BorderLayout.CENTER);
		
		
		// Re-position sub-components: The main tabbed panel.
		
		//pack();
		

		// Make visible.
		
		setVisible(true);
	}
	
	
	/**
	 * To be called by this class or any other to close the application gracefully.
	 */
	 
	synchronized void closeMainWindowAndApplication()
	{
		if (! exitAll()) return;
		setVisible(false);
		dispose();
	}
	
	public synchronized void windowClosing(WindowEvent e)
	{
		if (! exitAll()) return;
		setVisible(false);
		dispose();
	}
	
	
	/**
	 * Close open files and stop any activities in which the application is
	 * involved. This might result in asking the user for permission to abort
	 * some activities. If permission is not given, then the exit is aborted.
	 * Return true if successfully completed.
	 */
	 
	protected synchronized boolean exitAll()
	{
		if (exitCleanupPhaseCompleted) return true;
		exitCleanupPhaseCompleted = true;
		return true;
	}
}


/**
 * The main tabbed panel for the Expressway GUI application.
 */
 
class MainTabbedPanel extends JTabbedPane
{
	private final JFrame frame;
	private DomainListPanel domainListPanel = null;
	
	
	MainTabbedPanel(JFrame frame)
	{
		this.frame = frame;
		
		// Instantiate sub-components.
		
		domainListPanel = new DomainListPanel(this);
		addTab("Domains", domainListPanel);
		
		
		// Re-position sub-components.
		
		validate();

		// Make visible.

		setVisible(true);
	}
	
	
	JFrame getFrame() { return frame; }
}


/**
 * Provide indirection for classes that use the ModelEngine, so that if the
 * connection to the ModelEngine is re-established, the actual client stub
 * can be replaced without impacting other classes that use the ModelEngine.
 *
 
class ModelEngineGateway implements ModelEngine
{
	private ModelEngine modelEngine = null;
	
	
	synchronized void setModelEngine(ModelEngine modelEngine);
	
	synchronized ModelEngine getModelEngine()
	{
		return modelEngine;
	}
}
*/

