/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.*;
import expressway.ModelElement.*;
import expressway.ClientModel.*;
import expressway.ModelAPITypes.*;
import expressway.Constants;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.Vector;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Collection;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;



/**
 * A Panel depicting a list of Domains that are available from a Model Engine
 * that is specified by the user via a JTextField.
 */
 
public class DomainListPanel extends JPanel
{
	private final JTabbedPane mainTabbedPanel;
	private JTextField modelEngineNetworkPathField = null;
	private JButton reconnectButton = null;
	private JTree domainTree = null;
	private DomainListModel treeModel = null;
	private JPanel topPanel = null;
	private JPanel bottomPanel = null;
	
	private ModelEngine modelEngine = null;
	
	
	DomainListPanel(final JTabbedPane mainTabbedPanel)
	{
		this.mainTabbedPanel = mainTabbedPanel;
		
		/*
		 * Set initial position and size:
		
			Instantiate sub-components:
			1. The text input field specifying network path to the ModelEngine
					server.
			2. A "Connect to server" button.
			3. the tree of Domains and their Scenarios.
		 */
		
		setLayout(new BorderLayout());
		add(topPanel = new JPanel(), BorderLayout.NORTH);
		add(bottomPanel = new JPanel(), BorderLayout.CENTER);
		
		topPanel.add(new JLabel("Model Engine Host DNS Path: "));
		topPanel.add(modelEngineNetworkPathField = new JTextField());
		modelEngineNetworkPathField.setColumns(40);
		topPanel.add(reconnectButton = new JButton("Connect"));
		
		reconnectButton.addActionListener(new ActionListener()
		{
			public synchronized void actionPerformed(ActionEvent e)
			{
				try // to Connect to ModelEngine
				{
					String hostName = modelEngineNetworkPathField.getText();
					System.out.println("Connecting to Registry on " + hostName + "...");
					Registry registry = LocateRegistry.getRegistry(hostName);
					
					System.out.println("...Registry found; connecting to Model Engine...");
					Remote remote = registry.lookup(Constants.RMIServiceName);
					
					modelEngine = (ModelEngine)remote;
					System.out.println("Connected to Model Engine.");
				}
				catch (Throwable t)
				{
					t.printStackTrace();
					
					JOptionPane.showMessageDialog(DomainListPanel.this,
						t.getMessage(),
						"While connecting to Registry or Model Engine",
						JOptionPane.ERROR_MESSAGE); return;
				}
				
				
				try // to retrieve and display the Domains available from the server.
				{
					// Retrieve the list of domains available from the Model Engine.
					
					List<String> domainNames = modelEngine.getDomainNames();
					System.out.println("Obtained List of " + domainNames.size() +
						" Domain names:");
					for (String dName : domainNames)
						System.out.println("\t" + dName);
					

					// Create a new domain tree GUI model.
					
					treeModel = new DomainListModel(domainNames, DomainListPanel.this, modelEngine);
					//RowMapper rm = treeModel.getRowMapper();
					//System.out.println("Row Mapper = " + (rm == null ? "null" : rm.toString()));
					

					// Remove the old tree.
					
					if (domainTree != null)
					{
						bottomPanel.remove(domainTree);
						domainTree = null;
					}


					// Create a new tree.
					
					domainTree = new JTree(treeModel);
					treeModel.setRowMapper(new RowMapper()
					{
						public int[] getRowsForPaths(TreePath[] paths)
						{
							int[] rows = new int[paths.length];
							int rowNo = 0;
							for (TreePath path : paths)
							{
								rows[rowNo++] = domainTree.getRowForPath(path);
							}
							
							return rows;
						}
					});


					// Create a listener to respond to GUI leaf double-click Events.

					MouseListener ml = new MouseAdapter()
					{
						public void mousePressed(MouseEvent e)
						{
							System.out.print("Selection: ");
							
							int selRow = domainTree.getRowForLocation(e.getX(), e.getY());
							TreePath path = domainTree.getPathForLocation(e.getX(), e.getY());
							
							//if ((selRow != -1) && (e.getClickCount() == 2)) try
							if ((path != null) && (e.getClickCount() == 2)) try
							{
								System.out.println("Path is " + path.toString());
								
								Object[] pathParts = path.getPath();
								
								if (pathParts.length != 2) throw new RuntimeException(
									"Domain path does not have two TreePath parts.");
								if (! (pathParts[1] instanceof String))
									throw new RuntimeException(
										"Domain path is not a String: " +
											(pathParts[1]).getClass().getName());
								
								String domainName = (String)(pathParts[1]);
								System.out.println("\t" + domainName);
								
								
								// Create a View for the selected Domain.
								
								ModelDomainViewPanel modelDomainViewPanel =
									new ModelDomainViewPanel(
										mainTabbedPanel, domainName, modelEngine);
					
								// Call shallowPopulate() on the View and register
								// a Peer Listener. Add the new ModelDomainViewPanel
								// to as a new tab in the MainTabbedPanel.
								
								//modelDomainViewPanel.deepPopulate();
								
								modelDomainViewPanel.shallowPopulate();
								System.out.println("Returned from shallowPopulate.");
								
								
								// Add the View as a new tabbed pane.
								
								mainTabbedPanel.addTab(domainName, modelDomainViewPanel);
								mainTabbedPanel.invalidate();
								mainTabbedPanel.validate();
							}
							catch (Exception ex)
							{
								ex.printStackTrace();
								
								JOptionPane.showMessageDialog(DomainListPanel.this,
									ex.getMessage(),
									"Error",
									JOptionPane.ERROR_MESSAGE); return;
							}
						}
					};
					
					domainTree.addMouseListener(ml);


					// Show the new tree.
					
					bottomPanel.add(domainTree);
					domainTree.show();
					bottomPanel.invalidate();
					bottomPanel.validate();
					invalidate();
					validate();
					System.out.println("Done rendering Tree Model of Domain names");
				}
				catch (Throwable t)
				{
					t.printStackTrace();
					
					JOptionPane.showMessageDialog(DomainListPanel.this,
						t.getMessage(),
						"Error",
						JOptionPane.ERROR_MESSAGE); return;
				}
			}
		});
		
		
		// Re-position sub-components.
		
		validate();
		

		// Make visible.
		
		setVisible(true);
	}
}


/**
 * A Swing Tree Model for representing a list of Domains.
 */
 
class DomainListModel
	extends DefaultTreeSelectionModel
	implements TreeModel
{
	private List<String> listOfAllDomains = null;
	
	private Vector<TreeModelListener> treeModelListeners =
		new Vector<TreeModelListener>();


	DomainListModel(Collection domainNames, Component component, ModelEngine modelEngine)
	{
		listOfAllDomains = new Vector(domainNames);
	}
	
	
	public synchronized void addTreeModelListener(TreeModelListener l)
	{
		treeModelListeners.addElement(l);
	}
	
	
	public synchronized Object getChild(Object parent, int index)
	{
		if (parent == listOfAllDomains) return listOfAllDomains.get(index);
		
		return null;
	}
	
	
	public synchronized int getChildCount(Object parent)
	{
		if (parent == listOfAllDomains) return listOfAllDomains.size();
		
		return 0;
	}
	
	
	public synchronized int getIndexOfChild(Object parent, Object child)
	{
		if (parent == listOfAllDomains) return listOfAllDomains.indexOf(child);
		
		return -1;
	}
	
	
	public synchronized Object getRoot()
	{
		return listOfAllDomains;
	}
	
	
	public synchronized boolean isLeaf(Object node)
	{
		if (node == listOfAllDomains) return false;
		
		if (listOfAllDomains.contains(node)) return true;
		
		return false;
	}
	
	
	public synchronized void removeTreeModelListener(TreeModelListener l)
	{
		treeModelListeners.removeElement(l);
	}
	
	
	public synchronized void valueForPathChanged(TreePath path, Object newValue)
	{
		if (! newValue.equals(path.getLastPathComponent()))
		{
			for (TreeModelListener listener : treeModelListeners)
			{
				listener.treeNodesChanged(new TreeModelEvent(this, path));
			}
		}
	}
}

