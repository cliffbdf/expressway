package expressway.ser;


import expressway.common.*;
import java.net.URL;


public class ClassificationDomainSer extends ClassificationSer
	implements HierarchyDomainSer, NodeDomainSer
{
	public String sourceName;
	public URL sourceURL;
	public String declaredName;
	public String declaredVersion;
	public String viewClassName;
	public String[] namedReferenceNames;

	public String getNodeKind() { return NodeKindNames.ClassificationDomain; }
	public String getSourceName() { return sourceName; }
	public URL getSourceURL() { return sourceURL; }
	public String getDeclaredName() { return declaredName; }
	public String getDeclaredVersion() { return declaredVersion; }
	public String getViewClassName() { return viewClassName; }
	public String[] getNamedReferenceNames() { return namedReferenceNames; }
	public void setNamedReferenceNames(String[] names) { this.namedReferenceNames = names; }

	public void setSourceName(String sourceName) { this.sourceName = sourceName; }
	public void setSourceURL(URL sourceURL) { this.sourceURL = sourceURL; }
	public void setDeclaredName(String declaredName) { this.declaredName = declaredName; }
	public void setDeclaredVersion(String declaredVersion) { this.declaredVersion = declaredVersion; }
	public void setViewClassName(String name) { this.viewClassName = name; }
}

