/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ClassificationElement.*;
import expressway.ser.*;
import expressway.server.Event.*;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.SortedSet;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


public class ClassificationAttributeImpl extends HierarchyAttributeBase
	implements ClassificationAttribute, Cloneable
{
  /* Constructors */
  
	
	ClassificationAttributeImpl(String baseName, ClassificationElement parent)
	{
		super(baseName, parent);
	}
	
	
	ClassificationAttributeImpl(String baseName, ClassificationElement parent,
		Serializable defaultValue)
	{
		super(baseName, parent, defaultValue);
	}
	
	
  /* From HierarchyElementBase */


	protected void writeSpecializedXMLAttributes(PrintWriter writer) {}	
	
	
	protected void writeSpecializedXMLElements(PrintWriter writer, int indentation) {}
	
	
  /* From PersistentNodeImpl */
	
	
	protected Class getSerClass() { return ClassificationAttributeSer.class; }
	
	
	protected NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		synchronized (this.getDomain())
		{
			ClassificationAttributeSer ser = (ClassificationAttributeSer)nodeSer;
			return super.externalize(ser);
		}
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		ClassificationAttributeImpl newInstance =
			(ClassificationAttributeImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}

	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		writeAsXML(writer, indentation, null);
	}

	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
  /* From Attribute */

  /* From PersistentNodeImpl */


	protected void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.AttributeIconImageName);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		 { return new TreeSet<PersistentNode>(); }
	}
	
	
	public double getPreferredWidthInPixels() { return 50; }
	

	public double getPreferredHeightInPixels() { return 0; }
	
	
	public Attribute constructAttribute(String name, Serializable defaultValue)
	throws
		ParameterError
	{
		return new ClassificationAttributeImpl(name, this, defaultValue);
	}
	

	public Attribute constructAttribute(String name)
	{
		return new ClassificationAttributeImpl(name, this);
	}


  /* From ClassificationElement */
	
	
	public ClassificationDomain getClassificationDomain() { return (ClassificationDomain)(getDomain()); }
	
	
	public ClassificationAttribute createClassificationAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (ClassificationAttribute)(createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef));
	}
	
	
	public ClassificationAttribute createClassificationAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (ClassificationAttribute)(createAttribute(layoutBound, outermostAffectedRef));
	}


	public ClassificationAttribute createClassificationAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (ClassificationAttribute)(createAttribute(name, layoutBound, outermostAffectedRef));
	}
}

