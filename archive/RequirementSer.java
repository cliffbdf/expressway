package expressway.ser;

import expressway.common.*;
import java.io.Serializable;
import expressway.generalpurpose.StringUtils;


public class RequirementSer extends RequirementElementSer
	implements HierarchySer, TabularNodeSer
{
	public static String[] fieldNames =
	{
		"Name",
		"HTML Summary",
		"HTML Full Description"
	};
	
	public String htmlSummary;
	public String[] subRequirementNodeIds;
	public int seqNo;

	public String getNodeKind() { return NodeKindNames.Requirement; }
	
	
  /* From HierarchySer */

	
	public void setSubHierarchyNodeIds(String[] ids) { this.subRequirementNodeIds = ids; }
	
	
	public String[] getSubHierarchyNodeIds() { return subRequirementNodeIds; }
	
	
  /* From TabularNodeSer */

	
	public String[] getFieldNames() { return fieldNames; }
	
	
	public void setValueAt(Serializable value, int column)
	{
		switch (column)
		{
		case 0: setName((String)value); break;
		case 1: htmlSummary = StringUtils.safeToStringOrNull(value); break;
		case 2: setHtmlDescription(StringUtils.safeToStringOrNull(value)); break;
		}
	}
	
	
	public Serializable getValueAt(int column)
	{
		switch (column)
		{
		case 0: return getName();
		case 1: return htmlSummary;
		case 2: return getHtmlDescription();
		}
		
		throw new RuntimeException("Unexpected column number: " + column);
	}


	public int getSeqNo() { return seqNo; }
}

