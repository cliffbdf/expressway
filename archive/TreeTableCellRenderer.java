package expressway.gui.activityrisk;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import javax.swing.tree.DefaultTreeCellRenderer;

public class TreeTableCellRenderer extends DefaultTreeCellRenderer
{
	public static final String MinusImageName = "/images/Minus.png";
	public static final String PlusImageName = "/images/Plus.png";


	public TreeTableCellRenderer()
	{
    	setOpenIcon(new ImageIcon(MinusImageName));
    	setClosedIcon(new ImageIcon(PlusImageName));
    }
	

	public Component getTreeCellRendererComponent(
		JTree tree,
		Object value,
		boolean sel,
		boolean expanded,
		boolean leaf,
		int row,
		boolean hasFocus)
	{
        super.getTreeCellRendererComponent(
			tree, value, sel, expanded, leaf, row, hasFocus);
			
        DefaultMutableTreeTableNode node = (DefaultMutableTreeTableNode)value;
        
        if(node != null && node.getUserObject() != null && 
			(node.getUserObject() instanceof ActivityTreeData))
        {
        	ActivityTreeData item = (ActivityTreeData)(node.getUserObject());
			
        	setText(item.getName());
			
         	setOpenIcon(new ImageIcon(MinusImageName));
            setClosedIcon(new ImageIcon(PlusImageName));
        }
        else
        {
        	setIcon(null);
        }
        return this;

    }
}
