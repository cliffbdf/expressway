/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package archive;

import expressway.ModelAPITypes.*;
import expressway.DecisionElement.*;

class ModelDomainVersionImpl extends ModelElementImpl implements ModelDomainVersion
{
	public ModelDomain modelDomain = null;

	public ModelDomainVersionImpl(ModelDomain modelDomain)
	{
		this.modelDomain = modelDomain;
	}
}
