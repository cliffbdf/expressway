/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.activityrisk;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.ActivityVisual;
import expressway.gui.*;
import expressway.ser.ActivitySer;
import expressway.ser.AttributeSer;
import expressway.ser.ModelAttributeSer;
import expressway.ser.ModelElementSer;
import expressway.common.Types.Activity.Transformation;
import expressway.generalpurpose.ThrowableUtil;
import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.Color;
import java.awt.Component;
import javax.swing.*;
import javax.swing.tree.*;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;






/** ************************************************************************
 * Class for the data used to depict a Transormation Activity in a Tree Table.
 * A TreeNode in a TreeTable references an instance of this class.
 * This class implements ActivityVisual, in order to be able to receive and
 * handle updates from the server (dispatched to the ActivityVisual via the
 * containing View's Peer Listener).
 */

public class ActivityTreeData extends DefaultMutableTreeTableNode implements ActivityVisual
{
  /* ***************************************************************************
	 * *************************************************************************
	 * Initiatlization permanent state.
	 */

	/** . */
	private final String nodeId;
	
	/** . */
	private final View view;
	

	private final PanelManager panelManager;
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Transient state:
	 */

	/** . */
	private ActivitySer activity;
	
	/**   */
	private ModelAttributeSer transAttribute;
	
	/** . */
	private String name;

	/** . */
	private String stdEvidence;

	/** . */
	private String criteria;

	/** . */
	private String actEvidence;

	/** . */
	private String comment;
	

	/** Indicates whether this Visual has been determined (by the server) to
	 be inconsistent (with respect to the server). */
	private boolean inconsistent;
	
	/** Indicates whether this Visual contains state that has not been
	 propagated to the server. */
	private boolean unpropagated;
	
	/** The TreeNode to which this data belongs. */
	private DefaultMutableTreeTableNode treeNode;
	
	/** Not actually used at present. */
	private ImageIcon imageIcon;
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Constructors.
	 */
	 
	
	public ActivityTreeData(View view, PanelManager panelManager, ActivitySer activity)
	{
		this.view = view;
		
		this.panelManager = panelManager;
		
		this.activity = activity;
		
		this.nodeId = activity.getNodeId();
		
		this.name = activity.getName();
	}
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Public accessors.
	 */
	 
	//public String getNodeId() { return nodeId; }
	
	
	public void setActivity(ActivitySer activity) { this.activity = activity; }
	
	public ActivitySer getActivity() { return activity; }
	
	public void setActivityName(String name) { this.name = name; }
	
	public String getActivityName() { return name; }
	
	public void setStdEvidence(String stdEvidence) { this.stdEvidence = stdEvidence; }
	
	public String getStdEvidence() { return stdEvidence; }
	
	public void setCriteria(String criteria) { this.criteria = criteria; }
	
	public String getCriteria() { return criteria; }
	
	public void setActEvidence(String actEvidence) { this.actEvidence = actEvidence; }
	
	public String getActEvidence() { return actEvidence; }
	
	public void setComment(String comment) { this.comment = comment; }
	
	public String getComment() { return comment; }
	
	public String toString() { return getName(); }
	
	public void setTreeNode(DefaultMutableTreeTableNode treeNode) { this.treeNode = treeNode; }
	
	public DefaultMutableTreeTableNode getTreeNode() { return treeNode; }


	
  /* ***************************************************************************
	 * *************************************************************************
	 * Package and protected accessors.
	 */
	
	void setTransAttribute(ModelAttributeSer transAttribute) { this.transAttribute = transAttribute; }
	
	
	protected long getLastUpdated() { return activity.getLastUpdated(); }
	
	
	protected int getVersionNumber() { return activity.getVersionNumber(); }
	

	protected void setInconsistent(boolean newInconsistent) 
	{
		//System.out.println("INCONSISTENT: for " + this.getName() +
		//	": changing from " + this.inconsistent + " to " +
		//	newInconsistent);
			
		this.inconsistent = newInconsistent;
	}
	
	
	protected boolean isInconsistent() { return inconsistent; }
	
	
	protected void setUnpropagated(boolean newUnpropagated)
	{
		this.unpropagated = newUnpropagated;
	}
	
	
	protected boolean isUnpropagated() { return unpropagated; }
	
	

  /* ***************************************************************************
	 * *************************************************************************
	 * From VisualComponent:
	 */
	 
	public String getNodeId() { return nodeId; }
	
	
	public String getName() { return name; }
	
	
	public String getFullName() { return activity.getFullName(); }


	public ImageIcon getImageIcon() { return imageIcon; }
	
	
	public void setImageIcon(ImageIcon imgIcon) { this.imageIcon = imgIcon; }
		

	public String getDescriptionPageName() { return "Risk Mitigation View"; }


	public int getSeqNo() { return 0; }
	
		 
	public boolean contains(VisualComponent comp)
	{
		int noOfChildren = treeNode.getChildCount();
		for (int i = 0; i < noOfChildren; i++)
		{
			TreeTableNode childTreeNode = treeNode.getChildAt(i);
			if (childTreeNode == null) return false;
			
			if (! (childTreeNode instanceof VisualComponent)) continue;
			if ((VisualComponent)childTreeNode == comp) return true;
			try { if (((VisualComponent)childTreeNode).contains(comp)) return true; }
			catch (VisualComponent.NoNodeException ex) {}  // continue
		}
		
		return false;
	}


	public boolean isOwnedByMotifDef()
	{
		return ((ModelElementSer)(getNodeSer())).isOwnedByMotifDef;
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		return ((ModelElementSer)(getNodeSer())).isOwnedByActualTemplateInstance;
	}

	
	public String[] getChildIds() { return new String[0]; }
	

	public void nameChangedByServer(String newName, String newFullName)
	{
		this.activity.setName(newName);
		this.activity.setFullName(newFullName);
		this.setActivityName(newName);
		//...how to make this tree node redraw itself?
	}
	
	
	public void nodeDeleted()  // the Activity was deleted on the server.
	{
		getTreeNode().removeFromParent();
	}
	
	
	public View getView() { return view; }
	
	
	public ModelEngineRMI getModelEngine() { return view.getModelEngine(); }

	
	public NodeSer getNodeSer() { return this.activity; }
	
	
	public void setNodeSer(NodeSer nodeSer) { this.activity = (ActivitySer)nodeSer; }
	
	
	public void update()
	{
		NodeSer nodeSer = null;
		
		try { nodeSer = getView().getModelEngine().getNode(getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog((Component)(this.panelManager),
					ThrowableUtil.getAllMessages(ex),
					"Error while updating Visual " + this.getName(),
					ex);
					
			ex.printStackTrace();
		}
		
		update(nodeSer);
	}
	

	public void update(NodeSer nodeSer)
	{
		if (! (nodeSer instanceof ActivitySer)) throw new RuntimeException(
			"ActivityTreeData received a non-ActivitySer to update itself.");
		
		this.setActivity((ActivitySer)nodeSer);
		try { this.refreshRedundantState(); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog((Component)(this.panelManager),
					ThrowableUtil.getAllMessages(ex),
					"Error while updating Visual " + this.getName(),
					ex);
					
			ex.printStackTrace();
		}
	}
	
	
	public void redraw() { }  // tree redraws itself automatically whenever it changes.
	
	
	public void refresh() { update(); redraw(); }
	
	
	public java.awt.Point getLocation()
	{
		throw new RuntimeException("Should not be called");
	}
	

	public void refreshLocation()
	{
	}
	
	
	public void refreshSize()
	{
	}
	
	
	public void setLocation()
	throws
		Exception
	{
	}
	
	
	public void setSize()
	throws
		Exception
	{
	}

		
	public void refreshRedundantState()
	throws
		Exception
	{
		if (transAttribute == null) throw new Exception(
			"Activity does not have a Transformation Attribute.");
		
		Serializable defValue = transAttribute.defaultValue;
		
		if (defValue == null) throw new Exception(
			"Transformation Attribute does not have a default value.");
		else if (! (defValue instanceof Transformation))
			throw new Exception("Transformation Attribute default value does not implement Transformation.");
		
		Transformation trans = (Transformation)defValue;
		
		setActivityName(this.activity.getName());
		
		setStdEvidence(trans.getStdEvidence());
		setCriteria(trans.getCriteria());
		setActEvidence(trans.getActEvidence());
		setComment(trans.getComment());
	}
	

	public boolean getAsIcon() { return false; }
	
	
	public void setAsIcon(boolean yes) {}
	
	
	public boolean useBorderWhenIconified() { return false; }
	
	
	public void populateChildren()
	throws
		ParameterError,
		Exception {}
	
	
	public Set<VisualComponent> getChildVisuals()
	{
		return new HashSet<VisualComponent>();
	}
	
	
	public Object getParentObject() { return this.treeNode; }
	
	
	public void highlight(boolean yes) {}
	
	
	public boolean isHighlighted() { return false; }
	

	public Color getNormalBackgroundColor() { return null; }  // not used.
	
	
	public Color getHighlightedBackgroundColor() { return null; }  // not used.
	
	
	public int getWidth() { return 0; }
	
	public int getHeight() { return 0; }
	
	

  /* ***************************************************************************
	 * *************************************************************************
	 * From ActivityVisual. These implementations are empty because the intent
	 * of this View is not to depict simulation-time changes.
	 */

	 
	public void showStart()
	{
	}
	
	
	public void showStop()
	{
	}
	
	
	public void showEnteredRespond()
	{
	}



  /* ***************************************************************************
	 * *************************************************************************
	 * From ModelContainerVisual. These implementations are empty because the
	 * intent of this View is not to depict simulation-time changes.
	 */

	 
	public void resetConduitColors() {}
	public void incrementConduitColor() {}
	public Color getConduitColor() { return null; }
	
	
	
  /* *******************************************************************
	 * *************************************************************************
	 * Methods defined in ContainerVisual.
	 */
	
	
	public void componentResized(VisualComponent child)
	throws
		Exception
	{
		child.refresh();
	}
	
	
			
  /* ***************************************************************************
	 * *************************************************************************
	 * Non-public methods.
	 */
	 

	/** ************************************************************************
	 * Attempt to copy this Visual's state to the server.
	 * All methods that update the server should call this method.
	 * Automatically re-syncs any redundant Visual state.
	 *
	 
	final void updateServer()
	throws
		Exception
	{
		this.inconsistent = true;
		
		NodeSer updatedNodeSer = getView().getModelEngine().updateNode(???this.activity);
		if (! (updatedNodeSer instanceof ActivitySer)) throw new RuntimeException(
			"An Activity was updated, but server returned something else.");
		
		setActivitySer((ActivitySer)updatedNodeSer);
		
		refreshRedundantState();
	}*/
	
	
	final void setActivitySer(ActivitySer newActivitySer)
	{
		this.activity = newActivitySer;

		this.setUnpropagated(false);
		this.inconsistent = false;
	}


	/** ************************************************************************
	 * Retrieve the specified Attribute from the Persistent Node depicted by
	 * this Activity Visual.
	 */
	 
	AttributeSer getAttribute(String attrName)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return getView().getModelEngine().getAttribute(this.getNodeId(), attrName);
	}
	
	
	/** ************************************************************************
	 * Retrieve the Attributes that implement the specified Class and that belong
	 * to the Persistent Node depicted by this Activity Visual.
	 */
	 
	ModelAttributeSer[] getModelAttributes(Class c, String modelScenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return getView().getModelEngine().getModelAttributes(this.getNodeId(), c, 
			modelScenarioId);
	}
	
	
	/** ************************************************************************
	 * Return the Set of Visuals that are currently owned by this Visual.
	 */
	 
	Set<ActivityTreeData> getChildren()
	{
		Set<ActivityTreeData> children = new HashSet<ActivityTreeData>();
		
		TreeNode treeNode = this.getTreeNode();
		
		for (Enumeration e = treeNode.children(); e.hasMoreElements() ;)
		{
			Object object = e.nextElement();
			if (! (object instanceof DefaultMutableTreeNode)) throw new RuntimeException(
				"Not a DefaultMutableTreeNode: is a " + object.getClass().getName());
			
			DefaultMutableTreeNode childTreeNode = (DefaultMutableTreeNode)object;
			object = childTreeNode.getUserObject();
			if (! (object instanceof ActivityTreeData)) throw new RuntimeException(
				"Not an ActivityTreeData: is a " + object.getClass().getName());
			
			ActivityTreeData childVisual = (ActivityTreeData)object;
			children.add(childVisual);
		}
		
		return children;
	}
}

