/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.OperationMode.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import expressway.server.DecisionElement.*;
import expressway.generalpurpose.DateAndTimeUtils;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Date;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.IOException;


public class SimulationRunImpl extends ModelElementImpl
	implements SimulationRun  // is Runnable.
{
	/* -------------------------------------------------------------------------
		Instance variables. */

	/** ownership.
		A List, ordered by iteration, of all of the Events that occurred in that
		iteration. The first (0th) element in the List is iteration 1.
	 */
	
	private List<SortedEventSet> iterEventList = new Vector<SortedEventSet>();
	
	/** ownership.
		All epochs, in order of causality. An epoch is an iteration of
		the simulation main loop, and represents a slice of time. Any
		event generated within an epoch can only affect other components
		during the next epoch at the soonest.
	 */
	
	public List<Epoch> epochs = new Vector<Epoch>();

	
	/** ownership.
		List of errors that occur during this SimulationRun, in order
		of occurrence.
	 */
	
	public List<SimulationError> simErrors = new Vector<SimulationError>();

	
	/** ownership.
		The value of each model State at the end of this SimulationRun.
	 */
	private Map<State, Serializable> finalStateValues = new HashMap<State, Serializable>();
		

	/** ownership.
		A sequential List of all of the iterations that occurred during
		this simulation run.
	 */
	
	public List<Integer> iterations = new Vector<Integer>();
		
	
	/** ownership.
		Simulation begins at this simulated time.
	 */
	
	public Date startingDate = null;
	
	public Date getStartingDate() { return startingDate; }
	
	
	/** ownership.
		Simulation does not progress pass this simulated time. If null,
		there is no limit for simulated time.
	 */
	
	public Date finalDate = null;
	
	public Date getFinalDate() { return finalDate; }

	
	/** ownership.
		If the simulation has not completed after this number of
		iterations, it will abort.
	 */
	
	public int iterationLimit = 0;
	
	public int getIterationLimit() { return iterationLimit; }
	
	
	/** ownership.
		The elapsed time (ms) past which the simulation should not progress.
	 */
	
	public long duration;
	
	public long getDuration() { return duration; }

	
	/** ownership.
	 */
	
	public boolean completedNorm = false;
	
	public boolean getCompletedNorm() { return completedNorm; }


	/** ownership.
	 */
	 
	public int currentIteration = 0;
	
	
	
	public int getActualNoOfEpochs()
	{
		return epochs.size();
	}
	
	
	public synchronized long getActualElapsedTime()
	{
		if (epochs.size() == 0) return 0;
		return getActualEndTime() - getActualStartTime();
	}
	
	
	public synchronized long getActualStartTime()
	{
		if (epochs.size() == 0) return 0;
		return epochs.get(0).getTime();
	}
	
	
	public synchronized long getActualEndTime()
	{
		if (epochs.size() == 0) return 0;
		
		long finalEpochTime = epochs.get(epochs.size()-1).getTime();
		
		if (! completedNorm) return finalEpochTime;
		
		return finalDate.getTime();
	}
	

	/** ownership.
		List of the Events generated during this SimulationRun. Does not
		include the pre-defined Events.
	 */
	
	private List<GeneratedEvent> genEvents = new Vector<GeneratedEvent>();

	/**
		List of all of the Events that were simulated.
	 */
	
	private List<Event> allEvents = new Vector<Event>();
		
		
	/* -------------------------------------------------------------------------
		"Working" objects that change during simulation and that are not intended
		to be part of the simulation's final state. */

	/**
		The predefined events at the beginning of this SimulationRun.
	 */
	
	private transient SortedEventSet futureEvents = new SortedEventSetImpl();

	
	/**
		Cache of Set of Ports that are sensitive to each State.
	 */
	
	private transient Map<State, Set<Port>> sensitivePorts = new HashMap<State, Set<Port>>();

	
	/**
		Cache of Set of Ports that are reachable by each State.
	 */
	
	private transient Map<State, Set<Port>> reachablePorts = new HashMap<State, Set<Port>>();

	
	/**
		Cache of Set of sensitive Functions, keyed by State.
	 */
	
	private transient Map<State, Set<Function>> sensitiveFunctions = new HashMap<State, Set<Function>>();

	
	/**
		Cache of Set of sensitive Activities, keyed by State.
	 */
	
	private transient Map<State, Set<Activity>> sensitiveActivities = new HashMap<State, Set<Activity>>();

	
	/**
		Map of the State that drives each Port in the current epoch. Thus,
		this Map changes during the course of simulation.
	 */
	
	private transient Map<Port, State> currentDriver = new HashMap<Port, State>();

	
	/**
	 */
	
	private transient SimCallback callback = null;

	
	/**
	 */
	
	private transient Date currentTime = null;  // maintained by getCurrentEvents().

	
	/**
	 */
	
	private transient Thread thread = null;
	
	
	/**
		Map of the native implementation used by each model component.
	 */
	
	private transient Map<ModelComponent, NativeComponentImplementation> nativeImpls =
		new HashMap<ModelComponent, NativeComponentImplementation>();
	

	/* -------------------------------------------------------------------------
		For serialization. */
	
	public transient String[][] iterEventNodeIds = null;
	public transient TableEntry[] finalStateValuesByNodeId = null;
	public transient String modelScenarioNodeId = null;
	public transient String modelDomainNodeId = null;
	public transient String[] genEventNodeIds = null;
	public transient String[] allEventNodeIds = null;
	
	public String[][] getIterEventNodeIds() { return iterEventNodeIds; }
	public TableEntry[] getFinalStateValuesByNodeId() { return finalStateValuesByNodeId; }
	public String getModelScenarioNodeId() { return modelScenarioNodeId; }
	public String getModelDomainNodeId() { return modelDomainNodeId; }
	public String[] getGenEventNodeIds() { return genEventNodeIds; }
	public String[] getAllEventNodeIds() { return allEventNodeIds; }
	
	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		/* Owned: 
		finalStateValues (not allowed)
		simErrors (not allowed)
		iterEventList (not allowed)
		*/
		
		super.renameChild(child, newName);
	}
	
	
	/*public NodeSer externalize() throws ParameterError
	{
		super.externalize();
		
		int noOfIterations = iterEventList.size();
		iterEventNodeIds = new String[noOfIterations][];
		int iterNo = 0;
		for (SortedEventSet eventSet : iterEventList)
			// each eventSet in the List,
		{
			String[] eventArray = createNodeIdArray(eventSet);
			iterEventNodeIds[iterNo++] = eventArray;
		}
		
		finalStateValuesByNodeId = convertMapToTableEntryArray(finalStateValues);
		modelScenarioNodeId = getNodeIdOrNull(getModelScenario());
		genEventNodeIds = createNodeIdArray(genEvents);
		allEventNodeIds = createNodeIdArray(allEvents);
		
		return new SimulationRunSer(this);
	}*/


	protected Class getSerClass() { return SimulationRunSer.class; }
	
	
	protected NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		synchronized (this)
		{
			// Set all transient externalizable Node fields.
			
			int noOfIterations = iterEventList.size();
			iterEventNodeIds = new String[noOfIterations][];
			int iterNo = 0;
			
			List<SortedEventSet> iterEventListCopy = new Vector<SortedEventSet>(iterEventList);
			for (SortedEventSet eventSet : iterEventListCopy)
				// each eventSet in the List,
			{
				String[] eventArray = createNodeIdArray(eventSet);
				iterEventNodeIds[iterNo++] = eventArray;
			}
			
			finalStateValuesByNodeId = convertMapToTableEntryArray(finalStateValues);
			modelScenarioNodeId = getNodeIdOrNull(getModelScenario());
			genEventNodeIds = createNodeIdArray(genEvents);
			allEventNodeIds = createNodeIdArray(allEvents);
			
			
			// Set Ser fields.
			
			((SimulationRunSer)nodeSer).iterEventNodeIds = this.getIterEventNodeIds();
			((SimulationRunSer)nodeSer).epochs = this.getEpochs();
			((SimulationRunSer)nodeSer).simErrors = this.getErrors();
			((SimulationRunSer)nodeSer).finalStateValuesByNodeId = this.getFinalStateValuesByNodeId();
			((SimulationRunSer)nodeSer).iterations = this.getIterations();
			((SimulationRunSer)nodeSer).modelScenarioNodeId = this.getModelScenarioNodeId();
			//((SimulationRunSer)nodeSer).modelDomainNodeId = this.getModelDomainNodeId();
			((SimulationRunSer)nodeSer).startingDate = this.getStartingDate();
			((SimulationRunSer)nodeSer).finalDate = this.getFinalDate();
			((SimulationRunSer)nodeSer).iterationLimit = this.getIterationLimit();
			((SimulationRunSer)nodeSer).duration = this.getDuration();
			((SimulationRunSer)nodeSer).completedNorm = this.getCompletedNorm();
			((SimulationRunSer)nodeSer).genEventNodeIds = this.getGenEventNodeIds();
			((SimulationRunSer)nodeSer).allEventNodeIds = this.getAllEventNodeIds();
			
			return super.externalize(nodeSer);
		}
	}
	
		
	public boolean isResizable() { return false; }

	
	public Object clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		SimulationRunImpl newInstance = (SimulationRunImpl)(super.clone(cloneMap, cloneParent));
		
		// Don't implement because we don't need it.
		
		return newInstance;
	}

	
	public void writeToArchive(PrintWriter writer)
	throws
		IOException
	{
		writer.println("simulation_run:");
		writer.println("duration=" + duration);
		writer.println("iterationLimit=" + iterationLimit);
		writer.println("startingDate=" + 
			(startingDate == null ? "unspecified" : startingDate.getTime()));
		writer.println("finalDate=" + 
			(finalDate == null ? "unspecified" : finalDate.getTime()));
		writer.println("completedNorm=" + completedNorm);
		
		writer.println("simulation_errors:");
		for (SimulationError simError : simErrors)
		{
			writer.print("simulation_error:");
			writer.println(simError.toString());
		}
		
		writer.println("end simulation_errors");
		
		writer.println("events:");		
		
		List<Event>[] eventLists = new Vector[iterEventList.size()];
		int i = 0;
		for (Set eventSet : iterEventList)
		{
			List<Event> eventList = new Vector<Event>();
			eventLists[i++] = eventList;
			
			eventList.addAll(eventSet);
		}
		
		
		EventListArchiver.writeEvents(this.getModelDomain().getName(), 
			this.getModelScenario().getName(), this.getName(), 
			eventLists, writer);
		
		writer.println("end simulation_run");
	}
	
	
	public static SimulationRun readFromArchive(ModelEngineLocal modelEngine,
		BufferedReader br)
	throws
		CorruptInput,
		ParameterError,
		IOException
	{
		String line = br.readLine();
		if (line == null) throw new IOException("input file is empty");
		
		String nameNew;
		ModelScenario modelScenarioNew;
		ModelDomain modelDomainNew;
		long durationNew;
		int iterationLimitNew;
		Date startingDateNew;
		Date finalDateNew;
		boolean completedNormNew;
		List<SimulationError> simErrorsNew = new Vector<SimulationError>();


		line = br.readLine();
		if (! (line.equals("simulation_run:"))) throw new CorruptInput(
			"Expected 'simulation_run:'");
		
		
		// Read duration.
		
		line = br.readLine();
		String[] parts = line.split("=");
		if (! (parts[0].equals("duration"))) throw new CorruptInput(
			"Expected 'duration='");
		
		durationNew = Long.parseLong(parts[1]);
		
		
		// Read iterationLimit.
		
		line = br.readLine();
		parts = line.split("=");
		if (! (parts[0].equals("iterationLimit"))) throw new CorruptInput(
			"Expected 'iterationLimit='");
		
		iterationLimitNew = Integer.parseInt(parts[1]);
		
		
		// Read startingDate.
		
		line = br.readLine();
		parts = line.split("=");
		if (! (parts[0].equals("startingDate"))) throw new CorruptInput(
			"Expected 'startingDate='");
		
		long startingDateTime = Long.parseLong(parts[1]);
		startingDateNew = new Date(startingDateTime);
		
		
		// Read finalDate.
		
		line = br.readLine();
		parts = line.split("=");
		if (! (parts[0].equals("finalDate"))) throw new CorruptInput(
			"Expected 'finalDate='");
		
		long finalDateTime = Long.parseLong(parts[1]);
		finalDateNew = new Date(finalDateTime);
		
				
		// Read completedNorm.
		
		line = br.readLine();
		parts = line.split("=");
		if (! (parts[0].equals("completedNorm"))) throw new CorruptInput(
			"Expected 'completedNorm='");
		
		completedNormNew = Boolean.parseBoolean(parts[1]);
		
						
		// Read SimulationErrors.
		
		for (;;)
		{
			line = br.readLine();
			if (line == null) throw new CorruptInput("Ended early: no Events.");
			
			if (line.equals("end simulation_errors")) break;
			
			if (! (line.startsWith("simulation_error:"))) throw new CorruptInput(
				"Expected 'simulation_error:...'; found " + line);
			
			String errorString = line.substring(17);
			
			SimulationError simError;
			try { simError = SimulationError.parseSimulationErrorString(errorString); }
			catch (IllegalArgumentException ex) { throw new CorruptInput(
				"Simulation Error string is invalid: " + errorString); }
			
			simErrorsNew.add(simError);
		}
		
		
		// Get Model Domain and Model Scenario Nodes.
		
		PersistentNode[] domScenRunAr = new PersistentNode[3];
		EventListArchiver.readDomScenRun(modelEngine, domScenRunAr, br);
		
		nameNew = domScenRunAr[2].getName();
		
		modelScenarioNew = (ModelScenario)(domScenRunAr[1]);
		modelDomainNew = (ModelDomain)(domScenRunAr[0]);
		
		
		// Construct a SimulationRun object.
		
		SimulationRun simRunNew = modelScenarioNew.createSimulationRun(
			nameNew,
			modelScenarioNew, 
			modelDomainNew, 
			null,  // no callback because we will not be simulating.
			startingDateNew,
			finalDateNew,
			iterationLimitNew,
			durationNew
			);
		
			
		// Add Simulation Errors to the newly constructed Simulation Run.
		
		((SimulationRunImpl)simRunNew).addSimulationErrors(simErrorsNew);
				
			
		// Read Events.
		
		List<Event>[] arOfListOfEvent = 
			EventListArchiver.readEventRecords(modelDomainNew, modelScenarioNew, 
				simRunNew, br);
				
			
		// Read final terminator.
		
		line = br.readLine();
		if (line == null) throw new CorruptInput(
			"Ended early: no 'end simulation_run'.");
		
		if (! line.equals("end simulation_run")) throw new CorruptInput(
			"Expected 'end simulation_run'; found: " + line);
		
		
		return simRunNew;
	}


	public static class CorruptInput extends BaseException
	{
		public CorruptInput() { super(); }
		public CorruptInput(String msg) { super(msg); }
		public CorruptInput(String msg, Throwable t) { super(msg, t); }
		public CorruptInput(Throwable t) { super(t); }
	}


	public PersistentNode update(NodeSer nodeSer)
	throws
		ParameterError,
		ModelContainsError,
		InternalEngineError
	{
		SimulationRunSer ser = 
			(SimulationRunSer)checkInstanceCompatibility(nodeSer, SimulationRunSer.class);
			
			
		if (epochs.size() == 0)
		{
			this.startingDate = ser.startingDate;
			this.finalDate = ser.finalDate;
			this.iterationLimit = ser.iterationLimit;
			this.duration = ser.duration;
		}
		
		
		// Verify object references.
		// TBD.
		
		
		return super.update(nodeSer);
	}
	

	public void prepareForDeletion()
	{
		super.prepareForDeletion();
		
		// Call <type>Deleted(this) on each DeletionListener.
		// (There are none.)
		
		
		// For each owned Node, call prepareForDeletion() and then remove it.
		// (There are none.)

		List<GeneratedEvent> genEventsCopy = new Vector<GeneratedEvent>(genEvents);
		for (GeneratedEvent e : genEventsCopy)
		{
			e.prepareForDeletion();
			genEvents.remove(e);
		}
		
		
		// Remove other non-Node owned Objects.
		
		List<Set> iterEventSets = new Vector<Set>(iterEventList);
		for (Set s : iterEventSets)
		{
			iterEventList.remove(s);
		}
		
		List<Epoch> epochsCopy = new Vector<Epoch>(epochs);
		for (Epoch e : epochsCopy) epochs.remove(e);
		
		List<SimulationError> simErrorsCopy = new Vector<SimulationError>(simErrors);
		for (SimulationError e : simErrorsCopy) simErrors.remove(e);
		
		Set<State> valueStates = new HashSet<State>(finalStateValues.keySet());
		for (State s : valueStates) finalStateValues.remove(s);

		List<Integer> iterCopy = new Vector<Integer>(iterations);
		for (Integer i : iterCopy) iterations.remove(i);
		
		startingDate = null;
		finalDate = null;
	}
	
	
	/**
	 * Constructor for creating a new SimulationRun.
	 */

	SimulationRunImpl(
		String name,
		ModelScenario modelScenario,
		ModelDomain modelDomain,
		SimCallback callback,
		Date startingDate,
		Date finalDate,
		int iterationLimit,
		long duration)
	{
		super(name, modelScenario, modelDomain);

		//this.modelDomain = modelDomain;
		this.callback = callback;
		this.startingDate = startingDate;
		this.finalDate = finalDate;
		this.iterationLimit = iterationLimit;
		this.duration = duration;
	}
	
	
	protected void addSimulationErrors(Collection<SimulationError> simErrors)
	{
		this.simErrors.addAll(simErrors);
	}
	
	
	/**
	 * Intended to be called when reconstructing an archived SimulationRun.
	 */
	 
	protected void addEvents(List<Event>[] arOfListOfEvent)
	{
		int iteration = 0;
		for (List<Event> events : arOfListOfEvent)  // each epoch
		{
			this.iterations.add(new Integer(iteration));
			
			SortedEventSet iterEvents = new SortedEventSetImpl();
			this.iterEventList.add(iterEvents);

			boolean addedEpoch = false;
			for (Event event : events)
			{
				if (! addedEpoch) 
					this.epochs.add(new EpochImpl(event.getTime(), iteration));
				
				this.allEvents.add(event);
				
				if (event instanceof GeneratedEvent)
					this.genEvents.add((GeneratedEvent)event);
				
				iterEvents.add(event);
				
				this.finalStateValues.put(event.getState(), event.getNewValue());
			}
			
			iteration++;
		}
	}
	

	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	/*
	 * Methods from Runnable:
	 */


	/**
	 * The top-level entry point for a simulation run. This method contains
	 * the 'simulation loop'. Simulate the model until there are no future
	 * events pending, or until the finalDate has passed.
	 */

	public void run()
	{
		//if (OperationMode.TestMode)
		//	System.out.println(">>>Beginning simulation " + getName());
		
		this.thread = Thread.currentThread();
		
		Set<NativeComponentImplementation> startedNativeImpls =
			new HashSet<NativeComponentImplementation>();

		this.currentIteration = 0;
		
		if (startingDate == null) currentTime = new Date();  // today's date.
		else currentTime = startingDate;
		
		Date actualStartTime = currentTime;

		try
		{

		/*
		 * Propagate Decision Parameters to associated Attribute values in
		 * the ModelScenario, or apply default Attribute values, as applicable.
		 */

		 try { initializeAttributes(); }
		 catch (ParameterError pe)
		 {
			 callback.showMessage(pe.getMessage());
			 return;
		 }


		/*
		 * Start each native ModelComponent implementation, and give each component
		 * a ModelContext.
		 */

		//for (NativeComponentImplementation impl :
		//	modelDomain.getNativeComponentImplementations())
			
		for (ModelComponent comp : getModelDomain().getComponentsWithNativeImpls())
		{
			NativeComponentImplementation impl = null;
			
			Class implClass = null;
				
			try
			{
				if (comp instanceof Function)
				{
					implClass = ((Function)comp).getNativeImplementationClass();
					Function.NativeFunctionImplementation funcImpl = null;

					funcImpl =
						(Function.NativeFunctionImplementation)
										(implClass.newInstance());
										
					funcImpl.setModelComponent(comp);
					
					funcImpl.start(new FunctionContextImpl(this, comp));
					impl = funcImpl;
				}
				else if (comp instanceof Activity)
				{
					implClass = ((Activity)comp).getNativeImplementationClass();
					Activity.NativeActivityImplementation actImpl = null;
					
					actImpl = (Activity.NativeActivityImplementation)
										(implClass.newInstance());
										
					actImpl.setModelComponent(comp);
					
					actImpl.start(new ActivityContextImpl(this, comp));
					impl = actImpl;
				}
				else throw new RuntimeException(
					"Unexpected: a " + comp.getClass().getName() + " has a native impl");
			}
			catch (InstantiationException ie)
			{
				ie.printStackTrace();
				callback.showMessage(
					"Unable to instantiate '" + implClass.getName() + "' for " +
						comp.getName() + "; " +
						ie.getMessage() + "; stack trace printed");
				return;
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				callback.showMessage(
					"Unable to start native implementation for " +
						comp.getName() + "; " +
						ex.getMessage() + "; stack trace printed");
				return;
			}
			
			synchronized (this)
			{
				nativeImpls.put(comp, impl);
			}

			startedNativeImpls.add(impl);
		}


		/*
		 * Seed the epochs List with T=0.
		 *

		if (startingDate == null) epochs.add(new EpochImpl(0, 1));
		else epochs.add(new EpochImpl(startingDate.getTime(), 1));*/
		
		
		/*
		 * Calculate the Date of the final epoch. It is the earlier of the final
		 * epoch specified by the caller, or the epoch that is 'duration' ms from
		 * the starting epoch.
		 */

		Date d = null;
		if (duration > 0) try
		{
			d = new Date(currentTime.getTime() + duration);
			if ((finalDate == null) || (d.getTime() < finalDate.getTime()))
				finalDate = d;
		}
		catch (Exception ex) { callback.aborted(ex); ex.printStackTrace(); return; }
		
		
		/*
		 * Create initial event population. The futureEvents Set is
		 * populated based on various sources of Predefined and Startup
		 * Events.
		 */

		// Send an event to each EventProducer, signaling it to
		// drive its States.
		
		signalEventProducers(getModelDomain());


		// Schedule PredefinedEvents.

		getPredefinedEvents(getModelDomain(), futureEvents);
	
		
		// Get the earliest event set that precedes finalDate.
		
		SortedEventSet curEvents = getCurrentEvents(getCurrentTime(), finalDate);
		

		// Check that the earliest Event does not precede the start date.
		
		if ((startingDate != null) && (currentTime.compareTo(startingDate) < 0))
		{
			callback.aborted(
				"First event occurs prior to the start time that was specified.");
				
			return;
		}
		
		
		/* *********************************************************************
		 * Simulation loop: iterate until no more events to process, or until
		 * the designated time epoch has passed.
		 */

		simloop: for  // each epoch (time) identified by the complete set of future events...
			// (Note that this set may grow during simulation as the result of
			// the generation of events.)
		(
			;

			// while the next event set is not empty,
			//  (Note: events cannot be generated for the current epoch.)
			;
		)
		{
			if (Runtime.getRuntime().freeMemory() < 30000000)  // abort
			{
				callback.aborted("Server memory getting too low.");
				break;
			}
			
			
			++(this.currentIteration);  // First iteration is iteration 1.
			
			//System.out.println("==============================================");
			//System.out.println("Iteration " + this.currentIteration + " at " + currentTime
			//	 + "; current events:");
			//for (Event e : curEvents) e.dump(1);
			
			//System.out.println("Future events:");
			//for (Event e : futureEvents) e.dump(1);

			if (curEvents.isEmpty())
			{
				//System.out.println("No more Events at start of iteration " + this.currentIteration);
				break;
			}
			
			
			if ((iterationLimit > 0) && (this.currentIteration > iterationLimit))
			{
				callback.aborted("Iteration limit exceeded");
				break;
			}
			

			/*
			 * Define epoch to aggregate the current events
			 */
			 
			synchronized (this)
			{
				iterations.add(new Integer(this.currentIteration));
				epochs.add(new EpochImpl(currentTime, this.currentIteration));
			}
			
			if (callback.checkAbort()) { callback.aborted(); return; }


			/*
			 * Identify all Functions that need to be triggered for
			 * this epoch. Do this repetitively until no more events are
			 * generated by any Functions. First we loop over all current
			 * events for this epoch, allowing each sensitive Function to
			 * generate more events. Then we loop over each of those, again
			 * allowing for Functions to generate more events. We do this
			 * until no function has generated any more events. Note that
			 * a function can only generate an event for the current epoch.
			 */

			Map<Function, Integer> evaluationCounts = new HashMap<Function, Integer>();
				// Count the number of times each Function is invoked during
				// this epoch. This is used to prevent race conditions.
			
			Set<Function> allFunctions = new TreeSet<Function>();
			
			Set<Function> triggeredFunctions = new TreeSet<Function>();
				// Functions that have been triggered in this epoch by any Event
				// other than a Startup Event.

			Set<Function> startupFunctions = new TreeSet<Function>();
				// Functions that have been scheduled in this epoch for
				// evaluation as part of startup.
			
			for	// until there are no more Function-generated events...
			(
				SortedEventSet functionGeneratedEvents = new TreeSet(curEvents);
				! functionGeneratedEvents.isEmpty();
			)
			{
				SortedEventSet noPropagateEvents = 
					findNoPropagateEvents(functionGeneratedEvents);
				
				
				/*
				 * Identify all behavioral Functions that are sensitive to
				 * the set of events that we are looping over.
				 */

				Set<Function> functions = new TreeSet<Function>();

				for // each event in the current epoch event set...
				(
					Event event : functionGeneratedEvents
				)
				{
					if (callback.checkAbort()) { callback.aborted(); return; }

					if (event instanceof StartupEvent)
					{
						ModelComponent component = ((StartupEvent)event).getComponent();
						
						// We are only interested in Functions here, so ignore others.
						//if (component instanceof Function)
						//	callback.showProgress(
						//		component.getName() + ":" + " Startup signal.");
					}
					else
					{
						Serializable newValue = event.getNewValue();
						/*
						callback.showProgress
						(
							event.getState().getEventProducer().getName() + ":" +
							event.getState().getName() + " -> " +
							(newValue == null ? "null" : newValue.toString()) + " at t=" +
							(event.getTime() == null ? "null" : event.getTime().toString())
						);
						*/
					}
					
					
					// If signaling a Function, add the Function to the list of
					// Functions to be activated.
					
					if (event instanceof StartupEvent)
					{
						ModelComponent component = ((StartupEvent)event).getComponent();
						if (component instanceof Function)
						{
							functions.add((Function)component);
							startupFunctions.add((Function)component);
						}
						
						continue; // don't need to do the rest.
					}


					/*
						Identify all Functions that are sensitive to the event,
						so that these Functions can be evaluated.
					 */

					Set<Function> eventFunctions = null;

					try { eventFunctions = getSensitiveFunctions(event.getState()); }
					catch (ModelContainsError ex)
					{ callback.showMessage(ex.getMessage()); callback.aborted(); 
						ex.printStackTrace(); return; }

					/*
					if (OperationMode.TestMode)
					{
						System.out.println("Sensitive functions:");
						for (Function f : eventFunctions)
						{
							System.out.println("\t" + f.getName());
						}
					}
					*/
					
					triggeredFunctions.addAll(eventFunctions);

					functions.addAll(eventFunctions);
					// Note: the fact that we add to this list non-redundantly will
					// prevent a race condition from occurring.
				}


				/*
					Evaluate each Function. This may result in the generation
					of more events for the CURRENT epoch. These events must be
					scheduled for THIS simulation cycle: this is accomplished by
					adding the new events to the current event set, and by
					again determining which functions are sensitive to these NEW
					events and evaluating those.
				 */

				SortedEventSet nextFunctionGeneratedEvents = new SortedEventSetImpl();

				for	// each Function that is sensitive to the event...
				(
					Function function : functions
				)
				{
					// Check for race conditions:
					// If the function has been triggered at least once in this
					// epoch by a non-startup event, then do not evaluate it
					// again in this epoch.
					
					int evalCount = 0;
					Integer i = evaluationCounts.get(function);
					if (i == null) evalCount = 0;
					else evalCount = i.intValue();
					
					if (allFunctions.contains(function) &&
						//(! startupFunctions.contains(function)) &&
						(evalCount >= function.getMaxEvals()))
						//(triggeredFunctions.contains(function)))
					{
						// Function has already been evaluated in this epoch.
						// To prevent a potential race condition, do not
						// evaluate it again.

						callback.showMessage("Not re-evaluating Function " +
							function.getName() + " to avoid possible race condition.");
								
						continue;
					}

					// Evaluate the Function.
					
					Set<GeneratedEvent> newFuncGenEvents = null;

					try
					{
						SortedEventSet visibleEvents =
							getVisibleSubset(functionGeneratedEvents, noPropagateEvents,
								function);

						//System.out.println("Evaluating " + function.getName());

						newFuncGenEvents =
							function.respond(this, removeStartupEvents(visibleEvents));
						
						/*
						if (OperationMode.TestMode)
						{
							GeneratedEventImpl.dump(newFuncGenEvents, 0,
								"***New Function-Generated Events");
						}
						*/
					}
					catch (ModelContainsError ex)
					{
						Thread t = Thread.currentThread();
						//System.out.println("----" + t.getClass().getName());
						callback.aborted("In Function " + function.getName() +
							"; " + ex.getMessage());

						ex.printStackTrace();
						return;
					}


					// Increment the count of how many times this Function has
					// been evaluated during this epoch.
					
					Integer priorCount = evaluationCounts.get(function);
					if (priorCount == null) priorCount = new Integer(0);
					
					evaluationCounts.put(function,
						new Integer(priorCount.intValue() + 1));

					
					//
					// Delete overridden Events.
					//
					
					for (Event newEvent : newFuncGenEvents)
					{
						Event obsoleteEvent = null;
						for (Event prospectiveEvent : functionGeneratedEvents)
						{
							if (prospectiveEvent.getState() == newEvent.getState())
							{
								obsoleteEvent = prospectiveEvent;
								break;
							}
							// an Event in functionGeneratedEvents
							// that is for the same State as newEvent (should
							// not be more than one).
						}
							
						if (obsoleteEvent != null)
							// Delete the event from the Set of function-generated Events.
							functionGeneratedEvents.remove(obsoleteEvent);
					}


					nextFunctionGeneratedEvents.addAll(newFuncGenEvents);
					
				}  // next Function
				
				
				/*
				 * Add the generated events of all Functions to the set of
				 * events for the current epoch.
				 */

				curEvents.addAll(functionGeneratedEvents);

				allFunctions.addAll(functions);

				synchronized (this)
				{
					allEvents.addAll(curEvents);  // maintain list of all events.
				}

				functionGeneratedEvents = nextFunctionGeneratedEvents;
			}

			
			/*
			 * Identify all Activities that need to be triggered for
			 * this epoch. While doing this, identify all of the Ports
			 * that change value, and the States that will drive them.
			 */

			Set<Activity> activities = new TreeSet<Activity>();

			for // each event in the current epoch event set...
			(
				Event event : curEvents
			)
			{
				if (callback.checkAbort())
				{
					((ServiceThread)(Thread.currentThread())).aborted();
					callback.aborted(); return;
				}
				
				//System.out.println("Checking for activities triggered by " + event.toString());

				/*
				if (event instanceof StartupEvent)
				{
					ModelComponent component = ((StartupEvent)event).getComponent();
					if (component instanceof Activity) callback.showProgress
					(
						component.getName() + ":" +
						" Startup signal."
					);
				}
				else
				{
					Serializable newValue = event.getNewValue();
					callback.showProgress
					(
						event.getState().getEventProducer().getName() + ":" +
						event.getState().getName() + " -> " +
						(newValue == null ? "null" : newValue.toString()) + " at t=" +
						(event.getTime() == null ? "null" : event.getTime().toString())
					);
				}
				*/

					
				// If signaling an Activity, add the Activity to the list of
				// Activities to be activated.
					
				if (event instanceof StartupEvent)
				{
					ModelComponent component = ((StartupEvent)event).getComponent();
					if (component instanceof Activity) activities.add((Activity)component);
					continue; // don't need to do the rest.
				}


				/*
					Identify all behavioral Activities that are sensitive to
					the Event, so that these Activities can be activated.

					While doing this, the Ports that will change value are
					identified, and their new State driver.
				 */

				Set<Activity> eventActivities = null;
				try { eventActivities = getSensitiveActivities(event.getState()); }
				catch (ModelContainsError ex)
				{
					callback.showMessage(ex.getMessage());
					callback.aborted();
					ex.printStackTrace();
					return;
				}
				
				
				// Check for obvious race condition. This is only a partial check,
				// since it does not detect races that traverse multiple components.
				
				EventProducer ep = event.getState().getEventProducer();
				if (eventActivities.contains(ep)
					//eventActivities includes the events''s source
					&&
						((event.getTime() == null) || (event.getTime().equals(0))))
				{
					callback.showMessage("Warning: possible race condition: " +
						((ModelElement)ep).getFullName() + 
							" is responding to its own Event: " + event.toString());
					//System.out.println("Warning: possible race condition: " +
					//	((ModelElement)ep).getFullName() + 
					//		" is responding to its own Event: " + event.toString());
				}
				
				
				//ActivityImpl.dump(eventActivities, 0, "Activities sensitive to that event:");

				activities.addAll(eventActivities);
			}

			//ActivityImpl.dump(activities, 0, "***Activities To Be Activated");


			/*
			 * Log the currentEvents for the epoch.
			 */

			synchronized (this)
			{
			iterEventList.add(curEvents);
			}
			//System.out.println("Added events for iteration " + this.currentIteration);
			
			
			/*
			 * Check if any Events in this epoch represent the same state: this
			 * is an error if the new values are different.
			 */

			Set<State> states = new TreeSet<State>();

			SortedEventSet currentEventsCopy = new SortedEventSetImpl(curEvents);
			for (Event event : currentEventsCopy)
			{
				if (callback.checkAbort())
				{
					callback.aborted();
					return;
				}

				if (event instanceof StartupEvent) continue;
				
				if (event.getNewValue() == null) callback.showMessage(
					"Warning: Event has null value: " + event);
				
				State state = event.getState();
				if (states.contains(state))
				{
					callback.showMessage(
						"Warning: The state " + state.getName() +
						" is driven by multiple events at time " + event.getTime());
					
					// Verify that the State is not being set differently by
					// different Events.
					
					for (Event e : currentEventsCopy)
					{
						if (event == e) continue;
						
						if (event.getNewValue() == null) callback.showMessage(
							"Warning: Event has null value: " + e);
				
						if (state == e.getState())
						{
							if (ObjectValueComparator.compare(event.getNewValue(), e.getNewValue()))
							{
								//curEvents.remove(e);  // Delete the redundant Event.
							}
							else
							{
								callback.showErrorMessage(
									"Conflicting values for State " + state.getName() +
									" in iteration " + getCurrentEpoch().getIteration() +
									" at t=" + getCurrentTime() + " (elapsed time=" +
									DateAndTimeUtils.convertMsToDayHourMinSec(
										getActualElapsedTime()) + ")");
									
								callback.showErrorMessage("The conflicting Events are: [" +
									event.toString() + "] and [" + e.toString() + "]");
									
								SortedEventSet trigEvs1 = event.getTriggeringEvents();
								SortedEventSet trigEvs2 = e.getTriggeringEvents();
								
								if (trigEvs1 != null)
								{
									callback.showErrorMessage(
										"The first event was caused by the following events:");
									
									for (Event trigEv1 : trigEvs1)
										callback.showErrorMessage(trigEv1.toString());
								}
								
								if (trigEvs2 != null)
								{
									callback.showErrorMessage(
										"The second event was caused by the following events:");
									
									for (Event trigEv2 : trigEvs2)
										callback.showErrorMessage(trigEv2.toString());
								}
									
								callback.aborted("Due to Event conflict");

								//System.out.println("Conflicting values for State " + state.getName() +
								//	" at t=" + getCurrentTime());
								//System.out.println("The two conflicting events are:");
								//event.dump(1);
								//e.dump(1);
								//(new Exception()).printStackTrace();
								
								return;
							}
						}
					}
				}
				else
				{
					states.add(state);
				}
			}


			if (curEvents.size() == 0)
			{
				callback.showMessage(
					"After no-effect events are removed, there are no " +
						"events for iteration " + this.currentIteration);

				curEvents = getCurrentEvents(getCurrentTime(), finalDate);
				
				continue simloop; // skip this iteration.
			}
			
			
			/*
			 * Identify events that do not change the value driving any Port.
			 * Such Events will not be deleted but they will not be propagated.
			 */

			SortedEventSet noPropagateEvents = findNoPropagateEvents(curEvents);
			
			//System.out.println("Events that should not propagate:");
			//for (Event e : noPropagateEvents) System.out.println("\t" + e);
			

			/*
			 * Apply the events: update the value of each Event's State.
			 * While doing this, update the Map of current drivers (States)
			 * for each Port.
			 */

			Map<Port, Event> epochDriverEvents = new HashMap<Port, Event>();
				// Events that start to drive a Port during this epoch.
			
			for (Event event : curEvents)
			{
				//System.out.println("^^^^Applying event " + event.toString());
				
				if (callback.checkAbort())
				{
					callback.aborted();
					return;
				}

				if (! (event instanceof StartupEvent))
				{
					State state = event.getState();
					if (state == null) throw new RuntimeException("State is null");
					
					synchronized (this)
					{
						finalStateValues.remove(state);
					}

					Serializable valueCopy = null;
					try
					{
						//System.out.println(
						//	">>>>>>Attempting to set new State Value for " +
						//	state.getName());
						valueCopy = PersistentNodeImpl.copyObject(event.getNewValue());
						
						Set<Port> boundPorts = event.getState().getPortBindings();
						for (Port p : boundPorts)
						{
							currentDriver.put(p, event.getState());
							//System.out.println("Set driver of " + p.getFullName() +
							//	" to be " + event.getState().getFullName());
						}

						if (! noPropagateEvents.contains(event))  // Propagate the Event.
						{
							// Identify Ports that can receive an Event from state.
							Set<Port> reachablePorts = getReachablePorts(state);
							/*Set<Port> sensitivePorts = getSensitivePorts(state);
								....not right: the above will not identify Ports on
								structural components, and it will only include
								Ports that can conduct an Event into its component.*/
								
							for (Port p : reachablePorts)
							{
								//System.out.println("Port " + p.getFullName() +
								//	" is sensitive to State " + state.getFullName());
									
								// Check for conflict: that no two different-valued Events
								// are attempting to drive the same Port.
							
								Event otherEvent = epochDriverEvents.get(p);
								if (otherEvent == null)
								{
									epochDriverEvents.put(p, event);
								}
								else  // another State is driving the Port in this epoch.
								{
									// Check that value is the same.
									Serializable otherValue = otherEvent.getNewValue();
								
									if (! ObjectValueComparator.compare(otherValue, valueCopy))
									{
										//((otherValue == null) && (valueCopy != null)) ||
										//((otherValue != null) && (! otherValue.equals(valueCopy))))
										System.out.println("Ports connected to " + state.getFullName() + ":");
										for (Port conPort : reachablePorts)
										{
											System.out.println("\t" + conPort.getFullName());
										}
										
										throw new ModelContainsError(
										//System.out.println(
											"In epoch " + getCurrentEpoch() +
											", two Events are attempting to drive Port " + 
											p.getFullName() + " with different values, " +
											otherValue + " and " + valueCopy + ", from states " +
											otherEvent.getState().getFullName() + " and " +
											event.getState().getFullName() + ", respectively.");
									}
								}							
								
								currentDriver.put(p, state);
							}
						}
					}
					catch (ModelContainsError mce)
					{
						callback.aborted(mce.getMessage());
						mce.printStackTrace();
						return;
					}
					catch (CloneNotSupportedException ex)
					{
						callback.aborted("Uncloneable value in Event at t=" +
							(event.getTime() == null ? "null" : 
								event.getTime().toString()));

						ex.printStackTrace();
						return;
					}

					synchronized (this)
					{
						finalStateValues.put(state, valueCopy);
					}
					
					//System.out.println("^^^^^^Updated " + state.getFullName() + " -> " + valueCopy);
					//System.out.println("^^^^^^>Verify: " + state.getFullName() + " value = " +
					//	finalStateValues.get(state));
				}
			}

			//if (OperationMode.TestMode) StateImpl.dump(states, 0,
			//	"***Updated States To Reflect Events For This Epoch");


			/*
				Activate each Activity. This may result in the generation
				of future events. (Events that are generated to occur
				after time delay 0 are scheduled for the next cycle of
				the simulation loop.)
			 */

			Set<GeneratedEvent> epochGenEvents = new TreeSet<GeneratedEvent>();

			for	// each Activity that is sensitive to the event...
			(
				Activity activity : activities
			)
				// Determine the Activity's response to the event.
			{
				if (callback.checkAbort())
				{
					callback.aborted();
					return;
				}

				Set<GeneratedEvent> actGenEvents = null;

				try
				{
					SortedEventSet visibleEvents =
						getVisibleSubset(curEvents, noPropagateEvents, activity);

					//System.out.println(activity.getName() +
					//	" visibleEvent.size()=" + visibleEvents.size());

					if (visibleEvents.size() > 0)
					{
						//callback.showProgress("Triggering " + activity.getName());
						actGenEvents = activity.respond(this, 
							removeStartupEvents(visibleEvents));

						// Diagnostic: dump the event queue....
						
						//System.out.println("****generated events for " + activity.getFullName());
						//for (Event e : actGenEvents) e.dump(1);
						//System.out.println("----");
						
						epochGenEvents.addAll(actGenEvents);
						
						// diagnostic:
						//System.out.println("****epoch generated events:");
						//for (Event ee : epochGenEvents) ee.dump(1);
						//System.out.println("----");
					}
				}
				catch (ModelContainsError ex)
				{
					callback.aborted("In Activity " + activity.getName() +
						" due to: " + ex.getMessage());
					ex.printStackTrace();
					return;
				}
			}

			//System.out.println(">>>>before, futureEvents size=" + futureEvents.size());

			//if (OperationMode.TestMode) GeneratedEventImpl.dump(epochGenEvents, 0,
			//	"***Events Generated By Activities In This Epoch");

			//System.out.println(">>>>epochGenEvents size=" + epochGenEvents.size());


			// Add this epoch's generated events for all Activities to the set of
			// future events.

			synchronized (this)
			{
				futureEvents.addAll(epochGenEvents);
			}

			//System.out.println("****future events:");
			//for (Event eee : futureEvents) eee.dump(1);
			//System.out.println("----");
			
			//if (OperationMode.TestMode) EventImpl.dump(futureEvents, 0,
			//	"***Future Events (at the end of the current epoch)");

			//System.out.println(">>>>after, futureEvents size=" + futureEvents.size());

			
			// Report progress.
			long elapsedTime = currentTime.getTime() - actualStartTime.getTime();
			callback.showProgress(this.currentIteration, elapsedTime);


			// Get next event set.
			curEvents = getCurrentEvents(getCurrentTime(), finalDate);
			
		}  // end of main loop


		//System.out.println("No more events");
		
		try { updateAttributes(); }
		catch (ParameterError pe)
		{
			callback.showMessage(pe.getMessage());
			pe.printStackTrace();
			return;
		}
		
		completedNorm = true;
		callback.completed(this);
		}
		catch (RuntimeException ex)
		{
			callback.showMessage("Internal simulator error: " + ex.getMessage());
			ex.printStackTrace();
			throw ex;
		}
		catch (Error er)
		{
			//System.out.println(er.getMessage());
			Runtime.getRuntime().gc();
			callback.showMessage("Internal simulator error: " + er.getMessage());
			er.printStackTrace();
			throw er;
		}
		finally  // stop all native components.
		{
			//System.out.println(">>>in finally block of " + getName());
			
			for (NativeComponentImplementation impl : startedNativeImpls)
			{
				try
				{
					impl.stop();
					//callback.showMessage(impl.getModelComponent().getName() +
					//	" stopped.");
				}
				catch (Exception ex) { callback.showMessage(
					"Could not stop native implementation for " +
						impl.getModelComponent().getName()); }
			}

			/*
			if (OperationMode.TestMode)
			{
				System.out.println("Simulation run '" + getName() +
					"' final state values after " + iteration + " iterations " +
					"and " + allEvents.size() + " events:");
					
				// Display the final value of each State that changed.
				//System.out.println("Final State values:");
				for (State state : finalStateValues.keySet())
				{
					System.out.println("\t" + state.getName() + "=" +
						finalStateValues.get(state));
				}
				
				System.out.println("All events:");
				
				for (Event event : allEvents)
				{
					System.out.println("\t" + event.toString());
				}
				
				System.out.println("Simulation run '" + getName() + "' exiting.");
			}
			*/
			
			callback.completed();
		}
	}


	/*
	 * Methods from SimulationRun:
	 */
	 
	public synchronized NativeComponentImplementation getNativeImplementation(
		ModelComponent comp)
	{
		return nativeImpls.get(comp);
	}
	
		
	public synchronized ModelScenario getModelScenario()
	{
		return (ModelScenario)(getParent());
	}


	public synchronized List<GeneratedEvent> getGeneratedEvents() { return genEvents; }


	public synchronized List<GeneratedEvent> getGeneratedEvents(State state)
	{
		List<GeneratedEvent> stateEvents = new Vector<GeneratedEvent>();
		for (GeneratedEvent event : genEvents)
		{
			if (event.getState() == state) stateEvents.add(event);
		}

		return stateEvents;
	}


	public synchronized List<Event> getEvents()
	{
		return allEvents;
	}


	public synchronized List<Event> getEvents(State state)
	{
		List<Event> stateEvents = new Vector<Event>();
		for (Event event : allEvents)
		{
			if (event.getState() == null) continue;
			if (event.getState() == state) stateEvents.add(event);
		}

		return stateEvents;
	}


	public synchronized List<Event> getEvents(State state, long startTime, long endTime)
	{
		List<Event> stateEvents = new Vector<Event>();
		for (Event event : allEvents)
		{
			if (event.getTime().getTime() < startTime) continue;
			if (event.getTime().getTime() > endTime) return stateEvents;
			
			if (event.getState() == null) continue;
			if (event.getState() == state) stateEvents.add(event);
		}

		return stateEvents;
	}


	public synchronized List<Event> getEvents(State state, Event firstEvent, long endTime)
	{
		List<Event> stateEvents = new Vector<Event>();
		boolean firstEventFound = false;
		for (Event event : allEvents)
		{
			if (event == firstEvent) firstEventFound = true;
			
			if (firstEventFound)
			{
				if (event.getTime().getTime() > endTime) return stateEvents;
				
				if (event.getState() == null) continue;
				if (event.getState() == state) stateEvents.add(event);
			}
		}

		return stateEvents;
	}
	
	
	public synchronized Event getFirstEvent(State state)
	{
		for (Event event : allEvents)
		{
			if (event.getState() == state) return event;
		}
		
		return null;
	}


	public synchronized List<Epoch> getEpochs() { return epochs; }


	public synchronized List<Integer> getIterations() { return iterations; }
	

	public synchronized Epoch getOrAppendEpochForIteration(int iteration, long ms)
	throws
		IndexOutOfBoundsException
	{
		Epoch epoch;
		try { epoch = epochs.get(iteration-1); }
		catch (IndexOutOfBoundsException ex)
		{
			if (iteration <= 0) throw ex;
			if (iteration != epochs.size()) throw ex;
			
			epoch = new EpochImpl(ms, iteration);
			epochs.add(epoch);
		}
		
		return epoch;
	}


	public synchronized Epoch getEpochForIteration(int iteration)
	throws
		IndexOutOfBoundsException
	{
		return epochs.get(iteration-1);
	}


	public synchronized SortedEventSet getEventsAtIteration(int iteration)
	throws
		ParameterError
	{
		//System.out.println("iterEventList size=" + iterEventList.size());
		
		if (iterEventList.size() < iteration) throw new ParameterError(
			"No iteration " + iteration);
		
		SortedEventSet events = iterEventList.get(iteration-1);
		
		if (events == null) return new SortedEventSetImpl();
		
		return events;
	}
	

	public synchronized List<SimulationError> getErrors() { return simErrors; }


	public synchronized boolean completedNormally() { return completedNorm; }


	public synchronized Serializable getStateValueAtEpoch(State state, Epoch epoch)
	{
		// Traverse through all events until the specified Epoch
		// is reached, and return the most recent event value. Note that this
		// implementation relies on the assumption that each Epoch object is
		// unique in the epochs List. It is also assumed that a State
		// cannot have more than one value during an Epoch.

		//List<Event> stateEvents = getEvents(state);
		Serializable mostRecentValue = null;
		//Boolean epochReached = false;
		for (Event event : this.allEvents)
		{
			if (event instanceof StartupEvent) continue;
			
			if (event.getState() == state)
				mostRecentValue = event.getNewValue();

			if (event.getTime() == epoch)
				// Note that Epoch object identity is relied upon.
				return mostRecentValue;
		}

		return mostRecentValue;
	}
	
	
	public synchronized Serializable[] getStateValuesAtDate(State state, Date date)
	{
		// Traverse through all events until the specified Date value
		// is reached, and return the most recent event value.
		
		List<Serializable> mostRecentValues = new Vector<Serializable>();
		for (Event event : this.allEvents)
		{
			if (event instanceof StartupEvent) continue;
			
			if (event.getTime().before(date)) continue;
			
			if (event.getTime().after(date)) return mostRecentValues.toArray();
			
			if (event.getState() == state)
				mostRecentValues.add(event.getNewValue());
		}

		return mostRecentValues.toArray();
	}


	public synchronized Map<State, Serializable> getFinalStateValues()
	{
		return finalStateValues;
	}
	
	
	public synchronized void setInitialStateValue(State state, Serializable initValue)
	throws
		ParameterError
	{
		finalStateValues.put(state, initValue);
	}


	public synchronized Serializable getStateFinalValue(State state)
	throws
		ParameterError
	{
		if (! completedNorm) { (new Exception()).printStackTrace(); throw new ParameterError(
			"Value for State " + state.getName() + " is indeterminate.");}

		Serializable value = finalStateValues.get(state);
		return value;
	}


	public synchronized double computeTimeAveragedValue(State state, long startTime,
		long endTime)
	throws
		UndefinedValue,
		ParameterError
	{
		if (endTime - startTime == 0) throw new ParameterError(
			"Cannot time average: elapsed time is zero.");
		
		Event firstEvent = getDrivingEventAt(state, startTime);
		
		if (firstEvent == null) firstEvent = getFirstEvent(state);
		
		if (firstEvent.getTime().getTime() > endTime) throw new UndefinedValue(
			"End time is " + new Date(endTime) + " and so " +
			state.getFullName() + " is undefined at " + firstEvent.getTime());
		
		if (endTime > finalDate.getTime()) throw new ParameterError(
			"Simulation Run " + this.getName() + 
			" does not extend past the specified end time " + (new Date(endTime)));
		
		Serializable firstValueObject = firstEvent.getNewValue();
		if (! (firstValueObject instanceof Number)) throw new ParameterError(
			"Event value at time " + firstEvent.getTime() + " is not a Number.");
		
		List<Event> events = getEvents(state, firstEvent, endTime);
		
		// Check if the elapsed time is zero.
		if (endTime == firstEvent.getTime().getTime()) throw new ParameterError(
			"Cannot time average: elapsed time between first Event and end time is 0.");
		
		double priorTime = startTime;
		double priorValue = ((Number)firstValueObject).doubleValue();
		double average = 0.0;
		events.remove(firstEvent);
		for (Event event : events)
		{
			Serializable valueObject = event.getNewValue();
			if (! (valueObject instanceof Number)) throw new ParameterError(
				"Event value at time " + event.getTime() + " is not a Number.");
			
			long time = event.getTime().getTime();
			double value = ((Number)valueObject).doubleValue();
			
			average += (priorValue * (time - priorTime));
			
			priorValue = value;
			priorTime = time;
		}
		
		if (priorTime < endTime) average += (priorValue * (endTime - priorTime));
		
		average /= (endTime - firstEvent.getTime().getTime());
		
		return average;
	}
	
	
	public Event getDrivingEventAt(State state, long time)
	{
		Event drivingEvent = null;
		//Object drivingValue = null;
		
		for (Event event : allEvents)
		{
			if (event.getState() != state) continue;
			
			long eventTime = event.getTime().getTime();
			
			if (eventTime <= time)
			{
				drivingEvent = event;
				//drivingValue = event.getNewValue();
			}
			else
				return drivingEvent;
		}
		
		return null;
	}
	
	
	
  /* The following methods support the simulation lifecycle. They cannot be
  	called after simulation has completed.
	*/
	
	public synchronized GeneratedEvent generateEvent(State state,
		Date time, Serializable newValue, SortedEventSet triggeringEvents)
	throws
		ParameterError // if the specified time is earlier than the current time.
	{
		GeneratedEvent genEvent = new GeneratedEventImpl(state, time, 
			getCurrentEpoch(), newValue, this);
			
		genEvent.setTriggeringEvents(triggeringEvents);

		return genEvent;
	}


	public synchronized State getCurrentDriver(Port port)
	{
		return currentDriver.get(port);
	}


	public synchronized int purgeFutureEvents(Activity activity)
	{
		//System.out.println("Entered purgeFutureEvents; future event are:");
		//for (Event eee : futureEvents) eee.dump(1);
		
		/*
		 * Remove future events that are driven by each State that is
		 * defined within the Activity, or by any embedded Activities.
		 */
		 
		//System.out.println("\tNow removing events...");
		
		int noOfEventsDeleted = 0;
		
		SortedEventSet eventsToRemove = new SortedEventSetImpl();
		
		for (State state : activity.getStates())
		{
			//System.out.println("\tFor state " + state.getName());
			
			for (Event e : futureEvents)
			{
				if (e.getState() != state) continue;
				
				//System.out.println("\t\tFor event " + e);
				
				if (e.getTime() == getCurrentTime()) continue;
					// not a future event.
				
				// Remove the event from the queue.
				
				//System.out.println("\t\tMarking event for removal");
				
				eventsToRemove.add(e);
			}
		}
		
		for (Event event : eventsToRemove)
		{
			futureEvents.remove(event);
			noOfEventsDeleted++;
		}
		
		for (Activity a : activity.getActivities())  // recursive.
		{
			noOfEventsDeleted += purgeFutureEvents(a);
		}
		
		return noOfEventsDeleted;
	}


	public synchronized void initializeAttributes()
	throws
		ParameterError
	{
		/*
		 * Note: During simulation, if an Attribute is found to not have
		 * a value within the ModelScenario, then the Attribute's default
		 * value is retrieved and written to the ModelScenario. I.e., a
		 * lazy retrieval is used for default values.
		 */

		//System.out.println("Initializing Attribute values...");

		/*
		 * Identify all of the Attributes within the ModelDomain that are
		 * bound to Variables in a DecisionDomain, and for each of those,
		 * identify the current DecisionScenario and the Decision that
		 * applies to the Variable, if any.
		 */

		for (VariableAttributeBinding varAttrBinding :
				getModelDomain().getVariableAttributeBindings())

			// each Attribute in this ModelDomain that is bound to a
			// Variable in some DecisionDomain...
		{
			Variable variable = varAttrBinding.getVariable();
			Attribute attribute = varAttrBinding.getAttribute();


			// Identify the current DecisionScenario for the DecisionDomain
			// that owns the bound Variable.

			DecisionDomain varDecisionDomain =
				variable.getDecisionPoint().getDecisionDomain();

			DecisionScenario varDecisionScenario =
				varDecisionDomain.getCurrentDecisionScenario();

			if (varDecisionScenario == null)
				throw new ParameterError(
					"Cannot identify the DecisionScenario from which to " +
					"read the value of Variable " + variable.getName());


			// Copy the value from the Decision that is associated with
			// the bound Variable.

			Decision decision = varDecisionScenario.getDecision(variable);

			if (decision != null)
			{
				Object value = decision.getValue();
				getModelScenario().setAttributeValue(attribute, value);
				//System.out.println("...Decision: value=" + value + "...");
			}
			//else System.out.println("...no Decision...");
		}


		/*
		 * Identify all of the Attributes within the ModelDomain that are
		 * bound to State (in another ModelDomain), and for each of those,
		 * identify the current ModelScenario that applies to the ModelDomain
		 * that owns that State, read the State's final value, and use it
		 * to update the Attribute value in this SimulationRun's ModelScenario.
		 */

		for (AttributeStateBinding attrStateBinding :
			getModelDomain().getAttributeStateBindings())
		{
			Attribute attribute = attrStateBinding.getAttribute();
			State state = attrStateBinding.getForeignState();
			ModelDomain stateModelDomain = state.getModelDomain();

			ModelScenario stateModelScenario =
				stateModelDomain.getCurrentModelScenario();

			if (stateModelScenario == null)
				throw new ParameterError(
					"Could not identify the ModelScenario from which to " +
					"obtain the value of State " + state.getName());

			Serializable[] values = stateModelScenario.getStateFinalValues(state);
			
			Serializable value = attrStateBinding.deriveAttributeValue(values);

			stateModelScenario.setAttributeValue(attribute, value);
		}


		//System.out.println("...finished initializing Attribute values.");
	}


	public synchronized void signalEventProducers(ModelContainer container)
	{
		Set<ModelComponent> eventProducers = new TreeSet<ModelComponent>();

		for (ModelComponent component : container.getFunctions())
			eventProducers.add(component);
		
		for (ModelComponent component : container.getActivities())
			eventProducers.add(component);
		
		for (ModelComponent component : eventProducers)
		{
			// Generate a StartupEvent for the EventProducer at t=null.
			StartupEvent e = null;
			try { e = new StartupEventImpl(component, this); }
			catch (ParameterError ex) { throw new RuntimeException("Should not happen"); }
			
			futureEvents.add(e);
			
			// Generate events for sub-components, if any.
			
			if (component instanceof ModelContainer)
			{
				signalEventProducers((ModelContainer)component);
			}
		}
	}


	public synchronized void updateAttributes()
	throws
		ParameterError
	{
		//System.out.println("There are " +
		//	finalStateValues.keySet().size() +
		//		" final state values for ModelDomain " + modelDomain.getName() +
		//		":");

		//for (AttributeStateBinding binding :
		//	modelDomain.getAttributeStateBindings())
		//{
			//State state = binding.getForeignState();


		for (State state : finalStateValues.keySet())
		{
			Serializable finalValue = finalStateValues.get(state);
			
			//System.out.println("\tState " + state.getName() + ":" +
			//	finalValue.toString());

			for (AttributeStateBinding binding : state.getAttributeBindings())
			{
				Attribute attribute = binding.getAttribute();
				
				//System.out.println("\t\t (bound to Attribute " +
				//	attribute.getName() +  ")");

				ModelDomain attrModelDomain = attribute.getModelDomain();

				if (attrModelDomain == null)
				{
					throw new RuntimeException(
						"Attribute " + attribute.getName() + " has no ModelDomain");
				}


				// Get the ModelScenario for the Attribute's ModelDomain.
				
				ServiceContext context = ServiceContext.getServiceContext();
				ModelScenario scenario =
					(ModelScenario)(context.getScenario(attrModelDomain));

				if (scenario == null)
				{
					// Identify the "current" Scenario for that Domain.
					ModelScenario curScenario = 
						attrModelDomain.getCurrentModelScenario();
					
					if (curScenario != null)
					{
						// Create a new Scenario for the Domain, based on
						// the "current" Scenario.
						try
						{
							scenario = attrModelDomain.createScenario(
								attrModelDomain.getName(), curScenario);
						}
						catch (CloneNotSupportedException ex)
						{
							throw new ParameterError(
								"Some values in Scenario " +
								curScenario.getName() +
								" are not cloneable.", ex);
						}
					}
					else
					{
						scenario = attrModelDomain.createScenario(
							attrModelDomain.getName());
						
						try
						{
							attrModelDomain.setCurrentModelScenario(scenario);
						}
						catch (ElementNotFound ex)
						{
							throw new RuntimeException(ex);
						}
					}
				}

				scenario.setAttributeValue(attribute, finalValue);
			}
		}
	}


	/* -------------------------------------------------------------------------
	 * Implementation-defined (protected) methods.
	 */


	/**
	 * Find the Set of Events among currentEvents that do not actually cause
	 * the value on a Conduit to change. Ignore StartupEvents.
	 */
	 
	protected SortedEventSet findNoPropagateEvents(SortedEventSet currentEvents)
	{
		SortedEventSet noPropagateEvents = new SortedEventSetImpl();

		for (Event event : currentEvents)
		{
			if (event instanceof StartupEvent) continue;
			
			
			// Find any bound port for the State.
			
			State state = event.getState();
			Set<Port> boundPorts = state.getPortBindings();
			if (boundPorts.size() == 0) continue;
			
			//Port[] portsArray = boundPorts.toArray(new Port[2]);
			//Port anyBoundPort = portsArray[0];
			
			
			boolean changesValue = false;
			for (Port boundPort : boundPorts)
			{
				/* Determine if a Conduit is attached to the bound Port and the
					value driving the Conduit equals the Event value.
					
					If so, the Event is not a true logical Event and should not
					be propagated. */
				
				
				// Determine which State is had been driving the bound Port.
				
				State drivingState = getCurrentDriver(boundPort);
				//System.out.println("Current driver of " + boundPort.getFullName() +
				//	" is " + (drivingState == null? "null" : drivingState.getFullName()));
				
				//if (drivingState == state) continue;
				
				// Obtain the value that was driving the bound Port.
				
				Serializable currentPortValue = null;
				if (drivingState != null) currentPortValue = 
						finalStateValues.get(drivingState);
				
				Serializable newValue = event.getNewValue();

				// Determine if the Event actually changes the value driving
				// the Port. If not, it is not visible to any other Activities
				// or Functions.
				
				if (! ObjectValueComparator.compare(currentPortValue, newValue))
				{
					changesValue = true;
					//System.out.println("Value of Port " + boundPort + 
					//	" changes from " + currentPortValue + " to " + newValue);
					break;
				}
				//else System.out.println(currentPortValue + " = " + newValue);
			}
			
			if (! changesValue)  // Does not change value: do not propagate.
			{
				noPropagateEvents.add(event);
				//callback.showMessage(
				//	"*Event '" + event.toString() + "' does not " +
				//	"change any Ports and so it will not be propagated.");
			}
		}
		
		return noPropagateEvents;
	}


	/**
	 * Create and return a Set of Events that is a subset of the input Set,
	 * but not containing any events to which the specified PortedContainer
	 * is not sensitive.
	 */

	protected synchronized SortedEventSet getVisibleSubset(SortedEventSet allEvents,
		SortedEventSet noPropagateEvents, PortedContainer container)
	throws
		ModelContainsError
	{
		SortedEventSet visibleEvents = new SortedEventSetImpl();

		for (Event event : allEvents)
		{
			if (noPropagateEvents.contains(event)) continue;
			
			
			// Automatically include Startup Events, to ensure that the
			// associated component receives the Event.
			
			if (event instanceof StartupEvent)
			{
				visibleEvents.add(event);
				continue;
			}
			
			
			// For non-Startup Events:
			
			State state = event.getState();

			if (container instanceof Activity)
			{
				Set<Activity> sas = null;

				if ((sas = getSensitiveActivities(state)).contains((Activity)container))
				{
					//System.out.println("======" + container.getName() +
					//	": sas.size()=" + sas.size());

					visibleEvents.add(event);
				}
			}
			else if (container instanceof Function)
			{
				if (getSensitiveFunctions(state).contains((Function)container))
				{
					visibleEvents.add(event);
				}
			}
			else throw new RuntimeException(
				"Unexpected type of PortedContainer");
		}

		return visibleEvents;
	}
	
	
	protected synchronized SortedEventSet removeStartupEvents(SortedEventSet events)
	{
		Event[] eventArray = events.toArray(new Event[0]);
		for (Event event : eventArray)
		{
			if (event instanceof StartupEvent) events.remove(event);
		}
		
		return events;
	}
	

	/**
	 * Return a Set of Events that are statically defined by the model.
	 * Recursive. The caller provides the Set.
	 */

	protected synchronized void getPredefinedEvents(ModelElement element,
		SortedEventSet events)
	{
		if (element instanceof EventProducer)
		{
			for (State state : ((EventProducer)element).getStates())
			{
				SortedEventSet stateEvents = state.getPredefinedEvents();
				for (Event stateEvent : stateEvents)
				{
					Event event = stateEvent;
					if (stateEvent.getTime() == null) try
					{
						// Replace the Event with one that occurs during the first
						// simulation epoch, instead of at a null time.
						event = new PredefinedEventImpl(
							state, currentTime /* is set to simulation start */,
								stateEvent.getNewValue());
					}
					catch (ParameterError pe) { throw new RuntimeException(pe); }
					
					events.add(event);
				}
			}
		}

		// Recursively get Predefined Events of children.
		
		if (element instanceof ModelContainer)
		{
			ModelContainer container = (ModelContainer)element;

			for (ModelElement child : container.getFunctions())
			{
				getPredefinedEvents(child, events);
			}

			for (ModelElement child : container.getActivities())
			{
				getPredefinedEvents(child, events);
			}
		}
	}


	/**
	 * Return a Set of Events that are all of the same time and the earliest
	 * time among all future events, but not after the specified finalDate.
	 * Remove these events from the set of all future events. Null is never
	 * returned.
	 */

	protected synchronized SortedEventSet getCurrentEvents(Date priorEpoch,
		Date finalDate)
	{
		SortedEventSet currentEvents = new SortedEventSetImpl();

		for (Event event : futureEvents)
		{
			Date eventTime = event.getTime();  // may be null.

			if (eventTime == null)  // Current epoch is 0 time after prior epoch.
				eventTime = priorEpoch;
			else if (eventTime.getTime() < 0) throw new RuntimeException(
				"Event has negative time.");


			if ((finalDate != null) && (eventTime != null) && eventTime.after(finalDate))
			{
				// have passed the final epoch.
			
				break;
			}

			if (currentEvents.size() == 0)  // first time through in this new epoch.
				if (eventTime != null) currentTime = eventTime;
			
			if ((eventTime == null) || currentTime.equals(eventTime))
				currentEvents.add(event);
			else // should be later: we are done.
			{
				break;
			}
		}


		futureEvents.removeAll(currentEvents);
		
		/*if (currentEvents.isEmpty() && (! futureEvents.isEmpty()))
		{
			throw new RuntimeException(
				"getCurrentEvents returned an empty Set when there are future Events");
		}*/

		//System.out.println("...leaving getCurrentEvents.");
		return currentEvents;
	}


	/**
	 * Retrieve the current simulation epoch.
	 */

	protected synchronized Epoch getCurrentSimulationEpoch()
	{
		return epochs.get(epochs.size()-1);
	}


	/**
	 * See determineSensitivePorts.
	 *
	 * This implementation of this method caches its result in the SimulationRun.
	 */

	protected synchronized Set<Port> getSensitivePorts(State state)
	//protected synchronized Set<Port> getConnectedPorts(State state)
	throws
		ModelContainsError
	{
		Set<Port> ports = this.sensitivePorts.get(state);

		if (ports == null)
		{
			ports = getModelDomain().determineSensitivePorts(state);
			//ports = getModelDomain().determineConnectedPorts(state);
			this.sensitivePorts.put(state, ports);
		}

		return ports;
	}
	
	
	/**
	 * See determineReachablePorts.
	 *
	 * This implementation of this method caches its result in the SimulationRun.
	 */
	 
	protected synchronized Set<Port> getReachablePorts(State state)
	throws
		ModelContainsError
	{
		Set<Port> ports = reachablePorts.get(state);
		
		if (ports == null)
		{
			ports = getModelDomain().determineReachablePorts(state);
			this.reachablePorts.put(state, ports);
		}
		
		return ports;
	}


	/**
	 * See determineSensitiveFunctions.
	 *
	 * This implementation of this method caches its result in the SimulationRun.
	 */

	protected synchronized Set<Function> getSensitiveFunctions(State state)
	throws
		ModelContainsError // when a model conflict of any kind is detected.
	{
		Set<Function> functions = sensitiveFunctions.get(state);

		if (functions == null)
		{
			functions = getModelDomain().determineSensitiveFunctions(state);
			sensitiveFunctions.put(state, functions);
		}

		return functions;
	}


	/**
	 * See {@link #determineSensitiveActivities(State)}.
	 *
	 * This implementation of this method caches its result in the SimulationRun.
	 */

	protected synchronized Set<Activity> getSensitiveActivities(State state)
	throws
		ModelContainsError // when a model conflict of any kind is detected.
	{
		Set<Activity> activities = sensitiveActivities.get(state);
		//if (activities == null)
		//{
			activities = getModelDomain().determineSensitiveActivities(state);
		//	sensitiveActivities.put(state, activities);
		//}

		return activities;
	}
	
	
	/**
	 * Return the current simulation epoch.
	 */
	 
	protected Epoch getCurrentEpoch()
	{
		if (epochs.size() == 0) return null;
		return epochs.get(epochs.size()-1);
	}


	/**
	 * Return the Date corresponding to the current simulation epoch. This
	 * method is intended to be called during simulation, since outside of
	 * simulation there is no "current epoch".
	 */

	protected synchronized Date getCurrentTime()
	{
		Epoch epoch = getCurrentEpoch();
		if (epoch == null) return null;
		return epoch.getDate();
	}
	
	
	void waitForFinish()
	throws
		ParameterError
	{
		synchronized (this)
		{
			if (thread == null) throw new ParameterError(
				"Thread is null; perhaps simulation has not started yet.");
		}
		
		try { this.thread.join(); }
		catch (InterruptedException ie) {}
	}
	
	
	public void dumpEvents()
	{
		System.out.println("Events by iteration:");
		int iter = 0;
		for (SortedEventSet eventsForIter : iterEventList)
		{
			System.out.println("\tIteration " + (++iter));
			if (eventsForIter == null)
			{
				System.out.println("\t\t (null Event list)");
				continue;
			}
			
			for (Event event : eventsForIter)
			{
				System.out.println("\t\t" + event.toString());
			}
		}
	}
}

