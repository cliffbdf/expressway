	// debug
	public void setComponentAt(int index, Component component)
	{
		GlobalConsole.println("setComponentAt(" + index + "," + component.getClass().getName() + ")");
		super.setComponentAt(index, component);
	}
	
	public void setTabComponentAt(int index, Component component)
	{
		GlobalConsole.println("setTabComponentAt(" + index + "," + component.getClass().getName() + ")");
		super.setTabComponentAt(index, component);
	}
	
	public Component add(Component component)
	{
		GlobalConsole.println("add(" + component.getClass().getName() + ")");
		return super.add(component);
	}
	
	public Component add(Component component, int index)
	{
		GlobalConsole.println("add(" + component.getClass().getName() + "," + index + ")");
		return super.add(component, index);
	}
	
	public void add(Component component, Object constraints)
	{
		GlobalConsole.println("add(" + component.getClass().getName() + "," + "constraints...)");
		super.add(component, constraints);
	}
	
	public void add(Component component, Object constraints, int index)
	{
		GlobalConsole.println("add(" + component.getClass().getName() + ", constraints, " + index + ")");
		super.add(component, constraints, index);
	}
	
	public Component add(String title, Component component)
	{
		GlobalConsole.println("add('" + title + "'," + component.getClass().getName() + ")");
		return super.add(title, component);
	}
	
	public void addTab(String title, Component component)
	{
		if (title == null) throw new RuntimeException("title is null");
		GlobalConsole.println("addTab('" + title + "'," + component.getClass().getName() + ")");
		super.addTab(title, component);
	}
	
	public void addTab(String title, javax.swing.Icon icon, Component component)
	{
		GlobalConsole.println("addTab('" + title + "', icon, " + component.getClass().getName() + ")");
		super.addTab(title, icon, component);
	}
	
	public void addTab(String title, javax.swing.Icon icon, Component component, String tip)
	{
		GlobalConsole.println("addTab('" + title + "', icon, " + component.getClass().getName() +
			", '" + tip + "')");
		super.addTab(title, icon, component, tip);
	}
	// end debug
	
	
	
	

