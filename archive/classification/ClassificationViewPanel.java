/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.classification;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.gui.*;
import expressway.ser.*;


/**
 * Provide a hierarchical depiction of a ClassificationElements. The View may be
 * rooted at any Classification - not necessarily a ClassificationDomain. Elements are
 * editable and deletable, and can be moved from one place to another within
 * their hierarchy.
 */
 
public class ClassificationViewPanel
	extends GenericTreeTablePanelView<ClassificationSer>
{
	public ClassificationViewPanel(PanelManager panelManager, NodeSer nodeSer, 
		ViewFactory viewFactory, ModelEngineRMI modelEngine)
	{
		super(panelManager, nodeSer, viewFactory, modelEngine, "list");
	}
	
	
	protected Class getTreeNodeVisualType() { return ClassificationVisualJ.class; }
	
	
	protected Class getAttributeVisualType() { return ClassificationAttributeVisualJ.class; }
}

