/*
 */

package expressway.gui.activityrisk;

import java.util.Date;
import javax.swing.tree.*;
import javax.swing.tree.TreeNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;


/**
 */
public class ActivityRiskMitModel extends DefaultTreeTableModel
{
	/** Names of the columns. */
	private static final String[] columnNames =
		{ "Name", "Std Evidence", "Criteria", "Act Evidence", "Comment" };

	
    /** Classes presenting the data. */
	private static final Class[] classes =
		{ String.class, String.class, String.class, String.class, String.class };


	public ActivityRiskMitModel(DefaultMutableTreeTableNode root)
	{
		super(root);
		
		// Set column names.
		
		// 
		
		
		
		//super(root, columnNames, methodNames, setterMethodNames, classes);
	}


	.....public Object getValueAt(Object arg0, int arg1)
	{
		if(arg0 instanceof ActivityTreeData)
		{
			ActivityTreeData data = (ActivityTreeData)arg0;
			if(data != null)
			{
				switch(arg1)
				{
				case 0: return data.getName();
				case 1: return data.getStdEvidence();
				case 2: return data.getCriteria();
				case 3: return data.getActEvidence();
				case 4: return data.getComment();
				}
			}
			
		}
		
		if(arg0 instanceof DefaultMutableTreeTableNode)
		{
			DefaultMutableTreeTableNode dataNode = (DefaultMutableTreeTableNode)arg0;
			ActivityTreeData data = (ActivityTreeData)dataNode.getUserObject();
			if(data != null)
			{
				switch(arg1)
				{
				case 0: return data.getName();
				case 1: return data.getStdEvidence();
				case 2: return data.getCriteria();
				case 3: return data.getActEvidence();
				case 4: return data.getComment();
				}
			}
			
		}
		return null;
	}
	

	public int getChildCount(Object arg0)
	{
		
		if(arg0 instanceof DefaultMutableTreeTableNode)
		{
			DefaultMutableTreeTableNode nodes = (DefaultMutableTreeTableNode)arg0;
			return nodes.getChildCount();
		}
		return 0;
	}
	

	public int getIndexOfChild(Object arg0, Object arg1)
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public boolean isLeaf(Object node) 
	{
		return getChildCount(node) == 0;
	}


    /**
     * <code>isCellEditable</code> is invoked by the JTreeTable to determine
     * if a particular entry can be added. This is overridden to return true
     * for the first column, assuming the node isn't the root, as well as
     * returning two for the second column if the node is a BookmarkEntry.
     * For all other columns this returns false.
     */
    public boolean isCellEditable(Object node, int column)
	{
		switch (column)
		{
		case 0:
			// Allow editing of the name, as long as not the root.
			return (node != getRoot());
		case 1:
		case 2:
		case 3:
		case 4:
			// Allow editing
			return (node instanceof ActivityTreeData);
		default:
			return false;
		}
    }
}

