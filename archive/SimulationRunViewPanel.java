/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import expressway.common.*;
import expressway.gui.*;
import expressway.ser.*;
import java.util.Date;
import javax.swing.JTabbedPane;


/** ****************************************************************************
 * Implements a TreeTable View of a Simulation Run, in which States are listed
 * under the Component hierarchy and their values over time are displayed to
 * the right.
 */
 
public class SimulationRunViewPanel extends AbstractGraphicViewPanel
	 implements View
{
	public SimulationRunViewPanel(JTabbedPane container, 
		SimulationRunSer simRunSer, ModelEngineRMI modelEngine, String name)
	throws
		Exception
	{
		super(container, modelEngine, name);
	}




  /* ***************************************************************************
   * Methods defined in SimulationRunVisual.
   */


	public void attributesUpdated() {}


	public void stateInitEventsCreated() {}


	public void predefinedEventsRetrieved() {}


	public void epochStarted(int epochNo, Date date) {}


	public void epochFinished(int epochNo, Date date) {}


	public void allFunctionsEvaluatedForEpoch(int epochNo, Date date) {}


	public void allActivitiesActivatedForEpoch(int epochNo, Date date) {}


	public void notifyVisualMoved(VisualComponent visual) {}


	public ViewConstants getDisplayConstants() { return null; }


	public void notifyVisualMoved(VisualComponent visual) {}
}

