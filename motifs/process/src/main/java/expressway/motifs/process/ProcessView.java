/*
 * Confidential property of Cliff Berg.
 * Copyright (c) 2007-2022 by Cliff Berg. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.motifs.process;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.gui.modeldomain.VisualJComponent;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;



/**
 * For displaying the structure of a Process.
 * Display a ModelView as usual but layer on Visuals for the Roles, each in a
 * horizontal row, separated from the other rows by a line. Dragging an Activity
 * into a row causes the Activity to assume the Role of that row. The last (bottom)
 * row is the 'Unassigned' row, for Activities that do not have a Role assigned
 * to them.
 */
 
public class ProcessView extends ModelScenarioViewPanel
{
	public static final Color RoleVisualColor = Color.lightGray;
	public static final int RoleWidth = 30;
	public static final double PiOver2 = Math.PI/2.0;
	public static final java.awt.Font LabelFont = new Font("Arial", Font.BOLD, 14);
	public static final java.awt.Color LabelTextColor = Color.black;
	public static final java.awt.FontMetrics LabelFontMetrics =
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(LabelFont);
	
	private String[] roleIds;
	private double rowHeight;
	
	
	public ProcessView(PanelManager panelManager, 
		NodeSer nodeSer, ViewFactory viewFactory, ModelEngineRMI modelEngine)
	{
		super(panelManager, nodeSer, viewFactory, modelEngine);
		
		
		// Extend the VisualFactory so that RoleVisuals can be created.
		
		final VisualComponentFactory baseFactory = getVisualFactory();
		setVisualFactory(new GraphicVisualComponentFactory()
		{
			public Class getVisualClass(NodeSer nodeSer)
			{
				if (nodeSer instanceof RoleSer)
					return ProcessRoleVisual.class;
				else
					return baseFactory.getVisualClass(nodeSer);
			}
			
			
			public VisualComponent makeGraphicVisual(NodeSer nodeSer, Container container,
				boolean asIcon)
			throws
				ParameterError,
				Exception
			{
				if (nodeSer instanceof RoleSer)
					return new ProcessRoleVisual((RoleSer)nodeSer);
				else
					return baseFactory.makeGraphicVisual(nodeSer, container, asIcon);
			}
		});
	}
	
	
	/**
	 * Extend to add Visuals for the Roles used by this View's Activities.
	 */
	 
	public void populateChildren()
	throws
		ParameterError,
		Exception
	{
		// Get the Role Ids used by this View's Activities and row height from
		// the server.
		
		Object obj = getModelEngine().getAttributeDefaultValue(getNodeSer().getFullName() + ".Role Ids");
		
		if (! (obj instanceof String[])) throw new ParameterError(
			"Attribute 'Roles' is of type " + obj.getClass().getName() +
			"; must be compatible with type String[].");
		
		this.roleIds = (String[])obj;
		
		obj = getModelEngine().getAttributeDefaultValue(getNodeSer().getFullName() + ".Row Height");
		if (! (obj instanceof Number)) throw new ParameterError(
			"Attribute 'Row Height' is of type " + obj.getClass().getName() +
			"; must be compatible with type Number.");
		this.rowHeight = ((Number)obj).doubleValue();
		
		// Call addNode for each Role.

		for (String roleId : roleIds) addNode(roleId);
		super.populateChildren();
	}


	/**
	 * Extend to enable this View to create Visuals for the Roles and place them
	 * in rows (instead of in the locations that they occupy in their own Domain).
	 * This is necessary because the Roles do not belong to the same Domain as the
	 * Activities in this View.
	 */
	 
	public Set<VisualComponent> addNode(String nodeId)
	throws
		Exception
	{
		Set<VisualComponent> visualsCreated = new HashSet<VisualComponent>();
		
		// Recognize when a Role is being added and place it appropriately.
		NodeSer nodeSer = getModelEngine().getNode(nodeId);
		if (nodeSer instanceof RoleSer)
		{
			RoleSer roleSer = (RoleSer)nodeSer;
			
			VisualComponent visual =
				makeVisual(roleSer, (Container)(this.getDisplayArea()), true);
			
			// Set RoleVisual's location. This cannot be done in the normal way,
			// which would be to call setLocation(), because the Roel does not
			// belong to the Domain being displayed.
			if (panelManager != null) panelManager.add(visual);
			visual.validate();
			
			// Standard protocol for initializing a Visual.
			visual.setView(this);	
			visual.refreshRedundantState();
			visual.setVisible(true);

			// Add standard mouse listeners to the new Visual.
			visual.addMouseListener(this.getPopupListener());
			visual.addMouseListener(this.getMouseInputListener());
			visual.addMouseMotionListener(this.getMouseMotionListener());

			// Subscribe to receive update notifications for the Node.
			// Note that this does NOT result in additional server connections.
			if (this.getListenerRegistrar() != null)
				this.getListenerRegistrar().subscribe(nodeId, true);
			
			visualsCreated.add(visual);
		}
		else
			visualsCreated.addAll(super.addNode(nodeId));
		
		return visualsCreated;
	}
	
	
	/**
	 * Intecept the resize action that occurs when the user attempts to drag the
	 * corner of a Visual.
	 */
	 
	public void resizeNode(GraphicVisualComponent visual, double dw, double dh,
		double childShiftX, double childShiftY, boolean notify)
	throws
		Exception
	{
		if (visual instanceof ProcessRoleVisual)
		{
			return;  // do not allow resizing of a Role Visual. Its size is
				// determined by the ProcessRoleVisual validate method.
		}
		else
			super.resizeNode(visual, dw, dh, childShiftX, childShiftY, notify);
	}
		
		
	/**
	 * Intecept the move action that occurs when the user attempts to drag a Visual.
	 */
	 
	public void moveNode(GraphicVisualComponent visual, double dx, double dy, boolean notify)
	throws
		Exception
	{
		if (visual instanceof ProcessRoleVisual)
		{
			ProcessRoleVisual roleVisual = (ProcessRoleVisual)visual;
			
			// If the Role Visual is being moved to a new row, move the Role's row
			// to above or below the destination row, depending on whether the
			// new position (indicated by dx,dy) is above or below the centerline
			// of the destination row.
			
			int currentRowNo = getRowNoForRoleId(visual.getNodeId());
			int newRowNo = getRowNoForYLocation(visual.getNodeSer().getY() + dy);
			if (currentRowNo != newRowNo) try
			{
				String newRoleId = getRoleIdForRowNo(newRowNo);
				if (newRoleId == null) return;  // just leave the RoleVisual where it is.
					
				CrossReferenceSer[] crSers =
					getModelEngine().getCrossReferences(roleVisual.getNodeId(), null);
				if (crSers.length == 0) throw new ModelContainsError(
					"Role '" + roleVisual.getFullName() + "' has no 
				if (crSers.length > 1) throw new ModelContainsError(
					"There is more than one Role assigned to " + toNodeSer.getFullName());
				
				CrossReferenceSer crSer = crSers[0];
				getModelEngine().deleteCrossReference(confirm, crSer.getNodeId());
				getModelEngine().createCrossReference(confirm, 
					"Performs", newRoleId, crSer.toNodeId);
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog((Component)(visual.getDisplayArea()), ex);
			}
		}
		else
			super.moveNode(visual, dx, dy, notify);
	}
	
	
	/**
	 * Define a VisualComponent to depict Roles in this View. Note that the Roles
	 * belong to a different Domain that the other Nodes depicted in this View.
	 * Therefore, the Roles cannot be drawn at the locations that they specify
	 * in their NodeSers, since those locations are relative to the Role's parent
	 * in their Domain. In this View, the Roles are drawn along the left hand side
	 * of the View, from top to bottom, beginning with the first Role listed in
	 * 'roleIds'.
	 */
	 
	protected class ProcessRoleVisual extends VisualJComponent
	{
		ProcessRoleVisual(RoleSer roleSer)
		throws
			ParameterError
		{
			super(roleSer, ProcessView.this, true);
			setLayout(null);
			setBackground(Color.lightGray);
		}
		
		
		RoleSer getRoleSer() { return (RoleSer)(getNodeSer()); }
		
		
		/**
		 * Border draws a title and we don't want that.
		 */
		 
		public boolean useBorderWhenIconified() { return false; }
		
		
		/**
		 * Override to re-set the position and size of this RoleVisual after the
		 * View's layout has changed.
		 */
		 
		public void validate()
		{
			int h = transformNodeDYToView(rowHeight);
			setSize(RoleWidth, h);
			setLocation(0, h * getRowNoForRoleId(getNodeId()));
		}
		
		
		/**
		 * Paint with text label at 90 degrees.
		 */
		 
		public void paintComponent(Graphics g)
		{
			Dimension size = getSize();
			g.setColor(RoleVisualColor);
			g.fillRect(0, 0, size.width, size.height);
			g.setColor(Color.black);
			g.drawRect(0, 0, size.width, size.height);
			
			String stringValue = getNodeSer().getName();
			
			char[] charValue = stringValue.toCharArray();
			int stringWidth = LabelFontMetrics.charsWidth(charValue, 0, stringValue.length());
			int charHeight = LabelFontMetrics.getHeight();
			
			int xNotchDisplay = (getWidth() - charHeight) / 2;
			int yNotchDisplay = (getHeight() - stringWidth) / 2;

			((Graphics2D)g).rotate(-PiOver2, xNotchDisplay, yNotchDisplay);
			
			g.setColor(Color.black);
			g.drawString(stringValue, xNotchDisplay - stringWidth - 10, 
				yNotchDisplay);
			
			((Graphics2D)g).rotate(PiOver2, xNotchDisplay, yNotchDisplay);
		}
	}
	
	
	int getRowNoForRoleId(String roleId)
	{
		int = 0;
		for (String id : roleIds)
		{
			if (id.equals(roleId)) return i;
			i++;
		}
		throw new RuntimeException("Row with Id '" + roleId + "' not found");
	}
	
	
	/**
	 * May return null.
	 */
	
	String getRoleIdForRowNo(int rowNo)
	{
		return roleId[rowNo];
	}
}

